﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Threading.Tasks;
using log4net;

namespace SalesLogicExpress.Presentation.Converters
{
    public class WidthConverterCustStopSequnecing : IValueConverter
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Presentation.Converter.WidthConverterCustStopSequnecing");
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            try
            {
                return Double.Parse(value.ToString()) - (parameter != null ? System.Convert.ToInt32(parameter) : 10);

            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Presentation.Converter][WidthConverterCustStopSequnecing][Convert][Message: " + ex.StackTrace + "]");
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (string)value;
        }
    }
}
