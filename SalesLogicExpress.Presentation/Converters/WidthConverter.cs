﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SalesLogicExpress.Presentation.Converters
{
    public class WidthConverter : IValueConverter
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Presentation.Converter.WidthConverter");
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            try
            {
                return Double.Parse(value.ToString()) - (parameter != null ? System.Convert.ToInt32(parameter) : 35);

            }
            catch (Exception e)
            {
                log.Error("[SalesLogicExpress.Presentation.Converter][WidthConverter][Convert][Message: " + e.StackTrace + "]");
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (string)value;
        }
    }
}
