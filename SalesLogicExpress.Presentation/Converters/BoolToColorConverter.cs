﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SalesLogicExpress.Presentation.Converters
{
    public class BoolToColorConverter : IValueConverter
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Presentation.Converter.BoolToColorConverter");
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            try
            {
                log.Info("[SalesLogicExpress.Presentation.Converter][BoolToColorConverter][Start:Convert]");
                if(parameter==null)
                {
                    return parameter; 
                }

                string[] color=parameter.ToString().Split(',');
                log.Info("[SalesLogicExpress.Presentation.Converter][BoolToColorConverter][End:Convert]");
                return (System.Convert.ToBoolean(value) ? color[0].Trim() : color[1].Trim());
                
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Presentation.Converter][BoolToColorConverter][Convert][Message: " + ex.StackTrace + "]");
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (string)value;
        }
    }
}
