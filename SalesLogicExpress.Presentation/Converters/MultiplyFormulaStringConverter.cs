﻿using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SalesLogicExpress.Presentation.Converters
{
    public class MultiplyFormulaStringConverter : IMultiValueConverter
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Presentation.Converter.MultiplyFormulaStringConverter");
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            log.Info("[SalesLogicExpress.Presentation.Converter][MultiplyFormulaStringConverter][Start:Convert]");
            double result = 1;
            for (int i = 0; i < values.Length; i++)
            {
                result = result * System.Convert.ToDouble(values[i]);
            }
            log.Info("[SalesLogicExpress.Presentation.Converter][MultiplyFormulaStringConverter][End:Convert]");
            return result;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
