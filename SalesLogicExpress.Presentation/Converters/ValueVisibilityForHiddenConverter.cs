﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SalesLogicExpress.Presentation.Converters
{
    public class ValueVisibilityForHiddenConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || value.ToString().Trim().Length == 0 || value.ToString().Trim() == "NA" || value.ToString().Trim() == "na" || value.ToString().Trim().ToLower() == "false" || value.ToString().Trim().ToLower() == "1")
            {
                return System.Windows.Visibility.Hidden;
            }
            if (value.ToString() == "")
            {
                return System.Windows.Visibility.Hidden;
            }
            return System.Windows.Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (bool)value;
        }
    }
}
