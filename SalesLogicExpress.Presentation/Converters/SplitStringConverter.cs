﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SalesLogicExpress.Presentation.Converters
{
    public class SplitStringConverter : IValueConverter
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Presentation.Converter.SplitStringConverter");
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                log.Info("[SalesLogicExpress.Presentation.Converter][SplitStringConverter][Start:Convert]");
                if (value == null) return "";
                string currentString = (string)value;
                string s = value.ToString();
                string[] param = parameter.ToString().Split(',');
                log.Info("[SalesLogicExpress.Presentation.Converter][SplitStringConverter][End:Convert]");
                if(currentString != string.Empty)
                    currentString = currentString.Split(System.Convert.ToChar(param[0]))[System.Convert.ToInt32(param[1])];                
                return currentString;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Presentation.Converter][SplitStringConverter][Convert][Message: " + ex.StackTrace + "]");
                return new string[] { "", "" };
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (string)value;
        }
    }
}
