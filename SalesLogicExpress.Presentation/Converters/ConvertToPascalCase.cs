﻿using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
namespace SalesLogicExpress.Presentation.Converters
{
    public class ConvertToPascalCase : IValueConverter
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Presentation.Converter.ConvertToPascalCase");
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            log.Info("[SalesLogicExpress.Presentation.Converter][ConvertToPascalCase][Start:Convert]");
            string strPascal = "";
            if (!(value == null || value.ToString() == ""))
            {
                string tempString = value.ToString();
                strPascal = tempString.ToLower().Trim();
                strPascal = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(strPascal);
                log.Info("[SalesLogicExpress.Presentation.Converter][ConvertToPascalCase][End:Convert]");
                return strPascal;
            }
            return value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value.ToString();
        }
    }
}
