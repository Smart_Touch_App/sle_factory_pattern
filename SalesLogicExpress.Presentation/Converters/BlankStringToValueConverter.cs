﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SalesLogicExpress.Presentation.Converters
{
    public class BlankStringToValueConverter : IValueConverter
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Presentation.Converter.BlankStringToValueConverter");
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string formattedString = string.Empty;
            log.Info("[SalesLogicExpress.Presentation.Converter][BlankStringToValueConverter][Start:Convert]");
            if (value == null)
            {
                value = "";
            }

            if(parameter==null)
            {
                parameter = "";
            }
            log.Info("[SalesLogicExpress.Presentation.Converter][BlankStringToValueConverter][End:Convert]");
            return ((value.ToString().Trim().Length<=0) ? parameter.ToString() : value.ToString().Trim());
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (string)value;
        }
    }
}
