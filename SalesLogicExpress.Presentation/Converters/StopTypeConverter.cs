﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SalesLogicExpress.Presentation.Converters
{
    public class StopTypeConverter : IValueConverter
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Presentation.Converter.StopTypeConverter");
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string SequenceNo = "";
            log.Info("[SalesLogicExpress.Presentation.Converter][StopTypeConverter][Start:Convert]");
            if (!(value == null || value.ToString() == ""))
            {
                try
                {
                    int number = System.Convert.ToInt32(value);
                  
                        if (number < 10)
                        {
                            SequenceNo = 0 + System.Convert.ToString(value);
                        }
                        else
                        {
                            SequenceNo = System.Convert.ToString(value);
                        }
                    
                }
                catch (Exception ex)
                {
                    log.Error("[SalesLogicExpress.Presentation.Converter][StopTypeConverter][Convert][Message: " + ex.StackTrace + "]");
                    return null;
                }
            }
            log.Info("[SalesLogicExpress.Presentation.Converter][StopTypeConverter][End:Convert]");
            return SequenceNo;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (string)value;
        }

    }
}
