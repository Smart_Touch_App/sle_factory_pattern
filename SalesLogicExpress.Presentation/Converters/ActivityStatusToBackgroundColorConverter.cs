﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SalesLogicExpress.Presentation.Converters
{
    public class ActivityStatusToBackgroundColorConverter : IValueConverter
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Presentation.Converter.ActivityStatusToBackgroundColorConverter");
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string backgroundColorCode = string.Empty;
            log.Info("[SalesLogicExpress.Presentation.Converter][ActivityStatusToBackgroundColorConverter][Start:Convert]");
            try
            {
                switch (value.ToString().Trim())
                {
                    case "PICK ORDER":
                    case "DELIVERY SCREEN":
                    case "CREATE ORDER":
                    case "CASH DELIVERY":
                    case "RTN ORD PRINT":
                    case "RTN ORD ENTRY":
                    case "RTN ORD PICK":
                        backgroundColorCode = "#0081d1";
                        break;

                    case "VOID":
                        backgroundColorCode = "#e94932";
                        break;

                    case "SETTLED":
                    case "VOIDED / SETTLED":
                        backgroundColorCode = "#A5A5A5";
                        break;

                    case "DELIVERED":
                    case "PREORDER":
                        backgroundColorCode = "#13AB31";
                        break;
                }
                log.Info("[SalesLogicExpress.Presentation.Converter][ActivityStatusToBackgroundColorConverter][End:Convert]");
                return backgroundColorCode;

            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Presentation.Converter][ActivityStatusToBackgroundColorConverter][Convert][Message: " + ex.StackTrace + "]");

                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (string)value;
        }
    }
}
