﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SalesLogicExpress.Presentation.Converters
{
    public class PhoneFormatConverter : IValueConverter
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Presentation.Converter.PhoneFormatConverter");
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            log.Info("[SalesLogicExpress.Presentation.Converter][PhoneFormatConverter][Start:Convert]");
            if (value == null) return "";
            string phoneno = Regex.Replace(value.ToString(), "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
            try
            {
                string fphoneno = (string.IsNullOrEmpty(phoneno) || phoneno == "NA" ? "" : phoneno);//" " + String.Format("{0:(###) ###-#### ##}", Double.Parse(phoneno)));
                fphoneno = fphoneno.IndexOf("()") > -1 ? fphoneno.Replace("()", "") : fphoneno;
                log.Info("[SalesLogicExpress.Presentation.Converter][PhoneFormatConverter][End:Convert]");
                return fphoneno;
            }
            catch (Exception e)
            {
                log.Error("[SalesLogicExpress.Presentation.Converter][PhoneFormatConverter][Convert][Message: " + e.StackTrace + "]");
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (string)value;
        }
    }
}
