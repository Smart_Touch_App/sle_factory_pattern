@echo off

setlocal
call sqlanyenv.bat
cd %CD%\remote_
start cmd /k dbsrv16 -n remote_eng remote.db -n remote_db
timeout /t 8

"%__SABIN%\dbisql" -onerror exit -c "UID=dba;PWD=sql;Server=remote_eng;DBN=remote_db;ASTART=No" SET OPTION PUBLIC.ml_remote_id = '%1'

"%__SABIN%\dbmlsync" -c "SERVER=remote_eng;DBN=remote_db;UID=dba;PWD=sql" -qc -n pub_validate_user -u validate_user

if errorlevel 1 goto RunTimeError
goto done

:RunTimeError
echo Error: Error occurred while starting Remote Database. Filename 'StartRemoteDB.bat'
SET errorlevel=1

:done
SET __SA=
SET __SABIN=
SET __SASAMPLES=
exit /b %errorlevel%
endlocal