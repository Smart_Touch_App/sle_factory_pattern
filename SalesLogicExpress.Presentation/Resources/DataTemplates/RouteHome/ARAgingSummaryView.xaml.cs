﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SalesLogicExpress.Presentation.Helpers;
using System.Threading.Tasks;
using Telerik;
using Telerik.Windows.Data;
using System.ComponentModel;

namespace SalesLogicExpress.Resources.DataTemplates.RouteHome
{
    public partial class ARAgingSummaryView
    {
        public ARAgingSummaryView()
        {

        }

        private void grd_Summary_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            Telerik.Windows.Controls.RadGridView grd = sender as Telerik.Windows.Controls.RadGridView;
            GroupDescriptor countryDescriptor = new GroupDescriptor();
            countryDescriptor.Member = "Age";
            countryDescriptor.SortDirection = ListSortDirection.Descending;
            grd.GroupDescriptors.Add(countryDescriptor);
        }
    }
}
