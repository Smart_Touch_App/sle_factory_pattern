﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Domain;
using System.Collections.ObjectModel;

namespace SalesLogicExpress.Resources.DataTemplates.ReturnsAndCredits
{
    /// <summary>
    /// Interaction logic for CustomerOrdersView.xaml
    /// </summary>
    public partial class CustomerOrdersView
    {
        RadGridView refGrid = new RadGridView();
        public CustomerOrdersView()
        {

        }

        private void btnShowZeroQty_Checked(object sender, RoutedEventArgs e)
        {
            Telerik.Windows.Controls.GridViewColumn qtyColumn = refGrid.Columns["QtyAvailToReturn"];
            if (qtyColumn == null)
            {
                return;
            }
            qtyColumn.ClearFilters();
        }

        private void btnShowZeroQty_Unchecked(object sender, RoutedEventArgs e)
        {
            FilterQuantityColumn();
        }

        private void grd_Orders_Loaded(object sender, RoutedEventArgs e)
        {
            Telerik.Windows.Controls.RadGridView grd = sender as Telerik.Windows.Controls.RadGridView;

            refGrid = grd;
            //FilterQuantityColumn();

        }
        private void FilterQuantityColumn()
        {
            Telerik.Windows.Controls.GridViewColumn qtyColumn = refGrid.Columns["QtyAvailToReturn"];
            if (qtyColumn == null)
            {
                return;
            }
            // Getting it from the property will create it and associate it with its column automatically.
            IColumnFilterDescriptor columnDescriptor = qtyColumn.ColumnFilterDescriptor;
            columnDescriptor.SuspendNotifications();
            columnDescriptor.FieldFilter.Filter1.Operator = FilterOperator.IsNotEqualTo;
            columnDescriptor.FieldFilter.Filter1.Value = "0";
            columnDescriptor.ResumeNotifications();
        }
    }
}
