﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Threading;
using ViewModelApplication = SalesLogicExpress.Application;
using viewmodels = SalesLogicExpress.Application.ViewModels;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace SalesLogicExpress.Resources.Pop_Ups
{
    /// <summary>
    /// Interaction logic for CreditProcessDialogs.xaml
    /// </summary>
    public partial class CreditProcessDialogs : BaseWindow
    {
        viewmodels.CreditProcessViewModel viewModel;
        public CreditProcessDialogs()
        {
        }

        private void txt_ZipCode_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (viewModel != null)
            {
                viewModel.IsSwitchDisable = true;
            }
            Regex regex = new Regex("^[0-9]([-][0-9]{1,3})?$");
            e.Handled = !regex.IsMatch(e.Text);
        }
    }
}
