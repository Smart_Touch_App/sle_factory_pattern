﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SalesLogicExpress.Presentation.Helpers
{
    public class ResourceManager
    {
        public static readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Presentation.Helpers");

        public enum ApplicationWindowState
        {
            Minimized,
            Maximized,
            FullScreen
        }
        public static ApplicationWindowState AppWindowState = ApplicationWindowState.Maximized;
        public static void SetWindowState(Window window)
        {
            double width = SystemParameters.PrimaryScreenWidth;
            double height = SystemParameters.PrimaryScreenHeight;
            int nTaskBarHeight = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Bottom - System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Bottom;

            switch (AppWindowState)
            {
                case ApplicationWindowState.FullScreen:
                    window.Height = height;
                    window.Width = width;

                    break;
                case ApplicationWindowState.Maximized:
                    window.Height = height - nTaskBarHeight+12;//Sathish adding the height value so that it will fit into screen
                    window.Width = width;
                    break;
                case ApplicationWindowState.Minimized:

                    break;
            }
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][ResourceManager][SetWindowState][WindowTitle=" + window.Title + "][width=" + window.Width.ToString() + "][height=" + window.Height.ToString() + "]");

        }

        public static void ToggleWindowState()
        {
            Window window = App.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive);
            SetWindowState(window);
        }
    }
}
