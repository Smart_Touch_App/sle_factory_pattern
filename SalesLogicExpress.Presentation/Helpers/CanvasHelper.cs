﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SalesLogicExpress.Helpers
{
    public static class CanvasHelper
    {
        //Sathish Changed this code.Disposing the objects
        //private static readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Helpers.CanvasHelper");
        public const string SaveAsFileName = @"\Docs\test.jpg";

        public static void CreateBlankImage()
        {
            if (!File.Exists("blank.jpg"))
            {
                if (File.Exists(ResourceManager.DomainPath + @"Resources\Images\blank.jpg"))
                    File.Copy(ResourceManager.DomainPath + @"Resources\Images\blank.jpg", "blank.jpg");
            }

        }

        public static InkCanvas GlobalCanvas { get; set; }
        //public static void SaveCanvas(InkCanvas canvas)
        //{
        //    CreateImage(ResourceManager.DomainPath +SaveAsFileName, canvas);
        //}

        //private static void CreateImage(string fileName, InkCanvas canvas)
        //{
        //    System.IO.Directory.CreateDirectory(ResourceManager.DomainPath + @"\Docs");
        //    if (File.Exists(fileName))
        //    {
        //        File.Delete(fileName);
        //    }
        //    RenderTargetBitmap rtb = new RenderTargetBitmap((int)canvas.ActualWidth - 10, (int)canvas.ActualHeight, 0, 0, PixelFormats.Pbgra32);
        //    rtb.Render(canvas);
        //    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
        //    encoder.Frames.Add(BitmapFrame.Create(rtb));
        //    FileStream file = new FileStream(fileName, FileMode.Create);
        //    encoder.Save(file);
        //    file.Close();
        //}

        public static void ClearCanvas()
        {
            if (GlobalCanvas != null)
            {
                GlobalCanvas.Strokes.Clear();
                //Logger.Error("ClearCanvas  StackTrace:" + System.Environment.StackTrace);
            }
        }
        //public static bool IsCanvasFilled()
        //{
        //    if (GlobalCanvas != null)
        //    {
        //        return GlobalCanvas.Strokes.Count > 0 ? true : false;
        //    }
        //    else
        //    {
        //        return false;
        //    }

        //}
    }
}
