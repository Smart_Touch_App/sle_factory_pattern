﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using GalaSoft.MvvmLight.Messaging;
using Telerik.Windows.Controls;
using SalesLogicExpress.Presentation.Helpers;
using ViewModels = SalesLogicExpress.Application.ViewModels.ProspectViewModels;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows.Threading;
using log4net;
using SalesLogicExpress.Application.Helpers;
using viewmodels = SalesLogicExpress.Application.ViewModels;
using ViewModelApplication = SalesLogicExpress.Application;
using SalesLogicExpress.Application.ViewModels;


namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for ProspectHome.xaml
    /// </summary>
    public partial class ProspectHome : BaseWindow,IDisposable
    {
        viewmodels.ProspectsHome viewModel = null;
        viewmodels.ProspectsHome viewModelProspectHome = null;
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Views.ProspectHome");
        int Count = 0;

        public ProspectHome()
        {
            InitializeComponent();
            Logger.Info("[SalesLogicExpress.Views.ProspectHome][ProspectHome][Enter in Constructor]");
            DefaultSeletedTab = "DashboardProspectTab";
            ScreenID = Helpers.Constants.Title_CustomerHomeViewTitle;
            ScreenLifeCycle = Helpers.Constants.ApplicationLifeCycle;
            this.DataContext = viewModelProspectHome;
            this.Activated += ProspectHome_Activated;
            LoadContext();
            AddTagsToControls();
            ButtonEditProspect.IsEnabled = false;
            ButtonDeleteProspect.IsEnabled = false;
            ButtonEditExpensed.IsEnabled = false;
            ButtonDeleteExpensed.IsEnabled = false;
        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Service Route";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = ViewModelMappings.View.ProspectHome.GetEnumDescription();
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.ServiceRoute;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ProspectHome;

        }
        void ProspectHome_Activated(object sender, EventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Views.ProspectHome][WindowActivated][Enter in WindowActivated]");

            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }

        public void CloseForwardedWindows()
        {
            List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View> WindowsToClosed = new List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View>();
            AssociatedWindows = WindowsToClosed;
            CloseAssociatedWindows();
        }

        private void AddTagsToControls()
        {
            ContactProspectTab.Tag = Helpers.Constants.PD_ViewContacts;
            DashboardProspectTab.Tag = Helpers.Constants.PD_ViewDashboard;
            InformationProspectTab.Tag = Helpers.Constants.PD_ViewInfo;
            //CompetitorInformationProspectTab.Tag = Helpers.Constants.PD_ViewItemRestrictions;
            NotesProspectTab.Tag = Helpers.Constants.PD_ViewNotes;
            PricingProspectTab.Tag = Helpers.Constants.PD_ViewPricing;
            QuotesProspectTab.Tag = Helpers.Constants.PD_ViewQuotes;
        }

        public override void RefreshWindow(object payload)
        {
            if (this.DataContext != null)
            {
                ((ProspectsHome)this.DataContext).IsBusy = true;
            }

            Logger.Info("[SalesLogicExpress.Views.CustomerHome][RefreshWindow][Enter in RefreshWindow]");
            LoadContext();
            CloseForwardedWindows();
            base.RefreshWindow(payload);
        }

        protected override void DisplaySettingsChanged(object sender, EventArgs e)
        {
            base.DisplaySettingsChanged(sender, e);
            if (WindowOrientation == WindowOrientation.Portrait)
            {

            }
            if (WindowOrientation == WindowOrientation.Landscape)
            {

            }
        }

        async void LoadContext()
        {
            await Task.Run(() =>
            {
                SetNavigationDefaults();
                viewModelProspectHome = new viewmodels.ProspectsHome();

                viewmodels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.DataContext = viewModelProspectHome;
                    viewModelProspectHome.MessageToken = this.Token;
                    NavigationHeader.DataContext = cminfo;

                }));
            });
        }

        #region prospects

        private void ProspectsHomeTabControl_SelectionChanged(object sender, Telerik.Windows.Controls.RadSelectionChangedEventArgs e)
        {
            try
            {
            if (ProspectsHomeTabControl.SelectedIndex == 7)
            {
                rtc_Competitor.Visibility = System.Windows.Visibility.Visible;
                rtc_Equipment.Visibility = System.Windows.Visibility.Collapsed;

                SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Prospects Home";
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Competitors";
                Count = 0;
            }
            else if (ProspectsHomeTabControl.SelectedIndex == 8)
            {
                rtc_Competitor.Visibility = System.Windows.Visibility.Collapsed;
                rtc_Equipment.Visibility = System.Windows.Visibility.Visible;

                SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Prospects Home";
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Equipment";
                Count = 0;
            }
            else if (ProspectsHomeTabControl.SelectedIndex == 0)
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Dashboard";
            }
            else if (ProspectsHomeTabControl.SelectedIndex == 1)
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Activity";
            }
            else if (ProspectsHomeTabControl.SelectedIndex == 2)
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Information";
            }
            else if (ProspectsHomeTabControl.SelectedIndex == 3)
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Notes";
            }
            else if (ProspectsHomeTabControl.SelectedIndex == 4)
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Contacts";
            }
            else if (ProspectsHomeTabControl.SelectedIndex == 5)
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Pricing";
            }
            else if (ProspectsHomeTabControl.SelectedIndex == 6)
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Quotes";
            }
            else if (ProspectsHomeTabControl.SelectedIndex != 2 || viewModel.HasPendingCountRequests)
            {
                rtc_Competitor.Visibility = System.Windows.Visibility.Collapsed;
                rtc_Equipment.Visibility = System.Windows.Visibility.Collapsed;
            }

            else
            {
                rtc_Competitor.Visibility = System.Windows.Visibility.Collapsed;
                rtc_Equipment.Visibility = System.Windows.Visibility.Collapsed;
            }
             }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Presentation.Views][ProspectHome][EquipmCheckBox_Click][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }

        private void CreateStopText_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void EquipmCheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
            CheckBox checkbox = (sender as CheckBox);
            bool flag = checkbox.IsChecked.Value;
            if (flag == true)
            {
                Count += 1;
            }
            if (Count > 1)
            {
                ButtonDeleteProspect.IsEnabled = true;
                ButtonEditProspect.IsEnabled = false;
                ButtonAddProspect.IsEnabled = false;
            }
            else
            {
                ButtonDeleteProspect.IsEnabled = true;
                ButtonEditProspect.IsEnabled = true;
                ButtonAddProspect.IsEnabled = false;
            }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Presentation.Views][ProspectHome][EquipmCheckBox_Click][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }

        private void EquipmentCheck_Unchecked(object sender, RoutedEventArgs e)
        {
            try{
            CheckBox checkbox = (sender as CheckBox);
            bool flag = checkbox.IsChecked.Value;
            if (flag == false)
            {
                Count -= 1;
            }
            if (Count > 1)
            {
                ButtonDeleteProspect.IsEnabled = false;
                ButtonEditProspect.IsEnabled = false;
                ButtonAddProspect.IsEnabled = true;
            }
            else if (Count == 0)
            {
                ButtonEditProspect.IsEnabled = false;
                ButtonDeleteProspect.IsEnabled = false;
                ButtonAddProspect.IsEnabled = true;
            }
            else
            {
                ButtonEditProspect.IsEnabled = true;
                ButtonDeleteProspect.IsEnabled = true;
                ButtonAddProspect.IsEnabled = false;
            }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Presentation.Views][ProspectHome][ExpCheckBox_Checked][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }

        private void ExpCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
            CheckBox checkbox = (sender as CheckBox);
            bool flag = checkbox.IsChecked.Value;
            if (flag == true)
            {
                Count += 1;
            }
            if (Count > 1)
            {
                ButtonDeleteExpensed.IsEnabled = true;
                ButtonEditExpensed.IsEnabled = false;
                ButtonAddExpensed.IsEnabled = false;
            }
            else
            {
                ButtonDeleteExpensed.IsEnabled = true;
                ButtonEditExpensed.IsEnabled = true;
                ButtonAddExpensed.IsEnabled = false;
            }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Presentation.Views][ProspectHome][ExpCheckBox_Checked][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }

        private void ExpCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
            CheckBox checkbox = (sender as CheckBox);
            bool flag = checkbox.IsChecked.Value;
            if (flag == false)
            {
                Count -= 1;
            }
            if (Count > 1)
            {
                ButtonDeleteExpensed.IsEnabled = false;
                ButtonEditExpensed.IsEnabled = false;
                ButtonAddExpensed.IsEnabled = true;
            }
            else if (Count == 0)
            {
                ButtonEditExpensed.IsEnabled = false;
                ButtonDeleteExpensed.IsEnabled = false;
                ButtonAddExpensed.IsEnabled = true;
            }
            else
            {
                ButtonEditExpensed.IsEnabled = true;
                ButtonDeleteExpensed.IsEnabled = true;
                ButtonAddExpensed.IsEnabled = false;
            }
             }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Presentation.Views][ProspectHome][ExpCheckBox_Unchecked][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }

        int CoffeeCount = 0;
        private void CoffeeCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
            CheckBox checkbox = (sender as CheckBox);
            bool flag = checkbox.IsChecked.Value;
            if (flag == true)
            {
                CoffeeCount += 1;
            }
            if (CoffeeCount > 1)
            {
                ButtonDeleteCoffeeProspect.IsEnabled = true;
                ButtonEditCoffeeProspect.IsEnabled = false;
                ButtonAddCoffeeProspect.IsEnabled = false;
            }
            else if (CoffeeCount == 1)
            {
                ButtonDeleteCoffeeProspect.IsEnabled = true;
                ButtonEditCoffeeProspect.IsEnabled = true;
                ButtonAddCoffeeProspect.IsEnabled = false;
            }
             }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Presentation.Views][ProspectHome][CoffeeCheckBox_Checked][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }

        private void CoffeeCheck_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckBox checkbox = (sender as CheckBox);
                bool flag = checkbox.IsChecked.Value;
                if (flag == false)
                {
                    CoffeeCount -= 1;
                }
                if (CoffeeCount > 1)
                {
                    ButtonAddCoffeeProspect.IsEnabled = false;
                    ButtonEditCoffeeProspect.IsEnabled = false;
                    ButtonDeleteCoffeeProspect.IsEnabled = true;
                }
                else if (CoffeeCount == 0)
                {
                    ButtonDeleteCoffeeProspect.IsEnabled = false;
                    ButtonEditCoffeeProspect.IsEnabled = false;
                    ButtonAddCoffeeProspect.IsEnabled = true;
                }
                else if (CoffeeCount == 1)
                {
                    ButtonEditCoffeeProspect.IsEnabled = true;
                    ButtonDeleteCoffeeProspect.IsEnabled = true;
                    ButtonAddCoffeeProspect.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Presentation.Views][ProspectHome][CoffeeCheck_Unchecked][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }

        int AlliedCount = 0;
        private void AlliedCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox checkbox = (sender as CheckBox);
            bool flag = checkbox.IsChecked.Value;
            if (flag == true)
            {
                AlliedCount += 1;
            }
            if (AlliedCount > 1)
            {
                ButtonDeleteAllied.IsEnabled = true;
                ButtonEditAllied.IsEnabled = false;
                ButtonAddAllied.IsEnabled = false;
            }
            else if (AlliedCount == 1)
            {
                ButtonDeleteAllied.IsEnabled = true;
                ButtonEditAllied.IsEnabled = true;
                ButtonAddAllied.IsEnabled = false;
            }
        }

        private void AlliedCheck_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
            CheckBox checkbox = (sender as CheckBox);
            bool flag = checkbox.IsChecked.Value;
            if (flag == false)
            {
                AlliedCount -= 1;
            }
            if (AlliedCount > 1)
            {
                ButtonDeleteAllied.IsEnabled = true;
                ButtonEditAllied.IsEnabled = false;
                ButtonAddAllied.IsEnabled = false;
            }
            else if (AlliedCount == 0)
            {
                ButtonDeleteAllied.IsEnabled = false;
                ButtonEditAllied.IsEnabled = false;
                ButtonAddAllied.IsEnabled = true;
            }
            else if (AlliedCount == 1)
            {
                ButtonDeleteAllied.IsEnabled = true;
                ButtonEditAllied.IsEnabled = true;
                ButtonAddAllied.IsEnabled = false;
            }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Presentation.Views][ProspectHome][AlliedCheck_Unchecked][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }

        protected override void dialogWindow_PreviewClosed(object sender, WindowPreviewClosedEventArgs e)
        {
            try
            {
                if (viewModelProspectHome.IsDirty)
                {
                    DialogParameters parameters = new DialogParameters();
                    parameters.Content = "Do you really want to close?";
                    bool? dialogResult = null;
                    parameters.Closed = (confirmDialog, eventArgs) => { dialogResult = eventArgs.DialogResult; };
                    parameters.OkButtonContent = "OK";
                    parameters.CancelButtonContent = "Cancel";
                    parameters.WindowStyle = (Style)(this.Resources["RadWindowStyle"]);
                    RadWindow.Confirm(parameters);

                    if (dialogResult == null)
                    {
                        e.Cancel = true;
                        return;
                    }
                    e.Cancel = !(bool)dialogResult.Value;
                    viewModelProspectHome.IsDirty = !(bool)dialogResult.Value;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Presentation.Views][ProspectHome][dialogWindow_PreviewClosed][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }

        private void OnTextBoxKeyDown(object sender, KeyEventArgs e)
        {
            //if (e.Key == Key.Return)
            //{
            //    TraversalRequest request = new TraversalRequest(FocusNavigationDirection.Next);
            //    MoveFocus(request);
            //}
        }

        private void txt_ZipCode_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("^[0-9]([-][0-9]{1,3})?$");
            e.Handled = !regex.IsMatch(e.Text);
        }

        private void txt_ZipCode_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        #endregion

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
