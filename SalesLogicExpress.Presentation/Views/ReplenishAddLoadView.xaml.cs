﻿using log4net;
using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Application.Helpers;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for ReplenishAddLoadView.xaml
    /// </summary>
    public partial class ReplenishAddLoadView : BaseWindow,IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Views.ReplenishAddLoadViewModel");
        SalesLogicExpress.Application.ViewModels.ReplenishAddLoadViewModel ReplenishAddLoadVM;
        public ReplenishAddLoadView()
        {
            InitializeComponent();
            ScreenID = Helpers.Constants.Title_AddLoadViewTitle;
            ScreenLifeCycle = Helpers.Constants.ApplicationLifeCycle;
            LoadContext();
            this.Activated += ReplenishAddLoadView_Activated;
            this.Loaded += ReplenishAddLoadView_Loaded;
        }

        void ReplenishAddLoadView_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("ReplenishAddLoadView_Loaded, " + DateTime.Now.Millisecond);
            SetNavigationDefaults();
        }

        void ReplenishAddLoadView_Activated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }
        async void LoadContext()
        {
            await Task.Run(() =>
            {
                try
                {

                    ReplenishAddLoadVM = new Application.ViewModels.ReplenishAddLoadViewModel();
                    SetNavigationDefaults();
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        ReplenishAddLoadVM.MessageToken = this.Token;
                        this.DataContext = ReplenishAddLoadVM;
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo cminfo = SalesLogicExpress.Application.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                        cminfo.ParentViewModel = ReplenishAddLoadVM;
                        NavigationHeader.DataContext = cminfo;
                        ReplenishAddLoadVM.IsBusy = false;
                    }));


                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Views.ReplenishAddLoadView][AddLoad][LoadContext][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }

            });

        }
        public override void RefreshWindow(object payload)
        {
            if (this.DataContext != null)
            {
                ((SalesLogicExpress.Application.ViewModels.ReplenishAddLoadViewModel)this.DataContext).IsBusy = true;
            }
            LoadContext();
            SetNavigationDefaults();
            base.RefreshWindow(payload);
        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            //SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = PayloadManager.RouteReplenishmentPayload.IsAddLoadView ? "Add Load" : "Suggestion";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishAddLoadView.ToString() ?
                ViewModelMappings.View.ReplenishAddLoadView.GetEnumDescription() : (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishSggestionView.ToString()) ? 
                ViewModelMappings.View.ReplenishSggestionView.GetEnumDescription() : (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnView.ToString()) ? 
                ViewModelMappings.View.SuggestionReturnView.GetEnumDescription() :(PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishUnloadView.ToString()) ?
                ViewModelMappings.View.ReplenishUnloadView.GetEnumDescription() : ViewModelMappings.View.HeldReturnView.GetEnumDescription();

            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Route Home";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteHome;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ReplenishAddLoadView;
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
