﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using ViewModels = SalesLogicExpress.Application.ViewModels;



namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for ReturnOrderPickScreen.xaml
    /// </summary>
    /// 
    public partial class ReturnOrderPickScreen : BaseWindow,IDisposable
    {
        ViewModels.CustomerReturnsPickViewModel viewModel;
        public ReturnOrderPickScreen()
        {
            InitializeComponent();
            this.DefaultFocusElement = ManualPickTextBox;
            LoadContext();
        }

        private void RadButton_Click(object sender, RoutedEventArgs e)
        {
            KeyPadPanel.Visibility = KeyPadPanel.Visibility == System.Windows.Visibility.Visible ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
        }

        private void Btn_PickOrder_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Item Returns Pick";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Item Return";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.ItemReturnsScreen;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ReturnOrderPickScreen;
        }

        async void LoadContext()
        {
            await Task.Run(() =>
            {
                 try{
                SetNavigationDefaults();
                viewModel = new ViewModels.CustomerReturnsPickViewModel();
                viewModel.ModelChanged += viewModel_ModelChanged;
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    viewModel.MessageToken = this.Token;
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo cminfo = SalesLogicExpress.Application.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                    cminfo.ParentViewModel = viewModel;
                    NavigationHeader.DataContext = cminfo;
                    this.DataContext = viewModel;
                    this.AlwaysShowKeyboard = true;

                    base.AlwaysShowNumericKeyboard();
                }));
                 }
                 catch (Exception ex)
                 {
                     Logger.Error("[SalesLogicExpress.Presentation.Views][ReturnOrderPickScreen][LoadContext][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                 }
            });
        }

        private void viewModel_ModelChanged(object sender, ViewModels.BaseViewModel.ModelChangeArgs e)
        {
            //throw new NotImplementedException();
        }

        private void RadNumericUpDown_ValueChanged(object sender, Telerik.Windows.Controls.RadRangeBaseValueChangedEventArgs e)
        {
            try{
            var dc = (SalesLogicExpress.Application.ViewModels.CustomerReturnsPickViewModel)this.DataContext;
            if (dc != null)
            {
                if (e.OldValue != null)
                {
                    if ((e.NewValue < e.OldValue))
                    {
                        PickOrderGrid.SelectedItem = (((FrameworkElement)sender)).DataContext;
                        if (dc.OldExceptionCount == Convert.ToInt32(e.OldValue) && dc.NewExceptionCount == Convert.ToInt32(e.NewValue) && (dc.ChkSamePickItem == dc.SelectedReturnItem.ItemNumber && dc.ChkSamePickOrderId == dc.SelectedReturnItem.TransactionDetailID))
                            return;
                        dc.OldExceptionCount = Convert.ToInt32(e.OldValue.HasValue ? e.OldValue : 0);
                        dc.NewExceptionCount = Convert.ToInt32(e.NewValue.HasValue ? e.NewValue : 0);
                        if (!dc.IsScannerOn && (((FrameworkElement)sender)).DataContext != null && (dc.PickItemsException.Count > 0))
                        {

                            double OldValue = 0, NewValue = 0, CurrentValue = 0;
                            if (e.OldValue != null)
                                OldValue = (double)e.OldValue;
                            if (e.NewValue != null)
                                NewValue = (double)e.NewValue;
                            CurrentValue = NewValue - OldValue;

                            string strRmvSign = Convert.ToString(CurrentValue);
                            strRmvSign = strRmvSign.Remove(0, 1);
                            CurrentValue = Convert.ToDouble(strRmvSign);

                            dc.InventoryExecuteVal = CurrentValue;
                            dc.ChkSamePickItem = dc.SelectedReturnItem.ItemNumber;
                            dc.ChkSamePickOrderId = dc.SelectedReturnItem.TransactionDetailID;
                            dc.ScanByDeviceExceptionCommand.Execute((((FrameworkElement)sender)).DataContext);

                        }
                    }
                }
            }
             }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Presentation.Views][ReturnOrderPickScreen][RadNumericUpDown_ValueChanged][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

        }


        private void RadNumericUpDown1_ValueChanged(object sender, Telerik.Windows.Controls.RadRangeBaseValueChangedEventArgs e)
        {
            try
            {
                var dc = (SalesLogicExpress.Application.ViewModels.CustomerReturnsPickViewModel)this.DataContext;
                if (e.OldValue != null)
                {
                    if (dc != null)
                    {
                        if (!dc.IsScannerOn && (((FrameworkElement)sender)).DataContext != null && (dc.PickItemsException.Count <= 0 || dc.PickItemsException == null))
                        {
                            if (!SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsNavigateFromReturnPickScr)
                            {
                                PickOrderGrid.SelectedItem = (((FrameworkElement)sender)).DataContext;
                                //dc.SelectedReturnItem = null;
                                dc.ManuallyPickCountOfSelected = Convert.ToInt32(e.NewValue);
                                dc.SelectedReturnItem.IsAllowManualPick = true;
                                dc.IsScannerOn = true;
                                double OldValue = 0, NewValue = 0, CurrentValue = 0;
                                if (e.OldValue != null)
                                    OldValue = (double)e.OldValue;
                                if (e.NewValue != null)
                                    NewValue = (double)e.NewValue;
                                CurrentValue = NewValue - OldValue;
                                char NumericSign = (CurrentValue.ToString()[0]);
                                if (NumericSign != '-')
                                    NumericSign = '+';
                                else if (NumericSign == '-')
                                {
                                    string strRmvSign = Convert.ToString(CurrentValue);
                                    strRmvSign = strRmvSign.Remove(0, 1);
                                    CurrentValue = Convert.ToDouble(strRmvSign);
                                }
                                dc.PickNumericSign = NumericSign;
                                dc.PickNumericCurrVal = CurrentValue;
                                dc.IsScannerOn = false;

                                dc.UpdateOnManualCountCommand.Execute((((FrameworkElement)sender)).DataContext);

                                dc.SelectedReturnItem.IsAllowManualPick = false;
                                dc.IsScannerOn = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Presentation.Views][ReturnOrderPickScreen][RadNumericUpDown1_ValueChanged][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
             
        }

        private void RCB_ManualPickReasonList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }


        private void txt_OrderQty_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !(new Regex(@"^\d{0,4}(\d{0,2})?$").IsMatch(e.Text));
        }
        private void txt_OrderQty_GotFocus(object sender, RoutedEventArgs e)
        {
            (sender as TextBox).SelectAll();
        }
        private void txt_OrderQty_KeyDown(object sender, KeyEventArgs e)
        {

        }


        private void btn_RemoveExceptions_Click(object sender, RoutedEventArgs e)
        {

        }

        private void PickOrderGrid_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangeEventArgs e)
        {

        }

        private void ExceptionGrid_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangeEventArgs e)
        {
            ManualPickTextBox.Focus();
        }

        private void RadButton_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
