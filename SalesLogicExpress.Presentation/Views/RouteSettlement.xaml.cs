﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using SalesLogicExpress.Presentation.Helpers;
using ViewModelApplication = SalesLogicExpress.Application;
using viewmodels = SalesLogicExpress.Application.ViewModels;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for RouteSettlement.xaml
    /// </summary>
    public partial class RouteSettlement : BaseWindow, IDisposable
    {
        private viewmodels.RouteSettlementViewModel viewModel;

        private bool disposed = false;
        private System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);

        public RouteSettlement()
        {
            this.InitializeComponent();
            this.DefaultSeletedTab = "TabItem_RouteSettlement_Transactions";
            this.InitView();
            this.LoadContext();
        }

        public override void RefreshWindow(object payload)
        {
            //if (this.DataContext != null)
            //{
            //    ((viewmodels.RouteSettlementViewModel)this.DataContext).IsBusy = true;
            //}
            System.Diagnostics.Debug.WriteLine(string.Format("RouteSettlement.xaml.cs > RefreshWindow --> payload = {0}", payload));
            this.LoadContext();
            base.RefreshWindow(payload);

            if (viewmodels.CommonNavInfo.IsManuallySyncAllProfiles == true)
            {
                ViewModelApplication.Helpers.ResourceManager.CommonNavInfo.StartSync.Execute(null);
                viewmodels.CommonNavInfo.IsManuallySyncAllProfiles = false;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            if (disposing)
            {
                this.handle.Dispose();
            }
            this.disposed = true;
        }

        private void InitView()
        {
            this.Activated += this.RouteSettlement_Activated;
            this.Loaded += this.RouteSettlement_Loaded;
        }

        private void RouteSettlement_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(string.Format("RouteSettlement_Loaded, {0}", DateTime.Now.Millisecond));
            this.SetNavigationDefaults();
        }

        private void RouteSettlement_Activated(object sender, EventArgs e)
        {
            this.SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }

        //public void LoadContext()
        //{
        //    if (viewModel == null)
        //    {
        //        viewModel = new viewmodels.RouteSettlementViewModel();
        //        viewmodels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
        //        cminfo.ParentViewModel = viewModel;
        //        this.DataContext = viewModel;
        //        viewModel.MessageToken = this.Token;
        //        NavigationHeader.DataContext = cminfo;

        //    }
        //}
        private async void LoadContext()
        {
            await Task.Run(() =>
            {
                this.Logger.Info("[SalesLogicExpress.Presentation.Views][RouteSettlement][Start:LoadContext]");
                try
                {
                    this.viewModel = new viewmodels.RouteSettlementViewModel();
                    //viewmodels.ServiceRoute.SelectedCalendarDate = viewModel.DailyStopDate;
                    viewmodels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                    cminfo.ParentViewModel = this.viewModel;
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.DataContext = this.viewModel;
                        this.viewModel.MessageToken = this.Token;
                        if (this.viewModel.RouteSettlemenntTransationsVM != null)
                        {
                            this.viewModel.RouteSettlemenntTransationsVM.MessageToken = this.Token;
                        }
                        if (this.viewModel.RouteSettlementSettlementsVM != null)
                        {
                            this.viewModel.RouteSettlementSettlementsVM.MessageToken = this.Token;
                        }

                        this.NavigationHeader.DataContext = cminfo;
                        //viewModel.IsBusy = false;
                    }));
                }
                catch (Exception ex)
                {
                    this.Logger.Error(string.Format("[SalesLogicExpress.Presentation.Views][RouteSettlement][LoadContext][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
                }
                this.Logger.Info("[SalesLogicExpress.Presentation.Views][RouteSettlement][End:LoadContext]");
            });
        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Route Home";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Route Settlement";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteHome;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.RouteSettlement;
        }
    }
}
