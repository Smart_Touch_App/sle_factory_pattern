﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using SalesLogicExpress.Presentation.Helpers;
using ViewModelApplication = SalesLogicExpress.Application;
using viewmodels = SalesLogicExpress.Application.ViewModels;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for MoneyOrderWindow.xaml
    /// </summary>
    public partial class MoneyOrderWindow : BaseWindow, IDisposable
    {
        private viewmodels.MoneyOrderViewModel viewModel;
        private bool disposed = false;
        private System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);

        public MoneyOrderWindow()
        {
            this.InitView();
            this.InitializeComponent();
            this.LoadContext();
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            if (disposing)
            {
                this.handle.Dispose();

                if (viewModel != null)
                    viewModel.Dispose();

            }
            this.disposed = true;
        }

        private void InitView()
        {
            this.Activated += this.MoneyOrder_Activated;
            this.Loaded += this.MoneyOrder_Loaded;
        }

        private void MoneyOrder_Loaded(object sender, RoutedEventArgs e)
        {
            // System.Diagnostics.Debug.WriteLine("PreTripInspection_Loaded, " + DateTime.Now.Millisecond);
            this.AlwaysShowKeyboard = true;
        }

        private void MoneyOrder_Activated(object sender, EventArgs e)
        {
            //SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }

        private async void LoadContext()
        {
            await Task.Run(() =>
            {
                if (true)
                {
                    this.viewModel = new viewmodels.MoneyOrderViewModel();
                    this.SetNavigationDefaults();

                    //viewmodels.ServiceRoute.SelectedCalendarDate = viewModel.DailyStopDate;
                    viewmodels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                    cminfo.ParentViewModel = this.viewModel;
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.DataContext = this.viewModel;
                        this.viewModel.MessageToken = this.Token;
                        this.NavigationHeader.DataContext = cminfo;
                        //NavigationHeader.DataContext = cminfo;
                    }));
                }
            });
        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Route Settlement";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Money Orders";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteSettlement;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.MoneyOrderWindow;
        }
    }
}
