﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Threading;
using ViewModelApplication = SalesLogicExpress.Application;
using viewmodels = SalesLogicExpress.Application.ViewModels;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for CycleCount.xaml
    /// </summary>
    public partial class CycleCount : BaseWindow,IDisposable
    {
        viewmodels.CycleCountViewModel viewModel;
        
        public CycleCount()
        {
            InitView();
            InitializeComponent();
           // LoadContext();
        }

        public CycleCount(Object Payload)
        {
            InitView();
            InitializeComponent();
            LoadContext(Payload);
        }

        private void InitView()
        {
            this.Activated += CycleCount_Activated;
            this.Loaded += CycleCount_Loaded;
        }

        void CycleCount_Loaded(object sender, RoutedEventArgs e)
        {
           // System.Diagnostics.Debug.WriteLine("PreTripInspection_Loaded, " + DateTime.Now.Millisecond);
            this.AlwaysShowKeyboard = true;
        }

        void CycleCount_Activated(object sender, EventArgs e)
        {
            //SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);

        }

        async void LoadContext(Object Payload)
        {
            
            await Task.Run(() =>
            {
                if (true)
                {
                    SalesLogicExpress.Domain.CycleCount CCObject = (SalesLogicExpress.Domain.CycleCount)Payload;
                   viewModel = new viewmodels.CycleCountViewModel(this.Token, false, CCObject);
                    SetNavigationDefaults();


                    //viewmodels.ServiceRoute.SelectedCalendarDate = viewModel.DailyStopDate;
                    viewmodels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                    cminfo.ParentViewModel = viewModel;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.DataContext = viewModel;
                        viewModel.MessageToken = this.Token;
                        NavigationHeader.DataContext = cminfo;

                        //NavigationHeader.DataContext = cminfo;
                    }));
                }
            });
        }

        private void RadNumericUpDown_ValueChanged(object sender, Telerik.Windows.Controls.RadRangeBaseValueChangedEventArgs e)
        {
            var dc = (SalesLogicExpress.Application.ViewModels.CycleCountViewModel)this.DataContext;

            if (dc != null)
            {
                if (!dc.IsScannerOn && (((FrameworkElement)sender)).DataContext != null)
                {
                    CycleCountDetails.SelectedItem = (((FrameworkElement)sender)).DataContext;
                    //dc.SelectedReturnItem = null;
                    dc.ManualOrderQty = Convert.ToInt32(e.NewValue);
                    //dc.SelectedReturnItem.IsAllowManualPick = true;
                    dc.UpdateOnManualCount.Execute((((FrameworkElement)sender)).DataContext);
                    //dc.SelectedReturnItem.IsAllowManualPick = false;
                }
            }
           
        }

        void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Route Home";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Cycle Count Items";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteHome;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.CycleCount;        
        }

        /// <summary>
        /// code added in view model to handle below functionality
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KeyPadToggleButton_Click(object sender, RoutedEventArgs e)
        {
            KeyPadPanel.Visibility = KeyPadPanel.Visibility == System.Windows.Visibility.Visible ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
           // KeyboardPanel.Visibility = KeyboardPanel.Visibility == System.Windows.Visibility.Visible ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
        }

        private void txt_OrderQty_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !(new Regex(@"^\d{0,4}(\d{0,2})?$").IsMatch(e.Text));
        }
        private void txt_OrderQty_GotFocus(object sender, RoutedEventArgs e)
        {
            (sender as TextBox).SelectAll();
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
