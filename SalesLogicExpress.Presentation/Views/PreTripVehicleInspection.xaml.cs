﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Threading;
using ViewModelApplication = SalesLogicExpress.Application;
using viewmodels = SalesLogicExpress.Application.ViewModels;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for PreTripVehicleInspection.xaml
    /// </summary>
    public partial class PreTripVehicleInspection : BaseWindow,IDisposable
    {
        viewmodels.PreTripInspectionViewModel viewModel;
        public PreTripVehicleInspection() : this(null)
        {
            InitializeComponent();
            InitView();
            // LoadContext();

        }
        public PreTripVehicleInspection(Object Payload)
        {
            InitializeComponent();
            InitView();
            LoadContext(Payload);

        }
        private void InitView()
        {
            this.Activated += PreTripInspection_Activated;
            this.Loaded += PreTripInspection_Loaded;
            this.TestInkCanvasDummy.Loaded += TestInkCanvasDummy_Loaded;
            this.txtAudometer.LostFocus += txtAudometer_LostFocus;
            this.txtAudometer.GotFocus += txtAudometer_GotFocus;

            
        }

        void txtAudometer_GotFocus(object sender, RoutedEventArgs e)
        {
            if (viewModel != null)
            {
                viewModel.IsSwitchDisable = true;
            }
        }

        void txtAudometer_LostFocus(object sender, RoutedEventArgs e)
        {
            if (viewModel != null)
            {
                viewModel.IsSwitchDisable = false;
            }
        }

        void TestInkCanvasDummy_Loaded(object sender, RoutedEventArgs e)
        {
            InkCanvas A= this.TestInkCanvasDummy;
            SalesLogicExpress.Application.ViewModelPayload.PayloadManager.RouteSettlementPayload.MyCanvasPayload = A;
            //this.TestInkCanvasDummy.Visibility = Visibility.Collapsed;

        }

       
        void TestInkCanvasDummy_StrokeCollected(object sender, InkCanvasStrokeCollectedEventArgs e)
        {
            if (this.TestInkCanvasDummy.Strokes.Count > 0)
            {
                //viewModel.MyStrokeCollection = new System.Windows.Ink.StrokeCollection();
                //viewModel.MyStrokeCollection = this.TestInkCanvas.Strokes;
                viewModel.MyCanvas = new InkCanvas();
                viewModel.MyCanvas = this.TestInkCanvasDummy;
            }
        }
       
        void PreTripInspection_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("PreTripInspection_Loaded, " + DateTime.Now.Millisecond);
        }

        void PreTripInspection_Activated(object sender, EventArgs e)
        {
            //SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e); ;
        }
        async void LoadContext(Object Payload)
        {
            await Task.Run(() =>
            {
                if (true)
                {
                    Domain.PreTripVehicleInspection inspectionModel = (Domain.PreTripVehicleInspection)Payload;
                    
                    viewModel = new viewmodels.PreTripInspectionViewModel(this.Token, inspectionModel);
                    SetNavigationDefaults();
                
                    //viewmodels.ServiceRoute.SelectedCalendarDate = viewModel.DailyStopDate;
                    viewmodels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                    cminfo.ParentViewModel = viewModel;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.DataContext = viewModel;
                        viewModel.MessageToken = this.Token;
                        NavigationHeader.DataContext = cminfo;


                        //NavigationHeader.DataContext = cminfo;
                    }));
                }
            });
        }
     
        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Route Home";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Pre Trip Vehicle Inspection";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteHome;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.PreTripVehicleInspection;
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
           // if (viewModel != null && viewModel.PreTripInspectionVM != null)
                //viewModel.PreTripInspectionVM.PropertyLostFocus.Execute(sender);
            TextBox text = sender as TextBox;
            BindingOperations.GetBindingExpression(text, TextBox.TextProperty).UpdateSource();
        }

        private void btnClearCanvas_Click(object sender, RoutedEventArgs e)
        {

        }

        private void txt_ZipCode_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (viewModel!=null)
            {
                viewModel.IsSwitchDisable = true;
            }
            Regex regex = new Regex("^[0-9]([-][0-9]{1,3})?$");
            e.Handled = !regex.IsMatch(e.Text);
        }

        //private void btnClearCanvas_Click(object sender, RoutedEventArgs e)
        //{
        //    if (this.TestInkCanvas.Strokes.Count > 0)
        //    {
        //        this.TestInkCanvas.Strokes.Clear();
        //    }
        //}

        protected override void dialogWindow_PreviewClosed(object sender, WindowPreviewClosedEventArgs e)
        {
            if (viewModel!=null)
            {
                viewmodels.PreTripInspectionViewModel.IsCanvasEnable = false;
            }

        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

    }

}
