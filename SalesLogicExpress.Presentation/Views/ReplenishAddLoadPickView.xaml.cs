﻿using log4net;
using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using SalesLogicExpress.Application;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Application.Helpers;


namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for ReplenishAddLoadPickView.xaml
    /// </summary>
    public partial class ReplenishAddLoadPickView : BaseWindow,IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Views.ReplenishAddLoadPickView");
        SalesLogicExpress.Application.ViewModels.ReplenishAddLoadPickViewModel ReplenishAddLoadPickVM;
        public ReplenishAddLoadPickView()
        {
            InitializeComponent();
            ScreenID = Helpers.Constants.Title_AddLoadViewTitle;
            ScreenLifeCycle = Helpers.Constants.ApplicationLifeCycle;
            LoadContext();
            this.Activated += ReplenishAddLoadPickView_Activated;
            this.Loaded += ReplenishAddLoadPickView_Loaded;
            this.AlwaysShowKeyboard = true;
            this.DefaultFocusElement = ManualPickTextBox;
        }

        void ReplenishAddLoadPickView_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("ReplenishAddLoadPickView_Loaded, " + DateTime.Now.Millisecond);
            SetNavigationDefaults();
        }

        void ReplenishAddLoadPickView_Activated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }


        async void LoadContext()
        {
            await Task.Run(() =>
            {
                try
                {

                    ReplenishAddLoadPickVM = new Application.ViewModels.ReplenishAddLoadPickViewModel();
                    SetNavigationDefaults();
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        ReplenishAddLoadPickVM.MessageToken = this.Token;
                        this.DataContext = ReplenishAddLoadPickVM;
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo cminfo = SalesLogicExpress.Application.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                        cminfo.ParentViewModel = ReplenishAddLoadPickVM;
                        NavigationHeader.DataContext = cminfo;
                        ReplenishAddLoadPickVM.IsBusy = false;
                    }));


                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Views.ReplenishAddLoadPickView][AddLoadPick][LoadContext][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }

            });

        }
        public override void RefreshWindow(object payload)
        {
            if (this.DataContext != null)
            {
                ((SalesLogicExpress.Application.ViewModels.ReplenishAddLoadViewModel)this.DataContext).IsBusy = true;
            }
            LoadContext();
            SetNavigationDefaults();
            base.RefreshWindow(payload);
        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishAddLoadPickView.ToString() ?
                ViewModelMappings.View.ReplenishAddLoadPickView.GetEnumDescription() : (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishSuggestionPickView.ToString()) ?
                ViewModelMappings.View.ReplenishSuggestionPickView.GetEnumDescription() : (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnPickView.ToString()) ? 
                ViewModelMappings.View.SuggestionReturnPickView.GetEnumDescription() :(PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishUnloadPickView.ToString()) ?
                ViewModelMappings.View.ReplenishUnloadPickView.GetEnumDescription() : ViewModelMappings.View.HeldReturnPickView.GetEnumDescription();
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Route Home";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteHome;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ReplenishAddLoadPickView;
        }

        private void ExceptionGrid_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangeEventArgs e)
        {
            ManualPickTextBox.Focus();
        }

        private void RadNumericUpDown_ValueChanged(object sender, Telerik.Windows.Controls.RadRangeBaseValueChangedEventArgs e)
        {
             try
                {
            var dc = (SalesLogicExpress.Application.ViewModels.ReplenishAddLoadPickViewModel)this.DataContext;

            if (dc != null)
            {

                if (!dc.IsScannerOn && (((FrameworkElement)sender)).DataContext != null)
                {
                    if (e.OldValue != null)
                    {
                        PickLoadGrid.SelectedItem = (((FrameworkElement)sender)).DataContext;
                        if (dc.OldManualPickCount == Convert.ToInt32(e.OldValue) && dc.NewManualPickCount == Convert.ToInt32(e.NewValue) && (dc.ChkManualPickSameItem == dc.AddLoadPickSelectedItem.ItemNumber && dc.ChkManualPickSameOrder == dc.AddLoadPickSelectedItem.TransactionDetailID))
                            return;
                        dc.OldManualPickCount = Convert.ToInt32(e.OldValue.HasValue ? e.OldValue : 0);
                        dc.NewManualPickCount = Convert.ToInt32(e.NewValue.HasValue ? e.NewValue : 0);

                        dc.ManuallyPickCountOfSelected = Convert.ToInt32(e.NewValue);
                        dc.AddLoadPickSelectedItem.IsAllowManualPick = true;
                        dc.UpdateOnManualCountCommand.Execute((((FrameworkElement)sender)).DataContext);
                        dc.AddLoadPickSelectedItem.IsAllowManualPick = false;
                        dc.ChkManualPickSameItem = dc.AddLoadPickSelectedItem.ItemNumber;
                        dc.ChkManualPickSameOrder = dc.AddLoadPickSelectedItem.TransactionDetailID;
                    }
                }
            }
                }
             catch (Exception ex)
             {
                 Logger.Error("[SalesLogicExpress.Views.ReplenishAddLoadPickView][AddLoadPick][RadNumericUpDown_ValueChanged][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
             }
           
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
