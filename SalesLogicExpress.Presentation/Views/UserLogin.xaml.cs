﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ViewModels = SalesLogicExpress.Application.ViewModels;
using ApplicationHelpers = SalesLogicExpress.Application.Helpers;
using System.Configuration;
using EXT = SalesLogicExpress.Extensions;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for UserLogin.xaml
    /// </summary>
    public partial class UserLogin : BaseWindow,IDisposable
    {

        #region Properties
        public static string EnvironmentConfigured { get; set; }
        public static string RouteConfigured { get; set; }
        #endregion

        #region Constructor
        public UserLogin()
        {
            InitializeComponent();
            ScreenID = Helpers.Constants.Title_UserLoginViewTitle;
            ScreenLifeCycle = Helpers.Constants.ApplicationLifeCycle;

            ViewModels.UserLogin UserInfo = new ViewModels.UserLogin();
            UserInfo.MessageToken = this.Token;

            //Please do not change this as other settings start giving problem of escape character - Arvind
            UserInfo.DomainUser =  string.Concat(Environment.UserDomainName, @"\\", Environment.UserName);

            UserInfo.DeviceID = Application.Helpers.ResourceManager.DeviceID;
            UserInfo.DomainUser = UserInfo.DomainUser;
            UserInfo.Password = "*****";

            this.DataContext = UserInfo;



            UserInfo.Route = RouteConfigured;

            //Setting RemoteDB info
            if (!string.IsNullOrWhiteSpace(EXT.SLEApplication.SLEPath)) //App Location found
            {
                if (ShouldAutoLogin())
                {
                    UserInfo.LoginID = UserInfo.DomainUser;
                    UserInfo.DomainUser = UserInfo.LoginID;
                    // UserInfo.AppUser = 

                    //Navigate to route home page
                    UserInfo.LoginUser.Execute(UserInfo);
                    System.Threading.Thread.Sleep(1000);
                    return;
                }
            }


            UserInfo.LoginUser.Execute(UserInfo);
  
            //Commented By Arvind For Relay
            ////string id = DeviceManager.Device.GenerateDeviceID();
            //string mobilinkServer = ConfigurationManager.AppSettings["MoblinkServerIP"].ToString();
            //string mobilinkServerPort = ConfigurationManager.AppSettings["MoblinkServerPort"].ToString();
            //SalesLogicExpress.Application.Managers.SetupManager man = new Application.Managers.SetupManager();
            //man.ConfigureRemoteDbEndPoint(mobilinkServer, mobilinkServerPort);

            //#region Auto login based on configuration
            //AppInfo InstalledSLEInfo = InstalledApplictionInfo(Convert.ToString(ConfigurationManager.AppSettings["BootstrapAppName"]));//"Sales Logic Launcher"
            //if (!string.IsNullOrWhiteSpace(InstalledSLEInfo.AppLocation))
            //{
            //    string routeXMLFile = string.Concat(InstalledSLEInfo.AppLocation, @"Resources\route.xml");
            //    PopulateOnboardRoute(routeXMLFile);
            //    viewModel.Route = RouteConfigured;
            //    SalesLogicExpress.Application.Helpers.ResourceManager.CloseConnectionInstance();
            //    SalesLogicExpress.Application.Helpers.ResourceManager.ActiveDatabaseConnectionString = string.Format(new SalesLogicExpress.Application.Managers.SetupManager().RouteDbConnectionString, viewModel.Route);
            //    viewModel.LoginID = SalesLogicExpress.Application.Managers.UserManager.GetLoginIDofDomainUser(viewModel.DomainUser);

            //    //Navigate to route home page
            //    viewModel.LoginUser.Execute(viewModel);
            //}
            //#endregion Auto login based on configuration
        }
        #endregion


        /// <summary>
        /// Checks and Logs in if Auto Login info is available in Resources\route.xml
        /// </summary>
        private bool ShouldAutoLogin()
        {
            if (!string.IsNullOrWhiteSpace(RouteConfigured)) //Route Info Found, Can Auto Login
            {
                return true;
            }
            return false;

        }

        #region Private Methods - Launcher App
        public AppInfo InstalledApplictionInfo(string p_name)
        {
            AppInfo objAppInfo = new AppInfo();
            try
            {
                string displayName, AppVersion, AppLocation;
                Microsoft.Win32.RegistryKey key;

                //// search in: CurrentUser
                //key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall");
                //foreach (String keyName in key.GetSubKeyNames())
                //{
                //    RegistryKey subkey = key.OpenSubKey(keyName);
                //    displayName = subkey.GetValue("DisplayName") as string;
                //    AppLocation = subkey.GetValue("InstallLocation") as string;
                //    AppVersion = subkey.GetValue("DisplayVersion") as string;
                //    if (p_name.Equals(displayName, StringComparison.OrdinalIgnoreCase) == true)
                //    {
                //        return new AppInfo(displayName, AppLocation, AppVersion);
                //    }
                //}

                // search in: LocalMachine_32
                key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall");
                foreach (String keyName in key.GetSubKeyNames())
                {
                    Microsoft.Win32.RegistryKey subkey = key.OpenSubKey(keyName);
                    displayName = subkey.GetValue("DisplayName") as string;
                    AppLocation = subkey.GetValue("InstallLocation") as string;
                    AppVersion = subkey.GetValue("DisplayVersion") as string;
                    if (p_name.Equals(displayName, StringComparison.OrdinalIgnoreCase) == true)
                    {
                        return new AppInfo(displayName, AppLocation, AppVersion);
                    }
                }

                // search in: LocalMachine_64
                key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall");
                foreach (String keyName in key.GetSubKeyNames())
                {
                    Microsoft.Win32.RegistryKey subkey = key.OpenSubKey(keyName);
                    displayName = subkey.GetValue("DisplayName") as string;
                    AppLocation = subkey.GetValue("InstallLocation") as string;
                    AppVersion = subkey.GetValue("DisplayVersion") as string;
                    if (p_name.Equals(displayName, StringComparison.OrdinalIgnoreCase) == true)
                    {
                        return new AppInfo(displayName, AppLocation, AppVersion);
                    }
                }
            }
            catch (Exception ex)
            {
                return objAppInfo;
            }
            // NOT FOUND
            objAppInfo = new AppInfo();
            return objAppInfo;
        }

        private void PopulateOnboardRoute(string FileName)
        {
            try
            {
                System.Xml.XmlDocument myXmlDocument = new System.Xml.XmlDocument();
                myXmlDocument.Load(FileName);
                System.Xml.XmlNode node;
                node = myXmlDocument.DocumentElement;
                if (System.IO.File.Exists(FileName))
                {
                    foreach (System.Xml.XmlNode node1 in node.ChildNodes)
                        foreach (System.Xml.XmlNode node2 in node1.ChildNodes)
                        {
                            if (node1.Name == "Environment")
                            {
                                EnvironmentConfigured = node2.Value;
                            }
                            if (node1.Name == "Route")
                            {
                                RouteConfigured = node2.Value;
                            }
                        }
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
    #region class AppInfo

    public class AppInfo
    {
        public string AppName { get; set; }
        public string AppLocation { get; set; }
        public string AppVersion { get; set; }
        public AppInfo()
        {
        }
        public AppInfo(string name, string loc, string version)
        {
            AppName = name; AppLocation = loc; AppVersion = version;
        }
    }
    #endregion
}
