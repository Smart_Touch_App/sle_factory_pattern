﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Threading;
using ViewModelApplication = SalesLogicExpress.Application;
using viewmodels = SalesLogicExpress.Application.ViewModels;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModelPayload;


namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for ReturnsAndCredit.xaml
    /// </summary>
    public partial class ReturnsAndCredit : BaseWindow,IDisposable
    {
        viewmodels.ReturnsAndCredits viewModel;
        bool Isunplanned;
        public ReturnsAndCredit(bool flag)
        {
            Isunplanned = flag;
            InitView();
            InitializeComponent();
            SetNavigationDefaults();
            LoadContext();
            //AssociatedWindows.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.ItemReturnsScreen);
            //CloseAssociatedWindows();
        }
        public ReturnsAndCredit()
        {
            InitView();
            InitializeComponent();
            SetNavigationDefaults();
            LoadContext();
            //AssociatedWindows.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.ItemReturnsScreen);
            //CloseAssociatedWindows();
        }

        private void SetNavigationDefaults()
        {

            if (PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.EOD_Transaction.ToString())
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = Application.Helpers.ViewModelMappings.View.RouteSettlement.GetEnumDescription();
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = ViewModelMappings.View.ReturnsAndCredits.GetEnumDescription();
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteSettlement;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ReturnsAndCredits;
            }
            else
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = Application.Helpers.ViewModelMappings.View.CustomerHome.GetEnumDescription();
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = ViewModelMappings.View.ReturnsAndCredits.GetEnumDescription();
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.CustomerHome;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ReturnsAndCredits;
            }    
        }

        private void InitView()
        {
            this.Activated += ReturnsAndCredits_Activated;
            this.Loaded += ReturnsAndCredits_Loaded;
        }

        void ReturnsAndCredits_Loaded(object sender, RoutedEventArgs e)
        {
            // System.Diagnostics.Debug.WriteLine("PreTripInspection_Loaded, " + DateTime.Now.Millisecond);
            //this.AlwaysShowKeyboard = true;
        }

        void ReturnsAndCredits_Activated(object sender, EventArgs e)
        {
            //SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
            if(viewModel !=null) viewModel.IsBusy = false;

        }
        async void LoadContext()
        {
            await Task.Run(() =>
            {
                if (true)
                {
                    //SalesLogicExpress.Domain.CycleCount CCObject = (SalesLogicExpress.Domain.CycleCount)Payload;
                    viewModel = new viewmodels.ReturnsAndCredits(Isunplanned);
                    SetNavigationDefaults();


                    //viewmodels.ServiceRoute.SelectedCalendarDate = viewModel.DailyStopDate;
                    viewmodels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                    cminfo.ParentViewModel = viewModel;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.DataContext = viewModel;
                        viewModel.MessageToken = this.Token;
                        NavigationHeader.DataContext = cminfo;
                    }));
                }
            });
        }

        public override void RefreshWindow(object payload)
        {
            InitView();
            InitializeComponent();
            SetNavigationDefaults();
            LoadContext();
            base.RefreshWindow(payload);
        }
        private void txt_ZipCode_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (viewModel != null)
            {
                viewModel.IsSwitchDisable = true;
            }
            Regex regex = new Regex("^[0-9]([.][0-9]{1,2})?$");
            e.Handled = !regex.IsMatch(e.Text);
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
