﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using navbar = SalesLogicExpress.Views.Common;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using viewmodels = SalesLogicExpress.Application.ViewModels;
using System.Collections.ObjectModel;
using ViewModelApplication = SalesLogicExpress.Application;
using System.Windows.Threading;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using SalesLogicExpress.Application.Helpers;
using GalaSoft.MvvmLight.Messaging;
using System.Text.RegularExpressions;
namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for RouteHome.xaml
    /// </summary>
    public partial class RouteHome : BaseWindow, IDisposable
    {
        viewmodels.RouteHome viewModel = null;
        public RouteHome()
        {
            InitializeComponent();
            DefaultSeletedTab = "TabItemDashboard";//"TabItemDashboard";
            ScreenID = Helpers.Constants.Title_RouteHomeViewTitle;
            ScreenLifeCycle = Helpers.Constants.ApplicationLifeCycle;
            this.Activated += WindowActivated;
            this.Loaded += ServiceRoute_Loaded;
            LoadContext();
            AddTagToControls();
            // PYU - User Issue ID: 10 - For CR / Added By Velmani Karnan (12/13/2016)
            PayloadManager.RouteReplenishmentPayload.IsCheckedShowZeroQuantity = false;
        }

        private void AddTagToControls()
        {
            ButtonEODSettlement.Tag = Helpers.Constants.RH_ViewEODSettlement;
            ButtonInventoryAdjustment.Tag = Helpers.Constants.ViewInventory;
            ButtonPreTripInspection.Tag = Helpers.Constants.RH_ViewPreTripInspection;
            ButtonServiceRoute.Tag = Helpers.Constants.Title_ServiceRouteViewTitle;

            TabItemAgingSummary.Tag = Helpers.Constants.RH_ViewAgingSummary;
            TabItemDashboard.Tag = Helpers.Constants.SRV_ViewCustomerDashboard;
            TabItemInventory.Tag = Helpers.Constants.RH_ViewItemInventory;
            TabItemReplenishment.Tag = Helpers.Constants.RH_ViewItemReplenishment;
            TabItemCycleCount.Tag = Helpers.Constants.RH_ViewCycleCount;
        }
        async void LoadContext()
        {
            await Task.Run(() =>
            {
                SetNavigationDefaults();
                viewModel = new viewmodels.RouteHome();
                viewmodels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.DataContext = viewModel;
                    viewModel.MessageToken = this.Token;
                    NavigationHeader.DataContext = cminfo;
                    if (viewModel.InventoryVM != null)
                        viewModel.InventoryVM.ModelChanged += InventoryVM_ModelChanged;

                }));
            });
        }

        void InventoryVM_ModelChanged(object sender, viewmodels.BaseViewModel.ModelChangeArgs e)
        {
            if (routeHomeTabControl.SelectedIndex == 2 && !viewModel.HasPendingCountRequests && !viewModel.HasPendingCountRequestsForPast)
            {
                TextBoxSearch.Visibility = System.Windows.Visibility.Visible;
                ButtonInventoryAdjustment.Visibility = System.Windows.Visibility.Visible;
            }

        }
        private void ServiceRoute_Loaded(object sender, RoutedEventArgs e)
        {
            SetNavigationDefaults();
        }

        private void WindowActivated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }
        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Route Home";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = false;

        }
        protected override void DisplaySettingsChanged(object sender, EventArgs e)
        {
            base.DisplaySettingsChanged(sender, e);
            if (WindowOrientation == WindowOrientation.Portrait)
            {

            }
            if (WindowOrientation == WindowOrientation.Landscape)
            {

            }
        }

        private void routeHomeTabControl_SelectionChanged(object sender, Telerik.Windows.Controls.RadSelectionChangedEventArgs e)
        {
            if (routeHomeTabControl.SelectedIndex != 2 || viewModel.HasPendingCountRequests)
            {
                TextBoxSearch.Visibility = System.Windows.Visibility.Collapsed;
                ButtonInventoryAdjustment.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                TextBoxSearch.Visibility = System.Windows.Visibility.Visible;
                ButtonInventoryAdjustment.Visibility = System.Windows.Visibility.Visible;
                //FilterQuantityColumn();


                //TUI Issue ID 248 (01/02/2017) --Vignesh D  
                if (btnShowZeroQty.IsChecked==false)
                {
                    FilterQuantityColumn();
                }
                else
                {
                    BtnShowZeroQty_OnChecked(sender, e);
                }
            }
        }

        private void FilterQuantityColumn()
        {
            Telerik.Windows.Controls.GridViewColumn qtyColumn = this.inventoryDetails.Columns["Quantity"];
            // Getting it from the property will create it and associate it with its column automatically.
            IColumnFilterDescriptor columnDescriptor = qtyColumn.ColumnFilterDescriptor;
            columnDescriptor.SuspendNotifications();
            columnDescriptor.FieldFilter.Filter1.Operator = FilterOperator.IsNotEqualTo;
            columnDescriptor.FieldFilter.Filter1.Value = "0";
            columnDescriptor.ResumeNotifications();
        }

        private void BtnShowZeroQty_OnChecked(object sender, RoutedEventArgs e)
        {
            Telerik.Windows.Controls.GridViewColumn qtyColumn = this.inventoryDetails.Columns["Quantity"];
            qtyColumn.ClearFilters();
            // PYU - User Issue ID: 10 - For CR / Added By Velmani Karnan (12/13/2016)
            PayloadManager.RouteReplenishmentPayload.IsCheckedShowZeroQuantity = true;
            #region Commented
            // Getting it from the property will create it and associate it with its column automatically.
            //IColumnFilterDescriptor columnDescriptor = qtyColumn.ColumnFilterDescriptor;
            //columnDescriptor.SuspendNotifications();
            //columnDescriptor.FieldFilter.Filter1.Operator = FilterOperator.IsNotEqualTo;
            //columnDescriptor.FieldFilter.Filter1.Value = "Bugatti";
            ////columnDescriptor.FieldFilter.LogicalOperator = FilterCompositionLogicalOperator.Or;
            ////columnDescriptor.FieldFilter.Filter2.Operator = FilterOperator.IsEqualTo;
            ////columnDescriptor.FieldFilter.Filter2.Value = "Volvo";
            //columnDescriptor.ResumeNotifications();

            #endregion
        }

        private void BtnShowZeroQty_OnUnchecked(object sender, RoutedEventArgs e)
        {
            FilterQuantityColumn();
            // PYU - User Issue ID: 10 - For CR / Added By Velmani Karnan (12/13/2016)
            PayloadManager.RouteReplenishmentPayload.IsCheckedShowZeroQuantity = false;
        }

        public override void RefreshWindow(object payload)
        {
            if (this.DataContext != null)
            {
                ((viewmodels.RouteHome)this.DataContext).IsBusy = true;
            }
            System.Diagnostics.Debug.WriteLine("RouteHome.xaml.cs > RefreshWindow --> payload = " + payload);
            LoadContext();
            base.RefreshWindow(payload);

            if (viewmodels.CommonNavInfo.IsManuallySyncAllProfiles == true)
            {
                ViewModelApplication.Helpers.ResourceManager.CommonNavInfo.StartSync.Execute(null);
                viewmodels.CommonNavInfo.IsManuallySyncAllProfiles = false;
            }
        }

        private void TextBoxSearch_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            Telerik.Windows.Controls.GridViewColumn itemCodeColumn = this.inventoryDetails.Columns["ItemCode"];
            Telerik.Windows.Controls.GridViewColumn itemDescColumn = this.inventoryDetails.Columns["ItemDescription"];

            #region searchUsingFilters

            //if (!string.IsNullOrEmpty(TextBoxSearch.Text))
            //{
            //    // Getting it from the property will create it and associate it with its column automatically.
            //    IColumnFilterDescriptor columnDescDescriptor = itemDescColumn.ColumnFilterDescriptor;
            //    columnDescDescriptor.SuspendNotifications();
            //    columnDescDescriptor.FieldFilter.Filter1.Operator = FilterOperator.Contains;
            //    columnDescDescriptor.FieldFilter.Filter1.Value = TextBoxSearch.Text;
            //    columnDescDescriptor.ResumeNotifications();
            //    // Getting it from the property will create it and associate it with its column automatically.
            //    int code;
            //    var value = int.TryParse(TextBoxSearch.Text, out code);
            //    if (value)
            //    {


            //        IColumnFilterDescriptor columnCodeDescriptor = itemCodeColumn.ColumnFilterDescriptor;
            //        columnCodeDescriptor.SuspendNotifications();
            //        columnCodeDescriptor.FieldFilter.Filter1.Operator = FilterOperator.Contains;

            //        columnCodeDescriptor.FieldFilter.Filter1.Value = code.ToString();
            //        columnCodeDescriptor.ResumeNotifications();
            //    }
            //}
            //else
            //{
            //   itemCodeColumn.ClearFilters();
            //    itemDescColumn.ClearFilters();

            //}
            #endregion
        }

        private void RadGridView_Filtered(object sender, GridViewFilteredEventArgs e)
        {

        }

        // Update User Setup
        private void btnEditUserProfile_Click(object sender, RoutedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Presentation.Views][RouteHome][Start:btnEditUserProfile_Click]");
            try
            {
                if (String.IsNullOrEmpty(txtFirstName.Text))
                {
                    Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Constants.UserSetup.UserFirstName }, viewModel.MessageToken);
                    txtFirstName.Focus();
                    return;
                }
                if (String.IsNullOrEmpty(txtLastName.Text))
                {
                    Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Constants.UserSetup.UserLastName }, viewModel.MessageToken);
                    txtLastName.Focus();
                    return;
                }
                if (String.IsNullOrEmpty(txtDisplayName.Text))
                {
                    Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Constants.UserSetup.UserDisplayName }, viewModel.MessageToken);
                    txtDisplayName.Focus();
                    return;
                }
                bool isEmail = Regex.IsMatch(txtEmailAddress.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                if (!isEmail)
                {
                    Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Constants.UserSetup.EmailAddress }, viewModel.MessageToken);
                    txtEmailAddress.Focus();
                    return;
                }

                int AppUserId = viewModel.UserProfileInfo.FirstOrDefault().AppUserID;
                string FirstName = viewModel.UserProfileInfo.FirstOrDefault().FirstName.ToString();
                string LastName = viewModel.UserProfileInfo.FirstOrDefault().LastName.ToString();
                string DisplayName = viewModel.UserProfileInfo.FirstOrDefault().DisplayName.ToString();
                string EmailAddress = viewModel.UserProfileInfo.FirstOrDefault().EmailAddress.ToString();
                string PhoneNumber = viewModel.UserProfileInfo.FirstOrDefault().PhoneNumber.ToString();
                string SelectedMode = viewModel.UserProfileInfo.FirstOrDefault().SelectedMode.ToString();
                string Title = viewModel.UserProfileInfo.FirstOrDefault().SelectedTitle.ToString();

                // User Profile Edit                
                int TranStatus = 0;
                StringBuilder MyQuery = new StringBuilder();
                if (AppUserId > 0)
                {
                    MyQuery.Append("	UPDATE BUSDTA.UserProfileMaster SET FirstName='" + FirstName + "', Title='" + Title + "',	");
                    MyQuery.Append("	LastName='" + LastName + "',DisplayName='" + DisplayName + "',Phone='" + PhoneNumber + "',Email='" + EmailAddress + "' 	");
                    MyQuery.Append("	WHERE App_user_id='" + AppUserId + "'	");
                    TranStatus = DbEngine.ExecuteNonQuery(MyQuery.ToString());
                    if (TranStatus > 0)
                    {
                        // Insert || Update Signature
                        int RecordExists = 0;
                        MyQuery = new StringBuilder();
                        MyQuery.Append(" SELECT Count(ud.App_user_id) FROM BUSDTA.UserProfile_Detail ud WHERE ud.App_User_ID='" + AppUserId + "'");
                        RecordExists = Convert.ToInt32(DbEngine.ExecuteScalar(MyQuery.ToString()));
                        if (RecordExists != 0)
                        {
                            // Update Profile Details
                            MyQuery = new StringBuilder();
                            SelectedMode = (SelectedMode == "Auto") ? "A" : "M";
                            MyQuery.Append(" UPDATE BUSDTA.UserProfile_Detail SET ScannerMode='" + SelectedMode + "',UpdatedBy='" + AppUserId + "',UpdatedDatetime=NOW() ");
                            MyQuery.Append(" WHERE App_user_id='" + AppUserId + "'");
                            TranStatus = DbEngine.ExecuteNonQuery(MyQuery.ToString());
                            if (TranStatus > 0)
                            {
                                viewModel.SignatureCanvasVM.UpdateSignatureToDB(false, false, true, "test.jpg");
                                Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Constants.UserSetup.UserProfileUpdated }, viewModel.MessageToken);
                            }
                            else
                                Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Constants.UserSetup.UserProfileFailed }, viewModel.MessageToken);
                        }
                        else
                        {
                            // Insert Profile Details 
                            MyQuery = new StringBuilder();
                            SelectedMode = (SelectedMode == "Auto") ? "A" : "M";
                            MyQuery.Append(" INSERT INTO BUSDTA.UserProfile_Detail(App_user_id,ScannerMode,CreatedBy,CreatedDatetime) VALUES ('" + AppUserId + "','" + SelectedMode + "','" + AppUserId + "',NOW()) ");
                            TranStatus = DbEngine.ExecuteNonQuery(MyQuery.ToString());
                            if (TranStatus > 0)
                                Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Constants.UserSetup.UserProfileUpdated }, viewModel.MessageToken);
                            else
                                Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Constants.UserSetup.UserProfileFailed }, viewModel.MessageToken);
                        }
                    }
                }
                SalesLogicExpress.Application.Helpers.ResourceManager.QueueManager.QueueProcess("SystemCfg", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Presentation.Views][RouteHome][btnEditUserProfile_Click][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
