﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SalesLogicExpress.Presentation.Helpers;
using System.Text.RegularExpressions;
using System.Windows.Threading;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for PickOrder.xaml
    /// </summary>
    public partial class PickOrder : BaseWindow,IDisposable
    {

        SalesLogicExpress.Application.ViewModels.PickOrder viewModel;
        public PickOrder()
        {
            InitializeComponent();
            ScreenID = Helpers.Constants.Title_PickOrderViewTitle;
            ScreenLifeCycle = Helpers.Constants.OrderLifeCycle;
            LoadWindow();
            this.Activated += WindowActivated;
            AssociatedWindows.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PreviewOrder);
        }
        public PickOrder(object PAYLOAD)
        { }

        async void LoadWindow()
        {
            await Task.Run(() =>
            {
                Dispatcher.BeginInvoke(DispatcherPriority.DataBind, new Action(() =>
          {
              viewModel = new SalesLogicExpress.Application.ViewModels.PickOrder();
              SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = !viewModel.IsInVoidState;
              this.DataContext = viewModel;
              viewModel.MessageToken = this.Token;
              this.AlwaysShowKeyboard = true;
              this.DefaultFocusElement = txt_OrderQty;
              if (viewModel.CanOpenCashCollection)
              {
                  viewModel.OpenCashCollectionPopUp();
                  viewModel.CanOpenCashCollection = false;
              }
              viewModel.selectedReason.StateChanged += selectedReason_StateChanged;
              SetNavigationDefaults();

          }));

            });

        }
        public override void RefreshWindow(object payload)
        {
            if (this.DataContext != null)
            {
                ((SalesLogicExpress.Application.ViewModels.PickOrder)this.DataContext).IsBusy = true;
            }
            LoadWindow();
            SetNavigationDefaults();
            base.AlwaysShowNumericKeyboard();
            base.RefreshWindow(payload);
        }
        private void selectedReason_StateChanged(object sender, Application.ViewModels.OrderStateChangeArgs e)
        {
            List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View> WindowsToClosed = new List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View>();
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PreviewOrder);
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.Order);
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.OrderTemplate);
            AssociatedWindows = WindowsToClosed;
        }
        private void WindowActivated(object sender, EventArgs e)
        {
            //SetNavigationDefaults();
        }
        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;

            //SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            if (SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView == Application.Helpers.ViewModelMappings.View.DeliveryScreen)
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = false;
            else if (Application.ViewModels.Order.VoidOrderReasonId != 0)
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = false;
            else
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;

            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Pick Order";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Order";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.Order;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.PickOrder;
        }

        protected override void DisplaySettingsChanged(object sender, EventArgs e)
        {
            base.DisplaySettingsChanged(sender, e);
            if (WindowOrientation == WindowOrientation.Portrait)
            {
                CustomerAddressPanel.Visibility = System.Windows.Visibility.Collapsed;
                //ScanningStatusBlock.FontSize = 14;
            }
            if (WindowOrientation == WindowOrientation.Landscape)
            {
                CustomerAddressPanel.Visibility = System.Windows.Visibility.Visible;
                //ScanningStatusBlock.FontSize = 16;

            }
        }

        private void PickOrderGrid_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangeEventArgs e)
        {

            if (e.AddedItems.Count > 0)
            {
                viewModel.SetPickItemLabels.Execute(e.AddedItems[0]);
                //lbl_pickItemNo.Text = "";
                txt_OrderQty.Focus();
            }

        }

        private void txt_OrderQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var dc = (SalesLogicExpress.Application.ViewModels.PickOrder)this.DataContext;
                dc.ManualEnteredQty = txt_OrderQty.Text == "" ? 0 : int.Parse(txt_OrderQty.Text.ToString());
                dc.IsManualPick = true;
                dc.ManualPick.Execute(dc.IsAddingToPalette ? PickOrderGrid.SelectedItem : ExceptionGrid.SelectedItem);
            }
        }

        private void txt_OrderQty_GotFocus(object sender, RoutedEventArgs e)
        {
            (sender as TextBox).SelectAll();
        }


        private void RadNumericUpDown_ValueChanged(object sender, Telerik.Windows.Controls.RadRangeBaseValueChangedEventArgs e)
        {
             Logger.Info("[SalesLogicExpress.Presentation.Views][PickOrder][Start:RadNumericUpDown_ValueChanged]");
             try
             {
                 if (e.OldValue != null)
                 {
                     var dc = (SalesLogicExpress.Application.ViewModels.PickOrder)this.DataContext;
                     if (dc!=null)
                     {
                     if (!dc.IsScannerOn && (((FrameworkElement)sender)).DataContext != null)
                     {
                         PickOrderGrid.SelectedItem = (((FrameworkElement)sender)).DataContext;
                         dc.IsAddingToPaletteOneByOne = true;
                         dc.ScanOneByOneAdd.Execute((((FrameworkElement)sender)).DataContext);
                     }
                     if (dc.SelectedPickItem != null)
                     {
                         if (dc.SelectedPickItem.StkType == "N")
                         {
                             e.NewValue = e.OldValue;
                             dc.PickedItemQty = dc.SelectedPickItem.OrderQtyInPrimaryUOM;
                         }
                     }
                     }
                 }
             }
             catch (Exception ex)
             {
                 Logger.Error("[SalesLogicExpress.Presentation.Views][PickOrder][RadNumericUpDown_ValueChanged][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
             }
            Logger.Info("[SalesLogicExpress.Presentation.Views][PickOrder][End:RadNumericUpDown_ValueChanged]");
        }

        private void RadNumericUpDown1_ValueChanged(object sender, Telerik.Windows.Controls.RadRangeBaseValueChangedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Presentation.Views][PickOrder][Start:RadNumericUpDown1_ValueChanged]");
            try
            {
                if ((((FrameworkElement)sender)).Name == "ExceptionGridRad")
                {
                    if (e.OldValue != null)
                    {
                        if ((e.NewValue < e.OldValue))
                        {
                            var dc = (SalesLogicExpress.Application.ViewModels.PickOrder)this.DataContext;

                            if (dc.SelectedExceptionItem != null && dc.LastScanItem != null)
                                if (dc.LastScanItem.ItemNumber == dc.SelectedExceptionItem.ItemNumber)
                                    if (dc.OldExceptionCount == Convert.ToInt32(e.OldValue) && dc.NewExceptionCount == Convert.ToInt32(e.NewValue))
                                        return;

                            dc.OldExceptionCount = Convert.ToInt32(e.OldValue.HasValue ? e.OldValue : 0);
                            dc.NewExceptionCount = Convert.ToInt32(e.NewValue.HasValue ? e.NewValue : 0);
                            if (!dc.IsScannerOn && (((FrameworkElement)sender)).DataContext != null)
                            {
                                //ExceptionGrid.SelectedItem = (((FrameworkElement)sender)).DataContext;
                                dc.IsAddingToPaletteOneByOne = false;
                                dc.ScanOneByOneRemoveFromGrid.Execute((((FrameworkElement)sender)).DataContext);
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Presentation.Views][PickOrder][RadNumericUpDown1_ValueChanged][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Presentation.Views][PickOrder][End:RadNumericUpDown1_ValueChanged]");
        }

        private void RadButton_Click(object sender, RoutedEventArgs e)
        {
            KeyPadPanel.Visibility = KeyPadPanel.Visibility == System.Windows.Visibility.Visible ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
        }


        private void txt_OrderQty_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            try
            {
                e.Handled = !(new Regex(@"^\d{0,4}(\d{0,2})?$").IsMatch(e.Text));

            }
            catch (Exception ex)
            {

                // log.Error("Page:FinalOrderScreen.xaml.cs,Method:UOMPrice_PreviewTextInput, Message: " + ex.Message);

            }
        }

        private void RCB_ManualPickReasonList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            viewModel.ErrorText = "";
        }
        private void Btn_PickOrder_Click(object sender, RoutedEventArgs e)
        {
            viewModel.ShowPickSlipReport.Execute(this.viewModel);
            //var dc = (SalesLogicExpress.Application.ViewModels.PickOrder)this.DataContext;
            //SalesLogicExpress.Application.ViewModels.PrintPickOrder printPO = (SalesLogicExpress.Application.ViewModels.PrintPickOrder)this.viewModel;
            //PrintPickOrder po = new PrintPickOrder();
            //po.Show();

        }

        private void btn_RemoveExceptions_Click(object sender, RoutedEventArgs e)
        {
            PickOrderGrid.SelectedItems.Clear();
        }

        private void txt_PaymentAmt_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("^-{0,1}\\d+\\.{0,1}\\d*$");
            e.Handled = !regex.IsMatch((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
        }

        private void txt_ChequeNo_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void txt_ApprovalCode_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }


        private void txt_PaymentAmt_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private void txt_PaymentAmt_KeyDown(object sender, KeyEventArgs e)
        {
            string strkey = e.Key.ToString().Substring(e.Key.ToString().Length - 1,
               e.Key.ToString().Length - (e.Key.ToString().Length - 1));

            if (e.Key >= Key.D0 && e.Key <= Key.D9 ||
            e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
            {
                //Do not allow more than 2 digits after decimal point
                TextBox tb = sender as TextBox;
                int cursorPosLeft = tb.SelectionStart;
                int cursorPosRight = tb.SelectionStart + tb.SelectionLength;
                string result1 = tb.Text.Substring(0, cursorPosLeft) +
                      strkey + tb.Text.Substring(cursorPosRight);
                string[] parts = result1.Split('.');
                if (parts.Length > 1)
                {
                    if (parts[1].Length > 2 || parts.Length > 2)
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
