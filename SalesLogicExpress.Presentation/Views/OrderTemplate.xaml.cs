﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using GalaSoft.MvvmLight.Messaging;
using Telerik.Windows.Controls;
using SalesLogicExpress.Presentation.Helpers;
using ViewModels = SalesLogicExpress.Application.ViewModels;
using System.ComponentModel;
using System.Windows.Threading;
using log4net;
using Telerik.Windows.Controls.GridView;
namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for OrderTemplate.xaml
    /// </summary>
    public partial class OrderTemplate : BaseWindow,IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Views.OrderTemplate");

        //ViewModels.OrderTemplate OrderViewModel;
        ViewModels.OrderTemplate viewModel;
        public OrderTemplate()
        {
            InitializeComponent();
            ScreenID = Helpers.Constants.Title_OrderTemplateViewTitle;
            ScreenLifeCycle = Helpers.Constants.OrderLifeCycle;
            this.Activated += WindowActivated;
            this.Loaded += OrderTemplate_Loaded;
            LoadContext();
            AddTagsToControls();
        }

        private void AddTagsToControls()
        {
            ButtonDeleteTemplateItems.Tag = Helpers.Constants.PO_DeleteFromPreOrder;
            ButtonPlaceOrder.Tag = Helpers.Constants.OT_PlaceOrder;
        }
        //********************************************
        // TODO : Check for transition state for enabling/disabling buttons or other action elements when transiting
        //********************************************
        //this.TransitionStateChanged += OrderTemplate_TransitionStateChanged;
        //void OrderTemplate_TransitionStateChanged(object sender, BaseWindow.TransitionStateEventArgs e)
        //{
        //    viewModel.IsInTransition = e.State == TransitionState.Started ? true : false;
        //}
        //********************************************
        void viewModel_ModelChanged(object sender, ViewModels.OrderTemplate.ModelChangeArgs e)
        {
            if (e.Change == ViewModels.OrderTemplate.ChangeType.ItemAdded)
            {
                this.OrderTemplateGrid.ScrollIntoViewAsync(this.OrderTemplateGrid.Items[this.OrderTemplateGrid.Items.Count - 1], null);
            }
            if (e.Change == ViewModels.OrderTemplate.ChangeType.ResetCanvas)
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                     new Action(() =>
                     {
                         Helpers.CanvasHelper.ClearCanvas();
                         viewModel.ResetSign();
                     }
               ));
            }
            else
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                       new Action(() =>
                       {
                           if (viewModel.OrderHistoryHeaders != null && viewModel.OrderHistory != null)
                               BindOrderHistory(viewModel.OrderHistoryHeaders, viewModel.OrderHistory);
                       }
               ));
            }
        }

        public void BindOrderHistory(DataTable OrderHistoryHeaders, DataTable OrderHistoryItems)
        {
            try
            {
                List<Telerik.Windows.Controls.GridViewDataColumn> columnsToAdd = new List<Telerik.Windows.Controls.GridViewDataColumn>();

                GRD_OrderHistory.IsBusy = true;
                if (OrderHistoryHeaders.Rows.Count > 0)
                {
                    while (GRD_OrderHistory.Columns.Count > 2)
                    {
                        GRD_OrderHistory.Columns.RemoveAt(2);
                    }

                    DataView view = OrderHistoryHeaders.DefaultView;
                    view.Sort = "chronology asc";
                    OrderHistoryHeaders = view.ToTable();

                    for (int i = 0; i < OrderHistoryHeaders.Rows.Count; i++)
                    {
                        Telerik.Windows.Controls.GridViewDataColumn column = new Telerik.Windows.Controls.GridViewDataColumn();
                        column.HeaderCellStyle = (Style)this.FindResource("ColumnHeaderStyle");
                        string bindingName = "H" + (i + 1) + "_Qty";
                        ViewModels.Order.SalesHeader s = new ViewModels.Order.SalesHeader(OrderHistoryHeaders.Rows[i]["OrderNumber"].ToString(), OrderHistoryHeaders.Rows[i]["OrderDate"].ToString());
                        //column.Header = dt.Rows[i]["OrderNumber"].ToString();
                        column.Tag = OrderHistoryHeaders.Rows[i]["OrderNumber"].ToString() + "|" + OrderHistoryHeaders.Rows[i]["OrderDate"].ToString();
                        if (OrderHistoryItems.Columns[bindingName] != null)
                        {
                            if(!OrderHistoryItems.Columns.Contains(column.Tag.ToString()))
                                OrderHistoryItems.Columns[bindingName].ColumnName = column.Tag.ToString();
                        }
                        column.DataMemberBinding = new Binding(column.Tag.ToString());
                        column.DataMemberBinding.TargetNullValue = "0";
                        column.IsFilterable = false;
                        column.IsSortable = false;
                        column.HeaderTextAlignment = TextAlignment.Center;
                        column.TextAlignment = TextAlignment.Right;
                        columnsToAdd.Add(column);

                        if (i == 9) break;
                    }
                    for (int index = 0; index < columnsToAdd.Count; index++)
                    {
                        GRD_OrderHistory.Columns.Insert(2, columnsToAdd[index]);
                    }

                    for (int i = 0; i < OrderHistoryItems.Rows.Count; i++)
                    {
                        double averageQty = Convert.ToDouble(OrderHistoryItems.Rows[i]["AverageQty"]);
                        AverageStopQuantity(averageQty);
                    }
                }

                GRD_OrderHistory.IsBusy = false;
            }
            catch (Exception ex)
            {
                GRD_OrderHistory.IsBusy = false;
                Logger.Error("[SalesLogicExpress.Views.OrderTemplate][OrderTemplate][BindOrderHistory][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

        }

        public double AverageStopQuantity(double avgqty)
        {
            return avgqty;
        }
        async void LoadContext()
        {
            await Task.Run(() =>
            {
                SetNavigationDefaults();
                viewModel = new ViewModels.OrderTemplate();
                viewModel.ModelChanged += viewModel_ModelChanged;
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    viewModel.MessageToken = this.Token;
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo cminfo = SalesLogicExpress.Application.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                    cminfo.ParentViewModel = viewModel;
                    NavigationHeader.DataContext = cminfo;
                    this.DataContext = viewModel;
                }));
            });
        }
        void OrderTemplate_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void WindowActivated(object sender, EventArgs e)
        {
            if (this.DataContext != null)
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo cminfo = SalesLogicExpress.Application.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                cminfo.ParentViewModel = this.DataContext;
                NavigationHeader.DataContext = cminfo;
            }
            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Order Template";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Customer Home";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.CustomerHome;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.OrderTemplate;

        }
        public override void RefreshWindow(object payload)
        {
            if (this.DataContext != null)
            {
                ((SalesLogicExpress.Application.ViewModels.OrderTemplate)this.DataContext).IsBusy = true;
            }
            LoadContext();
            SetNavigationDefaults();
            base.RefreshWindow(payload);
        }

        protected override void DisplaySettingsChanged(object sender, EventArgs e)
        {
            base.DisplaySettingsChanged(sender, e);
            if (WindowOrientation == WindowOrientation.Portrait)
            {
                CustomerAddressPanel.Visibility = System.Windows.Visibility.Collapsed;
            }
            if (WindowOrientation == WindowOrientation.Landscape)
            {
                CustomerAddressPanel.Visibility = System.Windows.Visibility.Visible;
            }
        }



        private void OrderTemplateGrid_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e)
        {
            if (e.Cell.Column.UniqueName == "SeqNo")
            {
                string strSeqNo = ((Telerik.Windows.Controls.RadNumericUpDown)(e.EditingElement)).ContentText.ToString();
                if (strSeqNo != e.OldData.ToString())
                {
                    viewModel.OldSequenceNo = Convert.ToInt32(e.OldData.ToString());
                    if (!(e.Cell.DataContext == null))
                        viewModel.GridCellEditEnded.Execute(e.Cell.DataContext);

                    OrderTemplateGrid.SortDescriptors.Add(viewModel.SortGrid());

                }
                OrderTemplateGrid.SortDescriptors.Add(viewModel.SortGrid());

            }

            if (e.Cell.Column.UniqueName == "OrderQty")
            {
                //TODO: this function is not getting called, need to place it some where else
                viewModel.UpdateAvailableQty.Execute(e.Cell.DataContext);
            }
            if (e.Cell.Column.UniqueName != "OrderQty")
            {
                if (!Equals(e.OldData, e.NewData))
                {
                    Helpers.CanvasHelper.ClearCanvas();
                    viewModel.ResetSign();
                }
            }

        }


        //************************************************************************************************
        // Comment: Sorting & Sorted Event Implementation
        // Created: May 18, 2016
        // Author: TechUnison (Velmani Karnan)
        // User Issue ID:  91 Implementation
        //*************************************************************************************************

        private void OrderTemplateGrid_Sorting(object sender, GridViewSortingEventArgs e)
        {
            // Sorting Event
            //e.Cancel = true;
        }

        private void OrderTemplateGrid_Sorted(object sender, GridViewSortedEventArgs e)
        {
            // Sorted Test Code
            //e.Column.Background = new SolidColorBrush(Colors.Green);            
        }

        private void BtnAddToOrder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OrderTemplateGrid.SortDescriptors.Add(viewModel.SortGrid());
            }
            catch (Exception es)
            {

            }
        }

        private void CloseDropdown_Click(object sender, RoutedEventArgs e)
        {
            DropDownPreloadWith.IsOpen = false;
        }

        private void BaseWindow_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (bool.Parse(e.NewValue.ToString()) == true)
            {
                //viewModel.UpdateTemplateItemsFromOrder.Execute(null);
            }
        }
        private void RadNumericUpDown_ValueChanged(object sender, RadRangeBaseValueChangedEventArgs e)
        {
            if (e.OldValue != null && e.NewValue != null && !Equals(e.OldValue, e.NewValue))
            {
                Helpers.CanvasHelper.ClearCanvas();
                viewModel.ResetSign();
            }
            var operationDir = e.NewValue < e.OldValue ? "decreased" : "increased";
            RadNumericUpDown numberContextVal = (RadNumericUpDown)sender;
            if (numberContextVal.ContentText != "" && numberContextVal.ContentText != null)
                viewModel.isNumericIncDecAction = true;
            else
                viewModel.isNumericIncDecAction = false;



        }

        private void SearchTextValue_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox.Text.Length >= 3)
            {
                foreach (Telerik.Windows.Controls.GridViewColumn column in this.SearchItemList.Columns)
                {
                    column.ClearFilters();
                }

                // SearchItemList.FilterDescriptors.Clear();
            }
        }

        private void SearchItemList_Sorting(object sender, GridViewSortingEventArgs e)
        {
            if (e.NewSortingState == SortingState.None)
            {
                e.NewSortingState = SortingState.Ascending;
            }
        }

        private void OrderTemplateGrid_DataLoaded(object sender, EventArgs e)
        {
            RowReorderBehavior.SetIsEnabled(this.OrderTemplateGrid, true);
        }

        private void OnSelectionColumnHeaderClicked(object sender, RoutedEventArgs args)
        {
            CheckBox headerCheckBox = sender as CheckBox;
            if (headerCheckBox.IsChecked == true)
            {
            }
            else if (headerCheckBox.IsChecked == false)
            {
            }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            dynamic checkBox = (CheckBox)sender;
            if (checkBox != null)
            {
                if (checkBox.IsChecked == true)
                {


                }
            }

        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
