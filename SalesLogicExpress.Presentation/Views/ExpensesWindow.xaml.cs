﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using SalesLogicExpress.Presentation.Helpers;
using ViewModelApplication = SalesLogicExpress.Application;
using viewmodels = SalesLogicExpress.Application.ViewModels;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for ExpensesWindow.xaml
    /// </summary>
    public partial class ExpensesWindow : BaseWindow, IDisposable
    {
        private viewmodels.ExpensesViewModel viewModel;
        private bool disposed = false;
        private System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);

        public ExpensesWindow()
        {
            this.InitView();
            this.InitializeComponent();
            this.LoadContext();
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            if (disposing)
            {
                this.handle.Dispose();
            }
            this.disposed = true;
        }

        private void InitView()
        {
            this.Activated += this.Expenses_Activated;
            this.Loaded += this.Expenses_Loaded;
        }

        private void Expenses_Loaded(object sender, RoutedEventArgs e)
        {
            // System.Diagnostics.Debug.WriteLine("PreTripInspection_Loaded, " + DateTime.Now.Millisecond);
            this.AlwaysShowKeyboard = true;
        }

        private void Expenses_Activated(object sender, EventArgs e)
        {
            //SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Route Settlement";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Expenses";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteSettlement;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ExpensesWindow;
        }

        private async void LoadContext()
        {
            await Task.Run(() =>
            {
                if (true)
                {
                    this.viewModel = new viewmodels.ExpensesViewModel();
                    this.SetNavigationDefaults();

                    //viewmodels.ServiceRoute.SelectedCalendarDate = viewModel.DailyStopDate;
                    viewmodels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                    cminfo.ParentViewModel = this.viewModel;
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.DataContext = this.viewModel;
                        this.viewModel.MessageToken = this.Token;
                        this.NavigationHeader.DataContext = cminfo;
                        //NavigationHeader.DataContext = cminfo;
                    }));
                }
            });
        }
    }
}
