﻿using log4net;
using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for ReplenishSuggestionPickView.xaml
    /// </summary>
    public partial class ReplenishSuggestionPickView :BaseWindow,IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Views.ReplenishAddLoadViewModel");
        SalesLogicExpress.Application.ViewModels.ReplenishSuggestionPickViewModel ReplenishSuggestionPickVM;
        public ReplenishSuggestionPickView()
        {
            InitializeComponent();
            InitializeComponent();
            ScreenID = Helpers.Constants.Title_AddLoadViewTitle;
            ScreenLifeCycle = Helpers.Constants.ApplicationLifeCycle;
            LoadContext();
            this.Activated += ReplenishSuggestionPickView_Activated;
            this.Loaded += ReplenishSuggestionPickView_Loaded;
        }

        void ReplenishSuggestionPickView_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("ReplenishSuggestionPickView_Loaded, " + DateTime.Now.Millisecond);
            SetNavigationDefaults();
        }

        void ReplenishSuggestionPickView_Activated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }

        
        async void LoadContext()
        {
            await Task.Run(() =>
            {
                try
                {

                    ReplenishSuggestionPickVM = new Application.ViewModels.ReplenishSuggestionPickViewModel();
                    SetNavigationDefaults();
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        ReplenishSuggestionPickVM.MessageToken = this.Token;
                        this.DataContext = ReplenishSuggestionPickVM;
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo cminfo = SalesLogicExpress.Application.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                        cminfo.ParentViewModel = ReplenishSuggestionPickVM;
                        NavigationHeader.DataContext = cminfo;
                        ReplenishSuggestionPickVM.IsBusy = false;
                    }));

                    
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Views.ReplenishSuggestionPickView][AddLoad][LoadContext][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }

            });

        }
        public override void RefreshWindow(object payload)
        {
            if (this.DataContext != null)
            {
                ((SalesLogicExpress.Application.ViewModels.ReplenishSuggestionPickViewModel)this.DataContext).IsBusy = true;
            }
            LoadContext();
            SetNavigationDefaults();
            base.RefreshWindow(payload);
        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Add Load";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Route Home";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteHome;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ReplenishSuggestionPickView;
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
