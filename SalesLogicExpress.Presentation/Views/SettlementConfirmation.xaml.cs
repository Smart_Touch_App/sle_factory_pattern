﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Presentation.Helpers;
using ViewModelApplication = SalesLogicExpress.Application;
using viewmodels = SalesLogicExpress.Application.ViewModels;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for SettlementConfirmation.xaml
    /// </summary>
    public partial class SettlementConfirmation : BaseWindow, IDisposable
    {
        private viewmodels.SettlementConfirmationViewModel viewModel;

        private bool disposed = false;
        private System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);

        public SettlementConfirmation()
        {
            this.InitializeComponent();
            this.InitView();
            this.LoadContext();
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            if (disposing)
            {
                this.handle.Dispose();
            }
            this.disposed = true;
        }

        private void InitView()
        {
            this.Activated += this.SettlementConfirmation_Activated;
            this.Loaded += this.SettlementConfirmation_Loaded;
            this.TestInkCanvas.StrokeCollected += this.TestInkCanvas_StrokeCollected;  
        }

        private void TestInkCanvas_StrokeCollected(object sender, InkCanvasStrokeCollectedEventArgs e)
        {
            if (this.TestInkCanvas.Strokes.Count > 0)
            {
                //viewModel.MyStrokeCollection = new System.Windows.Ink.StrokeCollection();
                //viewModel.MyStrokeCollection = this.TestInkCanvas.Strokes;
                this.viewModel.MyCanvas = new InkCanvas();
                this.viewModel.MyCanvas = this.TestInkCanvas;
            }
        }

        private void SettlementConfirmation_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(string.Format("SettlementConfirmation_Loaded, {0}", DateTime.Now.Millisecond));
            this.SetNavigationDefaults();
        }

        private void SettlementConfirmation_Activated(object sender, EventArgs e)
        {
            this.SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e); 
        }

        private async void LoadContext()
        {
            await Task.Run(() =>
            {
                //if (true)
                //{
                this.Logger.Info("[SalesLogicExpress.Presentation.Views][SettlementConfirmation][Start:LoadContext]");
                try
                {
                    this.viewModel = new viewmodels.SettlementConfirmationViewModel();
                    //viewmodels.ServiceRoute.SelectedCalendarDate = viewModel.DailyStopDate;
                    //viewModel.MyCanvas = new InkCanvas();
                    this.viewModel.MyCanvas = this.TestInkCanvas;
                    viewmodels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                    cminfo.ParentViewModel = this.viewModel;
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.DataContext = this.viewModel;
                        this.viewModel.MessageToken = this.Token;
                        this.NavigationHeader.DataContext = cminfo;
                    }));
                }
                catch (Exception ex)
                {
                    this.Logger.Error(string.Format("[SalesLogicExpress.Presentation.Views][SettlementConfirmation][LoadContext][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
                }
                this.Logger.Info("[SalesLogicExpress.Presentation.Views][SettlementConfirmation][End:LoadContext]");
                //}
            });
        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = Application.Helpers.ViewModelMappings.View.RouteSettlement.GetEnumDescription();
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = ViewModelMappings.View.SettlementConfirmation.GetEnumDescription();
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteSettlement;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.SettlementConfirmation;
        }

        private void btnClearCanvas_Click(object sender, RoutedEventArgs e)
        {
            if (this.TestInkCanvas.Strokes.Count > 0)
            {
                this.TestInkCanvas.Strokes.Clear();
            }
        }
    }
}
