﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using ViewModels = SalesLogicExpress.Application.ViewModels;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for PreOrder.xaml
    /// </summary>
    public partial class PreOrder : BaseWindow,IDisposable
    {
        ViewModels.PreOrder viewModel;
        bool flag;
        public PreOrder()
        {
            InitializeComponent();
            this.Activated += WindowActivated;
            this.Loaded += PreOrder_Loaded;

        }
        public PreOrder(bool payload)
        {
            flag = payload;
            InitializeComponent();
            this.Activated += WindowActivated;
            this.Loaded += PreOrder_Loaded;

        }
        void PreOrder_Loaded(object sender, RoutedEventArgs e)
        {
            SetNavigationDefaults();
            viewModel = new ViewModels.PreOrder(flag);
            viewModel.MessageToken = this.Token;
            this.DataContext = viewModel;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo cminfo = SalesLogicExpress.Application.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
            cminfo.ParentViewModel = viewModel;
            NavigationHeader.DataContext = cminfo;
            viewModel.ModelChanged += viewModel_ModelChanged;
        }

        void viewModel_ModelChanged(object sender, ViewModels.PreOrder.ModelChangeArgs e)
        {
            if (e.Change == ViewModels.PreOrder.ChangeType.ItemAdded)
            {
                this.PreOrderGrid.ScrollIntoViewAsync(this.PreOrderGrid.Items[this.PreOrderGrid.Items.Count - 1], null);

            }
        }




        private void WindowActivated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Pre Order";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Customer Home";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.CustomerHome;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.PreOrder;

        }
        private void BaseWindow_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (bool.Parse(e.NewValue.ToString()) == true)
            {

            }
        }

        private void BtnAddToOrder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PreOrderGrid.SortDescriptors.Add(viewModel.SortGrid());
            }
            catch (Exception es)
            {

            }
        }

        private void CloseDropdown_Click(object sender, RoutedEventArgs e)
        {
            DropdownItems.IsOpen = false;
        }

        private void PreOrderGrid_CellEditEnded(object sender, Telerik.Windows.Controls.GridViewCellEditEndedEventArgs e)
        {

            if (e.Cell.Column.UniqueName == "SeqNo")
            {
                string strSeqNo = ((Telerik.Windows.Controls.RadNumericUpDown)(e.EditingElement)).ContentText.ToString();
                if (strSeqNo != e.OldData.ToString())
                {
                    viewModel.OldSequenceNo = Convert.ToInt32(e.OldData.ToString());
                    if (!(e.Cell.DataContext == null))
                        viewModel.GridCellEditEnded.Execute(e.Cell.DataContext);

                    PreOrderGrid.SortDescriptors.Add(viewModel.SortGrid());

                }
                PreOrderGrid.SortDescriptors.Add(viewModel.SortGrid());

            }

            if (e.Cell.Column.UniqueName == "OrderQty")
            {
                //TODO: this function is not getting called, need to place it some where else
                viewModel.UpdateAvailableQty.Execute(e.Cell.DataContext);
            }
        }

        protected override void DisplaySettingsChanged(object sender, EventArgs e)
        {
            base.DisplaySettingsChanged(sender, e);
            if (WindowOrientation == WindowOrientation.Portrait)
            {
                CustomerAddressPanel.Visibility = System.Windows.Visibility.Collapsed;
            }
            if (WindowOrientation == WindowOrientation.Landscape)
            {
                CustomerAddressPanel.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void SearchTextValue_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox.Text.Length >= 3)
            {
                foreach (Telerik.Windows.Controls.GridViewColumn column in this.SearchItemList.Columns)
                {
                    column.ClearFilters();
                }

                // SearchItemList.FilterDescriptors.Clear();
            }
        }



        private void RadNumericUpDown_ValueChanged(object sender, Telerik.Windows.Controls.RadRangeBaseValueChangedEventArgs e)
        {
            if (e.OldValue != null)
            {
                if (e.OldValue != e.NewValue)
                    viewModel.PreOrderQtyChanged.Execute(null);
            }
            //

        }

        private void SearchItemList_Sorting(object sender, Telerik.Windows.Controls.GridViewSortingEventArgs e)
        {
            if (e.NewSortingState == SortingState.None)
            {
                e.NewSortingState = SortingState.Ascending;
            }
        }




        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
