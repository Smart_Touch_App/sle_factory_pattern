﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using viewModels = SalesLogicExpress.Application.ViewModels;
using ViewModelApplication = SalesLogicExpress.Application;
using System.Windows.Shapes;
using System.Windows.Threading;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Application.Helpers;
using log4net;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for CustomerQuote.xaml
    /// </summary>
    public partial class CustomerQuote : BaseWindow,IDisposable
    {
        viewModels.CustomerQuoteViewModel viewModel = null;
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Views.CustomerQuote");

        public CustomerQuote()
        {
            InitializeComponent();
            ScreenID = PayloadManager.ProspectPayload.IsProspect ? Helpers.Constants.Title_ProspectQuoteTitle  : Helpers.Constants.Title_CustomerQuoteTitle ;
            ScreenLifeCycle = Helpers.Constants.ApplicationLifeCycle;
            CloseForwardedWindows();
            this.LoadContext();
            this.SetNavigationDefaults();
            this.Activated += CustomerQuote_Activated;
        }

        void CustomerQuote_Activated(object sender, EventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Views.CustomerQuote][WindowActivated][Enter in WindowActivated]");
            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }

        public void CloseForwardedWindows()
        {
            List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View> WindowsToClosed = new List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View>();            
            AssociatedWindows = WindowsToClosed;
            CloseAssociatedWindows();
        }
        async void LoadContext()
        {
            await Task.Run(() =>
            {
                viewModel = PayloadManager.ProspectPayload.IsProspect ? new viewModels.CustomerQuoteViewModel(this.Token, true) : new viewModels.CustomerQuoteViewModel();
                viewModels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                cminfo.ParentViewModel = viewModel;
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.DataContext = viewModel;
                    viewModel.MessageToken = this.Token;
                    NavigationHeader.DataContext = cminfo;
                    //busyIndicator.IsBusy = false;
                }));
            });
        }

        public override void RefreshWindow(object payload)
        {
            if (this.DataContext != null)
            {
                ((viewModels.CustomerQuoteViewModel)this.DataContext).IsBusy = true;
            }
            
            Logger.Info("[SalesLogicExpress.Views.CustomerHome][RefreshWindow][Enter in RefreshWindow]");
            //this.DataContext = PayloadManager.ProspectPayload.IsProspect ? new viewModels.CustomerQuoteViewModel(this.Token, true) { MessageToken = this.Token } : new viewModels.CustomerQuoteViewModel { MessageToken = this.Token };
            LoadContext();
            SetNavigationDefaults();
            base.RefreshWindow(payload);
        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            if (PayloadManager.ProspectPayload.IsProspect)
            {

                if (PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.EOD_Transaction.ToString())
                {
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = ViewModelMappings.View.RouteSettlement.GetEnumDescription();
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = ViewModelMappings.View.ProspectQuote.GetEnumDescription();
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteSettlement;
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ProspectQuote;
                }
                else
                {
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = ViewModelMappings.View.ProspectHome.GetEnumDescription();
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = ViewModelMappings.View.ProspectQuote.GetEnumDescription();
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.ProspectHome;
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ProspectQuote;
                }                
            }
            else
            {
                if (PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.EOD_Transaction.ToString())
                {
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = Application.Helpers.ViewModelMappings.View.RouteSettlement.GetEnumDescription();
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = Application.Helpers.ViewModelMappings.View.CustomerQuote.GetEnumDescription();
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteSettlement;
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.CustomerQuote;
                }
                else
                {
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = Application.Helpers.ViewModelMappings.View.CustomerHome.GetEnumDescription();
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = Application.Helpers.ViewModelMappings.View.CustomerQuote.GetEnumDescription();
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.CustomerHome;
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.CustomerQuote;
                }    
            }
            
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            

        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
