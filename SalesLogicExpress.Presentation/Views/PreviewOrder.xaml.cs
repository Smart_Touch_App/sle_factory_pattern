﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Windows.Controls;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Linq;
using System.Windows;
using log4net;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for PreviewOrder.xaml
    /// </summary>
    public partial class PreviewOrder : BaseWindow, IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Views.PreviewOrder");
        SalesLogicExpress.Application.ViewModels.PreviewOrder viewmodel;
        bool GoToCreateOrder;

        public PreviewOrder()
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Views.PreviewOrder][PreviewOrder][Enter in Constructor]");
                InitializeComponent();
                ScreenID = Helpers.Constants.Title_PreviewOrderViewTitle;
                ScreenLifeCycle = Helpers.Constants.OrderLifeCycle;
                LoadContext();
                Helpers.CanvasHelper.CreateBlankImage();
                this.Activated += WindowActivated;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.StaticPropertyChanged += CommonNavInfo_StaticPropertyChanged;
            }
            catch (Exception ex)
            {
                Logger.Info("[SalesLogicExpress.Views.PreviewOrder][PreviewOrder][ExceptionStackTrace = " + ex.Message == null ? ex.InnerException.Message : ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Views.PreviewOrder][PreviewOrder][Exit from Constructor]");
        }

        void PreviewOrder_LostFocus(object sender, RoutedEventArgs e)
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.StaticPropertyChanged -= CommonNavInfo_StaticPropertyChanged;
        }

        async void LoadContext()
        {
            await Task.Run(() =>
            {
                viewmodel = new SalesLogicExpress.Application.ViewModels.PreviewOrder();
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    viewmodel.MessageToken = Token;
                    MainGrid.DataContext = viewmodel;
                }));
                SetNavigationDefaults();
            });
        }

        public override void HideWindow()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.StaticPropertyChanged -= CommonNavInfo_StaticPropertyChanged;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
            base.HideWindow();
        }

        ~PreviewOrder()
        {
        }

        void CommonNavInfo_StaticPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsBackwardNavigationEnabled")
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    Window activeWindow = System.Windows.Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.IsActive);
                    if (activeWindow != this)
                    {
                        return;
                    }

                    if (SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled)
                    {
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Preview Order";
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Order";
                        grdSignAndPrint.Visibility = System.Windows.Visibility.Hidden;
                        SignAndPrint.Visibility = System.Windows.Visibility.Visible;
                        // TestInkCanvas.Strokes.Clear();
                    }
                    else
                    {
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Sign and Print";
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Preview Order";
                        grdSignAndPrint.Visibility = System.Windows.Visibility.Visible;
                        SignAndPrint.Visibility = System.Windows.Visibility.Collapsed;
                    }
                }));
            }
        }

        private void WindowActivated(object sender, EventArgs e)
        {
            // TestInkCanvas.Strokes.Clear();
            SetNavigationDefaults();
        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Preview Order";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Order";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.Order;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.PreviewOrder;
        }

        protected override void DisplaySettingsChanged(object sender, EventArgs e)
        {
            base.DisplaySettingsChanged(sender, e);
            if (WindowOrientation == WindowOrientation.Portrait)
            {
                this.OrderItemGrid.SetValue(Grid.ColumnSpanProperty, (object)2);
                this.orderSummaryGrid.SetValue(Grid.RowProperty, (object)1);
                this.orderSummaryGrid.SetValue(Grid.ColumnSpanProperty, (object)2);
                this.orderSummaryGrid.SetValue(Grid.ColumnProperty, (object)0);
                CustomerAddressPanel.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                this.OrderItemGrid.ClearValue(Grid.ColumnSpanProperty);
                this.orderSummaryGrid.ClearValue(Grid.ColumnSpanProperty);
                this.orderSummaryGrid.SetValue(Grid.RowProperty, (object)0);
                this.orderSummaryGrid.SetValue(Grid.ColumnProperty, (object)1);
                CustomerAddressPanel.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void btn_Accept_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                Logger.Info("PreviewOrder.xaml.cs Enter in btn_Accept_Click: ");

                Logger.Info("PreviewOrder.xaml.cs btn_Accept_Click before viewmodel.SaveSign  Token:" + Token + ", viewmodel.MessageToken:" + viewmodel.MessageToken);

                viewmodel.SaveSign.Execute(null);

                Logger.Info("PreviewOrder.xaml.cs btn_Accept_Click after viewmodel.SaveSign Token:" + Token + ", viewmodel.MessageToken:" + viewmodel.MessageToken);

                txtEnergySurchargeOvr.Text = "";
                ComboReasonCode.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.Error("PreviewOrder.xaml.cs btn_Accept_Click error: " + ex.Message);
            }
        }

        private void SignAndPrint_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Sign And Print";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "PreviewOrder";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = false;
        }

        private void txtEnergySurchargeOvr_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            if (e.Text == ".")
            {
                if (txtEnergySurchargeOvr.Text.Contains('.'))
                    e.Handled = true;
            }
        }

        bool disposed = false;
        private readonly System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}