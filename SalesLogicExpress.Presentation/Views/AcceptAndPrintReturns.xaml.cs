﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using ViewModels = SalesLogicExpress.Application.ViewModels;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for AcceptAndPrintReturns.xaml
    /// </summary>
    public partial class AcceptAndPrintReturns : BaseWindow,IDisposable
    {
        ViewModels.ItemReturnScreenVM viewModel;
        public AcceptAndPrintReturns()
        {
            InitializeComponent();
            LoadContext();
        }
        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = false;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = false;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Accept And Print";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Item Return Pick";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.ReturnOrderPickScreen;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.AcceptAndPrintReturns;
        }
        async void LoadContext()
        {
            await Task.Run(() =>
            {
                SetNavigationDefaults();
                viewModel = new ViewModels.ItemReturnScreenVM();
                viewModel.ModelChanged +=viewModel_ModelChanged;
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    viewModel.MessageToken = this.Token;
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo cminfo = SalesLogicExpress.Application.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                    cminfo.ParentViewModel = viewModel;
                    NavigationHeader.DataContext = cminfo;
                    this.DataContext = viewModel;
                }));
            });
        }

        private void viewModel_ModelChanged(object sender, ViewModels.BaseViewModel.ModelChangeArgs e)
        {
           // throw new NotImplementedException();
        }

        private void Accept_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btn_clear_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btn_Hold_Click(object sender, RoutedEventArgs e)
        {

        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
