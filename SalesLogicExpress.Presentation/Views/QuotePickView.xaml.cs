﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for QuotePickView.xaml
    /// </summary>
    public partial class QuotePickView : BaseWindow,IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Views.QuotePickView");
        SalesLogicExpress.Application.ViewModels.QuotePickViewModel QuotePickVM = null;

        public QuotePickView()
        {
            InitializeComponent();
            ScreenID = Helpers.Constants.Title_AddLoadViewTitle;
            ScreenLifeCycle = Helpers.Constants.ApplicationLifeCycle;
            LoadContext();
            this.Activated += QuotePickView_Activated;
            this.Loaded += QuotePickView_Loaded;
            this.AlwaysShowKeyboard = true;
            this.DefaultFocusElement = ManualPickTextBox;

        }

        void QuotePickView_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("QuotePickView_Loaded, " + DateTime.Now.Millisecond);
            SetNavigationDefaults();
        }

        void QuotePickView_Activated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }



        async void LoadContext()
        {
            await Task.Run(() =>
            {
                try
                {
                    if (QuotePickVM == null)
                    {
                        QuotePickVM = new Application.ViewModels.QuotePickViewModel();
                        SetNavigationDefaults();
                        Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            QuotePickVM.MessageToken = this.Token;
                            this.DataContext = QuotePickVM;
                            SalesLogicExpress.Application.ViewModels.CommonNavInfo cminfo = SalesLogicExpress.Application.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                            cminfo.ParentViewModel = QuotePickVM;
                            NavigationHeader.DataContext = cminfo;
                            QuotePickVM.IsBusy = false;
                        }));
                    }

                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Views.QuotePickView][QuotePickView][LoadContext][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }

            });

        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = ViewModelMappings.View.ProspectQuotePick.GetEnumDescription();
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = ViewModelMappings.View.ProspectQuote.GetEnumDescription();
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.ProspectQuote;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ProspectQuotePick;
        }

        private void RadNumericUpDown_ValueChanged(object sender, Telerik.Windows.Controls.RadRangeBaseValueChangedEventArgs e)
        {
            try
            {
                var dc = (SalesLogicExpress.Application.ViewModels.QuotePickViewModel)this.DataContext;
                if (dc != null)
                {
                    if (!dc.IsScannerOn && (((FrameworkElement)sender)).DataContext != null)
                    {
                        PickOrderGrid.SelectedItem = (((FrameworkElement)sender)).DataContext;
                        //dc.SelectedReturnItem = null;
                        dc.ManuallyPickCountOfSelected = Convert.ToInt32(e.NewValue);
                        dc.QuotePickSelectedItem.IsAllowManualPick = true;
                        dc.UpdateOnManualCountCommand.Execute((((FrameworkElement)sender)).DataContext);
                        dc.QuotePickSelectedItem.IsAllowManualPick = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Views.QuotePickView][QuotePickView][RadNumericUpDown_ValueChanged][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }

        public override void RefreshWindow(object payload)
        {
            if (this.DataContext != null)
            {
                ((SalesLogicExpress.Application.ViewModels.QuotePickViewModel)this.DataContext).IsBusy = true;
            }
            LoadContext();
            SetNavigationDefaults();
            base.RefreshWindow(payload);
        }

        private void ManualPickTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            //(sender as TextBox).SelectAll();
        }

        private void PickOrderGrid_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangeEventArgs e)
        {
            ManualPickTextBox.Focus();
        }

        private void ExceptionGrid_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangeEventArgs e)
        {
            ManualPickTextBox.Focus();
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
