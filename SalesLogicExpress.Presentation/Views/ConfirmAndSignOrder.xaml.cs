﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ViewModel = SalesLogicExpress.Application.ViewModels;
namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for ConfirmAndSignOrder.xaml
    /// </summary>
    public partial class ConfirmAndSignOrder : Window,IDisposable
    {
        public ConfirmAndSignOrder()
        {
            ViewModel.ConfirmAndSignOrder ConfirmAndSignOrderVM = new ViewModel.ConfirmAndSignOrder();
            this.DataContext = ConfirmAndSignOrderVM;
            InitializeComponent();
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
