﻿using log4net;
using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for DeliveryScreen.xaml
    /// </summary>
    public partial class DeliveryScreen : BaseWindow,IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Views.DeliveryScreen");
        SalesLogicExpress.Application.ViewModels.DeliveryScreen viewmodel;
        public DeliveryScreen()
        {
            InitializeComponent();
            ScreenID = Helpers.Constants.Title_DeliverOrderViewTitle;
            ScreenLifeCycle = Helpers.Constants.OrderLifeCycle;
            this.Activated += WindowActivated;
            LoadData();
            // Navigation information
        }

        public override void RefreshWindow(object payload)
        {
            if (this.DataContext != null)
            {
                ((SalesLogicExpress.Application.ViewModels.Order)this.DataContext).IsBusy = true;
            }
            LoadData();
            SetNavigationDefaults();
            base.RefreshWindow(payload);
        }


        async void LoadData()
        {
            await Task.Run(() =>
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                   {
                       SetNavigationDefaults();

                       viewmodel = new SalesLogicExpress.Application.ViewModels.DeliveryScreen();

                       viewmodel.MessageToken = Token;
                       MainGrid.DataContext = viewmodel;

                       if (viewmodel.IsInDeliverOrderMode)
                       {
                           AcceptOrder();
                           OrderDeliver(false);
                           viewmodel.IsInDeliverOrderMode = false;
                       }

                       // Cash on Delivery Customer - Cash Paid already so no Void Should not be show.
                       SalesLogicExpress.Application.Managers.OrderManager ordMgr = new Application.Managers.OrderManager();
                       string GetPaymentType = ordMgr.GetJDEOrderType();
                       if (GetPaymentType == "R3")
                           btnVoid.IsEnabled = false;

                       viewmodel.ReasonCodeList.StateChanged += ReasonCodeList_StateChanged;

                   }));
            });
        }

        void ReasonCodeList_StateChanged(object sender, Application.ViewModels.OrderStateChangeArgs e)
        {
            //List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View> WindowsToClosed = new List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View>();
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PreviewOrder);
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.Order);
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.OrderTemplate);
            //AssociatedWindows = WindowsToClosed;
        }
        private void WindowActivated(object sender, EventArgs e)
        {
            // TestInkCanvas.Strokes.Clear();
            SetNavigationDefaults();

        }
        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = false;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Delivery Screen";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.CustomerHome;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.DeliveryScreen;

            //List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View> WindowsToClosed = new List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View>();
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PreviewOrder);
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.Order);
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.OrderTemplate);
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PickOrder);
            //AssociatedWindows = WindowsToClosed;
        }

        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            AcceptOrder();
        }

        private void AcceptOrder()
        {
            SignPanelHeader.Text = "Deliver";
            btnDeliver.Visibility = System.Windows.Visibility.Visible;
            Accept.Visibility = System.Windows.Visibility.Collapsed;
            viewmodel.IsCanvasEnable = false;
            btnVoid.Visibility = System.Windows.Visibility.Collapsed;
            btn_Hold.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void btnDeliver_Click(object sender, RoutedEventArgs e)
        {
            OrderDeliver();
        }

        private void OrderDeliver(bool CanOrderSave = true)
        {
            SignPanelHeader.Text = "Delivered";
            btn_CustomerHome.Visibility = System.Windows.Visibility.Visible;
            btn_DailyStops.Visibility = System.Windows.Visibility.Visible;
            btnDeliver.Visibility = System.Windows.Visibility.Collapsed;
            btnPayment.Visibility = System.Windows.Visibility.Visible;
            txtCuctomerPo.IsEnabled = false;
            viewmodel.OrderDelivered.Execute(null);
        }

        private void btnVoid_Click(object sender, RoutedEventArgs e)
        {
            //List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View> WindowsToClosed = new List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View>();
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PreviewOrder);
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.Order);
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.OrderTemplate);
            //AssociatedWindows = WindowsToClosed;
        }

        
        private void btn_CustomerHome_Click(object sender, RoutedEventArgs e)
        {

        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
