﻿using System.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Telerik.Windows.Automation.Peers;
using ApplicationHelpers = SalesLogicExpress.Application.Helpers;
using ViewModels = SalesLogicExpress.Application.ViewModels;
using System.Runtime.InteropServices;
using SalesLogicExpress.Presentation.Helpers;
using EXT = SalesLogicExpress.Extensions;
using System.Net.NetworkInformation;

namespace SalesLogicExpress.Presentation
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : System.Windows.Application
    {
        private readonly ILog log = LogManager.GetLogger("App.xaml.cs");
        public static string customerID = "1165497";
        public static string routeID = "783";
        public App()
        {
            Activated += App_Activated;
            Deactivated += App_Deactivated;
            // Setup path for database initialization and needed resource paths
            ApplicationHelpers.ResourceManager.DatabaseResourcesDirectory = string.Concat(SalesLogicExpress.Application.Helpers.ResourceManager.DomainPath, "\\Resources\\RemoteDbSetupFiles");
            ApplicationHelpers.ResourceManager.DatabaseDirectory = string.Concat(SalesLogicExpress.Application.Helpers.ResourceManager.DomainPath, "\\Databases");
            // Get the device unique ID
            //ApplicationHelpers.ResourceManager.DeviceID = DeviceManager.Device.GenerateDeviceID();


            var ipProperties = IPGlobalProperties.GetIPGlobalProperties();
            var DeviceId = string.Format("{0}.{1}", ipProperties.HostName, ipProperties.DomainName);
            ApplicationHelpers.ResourceManager.DeviceID = DeviceId;

            // Create an active and singleton database connection
            //ApplicationHelpers.ResourceManager.GetOpenConnectionInstance();

            this.InitializeComponent();
            log4net.Config.XmlConfigurator.Configure();
            AutomationManager.AutomationMode = AutomationMode.Disabled;
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {

//#if Testing

            SetConfigs("Prototype-DV", "FBM056", "dba", "sql123", Convert.ToBoolean("True"), "http", "FBCMSWPNP1.FBCTORRANCE.COM", "6001", "/client/rs_client.dll/SLE_Backend_Farm_DV");
//#else
            //if (e.Args.Length == 9)
            //{
            //    SetConfigs(e.Args[0], e.Args[1], e.Args[2], e.Args[3], Convert.ToBoolean(e.Args[4]), e.Args[5], e.Args[6], e.Args[7], e.Args[8]);
            //}
            //else
            //{
            //    MessageBox.Show("Params:  environment route dbUser dbPassword EnableRelay RelayProtocol RelayHost RelayPort RelayUrlSuffix");
            //    if (this.MainWindow != null)
            //        this.MainWindow.Close();
            //    this.Shutdown();
            //    return;
            //}
//#endif

        }
        private void SetConfigs(string environment, string route, string dbUser, string dbPassword, bool EnableRelay, string RelayProtocol, string RelayHost, string RelayPort, string RelayUrlSuffix)
        {

            //var DeviceId = DeviceManager.Device.GenerateDeviceID();

            var ipProperties = IPGlobalProperties.GetIPGlobalProperties();
            var DeviceId = string.Format("{0}.{1}", ipProperties.HostName, ipProperties.DomainName);

            EXT.SLEConfigs.SetConfigs(deviceID: DeviceId);
            EXT.Storage.init(EXT.SLEApplication.SLELauncherResourceFullFileName);
            EXT.SLEConfigs.IsSLEInstalled = false;
            var AppInfo = EXT.SLEConfigs.InstalledApplictionInfo(EXT.SLEApplication.SLEName);
            if (!string.IsNullOrEmpty(AppInfo.AppName))
            {
                EXT.SLEConfigs.SLEAppRegName = AppInfo.AppName;
                EXT.SLEConfigs.SLEAppRegVersion = AppInfo.AppVersion;
                EXT.SLEConfigs.SLEAppRegLocation = AppInfo.AppLocation;
                EXT.SLEConfigs.IsSLEInstalled = true;

            }

            EXT.SLEConfigs.SetConfigs(Application.Helpers.ResourceManager.DeviceID, EXT.SLEApplication.SLEAppRegName, EXT.SLEApplication.SLEAppRegLocation, EXT.SLEApplication.SLEAppRegVersion, route);

            //REmoving dependecny frpm Config file, these params can be set in code.
            //These settings will be set into SalesLogicExpress.Extensions by SLEConfigs
            EXT.SLEConfigs.EnableRelay = EnableRelay;
            EXT.SLEConfigs.RelayProtocol = RelayProtocol;
            EXT.SLEConfigs.RelayHost = RelayHost;
            EXT.SLEConfigs.RelayPort = RelayPort;
            EXT.SLEConfigs.RelayUrlSuffix = RelayUrlSuffix;
            EXT.SLEConfigs.Route = route;


            //Setting Directly in RemoteDB
            EXT.RemoteDB.Route = route;
            EXT.RemoteDB.UserId = dbUser;
            EXT.RemoteDB.Password = dbPassword;


            SalesLogicExpress.Views.UserLogin.EnvironmentConfigured = environment;
            SalesLogicExpress.Views.UserLogin.RouteConfigured = route;
            // UserInfo.Route = RouteConfigured;


        }


        void App_Deactivated(object sender, EventArgs e)
        {
            // SalesLogicExpress.Presentation.Helpers.Taskbar.Show();
        }

        void App_Activated(object sender, EventArgs e)
        {
            //  SalesLogicExpress.Presentation.Helpers.Taskbar.Hide();
        }

        public void RCB_VoidOrderReasonList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        // TODO : Check how we can handle essaging at app level instead of window level.
        private object NavigateToViewExecute(NavigateToView action)
        {
            System.Windows.Window windowToShow, currentWindow;
            windowToShow = (Window)Helpers.WindowMappings.WindowInstance(action.NextViewName, action);
            currentWindow = (Window)Helpers.WindowMappings.WindowInstance(action.CurrentViewName, action);
            windowToShow.Visibility = System.Windows.Visibility.Visible;
            if (action.CloseCurrentView)
            {
                currentWindow.Close();
            }
            else
            {
                currentWindow.Visibility = System.Windows.Visibility.Hidden;
            }
            if (action.ShowAsModal)
            {
                windowToShow.ShowDialog();
                return null;
            }
            windowToShow.Show();

            return null;
        }
        //[DllImport("user32.dll", CharSet = CharSet.Auto)]
        //private static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, UIntPtr dwExtraInfo);
        //private const uint KEYEVENTF_EXTENDEDKEY = 0x1;  // Release key

        private void Application_Exit(object sender, ExitEventArgs e)
        {

            ///*Tempararily commented*/
            //keybd_event(0x14, 0x45, 0, (UIntPtr)0);
            //keybd_event(0x14, 0x45, KEYEVENTF_EXTENDEDKEY, (UIntPtr)0);

            ApplicationHelpers.ResourceManager.CloseConnectionInstance();
            ApplicationHelpers.ResourceManager.Synchronization.ShutDownSyncClient();
            SalesLogicExpress.Presentation.Helpers.Taskbar.Show();
        }
        void HideBusyIndicator()
        {
            try
            {
                Window activeWindow = System.Windows.Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.IsActive);
                Telerik.Windows.Controls.RadBusyIndicator indicator = activeWindow.FindLogicalChildren<Telerik.Windows.Controls.RadBusyIndicator>().FirstOrDefault();
                if (indicator != null)
                {
                    indicator.Visibility = System.Windows.Visibility.Collapsed;
                    indicator.IsBusy = false;
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("HideBusyIndicator, Message:{0}{1}", ex.StackTrace, ex.Message));
            }
        }
        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            if (!e.Exception.Message.Contains("ModelDialogForVoidOrderReason") && !e.Exception.Message.Contains("GradientStops"))
            {
                log.Error(string.Format("Exception:{0}, StackTrace:{1}, Message:{2}", e.Exception.ToString(), e.Exception.StackTrace, e.Exception.Message));
            }
            // Signal that we handled things--prevents Application from exiting
            HideBusyIndicator();
            e.Handled = true;
        }
    }

}
