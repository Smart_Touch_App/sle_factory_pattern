﻿using System;
using System.IO;
using System.Windows;
using Microsoft.Win32;
using WPFPdfViewer;
using SalesLogicExpress.Presentation.Helpers;
using GalaSoft.MvvmLight.Messaging;
namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class AppPDfWindow : BaseWindow
    {
        private Guid _MessageToken;

        private SalesLogicExpress.Application.GenericReport.ReportPayloads _Payload;
        public Guid MessageToken
        {

            get
            {
                return _MessageToken;
            }
            set
            {
                _MessageToken = value;
               
            }

        }

        public string PreviousViewTitle { get; set; }

        public AppPDfWindow(SalesLogicExpress.Application.GenericReport.ReportPayloads payload)
        {
            _Payload = payload;
            _Payload.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;

            InitializeComponent();
            SetNavigationDefaults();
            this.MessageToken = payload.MessageToken;
            FileToShow((byte[])payload.ReportFile);
            this.LoadFile();
            this.DataContext = this;
            //if (payload.PrintDirectly)
            //{
            //    PrintReport(payload.ShowPrintDialog);
            //    this.Visibility = System.Windows.Visibility.Hidden;
            //    //this.Close();
            //}

        }

        public string FileLocation { get; set; }

        public void FileToShow(byte[] fileBytes)
        {
            try
            {
                FileLocation = Path.GetTempFileName();
                File.WriteAllBytes(FileLocation, fileBytes);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK);
                this.Close();
            }
            finally
            {

                //File.Delete(fileName);
            }
        }

        private void SetNavigationDefaults()
        {
            
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;

            //SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            //if (SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView == Application.Helpers.ViewModelMappings.View.DeliveryScreen)
            //    SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = _Payload.ShowBackNavigation;
            //else if (Application.ViewModels.Order.VoidOrderReasonId != 0)
            //    SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = false;
            //else
            //    SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;

            SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;

            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = _Payload.CurrentViewTitle;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = _Payload.PreviousViewTitle;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            this.PreviousViewTitle = "Back To " +  _Payload.PreviousView.ToString();

           // Type t = typeof(Application.Helpers.ViewModelMappings.View);

            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = _Payload.PreviousView; //Application.Helpers.ViewModelMappings.View.PreTripVehicleInspection;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = _Payload.CurrentView;//Application.Helpers.ViewModelMappings.View.AppPDfWindow;
        }

        protected override void DisplaySettingsChanged(object sender, EventArgs e)
        {
            //base.DisplaySettingsChanged(sender, e);
            //if (WindowOrientation == WindowOrientation.Portrait)
            //{
            //    CustomerAddressPanel.Visibility = System.Windows.Visibility.Collapsed;
            //    //ScanningStatusBlock.FontSize = 14;
            //}
            //if (WindowOrientation == WindowOrientation.Landscape)
            //{
            //    CustomerAddressPanel.Visibility = System.Windows.Visibility.Visible;
            //    //ScanningStatusBlock.FontSize = 16;

            //}
        }

        //private void WindowLoaded(object sender, RoutedEventArgs e)
        //{

        //    if (!string.IsNullOrEmpty(FileLocation))
        //    {
        //        LoadFile();

        //        //this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        //        //this.WindowState = System.Windows.WindowState.Normal;
        //    }

        //}


        public bool LoadFile()
        {
            if (!string.IsNullOrEmpty(FileLocation))
            {
                try
                {

                    genericPDFViewer.LoadFile(FileLocation);

                    return true;
                }
                catch
                {

                    throw;
                }
            }

            return false;
        }
   
        #region Navigation Panel

        public bool GoToFirstPage()
        {
            try
            {
                genericPDFViewer.First();
                return true;

            }
            catch
            {

                throw;
            }
        }
        public bool GoToNextPage()
        {
            try
            {
                genericPDFViewer.Next();
                return true;

            }
            catch
            {

                throw;
            }
        }
        public bool GoToPreviousPage()
        {
            try
            {
                genericPDFViewer.Previous();
                return true;

            }
            catch
            {

                throw;
            }
        }
        public bool GoToLastPage()
        {
            try
            {
                genericPDFViewer.Last();
                return true;

            }
            catch
            {

                throw;
            }
        }
        public bool GoToSpecificPage(int pageNo)
        {
            try
            {
                genericPDFViewer.GoToThisPage(pageNo);
                return true;

            }
            catch
            {

                throw;
            }
        }


        public bool PrintReport(bool showDialog=false)
        {
            genericPDFViewer.Print(showDialog);
            return true;
        }


        //ToDo - to be implementd
        public bool EMail()
        {
            genericPDFViewer.Print();
            return true;
        }


        #endregion
   
        public bool Dispose()
        {
            try
            {
                genericPDFViewer.Dispose();
                return true;

            }
            catch
            {

                throw;
            }
        }

        private void WindowActivated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
        }
        protected override void OnClosed(System.EventArgs e)
        {
            var moveToView = new SalesLogicExpress.Application.Helpers.NavigateToView { NextViewName = _Payload.PreviousView, CurrentViewName = _Payload.CurrentView, CloseCurrentView = true, Payload = null, ShowAsModal = false };
            Messenger.Default.Send<SalesLogicExpress.Application.Helpers.NavigateToView>(moveToView, MessageToken);

    
        }

        private void RemoveTempFile()
        {
            if (File.Exists(FileLocation))
                File.Delete(FileLocation);
            
        }

        private void btnFirst_Click(object sender, RoutedEventArgs e)
        {
            GoToFirstPage();
        }

        private void btnLast_Click(object sender, RoutedEventArgs e)
        {
            GoToLastPage();
        }

        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            GoToPreviousPage();
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            GoToNextPage();
        }

        private void btnGoTo_Click(object sender, RoutedEventArgs e)
        {
            GoToSpecificPage(2);
        }

        //private void ButtonBack_Click(object sender, RoutedEventArgs e)
        //{
        //    //Clean();
        //    //Dispose();
        //    //var moveToView = new SalesLogicExpress.Application.Helpers.NavigateToView { NextViewName = _Payload.PreviousView, CurrentViewName = _Payload.CurrentView, CloseCurrentView = true, Payload = null, ShowAsModal = false };
        //    //Messenger.Default.Send<SalesLogicExpress.Application.Helpers.NavigateToView>(moveToView, MessageToken);
            
        //}

        private void ButtonEmailReport_Click(object sender, RoutedEventArgs e)
        {
            // Email Option
        }

        private void ButtonPrintReport_Click(object sender, RoutedEventArgs e)
        {
            // Print Option
            try
            {
                PrintReport(true);
            }
            catch (Exception)
            {
                
               // throw;
            }
        }
    }
}
