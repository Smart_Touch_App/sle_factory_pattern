﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Sap.Data.SQLAnywhere;
using log4net;


namespace RemoteDBDal
{
    public class Dal : IDisposable
    {

        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Helpers.DB_CRUD");
        private SAConnection _connection;

        public string ConnectionString { get; set; }
        public SAConnection Connection { get { return _connection; } }

        public Dal(SAConnection connection)
        {
            try
            {
                ConnectionString = connection.ConnectionString;
                _connection = connection;
                if(_connection.State != ConnectionState.Open)
                    _connection.Open();

            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][Constructor][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw ex;
            }
        }

        public Dal(string connectionString)
        {
            try
            {
                ConnectionString = connectionString;
                _connection = new SAConnection(ConnectionString);
                _connection.Open();

            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][Constructor][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw ex;
            }
        }

        ~Dal()
        {
            try
            {
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][Destructor][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw ex;
            }
        }

        public Boolean checkValidity(SACommand CommandToExecute)
        {
            bool result = false;
            try
            {
                SADataReader reader = null;
                reader = CommandToExecute.ExecuteReader();
                result = reader.Read();
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][checkValidity][Query = " + CommandToExecute.CommandText + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }

        public void OpenConnection()
        {
            try
            {
                if (_connection.State != ConnectionState.Open)
                    _connection.Open();
                
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][CloseConnection][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }

        public void CloseConnection()
        {
            try
            {
                if (_connection.State == ConnectionState.Open)
                    _connection.Close();
                _connection = null;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][CloseConnection][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }

        public SADataReader ExecuteReader(SACommand CommandToExecute)
        {
            SADataReader reader = null;
            try
            {
                CommandToExecute.Connection = _connection;
                reader = CommandToExecute.ExecuteReader();
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][ExecuteReader][Query = " + CommandToExecute.CommandText + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return reader;
        }

        public T ExecuteScalar<T>(SACommand CommandToExecute)
        {
            try
            {
                CommandToExecute.Connection = _connection;
                var o =  CommandToExecute.ExecuteScalar();
                if (o != null)
                    return (T)o;
               
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][ExecuteScalar][Query = " + CommandToExecute.CommandText + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return default(T);
        }

        public object ExecuteScalar(SACommand CommandToExecute)
        {
            try
            {
                CommandToExecute.Connection = _connection;
                return CommandToExecute.ExecuteScalar();
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][ExecuteScalar][Query = " + CommandToExecute.CommandText + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return string.Empty;
        }

        public SAConnection GetActiveConnection()
        {
            return _connection;
        }

        public DataTable GetDataIntoDt(SACommand CommandToExecute)
        {
            DataTable resultData = new DataTable();
            try
            {
                CommandToExecute.Connection = _connection;
                SADataAdapter da = new SADataAdapter(CommandToExecute);
                da.Fill(resultData);
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][GetDataIntoDt][Query = " + CommandToExecute.CommandText + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return resultData;
        }

        public int InsertUpdateData(SACommand CommandToExecute)
        {
            try
            {
                CommandToExecute.Connection = _connection;
                
                int inserted = CommandToExecute.ExecuteNonQuery();
                return inserted;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][Insertdata][Query = " + CommandToExecute.CommandText + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return -1;
        }

        #region Dispose Handling

        // Dispose Handling for Print Process and Optimization - Added on 12/28/2016 - Velmani

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        #endregion // Dispose Handling

    }
}
