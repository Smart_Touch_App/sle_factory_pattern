﻿using System;
using System.CodeDom.Compiler;
using System.IO;
using System.Windows.Forms;

namespace WPFPdfViewer
{
    public partial class PDFLib : WinFormPdfHost
    {
        //System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WinFormPdfHost));
        //public AxAcroPDFLib.AxAcroPDF axAcroPDF1;
        public string FileLocation { get; set; }

        WinFormPdfHost win = new WinFormPdfHost();
        public PDFLib()
        {
           // this.axAcroPDF1 = new AxAcroPDFLib.AxAcroPDF();
           // this.axAcroPDF1 = new AxAcroPDFLib.AxAcroPDF();
           // ((System.ComponentModel.ISupportInitialize)(this.axAcroPDF1)).BeginInit();
           //// this.SuspendLayout();
      
           // this.axAcroPDF1.Dock = System.Windows.Forms.DockStyle.Fill;
           // this.axAcroPDF1.Enabled = true;
           // this.axAcroPDF1.Location = new System.Drawing.Point(0, 0);
           // this.axAcroPDF1.Name = "axAcroPDF1";
           // this.axAcroPDF1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axAcroPDF1.OcxState")));
           // this.axAcroPDF1.Size = new System.Drawing.Size(150, 150);
           // this.axAcroPDF1.TabIndex = 0;
           // // 
           // // WinFormPdfHost
           // // 
           // //this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
           // //this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
           // //this.Controls.Add(this.axAcroPDF1);
           // //this.Name = "WinFormPdfHost";
           // ((System.ComponentModel.ISupportInitialize)(this.axAcroPDF1)).EndInit();
           //// this.ResumeLayout(false);
           // //this.axAcroPDF1.BeginInit();

        }

        public void PrintDirectly(byte[] fileBytes,bool showPrintDialog=false,System.Drawing.Printing.PrinterSettings settings = null)
        {
            try
            {
                FileLocation = Path.GetTempFileName();
                File.WriteAllBytes(FileLocation, fileBytes);
                LoadFile(FileLocation);
                Print(showPrintDialog);
            }
            catch (Exception ex)
            {
            }
            finally
            {

                File.Delete(FileLocation);
            }
        }

        public void LoadFile(string path)
        {

            win.LoadFile(path);
            //win.src = path;
            //win.setViewScroll("FitH", 0);
        }

        public void Print(bool showDialog=false)
        {
            if (showDialog)
                win.PrintDialog();
            else
                win.Print();

        }


        //public void SetShowToolBar(bool on)
        //{
        //    axAcroPDF1.setShowToolbar(on);
        //}
    }
}
