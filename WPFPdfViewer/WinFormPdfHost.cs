﻿using System.Windows.Forms;

namespace WPFPdfViewer
{
    public partial class WinFormPdfHost : UserControl
    {

        public WinFormPdfHost()
        {
            InitializeComponent();
            if(!DesignMode)
                axAcroPDF1.setShowToolbar(true);
        }

        public void LoadFile(string path)
        {
            axAcroPDF1.LoadFile(path);
            axAcroPDF1.src = path;
            axAcroPDF1.setViewScroll("FitH", 0);

        }



        public void Print(System.Drawing.Printing.PrinterSettings settings=null,bool showDialog = false)
        {
            if(settings!=null)
            {

            }
            if (showDialog)
                PrintDialog();
            else
                axAcroPDF1.printAll();
        }

        public void PrintDialog()
        {
           axAcroPDF1.printWithDialog();
      
        }


        public void Next()
        {

            axAcroPDF1.gotoNextPage();
        }

        public void First()
        {

            axAcroPDF1.gotoFirstPage();
        }
        public void Previous()
        {

            axAcroPDF1.gotoPreviousPage();
        }
        public void Last()
        {

            axAcroPDF1.gotoLastPage();
        }

        public void GoToThisPage(int pageNo)
        {

            axAcroPDF1.setCurrentPage(pageNo);
        }

        public void SetShowToolBar(bool on)
        {
            
            axAcroPDF1.setShowToolbar(on);
        }
    }
}
