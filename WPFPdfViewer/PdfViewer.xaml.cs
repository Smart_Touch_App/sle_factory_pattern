﻿using System.Windows.Controls;

namespace WPFPdfViewer
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class PdfViewer : UserControl
    {
        public PdfViewer()
        {
            InitializeComponent();
            _winFormPdfHost = pdfHost.Child as WinFormPdfHost;
        }



        public string PdfPath
        {
            get { return _pdfPath; }
            set
            {
                _pdfPath = value;
                LoadFile(_pdfPath);
            }
        }

        public bool ShowToolBar
        {
            get { return _showToolBar; }
            set
            {
                _showToolBar = value;
                _winFormPdfHost.SetShowToolBar(_showToolBar);
                
            }
        }


        public void Next()
        {

            _winFormPdfHost.Next();
        }

        public void First()
        {

            _winFormPdfHost.First();
        }
        public void Previous()
        {

            _winFormPdfHost.Next();
        }
        public void Last()
        {

            _winFormPdfHost.Last();
        }

        public void GoToThisPage(int pageNo)
        {

            _winFormPdfHost.GoToThisPage(pageNo);
        }

        public void SetShowToolBar(bool on)
        {                                             

            _winFormPdfHost.SetShowToolBar(on);
        }

        //public void Print()
        //{
        //    _winFormPdfHost.Print();
        //}


        public void Print(bool showDialog=false)
        {
            if (showDialog)
                _winFormPdfHost.PrintDialog();
            else
                _winFormPdfHost.Print();
        }

        public void LoadFile(string path)
        {
            _pdfPath = path;
            _winFormPdfHost.LoadFile(path);
        }

        public void Dispose()
        {

            _winFormPdfHost.Dispose();
        }
        private string _pdfPath;
        private bool _showToolBar;
        private readonly WinFormPdfHost _winFormPdfHost;
    }
}
