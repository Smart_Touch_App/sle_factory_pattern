namespace SalesLogicExpress.Reporting
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for Invoice1.
    /// </summary>
    public partial class ItemReturns : Telerik.Reporting.Report
    {
        public ItemReturns()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();


            // textBox39.Value = (string)(decimal.Parse(textBox33.Value) + decimal.Parse(textBox29.Value) + decimal.Parse(textBox36.Value)).ToString();
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        private void table1_ItemDataBinding(object sender, EventArgs e)
        {

        }

        private void textBox11_ItemDataBinding(object sender, EventArgs e)
        {
            Telerik.Reporting.Processing.TextBox txt = (Telerik.Reporting.Processing.TextBox)sender;
            txt.Value = Convert.ToDecimal(txt.DataObject["UNITPRICE"]).ToString("F2");
        }

        private void textBox12_ItemDataBinding(object sender, EventArgs e)
        {
            Telerik.Reporting.Processing.TextBox txt = (Telerik.Reporting.Processing.TextBox)sender;
            txt.Value = (-1 * Convert.ToDecimal(txt.DataObject["ExtendedPrice"])).ToString("F2");
        }

        private void detail_ItemDataBound(object sender, EventArgs e)
        {
            Telerik.Reporting.Processing.DetailSection section = (sender as Telerik.Reporting.Processing.DetailSection);

            if (section.DataObject["EnergySurcharge"] == null || Convert.ToDecimal(section.DataObject["EnergySurcharge"].ToString()) == 0)
            {
                lbl_Surcharge.Visible = false;
                fld_surcharge.Visible = false;
            }

            if (section.DataObject["TotalAllied"] == null || Convert.ToDecimal(section.DataObject["TotalAllied"].ToString()) == 0)
            {
                lbl_TotalAllied.Visible = false;
                fld_TotalAllied.Visible = false;
            }
            if (section.DataObject["TaxAmount"] == null || Convert.ToDecimal(section.DataObject["TaxAmount"].ToString()) == 0)
            {
                lbl_TaxAmount.Visible = false;
                fld_TaxAmount.Visible = false;
            }
            if (section.DataObject["TotalCoffee"] == null || Convert.ToDecimal(section.DataObject["TotalCoffee"].ToString()) == 0)
            {
                lbl_TotalCoffee.Visible = false;
                fld_TotalCoffee.Visible = false;
            }

        }
    }
}