namespace SalesLogicExpress.Reporting
{
    partial class ItemReturns
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector1 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector2 = new Telerik.Reporting.Drawing.DescendantSelector();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.txt_InvoiceNo = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.shape1 = new Telerik.Reporting.Shape();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.lbl_TotalAllied = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.lbl_TaxAmount = new Telerik.Reporting.TextBox();
            this.table7 = new Telerik.Reporting.Table();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.lbl_TotalCoffee = new Telerik.Reporting.TextBox();
            this.table5 = new Telerik.Reporting.Table();
            this.lbl_Surcharge = new Telerik.Reporting.TextBox();
            this.table8 = new Telerik.Reporting.Table();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.fld_TotalAllied = new Telerik.Reporting.TextBox();
            this.fld_TotalCoffee = new Telerik.Reporting.TextBox();
            this.fld_surcharge = new Telerik.Reporting.TextBox();
            this.fld_TaxAmount = new Telerik.Reporting.TextBox();
            this.pictureBox3 = new Telerik.Reporting.PictureBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.reportFooterSection1 = new Telerik.Reporting.ReportFooterSection();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9147578477859497D), Telerik.Reporting.Drawing.Unit.Cm(0.79104161262512207D));
            this.textBox8.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Courier New";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox8.StyleName = "Normal.TableHeader";
            this.textBox8.Value = "QUANTITY";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(73.7499771118164D), Telerik.Reporting.Drawing.Unit.Cm(0.79104161262512207D));
            this.textBox29.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox29.StyleName = "Normal.TableHeader";
            this.textBox29.Value = "Order No.";
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9806321859359741D), Telerik.Reporting.Drawing.Unit.Cm(0.79104167222976685D));
            this.textBox50.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox50.Style.Font.Bold = true;
            this.textBox50.Style.Font.Name = "Courier New";
            this.textBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox50.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox50.StyleName = "Normal.TableHeader";
            this.textBox50.Value = "CODE";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0198750495910645D), Telerik.Reporting.Drawing.Unit.Cm(0.79104161262512207D));
            this.textBox4.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox4.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox4.Style.Color = System.Drawing.Color.Black;
            this.textBox4.Style.Font.Name = "Courier New";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.StyleName = "Corporate.TableHeader";
            this.textBox4.Value = "PRODUCT DESCRIPTION";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(100.49995422363281D), Telerik.Reporting.Drawing.Unit.Cm(0.79104161262512207D));
            this.textBox35.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox35.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox35.Style.Color = System.Drawing.Color.Black;
            this.textBox35.Style.Font.Name = "Courier New";
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox35.StyleName = "Corporate.TableHeader";
            this.textBox35.Value = "Tax Amt";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5155491828918457D), Telerik.Reporting.Drawing.Unit.Cm(0.79104155302047729D));
            this.textBox5.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox5.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox5.Style.Color = System.Drawing.Color.Black;
            this.textBox5.Style.Font.Name = "Courier New";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox5.StyleName = "Corporate.TableHeader";
            this.textBox5.Value = "U/PRICE";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.080150842666626D), Telerik.Reporting.Drawing.Unit.Cm(0.79104161262512207D));
            this.textBox6.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox6.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox6.Style.Color = System.Drawing.Color.Black;
            this.textBox6.Style.Font.Name = "Courier New";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox6.StyleName = "Corporate.TableHeader";
            this.textBox6.Value = "AMOUNT";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Pixel(236.22993469238281D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.textBox20,
            this.textBox19,
            this.txt_InvoiceNo,
            this.textBox23,
            this.textBox25,
            this.textBox37,
            this.textBox40,
            this.textBox43,
            this.textBox44,
            this.textBox45,
            this.textBox46,
            this.textBox47,
            this.textBox42,
            this.textBox31,
            this.textBox52,
            this.textBox65});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox13
            // 
            this.textBox13.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.76199996471405029D), Telerik.Reporting.Drawing.Unit.Cm(3D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3999999761581421D), Telerik.Reporting.Drawing.Unit.Cm(0.29999998211860657D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Courier New";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox13.Value = "BRANCH";
            // 
            // textBox14
            // 
            this.textBox14.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.1620004177093506D), Telerik.Reporting.Drawing.Unit.Cm(3.0000002384185791D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.79960024356842041D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox14.Value = "RT.";
            // 
            // textBox15
            // 
            this.textBox15.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.76200008392334D), Telerik.Reporting.Drawing.Unit.Cm(3.0000004768371582D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6000020503997803D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox15.Value = "ACCOUNT/BILLING";
            // 
            // textBox16
            // 
            this.textBox16.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.699999809265137D), Telerik.Reporting.Drawing.Unit.Cm(3.0000002384185791D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.499800443649292D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Value = "INVOICE";
            // 
            // textBox17
            // 
            this.textBox17.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.700000762939453D), Telerik.Reporting.Drawing.Unit.Cm(4.3000001907348633D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0997995138168335D), Telerik.Reporting.Drawing.Unit.Cm(0.30020010471343994D));
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox17.Value = "DATE";
            // 
            // textBox18
            // 
            this.textBox18.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.76199996471405029D), Telerik.Reporting.Drawing.Unit.Cm(3.4000000953674316D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5000004768371582D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.textBox18.Value = "CALL FOR SERVICE";
            // 
            // textBox20
            // 
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.6620001792907715D), Telerik.Reporting.Drawing.Unit.Cm(4.3000001907348633D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0000009536743164D), Telerik.Reporting.Drawing.Unit.Cm(0.30020034313201904D));
            this.textBox20.Value = "=Fields.Customer.Address";
            // 
            // textBox19
            // 
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.6619999408721924D), Telerik.Reporting.Drawing.Unit.Cm(3.9000003337860107D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0000009536743164D), Telerik.Reporting.Drawing.Unit.Cm(0.30020016431808472D));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Value = "=Fields.Customer.Name";
            // 
            // txt_InvoiceNo
            // 
            this.txt_InvoiceNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.699999809265137D), Telerik.Reporting.Drawing.Unit.Cm(3.4000000953674316D));
            this.txt_InvoiceNo.Name = "txt_InvoiceNo";
            this.txt_InvoiceNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3971030712127686D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.txt_InvoiceNo.Value = "=Fields.OrderNumber + \' CO\'";
            // 
            // textBox23
            // 
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.1620001792907715D), Telerik.Reporting.Drawing.Unit.Cm(3.0000002384185791D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.9211544394493103D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.textBox23.Value = "=Fields.Customer.RouteBranch";
            // 
            // textBox25
            // 
            this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.9619998931884766D), Telerik.Reporting.Drawing.Unit.Cm(3.0000002384185791D));
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9982004165649414D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.textBox25.Value = "=Fields.Route";
            // 
            // textBox37
            // 
            this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.7406463623046875D), Telerik.Reporting.Drawing.Unit.Cm(3.4000000953674316D));
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5998011827468872D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.textBox37.Value = "=Fields.Customer.CustomerNo";
            // 
            // textBox40
            // 
            this.textBox40.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.223999977111816D), Telerik.Reporting.Drawing.Unit.Cm(4.3000001907348633D));
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(0.30020037293434143D));
            this.textBox40.Value = "=Fields.OrderDate";
            // 
            // textBox43
            // 
            this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.64064884185791D), Telerik.Reporting.Drawing.Unit.Cm(3.4000000953674316D));
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.699999213218689D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.textBox43.Value = "=Fields.Billing";
            // 
            // textBox44
            // 
            this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.3406467437744141D), Telerik.Reporting.Drawing.Unit.Cm(3.4000000953674316D));
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.29979977011680603D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.textBox44.Value = "/";
            // 
            // textBox45
            // 
            this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.2622003555297852D), Telerik.Reporting.Drawing.Unit.Cm(3.3997995853424072D));
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7701973915100098D), Telerik.Reporting.Drawing.Unit.Cm(0.30020010471343994D));
            this.textBox45.Value = "(661)942-7827";
            // 
            // textBox46
            // 
            this.textBox46.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.700000762939453D), Telerik.Reporting.Drawing.Unit.Cm(4.7000002861022949D));
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0997995138168335D), Telerik.Reporting.Drawing.Unit.Cm(0.30020010471343994D));
            this.textBox46.Style.Font.Bold = true;
            this.textBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox46.Value = "PAGE";
            // 
            // textBox47
            // 
            this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.223999977111816D), Telerik.Reporting.Drawing.Unit.Cm(4.7000002861022949D));
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3852548599243164D), Telerik.Reporting.Drawing.Unit.Cm(0.30020010471343994D));
            this.textBox47.Style.Font.Bold = true;
            this.textBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox47.Value = "{PageNumber} of {PageCount}";
            // 
            // textBox42
            // 
            this.textBox42.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox42.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.700000762939453D), Telerik.Reporting.Drawing.Unit.Cm(3.90000057220459D));
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0997990369796753D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.textBox42.Value = "PO #";
            // 
            // textBox31
            // 
            this.textBox31.CanShrink = true;
            this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.6619999408721924D), Telerik.Reporting.Drawing.Unit.Cm(4.7000002861022949D));
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0002012252807617D), Telerik.Reporting.Drawing.Unit.Cm(0.29999998211860657D));
            this.textBox31.Style.Font.Bold = true;
            this.textBox31.Style.Font.Name = "Courier New";
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox31.TextWrap = false;
            this.textBox31.Value = "=Fields.Customer.City + \' \'+ Fields.Customer.State +\' \'+ Fields.Customer.Zip";
            // 
            // textBox52
            // 
            this.textBox52.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox52.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.339352607727051D), Telerik.Reporting.Drawing.Unit.Cm(3.3999998569488525D));
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4314041137695313D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.textBox52.Style.Font.Bold = true;
            this.textBox52.Value = "Status";
            // 
            // textBox65
            // 
            this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.223999977111816D), Telerik.Reporting.Drawing.Unit.Cm(3.9000000953674316D));
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6865005493164063D), Telerik.Reporting.Drawing.Unit.Cm(0.29979813098907471D));
            this.textBox65.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox65.Value = "=Fields.CustomerPo";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Pixel(354.544921875D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1,
            this.table3,
            this.table2,
            this.table6,
            this.table7,
            this.table4,
            this.table5,
            this.table8,
            this.fld_TotalAllied,
            this.fld_TotalCoffee,
            this.fld_surcharge,
            this.fld_TaxAmount,
            this.pictureBox3,
            this.textBox9,
            this.textBox24,
            this.textBox27,
            this.textBox33,
            this.textBox39});
            this.detail.Name = "detail";
            this.detail.ItemDataBound += new System.EventHandler(this.detail_ItemDataBound);
            // 
            // table1
            // 
            this.table1.Anchoring = Telerik.Reporting.AnchoringStyles.Top;
            this.table1.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Items"));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(11.951679229736328D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(7.1959047317504883D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(15.61041259765625D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(19.806327819824219D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(50.198753356933594D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(21.272489547729492D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(15.155487060546875D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(20.801515579223633D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19685021042823792D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.10416657477617264D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.14583346247673035D)));
            this.table1.Body.SetCellContent(0, 4, this.textBox10);
            this.table1.Body.SetCellContent(0, 6, this.textBox11);
            this.table1.Body.SetCellContent(0, 7, this.textBox12);
            this.table1.Body.SetCellContent(0, 0, this.textBox48);
            this.table1.Body.SetCellContent(0, 1, this.textBox7);
            this.table1.Body.SetCellContent(1, 3, this.textBox53);
            this.table1.Body.SetCellContent(1, 4, this.textBox55);
            this.table1.Body.SetCellContent(1, 6, this.textBox56);
            this.table1.Body.SetCellContent(1, 7, this.textBox57);
            this.table1.Body.SetCellContent(1, 0, this.shape1, 1, 2);
            this.table1.Body.SetCellContent(2, 4, this.textBox59);
            this.table1.Body.SetCellContent(2, 6, this.textBox60);
            this.table1.Body.SetCellContent(2, 7, this.textBox61);
            this.table1.Body.SetCellContent(2, 0, this.textBox34, 1, 2);
            this.table1.Body.SetCellContent(2, 3, this.textBox1);
            this.table1.Body.SetCellContent(0, 3, this.textBox3);
            this.table1.Body.SetCellContent(1, 2, this.textBox32);
            this.table1.Body.SetCellContent(0, 2, this.textBox36);
            this.table1.Body.SetCellContent(2, 2, this.textBox30);
            this.table1.Body.SetCellContent(1, 5, this.textBox54);
            this.table1.Body.SetCellContent(2, 5, this.textBox58);
            this.table1.Body.SetCellContent(0, 5, this.textBox62);
            tableGroup2.Name = "group5";
            tableGroup3.Name = "group2";
            tableGroup1.ChildGroups.Add(tableGroup2);
            tableGroup1.ChildGroups.Add(tableGroup3);
            tableGroup1.ReportItem = this.textBox8;
            tableGroup5.Name = "group8";
            tableGroup4.ChildGroups.Add(tableGroup5);
            tableGroup4.Name = "group4";
            tableGroup4.ReportItem = this.textBox29;
            tableGroup6.ReportItem = this.textBox50;
            tableGroup7.ReportItem = this.textBox4;
            tableGroup8.Name = "group9";
            tableGroup8.ReportItem = this.textBox35;
            tableGroup9.ReportItem = this.textBox5;
            tableGroup10.ReportItem = this.textBox6;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.ColumnGroups.Add(tableGroup6);
            this.table1.ColumnGroups.Add(tableGroup7);
            this.table1.ColumnGroups.Add(tableGroup8);
            this.table1.ColumnGroups.Add(tableGroup9);
            this.table1.ColumnGroups.Add(tableGroup10);
            this.table1.ColumnHeadersPrintOnEveryPage = true;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox48,
            this.textBox7,
            this.textBox53,
            this.textBox55,
            this.textBox56,
            this.textBox57,
            this.shape1,
            this.textBox59,
            this.textBox60,
            this.textBox61,
            this.textBox34,
            this.textBox1,
            this.textBox3,
            this.textBox32,
            this.textBox36,
            this.textBox30,
            this.textBox54,
            this.textBox58,
            this.textBox62,
            this.textBox8,
            this.textBox29,
            this.textBox50,
            this.textBox4,
            this.textBox35,
            this.textBox5,
            this.textBox6});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5079999566078186D), Telerik.Reporting.Drawing.Unit.Inch(-4.9670536128587628E-08D));
            this.table1.Name = "table1";
            tableGroup11.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup11.Name = "Detail";
            tableGroup12.Name = "group6";
            tableGroup13.Name = "group7";
            this.table1.RowGroups.Add(tableGroup11);
            this.table1.RowGroups.Add(tableGroup12);
            this.table1.RowGroups.Add(tableGroup13);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(161.99256896972656D), Telerik.Reporting.Drawing.Unit.Inch(0.75828397274017334D));
            this.table1.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.table1.Style.LineColor = System.Drawing.Color.Transparent;
            this.table1.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.table1.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.table1.StyleName = "";
            this.table1.ItemDataBinding += new System.EventHandler(this.table1_ItemDataBinding);
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0198755264282227D), Telerik.Reporting.Drawing.Unit.Cm(0.4999995231628418D));
            this.textBox10.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox10.Style.Font.Name = "Courier New";
            this.textBox10.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox10.StyleName = "Corporate.TableBody";
            this.textBox10.Value = "= Fields.ItemDescription";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5155491828918457D), Telerik.Reporting.Drawing.Unit.Cm(0.49999961256980896D));
            this.textBox11.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox11.Style.Font.Name = "Courier New";
            this.textBox11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox11.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox11.StyleName = "Corporate.TableBody";
            this.textBox11.Value = "= Fields.UnitPrice";
            this.textBox11.ItemDataBinding += new System.EventHandler(this.textBox11_ItemDataBinding);
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.080150842666626D), Telerik.Reporting.Drawing.Unit.Cm(0.49999958276748657D));
            this.textBox12.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox12.Style.Font.Name = "Courier New";
            this.textBox12.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox12.StyleName = "Corporate.TableBody";
            this.textBox12.Value = "=(-1 * Fields.ExtendedPrice)";
            this.textBox12.ItemDataBinding += new System.EventHandler(this.textBox12_ItemDataBinding);
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1951669454574585D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox48.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox48.Style.Font.Bold = true;
            this.textBox48.Style.Font.Name = "Courier New";
            this.textBox48.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox48.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox48.StyleName = "Normal.TableBody";
            this.textBox48.Value = "= (-1 * Fields.OrderQty)";
            // 
            // textBox7
            // 
            this.textBox7.CanShrink = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(33.996402740478516D), Telerik.Reporting.Drawing.Unit.Cm(0.49999967217445374D));
            this.textBox7.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Courier New";
            this.textBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox7.StyleName = "Normal.TableBody";
            this.textBox7.Value = "=Fields.UM";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9806325435638428D), Telerik.Reporting.Drawing.Unit.Pixel(12.499985694885254D));
            this.textBox53.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox53.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox53.Style.Font.Name = "Courier New";
            this.textBox53.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox53.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox53.StyleName = "Corporate.TableBody";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0198755264282227D), Telerik.Reporting.Drawing.Unit.Pixel(12.49998950958252D));
            this.textBox55.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox55.Style.Font.Name = "Courier New";
            this.textBox55.StyleName = "Corporate.TableBody";
            // 
            // textBox56
            // 
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5155491828918457D), Telerik.Reporting.Drawing.Unit.Pixel(12.499985694885254D));
            this.textBox56.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox56.Style.Font.Name = "Courier New";
            this.textBox56.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox56.StyleName = "Corporate.TableBody";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.080150842666626D), Telerik.Reporting.Drawing.Unit.Pixel(12.49998664855957D));
            this.textBox57.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox57.Style.Font.Name = "Courier New";
            this.textBox57.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox57.StyleName = "Corporate.TableBody";
            // 
            // shape1
            // 
            this.shape1.Name = "shape1";
            this.shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9147582054138184D), Telerik.Reporting.Drawing.Unit.Pixel(12.499985694885254D));
            this.shape1.Style.BorderColor.Default = System.Drawing.Color.White;
            this.shape1.Style.Font.Bold = true;
            this.shape1.Style.Font.Name = "Courier New";
            this.shape1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.shape1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.shape1.StyleName = "Normal.TableBody";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0198755264282227D), Telerik.Reporting.Drawing.Unit.Pixel(17.500015258789062D));
            this.textBox59.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox59.Style.Font.Name = "Courier New";
            this.textBox59.StyleName = "Corporate.TableBody";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5155491828918457D), Telerik.Reporting.Drawing.Unit.Pixel(17.500017166137695D));
            this.textBox60.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox60.Style.Font.Name = "Courier New";
            this.textBox60.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox60.StyleName = "Corporate.TableBody";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.080150842666626D), Telerik.Reporting.Drawing.Unit.Pixel(17.500017166137695D));
            this.textBox61.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox61.Style.Font.Name = "Courier New";
            this.textBox61.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox61.StyleName = "Corporate.TableBody";
            // 
            // textBox34
            // 
            this.textBox34.CanShrink = true;
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9147582054138184D), Telerik.Reporting.Drawing.Unit.Pixel(17.500017166137695D));
            this.textBox34.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.Font.Name = "Courier New";
            this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox34.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox34.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox34.StyleName = "Normal.TableBody";
            this.textBox34.TextWrap = false;
            this.textBox34.Value = "=(-1*Sum(Fields.OrderQty))";
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9806325435638428D), Telerik.Reporting.Drawing.Unit.Pixel(17.500017166137695D));
            this.textBox1.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Courier New";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox1.StyleName = "Normal.TableBody";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9806321859359741D), Telerik.Reporting.Drawing.Unit.Cm(0.49999970197677612D));
            this.textBox3.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Courier New";
            this.textBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox3.StyleName = "Normal.TableBody";
            this.textBox3.Value = "=Fields.ItemNumber";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(73.7499771118164D), Telerik.Reporting.Drawing.Unit.Cm(0.2645830512046814D));
            this.textBox32.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox32.Style.Font.Bold = true;
            this.textBox32.Style.Font.Name = "Courier New";
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox32.StyleName = "Normal.TableBody";
            // 
            // textBox36
            // 
            this.textBox36.CanShrink = true;
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(73.7499771118164D), Telerik.Reporting.Drawing.Unit.Cm(0.49999967217445374D));
            this.textBox36.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox36.Style.Font.Bold = true;
            this.textBox36.Style.Font.Name = "Courier New";
            this.textBox36.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox36.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox36.StyleName = "Normal.TableBody";
            this.textBox36.Value = "=Fields.OrderID";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(73.7499771118164D), Telerik.Reporting.Drawing.Unit.Cm(0.3704170286655426D));
            this.textBox30.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox30.Style.Font.Bold = true;
            this.textBox30.Style.Font.Name = "Courier New";
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox30.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox30.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox30.StyleName = "Normal.TableBody";
            this.textBox30.Value = "TOTAL";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(100.49995422363281D), Telerik.Reporting.Drawing.Unit.Cm(0.26458311080932617D));
            this.textBox54.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox54.Style.Font.Name = "Courier New";
            this.textBox54.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox54.StyleName = "Corporate.TableBody";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(100.49995422363281D), Telerik.Reporting.Drawing.Unit.Cm(0.37041699886322021D));
            this.textBox58.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox58.Style.Font.Name = "Courier New";
            this.textBox58.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox58.StyleName = "Corporate.TableBody";
            // 
            // textBox62
            // 
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(100.49995422363281D), Telerik.Reporting.Drawing.Unit.Cm(0.4999995231628418D));
            this.textBox62.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox62.Style.Font.Bold = true;
            this.textBox62.Style.Font.Name = "Courier New";
            this.textBox62.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox62.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox62.StyleName = "Normal.TableBody";
            this.textBox62.Value = "=\'(\'+ Fields.TaxAmount + \')\'";
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0864591598510742D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.32305383682250977D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table3.Body.SetCellContent(1, 0, this.textBox22);
            this.table3.Body.SetCellContent(0, 0, this.textBox28);
            tableGroup14.Name = "tableGroup1";
            this.table3.ColumnGroups.Add(tableGroup14);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox22,
            this.textBox28});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.305354118347168D), Telerik.Reporting.Drawing.Unit.Cm(2.2999999523162842D));
            this.table3.Name = "table3";
            tableGroup16.Name = "group";
            tableGroup17.Name = "group1";
            tableGroup15.ChildGroups.Add(tableGroup16);
            tableGroup15.ChildGroups.Add(tableGroup17);
            tableGroup15.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup15.Name = "detailTableGroup";
            this.table3.RowGroups.Add(tableGroup15);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0864591598510742D), Telerik.Reporting.Drawing.Unit.Cm(0.82305389642715454D));
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0864591598510742D), Telerik.Reporting.Drawing.Unit.Pixel(23.622051239013672D));
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox22.StyleName = "";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0864589214324951D), Telerik.Reporting.Drawing.Unit.Cm(0.32305383682250977D));
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox28.Value = "TERMS OF SALES:";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.8277697563171387D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.29999998211860657D)));
            this.table2.Body.SetCellContent(0, 0, this.lbl_TotalAllied);
            tableGroup18.Name = "tableGroup3";
            this.table2.ColumnGroups.Add(tableGroup18);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.lbl_TotalAllied});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.3378982543945312D), Telerik.Reporting.Drawing.Unit.Cm(3.5000009536743164D));
            this.table2.Name = "table2";
            tableGroup19.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup19.Name = "detailTableGroup1";
            this.table2.RowGroups.Add(tableGroup19);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8277697563171387D), Telerik.Reporting.Drawing.Unit.Cm(0.29999998211860657D));
            // 
            // lbl_TotalAllied
            // 
            this.lbl_TotalAllied.Name = "lbl_TotalAllied";
            this.lbl_TotalAllied.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8277697563171387D), Telerik.Reporting.Drawing.Unit.Cm(0.29999998211860657D));
            this.lbl_TotalAllied.Style.Font.Bold = true;
            this.lbl_TotalAllied.Value = "TOTAL ALLIED";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.899998664855957D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.29939654469490051D)));
            this.table6.Body.SetCellContent(0, 0, this.lbl_TaxAmount);
            tableGroup20.Name = "tableGroup3";
            this.table6.ColumnGroups.Add(tableGroup20);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.lbl_TaxAmount});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.33799934387207D), Telerik.Reporting.Drawing.Unit.Cm(4.7000012397766113D));
            this.table6.Name = "table6";
            tableGroup21.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup21.Name = "detailTableGroup1";
            this.table6.RowGroups.Add(tableGroup21);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.899998664855957D), Telerik.Reporting.Drawing.Unit.Cm(0.29939654469490051D));
            // 
            // lbl_TaxAmount
            // 
            this.lbl_TaxAmount.Name = "lbl_TaxAmount";
            this.lbl_TaxAmount.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.899998664855957D), Telerik.Reporting.Drawing.Unit.Cm(0.29939654469490051D));
            this.lbl_TaxAmount.Style.Font.Bold = true;
            this.lbl_TaxAmount.Value = "TAX AMOUNT";
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.5425100326538086D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.29919630289077759D)));
            this.table7.Body.SetCellContent(0, 0, this.textBox38);
            tableGroup22.Name = "tableGroup3";
            this.table7.ColumnGroups.Add(tableGroup22);
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox38});
            this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.7380008697509766D), Telerik.Reporting.Drawing.Unit.Cm(5.1000008583068848D));
            this.table7.Name = "table7";
            tableGroup23.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup23.Name = "detailTableGroup1";
            this.table7.RowGroups.Add(tableGroup23);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5425100326538086D), Telerik.Reporting.Drawing.Unit.Cm(0.29919630289077759D));
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5425100326538086D), Telerik.Reporting.Drawing.Unit.Cm(0.29919630289077759D));
            this.textBox38.Style.Font.Bold = true;
            this.textBox38.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox38.Value = "TOTAL DUE -------->>";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.8999991416931152D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.29979813098907471D)));
            this.table4.Body.SetCellContent(0, 0, this.lbl_TotalCoffee);
            tableGroup24.Name = "tableGroup3";
            this.table4.ColumnGroups.Add(tableGroup24);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.lbl_TotalCoffee});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.33799934387207D), Telerik.Reporting.Drawing.Unit.Cm(3.8995990753173828D));
            this.table4.Name = "table4";
            tableGroup25.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup25.Name = "detailTableGroup1";
            this.table4.RowGroups.Add(tableGroup25);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8999991416931152D), Telerik.Reporting.Drawing.Unit.Cm(0.29979813098907471D));
            // 
            // lbl_TotalCoffee
            // 
            this.lbl_TotalCoffee.Name = "lbl_TotalCoffee";
            this.lbl_TotalCoffee.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8999991416931152D), Telerik.Reporting.Drawing.Unit.Cm(0.29979813098907471D));
            this.lbl_TotalCoffee.Style.Font.Bold = true;
            this.lbl_TotalCoffee.Value = "TOTAL COFFEE";
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.9000000953674316D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.29959678649902344D)));
            this.table5.Body.SetCellContent(0, 0, this.lbl_Surcharge);
            tableGroup26.Name = "tableGroup3";
            this.table5.ColumnGroups.Add(tableGroup26);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.lbl_Surcharge});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.33799934387207D), Telerik.Reporting.Drawing.Unit.Cm(4.30000114440918D));
            this.table5.Name = "table5";
            tableGroup27.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup27.Name = "detailTableGroup1";
            this.table5.RowGroups.Add(tableGroup27);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.29959678649902344D));
            // 
            // lbl_Surcharge
            // 
            this.lbl_Surcharge.Name = "lbl_Surcharge";
            this.lbl_Surcharge.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.29959678649902344D));
            this.lbl_Surcharge.Style.Font.Bold = true;
            this.lbl_Surcharge.Value = "SURCHARGE";
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(7D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.26458334922790527D)));
            this.table8.Body.SetCellContent(0, 0, this.textBox41);
            tableGroup28.Name = "tableGroup3";
            this.table8.ColumnGroups.Add(tableGroup28);
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox41});
            this.table8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.0786454677581787D), Telerik.Reporting.Drawing.Unit.Cm(6.0000009536743164D));
            this.table8.Name = "table8";
            tableGroup30.Name = "group3";
            tableGroup29.ChildGroups.Add(tableGroup30);
            tableGroup29.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup29.Name = "detailTableGroup1";
            this.table8.RowGroups.Add(tableGroup29);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Cm(0.26458334922790527D));
            // 
            // textBox41
            // 
            this.textBox41.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Cm(0.26458334922790527D));
            this.textBox41.Style.Font.Bold = true;
            this.textBox41.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox41.Value = "REMIT TO : ";
            // 
            // fld_TotalAllied
            // 
            this.fld_TotalAllied.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.16586971282959D), Telerik.Reporting.Drawing.Unit.Cm(3.5000004768371582D));
            this.fld_TotalAllied.Name = "fld_TotalAllied";
            this.fld_TotalAllied.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.250575065612793D), Telerik.Reporting.Drawing.Unit.Cm(0.29999998211860657D));
            this.fld_TotalAllied.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.fld_TotalAllied.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.fld_TotalAllied.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.fld_TotalAllied.Value = "=Fields.TotalAllied";
            // 
            // fld_TotalCoffee
            // 
            this.fld_TotalCoffee.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.238199234008789D), Telerik.Reporting.Drawing.Unit.Cm(3.8999996185302734D));
            this.fld_TotalCoffee.Name = "fld_TotalCoffee";
            this.fld_TotalCoffee.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1782450675964355D), Telerik.Reporting.Drawing.Unit.Cm(0.29979813098907471D));
            this.fld_TotalCoffee.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.fld_TotalCoffee.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.fld_TotalCoffee.Value = "=Fields.TotalCoffee";
            // 
            // fld_surcharge
            // 
            this.fld_surcharge.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.238199234008789D), Telerik.Reporting.Drawing.Unit.Cm(4.3000001907348633D));
            this.fld_surcharge.Name = "fld_surcharge";
            this.fld_surcharge.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1782426834106445D), Telerik.Reporting.Drawing.Unit.Cm(0.29959678649902344D));
            this.fld_surcharge.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.fld_surcharge.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.fld_surcharge.Value = "=Fields.EnergySurcharge";
            // 
            // fld_TaxAmount
            // 
            this.fld_TaxAmount.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.237998962402344D), Telerik.Reporting.Drawing.Unit.Cm(4.7000002861022949D));
            this.fld_TaxAmount.Name = "fld_TaxAmount";
            this.fld_TaxAmount.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1784439086914062D), Telerik.Reporting.Drawing.Unit.Cm(0.29939654469490051D));
            this.fld_TaxAmount.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.fld_TaxAmount.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.fld_TaxAmount.Value = "=Fields.TaxAmount";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Bindings.Add(new Telerik.Reporting.Binding("Value", "=Fields.LogoUrl"));
            this.pictureBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(474.23629760742188D), Telerik.Reporting.Drawing.Unit.Pixel(211.653564453125D));
            this.pictureBox3.MimeType = "";
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(138.1544189453125D), Telerik.Reporting.Drawing.Unit.Pixel(102.0472412109375D));
            this.pictureBox3.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox3.Value = "";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.405354499816895D), Telerik.Reporting.Drawing.Unit.Cm(2.2999999523162842D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(254.99996948242188D), Telerik.Reporting.Drawing.Unit.Cm(0.32305383682250977D));
            this.textBox9.StyleName = "";
            this.textBox9.Value = "=Fields.TermOfSales";
            // 
            // textBox24
            // 
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.0999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(6.3000006675720215D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Pixel(14.215662956237793D));
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox24.StyleName = "";
            this.textBox24.Value = "=Fields.RemitToName";
            // 
            // textBox27
            // 
            this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Cm(6.7000002861022949D));
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Pixel(16.000003814697266D));
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox27.StyleName = "";
            this.textBox27.Value = "=Fields.RemitToPOBox";
            // 
            // textBox33
            // 
            this.textBox33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.0999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(7.1235337257385254D));
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Pixel(17.999998092651367D));
            this.textBox33.Style.Font.Bold = true;
            this.textBox33.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox33.StyleName = "";
            this.textBox33.Value = "=Fields.RemitToCityZip";
            // 
            // textBox39
            // 
            this.textBox39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.337998390197754D), Telerik.Reporting.Drawing.Unit.Cm(5.100001335144043D));
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0784449577331543D), Telerik.Reporting.Drawing.Unit.Cm(0.29919630289077759D));
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox39.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox39.Value = "=Fields.TotalDue";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(1.0000003576278687D));
            this.textBox26.Value = "textBox26";
            // 
            // reportFooterSection1
            // 
            this.reportFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Pixel(153.07080078125D);
            this.reportFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox2,
            this.pictureBox1,
            this.textBox51,
            this.textBox49,
            this.textBox2,
            this.textBox21,
            this.textBox63,
            this.textBox64});
            this.reportFooterSection1.Name = "reportFooterSection1";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Bindings.Add(new Telerik.Reporting.Binding("Value", "= Fields.RouteSignImagePath"));
            this.pictureBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.7779999971389771D), Telerik.Reporting.Drawing.Unit.Cm(0.69999825954437256D));
            this.pictureBox2.MimeType = "";
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.43345832824707D), Telerik.Reporting.Drawing.Unit.Cm(1.0158007144927979D));
            this.pictureBox2.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.pictureBox2.Value = "";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Bindings.Add(new Telerik.Reporting.Binding("Value", "=Fields.CustomerSignImagePath"));
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.939035415649414D), Telerik.Reporting.Drawing.Unit.Cm(0.83689171075820923D));
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7997972965240479D), Telerik.Reporting.Drawing.Unit.Cm(1.2001990079879761D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox51
            // 
            this.textBox51.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.937999725341797D), Telerik.Reporting.Drawing.Unit.Cm(2.0372915267944336D));
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6712546348571777D), Telerik.Reporting.Drawing.Unit.Cm(0.39979955554008484D));
            this.textBox51.Style.Font.Bold = true;
            this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox51.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox51.Value = "= Now().ToString(\"MM/dd/yyyy hh:mm:ss \") + Now().ToString(\"tt\").ToLower()";
            // 
            // textBox49
            // 
            this.textBox49.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.9685306549072266D), Telerik.Reporting.Drawing.Unit.Cm(1.7370918989181519D));
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.800001859664917D), Telerik.Reporting.Drawing.Unit.Cm(0.29999944567680359D));
            this.textBox49.Style.Font.Bold = true;
            this.textBox49.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox49.Value = "CUSTOMER";
            // 
            // textBox2
            // 
            this.textBox2.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.97800034284591675D), Telerik.Reporting.Drawing.Unit.Cm(0.95399856567382812D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.79979968070983887D), Telerik.Reporting.Drawing.Unit.Cm(0.29999944567680359D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox2.Value = "REP";
            // 
            // textBox21
            // 
            this.textBox21.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.0786453485488892D), Telerik.Reporting.Drawing.Unit.Cm(0.026458332315087318D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.692110061645508D), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D));
            this.textBox21.Style.Font.Bold = false;
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox21.Value = "=Fields.VisitUsUrl";
            // 
            // textBox63
            // 
            this.textBox63.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(84D), Telerik.Reporting.Drawing.Unit.Pixel(81.07080078125D));
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(221.45474243164063D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox63.Value = "=Fields.DisplayName";
            // 
            // textBox64
            // 
            this.textBox64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(84D), Telerik.Reporting.Drawing.Unit.Pixel(105.07080078125D));
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(221.45474243164063D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox64.Value = "= Now().ToString(\"MM/dd/yyyy hh:mm:ss \") + Now().ToString(\"tt\").ToLower()";
            // 
            // ItemReturns
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.reportFooterSection1});
            this.Name = "Invoice1";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Pixel(22D), Telerik.Reporting.Drawing.Unit.Pixel(0D), Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Pixel(10D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.Name = "PrintLabels";
            reportParameter1.Type = Telerik.Reporting.ReportParameterType.Boolean;
            this.ReportParameters.Add(reportParameter1);
            this.Style.Font.Bold = true;
            this.Style.Font.Name = "Courier New";
            this.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Corporate.TableNormal")});
            styleRule2.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Name = "Tahoma";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Corporate.TableHeader")});
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector1});
            styleRule3.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(58)))), ((int)(((byte)(112)))));
            styleRule3.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule3.Style.Color = System.Drawing.Color.White;
            styleRule3.Style.Font.Name = "Tahoma";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            descendantSelector2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Corporate.TableBody")});
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector2});
            styleRule4.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule4.Style.Font.Name = "Tahoma";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4});
            this.UnitOfMeasure = Telerik.Reporting.Drawing.UnitType.Pixel;
            this.Width = Telerik.Reporting.Drawing.Unit.Pixel(792.3192138671875D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox lbl_TotalAllied;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox lbl_TotalCoffee;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox lbl_Surcharge;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox lbl_TaxAmount;
        private Telerik.Reporting.Table table7;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox txt_InvoiceNo;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.Table table8;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.Shape shape1;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox fld_TotalAllied;
        private Telerik.Reporting.TextBox fld_TotalCoffee;
        private Telerik.Reporting.TextBox fld_surcharge;
        private Telerik.Reporting.TextBox fld_TaxAmount;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.PictureBox pictureBox3;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.ReportFooterSection reportFooterSection1;
        private Telerik.Reporting.PictureBox pictureBox2;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox65;
    }
}