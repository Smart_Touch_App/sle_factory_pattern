namespace SalesLogicExpress.Reporting
{
    partial class AddLoadRequest
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(40.551303863525391D), Telerik.Reporting.Drawing.Unit.Pixel(29.858264923095703D));
            this.textBox29.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Font.Name = "Courier New";
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox29.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox29.StyleName = "Normal.TableHeader";
            this.textBox29.Value = "Item";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(77.551338195800781D), Telerik.Reporting.Drawing.Unit.Pixel(29.858266830444336D));
            this.textBox39.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.Font.Name = "Courier New";
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox39.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox39.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox39.StyleName = "Normal.TableHeader";
            this.textBox39.Value = "Description";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(33.551177978515625D), Telerik.Reporting.Drawing.Unit.Pixel(29.858264923095703D));
            this.textBox25.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox25.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox25.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.Color = System.Drawing.Color.Black;
            this.textBox25.Style.Font.Bold = true;
            this.textBox25.Style.Font.Name = "Courier New";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox25.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox25.StyleName = "Corporate.TableHeader";
            this.textBox25.Value = "Prm\r\nUOM";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(43.999996185302734D), Telerik.Reporting.Drawing.Unit.Pixel(29.858264923095703D));
            this.textBox19.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox19.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox19.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox19.Style.Color = System.Drawing.Color.Black;
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Name = "Courier New";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox19.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox19.StyleName = "Corporate.TableHeader";
            this.textBox19.Value = "OnHand Qty";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(53.000003814697266D), Telerik.Reporting.Drawing.Unit.Pixel(29.858264923095703D));
            this.textBox21.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox21.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox21.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox21.Style.Color = System.Drawing.Color.Black;
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.Font.Name = "Courier New";
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox21.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox21.StyleName = "Corporate.TableHeader";
            this.textBox21.Value = "Committed Qty";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(45.000007629394531D), Telerik.Reporting.Drawing.Unit.Pixel(29.858264923095703D));
            this.textBox27.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox27.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox27.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox27.Style.Color = System.Drawing.Color.Black;
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Font.Name = "Courier New";
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox27.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox27.StyleName = "Corporate.TableHeader";
            this.textBox27.Value = "Held Qty";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(48D), Telerik.Reporting.Drawing.Unit.Pixel(29.858264923095703D));
            this.textBox31.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox31.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox31.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.Color = System.Drawing.Color.Black;
            this.textBox31.Style.Font.Bold = true;
            this.textBox31.Style.Font.Name = "Courier New";
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox31.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox31.StyleName = "Corporate.TableHeader";
            this.textBox31.Value = "Available\r\nQty";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(41.000015258789062D), Telerik.Reporting.Drawing.Unit.Pixel(29.858264923095703D));
            this.textBox33.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox33.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox33.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.Font.Bold = true;
            this.textBox33.Style.Font.Name = "Courier New";
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox33.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox33.StyleName = "Corporate.TableHeader";
            this.textBox33.Value = "Par\r\nLevel";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(44.000038146972656D), Telerik.Reporting.Drawing.Unit.Pixel(29.858264923095703D));
            this.textBox37.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox37.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox37.Style.Font.Bold = true;
            this.textBox37.Style.Font.Name = "Courier New";
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox37.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox37.StyleName = "Corporate.TableHeader";
            this.textBox37.Value = "Load Qty";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(3.3999998569488525D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.textBox2,
            this.textBox4,
            this.textBox7,
            this.textBox8,
            this.textBox5,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox6,
            this.textBox3,
            this.textBox22,
            this.textBox45});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            this.pageHeaderSection1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(240D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(238.1102294921875D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Courier New";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox1.Style.Font.Underline = true;
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Value = "= Fields.ReportTitle";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(420D), Telerik.Reporting.Drawing.Unit.Pixel(34.0157470703125D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(45.346748352050781D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Courier New";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox2.Value = "DATE:";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(465.35430908203125D), Telerik.Reporting.Drawing.Unit.Pixel(34.0157470703125D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(109.60631561279297D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Courier New";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox4.Value = "=Fields.ReportDate";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(36.346672058105469D), Telerik.Reporting.Drawing.Unit.Pixel(52.91339111328125D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(109.60630798339844D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Courier New";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox7.Value = "TRANSFER FROM:";
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(145.95297241210938D), Telerik.Reporting.Drawing.Unit.Pixel(52.91339111328125D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(241.88980102539063D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Courier New";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox8.Value = "=Fields.TransferFrom";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(130.84242248535156D), Telerik.Reporting.Drawing.Unit.Pixel(71.811027526855469D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(257.00033569335938D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Name = "Courier New";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox5.Value = "=Fields.TransferTo";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(36.732864379882812D), Telerik.Reporting.Drawing.Unit.Pixel(71.811027526855469D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.102020263671875D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Courier New";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox9.Value = "TRANSFER TO:";
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(85.48052978515625D), Telerik.Reporting.Drawing.Unit.Pixel(34.0157470703125D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(302.36221313476562D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Courier New";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox10.Value = "=Fields.RouteNoAndName";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(36.346664428710938D), Telerik.Reporting.Drawing.Unit.Pixel(34.0157470703125D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(49.126304626464844D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Name = "Courier New";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox11.Value = "ROUTE:";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(476.70050048828125D), Telerik.Reporting.Drawing.Unit.Pixel(71.811027526855469D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(136.05551147460938D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Courier New";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox12.Value = "=Fields.Status";
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(420D), Telerik.Reporting.Drawing.Unit.Pixel(71.811027526855469D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(56.692962646484375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Courier New";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox13.Value = "STATUS:";
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(524.36187744140625D), Telerik.Reporting.Drawing.Unit.Pixel(90.708663940429688D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.488189697265625D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Name = "Courier New";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox14.Value = "=Fields.UserName";
            // 
            // textBox15
            // 
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(418.7479248046875D), Telerik.Reporting.Drawing.Unit.Pixel(90.708663940429688D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(105.60641479492188D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Name = "Courier New";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox15.Value = "REQUESTED BY:";
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(514.4957275390625D), Telerik.Reporting.Drawing.Unit.Pixel(52.91339111328125D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.488189697265625D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Name = "Courier New";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox6.Value = "=Fields.TransferNo";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(420D), Telerik.Reporting.Drawing.Unit.Pixel(52.91339111328125D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.488189697265625D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Courier New";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox3.Value = "ADD LOAD ID:";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(2D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table2});
            this.detail.Name = "detail";
            // 
            // table2
            // 
            this.table2.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Items"));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(40.551303863525391D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(77.551345825195312D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(33.551185607910156D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(43.999973297119141D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(52.999992370605469D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(45.000022888183594D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(48.000015258789062D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(41.000003814697266D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(44.000034332275391D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20866139233112335D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox23);
            this.table2.Body.SetCellContent(0, 1, this.textBox24);
            this.table2.Body.SetCellContent(0, 2, this.textBox26);
            this.table2.Body.SetCellContent(0, 6, this.textBox32);
            this.table2.Body.SetCellContent(0, 8, this.textBox34);
            this.table2.Body.SetCellContent(0, 7, this.textBox17);
            this.table2.Body.SetCellContent(0, 3, this.textBox18);
            this.table2.Body.SetCellContent(0, 4, this.textBox16);
            this.table2.Body.SetCellContent(0, 5, this.textBox20);
            tableGroup1.Name = "tableGroup";
            tableGroup1.ReportItem = this.textBox29;
            tableGroup2.Name = "tableGroup1";
            tableGroup2.ReportItem = this.textBox39;
            tableGroup3.Name = "tableGroup2";
            tableGroup3.ReportItem = this.textBox25;
            tableGroup4.Name = "group";
            tableGroup4.ReportItem = this.textBox19;
            tableGroup5.Name = "group1";
            tableGroup5.ReportItem = this.textBox21;
            tableGroup6.Name = "group3";
            tableGroup6.ReportItem = this.textBox27;
            tableGroup7.Name = "group9";
            tableGroup7.ReportItem = this.textBox31;
            tableGroup8.Name = "group19";
            tableGroup8.ReportItem = this.textBox33;
            tableGroup9.Name = "group2";
            tableGroup9.ReportItem = this.textBox37;
            this.table2.ColumnGroups.Add(tableGroup1);
            this.table2.ColumnGroups.Add(tableGroup2);
            this.table2.ColumnGroups.Add(tableGroup3);
            this.table2.ColumnGroups.Add(tableGroup4);
            this.table2.ColumnGroups.Add(tableGroup5);
            this.table2.ColumnGroups.Add(tableGroup6);
            this.table2.ColumnGroups.Add(tableGroup7);
            this.table2.ColumnGroups.Add(tableGroup8);
            this.table2.ColumnGroups.Add(tableGroup9);
            this.table2.ColumnHeadersPrintOnEveryPage = true;
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox23,
            this.textBox24,
            this.textBox26,
            this.textBox32,
            this.textBox34,
            this.textBox17,
            this.textBox18,
            this.textBox16,
            this.textBox20,
            this.textBox29,
            this.textBox39,
            this.textBox25,
            this.textBox19,
            this.textBox21,
            this.textBox27,
            this.textBox31,
            this.textBox33,
            this.textBox37});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(39.582412719726562D), Telerik.Reporting.Drawing.Unit.Pixel(18.89764404296875D));
            this.table2.Name = "table2";
            tableGroup10.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup10.Name = "detailTableGroup";
            this.table2.RowGroups.Add(tableGroup10);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(426.65386962890625D), Telerik.Reporting.Drawing.Unit.Inch(0.457480251789093D));
            this.table2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(40.551303863525391D), Telerik.Reporting.Drawing.Unit.Pixel(25.03936767578125D));
            this.textBox23.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox23.Style.Font.Bold = false;
            this.textBox23.Style.Font.Name = "Courier New";
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox23.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox23.StyleName = "Normal.TableBody";
            this.textBox23.Value = "=Fields.ItemNo";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(77.551345825195312D), Telerik.Reporting.Drawing.Unit.Pixel(25.03936767578125D));
            this.textBox24.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox24.Style.Font.Name = "Courier New";
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox24.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox24.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox24.StyleName = "Corporate.TableBody";
            this.textBox24.Value = "= Fields.ItemDescription";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(33.551185607910156D), Telerik.Reporting.Drawing.Unit.Pixel(25.03936767578125D));
            this.textBox26.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox26.Style.Font.Name = "Courier New";
            this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox26.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox26.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox26.StyleName = "Corporate.TableBody";
            this.textBox26.Value = "= Fields.PrimaryUOM";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(48D), Telerik.Reporting.Drawing.Unit.Cm(0.52999997138977051D));
            this.textBox32.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox32.Style.Font.Name = "Courier New";
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox32.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox32.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox32.StyleName = "Corporate.TableBody";
            this.textBox32.Value = "= Fields.AvailableQty";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(44.000038146972656D), Telerik.Reporting.Drawing.Unit.Cm(0.52999997138977051D));
            this.textBox34.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox34.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox34.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.Font.Bold = false;
            this.textBox34.Style.Font.Name = "Courier New";
            this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox34.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox34.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox34.StyleName = "Corporate.TableBody";
            this.textBox34.Value = "= Fields.LoadQuantity +\' \'+Fields.QtyUOM";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(41.000015258789062D), Telerik.Reporting.Drawing.Unit.Cm(0.52999997138977051D));
            this.textBox17.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox17.Style.Font.Name = "Courier New";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox17.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox17.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox17.StyleName = "Corporate.TableBody";
            this.textBox17.Value = "= Fields.ParLevel";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(43.999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(0.52999997138977051D));
            this.textBox18.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox18.Style.Font.Name = "Courier New";
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox18.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox18.StyleName = "Corporate.TableBody";
            this.textBox18.Value = "= Fields.OnHandQty";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(53.000003814697266D), Telerik.Reporting.Drawing.Unit.Cm(0.52999997138977051D));
            this.textBox16.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox16.Style.Font.Name = "Courier New";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox16.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox16.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox16.StyleName = "Corporate.TableBody";
            this.textBox16.Value = "= Fields.CommittedQty";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(45.000007629394531D), Telerik.Reporting.Drawing.Unit.Cm(0.52999997138977051D));
            this.textBox20.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox20.Style.Font.Name = "Courier New";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox20.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox20.StyleName = "Corporate.TableBody";
            this.textBox20.Value = "= Fields.HeldQty";
            // 
            // textBox22
            // 
            this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(524.3619384765625D), Telerik.Reporting.Drawing.Unit.Pixel(120D));
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.4881591796875D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox22.Value = "{PageNumber} of {PageCount}";
            // 
            // textBox45
            // 
            this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(419.3619384765625D), Telerik.Reporting.Drawing.Unit.Pixel(120D));
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(72.944587707519531D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox45.Style.Font.Bold = true;
            this.textBox45.Style.Font.Name = "Courier New";
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox45.Value = "Page";
            // 
            // AddLoadRequest
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail});
            this.Name = "AddLoadRequest";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Pixel(10D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.UnitOfMeasure = Telerik.Reporting.Drawing.UnitType.Pixel;
            this.Width = Telerik.Reporting.Drawing.Unit.Pixel(762D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox45;
    }
}