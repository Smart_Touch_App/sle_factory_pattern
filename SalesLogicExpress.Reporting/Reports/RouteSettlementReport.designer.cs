namespace  SalesLogicExpress.Reporting
{
    partial class RouteSettlementReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector1 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector2 = new Telerik.Reporting.Drawing.DescendantSelector();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.SettlementToDate = new Telerik.Reporting.TextBox();
            this.SettlementFromDate = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.BranchNo = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.RouteNo = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.TotalPaymentsOnAccount = new Telerik.Reporting.TextBox();
            this.CoffeePoundsSold = new Telerik.Reporting.TextBox();
            this.AlliedTotalAmt = new Telerik.Reporting.TextBox();
            this.CoffeeTotalAmt = new Telerik.Reporting.TextBox();
            this.AlliedPoundsSold = new Telerik.Reporting.TextBox();
            this.NetCashSales = new Telerik.Reporting.TextBox();
            this.TotalCashReceived = new Telerik.Reporting.TextBox();
            this.Expenses = new Telerik.Reporting.TextBox();
            this.MoneyOrder = new Telerik.Reporting.TextBox();
            this.Checks = new Telerik.Reporting.TextBox();
            this.BankDeposit = new Telerik.Reporting.TextBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.OrderType = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6502506732940674D), Telerik.Reporting.Drawing.Unit.Cm(1.0149415731430054D));
            this.textBox28.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox28.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox28.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox28.Style.Font.Bold = true;
            this.textBox28.Style.Font.Name = "Courier New";
            this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox28.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox28.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox28.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox28.StyleName = "Normal.TableHeader";
            this.textBox28.Value = "Order#";
            // 
            // textBox44
            // 
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(80.9999771118164D), Telerik.Reporting.Drawing.Unit.Cm(1.0149415731430054D));
            this.textBox44.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox44.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox44.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox44.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox44.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox44.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox44.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox44.StyleName = "Normal.TableHeader";
            this.textBox44.Value = "Date of Transaction";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0704653263092041D), Telerik.Reporting.Drawing.Unit.Cm(1.0149415731430054D));
            this.textBox36.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox36.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox36.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.Font.Bold = true;
            this.textBox36.Style.Font.Name = "Courier New";
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox36.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox36.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox36.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox36.StyleName = "Normal.TableHeader";
            this.textBox36.Value = "Order Type";
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(45.000026702880859D), Telerik.Reporting.Drawing.Unit.Cm(1.0149415731430054D));
            this.textBox41.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox41.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox41.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox41.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox41.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox41.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox41.StyleName = "Normal.TableHeader";
            this.textBox41.Value = "Ship To No";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(59.000049591064453D), Telerik.Reporting.Drawing.Unit.Cm(1.0149415731430054D));
            this.textBox27.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox27.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox27.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Font.Name = "Courier New";
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox27.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox27.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox27.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox27.StyleName = "Normal.TableHeader";
            this.textBox27.Value = "Ship To";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(53.0000114440918D), Telerik.Reporting.Drawing.Unit.Cm(1.0149415731430054D));
            this.textBox42.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox42.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox42.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox42.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox42.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox42.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox42.StyleName = "Normal.TableHeader";
            this.textBox42.Value = "Sold To No";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(72.499954223632812D), Telerik.Reporting.Drawing.Unit.Cm(1.0149415731430054D));
            this.textBox35.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox35.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox35.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox35.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.textBox35.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox35.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox35.StyleName = "Normal.TableHeader";
            this.textBox35.Value = "Customer Payment Terms";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(65.750175476074219D), Telerik.Reporting.Drawing.Unit.Cm(1.0149415731430054D));
            this.textBox38.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox38.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox38.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox38.Style.Font.Bold = true;
            this.textBox38.Style.Font.Name = "Courier New";
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox38.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox38.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox38.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox38.StyleName = "Normal.TableHeader";
            this.textBox38.Value = "Transaction Terms";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(72.000007629394531D), Telerik.Reporting.Drawing.Unit.Cm(1.0149415731430054D));
            this.textBox29.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox29.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox29.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Font.Name = "Courier New";
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox29.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox29.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox29.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox29.StyleName = "Normal.TableHeader";
            this.textBox29.Value = "Time of Transaction";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(48.999828338623047D), Telerik.Reporting.Drawing.Unit.Cm(1.0149415731430054D));
            this.textBox20.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox20.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox20.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.Font.Name = "Courier New";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox20.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox20.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox20.StyleName = "Normal.TableHeader";
            this.textBox20.Value = "Total Amt";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(45.000015258789062D), Telerik.Reporting.Drawing.Unit.Cm(1.0149415731430054D));
            this.textBox37.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox37.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox37.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox37.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox37.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox37.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox37.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox37.StyleName = "Normal.TableHeader";
            this.textBox37.Value = "Delivery day Seq #";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Pixel(48D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(254.8365478515625D), Telerik.Reporting.Drawing.Unit.Pixel(19.199996948242188D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(282.76348876953125D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Courier New";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox1.Style.Font.Underline = true;
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Value = "MOBILE ROUTE SETTLEMENT REPORT";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Pixel(600D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox21,
            this.textBox19,
            this.textBox18,
            this.textBox16,
            this.textBox15,
            this.textBox14,
            this.textBox13,
            this.textBox12,
            this.textBox10,
            this.textBox9,
            this.textBox7,
            this.textBox6,
            this.textBox4,
            this.SettlementToDate,
            this.SettlementFromDate,
            this.textBox3,
            this.textBox2,
            this.BranchNo,
            this.textBox11,
            this.RouteNo,
            this.textBox5,
            this.TotalPaymentsOnAccount,
            this.CoffeePoundsSold,
            this.AlliedTotalAmt,
            this.CoffeeTotalAmt,
            this.AlliedPoundsSold,
            this.NetCashSales,
            this.TotalCashReceived,
            this.Expenses,
            this.MoneyOrder,
            this.Checks,
            this.BankDeposit,
            this.table1,
            this.textBox25,
            this.textBox31,
            this.textBox51,
            this.textBox39,
            this.textBox46,
            this.textBox8,
            this.textBox48,
            this.textBox49,
            this.textBox50});
            this.detail.Name = "detail";
            // 
            // textBox21
            // 
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(9.5999984741210938D), Telerik.Reporting.Drawing.Unit.Pixel(324D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(192.00003051757813D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.Font.Name = "Courier New";
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox21.Style.Font.Underline = true;
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox21.Value = "ROUTE CHECKIN DETAIL";
            // 
            // textBox19
            // 
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(16.799999237060547D), Telerik.Reporting.Drawing.Unit.Pixel(253.80001831054688D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(192.20002746582031D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Name = "Courier New";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox19.Value = "Checks";
            // 
            // textBox18
            // 
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(16.799999237060547D), Telerik.Reporting.Drawing.Unit.Pixel(196.80001831054688D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(192.20002746582031D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.Font.Name = "Courier New";
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox18.Value = "Expenses";
            // 
            // textBox16
            // 
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(16.799999237060547D), Telerik.Reporting.Drawing.Unit.Pixel(158.80000305175781D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(192.00001525878906D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Courier New";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox16.Value = "Total Payments On Account";
            // 
            // textBox15
            // 
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(16.799999237060547D), Telerik.Reporting.Drawing.Unit.Pixel(177.80000305175781D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(192.20002746582031D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Name = "Courier New";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox15.Value = "Total Cash Received";
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(16.799999237060547D), Telerik.Reporting.Drawing.Unit.Pixel(139.80000305175781D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(192.20002746582031D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Name = "Courier New";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox14.Value = "Net Cash Sales";
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(16.799999237060547D), Telerik.Reporting.Drawing.Unit.Pixel(120.80000305175781D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(192.20002746582031D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Courier New";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox13.Value = "Allied";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(16.799999237060547D), Telerik.Reporting.Drawing.Unit.Pixel(234.80001831054688D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(192.20002746582031D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Courier New";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox12.Value = "MoneyOrder";
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(16.799999237060547D), Telerik.Reporting.Drawing.Unit.Pixel(100.80000305175781D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(192.20002746582031D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Courier New";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox10.Value = "Coffee Pounds";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(16.799999237060547D), Telerik.Reporting.Drawing.Unit.Pixel(215.80001831054688D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(192.20002746582031D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Courier New";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox9.Value = "Bank Deposit";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(413.7119140625D), Telerik.Reporting.Drawing.Unit.Pixel(72.199996948242188D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(86.399993896484375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Courier New";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox7.Value = "Sample";
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(266.39999389648438D), Telerik.Reporting.Drawing.Unit.Pixel(72.199996948242188D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.60003662109375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Name = "Courier New";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox6.Value = "Sold";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(9.5999984741210938D), Telerik.Reporting.Drawing.Unit.Pixel(55.199996948242188D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(192.00003051757813D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Courier New";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox4.Style.Font.Underline = true;
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox4.Value = "Branch Route Control";
            // 
            // SettlementToDate
            // 
            this.SettlementToDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(507.20028686523438D), Telerik.Reporting.Drawing.Unit.Pixel(19.20001220703125D));
            this.SettlementToDate.Name = "SettlementToDate";
            this.SettlementToDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(92.592453002929688D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.SettlementToDate.Style.Font.Bold = true;
            this.SettlementToDate.Style.Font.Name = "Courier New";
            this.SettlementToDate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.SettlementToDate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.SettlementToDate.Value = "=Fields.SettlementToDate";
            // 
            // SettlementFromDate
            // 
            this.SettlementFromDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(399.2000732421875D), Telerik.Reporting.Drawing.Unit.Pixel(19.20001220703125D));
            this.SettlementFromDate.Name = "SettlementFromDate";
            this.SettlementFromDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(76.792404174804688D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.SettlementFromDate.Style.Font.Bold = true;
            this.SettlementFromDate.Style.Font.Name = "Courier New";
            this.SettlementFromDate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.SettlementFromDate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.SettlementFromDate.Value = "=Fields.SettlementFromDate";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(480.20022583007812D), Telerik.Reporting.Drawing.Unit.Pixel(19.20001220703125D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(27D), Telerik.Reporting.Drawing.Unit.Pixel(15D));
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Courier New";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox3.Value = "To:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(349.20001220703125D), Telerik.Reporting.Drawing.Unit.Pixel(19.20001220703125D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(48D), Telerik.Reporting.Drawing.Unit.Pixel(15D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Courier New";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox2.Value = "From:";
            // 
            // BranchNo
            // 
            this.BranchNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(67.5999984741211D), Telerik.Reporting.Drawing.Unit.Pixel(19.20001220703125D));
            this.BranchNo.Name = "BranchNo";
            this.BranchNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(76.399993896484375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.BranchNo.Style.Font.Bold = true;
            this.BranchNo.Style.Font.Name = "Courier New";
            this.BranchNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.BranchNo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.BranchNo.Value = "=Fields.BranchNo";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(9.5999984741210938D), Telerik.Reporting.Drawing.Unit.Pixel(19.20001220703125D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(57.592430114746094D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Name = "Courier New";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox11.Value = "Branch:";
            // 
            // RouteNo
            // 
            this.RouteNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(201.60760498046875D), Telerik.Reporting.Drawing.Unit.Pixel(19.20001220703125D));
            this.RouteNo.Name = "RouteNo";
            this.RouteNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.60003662109375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.RouteNo.Style.Font.Bold = true;
            this.RouteNo.Style.Font.Name = "Courier New";
            this.RouteNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.RouteNo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.RouteNo.Value = "=Fields.RouteNo";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(152.60760498046875D), Telerik.Reporting.Drawing.Unit.Pixel(19.20001220703125D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(48.992424011230469D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Name = "Courier New";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox5.Value = "ROUTE:";
            // 
            // TotalPaymentsOnAccount
            // 
            this.TotalPaymentsOnAccount.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(516.91192626953125D), Telerik.Reporting.Drawing.Unit.Pixel(158.59999084472656D));
            this.TotalPaymentsOnAccount.Name = "TotalPaymentsOnAccount";
            this.TotalPaymentsOnAccount.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.60003662109375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.TotalPaymentsOnAccount.Style.Font.Bold = true;
            this.TotalPaymentsOnAccount.Style.Font.Name = "Courier New";
            this.TotalPaymentsOnAccount.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.TotalPaymentsOnAccount.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.TotalPaymentsOnAccount.Value = "=Fields.TotalPaymentsOnAccount";
            // 
            // CoffeePoundsSold
            // 
            this.CoffeePoundsSold.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(228D), Telerik.Reporting.Drawing.Unit.Pixel(100.80000305175781D));
            this.CoffeePoundsSold.Name = "CoffeePoundsSold";
            this.CoffeePoundsSold.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(129.16348266601563D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.CoffeePoundsSold.Style.Font.Bold = true;
            this.CoffeePoundsSold.Style.Font.Name = "Courier New";
            this.CoffeePoundsSold.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.CoffeePoundsSold.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.CoffeePoundsSold.Value = "=Fields.CoffeePoundsSold";
            // 
            // AlliedTotalAmt
            // 
            this.AlliedTotalAmt.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(516.91192626953125D), Telerik.Reporting.Drawing.Unit.Pixel(120.60000610351563D));
            this.AlliedTotalAmt.Name = "AlliedTotalAmt";
            this.AlliedTotalAmt.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.60003662109375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.AlliedTotalAmt.Style.Font.Bold = true;
            this.AlliedTotalAmt.Style.Font.Name = "Courier New";
            this.AlliedTotalAmt.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.AlliedTotalAmt.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.AlliedTotalAmt.Value = "=Fields.AlliedTotalAmt";
            // 
            // CoffeeTotalAmt
            // 
            this.CoffeeTotalAmt.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(516.91192626953125D), Telerik.Reporting.Drawing.Unit.Pixel(100.60001373291016D));
            this.CoffeeTotalAmt.Name = "CoffeeTotalAmt";
            this.CoffeeTotalAmt.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.60003662109375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.CoffeeTotalAmt.Style.Font.Bold = true;
            this.CoffeeTotalAmt.Style.Font.Name = "Courier New";
            this.CoffeeTotalAmt.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.CoffeeTotalAmt.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.CoffeeTotalAmt.Value = "=Fields.CoffeeTotalAmt";
            // 
            // AlliedPoundsSold
            // 
            this.AlliedPoundsSold.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(228D), Telerik.Reporting.Drawing.Unit.Pixel(120.79998779296875D));
            this.AlliedPoundsSold.Name = "AlliedPoundsSold";
            this.AlliedPoundsSold.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(129.16348266601563D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.AlliedPoundsSold.Style.Font.Bold = true;
            this.AlliedPoundsSold.Style.Font.Name = "Courier New";
            this.AlliedPoundsSold.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.AlliedPoundsSold.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.AlliedPoundsSold.Value = "=Fields.AlliedPoundsSold";
            // 
            // NetCashSales
            // 
            this.NetCashSales.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(516.91192626953125D), Telerik.Reporting.Drawing.Unit.Pixel(139.60000610351563D));
            this.NetCashSales.Name = "NetCashSales";
            this.NetCashSales.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.60003662109375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.NetCashSales.Style.Font.Bold = true;
            this.NetCashSales.Style.Font.Name = "Courier New";
            this.NetCashSales.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.NetCashSales.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.NetCashSales.Value = "=Fields.NetCashSales";
            // 
            // TotalCashReceived
            // 
            this.TotalCashReceived.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(516.91192626953125D), Telerik.Reporting.Drawing.Unit.Pixel(177.59999084472656D));
            this.TotalCashReceived.Name = "TotalCashReceived";
            this.TotalCashReceived.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.60003662109375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.TotalCashReceived.Style.Font.Bold = true;
            this.TotalCashReceived.Style.Font.Name = "Courier New";
            this.TotalCashReceived.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.TotalCashReceived.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.TotalCashReceived.Value = "=Fields.TotalCashReceived";
            // 
            // Expenses
            // 
            this.Expenses.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(516.91192626953125D), Telerik.Reporting.Drawing.Unit.Pixel(196.60002136230469D));
            this.Expenses.Name = "Expenses";
            this.Expenses.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.60003662109375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.Expenses.Style.Font.Bold = true;
            this.Expenses.Style.Font.Name = "Courier New";
            this.Expenses.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.Expenses.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Expenses.Value = "=Fields.Expenses";
            // 
            // MoneyOrder
            // 
            this.MoneyOrder.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(516.91192626953125D), Telerik.Reporting.Drawing.Unit.Pixel(234.60003662109375D));
            this.MoneyOrder.Name = "MoneyOrder";
            this.MoneyOrder.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.60003662109375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.MoneyOrder.Style.Font.Bold = true;
            this.MoneyOrder.Style.Font.Name = "Courier New";
            this.MoneyOrder.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.MoneyOrder.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.MoneyOrder.Value = "=Fields.MoneyOrder";
            // 
            // Checks
            // 
            this.Checks.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(516.91192626953125D), Telerik.Reporting.Drawing.Unit.Pixel(253.60003662109375D));
            this.Checks.Name = "Checks";
            this.Checks.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.60003662109375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.Checks.Style.Font.Bold = true;
            this.Checks.Style.Font.Name = "Courier New";
            this.Checks.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.Checks.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.Checks.Value = "=Fields.Checks";
            // 
            // BankDeposit
            // 
            this.BankDeposit.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(516.91192626953125D), Telerik.Reporting.Drawing.Unit.Pixel(215.60002136230469D));
            this.BankDeposit.Name = "BankDeposit";
            this.BankDeposit.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.60003662109375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.BankDeposit.Style.Font.Bold = true;
            this.BankDeposit.Style.Font.Name = "Courier New";
            this.BankDeposit.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.BankDeposit.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.BankDeposit.Value = "=Fields.BankDeposit";
            // 
            // table1
            // 
            this.table1.Anchoring = Telerik.Reporting.AnchoringStyles.Top;
            this.table1.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Items"));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(16.502506256103516D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(17.144992828369141D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(10.704652786254883D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(9.5250043869018555D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(12.488347053527832D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(11.218335151672363D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(15.345820426940918D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(13.917123794555664D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(15.240000724792481D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(10.371634483337402D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(9.5250024795532227D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.1968502402305603D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox32);
            this.table1.Body.SetCellContent(0, 2, this.OrderType);
            this.table1.Body.SetCellContent(0, 7, this.textBox33);
            this.table1.Body.SetCellContent(0, 8, this.textBox23);
            this.table1.Body.SetCellContent(0, 9, this.textBox22);
            this.table1.Body.SetCellContent(0, 4, this.textBox17);
            this.table1.Body.SetCellContent(0, 10, this.textBox34);
            this.table1.Body.SetCellContent(0, 6, this.textBox40);
            this.table1.Body.SetCellContent(0, 3, this.textBox43);
            this.table1.Body.SetCellContent(0, 5, this.textBox45);
            this.table1.Body.SetCellContent(0, 1, this.textBox47);
            tableGroup2.Name = "group5";
            tableGroup1.ChildGroups.Add(tableGroup2);
            tableGroup1.ReportItem = this.textBox28;
            tableGroup3.Name = "group10";
            tableGroup3.ReportItem = this.textBox44;
            tableGroup4.ReportItem = this.textBox36;
            tableGroup5.Name = "group8";
            tableGroup5.ReportItem = this.textBox41;
            tableGroup6.Name = "group1";
            tableGroup6.ReportItem = this.textBox27;
            tableGroup7.Name = "group9";
            tableGroup7.ReportItem = this.textBox42;
            tableGroup8.Name = "group7";
            tableGroup8.ReportItem = this.textBox35;
            tableGroup9.Name = "group2";
            tableGroup9.ReportItem = this.textBox38;
            tableGroup10.Name = "group4";
            tableGroup10.ReportItem = this.textBox29;
            tableGroup11.Name = "group6";
            tableGroup11.ReportItem = this.textBox20;
            tableGroup12.Name = "group3";
            tableGroup12.ReportItem = this.textBox37;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.ColumnGroups.Add(tableGroup5);
            this.table1.ColumnGroups.Add(tableGroup6);
            this.table1.ColumnGroups.Add(tableGroup7);
            this.table1.ColumnGroups.Add(tableGroup8);
            this.table1.ColumnGroups.Add(tableGroup9);
            this.table1.ColumnGroups.Add(tableGroup10);
            this.table1.ColumnGroups.Add(tableGroup11);
            this.table1.ColumnGroups.Add(tableGroup12);
            this.table1.ColumnHeadersPrintOnEveryPage = true;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox32,
            this.OrderType,
            this.textBox33,
            this.textBox23,
            this.textBox22,
            this.textBox17,
            this.textBox34,
            this.textBox40,
            this.textBox43,
            this.textBox45,
            this.textBox47,
            this.textBox28,
            this.textBox44,
            this.textBox36,
            this.textBox41,
            this.textBox27,
            this.textBox42,
            this.textBox35,
            this.textBox38,
            this.textBox29,
            this.textBox20,
            this.textBox37});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.25400006771087646D), Telerik.Reporting.Drawing.Unit.Inch(3.0999999046325684D));
            this.table1.Name = "table1";
            tableGroup13.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup13.Name = "Detail";
            this.table1.RowGroups.Add(tableGroup13);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(141.98342895507813D), Telerik.Reporting.Drawing.Unit.Inch(0.59643357992172241D));
            this.table1.Style.BorderColor.Default = System.Drawing.Color.DimGray;
            this.table1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table1.Style.LineColor = System.Drawing.Color.Transparent;
            this.table1.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.table1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.table1.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.table1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table1.StyleName = "";
            this.table1.ItemDataBinding += new System.EventHandler(this.table1_ItemDataBinding);
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6502506732940674D), Telerik.Reporting.Drawing.Unit.Cm(0.49999964237213135D));
            this.textBox32.Style.BorderColor.Default = System.Drawing.Color.DimGray;
            this.textBox32.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox32.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox32.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox32.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox32.Style.Font.Bold = true;
            this.textBox32.Style.Font.Name = "Courier New";
            this.textBox32.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox32.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox32.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox32.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox32.StyleName = "Normal.TableBody";
            this.textBox32.Value = "=Fields.OrderNumber";
            // 
            // OrderType
            // 
            this.OrderType.Name = "OrderType";
            this.OrderType.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0704653263092041D), Telerik.Reporting.Drawing.Unit.Cm(0.49999961256980896D));
            this.OrderType.Style.BorderColor.Default = System.Drawing.Color.DimGray;
            this.OrderType.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.OrderType.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.OrderType.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.OrderType.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.OrderType.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.OrderType.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.OrderType.Style.Font.Bold = true;
            this.OrderType.Style.Font.Name = "Courier New";
            this.OrderType.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.OrderType.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.OrderType.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.OrderType.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.OrderType.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.OrderType.StyleName = "Normal.TableBody";
            this.OrderType.Value = "=Fields.OrderType";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(65.750175476074219D), Telerik.Reporting.Drawing.Unit.Pixel(23.622030258178711D));
            this.textBox33.Style.BorderColor.Default = System.Drawing.Color.DimGray;
            this.textBox33.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox33.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox33.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox33.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.Font.Bold = true;
            this.textBox33.Style.Font.Name = "Courier New";
            this.textBox33.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox33.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox33.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox33.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox33.StyleName = "Normal.TableBody";
            this.textBox33.Value = "";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(72.000007629394531D), Telerik.Reporting.Drawing.Unit.Inch(0.1968502402305603D));
            this.textBox23.Style.BorderColor.Default = System.Drawing.Color.DimGray;
            this.textBox23.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox23.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox23.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox23.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Name = "Courier New";
            this.textBox23.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox23.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox23.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox23.StyleName = "Normal.TableBody";
            this.textBox23.Value = "=Fields.PaymentTime";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(48.999828338623047D), Telerik.Reporting.Drawing.Unit.Inch(0.1968502551317215D));
            this.textBox22.Style.BorderColor.Default = System.Drawing.Color.DimGray;
            this.textBox22.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox22.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox22.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox22.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.Font.Bold = true;
            this.textBox22.Style.Font.Name = "Courier New";
            this.textBox22.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox22.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox22.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox22.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox22.StyleName = "Normal.TableBody";
            this.textBox22.Value = "=Fields.InvoiceTotalAmt";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(59.000049591064453D), Telerik.Reporting.Drawing.Unit.Inch(0.1968502551317215D));
            this.textBox17.Style.BorderColor.Default = System.Drawing.Color.DimGray;
            this.textBox17.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox17.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox17.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox17.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.Font.Name = "Courier New";
            this.textBox17.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox17.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox17.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox17.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox17.StyleName = "Normal.TableBody";
            this.textBox17.Value = "=Fields.ShipTo";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(45.000015258789062D), Telerik.Reporting.Drawing.Unit.Inch(0.1968502402305603D));
            this.textBox34.Style.BorderColor.Default = System.Drawing.Color.DimGray;
            this.textBox34.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.Font.Name = "Courier New";
            this.textBox34.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox34.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox34.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox34.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox34.StyleName = "Normal.TableBody";
            this.textBox34.Value = "=Fields.DeliverySeq";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(72.499954223632812D), Telerik.Reporting.Drawing.Unit.Inch(0.1968502551317215D));
            this.textBox40.Style.BorderColor.Default = System.Drawing.Color.DimGray;
            this.textBox40.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox40.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox40.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox40.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.Style.Font.Bold = true;
            this.textBox40.Style.Font.Name = "Courier New";
            this.textBox40.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox40.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox40.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox40.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox40.StyleName = "Normal.TableBody";
            this.textBox40.Value = "=Fields.PaymentTerm";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(45.000026702880859D), Telerik.Reporting.Drawing.Unit.Inch(0.1968502402305603D));
            this.textBox43.Style.BorderColor.Default = System.Drawing.Color.DimGray;
            this.textBox43.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox43.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox43.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox43.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox43.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox43.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox43.Style.Font.Bold = true;
            this.textBox43.Style.Font.Name = "Courier New";
            this.textBox43.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox43.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox43.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox43.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox43.StyleName = "Normal.TableBody";
            this.textBox43.Value = "=Fields.ShipToID";
            // 
            // textBox45
            // 
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(53D), Telerik.Reporting.Drawing.Unit.Inch(0.1968502551317215D));
            this.textBox45.Style.BorderColor.Default = System.Drawing.Color.DimGray;
            this.textBox45.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox45.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox45.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox45.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox45.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox45.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox45.Style.Font.Bold = true;
            this.textBox45.Style.Font.Name = "Courier New";
            this.textBox45.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox45.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox45.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox45.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox45.StyleName = "Normal.TableBody";
            this.textBox45.Value = "=Fields.BillToID";
            // 
            // textBox47
            // 
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(80.9999771118164D), Telerik.Reporting.Drawing.Unit.Inch(0.1968502402305603D));
            this.textBox47.Style.BorderColor.Default = System.Drawing.Color.DimGray;
            this.textBox47.Style.BorderColor.Right = System.Drawing.Color.DimGray;
            this.textBox47.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox47.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox47.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox47.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox47.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox47.Style.Font.Bold = true;
            this.textBox47.Style.Font.Name = "Courier New";
            this.textBox47.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox47.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox47.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox47.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox47.StyleName = "Normal.TableBody";
            this.textBox47.Value = "=Fields.OrderDate";
            // 
            // textBox25
            // 
            this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(375.31192016601562D), Telerik.Reporting.Drawing.Unit.Pixel(100.80000305175781D));
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(124.80001831054688D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox25.Style.Font.Bold = true;
            this.textBox25.Style.Font.Name = "Courier New";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox25.Value = "=Fields.SampleCoffeePoundsSold";
            // 
            // textBox31
            // 
            this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(375.31192016601562D), Telerik.Reporting.Drawing.Unit.Pixel(120.79998779296875D));
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(124.80001831054688D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox31.Style.Font.Bold = true;
            this.textBox31.Style.Font.Name = "Courier New";
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox31.Value = "=Fields.SampleAlliedPoundsSold";
            // 
            // textBox51
            // 
            this.textBox51.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.413999557495117D), Telerik.Reporting.Drawing.Unit.Cm(1.0159999132156372D));
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.6846833229064941D), Telerik.Reporting.Drawing.Unit.Cm(0.30020013451576233D));
            this.textBox51.Style.Font.Bold = true;
            this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox51.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox51.Value = "= Now().ToString(\"MM/dd/yyyy hh:mm:ss \") + Now().ToString(\"tt\").ToLower()";
            // 
            // textBox39
            // 
            this.textBox39.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.1440000534057617D), Telerik.Reporting.Drawing.Unit.Cm(1.0159999132156372D));
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2956523895263672D), Telerik.Reporting.Drawing.Unit.Cm(0.30020010471343994D));
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox39.Value = "DATE:";
            // 
            // textBox46
            // 
            this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(516.91192626953125D), Telerik.Reporting.Drawing.Unit.Pixel(72.199989318847656D));
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(86.399993896484375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox46.Style.Font.Bold = true;
            this.textBox46.Style.Font.Name = "Courier New";
            this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox46.Value = "Total";
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(16.337738037109375D), Telerik.Reporting.Drawing.Unit.Pixel(276D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(192.20002746582031D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Courier New";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox8.Value = "NSV";
            // 
            // textBox48
            // 
            this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(516D), Telerik.Reporting.Drawing.Unit.Pixel(276D));
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.60003662109375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox48.Style.Font.Bold = true;
            this.textBox48.Style.Font.Name = "Courier New";
            this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox48.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox48.Value = "=Fields.NSV";
            // 
            // textBox49
            // 
            this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(16.800018310546875D), Telerik.Reporting.Drawing.Unit.Pixel(297D));
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(192.20002746582031D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox49.Style.Font.Bold = true;
            this.textBox49.Style.Font.Name = "Courier New";
            this.textBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox49.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox49.Value = "Verifier";
            // 
            // textBox50
            // 
            this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(516.91192626953125D), Telerik.Reporting.Drawing.Unit.Pixel(297D));
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.60003662109375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox50.Style.Font.Bold = true;
            this.textBox50.Style.Font.Name = "Courier New";
            this.textBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox50.Value = "=Fields.Verifier";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(1.0000003576278687D));
            this.textBox26.Value = "textBox26";
            // 
            // RouteSettlementReport
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail});
            this.Name = "Invoice1";
            this.PageNumberingStyle = Telerik.Reporting.PageNumberingStyle.ResetNumberingAndCount;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Pixel(10D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            reportParameter1.Name = "PrintLabels";
            reportParameter1.Type = Telerik.Reporting.ReportParameterType.Boolean;
            this.ReportParameters.Add(reportParameter1);
            this.Style.Font.Bold = true;
            this.Style.Font.Name = "Courier New";
            this.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Corporate.TableNormal")});
            styleRule2.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Name = "Tahoma";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Corporate.TableHeader")});
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector1});
            styleRule3.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(58)))), ((int)(((byte)(112)))));
            styleRule3.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule3.Style.Color = System.Drawing.Color.White;
            styleRule3.Style.Font.Name = "Tahoma";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            descendantSelector2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Corporate.TableBody")});
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector2});
            styleRule4.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule4.Style.Font.Name = "Tahoma";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4});
            this.UnitOfMeasure = Telerik.Reporting.Drawing.UnitType.Pixel;
            this.Width = Telerik.Reporting.Drawing.Unit.Pixel(773.697021484375D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox SettlementToDate;
        private Telerik.Reporting.TextBox SettlementFromDate;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox BranchNo;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox RouteNo;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox TotalPaymentsOnAccount;
        private Telerik.Reporting.TextBox CoffeePoundsSold;
        private Telerik.Reporting.TextBox AlliedTotalAmt;
        private Telerik.Reporting.TextBox CoffeeTotalAmt;
        private Telerik.Reporting.TextBox AlliedPoundsSold;
        private Telerik.Reporting.TextBox NetCashSales;
        private Telerik.Reporting.TextBox TotalCashReceived;
        private Telerik.Reporting.TextBox Expenses;
        private Telerik.Reporting.TextBox MoneyOrder;
        private Telerik.Reporting.TextBox Checks;
        private Telerik.Reporting.TextBox BankDeposit;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox OrderType;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox50;
    }
}