namespace SalesLogicExpress.Reporting
{
    partial class HeldReturnLoad
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(35.000034332275391D), Telerik.Reporting.Drawing.Unit.Pixel(29.858264923095703D));
            this.textBox5.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Name = "Courier New";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox5.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox5.StyleName = "Normal.TableHeader";
            this.textBox5.Value = "Date";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(35.551235198974609D), Telerik.Reporting.Drawing.Unit.Pixel(29.858266830444336D));
            this.textBox29.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Font.Name = "Courier New";
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox29.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox29.StyleName = "Normal.TableHeader";
            this.textBox29.Value = "Item";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(89.551338195800781D), Telerik.Reporting.Drawing.Unit.Pixel(29.858266830444336D));
            this.textBox39.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.Font.Name = "Courier New";
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox39.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox39.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox39.StyleName = "Normal.TableHeader";
            this.textBox39.Value = "Description";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(29.551189422607422D), Telerik.Reporting.Drawing.Unit.Pixel(29.858266830444336D));
            this.textBox25.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox25.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox25.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.Color = System.Drawing.Color.Black;
            this.textBox25.Style.Font.Bold = true;
            this.textBox25.Style.Font.Name = "Courier New";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox25.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox25.StyleName = "Corporate.TableHeader";
            this.textBox25.Value = "Prm\r\nUOM";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(47.000003814697266D), Telerik.Reporting.Drawing.Unit.Pixel(29.858264923095703D));
            this.textBox30.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox30.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox30.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox30.Style.Color = System.Drawing.Color.Black;
            this.textBox30.Style.Font.Bold = true;
            this.textBox30.Style.Font.Name = "Courier New";
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox30.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox30.StyleName = "Corporate.TableHeader";
            this.textBox30.Value = "OnHand Qty";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(48.000026702880859D), Telerik.Reporting.Drawing.Unit.Pixel(29.858264923095703D));
            this.textBox27.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox27.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox27.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox27.Style.Color = System.Drawing.Color.Black;
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Font.Name = "Courier New";
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox27.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox27.StyleName = "Corporate.TableHeader";
            this.textBox27.Value = "Comm. Qty\r\nUOM";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(52.000015258789062D), Telerik.Reporting.Drawing.Unit.Pixel(29.858264923095703D));
            this.textBox7.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox7.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox7.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.Color = System.Drawing.Color.Black;
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Courier New";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox7.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox7.StyleName = "Corporate.TableHeader";
            this.textBox7.Value = "Held Qty\r\nUOM";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(51.999969482421875D), Telerik.Reporting.Drawing.Unit.Pixel(29.858266830444336D));
            this.textBox31.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox31.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox31.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.Color = System.Drawing.Color.Black;
            this.textBox31.Style.Font.Bold = true;
            this.textBox31.Style.Font.Name = "Courier New";
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox31.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox31.StyleName = "Corporate.TableHeader";
            this.textBox31.Value = "Avlble\r\nQty";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(46.000034332275391D), Telerik.Reporting.Drawing.Unit.Pixel(29.858266830444336D));
            this.textBox33.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox33.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox33.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.Font.Bold = true;
            this.textBox33.Style.Font.Name = "Courier New";
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox33.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox33.StyleName = "Corporate.TableHeader";
            this.textBox33.Value = "Par\r\nLevel";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(50.000022888183594D), Telerik.Reporting.Drawing.Unit.Pixel(29.858266830444336D));
            this.textBox37.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox37.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox37.Style.Font.Bold = true;
            this.textBox37.Style.Font.Name = "Courier New";
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox37.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox37.StyleName = "Corporate.TableHeader";
            this.textBox37.Value = "Return\r\nQty";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(3.3500497341156006D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.textBox11,
            this.textBox10,
            this.textBox8,
            this.textBox9,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.textBox19,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox45});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(241.8897705078125D), Telerik.Reporting.Drawing.Unit.Pixel(18.897636413574219D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(238.1102294921875D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Courier New";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox1.Style.Font.Underline = true;
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Value = "= Fields.ReportTitle";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(30.236221313476562D), Telerik.Reporting.Drawing.Unit.Pixel(37.795272827148438D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(49.126304626464844D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Name = "Courier New";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox11.Value = "ROUTE:";
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(79.370086669921875D), Telerik.Reporting.Drawing.Unit.Pixel(37.795272827148438D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(302.36221313476562D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Courier New";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox10.Value = "=Fields.RouteNoAndName";
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(464.88189697265625D), Telerik.Reporting.Drawing.Unit.Pixel(37.795272827148438D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(45.346748352050781D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Courier New";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox8.Value = "DATE:";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(510.22869873046875D), Telerik.Reporting.Drawing.Unit.Pixel(37.795272827148438D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(158.7476806640625D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Courier New";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox9.Value = "=Fields.ReportDate";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(30.236221313476562D), Telerik.Reporting.Drawing.Unit.Pixel(52.920944213867188D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(109.60630798339844D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Courier New";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox12.Value = "TRANSFER FROM:";
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(139.84249877929688D), Telerik.Reporting.Drawing.Unit.Pixel(52.920951843261719D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(241.88980102539063D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Courier New";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox13.Value = "=Fields.TransferFrom";
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(464.88189697265625D), Telerik.Reporting.Drawing.Unit.Pixel(52.920944213867188D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(120.94485473632813D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Name = "Courier New";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox14.Value = "HELD RETURN ID:";
            // 
            // textBox15
            // 
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(585.8343505859375D), Telerik.Reporting.Drawing.Unit.Pixel(52.920944213867188D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.488189697265625D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Name = "Courier New";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox15.Value = "=Fields.TransferNo";
            // 
            // textBox16
            // 
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(464.88189697265625D), Telerik.Reporting.Drawing.Unit.Pixel(68.046615600585938D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(56.692962646484375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Courier New";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox16.Value = "STATUS:";
            // 
            // textBox17
            // 
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(521.58245849609375D), Telerik.Reporting.Drawing.Unit.Pixel(68.046615600585938D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(185.18917846679688D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.Font.Name = "Courier New";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox17.Value = "=Fields.Status";
            // 
            // textBox18
            // 
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(30.236221313476562D), Telerik.Reporting.Drawing.Unit.Pixel(68.046630859375D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.102020263671875D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.Font.Name = "Courier New";
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox18.Value = "TRANSFER TO:";
            // 
            // textBox19
            // 
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(124.73196411132813D), Telerik.Reporting.Drawing.Unit.Pixel(68.046630859375D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(257.00033569335938D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Name = "Courier New";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox19.Value = "=Fields.TransferTo";
            // 
            // textBox20
            // 
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(464.88189697265625D), Telerik.Reporting.Drawing.Unit.Pixel(83.17230224609375D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(105.60641479492188D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.Font.Name = "Courier New";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox20.Value = "REQUESTED BY:";
            // 
            // textBox21
            // 
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(570.49591064453125D), Telerik.Reporting.Drawing.Unit.Pixel(83.17230224609375D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(136.27572631835938D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.Font.Name = "Courier New";
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox21.Value = "=Fields.UserName";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(2D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table2});
            this.detail.Name = "detail";
            // 
            // table2
            // 
            this.table2.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Items"));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(35.000034332275391D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(35.551235198974609D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(89.551338195800781D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(29.55122184753418D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(47.000049591064453D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(48.000053405761719D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(52.000019073486328D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(52.000019073486328D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(46.000106811523438D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(50.00006103515625D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20866139233112335D)));
            this.table2.Body.SetCellContent(0, 1, this.textBox23);
            this.table2.Body.SetCellContent(0, 2, this.textBox24);
            this.table2.Body.SetCellContent(0, 3, this.textBox26);
            this.table2.Body.SetCellContent(0, 7, this.textBox32);
            this.table2.Body.SetCellContent(0, 9, this.textBox34);
            this.table2.Body.SetCellContent(0, 8, this.textBox4);
            this.table2.Body.SetCellContent(0, 0, this.textBox2);
            this.table2.Body.SetCellContent(0, 4, this.textBox6);
            this.table2.Body.SetCellContent(0, 5, this.textBox3);
            this.table2.Body.SetCellContent(0, 6, this.textBox28);
            tableGroup1.Name = "group";
            tableGroup1.ReportItem = this.textBox5;
            tableGroup2.Name = "tableGroup";
            tableGroup2.ReportItem = this.textBox29;
            tableGroup3.Name = "tableGroup1";
            tableGroup3.ReportItem = this.textBox39;
            tableGroup4.Name = "tableGroup2";
            tableGroup4.ReportItem = this.textBox25;
            tableGroup5.Name = "group4";
            tableGroup5.ReportItem = this.textBox30;
            tableGroup6.Name = "group3";
            tableGroup6.ReportItem = this.textBox27;
            tableGroup7.Name = "group1";
            tableGroup7.ReportItem = this.textBox7;
            tableGroup8.Name = "group9";
            tableGroup8.ReportItem = this.textBox31;
            tableGroup9.Name = "group19";
            tableGroup9.ReportItem = this.textBox33;
            tableGroup10.Name = "group2";
            tableGroup10.ReportItem = this.textBox37;
            this.table2.ColumnGroups.Add(tableGroup1);
            this.table2.ColumnGroups.Add(tableGroup2);
            this.table2.ColumnGroups.Add(tableGroup3);
            this.table2.ColumnGroups.Add(tableGroup4);
            this.table2.ColumnGroups.Add(tableGroup5);
            this.table2.ColumnGroups.Add(tableGroup6);
            this.table2.ColumnGroups.Add(tableGroup7);
            this.table2.ColumnGroups.Add(tableGroup8);
            this.table2.ColumnGroups.Add(tableGroup9);
            this.table2.ColumnGroups.Add(tableGroup10);
            this.table2.ColumnHeadersPrintOnEveryPage = true;
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox23,
            this.textBox24,
            this.textBox26,
            this.textBox32,
            this.textBox34,
            this.textBox4,
            this.textBox2,
            this.textBox6,
            this.textBox3,
            this.textBox28,
            this.textBox5,
            this.textBox29,
            this.textBox39,
            this.textBox25,
            this.textBox30,
            this.textBox27,
            this.textBox7,
            this.textBox31,
            this.textBox33,
            this.textBox37});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(20.1806640625D), Telerik.Reporting.Drawing.Unit.Pixel(17.005996704101562D));
            this.table2.Name = "table2";
            tableGroup11.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup11.GroupKeepTogether = true;
            tableGroup11.Name = "detailTableGroup";
            this.table2.RowGroups.Add(tableGroup11);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(484.65414428710938D), Telerik.Reporting.Drawing.Unit.Inch(0.457480251789093D));
            // 
            // textBox23
            // 
            this.textBox23.CanShrink = true;
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(35.551235198974609D), Telerik.Reporting.Drawing.Unit.Pixel(25.03936767578125D));
            this.textBox23.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox23.Style.Font.Bold = false;
            this.textBox23.Style.Font.Name = "Courier New";
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox23.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox23.StyleName = "Normal.TableBody";
            this.textBox23.Value = "=Fields.ItemNo";
            // 
            // textBox24
            // 
            this.textBox24.CanShrink = true;
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(89.551338195800781D), Telerik.Reporting.Drawing.Unit.Pixel(25.03936767578125D));
            this.textBox24.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox24.Style.Font.Name = "Courier New";
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox24.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox24.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox24.StyleName = "Corporate.TableBody";
            this.textBox24.Value = "= Fields.ItemDescription";
            // 
            // textBox26
            // 
            this.textBox26.CanShrink = true;
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(29.55122184753418D), Telerik.Reporting.Drawing.Unit.Pixel(25.03936767578125D));
            this.textBox26.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox26.Style.Font.Name = "Courier New";
            this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox26.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox26.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox26.StyleName = "Corporate.TableBody";
            this.textBox26.Value = "= Fields.PrimaryUOM";
            // 
            // textBox32
            // 
            this.textBox32.CanShrink = true;
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(51.999969482421875D), Telerik.Reporting.Drawing.Unit.Cm(0.53000003099441528D));
            this.textBox32.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox32.Style.Font.Name = "Courier New";
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox32.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox32.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox32.StyleName = "Corporate.TableBody";
            this.textBox32.Value = "= Fields.AvailableQty";
            // 
            // textBox34
            // 
            this.textBox34.CanShrink = true;
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(50.000019073486328D), Telerik.Reporting.Drawing.Unit.Cm(0.53000003099441528D));
            this.textBox34.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox34.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox34.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.Font.Bold = false;
            this.textBox34.Style.Font.Name = "Courier New";
            this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox34.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox34.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox34.StyleName = "Corporate.TableBody";
            this.textBox34.Value = "= Fields.LoadQuantity +\' \'+Fields.QtyUOM";
            // 
            // textBox4
            // 
            this.textBox4.CanShrink = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(46.000038146972656D), Telerik.Reporting.Drawing.Unit.Cm(0.53000003099441528D));
            this.textBox4.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox4.Style.Font.Name = "Courier New";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox4.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox4.StyleName = "Corporate.TableBody";
            this.textBox4.Value = "= Fields.ParLevel";
            // 
            // textBox2
            // 
            this.textBox2.CanShrink = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(35.000034332275391D), Telerik.Reporting.Drawing.Unit.Cm(0.52999997138977051D));
            this.textBox2.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox2.Style.Font.Bold = false;
            this.textBox2.Style.Font.Name = "Courier New";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox2.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox2.StyleName = "Normal.TableBody";
            this.textBox2.Value = "=Fields.OrderDateString";
            // 
            // textBox6
            // 
            this.textBox6.CanShrink = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(47.000003814697266D), Telerik.Reporting.Drawing.Unit.Cm(0.52999997138977051D));
            this.textBox6.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox6.Style.Font.Name = "Courier New";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox6.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox6.StyleName = "Corporate.TableBody";
            this.textBox6.Value = "= Fields.OnHandQty";
            // 
            // textBox3
            // 
            this.textBox3.CanShrink = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(48.000026702880859D), Telerik.Reporting.Drawing.Unit.Cm(0.52999997138977051D));
            this.textBox3.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox3.Style.Font.Name = "Courier New";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox3.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox3.StyleName = "Corporate.TableBody";
            this.textBox3.Value = "= Fields.CommittedQty";
            // 
            // textBox28
            // 
            this.textBox28.CanShrink = true;
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(52.000015258789062D), Telerik.Reporting.Drawing.Unit.Cm(0.52999997138977051D));
            this.textBox28.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox28.Style.Font.Name = "Courier New";
            this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox28.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox28.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox28.StyleName = "Corporate.TableBody";
            this.textBox28.Value = "= Fields.HeldQty";
            // 
            // textBox22
            // 
            this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(569.88189697265625D), Telerik.Reporting.Drawing.Unit.Pixel(108D));
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.4881591796875D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox22.Value = "{PageNumber} of {PageCount}";
            // 
            // textBox45
            // 
            this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(464.88189697265625D), Telerik.Reporting.Drawing.Unit.Pixel(108D));
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(72.944587707519531D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox45.Style.Font.Bold = true;
            this.textBox45.Style.Font.Name = "Courier New";
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox45.Value = "Page";
            // 
            // HeldReturnLoad
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail});
            this.Name = "HeldReturnLoad";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Pixel(10D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.UnitOfMeasure = Telerik.Reporting.Drawing.UnitType.Pixel;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(6D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox45;
    }
}