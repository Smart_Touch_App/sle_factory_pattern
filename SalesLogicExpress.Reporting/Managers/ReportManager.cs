﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Configuration;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace SalesLogicExpress.Reporting
{
    public static class ReportManager
    {

        static Telerik.Reporting.Report reportFile;
        public static InstanceReportSource GetReport(Reports report, object data, Dictionary<string, object> parameters)
        {
            Telerik.Reporting.ObjectDataSource objectDataSource = new Telerik.Reporting.ObjectDataSource();
            objectDataSource.DataSource = data;
            objectDataSource.DataMember = "GetData"; // Specifying the name of the data object method

            InstanceReportSource reportSource = new InstanceReportSource();
            Type reportType = Type.GetType(report.GetEnumDescription());
            reportFile = (Telerik.Reporting.Report)Activator.CreateInstance(reportType, null);
            reportFile.DataSource = objectDataSource;
            reportSource.ReportDocument = reportFile;
            if (parameters != null)
                foreach (KeyValuePair<string, object> item in parameters.ToList())
                {
                    reportSource.Parameters.Add(item.Key, item.Value);
                }
            return reportSource;
        }

        public static void GenerateReportRunTime(string reportType, string fileName, Dictionary<string, object> parameters)
        {
            try
            {
                ReportProcessor reportProcessor = new ReportProcessor();
                Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
                instanceReportSource.ReportDocument = reportFile;
                if (parameters != null)
                    foreach (KeyValuePair<string, object> item in parameters.ToList())
                    {
                        instanceReportSource.Parameters.Add(item.Key, item.Value);
                    }
                string DocumentPath = ConfigurationManager.AppSettings["DomainPath"].ToString();

                string FolderPath = DocumentPath + @"\Documents\" + reportType.ToString();
                System.IO.Directory.CreateDirectory(FolderPath);

                string fullFilePath = FolderPath + @"\" + fileName;

                if (File.Exists(fullFilePath))
                {
                    File.Delete(fullFilePath);
                }

                RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

                using (FileStream fs = new FileStream(fullFilePath, FileMode.Create))
                {
                    fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static string GetEnumDescription(this Enum enumValue)
        {
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return enumValue.ToString();
            }

        }
    }
}
