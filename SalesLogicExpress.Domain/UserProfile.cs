﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class UserProfile : User
    {
        int _AppUserID;
        string _FirstName, _LastName, _NTUser, _DisplayName, _Domain, _EmailAddress, _PhoneNumber, _SelectedMode, _SelectedTitle;

        public int AppUserID
        {
            get
            {
                if (_AppUserID == null)
                    _AppUserID = 0;
                return _AppUserID;
            }
            set { _AppUserID = value; }
        }

        public string FirstName
        {
            get
            {
                if (_FirstName == null)
                    _FirstName = string.Empty;
                return _FirstName;
            }
            set
            {
                _FirstName = value;
                OnPropertyChanged("FirstName");
            }
        }

        public string LastName
        {
            get
            {
                if (_LastName == null)
                    _LastName = string.Empty;
                return _LastName;
            }
            set { _LastName = value; OnPropertyChanged("LastName"); }
        }

        public string NTUser
        {
            get
            {
                if (_NTUser == null)
                    _NTUser = string.Empty;
                return _NTUser;
            }
            set { _NTUser = value; OnPropertyChanged("NTUser"); }
        }

        public string DisplayName
        {
            get
            {
                if (_DisplayName == null)
                    _DisplayName = string.Empty;
                return _DisplayName;
            }
            set { _DisplayName = value; OnPropertyChanged("DisplayName"); }
        }

        public string Domain
        {
            get
            {
                if (_Domain == null)
                    _Domain = string.Empty;
                return _Domain + "\\" + NTUser;
            }
            set { _Domain = value; OnPropertyChanged("Domain"); }
        }

        public string EmailAddress
        {
            get
            {
                if (_EmailAddress == null)
                    _EmailAddress = string.Empty;
                return _EmailAddress;
            }
            set { _EmailAddress = value; OnPropertyChanged("EmailAddress"); }
        }

        public string PhoneNumber
        {
            get
            {
                if (_PhoneNumber == null)
                    _PhoneNumber = string.Empty;
                return _PhoneNumber;
            }
            set { _PhoneNumber = value; OnPropertyChanged("PhoneNumber"); }
        }

        public string SelectedMode
        {
            get
            {
                if (_SelectedMode == null)
                    _SelectedMode = string.Empty;
                return _SelectedMode;
            }
            set { _SelectedMode = value; OnPropertyChanged("SelectedMode"); }
        }

        public string SelectedTitle
        {
            get
            {
                if (_SelectedTitle == null)
                    _SelectedTitle = string.Empty;
                return _SelectedTitle;
            }
            set { _SelectedTitle = value; OnPropertyChanged("SelectedTitle"); }
        }

        bool _isSaveAllowed = false;
        public Boolean IsSaveAllowed
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(FirstName) &&
                    !string.IsNullOrWhiteSpace(LastName) &&
                    !string.IsNullOrWhiteSpace(DisplayName) &&
                    !string.IsNullOrWhiteSpace(Domain))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                _isSaveAllowed = value;
                OnPropertyChanged("IsSaveAllowed");
            }
        }

        List<string> _UserTitle;
        public List<string> UserTitle
        {
            get
            {
                return _UserTitle;
            }
            set
            {
                _UserTitle = value;
            }
        }

        List<string> _ScannerMode;
        public List<string> ScannerMode
        {
            get
            {
                return _ScannerMode;
            }
            set
            {
                _ScannerMode = value;
            }
        }
    }
}
