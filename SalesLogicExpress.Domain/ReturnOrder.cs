﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace SalesLogicExpress.Domain
{
    public class ReturnOrder : Helpers.ModelBase
    {
        //public ObservableCollection<ReturnItem> returnItems 


        private int _OrderId = 0, returnOrderID =0;
        private bool _IsSelected = false;
        private bool _IsSettled = false;
        private DateTime _OrderDate;
        private decimal _Surcharge = 0;
        private decimal _orderTotal = 0;
        private bool _IsEnabled = false;

        public ReturnOrder()
        {
            
        }
        ObservableCollection<ReturnItem> returnItems = new ObservableCollection<ReturnItem>();
        public ObservableCollection<ReturnItem> ReturnItems
        {
            get
            {
                return returnItems;
            }
            set
            {
                returnItems = value;
                OnPropertyChanged("ReturnItems");
            }
        }


        public int OrderId
        {
            get { return _OrderId; }
            set { _OrderId = value; OnPropertyChanged("OrderId"); }
        }

        public int ReturnOrderID
        {
            get { return returnOrderID; }
            set { returnOrderID = value; OnPropertyChanged("ReturnOrderID"); }
        }

        public bool IsSelected
        {
            get { return _IsSelected; }
            set { _IsSelected = value; OnPropertyChanged("IsSelected"); }
        }

        public bool IsSettled
        {
            get { return _IsSettled; }
            set { _IsSettled = value; OnPropertyChanged("IsSettled"); }
        }

        public bool IsEnabled
        {
            get { return _IsEnabled; }
            set { _IsEnabled = value; OnPropertyChanged("IsEnabled"); }
        }

        public DateTime OrderDate
        {
            get { return _OrderDate; }
            set { _OrderDate = value; OnPropertyChanged("OrderDate"); }
        }

        public decimal Surcharge
        {
            get { return _Surcharge; }
            set { _Surcharge = value; OnPropertyChanged("Surcharge"); }
        }

        public decimal OrderTotal
        {
            get { return _orderTotal; }
            set
            {
                _orderTotal = value; OnPropertyChanged("OrderTotal");
            }
        }


        bool isOrderReturned;
        public bool IsOrderReturned
        {
            get
            {
                return isOrderReturned;
            }
            set
            {

                isOrderReturned = value;
                if (isOrderReturned)
                    IsEnabled = false;
                OnPropertyChanged("IsOrderReturned");
            }
        }

        
        int totalItems;
        public int TotalItems
        {
            get
            {
                return totalItems;
            }
            set
            {


                totalItems = value;
                OnPropertyChanged("TotalItems");
            }
        }



        bool isOnHold;
        public bool IsOnHold
        {
            get
            {
                return isOnHold;
            }
            set
            {


                isOnHold = value;
                OnPropertyChanged("IsOnHold");
            }
        }


        bool isVoid;
        public bool IsVoid
        {
            get
            {
                return isVoid;
            }
            set
            {

                isVoid = value;
                OnPropertyChanged("IsVoid");
            }
        }


        bool isHoldAtPick;
        public bool IsHoldAtPick
        {
            get
            {
                return isHoldAtPick;
            }
            set
            {

                isHoldAtPick = value;
                OnPropertyChanged("IsHoldAtPick");
            }
        }

        private decimal documentID;
        public decimal DocumentID
        {
            get
            {
                return documentID;
            }
            set
            {
                documentID = value;
            }
        }
    }
}
