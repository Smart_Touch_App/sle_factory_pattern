﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace SalesLogicExpress.Domain
{
    public class PreTripVehicleInspection : Helpers.ModelBase
    {
        private string routeID = string.Empty;
        public decimal DocumentId { get; set; }
        public string RouteID
        {
            get { return routeID; }
            set { routeID = value; }
        }
        private string preTripInspectionID = string.Empty;
        public string PreTripInspectionID
        {
            get { return preTripInspectionID; }
            set { preTripInspectionID = value; OnPropertyChanged("PreTripInspectionID"); }
        }

        private string templateID =string.Empty;
        public string TemplateID
        {
            get
            {
                return templateID;
            }
            set
            {
                templateID = value;
                OnPropertyChanged("templateID");
            }
        }

        private int statusID;
        public int StatusID
        {
            get
            {
                return statusID;
            }
            set
            {
                statusID = value;
                OnPropertyChanged("StatusID");
            }
        }

        private string templateName=string.Empty;
        public string TemplateName
        {
            get
            {
                return templateName;
            }
            set
            {
                templateName = value;
                OnPropertyChanged("TemplateName");
            }
        }

        private ObservableCollection<VehicleNumber> vehicleNo=new ObservableCollection<VehicleNumber>();
        public ObservableCollection<VehicleNumber> VehicleNo
        {
            get
            {
                return vehicleNo;
            }
            set
            {
                vehicleNo = value;
                OnPropertyChanged("VehicleNo");
            }
        }

        private ObservableCollection<VehicleMake> vehicleMake = new ObservableCollection<VehicleMake>();
        public ObservableCollection<VehicleMake> VehicleMake
        {
            get
            {
                return vehicleMake;
            }
            set
            {
                vehicleMake = value;
                OnPropertyChanged("VehicleMake");
            }
        }

        private ObservableCollection<PreTripInspectionFormList> questionList = new ObservableCollection<PreTripInspectionFormList>();
        public ObservableCollection<PreTripInspectionFormList> QuestionList
        {
            get { return questionList; }
            set { questionList = value; OnPropertyChanged("QuestionList"); }
        }

        private string vehicleModel=string.Empty;
        public string VehicleModel
        {
            get
            {
                return vehicleModel;
            }
            set
            {
                vehicleModel = value;
                OnPropertyChanged("VehicleModel");
            }
        }

        private string vehicleNumber=string.Empty;
        public string VehicleNumber
        {
            get { return vehicleNumber; }
            set { vehicleNumber = value; OnPropertyChanged("VehicleNumber"); }
        }

        private string statusTypeCD = string.Empty;
        public string StatusTypeCD
        {
            get
            {
                return statusTypeCD;
            }
            set
            {
                statusTypeCD = value;
               OnPropertyChanged("StatusTypeCD");
            }
        }

        private string odometerReading = string.Empty;
        public string OdometerReading
        {
            get
            {
                return odometerReading;
            }
            set
            {
                odometerReading = value;
                OnPropertyChanged("OdometerReading");
            }
        }

        private string additionalComments=string.Empty;
        public string AdditionalComments
        {
            get
            {
                return additionalComments;
            }
            set
            {
                additionalComments = value;
                OnPropertyChanged("AdditionalComments");
            }
        }

        private Byte[] verificationSignature;
        public Byte[] VerificationSignature
        {
            get
            {
                return verificationSignature;
            }
            set
            {
                verificationSignature = value;
                OnPropertyChanged("VerificationSignature");
            }
        }
        private DateTime preTripDateTime = new DateTime();

        public DateTime PreTripDateTime
        {
            get { return preTripDateTime; }
            set { preTripDateTime = value; }
        }

        private string statusCode = string.Empty;
        public string StatusCode
        {
            get { return statusCode; }
            set { statusCode = value; OnPropertyChanged("StatusCode"); }
        }
        private int statusCheckList = 0;

        public int StatusCheckList
        {
            get { return statusCheckList; }
            set { statusCheckList = value; OnPropertyChanged("StatusCheckList"); }
        }

        private bool showDetailsOfPreTripInspection = false;
        public bool ShowDetailsOfPreTripInspection
        {
            get { return showDetailsOfPreTripInspection; }
            set { showDetailsOfPreTripInspection = value; OnPropertyChanged("ShowDetailsOfPreTripInspection"); }
        }

        private bool isDetailsShown = true;
        public bool IsDetailsShown
        {
            get { return isDetailsShown; }
            set { isDetailsShown = value; OnPropertyChanged("IsDetailsShown"); }
        }
        
    }

    public class VehicleNumber : Helpers.ModelBase
    {
        private string vehicleID = string.Empty;
        public string VehicleID
        {
            get { return vehicleID; }
            set { vehicleID = value; OnPropertyChanged("VehicleID"); }
        }
        private string vehicleNo=string.Empty;
        public string VehicleNo
        {
            get { return vehicleNo; }
            set { vehicleNo = value; OnPropertyChanged("vehicleNo"); }
        }

    }

    public class VehicleMake : Helpers.ModelBase
    {
        private string makeID;
        public string MakeID
        {
            get { return makeID; }
            set { makeID = value; OnPropertyChanged("MakeID"); }
        }

        private string make;
        public string Make
        {
            get { return make; }
            set { make = value; OnPropertyChanged("Make"); }
        }
    }

    public class PreTripInspectionFormList : Helpers.ModelBase
    {
        private string questionID=string.Empty;
        public string QuestionID 
        { get
            { return questionID; }
            set 
            { 
                questionID = value;
                OnPropertyChanged("QuestionID");
            }
        }

        private string questionTitle=string.Empty; 
        public string QuestionTitle
        {
            get
            {
                return questionTitle;
            }
            set
            {
                questionTitle = value;
                OnPropertyChanged("QuestionTitle");
            }
        }

        private int responseID;
        public int ResponseID
        {
            get
            { 
            return responseID;
            }
            set
            {
                responseID = value;
                OnPropertyChanged("ResponseID");
            }
        }

        private string description=string.Empty;
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
                OnPropertyChanged("Description");
            }
        }
        //public bool IsYesChecked { get; set; }
       // public bool IsNoChecked { get; set; }
        private string notPassedReason=string.Empty;
        public string NotPassedReason
        {
            get
            {
                return notPassedReason;
            }
            set
            {
                notPassedReason = value;
                OnPropertyChanged("NotPassedReason");
            }
        }

        private bool isYesChecked=false;
        public bool IsYesChecked
        {
            get { return isYesChecked; }
            set
            {
                if (isYesChecked != value)
                {
                    isYesChecked = value;
                    if (isYesChecked)
                    {
                        IsNoChecked = false;
                    }
                    //else isno
                }


                OnPropertyChanged("IsYesChecked");
            }
        }

        private bool isNoChecked=false;
        public bool IsNoChecked
        {
            get { return isNoChecked; }
            set
            {
                if (isNoChecked != value)
                {
                    isNoChecked = value;
                    if (isNoChecked)
                    {
                        IsYesChecked = false;
                    }
                }
                OnPropertyChanged("IsNoChecked");
            }
        }

        private bool isCheckboxEnabled = true;
        public bool IsCheckboxEnabled
        {
            get { return isCheckboxEnabled; }
            set { isCheckboxEnabled = value; OnPropertyChanged("IsCheckboxEnabled"); }
        }

        private bool isNoPassedReasonEnabled = true;

        public bool IsNoPassedReasonEnabled
        {
            get { return isNoPassedReasonEnabled; }
            set { isNoPassedReasonEnabled = value; OnPropertyChanged("IsNoPassedReasonEnabled"); }
        }
    }
}
