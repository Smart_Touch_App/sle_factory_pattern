﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace SalesLogicExpress.Domain
{
   public class CustomerNote : Helpers.ModelBase, IDataErrorInfo
    {
        public string CustomerID { get; set; }
        public string NoteID { get; set; }
        public string DateTime { get; set; }
        public string NoteDetails { get; set; }
        bool _IsDefault = false;
        public bool IsDefault
        {
            get
            {
                return _IsDefault;
            }
            set
            {
                _IsDefault = value;
                OnPropertyChanged("IsDefault");
            }
        }
        bool _IsSelected = false;
        public bool IsSelected
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                _IsSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }
        public override string ToString()
        {
            return string.Concat("NoteID:", NoteID, "DateTime:", DateTime, "NoteDetails:", NoteDetails, "IsDefault:", IsDefault);
        } 
        public string Error
        {
            get { return null; }
        }
        public string this[string columnName]
        {
            get { return null; }
        }
    }
}
