﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Interactivity;
using Telerik.Windows;
using System.Windows;
using System.Windows.Input;
using System.Text.RegularExpressions;
using Telerik.Windows.Controls;
using System.Runtime.Serialization;

namespace SalesLogicExpress.Domain.Helpers
{
    [DataContract]
    [KnownType(typeof(SalesLogicExpress.Domain.Item))]
    public class ModelBase : BaseEntity, INotifyPropertyChanged, INotifyDataErrorInfo
    {
        #region Implementation for INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }
        public void RaisePropertyChanged(string PropertyName)
        {
            OnPropertyChanged(PropertyName);
        }
        protected void OnPropertyChanged(string propertyName)
        {
            this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Implmentation for INotifyDataErrorInfo
        private Dictionary<string, List<string>> errors = new Dictionary<string, List<string>>();

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        private void OnDataErrorsChanged(string propertyName)
        {
            if (this.ErrorsChanged != null)
            {
                this.ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
            }
        }

        protected virtual void AddError(string propertyName, string errorMessage)
        {
            if (string.IsNullOrEmpty(propertyName) || this.errors == null)
            {
                return;
            }
            if (!this.errors.ContainsKey(propertyName))
            {
                this.errors.Add(propertyName, new List<String>());
            }

            if (!this.errors[propertyName].Contains(errorMessage))
            {
                this.errors[propertyName].Add(errorMessage);
                this.OnDataErrorsChanged(propertyName);
            }
        }

        protected virtual void RemoveError(string propertyName, string errorMessage)
        {
            if (string.IsNullOrEmpty(propertyName) || this.errors == null)
            {
                return;
            }
            if (this.errors.ContainsKey(propertyName))
            {
                if (this.errors[propertyName].Contains(errorMessage))
                {
                    this.errors[propertyName].Remove(errorMessage);
                    if (this.errors[propertyName].Count == 0)
                    {
                        this.errors.Remove(propertyName);
                    }

                    this.OnDataErrorsChanged(propertyName);
                }
            }
        }

        public System.Collections.IEnumerable GetErrors(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName) && this.errors != null)
            {
                if (!this.errors.ContainsKey((propertyName == null ? "" : propertyName)))
                    return null;
            }
            return this.errors != null && this.errors.Count > 0 ? (this.errors.ContainsKey(propertyName) ? this.errors[propertyName] : null) : null;
        }

        public bool HasErrors
        {
            get { return this.errors != null ? this.errors.Count > 0 : false; }
        }
        #endregion
    }


}
