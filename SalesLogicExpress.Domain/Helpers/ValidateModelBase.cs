﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Interactivity;
using Telerik.Windows;
using System.Windows;
using System.Windows.Input;
using System.Text.RegularExpressions;
using Telerik.Windows.Controls;
using System.Runtime.Serialization;


namespace SalesLogicExpress.Domain.Helpers
{
    [DataContract]
    [KnownType(typeof(SalesLogicExpress.Domain.Visitee))]
    public class ValidateModelBase : BaseEntity, INotifyPropertyChanged, INotifyDataErrorInfo
    {

        #region Constructor
        public ValidateModelBase()
        {
            this.PropertyChanged += ValidationBase_PropertyChanged;
        }
        #endregion

        #region Methods
        bool IsModelValid()
        {
            var properties = this.GetType().GetProperties();
            PropertyInfo[] propertiess = (this.GetType()).GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            var results = new List<ValidationResult>();
            bool valid = true;
            IsValid = true;
            foreach (PropertyInfo prop in properties)
            {
                if (prop.CanWrite)
                {
                    var result = Validator.TryValidateProperty(
                                        prop.GetValue(this, null),
                                        new ValidationContext(this, null, null)
                                        {
                                            MemberName = prop.Name
                                        },
                                        results);
                    if (result == false)
                    {
                        IsValid = false;
                        break;
                    }

                }
            }
            return valid;
        }
        #endregion

        #region Properties


        bool _IsValid = false;
        public bool IsValid
        {
            get
            {
                return _IsValid;
            }
            set
            {
                _IsValid = value;
                OnPropertyChanged("IsValid");
            }
        }
        #endregion

        #region QueueChanged Handlers
        void ValidationBase_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IsValid"))
            {
                return;
            }
            string propertyName = e.PropertyName;
            PropertyInfo propertyInfo = this.GetType().GetProperty(propertyName);
            var results = new List<ValidationResult>();
            var result = Validator.TryValidateProperty(
                                      propertyInfo.GetValue(this, null),
                                      new ValidationContext(this, null, null)
                                      {
                                          MemberName = propertyName
                                      },
                                      results);
            if (!result)
            {
                var validationResult = results.First();
                if (_validationErrors.ContainsKey(propertyName) && !_validationErrors[propertyName].Contains(validationResult.ErrorMessage))
                    _validationErrors[propertyName].Add(validationResult.ErrorMessage);
                else
                    _validationErrors[propertyName] = new List<string> { validationResult.ErrorMessage };
                RaiseErrorsChanged(propertyName);
            }
            else
            {
                _validationErrors.Remove(propertyName);
                RaiseErrorsChanged(propertyName);
            }
            IsModelValid();
        }
        #endregion

        #region INotifyPropertyChange Implementation
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }
        public void RaisePropertyChanged(string PropertyName)
        {
            OnPropertyChanged(PropertyName);
        }
        protected void OnPropertyChanged(string propertyName)
        {
            this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Members and Methods required for Validation
        // Firstly declare a dictionary which can hold error, now why we have a Icollection in dictionary is that an UI element can have multiple errors
        protected readonly Dictionary<string, ICollection<string>> _validationErrors = new Dictionary<string, ICollection<string>>();

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        protected void RaiseErrorsChanged(string propertyName)
        {
            if (ErrorsChanged != null)
                ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
        }
        public IEnumerable GetErrors(string propertyName)
        {
            // Simple check which checks whether the propertyName is not empt/null and is contained in the validationError dictionary
            if (string.IsNullOrEmpty(propertyName) || !_validationErrors.ContainsKey(propertyName))
                return null;
            return _validationErrors[propertyName];
        }
        public bool HasErrors
        {
            get { return _validationErrors.Count > 0; }
        }
        #endregion


    }

  
    public sealed class StringLengthRangeAttribute : ValidationAttribute
    {
        public int Minimum { get; set; }
        public int Maximum { get; set; }

        public StringLengthRangeAttribute()
        {
            this.Minimum = 0;
            this.Maximum = int.MaxValue;
        }

        public override bool IsValid(object value)
        {
            string strValue = value as string;
            if (!string.IsNullOrEmpty(strValue))
            {
                int len = strValue.Trim().Length;
                return len >= this.Minimum && len <= this.Maximum;
            }
            return true;
        }
    }

    public class ExcludeChar : ValidationAttribute
    {
        private readonly string _chars;
        public ExcludeChar(string chars)
            : base("{0} contains invalid character.")
        {
            _chars = chars;
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                for (int i = 0; i < _chars.Length; i++)
                {
                    var valueAsString = value.ToString();
                    if (valueAsString.Contains(_chars[i]))
                    {
                        var errorMessage = FormatErrorMessage(validationContext.DisplayName);
                        return new ValidationResult(errorMessage);
                    }
                }
            }
            return ValidationResult.Success;
        }
    }

}
