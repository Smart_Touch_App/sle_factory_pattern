﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Domain;

namespace SalesLogicExpress.Domain.Vertex
{
    [Serializable]
    public class InvoiceObject
    {
        //Class Stores invoice level information for Vertex Api call.
        public InvoiceObject()
        {

        }
        private string _InvoiceNumber, _ProductSetCode, _Customer, _CustomerClassCode, _CompCode;
        private long _GeoCode;
        private double _InvoiceTax;
        private double _GrossAmt;
        public List<InvoiceItem> InvoiceItemList = new List<InvoiceItem>();


        public string InvoiceNumber
        {
            get { return _InvoiceNumber; }
            set { _InvoiceNumber = value; }
        }
        public string ProductSetCode
        {
            get { return _ProductSetCode; }
            set { _ProductSetCode = value; }
        }
        public string Customer
        {
            get { return _Customer; }
            set { _Customer = value; }
        }
        public string CustomerClassCode
        {
            get { return _CustomerClassCode; }
            set { _CustomerClassCode = value; }
        }
        public string CompCode
        {
            get { return _CompCode; }
            set { _CompCode = value; }
        }
        public long GeoCode
        {
            get { return _GeoCode; }
            set { _GeoCode = value; }
        }
        public double InvoiceTax
        {
            get { return _InvoiceTax; }
            set { _InvoiceTax = value; }
        }

        public double GrossAmt
        {
            get { return _GrossAmt; }
            set { _GrossAmt = value; }
        }

    }

    [Serializable]
    public class InvoiceItem
    {
        //Class stores Item level information for Vertex Api call.
        public InvoiceItem(string productcode, OrderItem orderitems)
        {
            this._ProductCode = productcode;
            this._ExtendedAmount = Convert.ToDouble(orderitems.ExtendedPrice);
            this._ItemNumber = orderitems.ItemNumber;
            this._ItemId = orderitems.ItemId;
            this._ProductQuantity = orderitems.OrderQty;
        }

        string _ProductCode;
        double _ItemTax = 0;
        string _ItemNumber, _ItemId;

        public string ItemId
        {
            get { return _ItemId; }
            set { _ItemId = value; }
        }
        int _ProductQuantity;
        double _ExtendedAmount;
        
        public double ItemTax
        {
            get { return _ItemTax; }
            set { _ItemTax = value; }
        }
        public string ProductCode
        {
            get { return _ProductCode; }
            set { _ProductCode = value; }
        }
        public string ItemNumber
        {
            get { return _ItemNumber; }
            set { _ItemNumber = value; }
        }
        public int ProductQuantity
        {
            get { return _ProductQuantity; }
            set { _ProductQuantity = value; }
        }
        public double ExtendedAmount
        {
            get { return _ExtendedAmount; }
            set { _ExtendedAmount = value; }

        }
    }
}
