﻿using SalesLogicExpress.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;

namespace SalesLogicExpress.Domain
{
    [DataContract]
    [KnownType(typeof(SalesLogicExpress.Domain.Customer))]
    public class Visitee : ValidateModelBase
    {
        public Visitee()
            : base()
        {

        }

        int _SequenceNo = -1;
        int _TempSequenceNo = 0;
        string _City = string.Empty;
        string _Zip = string.Empty;
        string _Phone = string.Empty;
        string _Phone1 = string.Empty;
        string _Phone2 = string.Empty;
        string _Phone3 = string.Empty;
        string _PhoneExtention = string.Empty;
        string _State = string.Empty;
        string _Address = string.Empty;
        string _AddressLine1 = string.Empty;
        string _AddressLine2 = string.Empty;
        string _AddressLine3 = string.Empty;
        string _AddressLine4 = string.Empty;
        string _Name = string.Empty;
        string _BusinessName = string.Empty;

        public int SequenceNo
        {
            get { return this._SequenceNo; }
            set
            {
                if (value != this._SequenceNo)
                {
                    this._SequenceNo = value;
                    this.OnPropertyChanged("SequenceNo");
                }
            }
        }
        public int TempSequenceNo
        {
            get
            {
                return _TempSequenceNo;
            }
            set
            {
                _TempSequenceNo = value;
                this.OnPropertyChanged("TempSequenceNo");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "AddressLine1 Required")]
        [StringLengthRange(Minimum = 3, Maximum = 40, ErrorMessage = "Min 3 and max 40 charactors are allowed")]
        public string AddressLine1
        {
            get { return _AddressLine1; }
            set
            {
                _AddressLine1 = value;

                _AddressLine1 = value != null ? Regex.Replace(value, @"\s+", " ") : value;
                OnPropertyChanged("AddressLine1");
                Address = AddressLine1.Trim() + " " + AddressLine2.Trim() + " " + AddressLine3.Trim() + " " + AddressLine4.Trim();

            }
        }
        public string AddressLine2
        {
            get { return _AddressLine2; }
            set
            {
                _AddressLine2 = value;
                _AddressLine2 = value != null ? Regex.Replace(value, @"\s+", " ") : value;

                OnPropertyChanged("AddressLine2");
                Address = AddressLine1.Trim() + " " + AddressLine2.Trim() + " " + AddressLine3.Trim() + " " + AddressLine4.Trim();

            }
        }
        public string AddressLine3
        {
            get { return _AddressLine3; }
            set
            {
                _AddressLine3 = value;
                _AddressLine3 = value != null ? Regex.Replace(value, @"\s+", " ") : value;

                OnPropertyChanged("AddressLine3");
                Address = AddressLine1.Trim() + " " + AddressLine2.Trim() + " " + AddressLine3.Trim() + " " + AddressLine4.Trim();

            }
        }
        public string AddressLine4
        {
            get { return _AddressLine4; }
            set
            {
                _AddressLine4 = value;
                _AddressLine4 = value != null ? Regex.Replace(value, @"\s+", " ") : value;

                OnPropertyChanged("AddressLine4");
                Address = AddressLine1.Trim() + " " + AddressLine2.Trim() + " " + AddressLine3.Trim() + " " + AddressLine4.Trim();
            }
        }

        public string Address
        {
            get { return _Address; }
            set
            {
                _Address = value;
                OnPropertyChanged("Address");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "City Required")]
        [ExcludeChar("%@!%^*#", ErrorMessage = "special characters not allowed")]
        [StringLengthRange(Minimum = 1, Maximum = 25, ErrorMessage = "Min 1 and max 25 charactors are allowed")]
        public string City
        {
            get
            {
                return _City;
            }
            set
            {
                _City = value;
                _City = value != null ? Regex.Replace(value, @"\s+", " ") : value;

                OnPropertyChanged("City");
                //OnPropertyChanged("IsSaveAllowed");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Name Required")]
        [StringLengthRange(Minimum = 3, Maximum = 40, ErrorMessage = "Min 3 and max 40 charactors are allowed")]
        [ExcludeChar("%!%^*", ErrorMessage = "special characters not allowed")]
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
                _Name = value != null ? Regex.Replace(value, @"\s+", " ") : value;
                OnPropertyChanged("Name");
            }
        }
        public string BusinessName
        {
            get
            {
                return _BusinessName;
            }
            set
            {
                _BusinessName = value;
                OnPropertyChanged("BusinessName");
            }
        }
        public string Phone
        {
            get
            {
                return _Phone;
            }
            set
            {
                _Phone = value;
                OnPropertyChanged("Phone");
            }
        }

        [RegularExpression(@"[0-9]+$", ErrorMessage = " ")]
        [StringLengthRange(Minimum = 3, Maximum = 3, ErrorMessage = " ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = " ")]
        [StringLength(3, ErrorMessage = " ")]
        public string Phone1
        {
            get
            {
                return _Phone1;
            }
            set
            {
                _Phone1 = value;
                _Phone1 = value != null ? Regex.Replace(value, @"\s+", "") : value;

                OnPropertyChanged("Phone1");
            }
        }

        [RegularExpression(@"[0-9]+$", ErrorMessage = " ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = " ")]
        [StringLengthRange(Minimum = 3, Maximum = 3, ErrorMessage = " ")]
        [StringLength(3, ErrorMessage = " ")]
        public string Phone2
        {
            get
            {
                return _Phone2;
            }
            set
            {
                _Phone2 = value;
                _Phone2 = value != null ? Regex.Replace(value, @"\s+", "") : value;

                OnPropertyChanged("Phone2");
            }
        }

        [RegularExpression(@"[0-9]+$", ErrorMessage = " ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = " ")]
        [StringLengthRange(Minimum = 4, Maximum = 4, ErrorMessage = " ")]
        [StringLength(4, ErrorMessage = " ")]
        public string Phone3
        {
            get
            {
                return _Phone3;
            }
            set
            {
                _Phone3 = value;
                _Phone3 = value != null ? Regex.Replace(value, @"\s+", "") : value;

                OnPropertyChanged("Phone3");
            }
        }

        [StringLength(7, ErrorMessage = " ")]
        public string PhoneExtention
        {
            get
            {
                return _PhoneExtention;
            }
            set
            {
                _PhoneExtention = value;
                _PhoneExtention = value != null ? Regex.Replace(value, @"\s+", "") : value;
                OnPropertyChanged("PhoneExtention");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "State Required")]
        public string State
        {
            get
            {
                return _State;
            }
            set
            {
                _State = value;
                OnPropertyChanged("State");
                //OnPropertyChanged("IsSaveAllowed");
            }
        }

        [RegularExpression(@"[0-9]+$", ErrorMessage = "Zip Should be numeric")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Zip code Required")]
        [StringLengthRange(Minimum = 5, Maximum = 5, ErrorMessage = "Minimum 5 digits required")]
        [StringLength(5)]
        public string Zip
        {
            get
            {
                return _Zip;
            }
            set
            {
                _Zip = value;
                OnPropertyChanged("Zip");

                //OnPropertyChanged("IsSaveAllowed");
            }
        }

        #region Stop Related Properties
        public string StopID
        {
            get;
            set;
        }

        public DateTime? StopDate
        {
            get;
            set;
        }
        public DateTime? OriginalDate
        {
            get;
            set;
        }
        public DateTime? RescheduledDate { get; set; }
        public string StopType { get; set; }
        public string PreviousStop { get; set; }

        public string ReferenceStopId { get; set; }

        private string nextStop;
        public string NextStop
        {
            get { return nextStop; }
            set
            {
                nextStop = value;
                OnPropertyChanged("NextStop");
            }
        }

        private bool isTodaysStop = false;
        public bool IsTodaysStop
        {
            get
            {
                return isTodaysStop;
            }
            set
            {
                isTodaysStop = value;
                OnPropertyChanged("IsTodaysStop");
            }
        }

        private bool isResquence = false;
        public bool IsResquence
        {
            get
            {
                return this.isResquence;
            }
            set
            {
                this.isResquence = value;
                OnPropertyChanged("IsResquence");
            }
        }
        public string VisiteeType
        {
            get;
            set;
        }

        public string VisiteeId { get; set; }

        private bool isFromListingTab = false;
        public bool IsFromListingTab
        {
            get
            {
                return isFromListingTab;
            }
            set
            {
                isFromListingTab = value;
                OnPropertyChanged("IsFromListingTab");
            }
        }


        public string SaleStatus
        {
            get;
            set;
        }
        public int CompletedActivity { get; set; }
        public int PendingActivity { get; set; }
        private bool hasActivity = false;
        public bool HasActivity
        {
            get
            {
                if ((PendingActivity > 0 || CompletedActivity > 0) && string.IsNullOrEmpty(SaleStatus))
                {
                    hasActivity = true;
                }
                else
                {
                    hasActivity = false;
                }
                return hasActivity;
            }
            set
            {
                hasActivity = value;
            }

        }
        #endregion

        #region Serialized
        string _CategoryText;
        public string CategoryText
        {
            get
            {
                return _CategoryText;
            }
            set
            {
                _CategoryText = value;
                OnPropertyChanged("CategoryText");
                //OnPropertyChanged("IsSaveAllowed");
            }
        }



        public string ContactID { get; set; }
        #endregion

        #region Coffee Prospect

        public bool IsCoffeeAddEnabled
        {
            get
            {
                //************************************************************************************************
                // Comment: Commented to remove validation for liquid coffee selection
                // Created: Jan 24, 2016
                // Author: Vivensas (Rajesh,Yuvaraj)
                // Revisions: 
                //*************************************************************************************************

                if (!string.IsNullOrWhiteSpace(CompetitorSelected) && !string.IsNullOrWhiteSpace(CoffeeBlendSelected) && !string.IsNullOrWhiteSpace(PackSizeSelected)
                    && !string.IsNullOrWhiteSpace(CSPKLBSelected) && !string.IsNullOrWhiteSpace(CoffeeVolumeSelected) //&& !string.IsNullOrWhiteSpace(LIQCoffeeSelected)
                    && (!string.IsNullOrWhiteSpace(Price.ToString()) && Price != 0))
                {
                    return true;
                }
                else
                {
                    return false;
                }
                //*************************************************************************************************
                // Vivensas changes ends over here
                //**************************************************************************************************
            }
            set
            {
                OnPropertyChanged("IsCoffeeAddEnabled");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Select Competitor")]
        string _competitorSelected;
        public string CompetitorSelected
        {
            get
            {
                return _competitorSelected;
            }
            set
            {
                _competitorSelected = value;
                OnPropertyChanged("IsCoffeeAddEnabled");
                OnPropertyChanged("CompetitorSelected");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Select Coffee Blend")]
        string _coffeeBlendSelectedSelected;
        public string CoffeeBlendSelected
        {
            get
            {
                return _coffeeBlendSelectedSelected;
            }
            set
            {
                _coffeeBlendSelectedSelected = value;
                OnPropertyChanged("IsCoffeeAddEnabled");
                OnPropertyChanged("CoffeeBlendSelected");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Select UOM")]
        private string _uOMSelected;
        public string UOMSelected
        {
            get
            {
                return _uOMSelected;
            }
            set
            {
                _uOMSelected = value;
                OnPropertyChanged("UOMSelected");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Select Pack Size")]
        private string _packSizeSelected;
        public string PackSizeSelected
        {
            get
            {
                return _packSizeSelected;
            }
            set
            {
                _packSizeSelected = value;
                OnPropertyChanged("IsCoffeeAddEnabled");
                OnPropertyChanged("PackSizeSelected");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Select CS/PK/LB")]
        private string _CSPKLBSelected;
        public string CSPKLBSelected
        {
            get
            {
                return _CSPKLBSelected;
            }
            set
            {
                _CSPKLBSelected = value;
                OnPropertyChanged("IsCoffeeAddEnabled");
                OnPropertyChanged("CSPKLBSelected");
            }
        }

        [RegularExpression(@"[0-9]+$", ErrorMessage = "Price Should be numeric")]
        [StringLengthRange(Minimum = 1, Maximum = 7, ErrorMessage = "Minimum 1 digits required")]
        [StringLength(7)]
        private decimal _Price;
        public decimal Price
        {
            get
            {
                return _Price;
            }
            set
            {
                _Price = value;
                OnPropertyChanged("Price");
                OnPropertyChanged("IsCoffeeAddEnabled");
                OnPropertyChanged("IsAlliedAddEnabled");
            }
        }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Select Coffee Volume")]
        private string _coffeeVolumeSelected;
        public string CoffeeVolumeSelected
        {
            get
            {
                return _coffeeVolumeSelected;
            }
            set
            {
                _coffeeVolumeSelected = value;
                OnPropertyChanged("IsCoffeeAddEnabled");
                OnPropertyChanged("CoffeeVolumeSelected");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Select LIQ Coffee")]
        private string _lIQCoffeeSelected;
        public string LIQCoffeeSelected
        {
            get
            {
                return _lIQCoffeeSelected;
            }
            set
            {
                _lIQCoffeeSelected = value;
                OnPropertyChanged("IsCoffeeAddEnabled");
                OnPropertyChanged("LIQCoffeeSelected");
            }
        }

        private string _ButtonTitle;
        public string ButtonTitle
        {
            get
            {
                return _ButtonTitle;
            }
            set
            {
                _ButtonTitle = value;
                OnPropertyChanged("ButtonTitle");
            }
        }
        private string _CompetitorDropDown;
        public string CompetitorDropDown
        {
            get
            {
                return _CompetitorDropDown;
            }
            set
            {
                _CompetitorDropDown = value;
                OnPropertyChanged("CompetitorDropDown");
            }
        }
        private string _CoffeeBlendsDropDown;
        public string CoffeeBlendsDropDown
        {
            get
            {
                return _CoffeeBlendsDropDown;
            }
            set
            {
                _CoffeeBlendsDropDown = value;
                OnPropertyChanged("CoffeeBlendsDropDown");
            }
        }
        private string _CategoryDropDown;
        public string CategoryDropDown
        {
            get
            {
                return _CategoryDropDown;
            }
            set
            {
                _CategoryDropDown = value;
                OnPropertyChanged("CategoryDropDown");
            }
        }
        private string _SubCategoryDropDown;
        public string SubCategoryDropDown
        {
            get
            {
                return _SubCategoryDropDown;
            }
            set
            {
                _SubCategoryDropDown = value;
                OnPropertyChanged("SubCategoryDropDown");
            }
        }
        #endregion

        #region Allied Prospect

        public bool IsAlliedAddEnabled
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(AlliedCompetitorSelected) && !string.IsNullOrWhiteSpace(AlliedCategorySelected) && !string.IsNullOrWhiteSpace(AlliedSubCategorySelected)
                    && !string.IsNullOrWhiteSpace(AlliedBrandSelected) && !string.IsNullOrWhiteSpace(AlliedPackSizeSelected) && !string.IsNullOrWhiteSpace(AlliedCSPKLBSelected)
                    && !string.IsNullOrWhiteSpace(AlliedVolumeSelected) && (!string.IsNullOrWhiteSpace(Price.ToString()) && Price != 0))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                OnPropertyChanged("IsAlliedAddEnabled");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Select Competitor")]
        string _AlliedCompetitorSelected;
        public string AlliedCompetitorSelected
        {
            get
            {
                return _AlliedCompetitorSelected;
            }
            set
            {
                _AlliedCompetitorSelected = value;
                OnPropertyChanged("IsAlliedAddEnabled");
                OnPropertyChanged("AlliedCompetitorSelected");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Select Allied Category")]
        string _AlliedCategorySelected;
        public string AlliedCategorySelected
        {
            get
            {
                return _AlliedCategorySelected;
            }
            set
            {
                _AlliedCategorySelected = value;
                OnPropertyChanged("IsAlliedAddEnabled");
                OnPropertyChanged("AlliedCategorySelected");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Select Allied Sub Category")]
        string _AlliedSubCategorySelected;
        public string AlliedSubCategorySelected
        {
            get
            {
                return _AlliedSubCategorySelected;
            }
            set
            {
                _AlliedSubCategorySelected = value;
                OnPropertyChanged("IsAlliedAddEnabled");
                OnPropertyChanged("AlliedSubCategorySelected");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Select Allied Brand")]
        private string _AlliedBrandSelected;
        public string AlliedBrandSelected
        {
            get
            {
                return _AlliedBrandSelected;
            }
            set
            {
                _AlliedBrandSelected = value;
                OnPropertyChanged("IsAlliedAddEnabled");
                OnPropertyChanged("AlliedBrandSelected");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Select Allied Pack Size")]
        private string _AlliedpackSizeSelected;
        public string AlliedPackSizeSelected
        {
            get
            {
                return _AlliedpackSizeSelected;
            }
            set
            {
                _AlliedpackSizeSelected = value;
                OnPropertyChanged("IsAlliedAddEnabled");
                OnPropertyChanged("AlliedPackSizeSelected");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Select CS/PK/LB")]
        private string _AlliedCSPKLBSelected;
        public string AlliedCSPKLBSelected
        {
            get
            {
                return _AlliedCSPKLBSelected;
            }
            set
            {
                _AlliedCSPKLBSelected = value;
                OnPropertyChanged("IsAlliedAddEnabled");
                OnPropertyChanged("AlliedCSPKLBSelected");
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Select Allied Volume")]
        private string _AlliedVolumeSelected;
        public string AlliedVolumeSelected
        {
            get
            {
                return _AlliedVolumeSelected;
            }
            set
            {
                _AlliedVolumeSelected = value;
                OnPropertyChanged("IsAlliedAddEnabled");
                OnPropertyChanged("AlliedVolumeSelected");
            }
        }

        bool _IsAlliedSelected = false;
        public bool IsAlliedSelected
        {
            get
            {
                return _IsAlliedSelected;
            }
            set
            {
                _IsAlliedSelected = value;
                OnPropertyChanged("IsAlliedSelected");
            }
        }
        #endregion

        #region Equipment
        //Disable Expensed Button
        public bool IsExpensedValid
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(EquipmentSelected) && !string.IsNullOrWhiteSpace(SubCategorySelected)
                    && !string.IsNullOrWhiteSpace(OwnedSelected) && (!string.IsNullOrWhiteSpace(QuantitySelected.ToString()) && QuantitySelected != 0))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                OnPropertyChanged("IsExpensedValid");
            }
        }
        //Disable Equipment Button
        public bool IsEquipmentValid
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(EquipmentSelected) && !string.IsNullOrWhiteSpace(SubCategorySelected)
                    && !string.IsNullOrWhiteSpace(OwnedSelected) && (!string.IsNullOrWhiteSpace(QuantitySelected.ToString()) && QuantitySelected != 0))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {

                OnPropertyChanged("IsEquipmentValid");
            }
        }

        public string _EquipmentSelected;
        public string EquipmentSelected
        {
            get
            {
                return _EquipmentSelected;
            }
            set
            {
                _EquipmentSelected = value;
                OnPropertyChanged("IsExpensedValid");
                OnPropertyChanged("IsEquipmentValid");
                OnPropertyChanged("EquipmentSelected");
            }
        }

        public string _SubCategorySelected;
        public string SubCategorySelected
        {
            get
            {
                return _SubCategorySelected;
            }
            set
            {
                _SubCategorySelected = value;
                OnPropertyChanged("IsExpensedValid");
                OnPropertyChanged("IsEquipmentValid");
                OnPropertyChanged("SubCategorySelected");
            }
        }
        public decimal? _QuantitySelected;
        public decimal? QuantitySelected
        {
            get
            {
                return _QuantitySelected;
            }
            set
            {
                _QuantitySelected = value;
                OnPropertyChanged("IsExpensedValid");
                OnPropertyChanged("IsEquipmentValid");
                OnPropertyChanged("QuantitySelected");
            }
        }
        public string _OwnedSelected;
        public string OwnedSelected
        {
            get
            {
                return _OwnedSelected;
            }
            set
            {
                _OwnedSelected = value;
                OnPropertyChanged("IsExpensedValid");
                OnPropertyChanged("IsEquipmentValid");
                OnPropertyChanged("OwnedSelected");
            }
        }
        #endregion

        #region Methods
        public Visitee DeepCopy()
        {
            Visitee newVisitee = (Visitee)this.MemberwiseClone();
            return newVisitee;
        }

        #endregion
    }
}
