﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class OrderHistory : Helpers.ModelBase
    {
        public string OrderDate { get; set; }
        public string OrderNumber { get; set; }
        public string ItemCode { get; set; }
        public string ItemDesc { get; set; }
        public int OrderQty1 { get; set; }
        public int OrderQty2 { get; set; }
        public int OrderQty3 { get; set; }
        public int OrderQty4 { get; set; }
        public int OrderQty5 { get; set; }
        public int OrderQty6 { get; set; }
        public int OrderQty7 { get; set; }
        public int OrderQty8 { get; set; }
        public int OrderQty9 { get; set; }
        public int OrderQty10 { get; set; }
        public float AverageQty { get; set; }
    }
}
