﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class ChargeOnAccount : Helpers.ModelBase
    {
        decimal _CreditLimit;
        decimal _TotalBalanceAmount;
        decimal _PreviousBalanceAmount;
        decimal _CurrentInvoiceAmount;
        string _RequestCode;
        string _ApprovalCode;
        int _ReasonCodeId;
        decimal _TemporaryCharge;
        Boolean _IsApprovalCodeNeeded;

        public decimal CurrentInvoiceAmount
        {
            get
            {
                return _CurrentInvoiceAmount;
            }
            set
            {
                _CurrentInvoiceAmount = value;
                OnPropertyChanged("CurrentInvoiceAmount");

            }
        }

        public Boolean IsApprovalCodeNeeded
        {
            get
            {
                return PreviousBalanceAmount > 0 ? true : false;
            }
            set
            {
                _IsApprovalCodeNeeded = value;
                OnPropertyChanged("IsApprovalCodeNeeded");

            }
        }

        public decimal PreviousBalanceAmount
        {
            get
            {
                return _PreviousBalanceAmount;
            }
            set
            {
                _PreviousBalanceAmount = value;
                OnPropertyChanged("PreviousBalanceAmount");
                OnPropertyChanged("IsApprovalCodeNeeded");
            }
        }

        public decimal TotalBalanceAmount
        {
            get
            {
                return _TotalBalanceAmount;
            }
            set
            {
                _TotalBalanceAmount = value;
                OnPropertyChanged("TotalBalanceAmount");
                //OnPropertyChanged("IsApprovalCodeNeeded");
            }
        }
        public string RequestCode
        {
            get
            {
                return IsApprovalCodeNeeded ? _RequestCode : "";
            }
            set
            {
                _RequestCode = value;
                OnPropertyChanged("RequestCode");

            }
        }
        public string ApprovalCode
        {
            get
            {
                return _ApprovalCode;
            }
            set
            {
                _ApprovalCode = value;
                OnPropertyChanged("ApprovalCode");

            }
        }
        public int ReasonCodeId
        {
            get
            {
                return _ReasonCodeId;
            }
            set
            {
                _ReasonCodeId = value;
                OnPropertyChanged("ReasonCodeId");

            }
        }
        public decimal TemporaryCharge
        {
            get
            {
                return _TemporaryCharge;
            }
            set
            {
                _TemporaryCharge = value;
                OnPropertyChanged("TemporaryCharge");

            }
        }

        public decimal CreditLimit
        {
            get
            {
                return _CreditLimit;
            }
            set
            {
                _CreditLimit = value;
                OnPropertyChanged("CreditLimit");
                //OnPropertyChanged("IsApprovalCodeNeeded");

            }
        }
    }
}
