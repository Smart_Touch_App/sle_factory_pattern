﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class QuotePickModel : PickItem
    {
        private bool isAllowManualPick = false;

        public bool IsAllowManualPick
        {
            get { return isAllowManualPick; }
            set { isAllowManualPick = value; OnPropertyChanged("IsAllowManualPick"); }
        }
        private bool isEnableScannerInException = false;

        public bool IsEnableScannerInException
        {
            get { return isEnableScannerInException; }
            set { isEnableScannerInException = value; OnPropertyChanged("IsEnableScannerInException"); }
        }

        private string pickingUM = string.Empty;

        public string PickingUM
        {
            get { return pickingUM; }
            set { pickingUM = value; }
        }

        public int DocumentID { get; set; }
    }
}
