﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class RouteSettlementModel
    {       
        private string billTo;
        private string shipTo;
        private string expenseType;
        private string amount;
        private string orderAmount;

        public string OrderAmount
        {
            get { return orderAmount; }
            set { orderAmount = value; }
        }
        private string moneyOrderNo;
        private string paymentTerm;
        private string expenseDescription;
        private string invoiceType;
        private string invoiceNo;
        private bool cODPaymentModeCheck;
        private string checkNo;

        public RouteSettlementModel()
        {
            ActivityStart = DateTime.SpecifyKind(ActivityStart, DateTimeKind.Utc);
            ActivityEnd = DateTime.SpecifyKind(ActivityEnd, DateTimeKind.Utc);
        }
        public string ActivityID
        {
            get;
            set;
        }
        public bool IsTxActivity
        {
            get;
            set;
        }
        public string ActivityType
        {
            get;
            set;
        }
        public string ActivityDescription
        {
            get;
            set;
        }

        public string ActivityHeaderID
        {
            get;
            set;
        }

        public string RouteID
        {
            get;
            set;
        }

        public string ActivityDetailClass
        {
            get;
            set;
        }

        public string ActivityDetails
        {
            get;
            set;
        }

        public string ActivityStatus
        {
            get;
            set;
        }
        public DateTime ActivityStart
        {
            get;
            set;
        }
        public DateTime ActivityEnd
        {
            get;
            set;
        }

        public string StopInstanceID
        {
            get;
            set;
        }

        public string CustomerID
        {
            get;
            set;
        }

        public string SettlementID
        {
            get;
            set;
        }

        public int ParentID
        {
            get;
            set;
        }


        public string CheckNo
        {
            get { return checkNo; }
            set { checkNo = value; }
        }

       

        public bool CODPaymentModeCheck
        {
            get { return cODPaymentModeCheck; }
            set { cODPaymentModeCheck = value; }
        }


        public string InvoiceNo
        {
            get { return invoiceNo; }
            set { invoiceNo = value; }
        }

        public string Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public string InvoiceType
        {
            get { return invoiceType; }
            set { invoiceType = value; }
        }

        public string PaymentTerm
        {
            get { return paymentTerm; }
            set { paymentTerm = value; }
        }

        public string MoneyOrderNo
        {
            get { return moneyOrderNo; }
            set { moneyOrderNo = value; }
        }
        private string moneyOrerFeeAmnt;

        public string MoneyOrerFeeAmnt
        {
            get { return moneyOrerFeeAmnt; }
            set { moneyOrerFeeAmnt = value; }
        }
        public string ExpenseType
        {
            get { return expenseType; }
            set { expenseType = value; }
        }

        public string ExpenseDescription
        {
            get { return expenseDescription; }
            set { expenseDescription = value; }
        }


        public string ShipTo
        {
            get { return shipTo; }
            set { shipTo = value; }
        }

        public string BillTo
        {
            get { return billTo; }
            set { billTo = value; }
        }

        public string FromBranch { get; set; }
        public string ToBranch { get; set; }
        public string ReplnType { get; set; }
        public string ReplnID { get; set; }
        public string RequestedBy { get; set; }
    }

    public class RouteSettlementPendingList
    {
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string StopDate { get; set; }
    }
}
