﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class CashDelivery : Helpers.ModelBase
    {
        public List<PaymentARModel.InvoiceDetails> ARDocumentId = new List<PaymentARModel.InvoiceDetails>();
        decimal _PreviousBalanceAmount;
        decimal _CurrentInvoiceAmount;
        decimal _TotalBalanceAmount;
        string _PaymentAmount;
        bool _PaymentMode;
        string _ChequeNo;
        string _ChequeNoId;

        public string ChequeNoId
        {
            get { return _ChequeNoId; }
            set { _ChequeNoId = value;
            OnPropertyChanged("ChequeNoId");
            }
        }
        DateTime _ChequeDate;
        Boolean _IsCashDeliveryAllowed;
        decimal _CreditLimit;
        Boolean _IsApprovalCodeNeeded;
        Boolean _IsChequeNoRequired;

        public decimal CreditLimit
        {
            get
            {
                return _CreditLimit;
            }
            set
            {
                _CreditLimit = value;
                OnPropertyChanged("CreditLimit");
                //OnPropertyChanged("IsApprovalCodeNeeded");
            }
        }
        public Boolean IsChequeNoRequired
        {
            get
            {
                if (_PaymentMode && string.IsNullOrEmpty(_ChequeNo))
                {
                    return true;
                }
                else if (_PaymentMode && !string.IsNullOrEmpty(_ChequeNo) && _ChequeNo.Length < 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                _IsChequeNoRequired = value;
                OnPropertyChanged("IsChequeNoRequired");

            }
        }

        public Boolean IsApprovalCodeNeeded
        {
            get
            {
                return PreviousBalanceAmount > 0 ? true : false;
            }
            set
            {
                _IsApprovalCodeNeeded = value;
                OnPropertyChanged("IsApprovalCodeNeeded");

            }
        }
        public Boolean IsCashDeliveryAllowed
        {
            get
            {
                if (!string.IsNullOrEmpty(PaymentAmount))
                {
                    bool IsActivate = false;
                    IsActivate = CurrentInvoiceAmount.ToString("F2") == Convert.ToDouble(PaymentAmount).ToString("F2") ? true : false;
                    if(!IsActivate)
                        IsActivate = TotalBalanceAmount.ToString("F2") == Convert.ToDouble(PaymentAmount).ToString("F2") ? true : false;

                    return IsActivate;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                _IsCashDeliveryAllowed = value;
                OnPropertyChanged("IsCashDeliveryAllowed");
            }
        }

        public decimal PreviousBalanceAmount
        {
            get
            {
                return _PreviousBalanceAmount;
            }
            set
            {
                _PreviousBalanceAmount = value;
                OnPropertyChanged("PreviousBalanceAmount");

            }
        }
        public decimal CurrentInvoiceAmount
        {
            get
            {
                return _CurrentInvoiceAmount;
            }
            set
            {
                _CurrentInvoiceAmount = value;
                OnPropertyChanged("CurrentInvoiceAmount");

            }
        }
        public decimal TotalBalanceAmount
        {
            get
            {
                return _TotalBalanceAmount;
            }
            set
            {
                _TotalBalanceAmount = value;
                OnPropertyChanged("TotalBalanceAmount");
                OnPropertyChanged("IsCashDeliveryAllowed");

            }
        }
        public string PaymentAmount
        {
            get
            {
                return _PaymentAmount;
            }
            set
            {
                _PaymentAmount = value;
                OnPropertyChanged("PaymentAmount");
                OnPropertyChanged("IsCashDeliveryAllowed");
            }
        }
        //0=Cash,1=Cheque
        public bool PaymentMode
        {
            get
            {
                if (!_PaymentMode)
                {
                    ChequeNo = "";
                }
                return _PaymentMode;
            }
            set
            {
                _PaymentMode = value;
                OnPropertyChanged("PaymentMode");
                OnPropertyChanged("ChequeNo");
                OnPropertyChanged("IsChequeNoRequired");
            }
        }
        public string ChequeNo
        {
            get
            {
                return _ChequeNo;
            }
            set
            {
                _ChequeNo = value;
                OnPropertyChanged("ChequeNo");
                OnPropertyChanged("IsChequeNoRequired");
            }
        }
        public DateTime ChequeDate
        {
            get
            {
                return _ChequeDate;
            }
            set
            {
                _ChequeDate = value;
                OnPropertyChanged("ChequeDate");

            }
        }

    }
}
