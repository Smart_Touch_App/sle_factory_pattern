﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class HeldReturnLogActivity
    {
        private string replnID = string.Empty;

        public string ReplnID
        {
            get { return replnID; }
            set { replnID = value; }
        }

        private string replnType = string.Empty;

        public string ReplnType
        {
            get { return replnType; }
            set { replnType = value; }
        }

        private string fromBranch = string.Empty;

        public string FromBranch
        {
            get { return fromBranch; }
            set { fromBranch = value; }
        }


        private string toBranch = string.Empty;

        public string ToBranch
        {
            get { return toBranch; }
            set { toBranch = value; }
        }

        private string requedtedBy = string.Empty;

        public string RequedtedBy
        {
            get { return requedtedBy; }
            set { requedtedBy = value; }
        }
    }
}
