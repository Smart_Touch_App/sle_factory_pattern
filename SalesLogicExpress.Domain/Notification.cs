﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class Notification : Helpers.ModelBase
    {
        
        int _NotificationID;
        public int NotificationID
        {
            get
            {
                return _NotificationID;
            }
            set
            {
                _NotificationID = value;
                OnPropertyChanged("NotificationID");
            }
        }
        
        int  _RouteID;
        public int  RouteID
        {
            get
            {
                return _RouteID;
            }
            set
            {


                _RouteID = value;
                OnPropertyChanged("RouteID");
            }
        }
        string _Title;
        public string Title
        {
            get
            {
                return _Title;
            }
            set
            {


                _Title = value;
                OnPropertyChanged("Title");
            }
        }
        string _Description;
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {


                _Description = value;
                OnPropertyChanged("Description");
            }
        }
        DateTime _ModifiedDate;
        public DateTime ModifiedDate
        {
            get
            {
                return _ModifiedDate;
            }
            set
            {


                _ModifiedDate = value;
                OnPropertyChanged("ModifiedDate");
            }
        }
        DateTime _CreatedDate;
        public DateTime CreatedDateTime
        {
            get
            {
                return _CreatedDate;
            }
            set
            {


                _CreatedDate = value;
                OnPropertyChanged("CreatedDateTime");
            }
        }
        string _Read;
        public string Read
        {
            get
            {
                return _Read;
            }
            set
            {


                _Read = value;
                OnPropertyChanged("Read");
            }
        }
        string _SubCategory;
        public string SubCategory
        {
            get
            {
                return _SubCategory;
            }
            set
            {


                _SubCategory = value;
                OnPropertyChanged("SubCategory");
            }
        }
        string _Category;
        public string Category
        {
            get
            {
                return _Category;
            }
            set
            {


                _Category = value;
                OnPropertyChanged("Category");
            }
        }
        DateTime _ValidTill;
        public DateTime ValidTill
        {
            get
            {
                return _ValidTill;
            }
            set
            {


                _ValidTill = value;
                OnPropertyChanged("ValidTill");
            }
        }
        string _Acknowledged;
        public string Acknowledged
        {
            get
            {
                return _Acknowledged;
            }
            set
            {


                _Acknowledged = value;
                OnPropertyChanged("Acknowledged");
            }
        }
        string _TypeID;
        public string TypeID
        {
            get
            {
                return _TypeID;
            }
            set
            {
                _TypeID = value;
                OnPropertyChanged("TypeID");
            }
        }
        string _Indicator;
        public string Indicator
        {
            get
            {
                return _Indicator;
            }
            set
            {


                _Indicator = value;
                OnPropertyChanged("Indicator");
            }
        }
        int _Initiator;
        public int Initiator
        {
            get
            {
                return _Initiator;
            }
            set
            {


                _Initiator = value;
                OnPropertyChanged("Initiator");
            }
        }
        string _Type;
        public string Type
        {
            get
            {
                return _Type;
            }
            set
            {


                _Type = value;
                OnPropertyChanged("Type");
            }
        }
        
        string _NotificationAge;
        public string NotificationAge
        {
            get
            {
                return _NotificationAge;
            }
            set
            {


                _NotificationAge = value;
                OnPropertyChanged("NotificationAge");
            }
        }
    }
}
