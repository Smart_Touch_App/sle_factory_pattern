﻿using SalesLogicExpress.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class CustomerInformation : Customer
    {
        string _Address1, _Address2, _Address3, _Address4, _MailingName, _TaxExemptCertificate, _CustomerPo;
        bool isSaveAllowed = false;
        bool isSaveButtonUsed = false;
        public Boolean IsSaveButtonUsed
        {
            get { return isSaveButtonUsed; }
            set { isSaveButtonUsed = value; OnPropertyChanged("IsSaveButtonUsed"); }
        }
        public Boolean IsSaveAllowed
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(Address1) && !string.IsNullOrWhiteSpace(CustInfoCity) && !string.IsNullOrWhiteSpace(CustInfoState) && !string.IsNullOrWhiteSpace(CustInfoZip) && CustInfoZip.Length >= 5)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                isSaveAllowed = value;
                OnPropertyChanged("IsSaveAllowed");
            }
        }
        [Required(AllowEmptyStrings = false, ErrorMessage = " ")]
        public string CustInfoZip
        {
            get
            {
                return _CustInfoZip;
            }
            set
            {
                _CustInfoZip = value;
                OnPropertyChanged("IsSaveAllowed");
                OnPropertyChanged("CustInfoZip");
               
            }
        }
        [Required(AllowEmptyStrings = false, ErrorMessage = " ")]
        public string CustInfoCity
        {
            get
            {
                return _CustInfoCity;
            }
            set
            {
                _CustInfoCity = value;
                OnPropertyChanged("IsSaveAllowed");
                OnPropertyChanged("CustInfoCity");
              
            }
        }
        [Required(AllowEmptyStrings = false, ErrorMessage = " ")]
        public string CustInfoState
        {
            get
            {
                return _CustInfoState;
            }
            set
            {
                _CustInfoState = value;
                OnPropertyChanged("IsSaveAllowed");
                OnPropertyChanged("CustInfoState");
              
            }
        }
        [Required(AllowEmptyStrings = false, ErrorMessage = " ")]
        public string Address1
        {
            get
            {
                return _Address1;
            }
            set
            {
                _Address1 = value;
                OnPropertyChanged("IsSaveAllowed");
                OnPropertyChanged("Address1");
             
            }
        }
        public string Address2
        { 
            get { return _Address2; } 
            set { _Address2 = value; OnPropertyChanged("Address2");  }
        }
        public string Address3 
        {
            get { return _Address3; }
            set { _Address3 = value;
            OnPropertyChanged("Address3");
            }
        }
        public string Address4 
        {
            get { return _Address4; }
            set { _Address4 = value; OnPropertyChanged("Address4");
            }
        }
        public string MailingName 
        {
            get { return _MailingName; }
            set { _MailingName = value; OnPropertyChanged("MailingName");
            }
        }
        public string TaxExemptCertificate 
        {
            get { return _TaxExemptCertificate; }
            set { _TaxExemptCertificate = value; OnPropertyChanged("TaxExemptCertificate");
            }
        }
        public string CustomerPo
        {
            get { return _CustomerPo; }
            set { _CustomerPo = value; OnPropertyChanged("CustomerPo"); }
        }

        public string SearchTypeCode { get; set; }
        public string OperatingUnitCode { get; set; }
        public string ParentCode { get; set; }
        public string RegionCode { get; set; }
        public string BillToCode { get; set; }
        public string DistrictCode { get; set; }
        public string RemitToCode { get; set; }
        public string BranchCode { get; set; }
        public string KAMNAMStreetCode { get; set; }
        public string ChainCode { get; set; }
        public string KindOfBusinessAccountCode { get; set; }
        public string RouteCode { get; set; }
        public string FreightHandlingCode { get; set; }
        public string StopCode { get; set; }


        public string SearchTypeDesc { get; set; }
        public string OperatingUnitDesc { get; set; }
        public string ParentDesc { get; set; }
        public string RegionDesc { get; set; }
        public string BillToDesc { get; set; }
        public string DistrictDesc { get; set; }
        public string RemitToDesc { get; set; }
        public string BranchDesc { get; set; }
        public string KAMNAMStreetDesc { get; set; }
        public string ChainDesc { get; set; }
        public string KindOfBusinessAccountDesc { get; set; }
        public string RouteDesc { get; set; }
        public string FreightHandlingCodeDesc { get; set; }
        public string StopCodeDesc { get; set; }

        string _CustInfoState;
        string _CustInfoCity;
        string _CustInfoZip;
    }
}
