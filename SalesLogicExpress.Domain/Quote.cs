﻿//************************************************************************************************
// Comment: Double dastatypes changed to decimal as part from nimesh consideration
// Created: feb 22, 2016
// Author: Vivensas (Rajesh,Yuvaraj)
// Revisions: 
//*************************************************************************************************
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    /// <summary>
    /// Represents the customer quote header 
    /// </summary>
    public class QuoteHeader: Helpers.ModelBase
    {
        #region Variable and object declaration 

        private bool _IsPrinted = false; 
        private DateTime _QuoteDate = DateTime.Now;
        private PricingDetails _MasterPriceSetup = null;
        private PricingDetails _OverridePriceSetup = null;
        private PricingDetails _MasterPriceCopySetup = null;
        private bool _IsNewQuote = false; 
        #endregion 

        #region Properties

        /// <summary>
        /// Get or set the customer/propect no
        /// </summary>
        /// 
        public decimal DocumentId { get; set; }

        public int CustomerId { get; set; }

        /// <summary>
        /// Get or set the flag to determine customer/prospect
        /// </summary>
        public bool IsProspect { get; set; }

        /// <summary>
        /// Get or set the flag to determine the new quote 
        /// </summary>
        public bool IsNewQuote
        {
            get { return _IsNewQuote; }
            set
            {
                _IsNewQuote = value;
                OnPropertyChanged("IsNewQuote");
            }
        }

        /// <summary>
        /// Get or set the quote if 
        /// </summary>
        public int QuoteId { get; set; }

        /// <summary>
        /// Get or set parent detail of the customer 
        /// </summary>
        public string Parent { get; set; }


        /// <summary>
        /// Get or set bill to detail of the customer
        /// </summary>
        public string BillTo { get; set; }

        /// <summary>
        /// Get or set quote date 
        /// </summary>
        public DateTime QuoteDate
        {
            get { return _QuoteDate; }
            set { _QuoteDate = value; }
        }

        public bool IsPrinted
        {
            get { return _IsPrinted; }
            set { _IsPrinted = value; }
        }
        /// <summary>
        /// Get or set master price setup 
        /// </summary>
        public PricingDetails MasterPriceSetup
        {
            get
            {
                if(_MasterPriceSetup==null)
                {
                    _MasterPriceSetup = new PricingDetails();
                    //ToDo - Write code to get master price for the customer 
                }

                return _MasterPriceSetup;
            }
            set
            {
                _MasterPriceSetup = value;
            }
        }


        //public PricingDetails MasterPriceCopySetup
        //{
        //    get
        //    {
        //        if (_MasterPriceCopySetup == null)
        //        {
        //            _MasterPriceCopySetup = new PricingDetails();
        //            //ToDo - Write code to get master price for the customer 
        //        }

        //        return _MasterPriceCopySetup;
        //    }
        //    set
        //    {
        //        _MasterPriceCopySetup = value;
        //    }
        //}


        /// <summary>
        /// Get or set overriden price setup
        /// </summary>
        public PricingDetails OverridePriceSetup
        {
            get
            {
                if (_OverridePriceSetup == null)
                {
                    _OverridePriceSetup = new PricingDetails();
                    //ToDo - Write code to get master price for the customer 
                }

                return _OverridePriceSetup;
            }
            set
            {
                _OverridePriceSetup = value;
            }
        }

        private string quoteIdString = "";

        public string QuoteIdString
        {
            get { return quoteIdString; }
            set { quoteIdString = value; OnPropertyChanged("QuoteIdString"); }
        }
        private string quotePersonName = "";

        public string QuotePersonName
        {
            get { return quotePersonName; }
            set { quotePersonName = value; OnPropertyChanged("QuotePersonName"); }
        }
        private string quotePersonAddress = "";

        public string QuotePersonAddress
        {
            get { return quotePersonAddress; }
            set { quotePersonAddress = value; OnPropertyChanged("QuotePersonAddress"); }
        }
        private string quotePersonCity = "";

        public string QuotePersonCity
        {
            get { return quotePersonCity; }
            set { quotePersonCity = value; OnPropertyChanged("QuotePersonCity"); }
        }
        private string quotePersonState = "";

        public string QuotePersonState
        {
            get { return quotePersonState; }
            set { quotePersonState = value; OnPropertyChanged("QuotePersonState"); }
        }
        private string quotePersonZip = "";

        public string QuotePersonZip
        {
            get { return quotePersonZip; }
            set { quotePersonZip = value; OnPropertyChanged("QuotePersonZip"); }
        }
        private string quotePersonPhone = "";

        public string QuotePersonPhone
        {
            get { return quotePersonPhone; }
            set { quotePersonPhone = value; OnPropertyChanged("QuotePersonPhone"); }
        }


        private bool isSampled = false;

        public bool IsSampled
        {
            get { return isSampled; }
            set { isSampled = value; OnPropertyChanged("IsSampled"); }
        }

        /// <summary>
        /// Get or set flag to indicate whether the quote is settled or not
        /// </summary>
        public bool IsSettled { get; set; }

        #endregion 

    }

    /// <summary>
    /// Represents the customer quote details model 
    /// </summary>
    public class QuoteDetail: Helpers.ModelBase
    {
        #region Properties

        private int quoteHeaderId = -1;
        /// <summary>
        /// Get or set the quote details id
        /// </summary>
        public int QuoteHeaderId
        {
            get { return quoteHeaderId; }
            set { quoteHeaderId = value; }
        }

        private int quoteDetailId = -1;

        
        /// <summary>
        /// Get or set the quote details id
        /// </summary>
        public int QuoteDetailId
        {
            get { return quoteDetailId; }
            set { quoteDetailId = value; OnPropertyChanged("QuoteDetailId"); }
        }

        private string itemId = string.Empty;

        public string ItemId
        {
            get { return itemId; }
            set { itemId = value; }
        }
        /// <summary>
        /// Get or set the item id
        /// </summary>

        private string itemNumber = string.Empty;

        public string ItemNumber
        {
            get { return itemNumber; }
            set { itemNumber = value; }
        }
        /// <summary>
        /// Get or set the item id
        /// </summary>
        private string itemDescription = string.Empty;
        /// <summary>
        /// Get or set the item description 
        /// </summary>
        public string ItemDescription
        {
            get { return itemDescription; }
            set { itemDescription = value; }
        }

        private int quoteQuantity = -1;        
        /// <summary>
        /// Get or set quoted quantity 
        /// </summary>
        public int QuoteQuantity
        {
            get { return quoteQuantity; }
            set { quoteQuantity = value; OnPropertyChanged("QuoteQuantity"); }
        }

        private string uOM = string.Empty;
        /// <summary>
        /// Get or set the primary UOM 
        /// </summary>
        public string UOM
        {
            get { return uOM; }
            set { uOM = value; OnPropertyChanged("UOM"); }
        }

        private decimal unitPrice = 0; 

        public decimal UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value;  }
        }

        private string transactionUOM = string.Empty;
        /// <summary>
        /// Get or set transaction UOM 
        /// </summary>
        public string TransactionUOM
        {
            get { return transactionUOM; }
            set { transactionUOM = value; OnPropertyChanged("TransactionUOM"); }
        }

        private string pricingUOM = string.Empty;
        /// <summary>
        /// Get or set pricing UOM 
        /// </summary>
        public string PricingUOM
        {
            get { return pricingUOM; }
            set { pricingUOM = value; OnPropertyChanged("PricingUOM"); }
        }
        private string primaryUOM = string.Empty;

        public string PrimaryUOM
        {
            get { return primaryUOM; }
            set { primaryUOM = value; OnPropertyChanged("PrimaryUOM"); }
        }

        private string otherUOM = string.Empty;

       
        /// <summary>
        /// Get or set other UOM 
        /// </summary>
        public string OtherUOM
        {
            get { return otherUOM; }
            set { otherUOM = value; OnPropertyChanged("OtherUOM"); }
        }

        private decimal transactionUOMPrice = 0;

       
        /// <summary>
        /// Get or set transaction UOM price
        /// </summary>
        public decimal TransactionUOMPrice
        {
            get { return transactionUOMPrice; }
            set { transactionUOMPrice = value; OnPropertyChanged("TransactionUOMPrice"); }
        }

        private decimal pricingUOMPrice = 0;

       
        /// <summary>
        /// Get or set Pricing UOM price 
        /// </summary>
        public decimal PricingUOMPrice
        {
            get { return pricingUOMPrice; }
            set { pricingUOMPrice = value; OnPropertyChanged("PricingUOMPrice"); }
        }

        private decimal otherUOMPrice = 0;
        /// <summary>
        /// Get or set pricing UOM price 
        /// </summary>
        public decimal OtherUOMPrice
        {
            get { return otherUOMPrice; }
            set { otherUOMPrice = value; OnPropertyChanged("OtherUOMPrice"); }
        }

        private string competitorUOM = string.Empty;
        /// <summary>
        /// Get or set the competitors UOM 
        /// </summary>
        public string CompetitorUOM
        {
            get { return competitorUOM; }
            set { competitorUOM = value; OnPropertyChanged("CompetitorUOM"); }
        }

        private string competitorName = string.Empty;
        /// <summary>
        /// Get or set the competitor name 
        /// </summary>
        public string CompetitorName
        {
            get { return competitorName; }
            set { competitorName = value; OnPropertyChanged("CompetitorName"); }
        }

        private double competitorPrice = 0;
        /// <summary>
        /// Get or set competitor price
        /// </summary>
        public double CompetitorPrice
        {
            get { return competitorPrice; }
            set { competitorPrice = value; OnPropertyChanged("CompetitorPrice"); }
        }

        private int availableQty = -1;
        /// <summary>
        /// Get or set Available Qty
        /// </summary>
        public int AvailableQty
        {
            get { return availableQty; }
            set { availableQty = value; OnPropertyChanged("AvailableQty"); MaxSampleQty = value; }
        }

        private ObservableCollection<QtyUOMClass> qtyUOMCollection = new ObservableCollection<QtyUOMClass>();
        public ObservableCollection<QtyUOMClass> QtyUOMCollection
        {
            get { return qtyUOMCollection; }
            set { qtyUOMCollection = value; }
        }

        private QtyUOMClass selectedQtyUOM = new QtyUOMClass();

        public QtyUOMClass SelectedQtyUOM
        {
            get { return selectedQtyUOM; }
            set
            {
                selectedQtyUOM = value; OnPropertyChanged("SelectedQtyUOM");
            }
        }


        private int sampleQty = 0;
        public int SampleQty
        {
            get { return sampleQty; }
            set { sampleQty = value; OnPropertyChanged("SampleQty");
            if (SampleQty==0)
            {
                SelectedQtyUOMSample = new QtyUOMClass();SampleUOM = "";
            }
            }
        }

        private string sampleUOM = string.Empty;

        public string SampleUOM
        {
            get { return sampleUOM; }
            set { sampleUOM = value; OnPropertyChanged("SampleUOM");
            MaxSampleQty = SampleUOM == "loose" || string.IsNullOrEmpty(SampleUOM) ? 9999 : (AvailableQty / Convert.ToInt32 (SelectedQtyUOMSample.UMMultipler));
            }
        }



        private ObservableCollection<QtyUOMClass> qtyUOMCollectionSample = new ObservableCollection<QtyUOMClass>();

        public ObservableCollection<QtyUOMClass> QtyUOMCollectionSample
        {
            get { return qtyUOMCollectionSample; }
            set { qtyUOMCollectionSample = value; OnPropertyChanged("QtyUOMCollectionSample"); }
        }


        private QtyUOMClass selectedQtyUOMSample = new QtyUOMClass();

        public QtyUOMClass SelectedQtyUOMSample
        {
            get { return selectedQtyUOMSample; }
            set { selectedQtyUOMSample = value; OnPropertyChanged("SelectedQtyUOMSample");
            MaxSampleQty = SelectedQtyUOMSample.UMCode == "loose" || string.IsNullOrEmpty(SelectedQtyUOMSample.UMCode) ? 9999 : (AvailableQty / Convert.ToInt32 (SelectedQtyUOMSample.UMMultipler));
            }
        }

        private bool isVisibleTransactionUM = false;

        public bool IsVisibleTransactionUM
        {
            get { return isVisibleTransactionUM; }
            set { isVisibleTransactionUM = value; OnPropertyChanged("IsVisibleTransactionUM"); }
        }
        private bool isVisibleOtherUM = false;
        public bool IsVisibleOtherUM
        {
            get { return isVisibleOtherUM; }
            set { isVisibleOtherUM = value; OnPropertyChanged("IsVisibleOtherUM"); }
        }

        private int pickedQty = 0;
        public int PickedQty
        {
            get { return pickedQty; }
            set { pickedQty = value; OnPropertyChanged("PickedQty"); }
        }

        private string pickedUOM = string.Empty;

        public string PickedUOM
        {
            get { return pickedUOM; }
            set { pickedUOM = value; OnPropertyChanged("PickedUOM"); }
        }

        private int maxSampleQty = 0;

        public int MaxSampleQty
        {
            get { return maxSampleQty; }
            set { maxSampleQty = value; OnPropertyChanged("MaxSampleQty"); }
        }
        #endregion
    }
}
