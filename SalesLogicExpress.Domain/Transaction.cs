﻿using SalesLogicExpress.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesLogicExpress.Domain
{
    public class Transaction
    {
        public int TransactionID
        {
            get;
            set;
        }

        public string RouteID
        {
            get;
            set;
        }

        public string TransactionType
        {
            get;
            set;
        }

        public string TransactionDetailClass
        {
            get;
            set;
        }

        public string TransactionDetails
        {
            get;
            set;
        }

        public DateTime TransactionStart
        {
            get;
            set;
        }

        public DateTime TransactionEnd
        {
            get;
            set;
        }

        public string StopInstanceID
        {
            get;
            set;
        }

        public string CustomerID
        {
            get;
            set;
        }

        public string SettlementID
        {
            get;
            set;
        }

        public int ParentTransactionID
        {
            get;
            set;
        }
    }

    public class TransactionSyncDetail
    {
        public string TableName { get; set; }
    }

    public enum SyncDownloadType
    {
        Both,
        Upload,
        Download
    }

    public class QueueProfileMapping
    {
        public int QueueID { get; set; }
        public string QueueName { get; set; }
        public int ProfileID { get; set; }
        public string ProfileName { get; set; }
        public string PublicationName { get; set; }
    }

    public class QueueType
    {
        public string Priority { get; set; }
    }

    public class SyncQueueDetails
    {
        public int SyncQueueId { get; set; }
        public int QueueMasterID { get; set; }
        public string QueueName { get; set; }
        public int ProfileID { get; set; }
        public string ProfileName { get; set; }
        public bool BlockUI { get; set; }
        public string SyncStatus { get; set; }
        public int FailCount { get; set; }
        public string PublicationName { get; set; }
        public string SyncDirection { get; set; }
    }

    public class SyncQueueChangedEventArgs : EventArgs
    {
        public List<Domain.TransactionSyncDetail> QueueItems { get; set; }
        public DateTime StateChangedTime { get; set; }
        public SyncDownloadType syncDownloadtype { get; set; }
        public string profileName { get; set; }
        public bool IsManualSync { get; set; }
    }

    public class SyncUpdatedEventArgs : EventArgs
    {
        public SyncUpdatedEventArgs()
        {
            StateChangedTime = DateTime.SpecifyKind(StateChangedTime, DateTimeKind.Utc);
        }
        public Domain.TransactionSyncDetail SyncActivity { get; set; }
        public SyncUpdateType State { get; set; }
        public List<Domain.TransactionSyncDetail> Queue { get; set; }
        public DateTime StateChangedTime { get; set; }
        public string ProfileName { get; set; }
        public bool IsManualSync { get; set; }
    }

    public enum SyncUpdateType
    {
        Initiated,
        Idle,
        UploadComplete,
        DownloadComplete,
        SyncComplete,
        SyncFailed,
        ServerNotReachable
    }

}
