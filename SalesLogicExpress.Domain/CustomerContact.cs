﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace SalesLogicExpress.Domain
{
    public class CustomerContact : Helpers.ModelBase, IDataErrorInfo
    {
        public string CustomerID { get; set; }

        public string LineID { get; set; }
        public string LineNo { get; set; }
        public string RelatedPersonID { get; set; }
        public string ContactID { get; set; }
        public string ContactName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string ContactTitle { get; set; }
        //public ObservableCollection<Title> TitleList { get; set; }
        public ObservableCollection<Email> EmailList { get; set; }
        public Email DefaultEmail { get; set; }
        public Phone DefaultPhone { get; set; }
        //public Title DefaultTitle { get; set; }
        public ObservableCollection<Phone> PhoneList { get; set; }
        bool _IsExpanded = false;
        public bool IsExpanded
        {
            get
            {
                return _IsExpanded;
            }
            set
            {
                _IsExpanded = value;
                OnPropertyChanged("IsExpanded");
            }
        }
        public bool IsDefault { get; set; }
        private bool showOnDashboard;
        public bool ShowOnDashboard
        {
            get
            {
                return this.showOnDashboard;
            }
            set
            {
                this.showOnDashboard = value;
                OnPropertyChanged("ShowOnDashboard");
            }
        }
        //public bool ShowOnDashboard { get; set; }
        public string ReferenceContact { get; set; }
        public bool IsActive { get; set; }
        private bool _TogglePhoneImg = true;
        public bool TogglePhoneImg
        {
            get
            {
                return _TogglePhoneImg;
            }
            set
            {
                _TogglePhoneImg = value;
                OnPropertyChanged("TogglePhoneImg");
            }
        }
        private bool _ToggleEmailImg = true;
        public bool ToggleEmailImg
        {
            get
            {
                return _ToggleEmailImg;
            }
            set
            {
                _ToggleEmailImg = value;
                OnPropertyChanged("ToggleEmailImg");
            }
        }
        bool _IsSelected = false;
        public bool IsSelected
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                _IsSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }
        public override string ToString()
        {
            return string.Concat("ContactID:", ContactID, ", FirstName:", FirstName, ", MiddleName:", MiddleName, ", LastName:", LastName, ", ContactTitle:", ContactTitle, ", IsDefault:", ShowOnDashboard, " ");
        }
        public string Error
        {
            get { return null; }
        }
        public string this[string columnName]
        {
            get { return null; }
        }
    }
    public class Email : Helpers.ModelBase
    {
        public int Index { get; set; }
        public string ContactID { get; set; }
        public string FormattedValue
        {
            get
            {
                return Value.Trim();
            }
        }
        public string Value { get; set; }
        public string Type { get; set; }
        public int TypeID { get; set; }
        public bool IsDefault { get; set; }
        bool _IsSelected;
        public bool IsSelectedEmail
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                _IsSelected = value;
                OnPropertyChanged("IsSelectedEmail");
            }
        }
        public override string ToString()
        {
            return string.Concat("Type:", Type, ", Value:", Value, ", IsDefault:", IsDefault, " ");
        }
    }
    public class Phone : Helpers.ModelBase
    {
        public int Index { get; set; }
        public string ContactID { get; set; }
        public string FormattedValue
        {
            get
            {
                return  (AreaCode.Trim().Length >0 ? "(" + AreaCode + ")" : "") + Value.Trim() + (Extension.Trim().Length >0 ? string.Format(" {0}",Extension) : "");
            }
        }
        public string Value { get; set; }
        public string Type { get; set; }
        public int TypeID { get; set; }
        public string Extension { get; set; }
        public string AreaCode { get; set; }
        public bool IsDefault { get; set; }
        bool _IsSelected;
        public bool IsSelectedPhone
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                _IsSelected = value;
                OnPropertyChanged("IsSelectedPhone");
            }
        }
        public override string ToString()
        {
            return string.Concat("Type:", Type, ", Value:", Value, ", IsDefault:", IsDefault, ", AreaCode:", AreaCode, ", Extension:", Extension);
        }
    }
    public class Title : Helpers.ModelBase
    {
        public string ContactID { get; set; }
        public string Type { get; set; }
        public int TypeID { get; set; }
        public string Value { get; set; }
        bool _IsSelected;
        public bool IsSelectedTitle
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                _IsSelected = value;
                OnPropertyChanged("IsSelectedTitle");
            }
        }
        public bool IsDefault { get; set; }
    }
}
