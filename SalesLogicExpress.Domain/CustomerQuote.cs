﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class CustomerQuoteHeader:QuoteHeader
    {
        private ObservableCollection<CustomerQuoteDetail> _Items = null;

        public ObservableCollection<CustomerQuoteDetail> Items
         {
            get
            {
                if (_Items == null)
                {
                    _Items = new ObservableCollection<CustomerQuoteDetail>();
                }

                return _Items;
            }
            set
            {
                _Items = value;
            }
        }
    }

    public class CustomerQuoteDetail:QuoteDetail
    {

    }
}
