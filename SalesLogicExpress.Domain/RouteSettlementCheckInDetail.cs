﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class RouteSettlementCheckInDetail
    {
        public string OrderNumber { get; set; }
        public string OrderType { get; set; }
        public string ShipTo { get; set; }
        public string BillTo { get; set; }
        public string OrderTotalAmt { get; set; }
        public string SalesTaxAmt { get; set; }
        public string InvoiceTotalAmt { get; set; }
        public string SurchargeAmt { get; set; }
        public string DeliverySeq { get; set; }
        public string PaymentTerm { get; set; }
        public string BillToID { get; set; }
        public string ShipToID { get; set; }
        public string OrderDate { get; set; }
        public string PaymentTime { get; set; }
    }
}
