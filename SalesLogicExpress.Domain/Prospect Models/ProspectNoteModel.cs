﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain.Prospect_Models
{
    public class ProspectNoteModel : Helpers.ModelBase, IDataErrorInfo
    {
        public string ProspectID { get; set; }
        public string ProspectNoteID { get; set; }
        public string DateTime { get; set; }
        public string ProspectNoteDetails { get; set; }
        public string ProspectNoteEnteredBy { get; set; }
        bool _IsDefault = false;
        public bool IsDefault
        {
            get
            {
                return _IsDefault;
            }
            set
            {
                _IsDefault = value;
                OnPropertyChanged("IsDefault");
            }
        }
        bool _IsSelected = false;
        public bool IsSelected
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                _IsSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }
        public override string ToString()
        {
            return string.Concat("NoteID:", ProspectNoteID, "DateTime:", DateTime, "NoteDetails:", ProspectNoteDetails, "IsDefault:", IsDefault);
        }
        public string Error
        {
            get { return null; }
        }
        public string this[string columnName]
        {
            get { return null; }
        }

    }
}
