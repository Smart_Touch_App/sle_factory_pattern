﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Domain
{
    public class MoneyOrder : Helpers.ModelBase
    {
        private string moneyOrderID;

        public string MoneyOrderID
        {
            get { return moneyOrderID; }
            set { moneyOrderID = value; OnPropertyChanged("MoneyOrderID"); }
        }

        private string routeID;
        public string RouteID
        {
            get { return routeID; }
            set { routeID = value; }
        }

        private string statusID;
        public string StatusID
        {
            get { return statusID; }
            set { statusID = value; OnPropertyChanged("StatusID"); }
        }

        private string routeSettlementId;
        public string RouteSettlementId
        {
            get { return routeSettlementId; }
            set { routeSettlementId = value; OnPropertyChanged("RouteSettlementId"); }
        }

        private string statusCode;
        public string StatusCode
        {
            get { return statusCode; }
            set { statusCode = value; OnPropertyChanged("StatusCode"); }
        }

        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; OnPropertyChanged("Status"); }
        }

        private string moneyOrderNumber;
        public string MoneyOrderNumber
        {
            get { return moneyOrderNumber; }
            set { moneyOrderNumber = value; OnPropertyChanged("MoneyOrderNumber"); }
        }

        private string moneyOrderAmount;
        public string MoneyOrderAmount
        {
            get { return moneyOrderAmount; }
            set { moneyOrderAmount = value; OnPropertyChanged("MoneyOrderAmount"); }
        }

        private string feeAmount;
        public string FeeAmount
        {
            get { return feeAmount; }
            set { feeAmount = value; OnPropertyChanged("FeeAmount"); }
        }

        private DateTime moneyOrderDate;
        public DateTime MoneyOrderDate
        {
            get { return moneyOrderDate; }
            set { moneyOrderDate = value; OnPropertyChanged("MoneyOrderDate"); }
        }

        private string totalAmount;
        public string TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; OnPropertyChanged("TotalAmount"); }
        }

        private string voidReasonID;
        public string VoidReasonID
        {
            get { return voidReasonID; }
            set { voidReasonID = value; OnPropertyChanged("VoidReasonID"); }
        }

        private string voidReason;
        public string VoidReason
        {
            get { return voidReason; }
            set { voidReason = value; OnPropertyChanged("VoidReason"); }
        }
    }
}
