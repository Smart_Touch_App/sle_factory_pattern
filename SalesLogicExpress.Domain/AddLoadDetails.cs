﻿using SalesLogicExpress.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Domain
{
    public class AddLoadDetails : ViewModelBase
    {
        private string itemId = string.Empty;
        public string ItemId
        {
            get { return itemId; }
            set { itemId = value; }
        }


        private bool isChecked = false;
        public bool IsChecked
        {
            get { return isChecked; }
            set { isChecked = value; }
        }


        private string itemNo = string.Empty;
        public string ItemNo
        {
            get { return itemNo; }
            set { itemNo = value; }
        }



        //ItemDescription PrimaryUOM   AvailableQty           
        private string primaryUOM = string.Empty;
        public string PrimaryUOM
        {
            get { return primaryUOM; }
            set { primaryUOM = value; }
        }




        private string qtyUOM = string.Empty;
        public string QtyUOM
        {
            get { return qtyUOM; }
            set
            {
                qtyUOM = value; OnPropertyChanged("QtyUOM");

                if (QtyUOM != PrimaryUOM)
                {
                    MaxLoadQty = SelectedQtyUOM.UMMultipler > 1 && !IsFromBranchToTruck ? Convert.ToInt32(Math.Truncate(AvailableQty / SelectedQtyUOM.UMMultipler)) : 9999;
                }
                else
                {
                    MaxLoadQty = !IsFromBranchToTruck ? AvailableQty : 9999;
                }
            }
        }



        private int availableQty = 0;
        public int AvailableQty
        {
            get { return availableQty; }
            set { availableQty = value; MaxLoadQty = value; }
        }


        private int parLevel = 0;
        public int ParLevel
        {
            get { return parLevel; }
            set { parLevel = value; }
        }



        private int loadQuantity = 1;
        public int LoadQuantity
        {
            get { return loadQuantity; }
            set { loadQuantity = value; OnPropertyChanged("LoadQuantity"); }
        }


        int suggestedQty = 0;
        public int SuggestedQty
        {
            get { return suggestedQty; }
            set { suggestedQty = value; OnPropertyChanged("SuggestedQty"); }
        }


        private int shippedQty = 0;
        public int ShippedQty
        {
            get { return shippedQty; }
            set { shippedQty = value; }
        }

        private ObservableCollection<QtyUOMClass> qtyUOMCollection = new ObservableCollection<QtyUOMClass>();
        public ObservableCollection<QtyUOMClass> QtyUOMCollection
        {
            get { return qtyUOMCollection; }
            set { qtyUOMCollection = value; }
        }



        private QtyUOMClass selectedQtyUOM = new QtyUOMClass();

        public QtyUOMClass SelectedQtyUOM
        {
            get { return selectedQtyUOM; }
            set
            {
                selectedQtyUOM = value; OnPropertyChanged("SelectedQtyUOM");
                if (value != null)
                {
                    QtyUOM = SelectedQtyUOM.UMCode;
                }
            }
        }


        private string itemDescription = string.Empty;
        public string ItemDescription
        {
            get { return itemDescription; }
            set { itemDescription = value; }
        }



        private int pickedQty = 0;
        public int PickedQty
        {
            get { return pickedQty; }
            set { pickedQty = value; }
        }



        private bool isEnableGrid = true;
        public bool IsEnableGrid
        {
            get { return isEnableGrid; }
            set { isEnableGrid = value; OnPropertyChanged("IsEnableGrid"); }
        }



        private bool _IsVisible = true;
        public bool IsVisible
        {
            get { return _IsVisible; }
            set { _IsVisible = value; OnPropertyChanged("IsVisible"); }
        }



        private string salesCat1 = string.Empty;
        public string SalesCat1
        {
            get { return salesCat1; }
            set { salesCat1 = value; }
        }



        private string salesCat4 = string.Empty;
        public string SalesCat4
        {
            get { return salesCat4; }
            set { salesCat4 = value; }
        }



        private string salesCat5 = string.Empty;
        public string SalesCat5
        {
            get { return salesCat5; }
            set { salesCat5 = value; }
        }



        private string stkType = string.Empty;
        public string StkType
        {
            get { return stkType; }
            set { stkType = value; }
        }



        private int onHandQty = 0;
        public int OnHandQty
        {
            get { return onHandQty; }
            set { onHandQty = value; }
        }


        private int committedQty = 0;
        public int CommittedQty
        {
            get { return committedQty; }
            set { committedQty = value; }
        }


        private int heldQty = 0;
        public int HeldQty
        {
            get { return heldQty; }
            set { heldQty = value; }
        }


        private int demandQty = 0;
        public int DemandQty
        {
            get { return demandQty; }
            set { demandQty = value; OnPropertyChanged("DemandQty"); }
        }


        private string openReplnQty = string.Empty;  //Load Qty for item in previous suggestion replenishment
        public string OpenReplnQty
        {
            get { return openReplnQty; }
            set
            {
                openReplnQty = value;
                if (openReplnQty == "0")
                {
                    openReplnQty = "";
                }
                OnPropertyChanged("OpenReplnQty");
            }
        }



        private int maxLoadQty = 0;
        public int MaxLoadQty
        {
            get { return maxLoadQty; }
            set { maxLoadQty = value; OnPropertyChanged("MaxLoadQty"); }
        }



        //private double convesionFactor = 1;
        //public double ConvesionFactor
        //{
        //    get { return convesionFactor; }
        //    set { convesionFactor = value; }
        //}


        private int onOrderQtyInPrimaryUM = 0;
        public int OnOrderQtyInPrimaryUM
        {
            get { return onOrderQtyInPrimaryUM; }
            set { onOrderQtyInPrimaryUM = value; }
        }


        private bool isFromBranchToTruck = false;
        public bool IsFromBranchToTruck
        {
            get { return isFromBranchToTruck; }
            set { isFromBranchToTruck = value; OnPropertyChanged("IsFromBranchToTruck"); }
        }


        private bool isFocusableTextBox = true;
        public bool IsFocusableTextBox
        {
            get { return isFocusableTextBox; }
            set { isFocusableTextBox = value; OnPropertyChanged("IsFocusableTextBox"); }
        }


        private DateTime orderDate = new DateTime();
        public DateTime OrderDate
        {
            get { return orderDate; }
            set { orderDate = value; OnPropertyChanged("OrderDate");
            OrderDateString = OrderDate.ToString("MM-dd-yyyy");
            }
        }

        private string orderDateString = string.Empty;

        public string OrderDateString
        {
            get { return orderDateString; }
            set { orderDateString = value; OnPropertyChanged("OrderDateString"); }
        }

        private int replnDetailId = 0;
        public int ReplnDetailId
        {
            get { return replnDetailId; }
            set { replnDetailId = value; OnPropertyChanged("ReplnDetailId"); }
        }


        private string orderID;
        public string OrderID
        {
            get { return orderID; }
            set { orderID = value; OnPropertyChanged("OrderID"); }
        }

    }

    public class QtyUOMClass:ModelBase
    {
        private string uMCode="";

        public string UMCode
        {
            get { return uMCode; }
            set { uMCode = value; OnPropertyChanged("UMCode"); }
        }

        private double uMMultipler=1;

        public double UMMultipler
        {
            get { return uMMultipler; }
            set { uMMultipler = value; OnPropertyChanged("UMMultipler"); }
        }
    }
}
