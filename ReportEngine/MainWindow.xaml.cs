﻿using System;
using System.IO;
using System.Windows;
using Microsoft.Win32;

namespace AppPdfViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class PDfWindow : Window
    {
        public PDfWindow()
        {
            InitializeComponent();

        }

        public string FileLocation { get; set; }

        public void FileToShow(byte[] fileBytes)
        {
            try
            {
                FileLocation = Path.GetTempFileName();
                File.WriteAllBytes(FileLocation, fileBytes);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message,"Error", MessageBoxButton.OK);
                this.Close();
            }
            finally
            {
                
                //File.Delete(fileName);
            }
        }

    

        //private void WindowLoaded(object sender, RoutedEventArgs e)
        //{

        //    if (!string.IsNullOrEmpty(FileLocation))
        //    {
        //        LoadFile();

        //        //this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        //        //this.WindowState = System.Windows.WindowState.Normal;
        //    }

        //}

        public bool LoadFile()
        {
            if (!string.IsNullOrEmpty(FileLocation))
            {
                try
                {
                    pdfViewer.LoadFile(FileLocation);
                   
                    return true;
                }
                catch
                {
                    
                    throw;
                }
            }

            return false;
        }


        #region Navigation Panel

        public bool GoToFirstPage()
        {
            try
            {
                pdfViewer.First();
                return true;

            }
            catch
            {

                throw;
            }
        }

        public bool GoToNextPage()
        {
            try
            {
                pdfViewer.Next();
                return true;

            }
            catch
            {

                throw;
            }
        }

        public bool GoToPreviousPage()
        {
            try
            {
                pdfViewer.Previous();
                return true;

            }
            catch
            {

                throw;
            }
        }
        public bool GoToLastPage()
        {
            try
            {
                pdfViewer.Last();
                return true;

            }
            catch
            {

                throw;
            }
        }
        public bool GoToSpecificPage(int pageNo)
        {
            try
            {
                pdfViewer.GoToThisPage(pageNo);
                return true;

            }
            catch
            {

                throw;
            }
        }
    

        public bool PrintReport()
        {
            pdfViewer.Print();
            return true;
        }


        //ToDo - to be implementd
        public bool EMail()
        {
            pdfViewer.Print();
            return true;
        }
  

        #endregion



        public bool Dispose()
        {
            try
            {
                pdfViewer.Dispose();
                return true;

            }
            catch
            {
                
                throw;
            }
        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);
            if (File.Exists(FileLocation))
                File.Delete(FileLocation);
        }

    }
}
