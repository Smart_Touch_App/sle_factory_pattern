﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using log4net;

namespace ReportEngine
{


    [Serializable]
    [DataObject]
    public class SLEReportDataSource
    {
        public SLEReportDataSource()
        { }
        public SLEReportDataSource(object dataObject)
        {
            this.DataObject = dataObject;
        }
        public object DataObject { get; set; }
        public Func<object> Action { get; set; }
        [DataObjectMethod(DataObjectMethodType.Select)]
        public virtual object GetData()
        {
            return DataObject;
        }


    }
    // _ReportWindow = new PdfViewerUI.PDfWindow();
    //_ReportWindow.Activated += _ReportWindow_Activated;
    //_ReportWindow.Closed += _ReportWindow_Closed;

    public class SLEReportEngine : IDisposable
    {
        public delegate void Notification(string msg);
        public event Notification Notify;
        private static volatile bool IsReporting = false;


        System.Drawing.Printing.PrintDocument docToPrint;
        private System.ComponentModel.Container components;
        private Font printFont;
        private StreamReader streamToPrint;

        Sap.Data.SQLAnywhere.SAConnection _connection;

        private SLEReportEngine(Sap.Data.SQLAnywhere.SAConnection connection)
        {
            _connection = connection;
        }

        public static SLEReportEngine Create(Sap.Data.SQLAnywhere.SAConnection connection)
        {

            return new SLEReportEngine(connection);

        }


        //static void _ReportWindow_Closed(object sender, EventArgs e)
        //{
        //    if (ReportEngine.SLEReportEngine.ParentWindow != null)
        //    {
        //        ReportEngine.SLEReportEngine.ParentWindow.IsEnabled = true;
        //    }
        //}

        //static void _ReportWindow_Activated(object sender, EventArgs e)
        //{
        //    if (ReportEngine.SLEReportEngine.ParentWindow != null)
        //    {

        //        ReportEngine.SLEReportEngine.ParentWindow.IsEnabled = false;
        //    }
        //}

        public ILog Log { get; set; }

        public Sap.Data.SQLAnywhere.SAConnection Connection { get; set; }




        public static byte[] SerializeBinary(Object objectToSerialize)
        {
            byte[] byteArray;

            IFormatter formatter = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                formatter.Serialize(ms, objectToSerialize);
                ms.Position = 0;
                // Read the first 20 bytes from the stream.
                byteArray = new byte[ms.Length];
                var count = ms.Read(byteArray, 0, byteArray.Length);
            }

            return byteArray;
        }

        public static T DeSerializeBinary<T>(byte[] objectToSerialize) where T : class
        {
            byte[] byteArray;

            IFormatter formatter = new BinaryFormatter();
            MemoryStream ms = new MemoryStream(objectToSerialize);
            return ((T)formatter.Deserialize(ms));


        }

        public static String SerializeXML(Object objectToSerialize, Type type)
        {
            if (objectToSerialize == null)
                throw new ArgumentNullException("objectToSerialize");
            if (type == null)
                throw new ArgumentNullException("type");

            XmlSerializer serializer = new XmlSerializer(type);
            StringWriter sw = new StringWriter(new StringBuilder(128),
                                        CultureInfo.InvariantCulture);
            XmlWriterSettings setting = new XmlWriterSettings();
            setting.OmitXmlDeclaration = true;
            setting.Indent = true;
            setting.IndentChars = "   ";
            setting.NewLineOnAttributes = true;
            XmlWriter messageWriter = XmlWriter.Create(sw, setting);
            serializer.Serialize(messageWriter, objectToSerialize);
            messageWriter.Close();
            sw.Close();

            return sw.ToString();
        }

        public DocumentInformation Generate<T>(decimal sourceDocumentNo, bool reGenerate, ReportCategory reportCategory,
            ReportEngine.ReportsTypes reportType, IReportSLE reportDataObject, Dictionary<string, object> parameters,
            ContentTypes reportFormat = ContentTypes.PDF) where T : class
        {
            decimal DocId = 0;

            Log.Info("[Start:ReportViewModel][Parameters:objForReport:" + reportDataObject.ReportName + ",reportType:" + reportDataObject.ContentType.ToString() + "]");

            //Static Formatting
            //Standardize format
            SLEReportDataSource SLEREpoDataSource = new SLEReportDataSource(reportDataObject);

            //Create object Data source, set data source to standardized formated object, GetData is the select method
            Telerik.Reporting.ObjectDataSource oDataSource = new Telerik.Reporting.ObjectDataSource();
            oDataSource.DataSource = SLEREpoDataSource;
            oDataSource.DataMember = "GetData";

            ////Dynamic binding
            Telerik.Reporting.InstanceReportSource xInstanceReportSource = new Telerik.Reporting.InstanceReportSource();
            //var reportType = Type.GetType(string.Format("{0},{1}", assemblyQualifiedReportName, "SalesLogicExpress.Reporting"));


            var executingReport = (Telerik.Reporting.Report)Activator.CreateInstance(typeof(T), null);
            //  var ExecutingReport = GetReportSource<T>(parameters);

            //Specify Standardized source of Data 
            executingReport.DataSource = oDataSource;
            //Specify which report to execute
            xInstanceReportSource.ReportDocument = executingReport;
            //Add parameter for the data
            if (parameters != null)
            {
                foreach (KeyValuePair<string, object> item in parameters.ToList())
                {
                    xInstanceReportSource.Parameters.Add(item.Key, item.Value);
                }
            }
            //Execute Report Programmatically
            Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
            // set any deviceInfo settings if necessary
            System.Collections.Hashtable deviceInfo = new System.Collections.Hashtable();
            Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport(reportFormat.ToString(), xInstanceReportSource, deviceInfo);
            //Save Rendered report

            // Byte Generation
            var DocInfo = new DocumentInformation();

            LocalDL.Document objDocument = new LocalDL.Document(reportDataObject.Connection);
            decimal RouteID = objDocument.GetRouteID(reportDataObject.AppUserId);
            DocId = objDocument.GetNext(reportDataObject.ReportName, RouteID);

            if (DocId > 0)
            {
                if (reportCategory == ReportCategory.Customer)
                {
                    LocalDL.CustomerDocument doc = new LocalDL.CustomerDocument(reportDataObject.Connection);
                    //DocId = doc.GetNext(reportDataObject.ReportName,RouteID);
                    DocInfo = new DocumentInformation() { DocumentId = DocId, IsDocumentCreated = true, IsEndOfSequence = false };
                    if (reGenerate)
                    {
                        doc.SetPreviousInactive(sourceDocumentNo);

                    }
                    doc.Insert(DocId, sourceDocumentNo, (int)reportType, (int)reportDataObject.ContentType, result.DocumentBytes, reportDataObject.CustomerInfo.CustomerShipTo, reportDataObject.AppUserId, DateTime.Now, null, null, null);
                    LocalDL.PrintHistory ph = new LocalDL.PrintHistory(reportDataObject.Connection);
                    ph.Insert(DocId, DateTime.Now, reportDataObject.MachineName, reportDataObject.AppUserId, (int)reportType, reportDataObject.AppUserId, DateTime.Now, null, null, null, 'C', reportDataObject.CustomerInfo.CustomerShipTo);
                }
                else
                {
                    LocalDL.RouteDocuments doc = new LocalDL.RouteDocuments(reportDataObject.Connection);
                    //DocId = doc.GetNext(reportDataObject.ReportName, RouteID);
                    DocInfo = new DocumentInformation() { DocumentId = DocId, IsDocumentCreated = true, IsEndOfSequence=false };
                    if (reGenerate)
                    {
                        doc.SetPreviousInactive(sourceDocumentNo);

                    }
                    doc.Insert(DocInfo.DocumentId, sourceDocumentNo, (int)reportType, (int)reportDataObject.ContentType, result.DocumentBytes, reportDataObject.RouteInfo.RouterMasterId,
                        reportDataObject.AppUserId, DateTime.Now, null, null, null);
                    LocalDL.PrintHistory ph = new LocalDL.PrintHistory(reportDataObject.Connection);
                    ph.Insert(DocId, DateTime.Now, reportDataObject.MachineName, reportDataObject.AppUserId, (int)reportType, reportDataObject.AppUserId, DateTime.Now, null, null, null, 'R', reportDataObject.RouteInfo.RouterMasterId);
                }
            }
            else
            {
                // End of The Sequence
                DocInfo.IsEndOfSequence = true;
            }
            return DocInfo;
        }


        //public ReportEngine.DocumentInformation SetDocumentView<T>(decimal documentId, ReportCategory reportCategory,
        //                                                       decimal SourceDocumentNo, int reportTypeId, Sap.Data.SQLAnywhere.SAConnection connection,
        //                                                       bool reGenerate = false,
        //                                                       System.Drawing.Printing.PrinterSettings printerSettings = null) where T : class
        //{

        //    return SetDocumentView<T>(documentId, reportCategory, reportTypeId, null, SourceDocumentNo, connection, reGenerate, printerSettings);
        //}

        //public ReportEngine.DocumentInformation SetDocumentView<T>(decimal documentId, ReportCategory reportCategory, int reportTypeId, ReportEngine.IReportSLE ReportData,
        //    decimal SourceDocumentNo, Sap.Data.SQLAnywhere.SAConnection connection, bool reGenerate = false,
        //    System.Drawing.Printing.PrinterSettings printerSettings = null, int printAction = -1) where T : class
        //{
        //    ReportEngine.ReportActions PrintAction = (ReportEngine.ReportActions)(printAction);
        //    if (printAction == -1)
        //    {
        //        int PAction = Convert.ToInt32(new LocalDL.Document(connection).GetPrintAction(reportTypeId));
        //        PrintAction = (ReportEngine.ReportActions)(PAction);
        //    }
        //    ReportEngine.DocumentInformation DocInfo = new ReportEngine.DocumentInformation();
        //    DocInfo.DocumentId = documentId;
        //    if (DocInfo.DocumentId <= 0 || (DocInfo.DocumentId > 0 && reGenerate))
        //    {
        //        //The report engine will generate and put binary report in db and will return doc id
        //        DocInfo = Generate<T>(SourceDocumentNo, reGenerate, reportCategory, (ReportsTypes)reportTypeId, ReportData, null, ReportEngine.ContentTypes.PDF);
        //    }

        //    switch (PrintAction)
        //    {
        //        case ReportEngine.ReportActions.Print:
        //            if (ReportData == null)
        //                PrintReport(connection, reportCategory, DocInfo.DocumentId, printerSettings);
        //            break;
        //        case ReportEngine.ReportActions.View:
        //            ViewReport(connection, reportCategory, DocInfo.DocumentId, printerSettings);
        //            break;
        //        case ReportEngine.ReportActions.None:
        //            break;
        //        default:
        //            break;
        //    }
        //    return DocInfo;
        //}


        //public void ViewReport(Sap.Data.SQLAnywhere.SAConnection connection, ReportCategory reportCategory, decimal documentId, System.Drawing.Printing.PrinterSettings printerSettings = null)
        //{
        //    if (!IsReporting)
        //    {
        //        IsReporting = true;
        //        Thread staThread = new Thread(delegate()
        //        {
        //            try
        //            {
        //                byte[] xReport;
        //                var xDoc = new LocalDL.Document(connection);
        //                if (reportCategory == ReportCategory.Customer)
        //                    xReport = xDoc.GetCustomerDocument(documentId);
        //                else
        //                    xReport = xDoc.GetRouteDocument(documentId);
        //                var _ReportWindow = new AppPdfViewer.PDfWindow();

        //                _ReportWindow.FileToShow(xReport);
        //                _ReportWindow.LoadFile();
        //                _ReportWindow.Topmost = true;
        //                _ReportWindow.ShowDialog();
        //            }
        //            catch (Exception)
        //            {

        //            }
        //            finally
        //            {
        //                IsReporting = false;
        //                System.Windows.Threading.Dispatcher.Run();

        //            }
        //        });
        //        staThread.SetApartmentState(ApartmentState.STA);
        //        staThread.Start();
        //    }
        //    else
        //    {
        //        if (Notify != null)
        //        {
        //            Notify("Please close active report first");
        //        }
        //    }
        //}


        void win_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {

        }



        public byte[] GetReport(Sap.Data.SQLAnywhere.SAConnection connection, ReportCategory reportCategory, decimal documentId, System.Drawing.Printing.PrinterSettings printerSettings = null)
        {
            byte[] xReport;
            var xDoc = new LocalDL.Document(connection);
            if (reportCategory == ReportCategory.Customer)
                xReport = xDoc.GetCustomerDocument(documentId);
            else
                xReport = xDoc.GetRouteDocument(documentId);
            return xReport;
        }

        /// <summary>
        /// Method for Printing PDF
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="reportCategory"></param>
        /// <param name="documentId"></param>
        /// <param name="printerSettings"></param>
        /// <returns></returns>
        public void PrintDirectly(Sap.Data.SQLAnywhere.SAConnection connection, ReportEngine.ReportCategory reportCategory, decimal documentId, System.Drawing.Printing.PrinterSettings printerSettings = null, bool showPrintDialog = false)
        {
            byte[] fileBytes;
            var xDoc = new LocalDL.Document(connection);
            if (reportCategory == ReportEngine.ReportCategory.Customer)
                fileBytes = xDoc.GetCustomerDocument(documentId);
            else
                fileBytes = xDoc.GetRouteDocument(documentId);

            WPFPdfViewer.PDFLib lib = new WPFPdfViewer.PDFLib();
            lib.PrintDirectly(fileBytes, showPrintDialog, printerSettings);



        }


        public static decimal GetExistingDocumentID(Sap.Data.SQLAnywhere.SAConnection connection, decimal PreInspecpctionHeaderID, decimal RouteID)
        {
            return new LocalDL.Document(connection).ExistPreTripInspectionReportId(PreInspecpctionHeaderID, RouteID);

        }



        public static decimal SetDocumentID(Sap.Data.SQLAnywhere.SAConnection connection, string tableName, string colName, decimal SourceDocumentNo, decimal documentId)
        {
            return new LocalDL.Document(connection).UpdateDocumentId(tableName, colName, SourceDocumentNo, documentId);

        }

        #region Dispose Handling

        // Dispose Handling for Print Process and Optimization - Added on 12/28/2016 - Velmani

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        #endregion // Dispose Handling

    }

}
