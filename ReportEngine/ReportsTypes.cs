﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportEngine
{
    public enum ReportsTypes
    {
        [Description("Reports.OrderReport")]
        OrderReport = 1,
        [Description("SalesLogicExpress.Reporting.rPickOrderSlip")]
        PickSlipReport = 2,
        [Description("SalesLogicExpress.Reporting.Invoice")]
        InvoiceReport = 3,
        [Description("SalesLogicExpress.Reporting.Receipt")]
        ReceiptReport = 4,
        [Description("SalesLogicExpress.Reporting.CreditMemo")]
        CreditMemoReport = 5,
        [Description("SalesLogicExpress.Reporting.AddLoadRequest")]
        AddLoadRequestReport = 6,
        [Description("SalesLogicExpress.Reporting.AddLoadPick")]
        AddLoadPickReport = 7,
        [Description("SalesLogicExpress.Reporting.LoadSuggetions")]
        LoadSuggetionsReport = 8,
        [Description("SalesLogicExpress.Reporting.LoadSuggetionsPick")]
        LoadSuggetionsPickReport = 9,
        [Description("SalesLogicExpress.Reporting.HeldReturnLoad")]
        HeldReturnLoadReport = 10,
        [Description("SalesLogicExpress.Reporting.HeldReturnPick")]
        HeldReturnPickReport = 11,
        [Description("SalesLogicExpress.Reporting.SuggesionReturnLoad")]
        SuggesionReturnLoadReport = 12,
        [Description("SalesLogicExpress.Reporting.SuggestionReturnPick")]
        SuggestionReturnPickReport = 13,
        [Description("SalesLogicExpress.Reporting.Unload")]
        UnloadReport = 14,
        [Description("SalesLogicExpress.Reporting.UnloadPick")]
        UnloadPickReport = 15,
        [Description("SalesLogicExpress.Reporting.AddLoadRequestDC")]
        AddLoadRequestReportDC = 16,
        [Description("SalesLogicExpress.Reporting.AddLoadPickDC")]
        AddLoadPickReportDC = 17,
        [Description("SalesLogicExpress.Reporting.LoadSuggetionsDC")]
        LoadSuggetionsReportDC = 18,
        [Description("SalesLogicExpress.Reporting.LoadSuggetionsPickDC")]
        LoadSuggetionsPickReportDC = 19,
        [Description("SalesLogicExpress.Reporting.SuggesionReturnLoadDC")]
        SuggesionReturnLoadReportDC = 20,
        [Description("SalesLogicExpress.Reporting.SuggestionReturnPickDC")]
        SuggestionReturnPickReportDC = 21,
        [Description("SalesLogicExpress.Reporting.UnloadDC")]
        UnloadReportDC = 22,
        [Description("SalesLogicExpress.Reporting.UnloadPickDC")]
        UnloadPickReportDC = 23,
        [Description("SalesLogicExpress.Reporting.ItemReturns")]
        ItemReturnsReport = 24,
        [Description("SalesLogicExpress.Reporting.QuotePick")]
        QuotePick = 25,
        [Description("SalesLogicExpress.Reporting.CustomerQuote")]
        CustomerQuote = 26,
        [Description("SalesLogicExpress.Reporting.ProspectQuote")]
        ProspectQuote = 27,
        [Description("SalesLogicExpress.Reporting.PreTripInspection")]
        PreTripInspection = 28,
        [Description("SalesLogicExpress.Reporting.RouteSettlementReport")]
        RouteSettlementReport = 29,
        [Description("SalesLogicExpress.Reporting.Inventory")]
        Inventory = 30
    }
}
