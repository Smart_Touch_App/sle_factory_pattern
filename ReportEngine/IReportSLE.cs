﻿using System;
using System.ComponentModel;
namespace ReportEngine
{
   

    public enum ReportActions
    {
        Print = 1,
        View = 2,
        None = 0
     }
    [Serializable]
    public enum ContentTypes
    {
        PDF = 0,
        XLS = 1,
        XLSX = 2,
        DOCX = 3,
        CSV = 4,
        XPS = 5,
        PPTX = 6,
        MHTML = 7,
        IMAGE = 8,
        IMAGEInteractive = 9,
        HTML5 = 10,
        HTML5Interactive = 11,
        XAML = 12,
        XAMLInteractive = 13,
        WPFXAML = 14,
        WPFXAMLInteractive = 15
    }
    [Serializable]
    public enum ReportCategory
    {
        Customer,
        Route

    }
    [Serializable]
    public class CustomerInformation
    {
        public int CustomerShipTo { get; set; }

    }
    [Serializable]
    public class RouteInformation
    {
        public int RouterMasterId { get; set; }

    }
    [Serializable]
    public class DocumentInformation
    {
        public bool IsDocumentCreated { get; set; }
        public decimal DocumentId { get; set; }
        public bool IsEndOfSequence { get; set; }
    }

   


    public interface IReportSLE
    {
        int AppUserId { get; set; }

        string MachineName { get; set; }

        string ReportName { get; set; }

        ReportCategory ContentType { get; set; }

        CustomerInformation CustomerInfo { get; set; }
        RouteInformation RouteInfo { get; set; }

        System.Collections.Generic.Dictionary<string, object> Parameters { get; set; }

        Sap.Data.SQLAnywhere.SAConnection Connection { get; set; }

        //ReportEngine.DocumentInformation Generate<T>(decimal sourceDocumentNo, bool reGenerate, ReportEngine.ReportsTypes reportType, ReportEngine.ContentTypes reportFormat = ReportEngine.ContentTypes.PDF) where T : class;
        //byte[] ViewReport(decimal documentId, Sap.Data.SQLAnywhere.SAConnection connection, ReportCategory category, System.Drawing.Printing.PrinterSettings printerSettings = null);
        //byte[] PrintReport(decimal documentId, Sap.Data.SQLAnywhere.SAConnection connection, ReportEngine.ReportCategory category, System.Drawing.Printing.PrinterSettings printerSettings = null);


    }


}
