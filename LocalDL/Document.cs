﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalDL
{
    public class Document : IDisposable
    {

        private Sap.Data.SQLAnywhere.SAConnection _Connection;
        private RemoteDBDal.Dal _DAL;

        public Sap.Data.SQLAnywhere.SAConnection Connection
        {
            set
            {
                SetConnection(value);

            }
        }

        private void SetConnection(Sap.Data.SQLAnywhere.SAConnection value)
        {
            _Connection = value;
            _DAL = new RemoteDBDal.Dal(_Connection);
        }

        public Document(Sap.Data.SQLAnywhere.SAConnection Connection)
        {
            SetConnection(Connection);
        }

        public byte[] GetCustomerDocument(decimal documentId)
        {
            try
            {
                string query = string.Empty;
                //--select FileContent from busdta.CustomerDocuments where DocumentID = 1
                query = string.Format(@"select FileContent from busdta.CustomerDocuments where DocumentID = ?");
                using (Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(query))
                {
                    Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();

                    parm = new Sap.Data.SQLAnywhere.SAParameter();
                    parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Decimal;
                    command.Parameters.Add(parm);
                    command.Parameters[0].Value = documentId;
                    return _DAL.ExecuteScalar<byte[]>(command);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public byte[] GetRouteDocument(decimal documentId)
        {
            try
            {
                string query = string.Empty;
                //--select FileContent from busdta.CustomerDocuments where DocumentID = 1
                query = string.Format(@"select FileContent from busdta.RouteDocuments where DocumentID = ?");

                using (Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(query))
                {
                    Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();

                    parm = new Sap.Data.SQLAnywhere.SAParameter();
                    parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Decimal;
                    command.Parameters.Add(parm);
                    command.Parameters[0].Value = documentId;
                    return _DAL.ExecuteScalar<byte[]>(command);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public decimal ExistingCustomerQuoteDocId(Int32 CustomerId, decimal CustomerQuoteId, decimal RouteID)
        {
            try
            {
                decimal Ret = 0;
                string extTable = "Customer_Quote_Header";
                var q = string.Format("SELECT DocumentId FROM BUSDTA.{0} where RouteId=? And CustomerQuoteId = ?", extTable);
                using (Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(q))
                {
                    Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();
                    //foreach (var prm in prms)
                    {
                        parm = new Sap.Data.SQLAnywhere.SAParameter();
                        parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Decimal;
                        command.Parameters.Add(parm);
                        command.Parameters[0].Value = RouteID;
                    }

                    parm = new Sap.Data.SQLAnywhere.SAParameter();
                    parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Decimal;
                    command.Parameters.Add(parm);
                    command.Parameters[1].Value = CustomerQuoteId;

                    Ret = _DAL.ExecuteScalar<decimal>(command);

                }
                return Ret;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public decimal ExistPreTripInspectionReportId(decimal PreInspecpctionHeaderID, decimal RouteID)
        {
            try
            {
                decimal Ret = 0;

                string extTable = "PreTrip_Inspection_Header_Ext";
                var q = string.Format("SELECT DocumentId FROM BUSDTA.{0} where RouteId=? And PreTripInspectionHeaderId = ?", extTable);
                using (Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(q))
                {
                    Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();
                    //foreach (var prm in prms)
                    {
                        parm = new Sap.Data.SQLAnywhere.SAParameter();
                        parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Decimal;
                        command.Parameters.Add(parm);
                        command.Parameters[0].Value = RouteID;
                    }

                    parm = new Sap.Data.SQLAnywhere.SAParameter();
                    parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Decimal;
                    command.Parameters.Add(parm);
                    command.Parameters[1].Value = PreInspecpctionHeaderID;

                    Ret = _DAL.ExecuteScalar<decimal>(command);

                }
                return Ret;
            }
            catch (Exception)
            {

                throw;
            }
        }

        //public int InsertPreTripInspectionReportId<A,B,C,D>(decimal documentId,string tableName,
        //                                                    Dictionary<string,A> prms1,
        //                                                    Dictionary<string,B> prms2,
        //                                                    Dictionary<string,C> prms3,
        //                                                    Dictionary<string,D> prms4,
        //                                                    )  where A:decimal 
        //                                                        where B:string 
        //                                                        where C:int 
        //                                                        where D:DateTime
        //{
        //    int Ret = 0;
        //    var paramsCount =  prms1!=null? prms1.Count():0;
        //    paramsCount+=  prms2!=null? prms2.Count():0;
        //    paramsCount+=  prms3!=null? prms3.Count():0;
        //    paramsCount+=  prms4!=null? prms4.Count():0;

        //    System.Collections.ArrayList lst = new System.Collections.ArrayList(paramsCount);
        //    lst.AddRange(prms1.ToList());
        //    lst.AddRange(prms2.ToList());
        //    lst.AddRange(prms3.ToList());
        //    lst.AddRange(prms4.ToList());
        //    //  string extTable = "PreTrip_Inspection_Header_Ext";
        //    string prms = string.Empty;

        //    for(int i=0;i<lst.Count;i++)
        //    {    
        //        prms+= string.Format("{0}=?,",lst[i]);
        //    }
        //    prms = prms.PadLeft(prms.Length-1);
        //    var q = string.Format("Update  BUSDTA.{0} set DocumentId where ", tableName,prms);
        //    using (Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(q))
        //    {
        //        Ret = _DAL.ExecuteScalar<int>(command);
        //    }
        //    return Ret;
        //}

        public int InsertPreTripInspectionReportId(decimal PreInspecpctionHeaderID, decimal RouteID, decimal documentId)
        {
            try
            {
                int Ret = 0;

                //  string extTable = "PreTrip_Inspection_Header_Ext";
                var q = string.Format("Update  BUSDTA.PreTrip_Inspection_Header set DocumentId where ", PreInspecpctionHeaderID, RouteID, documentId);
                using (Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(q))
                {
                    Ret = _DAL.ExecuteScalar<int>(command);
                }
                return Ret;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int InsertCustomerQuoteReportId(string CustomerId, decimal CustomerQuoteId, decimal RouteID, decimal documentId)
        {
            try
            {
                int Ret = 0;

                //  string extTable = "PreTrip_Inspection_Header_Ext";
                var q = string.Format("INSERT INTO BUSDTA.PreTrip_Inspection_Header_Ext (PreTripInspectionHeaderId,RouteId,DocumentId) VALUES({0},{1},{2})", CustomerQuoteId, RouteID, documentId);
                using (Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(q))
                {
                    Ret = _DAL.ExecuteScalar<int>(command);
                }
                return Ret;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetPrintAction(int reportId)
        {
            try
            {
                var q = string.Format("select PrintAction from  BUSDTA.ReportTypes  WHERE ReportTypeID={0}", reportId);
                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(q);
                var Ret = _DAL.ExecuteScalar<string>(command);
                return Ret;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public decimal GetSourceDocumentNoFromCustomer(decimal documentId)
        {
            try
            {
                var q = string.Format("select * from  BUSDTA.CustomerDocuments WHERE DocumentId={0}", documentId);
                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(q);
                var Ret = _DAL.ExecuteScalar<decimal>(command);
                return Ret;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public decimal GetSourceDocumentNoFromRoute(decimal documentId)
        {
            try
            {
                var q = string.Format("select * from  BUSDTA.RouteDocuments WHERE DocumentId={0}", documentId);
                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(q);
                var Ret = _DAL.ExecuteScalar<decimal>(command);
                return Ret;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public decimal UpdateDocumentId(string tableName, string colName, decimal SourceDocumentNo, decimal documentId)
        {

            try
            {
                var q = string.Format("update BUSDTA.{0} set DocumentId = {1} WHERE {2}={3}", tableName, documentId, colName, SourceDocumentNo);
                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(q);
                var Ret = _DAL.ExecuteScalar<decimal>(command);
                return Ret;
            }
            catch (Exception)
            {

                throw;
            }
        }

        // Get Next Number From Bucket
        public string GetNextNumber(string routeId, string entityId)
        {
            try
            {
                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(string.Format("SELECT BUSDTA.GetNextNumber({0},{1})", routeId, entityId));
                Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();
                return _DAL.ExecuteScalar<string>(command);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public decimal GetNext(string reportName, decimal RouteID)
        {
            //var q = "select top 1 DocumentID from BUSDTA.RouteDocuments Order By DocumentID desc";
            //Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(q);
            //var lastNumber = _DAL.ExecuteScalar<decimal>(command);
            //return lastNumber + 1;

            try
            {
                var lastNumber = Convert.ToInt32(GetNextNumber(RouteID.ToString(), "10"));
                return lastNumber;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public decimal GetRouteID(int AppUserID)
        {
            try
            {
                var q = string.Format("SELECT DISTINCT RouteID FROM BUSDTA.RouteSecurity WHERE UserID='{0}'", AppUserID);
                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(q);
                var Ret = _DAL.ExecuteScalar<decimal>(command);
                return Ret;
            }
            catch (Exception)
            {

                throw;
            }
        }

        #region Dispose Handling

        // Dispose Handling for Print Process and Optimization - Added on 12/28/2016 - Velmani

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        #endregion // Dispose Handling

    }
}
