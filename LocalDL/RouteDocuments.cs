﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalDL
{
    public class RouteDocuments : IDisposable
    {
        private Sap.Data.SQLAnywhere.SAConnection _Connection;
        private RemoteDBDal.Dal _DAL;

        public Sap.Data.SQLAnywhere.SAConnection Connection
        {
            set
            {
                SetConnection(value);

            }
        }

        private void SetConnection(Sap.Data.SQLAnywhere.SAConnection value)
        {
            _Connection = value;
            _DAL = new RemoteDBDal.Dal(_Connection);
        }

        public RouteDocuments(Sap.Data.SQLAnywhere.SAConnection Connection)
        {
            SetConnection(Connection);
        }

        //public string GetNextNumber(string routeId, string entityId)
        //{

        //    //"BUSDTA"."GetNextNumber"(  RouteID nvarchar(50), EntityID  nvarchar(50) )
        //    Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(string.Format("SELECT BUSDTA.GetNextNumber({0},{1})", routeId, entityId));
        //    Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();


        //    return _DAL.ExecuteScalar<string>(command);

        //}

        public byte[] GetDocumentContents(string documentId)
        {
            return null;
        }

        public int Insert(decimal DocumentId, decimal sourceDocumentNo, int ReportTypeId, int contentTypeId, byte[] fileContent, int routeMasterId, int? createdBy, DateTime? createdDateTime,
                            int? updatedBy, DateTime? UpdatedDateTime, int? lastModifiedBy, bool iSInsert = true)
        {
            string query = string.Empty;
            int Res = 0;

            if (DocumentId == 0)
                return 0;

            query = string.Format(@"INSERT INTO BUSDTA.RouteDocuments 
                                    (DocumentID,ReportTypeID,ContentTypeID,FileContent,RouteMasterID,CreatedBy,SourceDocumentNo,Active,CreatedDatetime,UpdatedBy,UpdatedDatetime) 
                                     VALUES({0},{1},{2},{3},{4},{5},{6},{7},GETDATE(),'" + createdBy + "',GETDATE())", "?", "?", "?", "?", "?", "?", "?", "?");

            using (Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(query))
            {
                Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();

                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Decimal;
                command.Parameters.Add(parm);
                command.Parameters[0].Value = DocumentId;


                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Integer;
                command.Parameters.Add(parm);
                command.Parameters[1].Value = ReportTypeId;

                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Integer;
                command.Parameters.Add(parm);
                command.Parameters[2].Value = contentTypeId;

                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.LongBinary;
                parm.Size = 2147483647;
                command.Parameters.Add(parm);
                command.Parameters[3].Value = fileContent; //new  { fileContent.DocumentBytes };


                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Integer;
                command.Parameters.Add(parm);
                command.Parameters[4].Value = routeMasterId;



                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Integer;
                command.Parameters.Add(parm);
                command.Parameters[5].Value = createdBy;



                //parm = new Sap.Data.SQLAnywhere.SAParameter();
                //parm.SADbType = Sap.Data.SQLAnywhere.SADbType.DateTime;
                //command.Parameters.Add(parm);
                //command.Parameters[6].Value = DateTime.Now; //string.Format("'{0}'", createdDateTime.Value.ToString("yyyy-MM-dd hh:mm:ss.sss"));



                //parm = new Sap.Data.SQLAnywhere.SAParameter();
                //parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Integer;
                //command.Parameters.Add(parm);
                //command.Parameters[6].Value = 1;


                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Decimal;
                command.Parameters.Add(parm);
                command.Parameters[6].Value = sourceDocumentNo;


                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Char;
                command.Parameters.Add(parm);
                command.Parameters[7].Value = 1;  //Active

                //parm = new Sap.Data.SQLAnywhere.SAParameter();
                //parm.SADbType = Sap.Data.SQLAnywhere.SADbType.DateTime;
                //command.Parameters.Add(parm);
                //command.Parameters[8].Value = string.Format("'{0}'", createdDateTime.Value.ToString("yyyy-MM-dd hh:mm:ss.sss"));



                //parm = new Sap.Data.SQLAnywhere.SAParameter();
                //parm.SADbType = Sap.Data.SQLAnywhere.SADbType.DateTime;
                //command.Parameters.Add(parm);
                //command.Parameters[9].Value = string.Format("'{0}'", createdDateTime.Value.ToString("yyyy-MM-dd hh:mm:ss.sss"));

                Res = _DAL.InsertUpdateData(command);
            }


            //try
            //{

            //    command.Transaction.Commit();

            //}
            //catch 
            //{

            //    command.Transaction.Rollback();
            //}                   
            return Res;

        }
        //public decimal GetNext(string reportName, int RouteID)
        //{
        //    //var q = "select top 1 DocumentID from BUSDTA.RouteDocuments Order By DocumentID desc";
        //    //Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(q);
        //    //var lastNumber = _DAL.ExecuteScalar<decimal>(command);
        //    //return lastNumber + 1;

        //    var lastNumber = Convert.ToInt32(GetNextNumber(RouteID.ToString(), "10"));
        //    return lastNumber;

        //}

        public int SetPreviousInactive(decimal sourceDocumentNo)
        {
            var q = string.Format("update BUSDTA.RouteDocuments set Active=0 where SourceDocumentNo={0}", sourceDocumentNo);           
            Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(q);
            var RowsUpdated = _DAL.ExecuteScalar<int>(command);
            return RowsUpdated;
        }

        #region Dispose Handling

        // Dispose Handling for Print Process and Optimization - Added on 12/28/2016 - Velmani

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        #endregion // Dispose Handling
    }
}
