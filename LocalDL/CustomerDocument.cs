﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalDL
{
    public class CustomerDocument : IDisposable
    {       
        private Sap.Data.SQLAnywhere.SAConnection _Connection;
        private RemoteDBDal.Dal _DAL;

        public Sap.Data.SQLAnywhere.SAConnection Connection
        {
            set
            {
                SetConnection(value);
            }
        }

        private void SetConnection(Sap.Data.SQLAnywhere.SAConnection value)
        {
            _Connection = value;
            _DAL = new RemoteDBDal.Dal(_Connection);
        }

        public CustomerDocument(Sap.Data.SQLAnywhere.SAConnection Connection)
        {
            SetConnection(Connection);
        }

        public int SetPreviousInactive(decimal sourceDocumentNo)
        {
            try
            {
                var q = string.Format("UPDATE BUSDTA.CustomerDocuments SET Active=0 WHERE SourceDocumentNo={0}", sourceDocumentNo);
                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(q);
                var RowsUpdated = _DAL.ExecuteScalar<int>(command);
                return RowsUpdated;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Insert(decimal DocumentId, decimal sourceDocumentNo, int ReportTypeId, int contentTypeId, byte[] fileContent, int customerShipTo, int? createdBy, DateTime? createdDateTime,
                            int? updatedBy, DateTime? UpdatedDateTime, int? lastModifiedBy)
        {
            try
            {
                string query = string.Empty;
                if (DocumentId == 0)
                    return 0;

                query = string.Format(@"INSERT INTO BUSDTA.CustomerDocuments 
                                    (DocumentID,ReportTypeID,ContentTypeID,FileContent,CustomerShipTo,CreatedBy,SourceDocumentNo,Active,CreatedDatetime,UpdatedBy,UpdatedDatetime) 
                                     VALUES({0},{1},{2},{3},{4},{5},{6},'1',GETDATE(),'" + createdBy + "',GETDATE())", "?", "?", "?", "?", "?", "?", "?");

                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(query);
                Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();

                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Decimal;
                command.Parameters.Add(parm);
                command.Parameters[0].Value = DocumentId;

                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Integer;
                command.Parameters.Add(parm);
                command.Parameters[1].Value = ReportTypeId;

                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Integer;
                command.Parameters.Add(parm);
                command.Parameters[2].Value = contentTypeId;

                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.LongBinary;
                parm.Size = 2147483647;
                command.Parameters.Add(parm);
                command.Parameters[3].Value = fileContent; //new  { fileContent.DocumentBytes };

                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.VarChar;
                command.Parameters.Add(parm);
                command.Parameters[4].Value = customerShipTo;

                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Integer;
                command.Parameters.Add(parm);
                command.Parameters[5].Value = createdBy;

                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Decimal;
                command.Parameters.Add(parm);
                command.Parameters[6].Value = sourceDocumentNo;

                return _DAL.InsertUpdateData(command);
            }
            catch (Exception)
            {               
                throw;
            }
        }

        #region Dispose Handling

        // Dispose Handling for Print Process and Optimization - Added on 12/28/2016 - Velmani

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        #endregion // Dispose Handling

        #region Commented

        //public decimal GetNext(string reportName,int RouteID)
        //{

        //    //ToDo - Take next number from SP NextNumber
        //    // var q = "select top 1 DocumentID from BUSDTA.CustomerDocuments Order By DocumentID desc";
        //    // Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(q);
        //    //var lastNumber = _DAL.ExecuteScalar<decimal>(command);
        //    // return lastNumber+1;

        //    var lastNumber = Convert.ToInt32(GetNextNumber(RouteID.ToString(), "10"));
        //    return lastNumber;
        //}

        //public string GetNextNumber(string routeId, string entityId)
        //{

        //    //"BUSDTA"."GetNextNumber"(  RouteID nvarchar(50), EntityID  nvarchar(50) )
        //    Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(string.Format("SELECT BUSDTA.GetNextNumber({0},{1})", routeId, entityId));
        //    Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();


        //    return _DAL.ExecuteScalar<string>(command);

        //}

        #endregion // Commented


    }
}
