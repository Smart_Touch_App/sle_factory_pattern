﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalDL
{
    public class PrintHistory : IDisposable
    {
        private Sap.Data.SQLAnywhere.SAConnection _Connection;
        private RemoteDBDal.Dal _DAL;

        public Sap.Data.SQLAnywhere.SAConnection Connection
        {
            set
            {
                SetConnection(value);

            }
        }

        private void SetConnection(Sap.Data.SQLAnywhere.SAConnection value)
        {
            _Connection = value;
            _DAL = new RemoteDBDal.Dal(_Connection);
        }

        public PrintHistory(Sap.Data.SQLAnywhere.SAConnection Connection)
        {
            SetConnection(Connection);
        }

        public string GetNextNumber(string routeId, string entityId)
        {
            //"BUSDTA"."GetNextNumber"(  RouteID nvarchar(50), EntityID  nvarchar(50) )
            try
            {
                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(string.Format("SELECT BUSDTA.GetNextNumber({0},{1})", routeId, entityId));
                Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();
                return _DAL.ExecuteScalar<string>(command);
            }
            catch (Exception)
            {

                throw;
            }

        }

        public decimal GetRouteID(int AppUserID)
        {
            try
            {
                var q = string.Format("SELECT DISTINCT RouteID FROM BUSDTA.RouteSecurity WHERE UserID='{0}'", AppUserID);
                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(q);
                var Ret = _DAL.ExecuteScalar<decimal>(command);
                return Ret;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public byte[] GetDocumentContents(string documentId)
        {
            try
            {
                string q = @"INSERT INTO BUSDTA.PrintHistory (DocumentID,PrintedDatetime,MachineName,App_user_id,ReportTypeID,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime,last_modified) 
                        VALUES(?,'?','?',?,?,?,?,?,?,'?')";


                q = @"INSERT INTO BUSDTA.PrintHistory (DocumentID,MachineName,App_user_id,ReportTypeID,CreatedBy,UpdatedBy) 
                        VALUES(?,?,?,?,?,?)";


                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(string.Format(q));
                Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();
                var bytes = _DAL.ExecuteScalar<byte[]>(command);
                return bytes;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int Insert(decimal DocumentId, DateTime PrintedDatetime, string machineName, int App_user_id, int ReportTypeID, int? createdBy, DateTime? createdDateTime,
                            int? updatedBy, DateTime? UpdatedDateTime, int? lastModifiedBy, char type, int RouteOrCustomerId)
        {
            try
            {
                string query = string.Empty;

                if (DocumentId == 0)
                    return 0;

                string RouteID = Convert.ToString(GetRouteID(App_user_id));
                decimal PrintID = Convert.ToInt32(GetNextNumber(RouteID, "11"));

                if (PrintID == 0)
                    return 0;


                if (type == 'C')
                    query = @"INSERT INTO BUSDTA.CustDocPrintHistory (DocumentID,MachineName,App_user_id,ReportTypeID,CreatedBy,UpdatedBy,CustomerShipTo,PrintID,CreatedDatetime,UpdatedDatetime) 
                        VALUES(?,?,?,?,?,?,?,?,GETDATE(),GETDATE())";
                else
                    query = @"INSERT INTO BUSDTA.RoutDocPrintHistory (DocumentID,MachineName,App_user_id,ReportTypeID,CreatedBy,UpdatedBy,RouteMasterId,PrintID,CreatedDatetime,UpdatedDatetime) 
                        VALUES(?,?,?,?,?,?,?,?,GETDATE(),GETDATE())";

                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(query);
                Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();

                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Decimal;
                command.Parameters.Add(parm);
                command.Parameters[0].Value = DocumentId;

                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.VarChar;
                parm.Size = 50;
                command.Parameters.Add(parm);
                command.Parameters[1].Value = machineName;

                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Integer;
                command.Parameters.Add(parm);
                command.Parameters[2].Value = App_user_id;

                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Integer;
                command.Parameters.Add(parm);
                command.Parameters[3].Value = ReportTypeID;

                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Integer;
                command.Parameters.Add(parm);
                command.Parameters[4].Value = createdBy;

                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Integer;
                command.Parameters.Add(parm);
                command.Parameters[5].Value = createdBy;

                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Integer;
                command.Parameters.Add(parm);
                command.Parameters[6].Value = RouteOrCustomerId;

                parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.Decimal;
                command.Parameters.Add(parm);
                command.Parameters[7].Value = PrintID;

                return _DAL.InsertUpdateData(command);
            }
            catch (Exception)
            {

                throw;
            }
        }

        #region Dispose Handling

        // Dispose Handling for Print Process and Optimization - Added on 12/28/2016 - Velmani

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        #endregion // Dispose Handling

    }
}
