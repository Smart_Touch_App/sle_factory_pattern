﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Application.ViewModelPayload;

namespace SalesLogicExpress.Application.Managers
{
    public class CustomerReturnsManager : IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.CustomerReturnsManager");
        PricingManager pm = new PricingManager();
        OrderManager objOrderManager = new OrderManager();

        public CustomerReturnsManager()
        {

        }

        /// <summary>
        /// Returns list of items with avilable retun qty 
        /// </summary>
        /// <param name="IncludeZeroAvailableItems">True if need to return item whose all quantities has been returned, false as otherwise</param>
        /// <returns>List of tem with their order information</returns>
        public ObservableCollection<ReturnItem> GetItemsForOrdersTab(bool IncludeZeroAvailableItems)
        {
            ObservableCollection<ReturnItem> returnItems = new ObservableCollection<ReturnItem>();
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:GetItemsForOrdersTab]");
            //  StringBuilder queryBuilder = new StringBuilder();   //Unused local   --Vignesh D
            try
            {
                //queryBuilder.Append("SELECT distinct");
                //queryBuilder.Append("\nS1.EnergySurchargeAmt");
                //queryBuilder.Append("\n,TL.TDSTTLID as SettlementID");
                //queryBuilder.Append("\n,S1.OrderID , S1.OrderID AS 'TransactionDetailID'");
                //queryBuilder.Append("\n,S1.ItemId");
                //queryBuilder.Append("\n,im.IMLITM as ItemNumber");
                //queryBuilder.Append("\n,im.IMDSC1 as ItemDescription");
                //queryBuilder.Append("\n,im.IMLNTY as StkType");
                //queryBuilder.Append("\n,im.IMSRP1  as SalesCat1");
                //queryBuilder.Append("\n,im.IMSRP5 as SalesCat5");
                //queryBuilder.Append("\n,im.IMSRP4 as SalesCat4");
                //queryBuilder.Append("\n,ISNULL(S2.ORDERQTY,0) AS PreviouslyReturnedQty");
                //queryBuilder.Append("\n,S1.OrderUM as OrderUM");
                //queryBuilder.Append("\n,S1.OrderQty");
                //queryBuilder.Append("\n,S1.OriginatingRouteId");
                //queryBuilder.Append("\n,S1.UnitPriceAmt");
                //queryBuilder.Append("\n,S1.ExtnPriceAmt");
                //queryBuilder.Append("\n,S1.ItemSalesTaxAmt");
                //queryBuilder.Append("\n,S1.IsTaxable");
                //queryBuilder.Append("\n,S1.ORDERQTY- ISNULL(S2.ORDERQTY,0) AS AvailableReturnQty");
                //queryBuilder.Append("\n,inv.OnHandQuantity as OnHandQty");
                //queryBuilder.Append("\n,inv.CommittedQuantity as CommittedQty");
                //queryBuilder.Append("\n,inv.HeldQuantity as HeldQty");
                //queryBuilder.Append("\n,(inv.OnHandQuantity - (inv.CommittedQuantity + inv.HeldQuantity)) as AvailableQty ");
                //queryBuilder.Append("\n,iconfig.PrimaryUM as PrimaryUM");
                //queryBuilder.Append("\n,iconfig.PricingUM");
                //queryBuilder.Append("\n,iconfig.TransactionUM as DefaultUM");
                //queryBuilder.Append("\n,iconfig.OtherUM1 as OtherUM1");
                //queryBuilder.Append("\n,iconfig.OtherUM2");
                //queryBuilder.Append("\n,S1.OrderDate, S1.DocumentId");
                //queryBuilder.Append("\nFROM (");
                //queryBuilder.Append("\nSELECT  OH.*, OD.OrderUM, OD.ITEMID,OD.ORDERQTY,OD.RETURNHELDQTY,OD.UnitPriceAmt,OD.ExtnPriceAmt,OD.ItemSalesTaxAmt, isnull(od.IsTaxable,0) as IsTaxable");
                //queryBuilder.Append("\nFROM BUSDTA.ORDER_HEADER OH LEFT OUTER JOIN busdta.ORDER_Detail OD ON OH.ORDERID=OD.ORDERID");
                //queryBuilder.Append("\nWHERE OH.OrderTypeId=" + StatusTypesEnum.SALORD.GetStatusIdFromDB() + " and OH.OrderStateId in (" + ActivityKey.OrderDelivered.GetStatusIdFromDB() + ",");
                //queryBuilder.Append("\n" + ActivityKey.OrderReturned.GetStatusIdFromDB() + "," + ActivityKey.ItemReturn.GetStatusIdFromDB() + "," + StatusTypesEnum.STTLD.GetStatusIdFromDB() + ")) S1");
                //queryBuilder.Append("\nLEFT OUTER JOIN(");
                //queryBuilder.Append("\nSELECT SUM(OD.ORDERQTY) AS ORDERQTY ,OD.ITEMID,OD.RelatedOrderID AS RORDERDETAILID FROM BUSDTA.ORDER_HEADER OH LEFT OUTER JOIN busdta.ORDER_Detail OD ON");
                //queryBuilder.Append("\nOH.ORDERID=OD.ORDERID");
                //// queryBuilder.Append("\nLEFT OUTER JOIN busdta.SalesOrder_ReturnOrder_Mapping SRM ON  OD.ORDERID=SRM.ReturnOrderID and OD.ItemID = SRM.ItemID");
                //queryBuilder.Append("\nWHERE OH.OrderTypeId=" + StatusTypesEnum.RETORD.GetStatusIdFromDB() + " AND OH.OrderStateId not in(" + ActivityKey.VoidAtROEntry.GetStatusIdFromDB() + ",");
                //queryBuilder.Append("\n" + ActivityKey.VoidAtROPick.GetStatusIdFromDB() + "," + ActivityKey.CreateReturnOrder.GetStatusIdFromDB() + ") AND ISNULL(RORDERDETAILID,0) <> 0");
                //queryBuilder.Append("\n GROUP BY OD.ITEMID,OD.RelatedOrderID");
                //queryBuilder.Append("\n) S2 ON S1.ORDERID= S2.RORDERDETAILID AND S1.ITEMID=S2.ITEMID");
                //queryBuilder.Append("\njoin busdta.F4101 im on S1.ItemId = im.IMITM ");
                //queryBuilder.Append("\njoin busdta.F4102 ib on ib.IBLITM = im.IMLITM ");
                //queryBuilder.Append("\njoin busdta.ItemConfiguration iconfig on S1.ItemId = iconfig.ItemID");
                //queryBuilder.Append("\njoin busdta.Inventory inv on inv.ItemId = S1.ItemId");
                //queryBuilder.Append("\nLEFT OUTER JOIN BUSDTA.M50012 TL ON TL.TDREFHDRID=S1.OrderID and TL.Tdsttlid<>''");
                ////queryBuilder.Append("\nLEFT OUTER JOIN BUSDTA.M50012 TL ON TL.TDREFHDRID=S1.OrderID");
                //queryBuilder.Append("\nwhere S1.CustShipToID='" + PayloadManager.ReturnOrderPayload.Customer.CustomerNo + "'" + " order by S1.OrderId desc"); // + (124 ? "" : "  AND (S1.ORDERQTY- ISNULL(S2.ORDERQTY,0))>0 ") + ** Condition removed as order is getting selected when we have one or more item returned
                //var dbResult = DbEngine.ExecuteDataSet(queryBuilder.ToString());
                int ReturnDurationInMonth = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ReturnDurationInMonth"].ToString());
                string Query = "call busdta.SP_GetItemForOrderTab(" + StatusTypesEnum.SALORD.GetStatusIdFromDB() + "," + ActivityKey.OrderDelivered.GetStatusIdFromDB() + "," + ActivityKey.OrderReturned.GetStatusIdFromDB() + "," + ActivityKey.ItemReturn.GetStatusIdFromDB() + "," + StatusTypesEnum.STTLD.GetStatusIdFromDB() + "," + StatusTypesEnum.RETORD.GetStatusIdFromDB() + "," + ActivityKey.VoidAtROEntry.GetStatusIdFromDB() + "," + ActivityKey.VoidAtROPick.GetStatusIdFromDB() + "," + ActivityKey.CreateReturnOrder.GetStatusIdFromDB() + ",'" + PayloadManager.ReturnOrderPayload.Customer.CustomerNo + "'," + (IncludeZeroAvailableItems ? "1" : "0") + "," + ReturnDurationInMonth.ToString() + ")";
                var dbResult = DbEngine.ExecuteDataSet(Query);
                if (dbResult.HasData())
                {
                    foreach (DataRow datarow in dbResult.Tables[0].Rows)
                    {
                        ReturnItem returnItem = new ReturnItem
                        {
                            DocumentId = string.IsNullOrEmpty(datarow["DocumentId"].ToString()) ? 0 : Convert.ToDecimal(datarow["DocumentId"].ToString()),
                            ItemId = string.IsNullOrEmpty(datarow["ItemId"].ToString()) ? string.Empty : datarow["ItemId"].ToString().Trim(),
                            ItemNumber = string.IsNullOrEmpty(datarow["ItemNumber"].ToString()) ? string.Empty : datarow["ItemNumber"].ToString().Trim(),
                            ItemDescription = string.IsNullOrEmpty(datarow["ItemDescription"].ToString()) ? string.Empty : datarow["ItemDescription"].ToString().Trim(),
                            StkType = string.IsNullOrEmpty(datarow["StkType"].ToString()) ? string.Empty : datarow["StkType"].ToString().Trim(),
                            SalesCat1 = string.IsNullOrEmpty(datarow["SalesCat1"].ToString()) ? string.Empty : datarow["SalesCat1"].ToString().Trim(),
                            SalesCat5 = string.IsNullOrEmpty(datarow["SalesCat5"].ToString()) ? string.Empty : datarow["SalesCat5"].ToString().Trim(),
                            SalesCat4 = string.IsNullOrEmpty(datarow["SalesCat4"].ToString()) ? string.Empty : datarow["SalesCat4"].ToString().Trim(),
                            PreviouslyReturnedQty = string.IsNullOrEmpty(datarow["PreviouslyReturnedQty"].ToString()) ? 0 : Convert.ToInt32(datarow["PreviouslyReturnedQty"].ToString().Trim()),
                            UM = string.IsNullOrEmpty(datarow["OrderUM"].ToString()) ? string.Empty : datarow["OrderUM"].ToString().Trim(),
                            OrderQty = string.IsNullOrEmpty(datarow["OrderQty"].ToString().Trim()) ? 0 : Convert.ToInt32(datarow["OrderQty"].ToString().Trim()),
                            RouteId = string.IsNullOrEmpty(datarow["OriginatingRouteId"].ToString().Trim()) ? 0 : Convert.ToInt32(datarow["OriginatingRouteId"].ToString().Trim()),
                            UnitPrice = string.IsNullOrEmpty(datarow["UnitPriceAmt"].ToString().Trim()) ? 0 : Convert.ToDecimal(datarow["UnitPriceAmt"].ToString().Trim()),
                            ExtendedPrice = string.IsNullOrEmpty(datarow["ExtnPriceAmt"].ToString().Trim()) ? 0 : Convert.ToDecimal(datarow["ExtnPriceAmt"].ToString().Trim()),
                            TaxAmount = string.IsNullOrEmpty(datarow["ItemSalesTaxAmt"].ToString().Trim()) ? 0 : Convert.ToDecimal(datarow["ItemSalesTaxAmt"].ToString().Trim()),
                            IsTaxable = datarow["IsTaxable"].ToString() == "0" ? false : true,
                            UMPrice = string.IsNullOrEmpty(datarow["PricingUM"].ToString()) ? string.Empty : datarow["PricingUM"].ToString().Trim(),
                            TransactionUOM = string.IsNullOrEmpty(datarow["DefaultUM"].ToString()) ? string.Empty : datarow["DefaultUM"].ToString().Trim(), /*Default UoM is Transaction UoM*/
                            OrderDateF = string.IsNullOrEmpty(datarow["OrderDate"].ToString()) ? new DateTime() : Convert.ToDateTime(datarow["OrderDate"].ToString().Trim()),
                            OrderID = string.IsNullOrEmpty(datarow["OrderID"].ToString().Trim()) ? 0 : Convert.ToInt32(datarow["OrderID"].ToString().Trim()),
                            QtyOnHand = string.IsNullOrEmpty(datarow["OnHandQty"].ToString().Trim()) ? 0 : Convert.ToInt32(datarow["OnHandQty"].ToString().Trim()),
                            HeldQty = string.IsNullOrEmpty(datarow["HeldQty"].ToString().Trim()) ? 0 : Convert.ToInt32(datarow["HeldQty"].ToString().Trim()),
                            CommittedQty = string.IsNullOrEmpty(datarow["CommittedQty"].ToString().Trim()) ? 0 : Convert.ToInt32(datarow["CommittedQty"].ToString().Trim()),
                            AvailableQty = string.IsNullOrEmpty(datarow["AvailableQty"].ToString().Trim()) ? 0 : Convert.ToInt32(datarow["AvailableQty"].ToString().Trim()),
                            QtyAvailableToReturn = string.IsNullOrEmpty(datarow["AvailableReturnQty"].ToString()) ? 0 : Convert.ToInt32(datarow["AvailableReturnQty"].ToString().Trim()),
                            PrimaryUM = string.IsNullOrEmpty(datarow["PrimaryUM"].ToString()) ? string.Empty : datarow["PrimaryUM"].ToString().Trim(),
                            EnergySurchargeAmt = string.IsNullOrEmpty(datarow["EnergySurchargeAmt"].ToString().Trim()) ? 0 : Convert.ToDouble(datarow["EnergySurchargeAmt"].ToString().Trim()),
                            SettlementID = string.IsNullOrEmpty(datarow["SettlementID"].ToString().Trim()) ? 0 : Convert.ToInt32(datarow["SettlementID"].ToString().Trim()),
                            TransactionDetailID = string.IsNullOrEmpty(datarow["TransactionDetailID"].ToString().Trim()) ? 0 : Convert.ToInt32(datarow["TransactionDetailID"].ToString().Trim())
                        };
                        returnItem.OrderDate = returnItem.OrderDateF.ToString("MM'/'dd'/'yyyy");
                        returnItems.Add(returnItem);
                    }
                    dbResult.Tables[0].Clear();
                    dbResult.Dispose();//Sathish Changed this Code After Disposing only have to do null.
                    dbResult = null;
                   
                    ////Filter by Qty Available To Return 
                    //if (IncludeZeroAvailableItems == false)
                    //    returnItems = returnItems.Where(x => x.QtyAvailableToReturn != 0).ToObservableCollection();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][GetItemsForOrdersTab][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:GetItemsForOrdersTab]");            
            return returnItems;
        }

        public bool IsHoldReturnItems(int returnOrderID, string itemnum)
        {
            bool result = false;
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][Start:DeleteHoldReturnItems]");
            try
            {
                var pickedSum = objOrderManager.CustomerReturnsManager_IsHoldReturnItems(returnOrderID, itemnum);
                result = string.IsNullOrEmpty(pickedSum.Trim());
            }
            catch (Exception ex)
            {
                result = false;
                Logger.Error("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][DeleteHoldReturnItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][End:DeleteHoldReturnItems]");
            return result;
        }

        public TrulyObservableCollection<ReturnItem> GetItemsForReturnsTab()
        {
            TrulyObservableCollection<ReturnItem> returnItems = new TrulyObservableCollection<ReturnItem>();
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:GetItemsForReturnsTab]");
            try
            {
                //                var query = @"
                //                        SELECT DISTINCT 
                //                        S1.OrderID,S1.EnergySurchargeAmt, S1.OrderID AS 'TransactionDetailID'
                //                        ,TL.TDSTID as SettlementID
                //                        ,isnull(S2.OrderStateId,0) as OrderStateId
                //                        ,case isnull(S2.OrderStateId,0) when  0  then 'BL' when {1} then 'OrderDelivered' when {2} then 'OrderReturned' when {3} then 'HOLD' end as OrderState
                //                        ,S2.OrderID as ReturnOrderID
                //                        ,S2.ItemId
                //                        ,im.IMLITM as ItemNumber
                //                        ,im.IMDSC1 as ItemDescription
                //                        ,im.IMLNTY as StkType
                //                        ,im.IMSRP1  as SalesCat1
                //                        ,im.IMSRP5 as SalesCat5
                //                        ,im.IMSRP4 as SalesCat4
                //                        ,ISNULL(S2.ORDERQTY,0) AS PreviouslyReturnedQty
                //                        ,S2.OrderUM as OrderUM
                //                        ,S1.OrderQty
                //                        ,S2.OrderQty as ReturnQty
                //                        ,S2.OriginatingRouteID as RouteId
                //                        ,S2.UnitPriceAmt
                //                        ,S2.ExtnPriceAmt
                //                        ,S2.ItemSalesTaxAmt
                //                        ,S2.IsTaxable
                //                        ,S1.ORDERQTY- ISNULL(S2.ORDERQTY,0) AS AvailableReturnQty
                //                        ,inv.OnHandQuantity as OnHandQty
                //                        ,inv.CommittedQuantity as CommittedQty
                //                        ,inv.HeldQuantity as HeldQty
                //                        ,(inv.OnHandQuantity - (inv.CommittedQuantity + inv.HeldQuantity)) as AvailableQty 
                //                        ,iconfig.PrimaryUM as PrimaryUM
                //                        ,iconfig.PricingUM
                //                        ,iconfig.TransactionUM as DefaultUM
                //                        ,iconfig.OtherUM1 as OtherUM1
                //                        ,iconfig.OtherUM2
                //                        ,S2.OrderDate
                //                        FROM
                //                        (
                //                        SELECT  OH.*, OD.OrderUM, OD.ITEMID,OD.ORDERQTY,OD.RETURNHELDQTY,OD.UnitPriceAmt,OD.ExtnPriceAmt,OD.ItemSalesTaxAmt, isnull(od.IsTaxable,0) as IsTaxable FROM BUSDTA.ORDER_HEADER OH LEFT OUTER JOIN busdta.ORDER_Detail OD ON OH.ORDERID=OD.ORDERID
                //                        WHERE OH.OrderTypeId=" + StatusTypesEnum.SALORD.GetStatusIdFromDB() + " and OH.OrderStateId in (" + ActivityKey.OrderDelivered.GetStatusIdFromDB() + ", " + ActivityKey.OrderReturned.GetStatusIdFromDB() + ")" +
                //                       @"  ) S1
                //                        right OUTER JOIN
                //                        (
                //                        SELECT OD.*,SRM.SALESoRDERID AS RORDERDETAILID,OH.OrderDate,OH.OriginatingRouteID, OH.OrderStateId  FROM BUSDTA.ORDER_HEADER OH LEFT OUTER JOIN busdta.ORDER_Detail OD ON OH.ORDERID=OD.ORDERID
                //                        LEFT OUTER JOIN busdta.SalesOrder_ReturnOrder_Mapping SRM ON  OD.ORDERID=SRM.ReturnOrderID and OD.ItemID=SRM.ItemID
                //                        WHERE OH.OrderTypeId=" + StatusTypesEnum.RETORD.GetStatusIdFromDB() + " AND ISNULL(RORDERDETAILID,0) <> 0" +
                //                        @") S2 ON S1.ORDERID= S2.RORDERDETAILID AND S1.ITEMID=S2.ITEMID
                //                         join busdta.F4101 im on S1.ItemId = im.IMITM 
                //                         join busdta.F4102 ib on ib.IBLITM = im.IMLITM 
                //                         join busdta.ItemConfiguration iconfig on S1.ItemId = iconfig.ItemID
                //                         join busdta.Inventory inv on inv.ItemId = S1.ItemId
                //                        LEFT OUTER JOIN BUSDTA.M50012 TL ON TL.TDREFHDRID=S2.OrderID
                //                        where S1.CustShipToID='{0}'  ORDER BY s1.OrderID DESC
                //                        ";

                //                var query = @"
                //                        SELECT DISTINCT 
                //                        S1.OrderID,S1.EnergySurchargeAmt, S1.OrderID AS 'TransactionDetailID'
                //                        ,TL.TDSTID as SettlementID
                //                        ,isnull(S2.OrderStateId,0) as OrderStateId
                //                        ,case isnull(S2.OrderStateId,0) when  0  then 'BL' when {1} then 'OrderDelivered' when {2} then 'OrderReturned' when {3} then 'HOLD' end as OrderState
                //                        ,S2.OrderID as ReturnOrderID
                //                        ,S2.ItemId
                //                        ,im.IMLITM as ItemNumber
                //                        ,im.IMDSC1 as ItemDescription
                //                        ,im.IMLNTY as StkType
                //                        ,im.IMSRP1  as SalesCat1
                //                        ,im.IMSRP5 as SalesCat5
                //                        ,im.IMSRP4 as SalesCat4
                //                        ,ISNULL(S2.ORDERQTY,0) AS PreviouslyReturnedQty
                //                        ,S2.OrderUM as OrderUM
                //                        ,S1.OrderQty
                //                        ,S2.OrderQty as ReturnQty
                //                        ,S2.OriginatingRouteID as RouteId
                //                        ,S2.UnitPriceAmt
                //                        ,S1.ExtnPriceAmt 
                //                        ,S2.ItemSalesTaxAmt
                //                        ,S2.IsTaxable
                //                        ,S1.ORDERQTY- ISNULL(S2.ORDERQTY,0) AS AvailableReturnQty
                //                        ,inv.OnHandQuantity as OnHandQty
                //                        ,inv.CommittedQuantity as CommittedQty
                //                        ,inv.HeldQuantity as HeldQty
                //                        ,(inv.OnHandQuantity - (inv.CommittedQuantity + inv.HeldQuantity)) as AvailableQty 
                //                        ,iconfig.PrimaryUM as PrimaryUM
                //                        ,iconfig.PricingUM
                //                        ,iconfig.TransactionUM as DefaultUM
                //                        ,iconfig.OtherUM1 as OtherUM1
                //                        ,iconfig.OtherUM2
                //                        ,S2.OrderDate, S2.DocumentId
                //                        FROM
                //                        (
                //                        SELECT  OH.*, OD.OrderUM, OD.ITEMID,OD.ORDERQTY,OD.RETURNHELDQTY,OD.UnitPriceAmt,OD.ExtnPriceAmt,OD.ItemSalesTaxAmt, isnull(od.IsTaxable,0) as IsTaxable FROM BUSDTA.ORDER_HEADER OH LEFT OUTER JOIN busdta.ORDER_Detail OD ON OH.ORDERID=OD.ORDERID
                //                        WHERE OH.OrderTypeId=" + StatusTypesEnum.SALORD.GetStatusIdFromDB() + " and OH.OrderStateId in (" + ActivityKey.OrderDelivered.GetStatusIdFromDB() + ", " + ActivityKey.OrderReturned.GetStatusIdFromDB() + ", " + StatusTypesEnum.STTLD.GetStatusIdFromDB() + ")" +
                //                       @"  ) S1
                //                        right OUTER JOIN
                //                        (
                //                        SELECT OD.*,od.RelatedOrderID AS RORDERDETAILID,OH.DocumentId, OH.OrderDate,OH.OriginatingRouteID, OH.OrderStateId  FROM BUSDTA.ORDER_HEADER OH LEFT OUTER JOIN busdta.ORDER_Detail OD ON OH.ORDERID=OD.ORDERID                       
                //                        WHERE OH.OrderTypeId=" + StatusTypesEnum.RETORD.GetStatusIdFromDB() + " AND ISNULL(RORDERDETAILID,0) <> 0" +
                //                        @") S2 ON S1.ORDERID= S2.RORDERDETAILID AND S1.ITEMID=S2.ITEMID
                //                         join busdta.F4101 im on S1.ItemId = im.IMITM 
                //                         join busdta.F4102 ib on ib.IBLITM = im.IMLITM 
                //                         join busdta.ItemConfiguration iconfig on S1.ItemId = iconfig.ItemID
                //                         join busdta.Inventory inv on inv.ItemId = S1.ItemId
                //                        LEFT OUTER JOIN BUSDTA.M50012 TL ON TL.TDREFHDRID=S2.OrderID 
                //                        where S1.CustShipToID='{0}'  ORDER BY s1.OrderID DESC
                //                        ";


                //                var query = @"
                //                        SELECT DISTINCT 
                //                        S1.OrderID,S1.EnergySurchargeAmt, S1.OrderID AS 'TransactionDetailID'
                //                        ,TL.TDSTID as SettlementID
                //                        ,isnull(S2.OrderStateId,0) as OrderStateId
                //                        ,case isnull(S2.OrderStateId,0) when  0  then 'BL' when {1} then 'OrderDelivered' when {2} then 'OrderReturned' when {3} then 'HOLD' end as OrderState
                //                        ,S2.OrderID as ReturnOrderID
                //                        ,S2.ItemId
                //                        ,im.IMLITM as ItemNumber
                //                        ,im.IMDSC1 as ItemDescription
                //                        ,im.IMLNTY as StkType
                //                        ,im.IMSRP1  as SalesCat1
                //                        ,im.IMSRP5 as SalesCat5
                //                        ,im.IMSRP4 as SalesCat4
                //                        ,ISNULL(S2.ORDERQTY,0) AS PreviouslyReturnedQty
                //                        ,S2.OrderUM as OrderUM
                //                        ,S1.OrderQty
                //                        ,S2.OrderQty as ReturnQty
                //                        ,S2.OriginatingRouteID as RouteId
                //                        ,S2.UnitPriceAmt
                //                        ,S1.ExtnPriceAmt 
                //                        ,S2.ItemSalesTaxAmt
                //                        ,S2.IsTaxable
                //                        ,case when s2.orderstateid in (39,40) then S1.ORDERQTY- S2.OrderQty else S1.ORDERQTY- ISNULL(prvorderqty,0) end AS AvailableReturnQty
                //                        ,inv.OnHandQuantity as OnHandQty
                //                        ,inv.CommittedQuantity as CommittedQty
                //                        ,inv.HeldQuantity as HeldQty
                //                        ,(inv.OnHandQuantity - (inv.CommittedQuantity + inv.HeldQuantity)) as AvailableQty 
                //                        ,iconfig.PrimaryUM as PrimaryUM
                //                        ,iconfig.PricingUM
                //                        ,iconfig.TransactionUM as DefaultUM
                //                        ,iconfig.OtherUM1 as OtherUM1
                //                        ,iconfig.OtherUM2
                //                        ,S2.OrderDate, S2.DocumentId
                //                        FROM
                //                        (
                //                        SELECT  OH.*, OD.OrderUM, OD.ITEMID,OD.ORDERQTY,OD.RETURNHELDQTY,OD.UnitPriceAmt,OD.ExtnPriceAmt,OD.ItemSalesTaxAmt, isnull(od.IsTaxable,0) as IsTaxable FROM BUSDTA.ORDER_HEADER OH LEFT OUTER JOIN busdta.ORDER_Detail OD ON OH.ORDERID=OD.ORDERID
                //                        WHERE OH.OrderTypeId=" + StatusTypesEnum.SALORD.GetStatusIdFromDB() + " and OH.OrderStateId in (" + ActivityKey.OrderDelivered.GetStatusIdFromDB() + ", " + ActivityKey.OrderReturned.GetStatusIdFromDB() + ", " + StatusTypesEnum.STTLD.GetStatusIdFromDB() + ")" +
                //                      @"  ) S1
                //                        right OUTER JOIN
                //                        (
                //                        SELECT OD.*,
                //                        (SELECT distinct SUM(ORDERQTY) FROM busdta.ORDER_Detail det join BUSDTA.Order_Header head on det.orderid = head.orderid  where det.RELATEDORDERID = od.RelatedOrderID and det.itemid = od.itemid and head.orderstateid  not  in (39,40) group by det.RELATEDORDERID) as prvorderqty,
                //                           od.RelatedOrderID AS RORDERDETAILID,OH.DocumentId, OH.OrderDate,OH.OriginatingRouteID, OH.OrderStateId  FROM BUSDTA.ORDER_HEADER OH LEFT OUTER JOIN busdta.ORDER_Detail OD ON OH.ORDERID=OD.ORDERID                       
                //                        WHERE OH.OrderTypeId=" + StatusTypesEnum.RETORD.GetStatusIdFromDB() + " AND ISNULL(RORDERDETAILID,0) <> 0" +
                //                       @") S2 ON S1.ORDERID= S2.RORDERDETAILID AND S1.ITEMID=S2.ITEMID
                //                         join busdta.F4101 im on S1.ItemId = im.IMITM 
                //                         join busdta.F4102 ib on ib.IBLITM = im.IMLITM 
                //                         join busdta.ItemConfiguration iconfig on S1.ItemId = iconfig.ItemID
                //                         join busdta.Inventory inv on inv.ItemId = S1.ItemId
                //                        LEFT OUTER JOIN BUSDTA.M50012 TL ON TL.TDREFHDRID=S2.OrderID 
                //                        where S1.CustShipToID='{0}'  ORDER BY s1.OrderID DESC
                //                        ";

                //                query = string.Format(query, PayloadManager.ReturnOrderPayload.Customer.CustomerNo, ActivityKey.OrderDelivered.GetStatusIdFromDB(), ActivityKey.OrderReturned.GetStatusIdFromDB(), ActivityKey.HoldAtROEntry.GetStatusIdFromDB());

                string query = "call busdta.SP_GetItemsForReturnsTab(" + ActivityKey.OrderDelivered.GetStatusIdFromDB() + ", " + ActivityKey.OrderReturned.GetStatusIdFromDB() + ", " + ActivityKey.HoldAtROEntry.GetStatusIdFromDB() + "," + StatusTypesEnum.SALORD.GetStatusIdFromDB() + ", " + StatusTypesEnum.STTLD.GetStatusIdFromDB() + "," + StatusTypesEnum.RETORD.GetStatusIdFromDB() + ",'" + PayloadManager.ReturnOrderPayload.Customer.CustomerNo + "')";
                var dbResult = DbEngine.ExecuteDataSet(query);
                if (dbResult.HasData())
                {
                    foreach (DataRow datarow in dbResult.Tables[0].Rows)
                    {
                        ReturnItem returnItem = new ReturnItem
                        {
                            DocumentId = string.IsNullOrEmpty(datarow["DocumentId"].ToString()) ? 0 : Convert.ToDecimal(datarow["DocumentId"].ToString()),
                            ItemId = string.IsNullOrEmpty(datarow["ItemId"].ToString()) ? string.Empty : datarow["ItemId"].ToString().Trim(),
                            ItemNumber = string.IsNullOrEmpty(datarow["ItemNumber"].ToString()) ? string.Empty : datarow["ItemNumber"].ToString().Trim(),
                            ItemDescription = string.IsNullOrEmpty(datarow["ItemDescription"].ToString()) ? string.Empty : datarow["ItemDescription"].ToString().Trim(),
                            StkType = string.IsNullOrEmpty(datarow["StkType"].ToString()) ? string.Empty : datarow["StkType"].ToString().Trim(),
                            SalesCat1 = string.IsNullOrEmpty(datarow["SalesCat1"].ToString()) ? string.Empty : datarow["SalesCat1"].ToString().Trim(),
                            SalesCat5 = string.IsNullOrEmpty(datarow["SalesCat5"].ToString()) ? string.Empty : datarow["SalesCat5"].ToString().Trim(),
                            SalesCat4 = string.IsNullOrEmpty(datarow["SalesCat4"].ToString()) ? string.Empty : datarow["SalesCat4"].ToString().Trim(),
                            PreviouslyReturnedQty = string.IsNullOrEmpty(datarow["PreviouslyReturnedQty"].ToString()) ? 0 : Convert.ToInt32(datarow["PreviouslyReturnedQty"].ToString().Trim()),
                            UM = string.IsNullOrEmpty(datarow["OrderUM"].ToString()) ? string.Empty : datarow["OrderUM"].ToString().Trim(),
                            OrderQty = string.IsNullOrEmpty(datarow["OrderQty"].ToString()) ? 0 : Convert.ToInt32(datarow["OrderQty"].ToString().Trim()),
                            ReturnQty = string.IsNullOrEmpty(datarow["ReturnQty"].ToString()) ? 0 : Convert.ToInt32(datarow["ReturnQty"].ToString().Trim()),
                            RouteId = string.IsNullOrEmpty(datarow["RouteId"].ToString()) ? 0 : Convert.ToInt32(datarow["RouteId"].ToString().Trim()),
                            UnitPrice = string.IsNullOrEmpty(datarow["UnitPriceAmt"].ToString()) ? 0 : Convert.ToDecimal(datarow["UnitPriceAmt"].ToString().Trim()),
                            ExtendedPrice = string.IsNullOrEmpty(datarow["ExtnPriceAmt"].ToString()) ? 0 : Convert.ToDecimal(datarow["ExtnPriceAmt"].ToString().Trim()),
                            TaxAmount = string.IsNullOrEmpty(datarow["ItemSalesTaxAmt"].ToString()) ? 0 : Convert.ToDecimal(datarow["ItemSalesTaxAmt"].ToString().Trim()),
                            IsTaxable = datarow["IsTaxable"].ToString() == "0" ? false : true,
                            UMPrice = string.IsNullOrEmpty(datarow["PricingUM"].ToString()) ? string.Empty : datarow["PricingUM"].ToString().Trim(),
                            TransactionUOM = string.IsNullOrEmpty(datarow["DefaultUM"].ToString()) ? string.Empty : datarow["DefaultUM"].ToString().Trim(),
                            OrderDateF = string.IsNullOrEmpty(datarow["OrderDate"].ToString()) ? new DateTime() : Convert.ToDateTime(datarow["OrderDate"].ToString().Trim()),
                            OrderID = string.IsNullOrEmpty(datarow["OrderID"].ToString()) ? 0 : Convert.ToInt32(datarow["OrderID"].ToString().Trim()),
                            QtyOnHand = string.IsNullOrEmpty(datarow["OnHandQty"].ToString()) ? 0 : Convert.ToInt32(datarow["OnHandQty"].ToString().Trim()),
                            HeldQty = string.IsNullOrEmpty(datarow["HeldQty"].ToString()) ? 0 : Convert.ToInt32(datarow["HeldQty"].ToString().Trim()),
                            CommittedQty = string.IsNullOrEmpty(datarow["CommittedQty"].ToString()) ? 0 : Convert.ToInt32(datarow["CommittedQty"].ToString().Trim()),
                            AvailableQty = string.IsNullOrEmpty(datarow["AvailableQty"].ToString()) ? 0 : Convert.ToInt32(datarow["AvailableQty"].ToString().Trim()),
                            PrimaryUM = string.IsNullOrEmpty(datarow["PrimaryUM"].ToString()) ? string.Empty : datarow["PrimaryUM"].ToString().Trim(),
                            QtyAvailableToReturn = string.IsNullOrEmpty(datarow["AvailableReturnQty"].ToString()) ? 0 : Convert.ToInt32(datarow["AvailableReturnQty"].ToString().Trim()),
                            ReturnOrderID = string.IsNullOrEmpty(datarow["ReturnOrderID"].ToString()) ? 0 : Convert.ToInt32(datarow["ReturnOrderID"].ToString().Trim()),
                            OrderState = string.IsNullOrEmpty(datarow["OrderState"].ToString()) ? "" : datarow["OrderState"].ToString().Trim(),
                            OrderStateID = string.IsNullOrEmpty(datarow["OrderStateID"].ToString()) ? 0 : Convert.ToInt32(datarow["OrderStateID"].ToString().Trim()),
                            SettlementID = string.IsNullOrEmpty(datarow["SettlementID"].ToString()) ? 0 : Convert.ToInt32(datarow["SettlementID"].ToString().Trim()),
                            EnergySurchargeAmt = string.IsNullOrEmpty(datarow["EnergySurchargeAmt"].ToString()) ? 0 : Convert.ToDouble(datarow["EnergySurchargeAmt"].ToString().Trim()),
                            TransactionDetailID = string.IsNullOrEmpty(datarow["TransactionDetailID"].ToString()) ? 0 : Convert.ToInt32(datarow["TransactionDetailID"].ToString().Trim())

                        };

                        returnItem.OrderDate = returnItem.OrderDateF.ToString("MM'/'dd'/'yyyy");
                        returnItems.Add(returnItem);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][GetItemsForReturnsTab][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:GetItemsForReturnsTab]");
            return returnItems;
        }

        //Unused Function Commented for performance   --Vignesh D
        //public ReturnOrder GetOrderDetails(int OrderId)
        //{
        //    var ro_detail = new ReturnOrder();
        //    Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][Start:GetOrderDetails]");
        //    try
        //    {
        //        //              var oh_query = @"select oh.*, (SELECT if(isnull(LTRIM(RTRIM(M.TDSTTLID)),0) > 0) then 1 else 0 AS STTID FROM BUSDTA.M50012 M WHERE 
        //        //                               TDSTAT='ORDERRETURNED' AND TDREFHDRID = " + OrderId + " ) as IsSettled " +
        //        //                               " from busdta.Order_Header oh where orderid = " + OrderId + "";
        //        //              var oh_Dbset = DbEngine.ExecuteDataSet(oh_query);

        //        var oh_Dbset = objOrderManager.CustomerReturnsManager_GetOrderDetails(OrderId);

        //        if (oh_Dbset.HasData())
        //        {
        //            foreach (DataRow order in oh_Dbset.Tables[0].Rows)
        //            {
        //                ReturnOrder ro_item = new ReturnOrder();
        //                ro_item.Surcharge = string.IsNullOrEmpty(order["EnergySurchargeAmt"].ToString()) ? 0 : Convert.ToDecimal(order["EnergySurchargeAmt"].ToString().Trim());
        //                ro_item.OrderId = OrderId;
        //                ro_item.IsSettled = !string.IsNullOrEmpty(order["IsSettled"].ToString()) && Convert.ToBoolean(order["IsSettled"]);
        //                ro_item.OrderDate = string.IsNullOrEmpty(order["OrderDate"].ToString()) ? new DateTime() : Convert.ToDateTime(order["OrderDate"].ToString().Trim());

        //                ro_detail = ro_item;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][GetOrderDetails][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //        ro_detail = null;
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][End:GetOrderDetails]");
        //    return ro_detail;
        //}

        /// <summary>
        /// Unused function.
        /// 10-13-2016
        /// </summary>
        /// <returns></returns>
        /// 
        //Unused Function commented for Performance   --Vignesh D
        //        public TrulyObservableCollection<ReturnItem> GetItemsForPick()
        //        {
        //            TrulyObservableCollection<ReturnItem> returnItems = new TrulyObservableCollection<ReturnItem>();
        //            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:GetItemsForPick]");
        //            try
        //            {
        //                var query = @"SELECT OD.OrderID ,OD.OrderDetailId, OD.OrderQty, OD.OrderUM,OH.HoldCommitted ,
        //                                inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo', inventory.RouteId, inventory.OnHandQuantity AS 'OnHandQty'
        //                                ,inventory.CommittedQuantity AS 'CommittedQty'
        //                                , inventory.HeldQuantity AS 'HeldQty',inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription'
        //                                ,(isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
        //                                ,OD.OrderDetailId AS 'TransactionDetailID'
        //                                , inventory.OnHandQuantity AS 'ActualQtyOnHand'
        //                                ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUM'
        //                                , OD.OrderQty AS 'TransactionQty'
        //                                ,OD.OrderUM AS 'TransactionUOM'
        //                                FROM BUSDTA.Order_Detail OD 
        //                                JOIN BUSDTA.Order_Header OH  ON OD.OrderID=OH.OrderID AND OD.RouteID=OH.RouteID
        //                                JOIN BUSDTA.Inventory inventory ON inventory.ItemId=OD.ItemId AND inventory.RouteId=OD.RouteID AND inventory.RouteId=OH.RouteID
        //                                join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID 
        //                                join busdta.F4101 IM on LTRIM(RTRIM(inventory.ItemNumber)) = IM.IMLITM  
        //                                WHERE   OD.OrderID= " + PayloadManager.ReturnOrderPayload.ReturnOrderID + "";

        //                DataSet dbResult = DbEngine.ExecuteDataSet(query);
        //                if (dbResult.HasData())
        //                {
        //                    returnItems = new TrulyObservableCollection<ReturnItem>(dbResult.GetEntityList<ReturnItem>());
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][GetItemsForPick][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //            }
        //            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:GetItemsForPick]");
        //            return returnItems;
        //        }



        //        public int CreateReturnOrder(ObservableCollection<ReturnItem> returnitems, string ReturnOrderID)
        //        {
        //            int returnValue = 1;

        //            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:SaveOrder]");
        //            try
        //            {

        //                string query = "select count(OrderID) from BUSDTA.ORDER_HEADER where OrderID='{0}'";
        //                query = string.Format(query, ReturnOrderID);
        //                bool orderExists = Convert.ToBoolean(Convert.ToInt32(DbEngine.ExecuteScalar(query)));
        //                if (orderExists)
        //                {

        //                    query = @"Update BUSDTA.ORDER_HEADER set 
        //                                    TotalCoffeeAmt='{0}'
        //                                    ,TotalAlliedAmt='{1}'
        //                                    ,OrderTotalAmt='{2}'
        //                                    ,SalesTaxAmt='{3}'
        //                                    ,InvoiceTotalAmt='{4}'
        //                                    ,EnergySurchargeAmt='{5}'
        //                                    ,HoldCommitted='0'
        //                                    ,UpdatedBy='{6}'
        //                                    ,UpdatedDatetime=getdate() 
        //                                    ,OrderStateId='{8}'
        //                                    where OrderID='{7}';";
        //                    query = string.Format(query, PayloadManager.ReturnOrderPayload.TotalCoffeeAmount, PayloadManager.ReturnOrderPayload.TotalAlliedAmount, PayloadManager.ReturnOrderPayload.OrderTotalAmount,
        //                        PayloadManager.ReturnOrderPayload.ROSalesTax, PayloadManager.ReturnOrderPayload.InvoiceTotalAmt, PayloadManager.ReturnOrderPayload.EnergySurcharge,
        //                        UserManager.UserId, ReturnOrderID, ActivityKey.CreateReturnOrder.GetStatusIdFromDB());
        //                    DbEngine.ExecuteNonQuery(query);

        //                    Int16 ival1 = 0;
        //                    // Step 2 : Insert into Order Detail
        //                    foreach (var item in returnitems)
        //                    {
        //                        query = "select count(OrderID) from BUSDTA.Order_Detail where OrderID='{0}'and  ItemId='{2}'";
        //                        query = string.Format(query, ReturnOrderID, CommonNavInfo.RouteID, item.ItemId);
        //                        bool orderItemExists = Convert.ToBoolean(Convert.ToInt32(DbEngine.ExecuteScalar(query)));

        //                        #region Check if Item exists , if yes then update else add a new mapping
        //                        if (orderItemExists)
        //                        {
        //                            var isTaxable = item.TaxAmount > 0 ? "1" : "0";
        //                            query = @"update BUSDTA.Order_Detail set
        //                                    OrderQty='{0}'
        //                                    ,OrderUM='{1}'
        //                                    ,UnitPriceAmt='{2}'
        //                                    ,ExtnPriceAmt='{3}'
        //                                    ,ItemSalesTaxAmt='{4}'
        //                                    ,IsTaxable='{5}'
        //                                    ,ReturnHeldQty='{6}'
        //                                    ,UpdatedBy='{7}'
        //                                    ,UpdatedDatetime=getdate()
        //                                    ,OrderQtyInPricingUM ='{12}'
        //                                    ,UnitPriceAmtInPriceUoM ='{13}'   
        //                                    where OrderID='{8}'  and ItemId='{11}'";
        //                            string unitpriceamt = DbEngine.ExecuteScalar("select UnitPriceAmtInPriceUoM from BUSDTA.Order_Detail where OrderID =" + "'" + item.OrderID + "' and ItemID='" + item.ItemId + "'");
        //                            query = string.Format(query, item.ReturnQty, item.UM, item.UnitPrice, item.ExtendedPrice, item.TaxAmount, isTaxable, item.NonSellableQty, UserManager.UserId, ReturnOrderID, CommonNavInfo.RouteID, item.OrderDetailID,
        //                                item.ItemId, decimal.Parse(pm.jdeUOMConversion(item.UM, item.UMPrice, int.Parse(item.ItemId)).ToString()) * item.ReturnQty,
        //                                unitpriceamt);
        //                            //   (decimal.Parse(pm.jdeUOMConversion(item.UM, item.UMPrice, int.Parse(item.ItemId)).ToString())) * Convert.ToDecimal(pm.loadPriceDetailsGrid(CommonNavInfo.UserBranch, CommonNavInfo.Customer.customerNo, item.ItemNumber, item.ReturnQty.ToString())));
        //                            // (Convert.ToDecimal(pm.loadPriceDetailsGrid(CommonNavInfo.UserBranch, CommonNavInfo.Customer.customerNo, item.ItemNumber, item.ReturnQty.ToString()))));
        //                            DbEngine.ExecuteNonQuery(query);
        //                        }
        //                        else
        //                        {
        //                            ival1++;
        //                            DataTable vOrderItemdetails = ItemManager.GetItemDetails(item.ItemId);

        //                            var isTaxable = item.TaxAmount > 0 ? "1" : "0";
        //                            query = @"INSERT INTO BUSDTA.Order_Detail (OrderID,LineID,ItemId,LongItem,LineType,IsNonStock
        //                                     ,SalesCat1,SalesCat2 ,SalesCat3 ,SalesCat4 ,SalesCat5 ,PurchasingCat1 ,PurchasingCat2 ,PurchasingCat3 ,PurchasingCat4 ,PurchasingCat5 ,
        //                                    OrderQty,OrderQtyInPricingUM,OrderUM,PricingUM,UnitPriceAmtInPriceUoM,UnitPriceOriginalAmtInPriceUM ,UnitPriceAmt,
        //                                    ExtnPriceAmt,ItemSalesTaxAmt,PriceOverrideReasonCodeId,IsTaxable,
        //                                    ReturnHeldQty,CreatedDatetime,UpdatedBy,UpdatedDatetime) " +
        //                                     " VALUES( " + ReturnOrderID + "," + ival1 + "," + item.ItemId + ",'" +
        //                                      vOrderItemdetails.Rows[0]["LongItm"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["IBLNTY"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["IsNonStock"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat1"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat2"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat3"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat4"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat5"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat1"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat2"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat3"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat4"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat5"] + "'," +
        //                                        item.ReturnQty +
        //                                        ",'" + decimal.Parse(pm.jdeUOMConversion(item.UM, item.UMPrice, int.Parse(item.ItemId)).ToString()) * item.ReturnQty + "','" +
        //                                         item.UM + "'," +
        //                                         "'" + item.UMPrice + "'," +
        //                                          "'" + (item.ReturnQty * decimal.Parse(pm.jdeUOMConversion(item.UM, item.UMPrice, int.Parse(item.ItemId)).ToString())) * Convert.ToDecimal(pm.loadPriceDetailsGrid(CommonNavInfo.UserBranch, CommonNavInfo.Customer.customerNo, item.ItemNumber, item.ReturnQty.ToString())) + @"'," +
        //                                        item.UnitPrice + "," + item.UnitPrice + "," + item.ExtendedPrice + "," + item.TaxAmount + "," +
        //                                     " NULL," + isTaxable + "," + item.NonSellableQty + ",getdate()," + UserManager.UserId + ",getdate())";
        //                            DbEngine.ExecuteNonQuery(query);


        //                            string OrderDetailId = DbEngine.ExecuteScalar("select @@identity").ToString();




        //                            // Commented For: SLER1-44 Sales_ReturnOrder_Mapping - Logic Change

        //                            // Step 3 : Insert into SalesOrder_ReturnOrder Mapping table
        //                            //query = "insert into busdta.SalesOrder_ReturnOrder_Mapping (SalesOrderId,ReturnOrderID,ItemID,OrderDetailID,RouteID,CreatedBy,CreatedDatetime) values({0},{1},{2},{3},{4},{5},getdate())";
        //                            //query = string.Format(query, item.OrderID, ReturnOrderID, item.ItemId, item.OrderID, CommonNavInfo.RouteID, PayloadManager.ApplicationPayload.LoggedInUserID);
        //                            //DbEngine.ExecuteNonQuery(query);
        //                        }
        //                        #endregion

        //                    }
        //                }
        //                else
        //                {
        //                    // Insert the new order in Order_Header, Order_Detailsand SalesOrder_ReturnOrder_Mapping
        //                    // Step 1 : Save Details in Order Header
        //                    CustomerManager Cm = new CustomerManager();
        //                    DataTable dt = new DataTable();

        //                    dt = Cm.GetCustomerDetailsForOrder(PayloadManager.ReturnOrderPayload.Customer.CustomerNo);
        //                    var Oh_query = @" INSERT INTO BUSDTA.ORDER_HEADER (OrderID,OriginatingRouteID,Branch,OrderTypeId,CustShipToId,CustBillToId,
        //                                      CustParentId,OrderDate,TotalCoffeeAmt,TotalAlliedAmt," +
        //                                    " OrderTotalAmt,SalesTaxAmt,InvoiceTotalAmt,EnergySurchargeAmt,SurchargeReasonCodeId,OrderStateId,OrderSignature,ChargeOnAccountSignature,VoidReasonCodeId,ChargeOnAccount,HoldCommitted," +
        //                                    " JDEOrderNumber,JDEOrderCompany,JDEOrderType,JDEInvoiceCompany,JDEOInvoiceType,JDEZBatchNumber,CustomerReference1,CustomerReference2, JDEProcessID,SettlementID,VarianceAmt, " +
        //                                    " CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime,  PaymentTerms,  AdjustmentSchedule,TaxExplanationCode,TaxArea)" +
        //                                    " VALUES(" + ReturnOrderID + "," + CommonNavInfo.RouteID + ",'" + GetBranch(ViewModels.CommonNavInfo.UserBranch) + "'," + StatusTypesEnum.RETORD.GetStatusIdFromDB() + "," +
        //                                    PayloadManager.ReturnOrderPayload.Customer.CustomerNo + "," + "'" + dt.Rows[0]["BillTo"] + "'," +
        //                                    "'" + pm.GetShipToParent(Convert.ToInt64(ViewModels.CommonNavInfo.Customer.customerNo)) + "'," +
        //                                              "getdate()," + PayloadManager.ReturnOrderPayload.TotalCoffeeAmount + "," + PayloadManager.ReturnOrderPayload.TotalAlliedAmount + "," +
        //                                              "" + PayloadManager.ReturnOrderPayload.OrderTotalAmount + "," + PayloadManager.ReturnOrderPayload.ROSalesTax + "," + PayloadManager.ReturnOrderPayload.InvoiceTotalAmt + "" +
        //                                              "," + PayloadManager.ReturnOrderPayload.EnergySurcharge + ",0,null,NULL,NULL,NULL,NULL,'0'," +
        //                                              " '" + ReturnOrderID + "','00800','CO','00800','RM','','','','0','0','0', " +
        //                                              " " + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate() " + ", '" + dt.Rows[0]["PaymentTerms"] + "', '" +
        //                                                dt.Rows[0]["AdjustmentSchedule"] + "' ,'" + dt.Rows[0]["TaxExplanationCode"] + "','" + dt.Rows[0]["Taxarea"] + "') ";
        //                    DbEngine.ExecuteNonQuery(Oh_query);

        //                    // Step 2 : Insert into Order Detail
        //                    Int16 ival = 0;


        //                    foreach (var item in returnitems)
        //                    {
        //                        ival++;
        //                        DataTable vOrderItemdetails = ItemManager.GetItemDetails(item.ItemId);
        //                        string JDEBranch = "00800";
        //                        var isTaxable = item.TaxAmount > 0 ? "1" : "0";

        //                        //decimal UnitPrice  = OrderManager.GetPriceByUomFactor(item.ItemId,vOrderItemdetails.Rows[0]["PriceUOM"],vOrderItemdetails.Rows[0]["AppliedUOM"],vOrderItemdetails.Rows[0]["UnitPriceForPricingUOM"])

        //                        query = @"INSERT INTO BUSDTA.Order_Detail (OrderID,LineID,JDEOrderLine,CustShipToID,ItemId,LongItem,LineType,IsNonStock
        //                                     ,SalesCat1,SalesCat2 ,SalesCat3 ,SalesCat4 ,SalesCat5 ,PurchasingCat1 ,PurchasingCat2 ,PurchasingCat3 ,PurchasingCat4 ,PurchasingCat5 ,RelatedOrderID,RelatedLineID,
        //                                     JDEOrderNumber,JDEOrderCompany,JDEOrderType,JDEInvoiceCompany,JDEOInvoiceType,JDEZBatchNumber,
        //                                    JDEProcessID,SettlementID,ExtendedAmtVariance,TaxAmountAmtVariance,HoldCode,
        //                                    OrderQty,OrderQtyInPricingUM,OrderUM,PricingUM,UnitPriceAmtInPriceUoM,UnitPriceOriginalAmtInPriceUM ,UnitPriceAmt,
        //                                    ExtnPriceAmt,ItemSalesTaxAmt,PriceOverriden,PriceOverrideReasonCodeId,IsTaxable,
        //                                    ReturnHeldQty,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime) " +
        //                                     " VALUES( " + ReturnOrderID + "," + ival + "," + ival + "," + PayloadManager.ReturnOrderPayload.Customer.CustomerNo + "," + item.ItemId + ",'" +
        //                                      vOrderItemdetails.Rows[0]["LongItm"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["IBLNTY"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["IsNonStock"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat1"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat2"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat3"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat4"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat5"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat1"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat2"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat3"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat4"] + "'" +
        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat5"] + "'" +
        //                                        ",'" + item.OrderID + "'" +
        //                                        ",'" + ival + "'" +
        //                                        ",'" + ReturnOrderID + "'" +
        //                                        ",'" + JDEBranch + "'" +
        //                                        ",'CO'" +
        //                                        ",'" + JDEBranch + "'" +
        //                                        ",'RM'" +
        //                                        ",''" +
        //                                        ",''" +
        //                                        ",'0'" +
        //                                        ",'0'" +
        //                                        ",'0'" +
        //                                        ",''," +
        //                                        item.ReturnQty +
        //                                        ",'" + decimal.Parse(pm.jdeUOMConversion(item.UM, item.UMPrice, int.Parse(item.ItemId)).ToString()) * item.ReturnQty + "','" +
        //                                         item.UM + "'," +
        //                                         "'" + item.UMPrice + "'," +
        //                            //  "'" + (item.UnitPrice) + @"'," +
        //                                          "'" + (item.OrderQty * decimal.Parse(pm.jdeUOMConversion(item.UM, item.UMPrice, int.Parse(item.ItemId)).ToString())) * Convert.ToDecimal(pm.loadPriceDetailsGrid(CommonNavInfo.UserBranch, CommonNavInfo.Customer.customerNo, item.ItemNumber, item.ReturnQty.ToString())) + @"'," +
        //                                        item.UnitPrice + "," + item.UnitPrice + "," + item.ROExtendedPrice + "," + item.TaxAmount + "," +
        //                                      " 1,5," + isTaxable + "," + item.NonSellableQty + "," + UserManager.UserId + "  ,getdate()," + UserManager.UserId + ",getdate())";
        //                        DbEngine.ExecuteNonQuery(query);


        //                        // New Logic(08/06/2016) - Update Price Details from Sales Order
        //                        DataTable tblSODetails = DbEngine.ExecuteDataSet("SELECT UnitPriceAmtInPriceUoM,UnitPriceOriginalAmtInPriceUM ,UnitPriceAmt FROM BUSDTA.Order_Detail WHERE OrderID='" + item.OrderID + "' AND ItemID='" + item.ItemId + "' ").Tables[0];
        //                        StringBuilder MyQuery = new StringBuilder();
        //                        MyQuery.Append(" UPDATE BUSDTA.Order_Detail SET UnitPriceOriginalAmtInPriceUM ='" + tblSODetails.Rows[0]["UnitPriceOriginalAmtInPriceUM"].ToString() + "', ");
        //                        MyQuery.Append(" UnitPriceAmtInPriceUoM ='" + tblSODetails.Rows[0]["UnitPriceAmtInPriceUoM"].ToString() + "', ");
        //                        MyQuery.Append(" UnitPriceAmt ='" + tblSODetails.Rows[0]["UnitPriceAmt"].ToString() + "' ");
        //                        MyQuery.Append(" WHERE OrderID='" + ReturnOrderID + "' AND ItemID='" + item.ItemId + "'");
        //                        int TranResult = DbEngine.ExecuteNonQuery(MyQuery.ToString());

        //                        item.OrderDetailID = string.IsNullOrEmpty(ReturnOrderID) ? 0 : Convert.ToInt32(ReturnOrderID);

        //                        // Commented By Velmani - Reason: Update Statement not Proper & Even not filter by itemId itself.
        //                        // Comment Start
        //                        ////string unitpriceamt = DbEngine.ExecuteScalar("select UnitPriceAmtInPriceUoM from BUSDTA.Order_Detail where OrderID =" + "'" + item.OrderID + "'");
        //                        ////string unitpriceoriginalamt = DbEngine.ExecuteScalar("select UnitPriceOriginalAmtInPriceUM from BUSDTA.Order_Detail where OrderID =" + "'" + item.OrderID + "'");
        //                        ////var OD_query = @"update busdta.order_detail set " +
        //                        ////   "UnitPriceAmtInPriceUoM = '" + tblSODetails.Rows[0]["UnitPriceAmtInPriceUoM"].ToString + "'," +
        //                        ////   "UnitPriceOriginalAmtInPriceUM = '" + unitpriceoriginalamt + "'" +
        //                        ////   "where OrderID=" + ReturnOrderID + " and ItemId = " + item.ItemId + "";
        //                        ////DbEngine.ExecuteNonQuery(OD_query);
        //                        ////string OrderDetailId = DbEngine.ExecuteScalar("select @@identity").ToString();
        //                        ////item.OrderDetailID = string.IsNullOrEmpty(OrderDetailId) ? 0 : Convert.ToInt32(OrderDetailId);
        //                        // Comment End


        //                        // Commented For: SLER1-44 Sales_ReturnOrder_Mapping - Logic Change (Velmani Karnan)

        //                        // Step 3 : Insert into SalesOrder_ReturnOrder Mapping table
        //                        //string returnOrderMappingQuery = "insert into busdta.SalesOrder_ReturnOrder_Mapping (SalesOrderId,ReturnOrderID,ItemID,OrderDetailID,RouteID,CustShipToId,CreatedBy,CreatedDatetime) values({0},{1},{2},{3},{4},{5},{6},getdate())";
        //                        //returnOrderMappingQuery = string.Format(returnOrderMappingQuery, item.OrderID, ReturnOrderID, item.ItemId, ival, CommonNavInfo.RouteID,PayloadManager.ReturnOrderPayload.Customer.CustomerNo, PayloadManager.ApplicationPayload.LoggedInUserID);
        //                        //DbEngine.ExecuteNonQuery(returnOrderMappingQuery);

        //                    }
        //                }
        //                SalesLogicExpress.Domain.Activity a = LogActivity(returnitems.Clone(), PayloadManager.ReturnOrderPayload.Customer, PayloadManager.ReturnOrderPayload.Customer.StopDate.Value, PayloadManager.ReturnOrderPayload.Customer.StopID, ActivityKey.CreateReturnOrder);
        //                if (a != null && a.ActivityHeaderID != null && PayloadManager.ReturnOrderPayload.TransactionID == null)
        //                {
        //                    PayloadManager.ReturnOrderPayload.TransactionID = a.ActivityHeaderID;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                returnValue = 0;
        //                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][SaveOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //            }
        //            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:SaveOrder]");
        //            return returnValue;
        //        }


        public int CreateReturnOrder(ObservableCollection<ReturnItem> returnitems, string ReturnOrderID)
        {
            int returnValue = 1;

            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:CreateReturnOrder]");
            try
            {

                //string query = "select count(OrderID) from BUSDTA.ORDER_HEADER where OrderID='{0}'";
                //query = string.Format(query, ReturnOrderID);
                //bool orderExists = Convert.ToBoolean(Convert.ToInt32(DbEngine.ExecuteScalar(query)));
                bool orderExists = Convert.ToBoolean(Convert.ToInt32(DbEngine.ExecuteScalar("call BUSDTA.SP_GetCreateReturnOrderCount('" + ReturnOrderID + "')")));
                if (orderExists)
                {

                    //                    query = @"Update BUSDTA.ORDER_HEADER set 
                    //                                    TotalCoffeeAmt='{0}'
                    //                                    ,TotalAlliedAmt='{1}'
                    //                                    ,OrderTotalAmt='{2}'
                    //                                    ,SalesTaxAmt='{3}'
                    //                                    ,InvoiceTotalAmt='{4}'
                    //                                    ,EnergySurchargeAmt='{5}'
                    //                                    ,HoldCommitted='0'
                    //                                    ,UpdatedBy='{6}'
                    //                                    ,UpdatedDatetime=getdate() 
                    //                                    ,OrderStateId='{8}'
                    //                                    where OrderID='{7}';";
                    //                    query = string.Format(query, PayloadManager.ReturnOrderPayload.TotalCoffeeAmount, PayloadManager.ReturnOrderPayload.TotalAlliedAmount, PayloadManager.ReturnOrderPayload.OrderTotalAmount,
                    //                        PayloadManager.ReturnOrderPayload.ROSalesTax, PayloadManager.ReturnOrderPayload.InvoiceTotalAmt, PayloadManager.ReturnOrderPayload.EnergySurcharge,
                    //                        UserManager.UserId, ReturnOrderID, ActivityKey.CreateReturnOrder.GetStatusIdFromDB());
                    //                    DbEngine.ExecuteNonQuery(query);


                    objOrderManager.CustomerReturnsManager_CreateReturnOrderUpdate(PayloadManager.ReturnOrderPayload.TotalCoffeeAmount, PayloadManager.ReturnOrderPayload.TotalAlliedAmount, PayloadManager.ReturnOrderPayload.OrderTotalAmount,
                        PayloadManager.ReturnOrderPayload.ROSalesTax, PayloadManager.ReturnOrderPayload.InvoiceTotalAmt, PayloadManager.ReturnOrderPayload.EnergySurcharge, UserManager.UserId, ReturnOrderID);

                    Int16 ival = 0;
                    // Delete return order details records before insert - Avoid Order_Details PK Violation
                    // int delRecordTranResult = objOrderManager.CustomerReturnsManager_DeleteReturnOrderItems(Convert.ToInt32(ReturnOrderID));    //Unused Local --Vignesh D
                    objOrderManager.CustomerReturnsManager_DeleteReturnOrderItems(Convert.ToInt32(ReturnOrderID));                              //Added by Vignesh D
                    foreach (var item in returnitems)
                    {
                        ival++;
                        DataTable vOrderItemdetails = ItemManager.GetItemDetails(item.ItemId);
                        string JDEBranch = "00800";
                        var isTaxable = item.TaxAmount > 0 ? "1" : "0";

                        //decimal UnitPrice  = OrderManager.GetPriceByUomFactor(item.ItemId,vOrderItemdetails.Rows[0]["PriceUOM"],vOrderItemdetails.Rows[0]["AppliedUOM"],vOrderItemdetails.Rows[0]["UnitPriceForPricingUOM"])

                        //                        query = @"INSERT INTO BUSDTA.Order_Detail (OrderID,LineID,JDEOrderLine,CustShipToID,ItemId,LongItem,LineType,IsNonStock
                        //                                     ,SalesCat1,SalesCat2 ,SalesCat3 ,SalesCat4 ,SalesCat5 ,PurchasingCat1 ,PurchasingCat2 ,PurchasingCat3 ,PurchasingCat4 ,PurchasingCat5 ,RelatedOrderID,RelatedLineID,
                        //                                     JDEOrderNumber,JDEOrderCompany,JDEOrderType,JDEInvoiceCompany,JDEOInvoiceType,JDEZBatchNumber,
                        //                                    JDEProcessID,SettlementID,ExtendedAmtVariance,TaxAmountAmtVariance,HoldCode,
                        //                                    OrderQty,OrderQtyInPricingUM,OrderUM,PricingUM,UnitPriceAmtInPriceUoM,UnitPriceOriginalAmtInPriceUM ,UnitPriceAmt,
                        //                                    ExtnPriceAmt,ItemSalesTaxAmt,PriceOverriden,PriceOverrideReasonCodeId,IsTaxable,
                        //                                    ReturnHeldQty,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime) " +
                        //                                     " VALUES( " + ReturnOrderID + "," + ival + "," + ival + "," + PayloadManager.ReturnOrderPayload.Customer.CustomerNo + "," + item.ItemId + ",'" +
                        //                                      vOrderItemdetails.Rows[0]["LongItm"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["IBLNTY"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["IsNonStock"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat1"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat2"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat3"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat4"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat5"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat1"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat2"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat3"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat4"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat5"] + "'" +
                        //                                        ",'" + item.OrderID + "'" +
                        //                                        ",'" + ival + "'" +
                        //                                        ",'" + ReturnOrderID + "'" +
                        //                                        ",'" + JDEBranch + "'" +
                        //                                        ",'CO'" +
                        //                                        ",'" + JDEBranch + "'" +
                        //                                        ",'RM'" +
                        //                                        ",''" +
                        //                                        ",''" +
                        //                                        ",'0'" +
                        //                                        ",'0'" +
                        //                                        ",'0'" +
                        //                                        ",''," +
                        //                                        item.ReturnQty +
                        //                                        ",'" + decimal.Parse(pm.jdeUOMConversion(item.UM, item.UMPrice, int.Parse(item.ItemId)).ToString()) * item.ReturnQty + "','" +
                        //                                         item.UM + "'," +
                        //                                         "'" + item.UMPrice + "'," +
                        //                            //  "'" + (item.UnitPrice) + @"'," +
                        //                                          "'" + (item.OrderQty * decimal.Parse(pm.jdeUOMConversion(item.UM, item.UMPrice, int.Parse(item.ItemId)).ToString())) * Convert.ToDecimal(pm.loadPriceDetailsGrid(CommonNavInfo.UserBranch, CommonNavInfo.Customer.customerNo, item.ItemNumber, item.ReturnQty.ToString())) + @"'," +
                        //                                        item.UnitPrice + "," + item.UnitPrice + "," + item.ROExtendedPrice + "," + item.TaxAmount + "," +
                        //                                      " 1,5," + isTaxable + "," + item.NonSellableQty + "," + UserManager.UserId + "  ,getdate()," + UserManager.UserId + ",getdate())";
                        //                        DbEngine.ExecuteNonQuery(query);

                        decimal ldcjdeUOMConv = decimal.Parse(pm.jdeUOMConversion(item.UM, item.UMPrice, int.Parse(item.ItemId)).ToString());
                        decimal ldcloadprice = Convert.ToDecimal(pm.loadPriceDetailsGrid(CommonNavInfo.UserBranch, CommonNavInfo.Customer.customerNo, item.ItemNumber, item.ReturnQty.ToString()));

                        objOrderManager.CustomerReturnsManager_CreateReturnOrderInsert_details(ReturnOrderID, ival, PayloadManager.ReturnOrderPayload.Customer.CustomerNo, vOrderItemdetails, item, JDEBranch, ldcjdeUOMConv, ldcloadprice, isTaxable);


                        // New Logic(08/06/2016) - Update Price Details from Sales Order
                        //DataTable tblSODetails = DbEngine.ExecuteDataSet("SELECT UnitPriceAmtInPriceUoM,UnitPriceOriginalAmtInPriceUM ,UnitPriceAmt FROM BUSDTA.Order_Detail WHERE OrderID='" + item.OrderID + "' AND ItemID='" + item.ItemId + "' ").Tables[0];

                        DataTable tblSODetails = objOrderManager.CustomerReturnsManager_UpdatePriceDetails(item.OrderID, item.ItemId);

                        //StringBuilder MyQuery = new StringBuilder();
                        //MyQuery.Append(" UPDATE BUSDTA.Order_Detail SET UnitPriceOriginalAmtInPriceUM ='" + tblSODetails.Rows[0]["UnitPriceOriginalAmtInPriceUM"].ToString() + "', ");
                        //MyQuery.Append(" UnitPriceAmtInPriceUoM ='" + tblSODetails.Rows[0]["UnitPriceAmtInPriceUoM"].ToString() + "', ");
                        //MyQuery.Append(" UnitPriceAmt ='" + tblSODetails.Rows[0]["UnitPriceAmt"].ToString() + "' ");
                        //MyQuery.Append(" WHERE OrderID='" + ReturnOrderID + "' AND ItemID='" + item.ItemId + "' AND RelatedOrderID='" + item.OrderID + "'");
                        //int TranResult = DbEngine.ExecuteNonQuery(MyQuery.ToString());

                        objOrderManager.CustomerReturnsManager_CreateReturnOrderUpdate_Detail(tblSODetails.Rows[0]["UnitPriceOriginalAmtInPriceUM"].ToString(), tblSODetails.Rows[0]["UnitPriceAmtInPriceUoM"].ToString(), tblSODetails.Rows[0]["UnitPriceAmt"].ToString(), ReturnOrderID, item.ItemId, item.OrderID);

                        item.OrderDetailID = string.IsNullOrEmpty(ReturnOrderID) ? 0 : Convert.ToInt32(ReturnOrderID);
                    }

                }
                else
                {
                    // Insert the new order in Order_Header, Order_Detailsand SalesOrder_ReturnOrder_Mapping
                    // Step 1 : Save Details in Order Header
                    CustomerManager Cm = new CustomerManager();
                    DataTable dt = new DataTable();

                    dt = Cm.GetCustomerDetailsForOrder(PayloadManager.ReturnOrderPayload.Customer.CustomerNo);
                    //                    var Oh_query = @" INSERT INTO BUSDTA.ORDER_HEADER (OrderID,OriginatingRouteID,Branch,OrderTypeId,CustShipToId,CustBillToId,
                    //                                      CustParentId,OrderDate,TotalCoffeeAmt,TotalAlliedAmt," +
                    //                                    " OrderTotalAmt,SalesTaxAmt,InvoiceTotalAmt,EnergySurchargeAmt,SurchargeReasonCodeId,OrderStateId,OrderSignature,ChargeOnAccountSignature,VoidReasonCodeId,ChargeOnAccount,HoldCommitted," +
                    //                                    " JDEOrderNumber,JDEOrderCompany,JDEOrderType,JDEInvoiceCompany,JDEOInvoiceType,JDEZBatchNumber,CustomerReference1,CustomerReference2, JDEProcessID,SettlementID,VarianceAmt, " +
                    //                                    " CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime,  PaymentTerms,  AdjustmentSchedule,TaxExplanationCode,TaxArea)" +
                    //                                    " VALUES(" + ReturnOrderID + "," + CommonNavInfo.RouteID + ",'" + GetBranch(ViewModels.CommonNavInfo.UserBranch) + "'," + StatusTypesEnum.RETORD.GetStatusIdFromDB() + "," +
                    //                                    PayloadManager.ReturnOrderPayload.Customer.CustomerNo + "," + "'" + dt.Rows[0]["BillTo"] + "'," +
                    //                                    "'" + pm.GetShipToParent(Convert.ToInt64(ViewModels.CommonNavInfo.Customer.customerNo)) + "'," +
                    //                                              "getdate()," + PayloadManager.ReturnOrderPayload.TotalCoffeeAmount + "," + PayloadManager.ReturnOrderPayload.TotalAlliedAmount + "," +
                    //                                              "" + PayloadManager.ReturnOrderPayload.OrderTotalAmount + "," + PayloadManager.ReturnOrderPayload.ROSalesTax + "," + PayloadManager.ReturnOrderPayload.InvoiceTotalAmt + "" +
                    //                                              "," + PayloadManager.ReturnOrderPayload.EnergySurcharge + ",0,null,NULL,NULL,NULL,NULL,'0'," +
                    //                                              " '" + ReturnOrderID + "','00800','CO','00800','RM','','','','0','0','0', " +
                    //                                              " " + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate() " + ", '" + dt.Rows[0]["PaymentTerms"] + "', '" +
                    //                                                dt.Rows[0]["AdjustmentSchedule"] + "' ,'" + dt.Rows[0]["TaxExplanationCode"] + "','" + dt.Rows[0]["Taxarea"] + "') ";
                    //                    DbEngine.ExecuteNonQuery(Oh_query);

                    decimal ldcShipToParent = pm.GetShipToParent(Convert.ToInt64(ViewModels.CommonNavInfo.Customer.customerNo));
                    objOrderManager.CustomerReturnsManager_CreateReturnOrderInsert_Header(ReturnOrderID, PayloadManager.ReturnOrderPayload, dt, ldcShipToParent);

                    // Step 2 : Insert into Order Detail
                    Int16 ival = 0;


                    foreach (var item in returnitems)
                    {
                        ival++;
                        DataTable vOrderItemdetails = ItemManager.GetItemDetails(item.ItemId);
                        string JDEBranch = "00800";
                        var isTaxable = item.TaxAmount > 0 ? "1" : "0";

                        //decimal UnitPrice  = OrderManager.GetPriceByUomFactor(item.ItemId,vOrderItemdetails.Rows[0]["PriceUOM"],vOrderItemdetails.Rows[0]["AppliedUOM"],vOrderItemdetails.Rows[0]["UnitPriceForPricingUOM"])

                        //                        query = @"INSERT INTO BUSDTA.Order_Detail (OrderID,LineID,JDEOrderLine,CustShipToID,ItemId,LongItem,LineType,IsNonStock
                        //                                     ,SalesCat1,SalesCat2 ,SalesCat3 ,SalesCat4 ,SalesCat5 ,PurchasingCat1 ,PurchasingCat2 ,PurchasingCat3 ,PurchasingCat4 ,PurchasingCat5 ,RelatedOrderID,RelatedLineID,
                        //                                     JDEOrderNumber,JDEOrderCompany,JDEOrderType,JDEInvoiceCompany,JDEOInvoiceType,JDEZBatchNumber,
                        //                                    JDEProcessID,SettlementID,ExtendedAmtVariance,TaxAmountAmtVariance,HoldCode,
                        //                                    OrderQty,OrderQtyInPricingUM,OrderUM,PricingUM,UnitPriceAmtInPriceUoM,UnitPriceOriginalAmtInPriceUM ,UnitPriceAmt,
                        //                                    ExtnPriceAmt,ItemSalesTaxAmt,PriceOverriden,PriceOverrideReasonCodeId,IsTaxable,
                        //                                    ReturnHeldQty,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime) " +
                        //                                     " VALUES( " + ReturnOrderID + "," + ival + "," + ival + "," + PayloadManager.ReturnOrderPayload.Customer.CustomerNo + "," + item.ItemId + ",'" +
                        //                                      vOrderItemdetails.Rows[0]["LongItm"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["IBLNTY"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["IsNonStock"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat1"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat2"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat3"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat4"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat5"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat1"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat2"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat3"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat4"] + "'" +
                        //                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat5"] + "'" +
                        //                                        ",'" + item.OrderID + "'" +
                        //                                        ",'" + ival + "'" +
                        //                                        ",'" + ReturnOrderID + "'" +
                        //                                        ",'" + JDEBranch + "'" +
                        //                                        ",'CO'" +
                        //                                        ",'" + JDEBranch + "'" +
                        //                                        ",'RM'" +
                        //                                        ",''" +
                        //                                        ",''" +
                        //                                        ",'0'" +
                        //                                        ",'0'" +
                        //                                        ",'0'" +
                        //                                        ",''," +
                        //                                        item.ReturnQty +
                        //                                        ",'" + decimal.Parse(pm.jdeUOMConversion(item.UM, item.UMPrice, int.Parse(item.ItemId)).ToString()) * item.ReturnQty + "','" +
                        //                                         item.UM + "'," +
                        //                                         "'" + item.UMPrice + "'," +
                        //                            //  "'" + (item.UnitPrice) + @"'," +
                        //                                          "'" + (item.OrderQty * decimal.Parse(pm.jdeUOMConversion(item.UM, item.UMPrice, int.Parse(item.ItemId)).ToString())) * Convert.ToDecimal(pm.loadPriceDetailsGrid(CommonNavInfo.UserBranch, CommonNavInfo.Customer.customerNo, item.ItemNumber, item.ReturnQty.ToString())) + @"'," +
                        //                                        item.UnitPrice + "," + item.UnitPrice + "," + item.ROExtendedPrice + "," + item.TaxAmount + "," +
                        //                                      " 1,5," + isTaxable + "," + item.NonSellableQty + "," + UserManager.UserId + "  ,getdate()," + UserManager.UserId + ",getdate())";
                        //                        DbEngine.ExecuteNonQuery(query);

                        decimal ldcjdeUOMConv = decimal.Parse(pm.jdeUOMConversion(item.UM, item.UMPrice, int.Parse(item.ItemId)).ToString());
                        decimal ldcloadprice = Convert.ToDecimal(pm.loadPriceDetailsGrid(CommonNavInfo.UserBranch, CommonNavInfo.Customer.customerNo, item.ItemNumber, item.ReturnQty.ToString()));
                        objOrderManager.CustomerReturnsManager_CreateReturnOrderInsert_details(ReturnOrderID, ival, PayloadManager.ReturnOrderPayload.Customer.CustomerNo, vOrderItemdetails, item, JDEBranch, ldcjdeUOMConv, ldcloadprice, isTaxable);

                        //// New Logic(08/06/2016) - Update Price Details from Sales Order
                        //DataTable tblSODetails = DbEngine.ExecuteDataSet("SELECT UnitPriceAmtInPriceUoM,UnitPriceOriginalAmtInPriceUM ,UnitPriceAmt FROM BUSDTA.Order_Detail WHERE OrderID='" + item.OrderID + "' AND ItemID='" + item.ItemId + "' ").Tables[0];

                        DataTable tblSODetails = objOrderManager.CustomerReturnsManager_UpdatePriceDetails(item.OrderID, item.ItemId);

                        //StringBuilder MyQuery = new StringBuilder();
                        //MyQuery.Append(" UPDATE BUSDTA.Order_Detail SET UnitPriceOriginalAmtInPriceUM ='" + tblSODetails.Rows[0]["UnitPriceOriginalAmtInPriceUM"].ToString() + "', ");
                        //MyQuery.Append(" UnitPriceAmtInPriceUoM ='" + tblSODetails.Rows[0]["UnitPriceAmtInPriceUoM"].ToString() + "', ");
                        //MyQuery.Append(" UnitPriceAmt ='" + tblSODetails.Rows[0]["UnitPriceAmt"].ToString() + "' ");
                        //MyQuery.Append(" WHERE OrderID='" + ReturnOrderID + "' AND ItemID='" + item.ItemId + "' AND RelatedOrderID='" + item.OrderID + "'");
                        //int TranResult = DbEngine.ExecuteNonQuery(MyQuery.ToString());

                        objOrderManager.CustomerReturnsManager_CreateReturnOrderUpdate_Detail(tblSODetails.Rows[0]["UnitPriceOriginalAmtInPriceUM"].ToString(), tblSODetails.Rows[0]["UnitPriceAmtInPriceUoM"].ToString(), tblSODetails.Rows[0]["UnitPriceAmt"].ToString(), ReturnOrderID, item.ItemId, item.OrderID);

                        item.OrderDetailID = string.IsNullOrEmpty(ReturnOrderID) ? 0 : Convert.ToInt32(ReturnOrderID);
                    }
                }
                SalesLogicExpress.Domain.Activity a = LogActivity(returnitems.Clone(), PayloadManager.ReturnOrderPayload.Customer, PayloadManager.ReturnOrderPayload.Customer.StopDate.Value, PayloadManager.ReturnOrderPayload.Customer.StopID, ActivityKey.CreateReturnOrder);
                if (a != null && a.ActivityHeaderID != null && PayloadManager.ReturnOrderPayload.TransactionID == null)
                {
                    PayloadManager.ReturnOrderPayload.TransactionID = a.ActivityHeaderID;
                }
            }
            catch (Exception ex)
            {
                returnValue = 0;
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][CreateReturnOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:CreateReturnOrder]");
            return returnValue;
        }
        public bool VoidOrder(ObservableCollection<ReturnItem> returnitems, string ReturnOrderID, string VoidReasonCodeID, ActivityKey VoidAt)
        {
            bool returnValue = true;

            Logger.Info("[SalesLogicExpress.Application.Manager][CustomerReturnsManager][Start:VoidOrder]");
            try
            {
                if (VoidAt == ActivityKey.VoidAtROEntry || VoidAt == ActivityKey.VoidAtROPick)
                {
                    //int affectedRecords = DbEngine.ExecuteNonQuery(@"update busdta.ORDER_HEADER set OrderStateId = " + VoidAt.GetStatusIdFromDB() + "," +
                    //                                                        "TotalCoffeeAmt = " + PayloadManager.ReturnOrderPayload.TotalCoffeeAmount + "," +
                    //                                                        "TotalAlliedAmt = " + PayloadManager.ReturnOrderPayload.TotalAlliedAmount + "," +
                    //                                                        "OrderTotalAmt = " + PayloadManager.ReturnOrderPayload.OrderTotalAmount + "," +
                    //                                                        "SalesTaxAmt = " + PayloadManager.ReturnOrderPayload.ROSalesTax + "," +
                    //                                                        "InvoiceTotalAmt = " + PayloadManager.ReturnOrderPayload.InvoiceTotalAmt + "," +
                    //                                                        "EnergySurchargeAmt = " + (PayloadManager.ReturnOrderPayload.IsReturnOrder == true ? PayloadManager.ReturnOrderPayload.EnergySurcharge : 0) + "," +
                    //                                                                               "VoidReasonCodeId = '" + VoidReasonCodeID + "' " +
                    //                                                                               " where OrderID = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + " and OriginatingRouteID = " + CommonNavInfo.RouteID + "");

                    objOrderManager.CustomerReturnsManager_StatusVsOrder(PayloadManager.ReturnOrderPayload, VoidAt.GetStatusIdFromDB(), VoidReasonCodeID, "Void");

                    LogActivity(returnitems.Clone(), PayloadManager.ReturnOrderPayload.Customer, PayloadManager.ReturnOrderPayload.Customer.StopDate.Value, PayloadManager.ReturnOrderPayload.Customer.StopID, VoidAt);


                    foreach (var item in returnitems)
                    {
                        //   var diff = item.ReturnQty - item.NonSellableQty;   //Unused Local --Vignesh D

                        //var OD_query = @"update busdta.order_detail set " +
                        //    "OrderQty = " + item.ReturnQty + "," +
                        //    "OrderUM = '" + item.UM + "'," +
                        //    "ExtnPriceAmt = " + item.ROExtendedPrice + "," +
                        //    "IsTaxable = " + (item.ROTaxAmount > 0 ? '1' : '0') + "," +
                        //    "ReturnHeldQty = " + item.NonSellableQty + ", " +
                        //    "ItemSalesTaxAmt = " + item.TaxAmount + " " +
                        //    "where OrderID=" + ReturnOrderID + " and itemid =  " + item.ItemId + " and relatedorderid = " + item.TransactionDetailID + "";
                        //DbEngine.ExecuteNonQuery(OD_query);
                        objOrderManager.CustomerReturnsManager_VoidOrHoldOrder_Detail(item, ReturnOrderID);
                    }
                }
            }
            catch (Exception ex)
            {
                returnValue = false;
                Logger.Error("[SalesLogicExpress.Application.Manager][CustomerReturnsManager][VoidOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Manager][CustomerReturnsManager][End:VoidOrder]");
            return returnValue;
        }


        //Unused Commented for Performance    --Vignesh D
        //public string GetBranch(string branch)
        //{
        //    string BrnchNo = branch;
        //    for (int i = BrnchNo.Length; i <= 11; i++)
        //    {
        //        BrnchNo = " " + BrnchNo;
        //    }
        //    return BrnchNo;
        //}


        public bool HoldOrder(ObservableCollection<ReturnItem> returnitems, string ReturnOrderID, ActivityKey HoldAt)
        {
            bool returnValue = true;
            Logger.Info("[SalesLogicExpress.Application.Manager][CustomerReturnsManager][Start:HoldOrder]");
            try
            {
                if (HoldAt == ActivityKey.HoldAtROEntry || HoldAt == ActivityKey.HoldAtPick || HoldAt == ActivityKey.HoldAtROPrint || HoldAt == ActivityKey.HoldAtROPick)
                {
                    //                    int affectedRecords = DbEngine.ExecuteNonQuery(@"update busdta.ORDER_HEADER set 
                    //                                                                            OrderStateId = " + HoldAt.GetStatusIdFromDB() + ", " +
                    //                                                                            "TotalCoffeeAmt = " + PayloadManager.ReturnOrderPayload.TotalCoffeeAmount + "," +
                    //                                                                            "TotalAlliedAmt = " + PayloadManager.ReturnOrderPayload.TotalAlliedAmount + "," +
                    //                                                                            "OrderTotalAmt = " + PayloadManager.ReturnOrderPayload.OrderTotalAmount + "," +
                    //                                                                            "SalesTaxAmt = " + PayloadManager.ReturnOrderPayload.ROSalesTax + "," +
                    //                                                                            "InvoiceTotalAmt = " + PayloadManager.ReturnOrderPayload.InvoiceTotalAmt + ", " +
                    //                                                                            "EnergySurchargeAmt = " + (PayloadManager.ReturnOrderPayload.IsReturnOrder == true ? PayloadManager.ReturnOrderPayload.EnergySurcharge : 0) + " " +
                    //                                                                                             " where OrderID = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + " and OriginatingRouteID = " + CommonNavInfo.RouteID + "");

                    objOrderManager.CustomerReturnsManager_StatusVsOrder(PayloadManager.ReturnOrderPayload, HoldAt.GetStatusIdFromDB(), "", "Hold");
                    LogActivity(returnitems.Clone(), PayloadManager.ReturnOrderPayload.Customer, PayloadManager.ReturnOrderPayload.Customer.StopDate.Value, PayloadManager.ReturnOrderPayload.Customer.StopID, HoldAt);
                }

                foreach (var item in returnitems)
                {
                    //   var diff = item.ReturnQty - item.NonSellableQty;   //Unused Local --Vignesh D

                    //var OD_query = @"update busdta.order_detail set " +
                    //    "OrderQty = " + item.ReturnQty + "," +
                    //    "OrderUM = '" + item.UM + "'," +
                    //    "ExtnPriceAmt = " + item.ROExtendedPrice + "," +
                    //    "IsTaxable = " + (item.ROTaxAmount > 0 ? '1' : '0') + "," +
                    //    "ReturnHeldQty = " + item.NonSellableQty + ", " +
                    //    "ItemSalesTaxAmt = " + item.TaxAmount + " " +
                    //    "where OrderID=" + ReturnOrderID + " and itemid =  " + item.ItemId + " and relatedorderid = " + item.TransactionDetailID + "";
                    //DbEngine.ExecuteNonQuery(OD_query);
                    objOrderManager.CustomerReturnsManager_VoidOrHoldOrder_Detail(item, ReturnOrderID);
                }
            }
            catch (Exception ex)
            {
                returnValue = false;
                Logger.Error("[SalesLogicExpress.Application.Manager][CustomerReturnsManager][HoldOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Manager][CustomerReturnsManager][End:HoldOrder]");
            return returnValue;
        }

        public bool SaveReturnOrder(ObservableCollection<ReturnItem> returnitems, string ReturnOrderID)
        {
            bool returnValue = true;

            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:SaveReturnOrder]");
            try
            {
                //var orderStateIdUpdate = PayloadManager.ReturnOrderPayload.IsReturnOrder ? "OrderStateId = " + ActivityKey.OrderReturned.GetStatusIdFromDB() : "OrderStateId = " + ActivityKey.ItemReturn.GetStatusIdFromDB();
                var orderStateIdUpdate = PayloadManager.ReturnOrderPayload.IsReturnOrder ? ActivityKey.OrderReturned.GetStatusIdFromDB() : ActivityKey.ItemReturn.GetStatusIdFromDB();

                decimal EnergyCharge = PayloadManager.ReturnOrderPayload.EnergySurcharge;
                if (!PayloadManager.ReturnOrderPayload.IsReturnOrder)
                    EnergyCharge = 0;

                //var OH_query = @"update busdta.ORDER_HEADER set  OrderStateId =" + orderStateIdUpdate + "," +
                //                  "TotalCoffeeAmt = " + PayloadManager.ReturnOrderPayload.TotalCoffeeAmount + "," +
                //                  "TotalAlliedAmt = " + PayloadManager.ReturnOrderPayload.TotalAlliedAmount + "," +
                //                  "OrderTotalAmt = " + PayloadManager.ReturnOrderPayload.OrderTotalAmount + "," +
                //                  "SalesTaxAmt = " + PayloadManager.ReturnOrderPayload.ROSalesTax + "," +
                //                  "InvoiceTotalAmt = " + PayloadManager.ReturnOrderPayload.InvoiceTotalAmt + "," +
                //                  "EnergySurchargeAmt = " + EnergyCharge + "" +
                //                 " where OrderID = " + ReturnOrderID + " and OriginatingRouteID = " + CommonNavInfo.RouteID + "";
                //int affectedRecords = DbEngine.ExecuteNonQuery(OH_query);

                objOrderManager.CustomerReturnsManager_StatusVsOrder(PayloadManager.ReturnOrderPayload, Convert.ToInt32(orderStateIdUpdate), "", "Save", EnergyCharge, ReturnOrderID);

                LogActivity(returnitems.Clone(), PayloadManager.ReturnOrderPayload.Customer, PayloadManager.ReturnOrderPayload.Customer.StopDate.Value, PayloadManager.ReturnOrderPayload.Customer.StopID, ActivityKey.OrderReturned);

                foreach (var item in returnitems)
                {
                    //  var diff = item.ReturnQty - item.NonSellableQty;    // UNused Local Vignesh D
                    //var invUpdateQuery = @"update busdta.Inventory set OnHandQuantity = OnHandQuantity +" + item.ReturnQtyInPrimaryUoM + " where ItemId = " + item.ItemId.Trim() + " and RouteId = " + CommonNavInfo.RouteID;

                    //DbEngine.ExecuteNonQuery(invUpdateQuery);

                    //var invUpdateQueryHeld = @"update busdta.Inventory set HeldQuantity = HeldQuantity +" + item.NonSellableQtyInPrimaryUoM + " where ItemId = " + item.ItemId.Trim() + " and RouteId = " + CommonNavInfo.RouteID;
                    //DbEngine.ExecuteNonQuery(invUpdateQueryHeld);

                    //var OD_query = @"update busdta.order_detail set " +
                    //    "OrderQty = " + item.ReturnQty + "," +
                    //    "OrderUM = '" + item.UM + "'," +
                    //    "ExtnPriceAmt = " + item.ROExtendedPrice + "," +
                    //    "IsTaxable = " + (item.ROTaxAmount > 0 ? '1' : '0') + "," +
                    //    "ReturnHeldQty = " + item.NonSellableQty + ", " +
                    //    "ItemSalesTaxAmt = " + item.TaxAmount +
                    //    ",OrderQtyInPricingUM ='" + decimal.Parse(pm.jdeUOMConversion(item.UM, item.UMPrice, int.Parse(item.ItemId)).ToString()) * item.ReturnQty + "' " +
                    //    //",UnitPriceAmtInPriceUoM ='" + (item.ReturnQty * decimal.Parse(pm.jdeUOMConversion(item.UM, item.UMPrice, int.Parse(item.ItemId)).ToString())) * Convert.ToDecimal(pm.loadPriceDetailsGrid(CommonNavInfo.UserBranch, CommonNavInfo.Customer.customerNo, item.ItemNumber, item.ReturnQty.ToString())) + "' " +
                    //    "where ItemID =" + item.ItemId + " and OrderID=" + ReturnOrderID + " and RelatedOrderID ='" + item.TransactionDetailID + "'";
                    //DbEngine.ExecuteNonQuery(OD_query);

                    decimal ldcUOMConv = decimal.Parse(pm.jdeUOMConversion(item.UM, item.UMPrice, int.Parse(item.ItemId)).ToString());
                    objOrderManager.CustomerReturnsManager_SaveReturnOrder(item, ReturnOrderID, ldcUOMConv);

                }
                if (PayloadManager.ReturnOrderPayload.IsReturnOrder)
                {
                    //var chngQuery = @"update busdta.ORDER_HEADER set OrderStateId = " + ActivityKey.OrderReturned.GetStatusIdFromDB() + "" +
                    //           " where OrderID = " + PayloadManager.ReturnOrderPayload.SalesOrderID + " and OriginatingRouteID = " + CommonNavInfo.RouteID + "";
                    //int result = DbEngine.ExecuteNonQuery(chngQuery);

                    objOrderManager.CustomerReturnsManager_SaveReturnOrder_Header(ActivityKey.OrderReturned.GetStatusIdFromDB(), PayloadManager.ReturnOrderPayload.SalesOrderID);

                }
            }
            catch (Exception ex)
            {
                returnValue = false;
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][SaveReturnOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:SaveReturnOrder]");
            return returnValue;
        }

        /// <summary>
        /// Unused function
        /// 10-13-2016
        /// </summary>
        /// <param name="returnitems"></param>
        /// <param name="ReturnOrderID"></param>
        /// <param name="activity"></param>
        /// <returns></returns>
        /// 

        // Unused function commented for performance --Vignesh D
        //public bool SaveOrHoldReturnOrder(ObservableCollection<ReturnItem> returnitems, string ReturnOrderID, ActivityKey activity)
        //{
        //    bool status = true;
        //    Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:SaveOrHoldReturnOrder]");
        //    InventoryManager objInvMgr = new InventoryManager();
        //    try
        //    {
        //        int orderStateID = 0;
        //        switch (activity)
        //        {
        //            case ActivityKey.OrderReturned:
        //                orderStateID = ActivityKey.OrderReturned.GetStatusIdFromDB();
        //                LogActivity(returnitems.Clone(), PayloadManager.ReturnOrderPayload.Customer, PayloadManager.ReturnOrderPayload.Customer.StopDate.Value, PayloadManager.ReturnOrderPayload.Customer.StopID, ActivityKey.OrderReturned);
        //                break;
        //            case ActivityKey.HoldAtROEntry:
        //                orderStateID = ActivityKey.HoldAtROEntry.GetStatusIdFromDB();
        //                LogActivity(returnitems.Clone(), PayloadManager.ReturnOrderPayload.Customer, PayloadManager.ReturnOrderPayload.Customer.StopDate.Value, PayloadManager.ReturnOrderPayload.Customer.StopID, ActivityKey.HoldAtROEntry);

        //                break;
        //            case ActivityKey.HoldAtROPrint:
        //                orderStateID = ActivityKey.HoldAtROPrint.GetStatusIdFromDB();
        //                LogActivity(returnitems.Clone(), PayloadManager.ReturnOrderPayload.Customer, PayloadManager.ReturnOrderPayload.Customer.StopDate.Value, PayloadManager.ReturnOrderPayload.Customer.StopID, ActivityKey.HoldAtROPrint);

        //                break;
        //            case ActivityKey.HoldAtPick:
        //                orderStateID = ActivityKey.HoldAtPick.GetStatusIdFromDB();
        //                LogActivity(returnitems.Clone(), PayloadManager.ReturnOrderPayload.Customer, PayloadManager.ReturnOrderPayload.Customer.StopDate.Value, PayloadManager.ReturnOrderPayload.Customer.StopID, ActivityKey.HoldAtPick);

        //                break;
        //            default:
        //                orderStateID = -1;
        //                break;
        //        }
        //        if (string.IsNullOrEmpty(ReturnOrderID))
        //        {
        //            throw new Exception("ReturnOrderID cannot be null or empty");
        //        }
        //        // POSSIBLE CASES
        //        // 1. Return Complete 
        //        // 2. Hold at
        //        //      Order Entry
        //        //      Pick
        //        //      Print
        //        // 3. Void
        //        var orderState = DbEngine.ExecuteScalar(@"select OrderStateId from BUSDTA.ORDER_HEADER where OrderID = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + " and RouteID = " + CommonNavInfo.RouteID + "").Trim();
        //        if (orderStateID == ActivityKey.HoldAtROEntry.GetStatusIdFromDB() ||
        //            orderStateID == ActivityKey.HoldAtPick.GetStatusIdFromDB() ||
        //            orderStateID == ActivityKey.HoldAtROPrint.GetStatusIdFromDB())
        //        {
        //            var updateOh_state = DbEngine.ExecuteNonQuery(@"update busdta.ORDER_HEADER set OrderStateId = " + activity.GetStatusIdFromDB() + " where OrderID = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + " and RouteID = " + CommonNavInfo.RouteID + "");
        //            ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
        //        }
        //        else
        //        {

        //            // TODO : Order signature is remaining
        //            // Step 1 : Save Details in Order Header
        //            //var Oh_query = @" INSERT INTO BUSDTA.ORDER_HEADER (OrderID,RouteID,OrderTypeId,CustomerId,OrderDate,TotalCoffeeAmt,TotalAlliedAmt," +
        //            //                " OrderTotalAmt,SalesTaxAmt,InvoiceTotalAmt,EnergySurchargeAmt,SurchargeReasonCodeId,OrderStateId,OrderSign,ChargeOnAccountSign,VoidReasonCodeId,ChargeOnAccount," +
        //            //                " HoldCommitted,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime)" +
        //            //                " VALUES(" + ReturnOrderID + "," + CommonNavInfo.RouteID + "," + StatusTypesEnum.RETORD.GetStatusIdFromDB() + "," + PayloadManager.ReturnOrderPayload.Customer.CustomerNo + "," +
        //            //                          "getdate()," + PayloadManager.ReturnOrderPayload.TotalCoffeeAmount + "," + PayloadManager.ReturnOrderPayload.TotalAlliedAmount + "," +
        //            //                          "" + PayloadManager.ReturnOrderPayload.OrderTotalAmount + "," + PayloadManager.ReturnOrderPayload.ROSalesTax + "," + PayloadManager.ReturnOrderPayload.InvoiceTotalAmt + "" +
        //            //                          "," + PayloadManager.ReturnOrderPayload.EnergySurcharge + ",null," + orderStateID + ",NULL,NULL,NULL,NULL,'0'," +
        //            //                          "" + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate()) ";

        //            //DbEngine.ExecuteNonQuery(Oh_query);

        //            foreach (var item in returnitems)
        //            {
        //                //// Step 2 : Insert into Order Detail
        //                //var isTaxable = item.TaxAmount > 0 ? "1" : "0";
        //                //var query = @"INSERT INTO BUSDTA.Order_Detail (OrderID,RouteID,ItemId,OrderQty,OrderUM,UnitPriceAmt,ExtnPriceAmt,ItemSalesTaxAmt,PriceOverrideReasonCodeId,IsTaxable,ReturnHeldQty,CreatedDatetime,UpdatedBy,UpdatedDatetime) " +
        //                //             " VALUES( " + ReturnOrderID + "," + CommonNavInfo.RouteID + "," + item.ItemId + "," + item.ReturnQty + ",'" + item.UM + "'," + item.UnitPrice + "," + item.ExtendedPrice + "," + item.TaxAmount + "," +
        //                //             " NULL," + isTaxable + "," + item.NonSellableQty + ",getdate()," + UserManager.UserId + ",getdate())";
        //                //DbEngine.ExecuteNonQuery(query);
        //                //string OrderDetailId = DbEngine.ExecuteScalar("select @@identity").ToString();
        //                //// Step 3 : Insert into SalesOrder_ReturnOrder Mapping table
        //                //string returnOrderMappingQuery = "insert into busdta.SalesOrder_ReturnOrder_Mapping (SalesOrderId,ReturnOrderID,ItemID,OrderDetailID,RouteID,CreatedBy,CreatedDatetime) values({0},{1},{2},{3},{4},{5},getdate())";
        //                //returnOrderMappingQuery = string.Format(returnOrderMappingQuery, item.OrderID, ReturnOrderID, item.ItemId, OrderDetailId, CommonNavInfo.RouteID, PayloadManager.ApplicationPayload.LoggedInUserID);
        //                //DbEngine.ExecuteNonQuery(returnOrderMappingQuery);

        //                // Commented Start - 09/14/2016 - Call Common Function
        //                // Step 4 : Update the Inventory
        //                //var invUpdateQuery = @"update busdta.Inventory set OnHandQuantity = OnHandQuantity +" + item.ReturnQty + " where ItemId = " + item.ItemId.Trim() + " and RouteId = " + CommonNavInfo.RouteID;
        //                //DbEngine.ExecuteNonQuery(invUpdateQuery);

        //                //var invUpdateQueryHeld = @"update busdta.Inventory set HeldQuantity = HeldQuantity +" + item.NonSellableQty + " where ItemId = " + item.ItemId.Trim() + " and RouteId = " + CommonNavInfo.RouteID;
        //                //DbEngine.ExecuteNonQuery(invUpdateQueryHeld);
        //                // Commented End

        //                // Call Common Function
        //                objInvMgr.UpdateReturnInventoryForSaveOrHold(item.ReturnQty, item.NonSellableQty, item.ItemId.Trim(), CommonNavInfo.RouteID);

        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        status = false;
        //        Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][SaveOrHoldReturnOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:SaveOrHoldReturnOrder]");
        //    return status;
        //}




        //  Unused Function Commented for Performance   --Vignesh D
        //public bool HoldReturnOrder()
        //{
        //    bool status = true;
        //    Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:HoldReturnOrder]");
        //    try
        //    {

        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][HoldReturnOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:HoldReturnOrder]");
        //    return status;
        //}
        /// <summary>
        /// Unused function
        /// 10-13-2016
        /// </summary>
        /// <param name="activity"></param>
        /// <param name="ReturnOrderID"></param>
        /// <param name="returnitems"></param>
        /// <returns></returns>
        /// 

        //Unused function commented for performance   --Vignesh D
        //public bool VoidReturnOrder(ActivityKey activity, int ReturnOrderID, ObservableCollection<ReturnItem> returnitems)
        //{
        //    bool status = true;
        //    Logger.Info("[SalesLogicExpress.Application.Manager][CustomerReturnsManager][Start:VoidReturnOrder]");
        //    try
        //    {
        //        var oh_count = DbEngine.ExecuteScalar("select count(1) from busdta.order_header where OrderID =" + ReturnOrderID + "");

        //        if (!string.IsNullOrEmpty(oh_count) && Convert.ToInt32(oh_count) > 0)
        //        {
        //            //var updateOh_state = DbEngine.ExecuteNonQuery(@"update busdta.ORDER_HEADER set OrderStateId = " + activity.GetStatusIdFromDB() + " where OrderID = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + " and RouteID = " + CommonNavInfo.RouteID + "");

        //            objOrderManager.CustomerReturnsManager_SaveOrHoldReturnOrder_Update(PayloadManager.ReturnOrderPayload.ReturnOrderID, activity.GetStatusIdFromDB());

        //            foreach (var r_item in returnitems)
        //            {
        //                //                        var r_query = @"update busdta.order_detail set OrderQty = 0 , ReturnHeldQty = 0 where OrderDetailID = '{0}' and OrderID = '{1}' 
        //                //                                        and RouteID = '{2}'";
        //                //                        var query = string.Format(r_query, r_item.OrderDetailID, ReturnOrderID, CommonNavInfo.RouteID);
        //                //                        DbEngine.ExecuteDataSet(query);

        //                objOrderManager.CustomerReturnsManager_VoidReturnOrder_UpdateDetail(ReturnOrderID, r_item.OrderDetailID);
        //            }
        //        }
        //        else
        //        {

        //            //var Oh_query = @" INSERT INTO BUSDTA.ORDER_HEADER (OrderID,RouteID,OrderTypeId,CustomerId,OrderDate,TotalCoffeeAmt,TotalAlliedAmt," +
        //            //                   " OrderTotalAmt,SalesTaxAmt,InvoiceTotalAmt,EnergySurchargeAmt,SurchargeReasonCodeId,OrderStateId,OrderSign,ChargeOnAccountSign,VoidReasonCodeId,ChargeOnAccount," +
        //            //                   " HoldCommitted,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime)" +
        //            //                   " VALUES(" + ReturnOrderID + "," + CommonNavInfo.RouteID + "," + StatusTypesEnum.RETORD.GetStatusIdFromDB() + "," + PayloadManager.ReturnOrderPayload.Customer.CustomerNo + "," +
        //            //                             "getdate()," + PayloadManager.ReturnOrderPayload.TotalCoffeeAmount + "," + PayloadManager.ReturnOrderPayload.TotalAlliedAmount + "," +
        //            //                             "" + PayloadManager.ReturnOrderPayload.OrderTotalAmount + "," + PayloadManager.ReturnOrderPayload.ROSalesTax + "," + PayloadManager.ReturnOrderPayload.InvoiceTotalAmt + "" +
        //            //                             "," + PayloadManager.ReturnOrderPayload.EnergySurcharge + ",null," + activity.GetStatusIdFromDB() + ",NULL,NULL,NULL,NULL,'0'," +
        //            //                             "" + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate()) ";

        //            //DbEngine.ExecuteNonQuery(Oh_query);

        //            objOrderManager.CustomerReturnsManager_VoidReturnOrder_InsertHeader(ReturnOrderID, StatusTypesEnum.RETORD.GetStatusIdFromDB(), PayloadManager.ReturnOrderPayload, activity.GetStatusIdFromDB());


        //            foreach (var item in returnitems)
        //            {
        //                // Step 2 : Insert into Order Detail
        //                var isTaxable = item.TaxAmount > 0 ? "1" : "0";
        //                //var query = @"INSERT INTO BUSDTA.Order_Detail (OrderID,RouteID,ItemId,OrderQty,OrderUM,UnitPriceAmt,ExtnPriceAmt,ItemSalesTaxAmt,PriceOverrideReasonCodeId,IsTaxable,ReturnHeldQty,CreatedDatetime,UpdatedBy,UpdatedDatetime) " +
        //                //             " VALUES( " + ReturnOrderID + "," + CommonNavInfo.RouteID + "," + item.ItemId + "," + item.ReturnQty + ",'" + item.UM + "'," + item.UnitPrice + "," + item.ExtendedPrice + "," + item.TaxAmount + "," +
        //                //             " NULL," + isTaxable + "," + item.NonSellableQty + ",getdate()," + UserManager.UserId + ",getdate())";
        //                //DbEngine.ExecuteNonQuery(query);

        //                objOrderManager.CustomerReturnsManager_VoidReturnOrder_InsertDetail(ReturnOrderID, isTaxable, item);

        //                string OrderDetailId = DbEngine.ExecuteScalar("select @@identity").ToString();

        //                // Commented For: SLER1-44 Sales_ReturnOrder_Mapping - Logic Change

        //                // Step 3 : Insert into SalesOrder_ReturnOrder Mapping table
        //                //string returnOrderMappingQuery = "insert into busdta.SalesOrder_ReturnOrder_Mapping (SalesOrderId,ReturnOrderID,ItemID,OrderDetailID,RouteID,CreatedBy,CreatedDatetime) values({0},{1},{2},{3},{4},{5},getdate())";
        //                //returnOrderMappingQuery = string.Format(returnOrderMappingQuery, item.OrderID, ReturnOrderID, item.ItemId, OrderDetailId, CommonNavInfo.RouteID, PayloadManager.ApplicationPayload.LoggedInUserID);
        //                //DbEngine.ExecuteNonQuery(returnOrderMappingQuery);


        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Manager][CustomerReturnsManager][VoidReturnOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Manager][CustomerReturnsManager][End:VoidReturnOrder]");
        //    return status;
        //}

        public int GetRONumber()
        {
            int OrderNum = 0;
            //   int updateStopActivityFlag = 0;    //Unused Local --Vignesh D
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:GetRONumber]");
            try
            {
                //TODO : Implement number pool for return order
                string val = new Managers.NumberManager().GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.ReturnOrder);
                OrderNum = Convert.ToInt32(val);

                if (CommonNavInfo.Customer.StopID != null)
                {
                    // Comment On - 09/16/2016
                    //updateStopActivityFlag = DbEngine.ExecuteNonQuery("update busdta.m56m0004 set rpactid=null where rpstid = " + CommonNavInfo.Customer.StopID + " and rpan8=" + CommonNavInfo.Customer.CustomerNo + "");
                    // Commnet End

                    // Call Common Function
                    StopManager objStopMgr = new StopManager();
                    //  updateStopActivityFlag = objStopMgr.UpdateStopActivity(CommonNavInfo.Customer.StopID, CommonNavInfo.Customer.CustomerNo);   //Unused Local --Vignesh D
                    objStopMgr.UpdateStopActivity(CommonNavInfo.Customer.StopID, CommonNavInfo.Customer.CustomerNo);
                }
                return OrderNum;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][GetRONumber][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                OrderNum = -1;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:GetRONumber]");
            return OrderNum;
        }

        private SalesLogicExpress.Domain.Activity LogActivity(ObservableCollection<ReturnItem> returnItems, Customer customer, DateTime stopDate, string stopID, ActivityKey activity)
        {
            Activity ac = new Activity();
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:LogActivity]");
            try
            {
                PayloadManager.ReturnOrderPayload.StopDate = customer.StopDate.Value;
                PayloadManager.ReturnOrderPayload.Customer = customer;
                TrulyObservableCollection<ReturnItem> oc = new TrulyObservableCollection<ReturnItem>(returnItems.ToList());
                PayloadManager.ReturnOrderPayload.ReturnItems = oc;
                ac = new Activity
                {
                    ActivityFlowID = PayloadManager.ReturnOrderPayload.ReturnOrderID,
                    ActivityHeaderID = PayloadManager.ReturnOrderPayload.TransactionID,
                    CustomerID = PayloadManager.ReturnOrderPayload.Customer.CustomerNo,
                    RouteID = PayloadManager.ApplicationPayload.Route,
                    ActivityType = activity.ToString(),
                    ActivityStart = DateTime.Now,
                    ActivityEnd = DateTime.Now,
                    StopInstanceID = stopID,
                    IsTxActivity = true,
                    ActivityDetailClass = PayloadManager.ReturnOrderPayload.ToString(),
                    ActivityDetails = PayloadManager.ReturnOrderPayload.SerializeToJson()
                };
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][LogActivity][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:LogActivity]");
            return ResourceManager.Transaction.LogActivity(ac);
        }

        public TrulyObservableCollection<ReturnItem> GetOrdersForOrderReturn(string OrderReturnId)
        {
            TrulyObservableCollection<ReturnItem> returnItems = new TrulyObservableCollection<ReturnItem>();
            DataTable dbResult = new DataTable();
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:GetOrdersForOrderReturn]");
            try
            {
                //                var query = "";
                //                query = @"
                //                        SELECT DISTINCT 
                //                        S1.OrderID
                //                        ,S2.OrderID as ReturnOrderID
                //                        ,S2.ItemId
                //                        ,im.IMLITM as ItemNumber
                //                        ,im.IMDSC1 as ItemDescription
                //                        ,im.IMLNTY as StkType
                //                        ,im.IMSRP1  as SalesCat1
                //                        ,im.IMSRP5 as SalesCat5
                //                        ,im.IMSRP4 as SalesCat4
                //                        ,ISNULL(S2.ORDERQTY,0) AS PreviouslyReturnedQty
                //                        ,S2.OrderUM as OrderUM
                //                        ,S1.OrderQty
                //                        ,S2.RouteId
                //                        ,S2.UnitPriceAmt
                //                        ,S2.ExtnPriceAmt
                //                        ,S2.ItemSalesTaxAmt
                //                        ,S2.IsTaxable
                //                        ,S1.ORDERQTY- ISNULL(S2.ORDERQTY,0) AS AvailableReturnQty
                //                        ,inv.OnHandQuantity as OnHandQty
                //                        ,inv.CommittedQuantity as CommittedQty
                //                        ,inv.HeldQuantity as HeldQty
                //                        ,(inv.OnHandQuantity - (inv.CommittedQuantity + inv.HeldQuantity)) as AvailableQty 
                //                        ,iconfig.PrimaryUM as PrimaryUM
                //                        ,iconfig.PricingUM
                //                        ,iconfig.TransactionUM as DefaultUM
                //                        ,iconfig.OtherUM1 as OtherUM1
                //                        ,iconfig.OtherUM2
                //                        ,S2.OrderDate, S2.DocumentId
                //                        FROM
                //                        (
                //                        SELECT  OH.*, OD.OrderUM, OD.ITEMID,OD.ORDERQTY,OD.RETURNHELDQTY,OD.UnitPriceAmt,OD.ExtnPriceAmt,OD.ItemSalesTaxAmt, isnull(od.IsTaxable,0) as IsTaxable FROM BUSDTA.ORDER_HEADER OH LEFT OUTER JOIN busdta.ORDER_Detail OD ON OH.ORDERID=OD.ORDERID
                //                        WHERE OH.OrderTypeId=" + StatusTypesEnum.SALORD.GetStatusIdFromDB() + "" +
                //                        @") S1
                //                        right OUTER JOIN
                //                        (
                //                        SELECT OD.*,OD.RelatedOrderID AS RORDERDETAILID,OH.DocumentId, OH.OrderDate,OH.OriginatingRouteID RouteID FROM BUSDTA.ORDER_HEADER OH LEFT OUTER JOIN busdta.ORDER_Detail OD ON OH.ORDERID=OD.ORDERID
                ////                        LEFT OUTER JOIN busdta.SalesOrder_ReturnOrder_Mapping SRM ON  OD.ORDERID=SRM.RETURNORDERID
                //                        WHERE OH.OrderTypeId=" + StatusTypesEnum.RETORD.GetStatusIdFromDB() + " AND ISNULL(RORDERDETAILID,0) <> 0" +
                //                       @" ) S2 ON S1.ORDERID= S2.RORDERDETAILID AND S1.ITEMID=S2.ITEMID
                //                         join busdta.F4101 im on S1.ItemId = im.IMITM 
                //                         join busdta.F4102 ib on ib.IBLITM = im.IMLITM 
                //                         join busdta.ItemConfiguration iconfig on S1.ItemId = iconfig.ItemID
                //                         join busdta.Inventory inv on inv.ItemId = S1.ItemId
                //                        where S1.CustShipToID='{0}' and ReturnOrderID={1}
                //                        ";
                //                query = string.Format(query, PayloadManager.ReturnOrderPayload.Customer.CustomerNo, OrderReturnId);
                var query = "call BUSDTA.SP_GetOrdersForOrderReturn (" + StatusTypesEnum.SALORD.GetStatusIdFromDB() + "," + StatusTypesEnum.RETORD.GetStatusIdFromDB() + ",'" + PayloadManager.ReturnOrderPayload.Customer.CustomerNo + "'," + OrderReturnId + ")";
                dbResult = DbEngine.ExecuteDataSet(query).Tables[0];
                if (dbResult.Rows.Count > 0)
                {
                    foreach (DataRow datarow in dbResult.Rows)
                    {
                        ReturnItem returnItem = new ReturnItem
                        {
                            DocumentId = string.IsNullOrEmpty(datarow["DocumentId"].ToString()) ? 0 : Convert.ToDecimal(datarow["DocumentId"].ToString()),
                            ItemId = string.IsNullOrEmpty(datarow["ItemId"].ToString()) ? string.Empty : datarow["ItemId"].ToString().Trim(),
                            ItemNumber = string.IsNullOrEmpty(datarow["ItemNumber"].ToString()) ? string.Empty : datarow["ItemNumber"].ToString().Trim(),
                            ItemDescription = string.IsNullOrEmpty(datarow["ItemDescription"].ToString()) ? string.Empty : datarow["ItemDescription"].ToString().Trim(),
                            StkType = string.IsNullOrEmpty(datarow["StkType"].ToString()) ? string.Empty : datarow["StkType"].ToString().Trim(),
                            SalesCat1 = string.IsNullOrEmpty(datarow["SalesCat1"].ToString()) ? string.Empty : datarow["SalesCat1"].ToString().Trim(),
                            SalesCat5 = string.IsNullOrEmpty(datarow["SalesCat5"].ToString()) ? string.Empty : datarow["SalesCat5"].ToString().Trim(),
                            SalesCat4 = string.IsNullOrEmpty(datarow["SalesCat4"].ToString()) ? string.Empty : datarow["SalesCat4"].ToString().Trim(),
                            PreviouslyReturnedQty = string.IsNullOrEmpty(datarow["PreviouslyReturnedQty"].ToString()) ? 0 : Convert.ToInt32(datarow["PreviouslyReturnedQty"].ToString().Trim()),
                            UM = string.IsNullOrEmpty(datarow["OrderUM"].ToString()) ? string.Empty : datarow["OrderUM"].ToString().Trim(),
                            OrderQty = string.IsNullOrEmpty(datarow["OrderQty"].ToString()) ? 0 : Convert.ToInt32(datarow["OrderQty"].ToString().Trim()),
                            RouteId = string.IsNullOrEmpty(datarow["RouteId"].ToString()) ? 0 : Convert.ToInt32(datarow["RouteId"].ToString().Trim()),
                            UnitPrice = string.IsNullOrEmpty(datarow["UnitPriceAmt"].ToString()) ? 0 : Convert.ToDecimal(datarow["UnitPriceAmt"].ToString().Trim()),
                            ExtendedPrice = string.IsNullOrEmpty(datarow["ExtnPriceAmt"].ToString()) ? 0 : Convert.ToDecimal(datarow["ExtnPriceAmt"].ToString().Trim()),
                            TaxAmount = string.IsNullOrEmpty(datarow["ItemSalesTaxAmt"].ToString()) ? 0 : Convert.ToDecimal(datarow["ItemSalesTaxAmt"].ToString().Trim()),
                            IsTaxable = datarow["IsTaxable"].ToString() == "0" ? false : true,
                            UMPrice = string.IsNullOrEmpty(datarow["PricingUM"].ToString()) ? string.Empty : datarow["PricingUM"].ToString().Trim(),
                            TransactionUOM = string.IsNullOrEmpty(datarow["DefaultUM"].ToString()) ? string.Empty : datarow["DefaultUM"].ToString().Trim(),
                            OrderDateF = string.IsNullOrEmpty(datarow["OrderDate"].ToString()) ? new DateTime() : Convert.ToDateTime(datarow["OrderDate"].ToString().Trim()),
                            OrderID = string.IsNullOrEmpty(datarow["OrderID"].ToString()) ? 0 : Convert.ToInt32(datarow["OrderID"].ToString().Trim()),
                            QtyOnHand = string.IsNullOrEmpty(datarow["OnHandQty"].ToString()) ? 0 : Convert.ToInt32(datarow["OnHandQty"].ToString().Trim()),
                            HeldQty = string.IsNullOrEmpty(datarow["HeldQty"].ToString()) ? 0 : Convert.ToInt32(datarow["HeldQty"].ToString().Trim()),
                            CommittedQty = string.IsNullOrEmpty(datarow["CommittedQty"].ToString()) ? 0 : Convert.ToInt32(datarow["CommittedQty"].ToString().Trim()),
                            AvailableQty = string.IsNullOrEmpty(datarow["AvailableQty"].ToString()) ? 0 : Convert.ToInt32(datarow["AvailableQty"].ToString().Trim()),
                            PrimaryUM = string.IsNullOrEmpty(datarow["PrimaryUM"].ToString()) ? string.Empty : datarow["PrimaryUM"].ToString().Trim(),
                            QtyAvailableToReturn = string.IsNullOrEmpty(datarow["AvailableReturnQty"].ToString()) ? 0 : Convert.ToInt32(datarow["AvailableReturnQty"].ToString().Trim()),
                            ReturnOrderID = string.IsNullOrEmpty(datarow["ReturnOrderID"].ToString()) ? 0 : Convert.ToInt32(datarow["ReturnOrderID"].ToString().Trim()),
                            CustomerPo = string.IsNullOrEmpty(datarow["CustomerPo"].ToString()) ? "" : datarow["CustomerPo"].ToString().Trim()
                        };
                        
                        returnItem.OrderDate = returnItem.OrderDateF.ToString("MM'/'dd'/'yyyy");
                        returnItems.Add(returnItem);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][GetOrdersForOrderReturn][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:GetOrdersForOrderReturn]");
            return returnItems;
        }


        //Unused function commented for performance   --Vignesh D
        //public static void UpdateOrderStatus(string orderNo, ActivityKey state, bool UpdateTable = true)
        //{
        //    CustomerReturnsManager CustLog = new CustomerReturnsManager();
        //    CustLog.Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:UpdateOrderStatus]");
        //    try
        //    {
        //        if (UpdateTable)
        //        {
        //            //string query = "update busdta.ORDER_HEADER set OrderStateId={0}, UpdatedDatetime=now() where OrderID='{1}' and isnull(OrderStateId,'')!='" + ActivityKey.OrderReturned.GetStatusIdFromDB() + "'";
        //            //query = string.Format(query, state.GetStatusIdFromDB(), orderNo);
        //            //DbEngine.ExecuteNonQuery(query);
        //            CustomerReturnsManager objCust = new CustomerReturnsManager();
        //            objCust.objOrderManager.CustomerReturnsManager_UpdateOrderStatus(ActivityKey.OrderReturned.GetStatusIdFromDB(), state.GetStatusIdFromDB(), orderNo);
        //            //SalesLogicExpress.Application.ViewModels.Order.OrderStatus = state;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        CustLog.Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][UpdateOrderStatus][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    CustLog.Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:UpdateOrderStatus]");
        //}

        public int CheckROExists()
        {
            int result = 0;
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:CheckROExists]");
                //var q_result = DbEngine.ExecuteScalar(@"select count(1) from busdta.Order_header where ordertypeid = " + StatusTypesEnum.RETORD.GetStatusIdFromDB() + " and orderid = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + "");
                var q_result = DbEngine.ExecuteScalar("call busdta.SP_GetCheckROExists(" + StatusTypesEnum.RETORD.GetStatusIdFromDB() + "," + PayloadManager.ReturnOrderPayload.ReturnOrderID + ")");
                result = string.IsNullOrEmpty(q_result.ToString().Trim()) ? -1 : Convert.ToInt32(q_result.Trim());
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:CheckROExists]");

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][CheckROExists][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }


        //Unused function commented for performance  -- Vignesh D
        //public int CheckROItems()
        //{
        //    int result = 0;
        //    try
        //    {
        //        Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:CheckROItems]");
        //        //var q_result = DbEngine.ExecuteScalar(@"select count(1) from busdta.Order_detail where orderid = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + "");

        //        var q_result = objOrderManager.CustomerReturnsManager_CheckROItems(PayloadManager.ReturnOrderPayload.ReturnOrderID);
        //        result = string.IsNullOrEmpty(q_result.ToString().Trim()) ? -1 : Convert.ToInt32(q_result.Trim());
        //        Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:CheckROItems]");

        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][CheckROItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    return result;
        //}


        //Unused function commented for performance --Vignesh D
        //public bool CleanROItems()
        //{
        //    bool result = false;
        //    try
        //    {
        //        Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:CleanROItems]");
        //        var q_result = DbEngine.ExecuteNonQuery(@"delete from busdta.Order_detail where orderid = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + "");
        //        result = q_result > 0;
        //        ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
        //        Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:CleanROItems]");

        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][CleanROItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    return result;
        //}


        /// <summary>
        /// Unused function
        /// 10-13-2016
        /// </summary>
        /// <returns></returns>
        /// 
        //Unused function commented for performance  --Vignesh D
        //public List<ReturnItem> GetROItems()
        //{
        //    List<ReturnItem> result = new List<ReturnItem>();
        //    try
        //    {
        //        Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:GetROItems]");
        //        var q_result = DbEngine.ExecuteDataSet(@"select * from busdta.Order_detail where orderid = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + "");


        //        if (q_result.HasData())
        //        {
        //            foreach (DataRow od_row in q_result.Tables[0].Rows)
        //            {
        //                var r_item = new ReturnItem();
        //                r_item.ItemId = string.IsNullOrEmpty(od_row["ItemId"].ToString()) ? string.Empty : od_row["ItemId"].ToString().Trim();
        //                r_item.ReturnQty = string.IsNullOrEmpty(od_row["OrderQty"].ToString()) ? 0 : Convert.ToInt32(od_row["OrderQty"].ToString().Trim());
        //                r_item.OrderID = PayloadManager.ReturnOrderPayload.ReturnOrderID;
        //                result.Add(r_item);
        //            }
        //        }

        //        Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:GetROItems]");

        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][GetROItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    return result;
        //}


        public void UpdateInventory(ObservableCollection<ReturnItem> returnItems, int returnOrderID, ReturnOrderActions action)
        {

            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:UpdateInventory]");
            InventoryManager objInvMgr = new InventoryManager();
            try
            {
                //bool isROModified = false;    //Unused Local --Vignesh D

                //                string query = @"select od.ItemID as ItemID,od.OrderQty as OrderQty,od.OrderUM as OrderUM,ic.PrimaryUM as PrimaryUM,im.imlitm as ItemNumber
                //                                 from busdta.Order_detail od join busdta.ItemConfiguration ic on od.ItemId = ic.ItemId 
                //                                 join busdta.F4101 im on od.itemid = im.imitm 
                //                                 where od.OrderID='{0}'";
                //                query = string.Format(query, returnOrderID);

                string query = "call BUSDTA.SP_GetUpdateInventoryForCustReturn('" + returnOrderID + "')";

                DataSet ds = DbEngine.ExecuteDataSet(query);
                List<ReturnItem> existingItems = new List<ReturnItem>();
                if (ds.HasData())
                {
                    foreach (DataRow itemRow in ds.Tables[0].Rows)
                    {
                        ReturnItem returnItem = new ReturnItem();
                        returnItem.ItemId = itemRow["ItemID"].ToString().Trim();
                        returnItem.UM = itemRow["OrderUM"].ToString().Trim();
                        returnItem.ReturnQty = Convert.ToInt32(itemRow["OrderQty"].ToString().Trim());
                        returnItem.PrimaryUM = itemRow["PrimaryUM"].ToString().Trim();
                        returnItem.ItemNumber = itemRow["ItemNumber"].ToString().Trim();

                        existingItems.Add(returnItem);

                    }
                }
                //** Cases handled
                //Existing item was removed
                //Existing item qty was altered
                //New Item was added 
                //Existing items were removed and new items were added with same qty
                //var checkAlreadyCommitted = DbEngine.ExecuteScalar("select count(1) from busdta.Order_Detail where orderqty <> 0 and orderid = " + returnOrderID + "");
                var checkAlreadyCommitted = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetCommittedOrderQty(@ReturnOrderID= " + returnOrderID + ")");
                var count = Convert.ToInt32(checkAlreadyCommitted);
                if (count > 0)
                {
                    foreach (var returnItem in existingItems)
                    {
                        var factor = 0.0m;
                        if (UoMManager.ItemUoMFactorList != null)
                        {
                            var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == returnItem.ItemId.Trim()) && (x.FromUOM == returnItem.UM.ToString()) && (x.ToUOM == returnItem.PrimaryUM));
                            factor = itemUomConversion == null ? Convert.ToDecimal(UoMManager.GetUoMFactor(returnItem.UM, returnItem.PrimaryUM, Convert.ToInt32(returnItem.ItemId.Trim()), returnItem.ItemNumber.Trim())) : Convert.ToDecimal(itemUomConversion.ConversionFactor);
                        }
                        else
                        {
                            factor = Convert.ToDecimal(UoMManager.GetUoMFactor(returnItem.UM, returnItem.PrimaryUM, Convert.ToInt32(returnItem.ItemId.Trim()), returnItem.ItemNumber.Trim()));
                        }
                        returnItem.ReturnQtyInPrimaryUoM = Convert.ToInt32(returnItem.ReturnQty * factor);

                        //var invr_query = @"update busdta.Inventory set CommittedQuantity = CommittedQuantity + " + (returnItem.ReturnQtyInPrimaryUoM) + " WHERE ItemId=" + returnItem.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                        //DbEngine.ExecuteNonQuery(invr_query);

                    }
                }
                switch (action)
                {
                    case ReturnOrderActions.Accept:
                        // Update committed Qty. for all items in RO
                        foreach (var r_item in returnItems) // Update inventory for first time.
                        {
                            // Commented Start - 09/14/2016 - Call Common Function
                            //var inv_query = @"update busdta.Inventory set CommittedQuantity = CommittedQuantity - " + r_item.ReturnQtyInPrimaryUoM + " WHERE ItemId=" + r_item.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                            //DbEngine.ExecuteNonQuery(inv_query);

                            // Call Common Function
                            objInvMgr.UpdateCommittedQuantityForReturn(r_item.ReturnQtyInPrimaryUoM, r_item.ItemId.Trim(), CommonNavInfo.RouteID);
                        }

                        break;

                    // Return Void Scenario implementation
                    //case ReturnOrderActions.Void:
                    //    if (count > 0)
                    //    {
                    //        foreach (var r_item in returnItems)
                    //        {
                    //            var inv_query = @"update busdta.Inventory set CommittedQuantity = CommittedQuantity - " + (r_item.ReturnQtyInPrimaryUoM) + " WHERE ItemId=" + r_item.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                    //            DbEngine.ExecuteNonQuery(inv_query);

                    //            //var invUpdateQueryHeld = @"update busdta.Inventory set HeldQuantity = HeldQuantity - " + r_item.NonSellableQtyInPrimaryUoM + " where ItemId = " + r_item.ItemId.Trim() + " and RouteId = " + CommonNavInfo.RouteID;
                    //            //DbEngine.ExecuteNonQuery(invUpdateQueryHeld);

                    //            var inv_upd_onHand_query = @"update busdta.Inventory set OnHandQuantity = OnHandQuantity - " + (r_item.ReturnQtyInPrimaryUoM) + " WHERE ItemId=" + r_item.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                    //            DbEngine.ExecuteNonQuery(inv_upd_onHand_query);
                    //        }
                    //    }
                    //    break;

                    // Return Completed Scenario implementation
                    case ReturnOrderActions.OrderReturned:
                        foreach (var r_item in returnItems) // Commit final qty's to inventory.
                        {
                            // Commented Start - 09/14/2016 - Call Common Function
                            // var inv_upd_query = @"update busdta.Inventory set CommittedQuantity = CommittedQuantity - " + (r_item.ReturnQtyInPrimaryUoM) + " WHERE ItemId=" + r_item.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                            // DbEngine.ExecuteNonQuery(inv_upd_query);
                            // Commented End

                            // Call Common Function
                            objInvMgr.UpdateCommittedQuantityForReturn(r_item.ReturnQtyInPrimaryUoM, r_item.ItemId.Trim(), CommonNavInfo.RouteID);

                            //var inv_upd_onHand_query = @"update busdta.Inventory set OnHandQuantity = OnHandQuantity + " + r_item.ReturnQtyInPrimaryUoM + " WHERE ItemId=" + r_item.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                            //DbEngine.ExecuteNonQuery(inv_upd_onHand_query);

                            // Commented Start - 09/14/2016 - Call Common Function
                            // var invUpdateQueryHeld = @"update busdta.Inventory set HeldQuantity = HeldQuantity +" + r_item.NonSellableQtyInPrimaryUoM + " where ItemId = " + r_item.ItemId.Trim() + " and RouteId = " + CommonNavInfo.RouteID;
                            // DbEngine.ExecuteNonQuery(invUpdateQueryHeld);
                            // Commented End

                            // Call Common Function
                            objInvMgr.UpdateHeldQuantityForReturn(r_item.NonSellableQtyInPrimaryUoM, r_item.ItemId.Trim(), CommonNavInfo.RouteID);
                        }
                        break;

                    case ReturnOrderActions.OrderModified:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][UpdateInventory][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:UpdateInventory]");

        }

        // Unused Function
        // Commented Start - 09/14/2016 - Call Common Function
        //public void UpdateCommitted(int returnQty = 0, string itemid = "", bool updateHold = false)
        //{

        //    Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][Start:UpdateCommitted]");
        //    try
        //    {
        //        if (!updateHold)
        //        {
        //            var inv_query = @"update busdta.Inventory set CommittedQuantity = CommittedQuantity - " + returnQty + " WHERE ItemId=" + itemid.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
        //            DbEngine.ExecuteNonQuery(inv_query);
        //        }
        //        else
        //        {
        //            var OH_query = @"update busdta.Order_Header set HoldCommitted = 0 where orderid = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + " AND RouteId='" + CommonNavInfo.RouteID + "'";
        //            DbEngine.ExecuteNonQuery(OH_query);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][UpdateCommitted][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][End:UpdateCommitted]");
        //}

        public bool IsROPicked(int returnOrderID)
        {
            bool result = false;
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][Start:IsROPicked]");
            try
            {
                //var pickedSum = DbEngine.ExecuteScalar(@"SELECT sum(pickedqty) FROM BUSDTA.Pick_Detail where TransactionID = " + returnOrderID + "");
                var pickedSum = DbEngine.ExecuteScalar(@"CALL BUSDTA.SP_GetROPickedQty(@ReturnOrderID=" + returnOrderID + ")");
                result = string.IsNullOrEmpty(pickedSum.Trim()) ? false : Convert.ToInt32(pickedSum.Trim()) > 0;
            }
            catch (Exception ex)
            {
                result = false;
                Logger.Error("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][IsROPicked][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][End:IsROPicked]");
            return result;

        }

        /// <summary>
        /// GetROStatusTypeID
        /// </summary>
        /// <param name="salesOrderID"></param>
        /// <returns></returns>
        /// 
        //unused function commented for performance  --Vignesh D
        //public int GetROStatusTypeId(int salesOrderID)
        //{
        //    int statusid = -1;
        //    Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManagr][Start:GetROstatusTypeId]");
        //    try
        //    {
        //        //string query = @"select OrderTypeId from busdta.order_header where orderid = (select top 1 OrderID from BUSDTA.Order_Detail where RelatedOrderID = " + salesOrderID + ")";
        //        //var result = DbEngine.ExecuteScalar(query);

        //        var result = objOrderManager.CustomerReturnsManager_GetROStatusTypeId(salesOrderID);
        //        statusid = Convert.ToInt32(string.IsNullOrEmpty(result) ? "-1" : result.Trim());
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManagr][GetROstatusTypeId][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManagr][End:GetROstatusTypeId]");
        //    return statusid;
        //}

        public Dictionary<string, bool> GetOrderFlags(int OrderID)
        {
            bool IsSettled = false;
            bool IsOrderReturned = false;
            Dictionary<string, bool> result = new Dictionary<string, bool>();
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][Start:GetOrderFlags]");
            try
            {
                //var q_ordcmp = "select if orderstateid = " + ActivityKey.OrderReturned.GetStatusIdFromDB() + " then 1 else 0 ENDIF from busdta.Order_header where orderid = " + ActivityKey.OrderReturned.GetStatusIdFromDB() + "";
                //var ordcmp = DbEngine.ExecuteScalar(q_ordcmp);
                var q_ordcmp = "CALL BUSDTA.SP_GetFlagsForROItems(@OrderStateID=" + ActivityKey.OrderReturned.GetStatusIdFromDB() + ",@OrderID=" + ActivityKey.OrderReturned.GetStatusIdFromDB() + ",@Flags=1)";
                var ordcmp = DbEngine.ExecuteScalar(q_ordcmp);

                IsOrderReturned = string.IsNullOrEmpty(ordcmp) ? false : Convert.ToInt32(ordcmp.Trim()) > 0;

                //var q_setld = "SELECT LTRIM(rtrim((M.TDSTTLID))) AS STID FROM BUSDTA.M50012 M WHERE TDREFHDRID = " + OrderID + "";
                //var settld = DbEngine.ExecuteScalar(q_setld);
                var q_setld = "CALL BUSDTA.SP_GetFlagsForROItems(@OrderID= " + OrderID + ",@Flags=2)";
                var settld = DbEngine.ExecuteScalar(q_setld);

                //var Obj = DbEngine.ExecuteDataSet("call BUSDTA.SP_GetOrderFlagsForReturn(" + ActivityKey.OrderReturned.GetStatusIdFromDB() + "," + OrderID + ")");



                IsSettled = string.IsNullOrEmpty(settld.Trim()) ? false : true;


                //var q_result = DbEngine.ExecuteDataSet(@"SELECT if isnull(LTRIM(RTRIM(M.TDSTTLID)),0) <> 0 then 1 else 0 endif AS STTID FROM BUSDTA.M50012 M WHERE TDSTAT='ORDERRETURNED' AND TDREFHDRID =" + OrderID + "");
                //if (q_result.HasData())
                //{
                //    IsSettled = !string.IsNullOrEmpty(q_result.Tables[0].Rows[0]["STTID"].ToString()) && Convert.ToBoolean(q_result.Tables[0].Rows[0]["STTID"]); ;
                //    IsOrderReturned = !string.IsNullOrEmpty(q_result.Tables[0].Rows[0]["IsCompleteReturn"].ToString()) && Convert.ToBoolean(q_result.Tables[0].Rows[0]["IsCompleteReturn"]); ;

                //}
                result.Add("IsSettled", IsSettled);
                result.Add("IsOrderReturned", IsOrderReturned);
            }
            catch (Exception ex)
            {
                result = null;
                Logger.Error("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][GetOrderFlags][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][End:GetOrderFlags]");
            return result;

        }

        public bool DeleteHoldReturnItems(int returnOrderID)
        {
            bool result = false;
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][Start:DeleteHoldReturnItems]");
            try
            {
                //var pickedSum = DbEngine.ExecuteScalar(@"DELETE FROM BUSDTA.Order_Detail  WHERE  ORDERID='" + returnOrderID + "' ");
                //result = string.IsNullOrEmpty(pickedSum.Trim()) ? false : Convert.ToInt32(pickedSum.Trim()) > 0;

                var pickedSum = objOrderManager.CustomerReturnsManager_DeleteHoldReturnItems(returnOrderID);
                result = string.IsNullOrEmpty(pickedSum.Trim()) ? false : Convert.ToInt32(pickedSum.Trim()) > 0;
            }
            catch (Exception ex)
            {
                result = false;
                Logger.Error("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][DeleteHoldReturnItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][End:DeleteHoldReturnItems]");
            return result;

        }
        public bool IsOrderPartiallyReturn(int salesOrderID)
        {
            int result = 0;
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][Start:DeleteHoldReturnItems]");
            try
            {
                /* ----commented by Hari ---19 Sept 2016 ---*/
                // Partial Return Check from Order_Detail - Instead of SalesOrder_Retrun_Mapping
                //var stringa = "select OrderID FROM BUSDTA.Order_Detail where RelatedOrderID = " + salesOrderID + "";
                //var dbset = DbEngine.ExecuteDataSet(stringa);

                //if (dbset.Tables[0].Rows.Count > 0)
                //{
                //    foreach (DataRow rw in dbset.Tables[0].Rows)
                //    {
                //        var checkResult = DbEngine.ExecuteScalar("select OrderStateId from busdta.order_header where OrderID = " + rw[0].ToString().Trim());
                //        if (!string.IsNullOrEmpty(checkResult))
                //            if (Convert.ToInt32(checkResult) == ActivityKey.VoidAtROPick.GetStatusIdFromDB() ||
                //                Convert.ToInt32(checkResult) == ActivityKey.VoidAtROEntry.GetStatusIdFromDB() ||
                //                Convert.ToInt32(checkResult) == ActivityKey.CreateReturnOrder.GetStatusIdFromDB())
                //                continue;
                //        // RelatedOrderID from Order_Detail - Instead of SalesOrder_Retrun_Mapping
                //        var itemIdSet = DbEngine.ExecuteDataSet("select RelatedLineID,RelatedOrderID from busdta.Order_Detail where RelatedOrderID = " + salesOrderID +
                //                                                " and OrderID =" + rw[0].ToString().Trim());

                //        if (itemIdSet.HasData())
                //        {
                //            foreach (DataRow dataRow in itemIdSet.Tables[0].Rows)
                //            {
                //                // Return Order Qty Checck from Order_Detail - Instead of SalesOrder_Retrun_Mapping
                //                string O_qty = DbEngine.ExecuteScalar("select orderqty from busdta.order_detail where RelatedLineID =" + dataRow[0].ToString().Trim() + "AND RelatedOrderID=" + dataRow[1].ToString().Trim()).ToString();
                //                if (!string.IsNullOrEmpty(O_qty))
                //                {
                //                    if (Convert.ToInt32(O_qty) > 0)
                //                        result++;
                //                }

                //            }
                //        }
                //    }
                //}

                /* ----Written by Hari ---19 Sept 2016 ---*/

                //var query = "select OrderID,RelatedLineID,RelatedOrderID,orderqty FROM BUSDTA.Order_Detail where RelatedOrderID = " + salesOrderID + "";
                //var ds = DbEngine.ExecuteDataSet(query);

                var ds = objOrderManager.CustomerReturnsManager_IsOrderPartiallyReturn(salesOrderID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow rw in ds.Tables[0].Rows)
                    {
                        var checkResult = DbEngine.ExecuteScalar("select OrderStateId from busdta.order_header where OrderID = " + rw[0].ToString().Trim());

                        if (!string.IsNullOrEmpty(checkResult))
                            if (Convert.ToInt32(checkResult) == ActivityKey.VoidAtROPick.GetStatusIdFromDB() ||
                                Convert.ToInt32(checkResult) == ActivityKey.VoidAtROEntry.GetStatusIdFromDB() ||
                                Convert.ToInt32(checkResult) == ActivityKey.CreateReturnOrder.GetStatusIdFromDB())
                                continue;
                        // RelatedOrderID from Order_Detail - Instead of SalesOrder_Retrun_Mapping
                        //var itemIdSet = DbEngine.ExecuteDataSet("select RelatedLineID,RelatedOrderID from busdta.Order_Detail where RelatedOrderID = " + salesOrderID +
                        //                                        " and OrderID =" + rw[0].ToString().Trim());
                        DataRow[] drr = ds.Tables[0].Select("OrderID =" + rw[0].ToString().Trim());
                        if (drr.Count() > 0)
                        {
                            foreach (DataRow dataRow in drr)
                            {
                                // Return Order Qty Checck from Order_Detail - Instead of SalesOrder_Retrun_Mapping
                                //string qty = ds.Tables[0].Select("OrderID =" + rw["OrderID"].ToString().Trim() + " and RelatedLineID =" + dataRow["RelatedLineID"].ToString().Trim() + " AND RelatedOrderID=" + dataRow["RelatedOrderID"].ToString().Trim()).ToString();
                                //if (!string.IsNullOrEmpty(qty))
                                //{
                                //    if (Convert.ToInt32(qty) > 0)
                                //        result++;
                                //}
                                DataRow[] drr1 = ds.Tables[0].Select("OrderID =" + rw["OrderID"].ToString().Trim() + " and RelatedLineID =" + dataRow["RelatedLineID"].ToString().Trim() + " AND RelatedOrderID=" + dataRow["RelatedOrderID"].ToString().Trim());
                                if (drr1.Count() > 0)
                                {
                                    if (Convert.ToInt32(drr1[0]["orderqty"]) > 0)
                                        result++;
                                }

                            }
                        }
                    }
                }



            }
            catch (Exception ex)
            {
                result = 0;
                Logger.Error("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][DeleteHoldReturnItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][End:DeleteHoldReturnItems]");
            return !(result > 0);

        }

        //Disposable Implementation --Vignesh D
        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {

                handle.Dispose();
                if (pm != null)
                    pm.Dispose();
                if (objOrderManager != null)
                    objOrderManager.Dispose();
            }
            GC.Collect();
            disposed = true;
        }

    }
    public enum ReturnOrderActions
    {
        Accept,
        Hold,
        Void,
        OrderReturned,
        OrderModified
    }


    // Class Unused --Vignesh D
    //class ReturnOrderChanges
    //{
    //    public int ItemID { get; set; }
    //    public int SalesOrderID { get; set; }
    //    public int OrderQty { get; set; }
    //}
}
