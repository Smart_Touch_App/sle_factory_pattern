﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Windows.Threading;

namespace SalesLogicExpress.Application.Managers
{
    public class StopManager : IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.StopManager");
        readonly ServiceRouteManager serviceRouteManager = new ServiceRouteManager();
        readonly string baseDate = ConfigurationManager.AppSettings["BaseDate"].ToString();
        //readonly int yearsToAdd = Convert.ToInt32(ConfigurationManager.AppSettings["YearsToAdd"].ToString());

        //private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.StopManager");
        DateTime selectionEndDate;

        public void MoveStopDialogContext(ref CreateAndMoveStopViewModel moveStopViewModelProperty, Visitee pVisitee, DateTime pDailyStopDate, Guid messageToken)
        {
            Visitee visiteeToMove = new Visitee();
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:MoveStopDialogContext][MoveStopViewModelProperty=" + moveStopViewModelProperty + "][CustomerToMove=" + visiteeToMove + "][pDailyStopDate=" + pDailyStopDate + "][MessageToken=" + messageToken + "]");
                visiteeToMove = pVisitee.DeepCopy();
                DateTime baseDateApp = Convert.ToDateTime(baseDate);
                DateTime lastStop = new DateTime();
                Dictionary<string, string> dict = new Dictionary<string, string>();
                selectionEndDate = GetLastDate;
                // Dictionary contains next stop date and previous stop date  *2nd parameter to be changed based on decision
                DateTime dateToCalcMove = pDailyStopDate.Date < DateTime.Today.Date ? DateTime.Today.Date : pDailyStopDate;
                if (visiteeToMove.IsTodaysStop && pDailyStopDate.Date < DateTime.Today.Date)
                {
                    dateToCalcMove = pDailyStopDate;
                }
                dict = serviceRouteManager.GetVisiteeStopInfo(visiteeToMove, dateToCalcMove, false, false);

                visiteeToMove.NextStop = dict["NextStop"].ToString();
                visiteeToMove.PreviousStop = dict["LastStop"].ToString();
                if (!(visiteeToMove.PreviousStop.ToLower() == "none"))
                {
                    if (visiteeToMove.VisiteeType.ToLower() == "prospect")
                    {
                        lastStop = DateTime.Today;
                    }
                    else
                    {
                        lastStop = DateTime.Parse(visiteeToMove.PreviousStop, CultureInfo.CreateSpecificCulture("en-US"));
                    }
                }
                else
                {
                    lastStop = baseDateApp;
                }
                moveStopViewModelProperty.Visitee = visiteeToMove;
                if (!(visiteeToMove.PreviousStop.ToLower() == "none"))
                {
                    if (DateTime.Compare(pDailyStopDate.Date, DateTime.Today.Date) == 0)
                    {
                        moveStopViewModelProperty.SelectableDateStart = DateTime.Today.Date.AddDays(1);
                    }
                    else
                    {
                        if (!(DateTime.Parse(visiteeToMove.PreviousStop, CultureInfo.CreateSpecificCulture("en-US")) < DateTime.Today))
                        {
                            moveStopViewModelProperty.SelectableDateStart = DateTime.Parse(visiteeToMove.PreviousStop, CultureInfo.CreateSpecificCulture("en-US"));

                        }
                        else
                            moveStopViewModelProperty.SelectableDateStart = DateTime.Today;
                    }
                }
                if (visiteeToMove.StopType.ToLower() == "unplanned")
                {
                    moveStopViewModelProperty.SelectableDateStart = DateTime.Today;
                }
                if (visiteeToMove.OriginalDate == null)
                {
                    visiteeToMove.OriginalDate = pDailyStopDate;
                }
                else
                {
                    //try
                    //{
                    //    CustomerToMove.OriginalDate = DateTime.Parse(CustomerToMove.OriginalDate, CultureInfo.CreateSpecificCulture("en-US")).ToString("MM'/'dd'/'yyyy ");
                    //}
                    //catch
                    //{
                    //    CustomerToMove.OriginalDate = Convert.ToDateTime(CustomerToMove.OriginalDate).ToString("MM'/'dd'/'yyyy ");
                    //}
                }
                if (!(visiteeToMove.VisiteeType.ToLower() == "prospect"))
                {
                    if (!(visiteeToMove.NextStop == "None"))
                    {

                        moveStopViewModelProperty.SelectableDateEnd = DateTime.Parse(visiteeToMove.NextStop, CultureInfo.CreateSpecificCulture("en-US")).AddDays(-1);

                    }
                    else
                    {
                        moveStopViewModelProperty.SelectableDateEnd = DateTime.Today.AddDays(7);
                    }
                    if (visiteeToMove.StopType.ToLower() == "unplanned")
                    {
                        moveStopViewModelProperty.SelectableDateEnd = selectionEndDate;
                    }
                }
                else
                {
                    moveStopViewModelProperty.SelectableDateEnd = selectionEndDate;
                }

                moveStopViewModelProperty.DailyStopDate = pDailyStopDate;
                bool isNotUnplanned = visiteeToMove.StopType.ToLower() != "unplanned";
                moveStopViewModelProperty.BlackOutDates = serviceRouteManager.GetPresentStops(visiteeToMove.VisiteeId, moveStopViewModelProperty.SelectableDateStart, moveStopViewModelProperty.SelectableDateEnd, isNotUnplanned);

                if (visiteeToMove.IsTodaysStop && pDailyStopDate.Date < DateTime.Today.Date)
                {
                    moveStopViewModelProperty.BlackOutDates.Add(DateTime.Today.Date);

                }
                //Commented on 11/10/2016
                //string stopStatusQuery = "select RPSTTP from busdta.m56m0004 where RPAN8=" + visiteeToMove.VisiteeId + " and RPSTDT='" + lastStop.ToString("yyyy-MM-dd") + "'";
                string stopStatusQuery = "Call BUSDTA.SP_GetMoveStopDialogContext(@RpanID='" + visiteeToMove.VisiteeId + "',@LastStopID='" + lastStop.ToString("yyyy-MM-dd") + "')";

                string stopStatus = DbEngine.ExecuteScalar(stopStatusQuery);
                if (!(stopStatus.ToLower() == "moved"))
                    moveStopViewModelProperty.BlackOutDates.Add(lastStop);


                CreateAndMoveStopViewModel objMoveStop = new CreateAndMoveStopViewModel();
                objMoveStop = moveStopViewModelProperty;
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    var dialog = new Helpers.DialogWindow { TemplateKey = "MoveStopDialog", Title = "Move Stop", Payload = objMoveStop };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, messageToken);

                    objMoveStop.IsCreateButton = dialog.Closed ? true : false;
                }));
                moveStopViewModelProperty = objMoveStop;
                objMoveStop.Dispose();
                Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:MoveStopDialogContext][MoveStopViewModelProperty=" + moveStopViewModelProperty + "][CustomerToMove=" + visiteeToMove + "][pDailyStopDate=" + pDailyStopDate + "][MessageToken=" + messageToken + "]");

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][StopManager][MoveStopDialogContext][MoveStopViewModelProperty=" + moveStopViewModelProperty + "][CustomerToMove=" + visiteeToMove + "][pDailyStopDate=" + pDailyStopDate + "][MessageToken=" + messageToken + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }

        public DateTime GetLastDate
        {
            get
            {
                try
                {
                    Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:GetLastDate]");
                    var lastGeneratedDate = DateTime.Now.AddYears(1);
                    //Commented on 11/10/2016
                    //string stopCount = DbEngine.ExecuteScalar("select count(1) from busdta.m56m0004");
                    string stopCount = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetLastDateCount()");

                    if (!string.IsNullOrEmpty(stopCount))
                    {
                        if (Convert.ToInt32(stopCount) > 0)
                        {
                            //Commented on 11/10/2016
                            //string query = "SELECT top 1 RPSTDT FROM BUSDTA.M56M0004 WHERE RPSTDT > '" + DateTime.Today.Date.ToString("yyyy-MM-dd") + "' order by RPSTDT desc";
                            string query = "CALL BUSDTA.SP_GetLastDate(@lastdate='" + DateTime.Today.Date.ToString("yyyy-MM-dd") + "')";
                            string strSelectedDateEnd = DbEngine.ExecuteScalar(query);
                            if (!string.IsNullOrEmpty(strSelectedDateEnd))
                                lastGeneratedDate = Convert.ToDateTime(strSelectedDateEnd);
                        }
                    }
                    Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][End:GetLastDate]");
                    return lastGeneratedDate;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][StopManager][GetLastDate][ExceptionStackTrace = " + ex.StackTrace + "]");
                    return DateTime.Now.AddYears(1);
                }
            }
        }

        public void CreateStopDialogContext(ref CreateAndMoveStopViewModel createStopViewModelProperty, Visitee pVisitee, DateTime pDailyStopDate, Guid messageToken)
        {
            Visitee visiteeToCreate = pVisitee;
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:CreateStopDialogContext][CreateStopViewModelProperty=" + createStopViewModelProperty + "][CustomerToCreate=" + visiteeToCreate + "][pDailyStopDate=" + pDailyStopDate + "][MessageToken=" + messageToken + "]");

                visiteeToCreate = pVisitee.DeepCopy();
                Dictionary<string, string> dict = new Dictionary<string, string>();
                //DateTime baseDateApp = Convert.ToDateTime(baseDate);
                //Commented on 11/10/2016
                //string query = "SELECT top 1 RPSTDT FROM BUSDTA.M56M0004 WHERE RPSTDT > '" + DateTime.Today.Date.ToString("yyyy-MM-dd") + "' order by RPSTDT desc";

                //String Handling for Memory Optimization --Vignesh D
                string query = string.Empty;
                query = "Call BUSDTA.SP_GetCreateStopDialogContextTopDate(@RPSTDate=" + DateTime.Today.Date.ToString("yyyy-MM-dd") + ")";

                string strSelectedDateEnd = string.Empty;
                strSelectedDateEnd = DbEngine.ExecuteScalar(query);
                query = string.Empty;

                selectionEndDate = Convert.ToDateTime(strSelectedDateEnd);
                strSelectedDateEnd = string.Empty;

                if (visiteeToCreate.IsTodaysStop && visiteeToCreate.StopType.ToLower() == "moved")
                    visiteeToCreate.IsTodaysStop = false;
                // Dictionary contains next stop date and previous stop date  *2nd parameter to be changed based on decision
                dict = serviceRouteManager.GetVisiteeStopInfo(visiteeToCreate, pDailyStopDate, false, false);
                //Commented on 11/10/2016
                //string count = "select count(1) from busdta.M56M0004 where RPAN8=" + visiteeToCreate.VisiteeId + " and RPSTDT='" + pDailyStopDate.Date.ToString("yyyy-MM-dd") + "' and RPSTTP <> 'Moved'";

                string count = string.Empty;
                count = "Call BUSDTA.SP_GetStopDateByDaily(@visiteeID='" + visiteeToCreate.VisiteeId + "',@Dailydate=" + pDailyStopDate.Date.ToString("yyyy-MM-dd") + ")";

                //String Handling for Memory Optimization --Vignesh D
                string stopPresent = string.Empty;
                stopPresent = DbEngine.ExecuteScalar(count);
                count = string.Empty;
                createStopViewModelProperty.Visitee = visiteeToCreate;
                // checks customer is from daily stop tab and is in todays stop list
                if (DateTime.Compare(pDailyStopDate.Date, DateTime.Today.Date) == 0)
                {

                    createStopViewModelProperty.SelectableDateStart = DateTime.Today.Date.AddDays(1);

                    if (!visiteeToCreate.IsTodaysStop || visiteeToCreate.IsFromListingTab)
                    {
                        createStopViewModelProperty.SelectableDateStart = DateTime.Today.Date;
                    }


                }
                // If create stop is from customers or prospects tab
                else
                {
                    //Commented on 11/10/2106
                    //string countAll = "select count(1) from busdta.M56M0004 where RPAN8=" + visiteeToCreate.VisiteeId + " and RPSTDT='" + DateTime.Now.Date.ToString("yyyy-MM-dd") + "' and rpsttp <> 'Moved'";

                    //string Handling for Memory Optimization --Vignesh D
                    string countAll = string.Empty;
                    countAll = "call BUSDTA.SP_GetStopDateByToday(@visiteeID='" + visiteeToCreate.VisiteeId + "', @Today=" + DateTime.Now.Date.ToString("yyyy-MM-dd") + ")";

                    string presentCount = string.Empty;
                    presentCount = DbEngine.ExecuteScalar(countAll);
                    countAll = string.Empty;

                    createStopViewModelProperty.SelectableDateStart = Convert.ToInt32(presentCount) > 0 ? DateTime.Today.Date.AddDays(1) : DateTime.Today; ;
                    presentCount = string.Empty;

                }
                // if selected item is prospect date selection will be from today
                if (visiteeToCreate.VisiteeType.ToLower() == "prospect")
                {
                    createStopViewModelProperty.SelectableDateStart = DateTime.Today;
                }
                if (visiteeToCreate.VisiteeType.ToLower() == "cust")
                {

                    visiteeToCreate.NextStop = dict["NextStop"].ToString();

                    visiteeToCreate.PreviousStop = dict["LastStop"].ToString();

                    if (visiteeToCreate.OriginalDate == null)
                    {

                        visiteeToCreate.OriginalDate = pDailyStopDate;
                    }

                    // Sets selectable date end as last stop generated date.
                    createStopViewModelProperty.SelectableDateEnd = selectionEndDate;
                }
                else
                {
                    visiteeToCreate.OriginalDate = null;
                    visiteeToCreate.NextStop = "";
                    visiteeToCreate.PreviousStop = "";
                    createStopViewModelProperty.SelectableDateEnd = selectionEndDate;
                }
                createStopViewModelProperty.DailyStopDate = pDailyStopDate;
                createStopViewModelProperty.BlackOutDates = serviceRouteManager.GetPresentStops(visiteeToCreate.VisiteeId, createStopViewModelProperty.SelectableDateStart, createStopViewModelProperty.SelectableDateEnd, false);
                // Removes selected date from planned stops collection .
                if (visiteeToCreate.IsFromListingTab == false)
                {
                    // before removing checks stop count for that day
                    if (!(Convert.ToInt32(stopPresent) > 0))
                    {
                        if (createStopViewModelProperty.BlackOutDates.Any(item => item == pDailyStopDate.Date) && visiteeToCreate.StopType != null && visiteeToCreate.StopType.ToLower() == "moved")
                        {
                            // check  for moved stop is in todays date or in future date list
                            //if (DateTime.Compare(pDailyStopDate.Date, DateTime.Today.Date) == 0) { }
                            createStopViewModelProperty.BlackOutDates.RemoveAll(item => item == pDailyStopDate.Date);
                        }
                        //Commented on 11/10/2016
                        //string movedStopSet = "select rpstdt as 'date' from busdta.M56M0004 where RPAN8=" + visiteeToCreate.VisiteeId + " and RPSTDT >='" + DateTime.Now.Date.ToString("yyyy-MM-dd") + "' and rpsttp = 'Moved'";

                        //string Handling for Memory Optimization --Vignesh D
                        string movedStopSet = string.Empty;
                        movedStopSet = "call BUSDTA.SP_GetMovedStopDate(@visiteeID='" + visiteeToCreate.VisiteeId + "',@Today=" + DateTime.Now.Date.ToString("yyyy-MM-dd") + ")";
                        DataSet presentStops = DbEngine.ExecuteDataSet(movedStopSet);
                        movedStopSet = string.Empty;
                        if (presentStops.HasData())
                        {
                            foreach (DataRow rw in presentStops.Tables[0].Rows)
                            {
                                DateTime dt = Convert.ToDateTime(rw["date"]);
                                createStopViewModelProperty.BlackOutDates.RemoveAll(item => item.Date == dt.Date);
                            }
                        }


                        //Dataset Disposing for memory Optimization --Vignesh D
                        presentStops.Dispose();
                        presentStops = null;
                    }
                }

                stopPresent = string.Empty;
                CreateAndMoveStopViewModel objCreateStop = new CreateAndMoveStopViewModel();
                objCreateStop = createStopViewModelProperty;
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    var dialog = new Helpers.DialogWindow { TemplateKey = "CreateStopDialog", Title = "Create Stop", Payload = objCreateStop };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, messageToken);

                    objCreateStop.IsCreateButton = dialog.Closed ? true : false;
                }));
                createStopViewModelProperty = objCreateStop;
                objCreateStop.Dispose();
                // objCreateStop = null;  //Object Disposing for memory Optimization --Vignesh D
                dict = null;

                Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][End:CreateStopDialogContext][CreateStopViewModelProperty=" + createStopViewModelProperty + "][CustomerToCreate=" + visiteeToCreate + "][pDailyStopDate=" + pDailyStopDate + "][MessageToken=" + messageToken + "]");

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][StopManager][CreateStopDialogContext][CreateStopViewModelProperty=" + createStopViewModelProperty + "][CustomerToCreate=" + visiteeToCreate + "][pDailyStopDate=" + pDailyStopDate + "][MessageToken=" + messageToken + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }

        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/
        /* Customer DashBoard Manager - Common Stop Function */
        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/

        public int UpdateCustomerNoSaleActivity(int noSaleReasonID, string stopID, string customerID, bool flag)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:UpdateCustomerNoSaleActivity]");
            int Result = 0;
            try
            {
                string query = string.Empty;
                if (flag)
                    query = "UPDATE busdta.M56M0004  SET RPRCID = {0} , RPACTID = 'NOSALE' WHERE RPSTID = '{1}' AND RPAN8 ='{2}'";
                else
                    query = "UPDATE busdta.M56M0004  SET RPRCID = {0} , RPACTID = 'NOSALE',RPUPDT=GETDATE() WHERE RPSTID = '{1}' AND RPAN8 ='{2}'";
                query = string.Format(query, noSaleReasonID, stopID, customerID);
                Result = Helpers.DbEngine.ExecuteNonQuery(query);
                ResourceManager.QueueManager.QueueProcess("StopManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][StopManager][UpdateCustomerNoSaleActivity][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:UpdateCustomerNoSaleActivity]");
            return Result;
        }

        public int UpdateForCloseActivity(string stopID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:UpdateForCloseActivity]");
            int Result = 0;
            try
            {
                string query = string.Empty;
                //Commented on 11/10/2016
                //query = "SELECT COUNT(*) REC FROM BUSDTA.M56M0004 WHERE RPSTID='" + stopID + "' AND RPPACT <> 0";
                query = "CALL BUSDTA.SP_GetCountForCloseActivity(@stopid='" + stopID + "')";

                int PrvRPPACT = Convert.ToInt32(DbEngine.ExecuteScalar(query));
                if (PrvRPPACT > 0)
                {
                    // Field Update - Modifed
                    query = "UPDATE BUSDTA.M56M0004 SET RPPACT=RPPACT-1, RPCACT =1 WHERE RPSTID ='" + stopID + "'";
                }
                else
                {
                    // Field Update - Modifed
                    query = "UPDATE BUSDTA.M56M0004 SET RPPACT=0, RPCACT =1 WHERE RPSTID ='" + stopID + "'";
                }
                Result = DbEngine.ExecuteNonQuery(query);
                ResourceManager.QueueManager.QueueProcess("StopManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][StopManager][UpdateForCloseActivity][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][End:UpdateForCloseActivity]");
            return Result;
        }

        public int UpdateStopActivityCount(string stopID, bool isPendingAct, bool isIncrement)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:UpdateStopActivityCount]");
            int Result = 0;
            try
            {
                string query = string.Empty;
                if (isPendingAct)
                {
                    if (isIncrement)
                    {
                        query = "UPDATE BUSDTA.M56M0004 SET RPPACT=RPPACT+1  WHERE RPSTID ='" + stopID + "'";
                    }
                    else
                    {
                        query = "UPDATE BUSDTA.M56M0004 SET RPPACT=RPPACT-1  WHERE RPSTID ='" + stopID + "' and RPPACT!=0";
                    }
                }
                else
                {
                    if (isIncrement)
                    {
                        query = "UPDATE BUSDTA.M56M0004 SET RPCACT=RPCACT+1  WHERE RPSTID ='" + stopID + "' ";
                    }
                    else
                    {
                        query = "UPDATE BUSDTA.M56M0004 SET RPCACT=RPCACT-1  WHERE RPSTID ='" + stopID + "' and RPCACT !=0";
                    }
                }
                Result = DbEngine.ExecuteNonQuery(query);
                ResourceManager.QueueManager.QueueProcess("StopManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][StopManager][UpdateStopActivityCount][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][End:UpdateStopActivityCount]");
            return Result;
        }

        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/
        /* Customer Manager - Common Stop Function */
        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/

        public void UpdateStopSequenceNumber(int seqID, string stopID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:UpdateStopSequenceNumber]");
            try
            {
                string queryToUpdateSeqNo = "UPDATE BUSDTA.M56M0004 SET RPSN='" + seqID + "' WHERE RPSTID='" + stopID + "'";
                DbEngine.ExecuteNonQuery(queryToUpdateSeqNo);
                ResourceManager.QueueManager.QueueProcess("StopManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][StopManager][UpdateStopSequenceNumber][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][End:UpdateStopSequenceNumber]");
        }

        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/
        /* Customer Return Manager - Common Stop Function */
        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/

        public int UpdateStopActivity(string stopID, string customerNo)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:UpdateStopSequenceNumber]");
            int Result = 0;
            try
            {
                string queryToUpdateSeqNo = "UPDATE BUSDTA.M56M0004 SET RPACTID=null where rpstid = " + stopID + " and rpan8=" + customerNo + " ";
                Result = DbEngine.ExecuteNonQuery(queryToUpdateSeqNo);
                ResourceManager.QueueManager.QueueProcess("StopManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][StopManager][UpdateStopSequenceNumber][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][End:UpdateStopSequenceNumber]");
            return Result;
        }

        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/
        /* Customer Stop Sequencing Manager - Common Stop Function */
        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/

        public void UpadateStopReschduledAndUnplanned(int seqNumber, string customerNo, string dateParameter)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:UpadateStopReschduledAndUnplanned]");
            try
            {
                string query = @"update BUSDTA.M56M0004  set RPSN =" + seqNumber + " where RPAN8='" + customerNo + "'   AND RPSTDT IN ( '" + dateParameter + "') ";
                DbEngine.ExecuteNonQuery(query);
                ResourceManager.QueueManager.QueueProcess("StopManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][StopManager][UpadateStopReschduledAndUnplanned][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][End:UpadateStopReschduledAndUnplanned]");
        }

        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/
        /* Order Manager - Common Stop Function */
        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/

        //public int UpdateStopNoSaleReason(string stopID, string customerID)
        //{
        //    Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:UpdateNoSaleReason]");
        //    int Result = 0;
        //    try
        //    {
        //        string query = "update busdta.M56M0004  set RPRCID = NULL, RPACTID = '' where RPSTID = '{0}' ";
        //        query = string.Format(query, stopID);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][StopManager][UpdateNoSaleReason][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][End:UpdateNoSaleReason]");
        //    return Result;
        //}

        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/
        /* Service Route   - Common Stop Function */
        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/

        public void UpadateStopResequence(int seqNumber, string stopID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:UpadateStopReschduledAndUnplanned]");
            try
            {
                //String HAndling for Memory Optimization  --Vignesh D
                string updateSeqNo = string.Empty;
                updateSeqNo = "update BUSDTA.M56M0004 set RPSN= " + seqNumber + " where RPSTID='" + stopID + "'";
                DbEngine.ExecuteNonQuery(updateSeqNo);
                updateSeqNo = string.Empty;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][StopManager][UpadateStopReschduledAndUnplanned][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][End:UpadateStopReschduledAndUnplanned]");
        }

        public void UpdateMovedStop(string RPRSTID, string originalStopID, string RouteUser, string VisiteeId, string orgDate)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:UpdateMovedStop]");
            try
            {
                string updateMovedStop = string.Empty;
                updateMovedStop = "UPDATE BUSDTA.M56M0004 SET RPRSTID=" + RPRSTID + ",RPSTTP='Moved'";
                updateMovedStop = updateMovedStop + " WHERE RPSTID=" + originalStopID + " AND RPROUT='" + RouteUser + "' AND RPAN8=" + VisiteeId + " AND RPSTDT=cast('" + orgDate + "' as date)";
                DbEngine.ExecuteNonQuery(updateMovedStop);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][StopManager][UpdateMovedStop][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][End:UpdateMovedStop]");
        }

        public void UpdateStop(string UserName, string updateDate, string stopID, string RouteUser, string VisiteeId, string startDate)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:UpdateStop]");
            try
            {
                string updateStop = string.Empty;
                updateStop = "UPDATE BUSDTA.M56M0004 SET RPOGDT=null,RPSTTP='Planned',RPRSTID = null,RPUPBY='" + UserName + "',RPUPDT='" + updateDate + "' ";
                updateStop += " WHERE RPSTID=(select RPSTID FROM BUSDTA.M56M0004 where RPRSTID =" + stopID + ") AND RPROUT='" + CommonNavInfo.RouteUser + "' AND RPAN8=" + VisiteeId + " AND RPSTDT='" + startDate + "'";
                DbEngine.ExecuteNonQuery(updateStop);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][StopManager][UpdateStop][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][End:UpdateStop]");
        }

        public int UpdateSequenceNumber(string stopId, string sequenceNO, string userName, string stopDate, string refStopID, bool refFlag)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:UpdateStop]");
            int Result = 0;
            try
            {
                string query = string.Empty;
                if (refFlag)
                {
                    query = @" UPDATE BUSDTA.M56M0004 R SET R.RPSN='" + sequenceNO + @"' ,R.RPUPBY='" + userName + @"' , 
                     R.RPISRSN=1 WHERE R.RPRSTID = '" + refStopID + "' AND R.RPSTDT='" + stopDate + "' ";
                }
                else
                {
                    query = @" UPDATE BUSDTA.M56M0004 R SET R.RPSN='" + sequenceNO + @"' ,R.RPUPBY='" + userName + @"' , 
                                 R.RPISRSN=1 WHERE R.RPSTID = " + stopId + " AND R.RPSTDT='" + stopDate + "' ";
                }
                Result = DbEngine.ExecuteNonQuery(query);
                query = string.Empty;
                ResourceManager.QueueManager.QueueProcess("StopManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][StopManager][UpdateStop][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][StopManager][End:UpdateStop]");
            return Result;
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
                if (serviceRouteManager != null)
                    serviceRouteManager.Dispose();
            }
            disposed = true;
        }
    }
}
