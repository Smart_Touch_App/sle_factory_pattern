﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Timers;
using Timer = System.Timers.Timer;

namespace SalesLogicExpress.Application.Managers
{
    public class QueueManager : IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.QueueManager");
        public event EventHandler<SyncQueueChangedEventArgs> QueueChanged;

        public QueueManager()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:QueueManager]");
            try
            {
                QueueTimer = new Timer();
                QueueTimer.Elapsed += QueueTimerElapsed;
                QueueTimer.Enabled = true;
                QueueTimer.AutoReset = true;
                QueueTimer.Interval = 59000; //59 Sec
                QueueTimer.Start();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][QueueManager][ExceptionStackTrace = " + ex.StackTrace + "][Message = ]" + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:QueueManager]");
        }

        private void QueueTimerElapsed(object sender, ElapsedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:QueueTimerElapsed]");
            try
            {
                var idleTime = IdleTimeDetector.GetIdleTimeInfo();
                double interval = string.IsNullOrEmpty(ConfigurationManager.AppSettings["syncPollingTime"].ToString()) ? 5 : Convert.ToInt32(ConfigurationManager.AppSettings["syncPollingTime"].ToString());
                if (idleTime.IdleTime.TotalMinutes >= interval)
                {
                    QueueProcess("", true);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][QueueTimerElapsed][ExceptionStackTrace = " + ex.StackTrace + "][Message = ]" + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:QueueTimerElapsed]");
        }


        public void ListenToSync()
        {
            ResourceManager.Synchronization.SyncProgressChanged += OnSyncStateChange;
        }

        List<QueueProfileMapping> queueProfileMapping = new List<QueueProfileMapping>();
        private void InitilizeQueueAndProfileDetails()
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:InitilizeQueueAndProfileDetails]");

                //Commented on 12/10/2016
                //                string query = @"select  qm.QueueMasterID as QueueID, qm.QueueName, pm.ProfileID, pm.ProfileName, pm.PublicationName from BUSDTA.SyncProfileMaster pm, BUSDTA.SyncQueueMaster qm, BUSDTA.SyncQueueProfileMapping qpm 
                //                                where pm.ProfileID=qpm.ProfileMasterID and qm.QueueMasterID=qpm.QueueMasterID order by qm.QueueMasterID";
                string query = "CALL BUSDTA.SP_GetQueueAndProfileDetails()";
                queueProfileMapping = DbEngine.ExecuteDataSet(query).GetEntityList<QueueProfileMapping>();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][QueueProcess][ExceptionStackTrace = " + ex.StackTrace + "][Message = ]" + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:InitilizeQueueAndProfileDetails]");
        }


        protected virtual void OnBlockUI(bool e)
        {
            EventHandler<bool> handler = BockUIManually;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public event EventHandler<bool> BockUIManually;
        private bool uiBlocked { get; set; }
        public async void QueueProcess(string QueueType = "", bool IsReadyToSync = false)
        {
            await Task.Factory.StartNew(() =>
            {
                try
                {
                    Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:QueueProcess]");

                    if (IsReadyToSync == false || !string.IsNullOrEmpty(QueueType))
                    {
                        AddQueueRequestDetails(QueueType);
                    }

                    if (IsReadyToSync == true)
                    {
                        //Populate Queue
                        var finalQueue = getAllSyncQueueDetails().OrderBy(x => x.SyncQueueId).ToList();

                        if (finalQueue.Count > 0)
                        {
                            foreach (var queue in finalQueue)
                            {
                                if (queue.BlockUI == true || QueueType == "All")
                                {
                                    if (uiBlocked == false)
                                    {
                                        OnBlockUI(true);
                                        uiBlocked = true;
                                    }
                                    ResourceManager.CommonNavInfo.IsCloseVisible = false;
                                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsSyncGoing = true;
                                    ResourceManager.CommonNavInfo.SyncEnabled = false;
                                    ResourceManager.CommonNavInfo.ProgressSteps = "Completing full synchronization. Please standby.";
                                }

                                if (QueueType != "All")
                                {
                                    var queueItems = populateQueueByType(queue.PublicationName);
                                    if (queueItems.Count > 0)
                                    {
                                        NotifyQueueChange(queue.ProfileName, queueItems, queue.SyncDirection.Trim().ParseEnum<SyncDownloadType>(SyncDownloadType.Both), queue.BlockUI);
                                    }
                                    else
                                    {
                                        UpdateQueueProcess(queue.ProfileName, "NoItemsToSync");
                                    }
                                }
                                else
                                {
                                    NotifyQueueChange(queue.ProfileName, null, queue.SyncDirection.Trim().ParseEnum<SyncDownloadType>(SyncDownloadType.Both), queue.BlockUI);
                                    ResourceManager.CommonNavInfo.IsCloseVisible = true;
                                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsSyncGoing = false;
                                    ResourceManager.CommonNavInfo.SyncEnabled = true;
                                    ResourceManager.CommonNavInfo.ProgressSteps = "Completed full synchronization.";
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][QueueProcess][ExceptionStackTrace = " + ex.StackTrace + "][Message = ]" + ex.Message);
                }

                uiBlocked = false;
                ResourceManager.CommonNavInfo.IsCloseVisible = true;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsSyncGoing = false;
                ResourceManager.CommonNavInfo.SyncEnabled = true;
                ResourceManager.CommonNavInfo.ProgressSteps = "Completed full synchronization.";
                Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:QueueProcess]");
            });
        }

        private void AddQueueRequestDetails(string reqType)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:AddQueueRequestDetails]");
            try
            {
                if (queueProfileMapping.Count == 0)
                    InitilizeQueueAndProfileDetails();

                if (queueProfileMapping != null)
                {
                    var queueList = queueProfileMapping.Where(x => x.QueueName == reqType).ToList();
                    if (queueList != null)
                    {
                        if (queueList.Count > 0)
                        {
                            int queueID = 0;
                            try
                            {
                                queueID = queueList.FirstOrDefault().QueueID;
                            }
                            catch (Exception) { }

                            if (queueID != 0)
                            {
                                //Commented on 12/10/2016
                                //string checkActive = "select COUNT(*) from busdta.SyncQueueActivityMapping where TRIM(QueueName)='" + reqType + "' and isActive=1";
                                string checkActive = "CALL BUSDTA.SP_GetCountQueueRequestDetails(@reqtype='" + reqType + "')";

                                int count = 0;
                                count = Convert.ToInt32(DbEngine.ExecuteScalar(checkActive));
                                if (count > 0 || reqType == "All")
                                {
                                    //                                    Thread.Sleep(1000);
                                    //                                    string SyncQueueID = await DbEngine.ExecuteScalarAsync("select ISNULL(MAX(SyncQueueId),0) + 1 from BUSDTA.SyncQueueProcess");
                                    //                                    string query = @"INSERT INTO BUSDTA.SyncQueueProcess (SyncQueueId,ReqType,SyncStartTime,SyncEndTime,SyncStatus,CreatedBy,CreatedDateTime,UpdatedBy,UpdatedDateTime,BlockUI) 
                                    //                                                values (" + Convert.ToInt32(SyncQueueID) + "," + queueID + ",GETDATE(),null,'Initiated','" + Environment.UserName + "',GETDATE(),null,null," + "(select Top 1 ISNULL(BlockUI,1) from busdta.SyncQueueActivityMapping where TRIM(QueueName)='" + reqType + "')" + ")";
                                    //                                    int result = await DbEngine.ExecuteNonQueryAsync(query, null, false);

                                    string query = @"Begin Declare SyncQueueID numeric(8,0) = (select ISNULL(MAX(SyncQueueId),0) + 1 from BUSDTA.SyncQueueProcess);
 INSERT INTO BUSDTA.SyncQueueProcess (SyncQueueId,ReqType,SyncStartTime,SyncEndTime,SyncStatus,CreatedBy,CreatedDateTime,UpdatedBy,UpdatedDateTime,BlockUI) 
 values (SyncQueueID," + queueID + ",GETDATE(),null,'Initiated','" + Environment.UserName + "',GETDATE(),null,null,(select Top 1 ISNULL(BlockUI,1) from busdta.SyncQueueActivityMapping where TRIM(QueueName)='" + reqType + "')); select SyncQueueID; end;";


                                    //string SyncQueueID = await DbEngine.ExecuteScalarAsync("CALL BUSDTA.SP_InsertQueueProcess(" + queueID + ",'" + Environment.UserName + "','" + reqType + "')");

                                    string SyncQueueID = DbEngine.ExecuteScalar(query);
                                    if (!string.IsNullOrEmpty(SyncQueueID))
                                    {
                                        int lineID = 1;
                                        foreach (var detail in queueList)
                                        {
                                            string detailQuery = @"INSERT INTO BUSDTA.SyncQueueProcessDetail (SyncQueueId,SyncLineId,SyncProfileID,SyncStartTime,SyncEndTime,SyncStatus,FailCount)
                                                values (" + SyncQueueID + "," + lineID + "," + detail.ProfileID + ",GETDATE(),null,'Initiated',0)";
                                            int result1 = DbEngine.ExecuteNonQuery(detailQuery, null, false);
                                            lineID++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][AddQueueRequestDetails][ExceptionStackTrace = " + ex.StackTrace + "][Message = ]" + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:AddQueueRequestDetails]");
        }

        private List<SyncQueueDetails> getAllSyncQueueDetails()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:getAllSyncQueueDetails]");
            List<SyncQueueDetails> syncQueueDetails = new List<SyncQueueDetails>();
            try
            {
                //Commented on 12/10/2016
                //                string query = @"select qp.SyncQueueId, qm.QueueMasterID, Trim(qm.QueueName) as QueueName, pm.ProfileID, Trim(pm.ProfileName) as ProfileName, Trim(pm.PublicationName) as PublicationName, 
                //       Trim(qpd.SyncStatus) as SyncStatus, ISNULL(qpd.FailCount,0) as FailCount , 
                //ISNULL(qam.BlockUI,1) as BlockUI, ISNULL(qam.SyncDirection,'Both') as SyncDirection
                //                                from 
                //BUSDTA.SyncProfileMaster pm , 
                //BUSDTA.SyncQueueMaster qm join
                //BUSDTA.SyncQueueProcess qp on qm.QueueMasterID = qp.ReqType join
                //BUSDTA.SyncQueueProcessDetail qpd  on qp.SyncQueueId = qpd.SyncQueueId
                //left join BUSDTA.SyncQueueActivityMapping qam on qam.QueueName=qm.QueueName
                //                                where qp.SyncQueueId=qpd.SyncQueueId and qp.ReqType=qm.QueueMasterID and pm.ProfileID=qpd.SyncProfileID 
                //                                and Trim(qpd.SyncStatus) in ('Initiated','SyncFailed','ServerNotRechable') order by qp.SyncQueueId";
                string query = "CALL BUSDTA.SP_GetAllSyncQueueDetails()";
                syncQueueDetails = DbEngine.ExecuteDataSet(query).GetEntityList<SyncQueueDetails>();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][getAllSyncQueueDetails][ExceptionStackTrace = " + ex.StackTrace + "][Message = ]" + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:getAllSyncQueueDetails]");

            return syncQueueDetails;
        }

        private List<TransactionSyncDetail> populateQueueByType(string PublicationName)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:PopulateQueueByType]");
            List<TransactionSyncDetail> result = new List<TransactionSyncDetail>();
            try
            {
                //Commented On 12/10/2016 
                //                string query = @"select TableName from busdta.SyncTableMethod where InsertCount<>InsertCountPre or UpdateCount<>UpdateCountPre or DeleteCount<>DeleteCountPre 
                //and TableName in (select c.table_name as TableName from SYS.SYSPUBLICATION a, SYS.SYSARTICLE b, sys.SYSTABLE c 
                //where a.publication_id=b.publication_id and  b.table_id=c.table_id and a.publication_name='" + PublicationName + @"')
                //order by TableName";
                string query = "CALL BUSDTA.SP_GetPopulateQueueByType(@publicationname='" + PublicationName + "')";
                result = DbEngine.ExecuteDataSet(query).GetEntityList<TransactionSyncDetail>();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][PopulateQueueByType][ExceptionStackTrace = " + ex.StackTrace + "][Message = ]" + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:PopulateQueueByType]");
            return result;
        }

        //Notify Sync manager to sync based on queue type
        private void NotifyQueueChange(string ProfileName, List<TransactionSyncDetail> queueItems, SyncDownloadType syncDownloadType, bool IsManualSync)
        {
            // Raising event for change
            SyncQueueChangedEventArgs args = new SyncQueueChangedEventArgs();
            args.QueueItems = queueItems;
            args.syncDownloadtype = syncDownloadType;
            args.StateChangedTime = DateTime.Now;
            args.profileName = ProfileName;
            args.IsManualSync = IsManualSync;
            OnQueueStateChange(args);
        }

        protected virtual void OnQueueStateChange(SyncQueueChangedEventArgs e)
        {
            EventHandler<SyncQueueChangedEventArgs> handler = QueueChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }


        void OnSyncStateChange(object sender, SyncUpdatedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:SyncProgressChanged][SyncManager.SyncUpdatedEventArgs=" + e.SerializeToJson() + "]");
            try
            {
                if (e.State == SyncUpdateType.SyncFailed || e.State == SyncUpdateType.ServerNotReachable)
                {
                    //  Popout error that sync didnot happened
                    ViewModels.CommonNavInfo.SyncMessage = Helpers.Constants.Common.SyncFailedMessage;
                    UpdateQueueProcess(e.ProfileName, e.State.ToString());
                    return;
                }
                if (e.State == SyncUpdateType.DownloadComplete)
                {
                    ViewModels.CommonNavInfo.SyncMessage = string.Empty;
                    UpdateQueueProcess(e.ProfileName, e.State.ToString());
                    if (e.Queue != null)
                    {
                        foreach (Domain.TransactionSyncDetail syncActivity in e.Queue)
                        {
                            UpdateQueue(syncActivity, e.StateChangedTime);
                        }
                    }
                    if (e.Queue == null && e.State == SyncUpdateType.DownloadComplete)
                    {
                        // Case of manual sync, where we are  processing all queue's hence queue is passed as null
                        UpdateQueue(null, e.StateChangedTime);
                    }
                    ViewModels.CommonNavInfo.SetLastSynched(DateTime.Now.ToString("M'/'dd'/'yyyy' 'hh:mm tt"));
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][SyncProgressChanged][SyncManager.SyncUpdatedEventArgs=" + e.SerializeToJson() + "][ExceptionStackTrace = " + ex.StackTrace + "][Message = ]" + ex.Message);
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:SyncProgressChanged][SyncManager.SyncUpdatedEventArgs=" + e.SerializeToJson() + "]");
        }

        private void UpdateQueue(TransactionSyncDetail syncItem, DateTime updateTimeStamp)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:UpdateQueue]");
            try
            {
                string query = string.Empty;
                if (syncItem == null)
                {
                    query = "update BUSDTA.SyncTableMethod set insertCount=0, InsertCOuntPre=0,UpdateCount=0,UpdateCountPre=0,DeleteCount=0,DeleteCountPre=0";
                }
                else
                {
                    query = "update BUSDTA.SyncTableMethod set insertCount=0, InsertCOuntPre=0,UpdateCount=0,UpdateCountPre=0,DeleteCount=0,DeleteCountPre=0 where TableName='" + syncItem.TableName + "'";
                }
                DbEngine.ExecuteNonQuery(query, null, false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][UpdateQueue][syncItem=" + syncItem + "][updateTimeStamp=" + updateTimeStamp.ToString() + "][ExceptionStackTrace = " + ex.StackTrace + "][Message = ]" + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:UpdateQueue]");
        }

        private void UpdateQueueProcess(string profileName, string syncStatus)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:UpdateQueueProcess]");

                string query = @"update BUSDTA.SyncQueueProcess set SyncEndTime=GETDATE(), SyncStatus='" + syncStatus + "', UpdatedBy='" + Environment.UserName + @"', UpdatedDateTime=GETDATE()
                                    where SyncQueueId IN (select sqpd.SyncQueueID from busdta.syncqueueprocessdetail sqpd where Trim(sqpd.SyncStatus) IN ('Initiated','SyncFailed','ServerNotRechable') and SyncProfileID in 
(select spm.ProfileID from busdta.syncprofilemaster spm where TRIM(spm.ProfileName)='" + profileName + "'))";

                int result = DbEngine.ExecuteNonQuery(query, null, false);

                if (syncStatus != "SyncFailed")
                {
                    string query1 = "update BUSDTA.SyncQueueProcessDetail set SyncEndTime=GETDATE(), SyncStatus='" + syncStatus + "' where SyncQueueId IN (select sqpd.SyncQueueID from busdta.syncqueueprocessdetail sqpd where Trim(sqpd.SyncStatus) IN ('Initiated','SyncFailed','ServerNotRechable') and SyncProfileID in " +
" (select spm.ProfileID from busdta.syncprofilemaster spm where TRIM(spm.ProfileName)='" + profileName + "'))";
                    int result1 = DbEngine.ExecuteNonQuery(query1, null, false);
                }
                else if (syncStatus == "SyncFailed")
                {
                    string query1 = "update BUSDTA.SyncQueueProcessDetail set SyncEndTime=GETDATE(), SyncStatus='" + syncStatus + "', FailCount=FailCount+1 where SyncQueueId IN (select sqpd.SyncQueueID from busdta.syncqueueprocessdetail sqpd where Trim(sqpd.SyncStatus) IN ('Initiated','SyncFailed','ServerNotRechable') and SyncProfileID in " +
" (select spm.ProfileID from busdta.syncprofilemaster spm where TRIM(spm.ProfileName)='" + profileName + "'))";
                    int result1 = DbEngine.ExecuteNonQuery(query1, null, false);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][UpdateQueueProcess][ExceptionStackTrace = " + ex.StackTrace + "][Message = ]" + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:UpdateQueueProcess]");
        }



        private Timer QueueTimer
        {
            get;
            set;
        }


        //List<SyncQueueDetails> allQueue = new List<SyncQueueDetails>();
        //private List<SyncQueueDetails> AllQueue
        //{
        //    get
        //    {
        //        return allQueue;
        //    }
        //    set
        //    {
        //        allQueue = value;
        //    }
        //}

        //internal ObservableCollection<SyncQueueItem> GetPendingSyncItems()
        //{
        //    Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:GetPendingSyncItems]");
        //    ObservableCollection<SyncQueueItem> queueitems = new ObservableCollection<SyncQueueItem>();
        //    try
        //    {
        //        string query = "select count(SDKEY) as Count, SDKEY as Transaction from busdta.M50052 where SDISYNCD=0 group by SDKEY";
        //        System.Collections.Generic.List<SyncQueueItem> queueItems = DbEngine.ExecuteDataSet(query).GetEntityList<SyncQueueItem>();
        //        queueitems = new ObservableCollection<SyncQueueItem>(queueItems);
        //        if (queueitems.Count != (PriorityQueue.Count + ImmediateQueue.Count + NormalQueue.Count))
        //        {
        //            Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][GetPendingSyncItems][Exception = InMemory and Db Queue count dont match..]");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][GetPendingSyncItems][ExceptionStackTrace = " + ex.Message + "]");
        //        return queueitems;
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:GetPendingSyncItems]");
        //    return queueitems;
        //}

        //************************************************************************************************
        // Comment: Dispose Implementation for Peformance Improvement
        // Created: Nov 03, 2016
        // Author: TechUnison (Velmani Karnan)
        //*************************************************************************************************

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }


    public static class IdleTimeDetector
    {
        [DllImport("user32.dll")]
        static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);

        public static IdleTimeInfo GetIdleTimeInfo()
        {
            int systemUptime = Environment.TickCount,
                lastInputTicks = 0,
                idleTicks = 0;

            LASTINPUTINFO lastInputInfo = new LASTINPUTINFO();
            lastInputInfo.cbSize = (uint)Marshal.SizeOf(lastInputInfo);
            lastInputInfo.dwTime = 0;

            if (GetLastInputInfo(ref lastInputInfo))
            {
                lastInputTicks = (int)lastInputInfo.dwTime;
                idleTicks = systemUptime - lastInputTicks;
            }
            return new IdleTimeInfo
            {
                LastInputTime = DateTime.Now.AddMilliseconds(-1 * idleTicks),
                IdleTime = new TimeSpan(0, 0, 0, 0, idleTicks),
                SystemUptimeMilliseconds = systemUptime,
            };
        }
    }

    public class IdleTimeInfo
    {
        public DateTime LastInputTime { get; internal set; }
        public TimeSpan IdleTime { get; internal set; }
        public int SystemUptimeMilliseconds { get; internal set; }
    }
    internal struct LASTINPUTINFO
    {
        public uint cbSize;
        public uint dwTime;
    }
}
