﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Ink;

namespace SalesLogicExpress.Application.Managers
{
    public class SignatureManager:IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.SignatureManager");

        public Int32 UpdateSignature(Flow lifeCycleFlow, SignType signType, int idToUpdate, byte[] signbyte, bool saveCanvasAsImage, string imageName)
        {

            Logger.Info("[SalesLogicExpress.Application.Manager][SignatureManager][Start:UpdateSignature]");
            try
            {
                string query = "";

                if ((lifeCycleFlow == Flow.OrderLifeCycle && signType == SignType.OrderSign) || (lifeCycleFlow == Flow.OrderReturn && signType == SignType.ReturnOrderSign))
                {
                    query = "Update busdta.ORDER_HEADER set OrderSignature=? where orderid=" + idToUpdate.ToString();
                }
                else if (lifeCycleFlow == Flow.OrderLifeCycle && signType == SignType.ChargeOnAccountSign)
                {
                    query = "Update busdta.ORDER_HEADER set ChargeOnAccountSignature=? where orderid=" + idToUpdate.ToString();
                }
                else if (lifeCycleFlow == Flow.Credits && signType == SignType.CreditProcessSign)
                {
                    //query = "Update busdta.Receipt_Header set CustomerSignature=? where ReceiptID=" + idToUpdate.ToString();
                    query = "Update BUSDTA.ORDER_HEADER set OrderSignature=? where OrderID=" + idToUpdate.ToString();
                }
                else if (lifeCycleFlow == Flow.UserSetup && signType == SignType.UserSetupSign)
                {
                    query = "Update BUSDTA.UserProfile_Detail set UserSignature=? where App_User_Id=" + idToUpdate.ToString();
                }
                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(query);
                Sap.Data.SQLAnywhere.SAParameter parms = new Sap.Data.SQLAnywhere.SAParameter();
                parms.ParameterName = "SignatureParam";
                parms.SADbType = Sap.Data.SQLAnywhere.SADbType.LongBinary;
                parms.Value = signbyte;
                command.Parameters.Add(parms);
                new DB_CRUD().InsertUpdateData(command);

                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);

                if (lifeCycleFlow == Flow.UserSetup && signType == SignType.UserSetupSign)
                    ResourceManager.QueueManager.QueueProcess("SystemCfg", false);

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Manager][SignatureManager][UpdateSignature][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Manager][SignatureManager][End:UpdateSignature]");
            return 1;

        }

        public StrokeCollection GetSignature(Flow lifeCycleFlow, SignType signType, int idToGet)
        {
            Logger.Info("[SalesLogicExpress.Application.Manager][SignatureManager][Start:GetSignature]");
            StrokeCollection SignStrokeCollection = new StrokeCollection();
            try
            {
                byte[] signbytes = null;
                string query = "";

                string flowCode = string.Empty;

                if ((lifeCycleFlow == Flow.OrderLifeCycle && signType == SignType.OrderSign) || (lifeCycleFlow == Flow.OrderReturn && signType == SignType.ReturnOrderSign))
                {
                    if (lifeCycleFlow == Flow.OrderLifeCycle && signType == SignType.OrderSign)

                        flowCode = "OrderSign";

                    else
                        flowCode = "ReturnOrderSign";


                    //query = "select OrderSignature from busdta.ORDER_HEADER where orderid=" + idToGet.ToString();
                }
                else if (lifeCycleFlow == Flow.OrderLifeCycle && signType == SignType.ChargeOnAccountSign)
                {
                    flowCode = "ChargeOnAccountSign";

                    //query = "select ChargeOnAccountSignature from busdta.ORDER_HEADER where orderid=" + idToGet.ToString();
                }
                else if (lifeCycleFlow == Flow.Credits && signType == SignType.CreditProcessSign)
                {
                    flowCode = "CreditProcessSign";

                    //query = "select OrderSignature from busdta.Order_Header where OrderID=" + idToGet.ToString();
                }

                //else if (lifeCycleFlow == Flow.OrderReturn && signType == SignType.ReturnOrderSign)
                //{
                //    flowCode = "ReturnOrderSign";
                //    //query = "select OrderSignature from busdta.ORDER_HEADER where orderid=" + idToGet.ToString();
                //}

                else if (lifeCycleFlow == Flow.PreTripInspection && signType == SignType.PreTripInspection)
                {
                    flowCode = "PreTripInspection";

                    //query = "select VerSignature from busdta.PreTrip_Inspection_Header where PreTripInspectionHeaderId=" + idToGet.ToString();
                }
                else if (lifeCycleFlow == Flow.UserSetup && signType == SignType.UserSetupSign)
                {
                    flowCode = "UserSetupSign";
                    //query = "select UserSignature from busdta.UserProfile_Detail where App_User_id=" + idToGet.ToString();
                }

                query = "CALL BUSDTA.SP_GetSignature(@signtype='" + flowCode + "', @orderId='" + idToGet.ToString() + "')";

                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(query);

                using (Sap.Data.SQLAnywhere.SADataReader sdr = new DB_CRUD().ExecuteReader(command))
                {
                    if (sdr.Read())
                    {
                        signbytes = string.IsNullOrEmpty(sdr.GetValue(0).ToString().Trim()) ? null : (byte[])sdr.GetValue(0);
                    }
                    //sdr.Close();
                }

                if (signbytes != null)
                {
                    using (MemoryStream ms = new MemoryStream(signbytes))
                    {
                        SignStrokeCollection = new StrokeCollection(ms);
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Manager][SignatureManager][GetSignature][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Manager][SignatureManager][End:GetSignature]");

            return SignStrokeCollection;

        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
