﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System.Data;
using System.Globalization;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Application.ViewModelPayload;

namespace SalesLogicExpress.Application.Managers
{
    class CreditProcessManager:IDisposable
    {
        OrderManager objOrderManger = new OrderManager();
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.CycleCountManager");
        ReasonCodeManager objReasonCodeManager = new ReasonCodeManager();
        public ObservableCollection<CreditProcess> GetCreditProcessList(string customerNo)
        {
            ObservableCollection<CreditProcess> creditProcessList = new ObservableCollection<CreditProcess>();
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:GetCreditProcessList]");

            try
            {
                //string query = "select rh.CustShipToId CustomerId, rh.RouteId, rh.ReceiptID, rh.ReceiptNumber, rh.ReasonCodeId,rh.StatusId, rh.CreditReasonCodeId, " +
                //               " rh.CreditMemoNote, rh.TransactionAmount, rh.SettelmentId, s.StatusTypeCD, rh.UpdatedDatetime, ";
                //query = query + " rc.ReasonCodeDescription from BUSDTA.Receipt_Header rh join BUSDTA.ReasonCodeMaster rc on rh.CreditReasonCodeId = rc.ReasonCodeId";                
                //query = query + " join BUSDTA.Status_Type s on s.StatusTypeID = rh.StatusId";
                //query = query + " where CustShipToId = '" + customerNo + "' and RouteId = '" + CommonNavInfo.RouteID + "'";

                //StringBuilder MySQLQuery = new StringBuilder();
                //MySQLQuery.Append("	SELECT oh.CustShipToId CustomerId,oh.OriginatingRouteID RouteId,oh.OrderID ReceiptID,oh.OrderID ReceiptNumber,	");
                //MySQLQuery.Append("	rc.ReasonCodeId,oh.OrderStateId StatusId, oh.CreditReasonCodeId, oh.CreditMemoNote,	");
                //MySQLQuery.Append("	 oh.InvoiceTotalAmt TransactionAmount, 0 SettelmentId,st.StatusTypeCD, oh.UpdatedDatetime,  rc.ReasonCodeDescription, oh.DocumentId	");
                //MySQLQuery.Append("	FROM BUSDTA.Order_Header oh LEFT JOIN BUSDTA.ReasonCodeMaster rc ON oh.CreditReasonCodeId = rc.ReasonCodeId ");
                //MySQLQuery.Append(" JOIN BUSDTA.Status_Type st ON st.StatusTypeID = oh.OrderStateId	");
                //MySQLQuery.Append("	WHERE oh.CustShipToId = '" + customerNo + "' AND oh.OriginatingRouteID = '" + CommonNavInfo.RouteID + "'	");
                //MySQLQuery.Append(" AND oh.CreditReasonCodeId > 0 ");

                //DataSet result = Helpers.DbEngine.ExecuteDataSet(MySQLQuery.ToString());
                DataSet result = Helpers.DbEngine.ExecuteDataSet("call busdta.SP_GetCreditProcessList(" + customerNo + "," + CommonNavInfo.RouteID + ")");
                if (result.HasData())
                {
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        DataRow row = result.Tables[0].Rows[i];
                        CreditProcess creditProcess = new CreditProcess();
                        creditProcess.CustomerNo = row["CustomerId"].ToString().Trim();
                        creditProcess.DocumentId = string.IsNullOrEmpty(row["DocumentId"].ToString()) ? 0 : Convert.ToDecimal(row["DocumentId"].ToString());
                        creditProcess.RouteID = row["RouteId"].ToString().Trim();
                        creditProcess.ReceiptID = Convert.ToInt32(row["ReceiptID"].ToString().Trim());
                        creditProcess.ReceiptNumber = Convert.ToInt32(row["ReceiptNumber"]);
                        creditProcess.CreditAmount = Convert.ToDecimal(row["TransactionAmount"]).ToString("F2");
                        creditProcess.CreditReasonID = row["CreditReasonCodeId"] != null ? row["CreditReasonCodeId"].ToString() : null;
                        creditProcess.CreditReason = row["ReasonCodeDescription"].ToString().Trim();
                        creditProcess.VoidReasonID = row["ReasonCodeId"].ToString().Trim();
                        creditProcess.CreditNote = row["CreditMemoNote"].ToString().Trim();
                        creditProcess.StatusCode = row["StatusTypeCD"].ToString().Trim();
                        creditProcess.RouteSettlementId = string.IsNullOrEmpty(row["SettelmentId"].ToString()) ? string.Empty : row["SettelmentId"].ToString().Trim();
                        creditProcess.StatusID = Convert.ToInt32(row["StatusId"]);
                        if (creditProcess.StatusCode == "USTLD")
                        {
                            creditProcess.CreditSeqID = 1;
                            creditProcess.Status = "UNSETTLED";
                        }
                        else if (creditProcess.StatusCode == "VOID")
                        {
                            creditProcess.CreditSeqID = 2;
                            creditProcess.Status = "VOID";
                        }
                        else if (creditProcess.StatusCode == "STTLD")
                        {
                            creditProcess.CreditSeqID = 3;
                            creditProcess.Status = "SETTLED";
                        }
                        else if (creditProcess.StatusCode == "VDSTLD")
                        {
                            creditProcess.CreditSeqID = 4;
                            creditProcess.Status = "VOIDED / SETTLED";
                        }
                        else
                        {
                            Int32 VoidTTID = Convert.ToInt32(Helpers.DbEngine.ExecuteScalar("Call BUSDTA.SP_GetTransactionID (@Transactionkey='VoidCreditMemo')"));
                            if (VoidTTID == creditProcess.StatusID)
                            {
                                creditProcess.CreditSeqID = 2;
                                creditProcess.Status = "VOID";
                            }
                        }
                        creditProcess.CreditMemoDate = Convert.ToDateTime((row["UpdatedDatetime"]).ToString().Trim());
                        creditProcess.CustomerName = PayloadManager.OrderPayload.Customer.Name.Trim();
                        creditProcessList.Add(creditProcess);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CreditProcessManager][GetCreditProcessList][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][End:GetCreditProcessList]");
           
            return creditProcessList;
        }

        public int GetStatusId(string code)
        {
            int statusID = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][GetStatusId][Start:AddCreditMemo]");

            try
            {
                //statusID = Convert.ToInt32(DbEngine.ExecuteScalar(string.Format("select StatusTypeID FROM busdta.Status_Type where StatusTypeCD = 'USTLD'")));
                statusID = Convert.ToInt32(DbEngine.ExecuteScalar(string.Format("CALL BUSDTA.SP_GetStatusId()")));
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][GetStatusId][AddCreditMemo][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");

                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][GetStatusId][End:AddCreditMemo]");

            return statusID;
        }

        // New Logic Added for Credit Memo

        //public void AddCreditMemo(CreditProcess creditObj)
        //{
        //    Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:AddCreditMemo]");
        //    try
        //    {
        //        string query = "insert into BUSDTA.Receipt_Header(CustShipToID, CustBilltoID, RouteId, ReceiptID, ReceiptNumber, ReceiptDate, TransactionAmount, PaymentMode, ReasonCodeId, ";
        //        query = query + " CreditReasonCodeId, CreditMemoNote, TransactionMode, StatusId, JDEStatusId, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)";
        //        query = query + " values ('" + creditObj.CustomerNo + "','" + CustomerManager.GetCustomerBillTo(creditObj.CustomerNo) + "', '" + CommonNavInfo.RouteID + "', '" + creditObj.ReceiptID + "', '" + creditObj.ReceiptNumber + "', getdate(), '" + creditObj.CreditAmount + "', ";
        //        query = query + " '1', '" + creditObj.VoidReasonID + "', '" + creditObj.CreditReasonID + "', ";
        //        query = query + " '" + creditObj.CreditNote + "', '1', '" + creditObj.StatusID + "', ";
        //        query = query + " (select StatusTypeId from busdta.Status_Type where StatusTypeCD = 'INMOBL'), ";
        //        query = query + " '" + UserManager.UserId + "', getdate(), '" + UserManager.UserId + "', getdate())";

        //        int result = Helpers.DbEngine.ExecuteNonQuery(query);

        //        CollectionManager collectionManager = new CollectionManager();
        //        collectionManager.InsertReceiptInCustomerLedger(CommonNavInfo.RouteID.ToString(), creditObj.ReceiptNumber, CommonNavInfo.Customer.CustomerNo, CustomerManager.GetCustomerBillTo(CommonNavInfo.Customer.customerNo), Convert.ToDecimal(creditObj.CreditAmount));
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][CreditProcessManager][AddCreditMemo][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
        //    }

        //    Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][End:AddCreditMemo]");

        //}




        //************************************************************************************************
        // Comment: Credit Memo Implementation & GetBranch Function
        // Created: May 23, 2016
        // Author: TechUnison (Velmani Karnan)
        // Issue ID:  Credit Memo Process & Table Changes
        //*************************************************************************************************

        public void AddCreditMemoNew(CreditProcess objCredit)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:AddCreditMemoNew]");
            try
            {
                //StringBuilder MyQuery;
                //int TransactionResult = 0;
                decimal ConvertedCreditAmount = Convert.ToDecimal(objCredit.CreditAmount);
                ConvertedCreditAmount = -Math.Abs(ConvertedCreditAmount);

                //  string StatusTypeID = DbEngine.ExecuteScalar("SELECT StatusTypeId FROM busdta.Status_Type WHERE StatusTypeCD = 'INMOBL'");
                string StatusTypeID = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetCreditMemoStatus('INMOBL')");
                CustomerManager cm = new CustomerManager();
                DataTable dt = cm.GetCustomerDetailsForOrder(objCredit.CustomerNo);

                // Order Header Table
                //MyQuery = new StringBuilder();
                //MyQuery.Append(" INSERT INTO BUSDTA.Order_Header(OrderID,OrderTypeId,OriginatingRouteID,Branch,CustShipToId,CustBillToId,CustParentId, ");
                //MyQuery.Append(" TotalCoffeeAmt,TotalAlliedAmt,OrderTotalAmt,SalesTaxAmt,EnergySurchargeAmt,VarianceAmt,SurchargeReasonCodeId,JDEProcessID,SettlementID, ");
                //MyQuery.Append(" JDEOrderNumber,JDEOrderCompany,JDEOrderType,JDEInvoiceCompany,JDEOInvoiceType,JDEZBatchNumber,PaymentTerms,CustomerReference1,CustomerReference2,");
                //MyQuery.Append(" AdjustmentSchedule,TaxExplanationCode,TaxArea,ChargeOnAccount,HoldCommitted, ");
                //MyQuery.Append(" OrderDate,CreditReasonCodeId, CreditMemoNote,InvoiceTotalAmt,OrderStateId,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime) ");
                //MyQuery.Append(" VALUES('" + objCredit.ReceiptID + "','" + StatusTypeID + "','" + CommonNavInfo.RouteID + "','" + GetBranch(ViewModels.CommonNavInfo.UserBranch) + "','" + objCredit.CustomerNo + "','" + CustomerManager.GetCustomerBillTo(CommonNavInfo.Customer.customerNo) + "','" + objCredit.CustomerNo + "', ");
                //MyQuery.Append(" 0,0,0,0,0,0,0,0,0,");
                //MyQuery.Append(" '" + objCredit.ReceiptID + "','00800','C5','00800','CN','','N07','','','" + dt.Rows[0]["AdjustmentSchedule"].ToString() + "','" + dt.Rows[0]["TaxExplanationCode"].ToString() + "','" + dt.Rows[0]["Taxarea"].ToString() + "','','', ");
                //MyQuery.Append(" GETDATE(), '" + objCredit.CreditReasonID + "','" + objCredit.CreditNote + "','" + ConvertedCreditAmount + "','" + objCredit.StatusID + "','" + UserManager.UserId + "',GETDATE(),'" + UserManager.UserId + "',GETDATE())");
                //TransactionResult = DbEngine.ExecuteNonQuery(MyQuery.ToString());

                objOrderManger.CreditProcess_AddCreditMemoNew(objCredit.ReceiptID, StatusTypeID, objCredit.CustomerNo, dt.Rows[0]["AdjustmentSchedule"].ToString(), dt.Rows[0]["TaxExplanationCode"].ToString(), dt.Rows[0]["Taxarea"].ToString(), objCredit.CreditReasonID, objCredit.CreditNote, ConvertedCreditAmount, objCredit.StatusID);

                // F4101
                //MyQuery = new StringBuilder();
                //MyQuery.Append(" SELECT * FROM BUSDTA.F4101 WHERE ");
                //MyQuery.Append(" IMLITM='930007' ");
                ////MyQuery.Append(" AND IMLITM='CUSTOMER_REQUEST' ");

                //DataTable tblItemDetails = DbEngine.ExecuteDataSet(MyQuery.ToString()).Tables[0];
                //if (tblItemDetails.Rows.Count > 0)
                //{
                //    // Order Detail Table
                //    MyQuery = new StringBuilder();
                //    MyQuery.Append(" INSERT INTO BUSDTA.Order_Detail(OrderID,LineID,ItemId,LongItem,LineType,IsNonStock,OrderQty,OrderUM,OrderQtyInPricingUM, ");
                //    MyQuery.Append(" UnitPriceOriginalAmtInPriceUM,UnitPriceAmtInPriceUoM,UnitPriceAmt,ExtnPriceAmt, ");
                //    MyQuery.Append(" PriceOverriden,IsTaxable,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime) ");
                //    MyQuery.Append(" VALUES('" + objCredit.ReceiptID + "',1,'" + tblItemDetails.Rows[0]["IMLITM"].ToString() + "', '" + tblItemDetails.Rows[0]["IMLITM"].ToString() + "', ");
                //    MyQuery.Append(" '" + tblItemDetails.Rows[0]["IMLNTY"].ToString() + "',0,-1, '" + tblItemDetails.Rows[0]["IMUOM1"].ToString() + "',-1, ");
                //    MyQuery.Append(" '" + ConvertedCreditAmount + "','" + ConvertedCreditAmount + "','" + ConvertedCreditAmount + "','" + ConvertedCreditAmount + "',1,0, ");
                //    MyQuery.Append(" '" + CommonNavInfo.RouteID + "',GETDATE(),'" + CommonNavInfo.RouteID + "',GETDATE()) ");
                //    TransactionResult = DbEngine.ExecuteNonQuery(MyQuery.ToString());
                //}

                string CreditOrderItem = "930007";
                //MyQuery = new StringBuilder();
                //MyQuery.Append(" Select IMLITM LongItm,IBLNTY, IBSRP1 SalesCat1 ,IBSRP2 SalesCat2,IBSRP3 SalesCat3,IBSRP4 SalesCat4,IBSRP5 SalesCat5,IBPRP1 PurchasingCat1, ");
                //MyQuery.Append(" IBPRP2 PurchasingCat2,IBPRP3 PurchasingCat3,IBPRP4 PurchasingCat4,IMUOM1,IMLNTY, ");
                //MyQuery.Append(" IBPRP5 PurchasingCat5,stktyp.LFIVI, CASE stktyp.LFIVI WHEN 'Y' THEN 0 ELSE 1 END IsNonStock ");
                //MyQuery.Append(" from Busdta.F4101 ItmA join Busdta.F4102 ItmB on  ItmA.IMLITM = ItmB.IBLITM join Busdta.F40205 stktyp on ItmB.IBLNTY = stktyp.LFLNTY  ");
                //MyQuery.Append(" Where IMLITM = '" + CreditOrderItem + "' ");
                //DataTable vOrderItemdetails = DbEngine.ExecuteDataSet(MyQuery.ToString()).Tables[0];

                DataTable vOrderItemdetails = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetCreditMemoOrdItemDetail('" + CreditOrderItem + "')").Tables[0];
                var isTaxable = 0;
                string PriceOverrideFlag = "1";
                string JDEBranch = "00800";

                objOrderManger.CreditProcess_AddCreditMemoNewDetails(objCredit.ReceiptID, vOrderItemdetails, objCredit.CustomerNo, CreditOrderItem, JDEBranch, isTaxable, PriceOverrideFlag, objCredit.CreditReasonID, ConvertedCreditAmount);

                //                DbEngine.ExecuteNonQuery(@"insert into BUSDTA.Order_Detail(
                //                        OrderID,LineID,JDEOrderLine,CustShipToID,ItemId,LongItem,LineType,
                //                        IsNonStock
                //                        ,SalesCat1,SalesCat2 ,SalesCat3 ,SalesCat4 ,SalesCat5 ,PurchasingCat1 ,PurchasingCat2 ,PurchasingCat3 ,PurchasingCat4 ,PurchasingCat5 ,
                //                        JDEOrderNumber,JDEOrderCompany,JDEOrderType,JDEInvoiceCompany,JDEOInvoiceType,JDEZBatchNumber,
                //                        JDEProcessID,SettlementID,ReturnHeldQty,ExtendedAmtVariance,TaxAmountAmtVariance,HoldCode,
                //                        OrderQty,OrderQtyInPricingUM,OrderUM,PricingUM,UnitPriceAmtInPriceUoM,UnitPriceOriginalAmtInPriceUM ,UnitPriceAmt ,ExtnPriceAmt,ItemSalesTaxAmt,
                //                        IsTaxable,PriceOverriden,PriceOverrideReasonCodeId, CreatedBy,UpdatedBy,CreatedDatetime,UpdatedDatetime
                //                        )values
                //                        (
                //                        '" + objCredit.ReceiptID + @"'," +
                //                    "'" + 1 + "'," +
                //                    "'" + 1 + "'," +
                //                    "'" + objCredit.CustomerNo + "'" +
                //                    "," + CreditOrderItem + @",'" +
                //                    vOrderItemdetails.Rows[0]["LongItm"] + "'" +
                //                    ",'" + vOrderItemdetails.Rows[0]["IBLNTY"] + "'" +
                //                    ",'" + vOrderItemdetails.Rows[0]["IsNonStock"] + "'" +
                //                    ",'" + vOrderItemdetails.Rows[0]["SalesCat1"] + "'" +
                //                    ",'" + vOrderItemdetails.Rows[0]["SalesCat2"] + "'" +
                //                    ",'" + vOrderItemdetails.Rows[0]["SalesCat3"] + "'" +
                //                    ",'" + vOrderItemdetails.Rows[0]["SalesCat4"] + "'" +
                //                    ",'" + vOrderItemdetails.Rows[0]["SalesCat5"] + "'" +
                //                    ",'" + vOrderItemdetails.Rows[0]["PurchasingCat1"] + "'" +
                //                    ",'" + vOrderItemdetails.Rows[0]["PurchasingCat2"] + "'" +
                //                    ",'" + vOrderItemdetails.Rows[0]["PurchasingCat3"] + "'" +
                //                    ",'" + vOrderItemdetails.Rows[0]["PurchasingCat4"] + "'" +
                //                    ",'" + vOrderItemdetails.Rows[0]["PurchasingCat5"] + "'" +
                //                    ",'" + objCredit.ReceiptID + "'" +
                //                    ",'" + JDEBranch + "'" +
                //                    ",'C5'" +
                //                    ",'" + JDEBranch + "'" +
                //                    ",'CN'" +
                //                    ",''" +
                //                    ",''" +
                //                    ",'0'" +
                //                    ",'0'" +
                //                    ",'0'" +
                //                    ",'0'" +
                //                    ",''" +
                //                    "," + -1 + @"" +
                //                    "," + -1 + @"," +
                //                    "'" + vOrderItemdetails.Rows[0]["IMUOM1"] + @"'," +
                //                    "'" + vOrderItemdetails.Rows[0]["IMUOM1"] + @"'," +
                //                    "'" + ConvertedCreditAmount + @"'," +
                //                    "'" + ConvertedCreditAmount + @"',
                //                        " + ConvertedCreditAmount + @",
                //                        " + ConvertedCreditAmount + @",
                //                        " + 0 + @",         
                //                        " + isTaxable + @",
                //                        '" + PriceOverrideFlag + @"',
                //                        " + objCredit.CreditReasonID + @",    
                //                        " + Managers.UserManager.UserId + @",   
                //                        " + Managers.UserManager.UserId + @",                        
                //                        now(),now()  
                //                        );
                //                        ");

                // Invoice_Header
                //string strOpenStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='OPEN'");
                //string strUnknownStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='UNKNOWN'");
                string strOpenStatusId = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetCreditMemoStatus('OPEN')");
                string strUnknownStatusId = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetCreditMemoStatus('UNKNOWN')");
                int InvoiceId = 0;
                OrderManager objOrderMgr = new OrderManager();
                Int32 JDEInvoiceNumber = objOrderMgr.GetInvoiceNextNumber();
                if (JDEInvoiceNumber > 0)
                {
                    //string IsInvoiceExists = DbEngine.ExecuteScalar("select TRIM(ISNULL(DeviceInvoiceNumber,'')) DeviceInvoiceNumber from BUSDTA.Invoice_Header where DeviceInvoiceNumber=" + JDEInvoiceNumber);
                    string IsInvoiceExists = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetCreditMemoStatus(" + JDEInvoiceNumber + ")");
                    if (string.IsNullOrEmpty(IsInvoiceExists))
                    {
                        string MyQueryStr = string.Empty;
                        MyQueryStr = " Insert into BUSDTA.Invoice_Header (InvoiceID,InvoicePaymentType,DeviceStatusID,ARStatusID,OrderId,RouteId,DeviceInvoiceNumber,ARInvoiceNumber,CustShipToId,CustBillToId,DeviceInvoiceDate,GrossAmt,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime) "
                              + " values ('" + JDEInvoiceNumber + "','CR Memo'," + strOpenStatusId + "," + strUnknownStatusId + "," + objCredit.ReceiptNumber + "," +
                              " " + CommonNavInfo.RouteID + "," + JDEInvoiceNumber + "," + JDEInvoiceNumber + "," + objCredit.CustomerNo + "," +
                              " " + CustomerManager.GetCustomerBillTo(CommonNavInfo.Customer.customerNo) + ",'" + objCredit.CreditMemoDate.ToString("yyyy-MM-dd") + "'," + ConvertedCreditAmount.ToString() + ",'" + UserManager.UserId + "',GETDATE(),'" + UserManager.UserId + "',GETDATE())";

                        int TranStatus = DbEngine.ExecuteNonQuery(MyQueryStr);
                        ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
                        //TranStatus = DbEngine.ExecuteNonQuery("update Busdta.Invoice_Header set DeviceInvoiceNumber=" + JDEInvoiceNumber + " , ARInvoiceNumber=" + JDEInvoiceNumber + ",UpdatedBy='" + UserManager.UserId + "',UpdatedDatetime=GETDATE() where InvoiceId=" + JDEInvoiceNumber + " AND DeviceInvoiceDate='" + objCredit.CreditMemoDate.ToString("yyyy-MM-dd") + "'");

                        // Customer_Ledger_R1
                        InvoiceId = JDEInvoiceNumber;
                        string ssql = "insert into BUSDTA.Customer_Ledger_R1 ([ARDocumentId],[ARDocumentType],[ARPayItem],[ARCompany],[TransactionTypeID],[SourceID],[CustBillToId] " +
                                       " ,[CustShipToId],[GrossAmt],[OpenAmt],[AdjustmentAmt],[VarianceAmt],[InvoiceDate],[DueDate],[SourceDocID],[SourceDocType],[Archive],CreatedBy,CreatedDatetime) " +
                                       " (select InvoiceID as [ARDocumentId],'CN' as [ARDocumentType] ,'001' as [ARPayItem],'00800' as [ARCompany] " +
                                       " ,1 as [TransactionTypeID],RouteID as [SourceID],[CustBillToId],[CustShipToId],[GrossAmt],GrossAmt as [OpenAmt] " +
                                       " ,0 as [AdjustmentAmt],0 as [VarianceAmt],DeviceInvoiceDate as [InvoiceDate],NULL [DueDate],OrderID as [SourceDocID] " +
                                       " ,'C5' as [SourceDocType],0 as [Archive], '" + UserManager.UserId + "' as CreatedBy, GETDATE() AS CreatedDatetime from BUSDTA.Invoice_Header where CustShipToId=" + objCredit.CustomerNo + " and InvoiceID=" + InvoiceId + ") ";

                        TranStatus = DbEngine.ExecuteNonQuery(ssql);
                        new InvoiceManager().UpdateInvoiceNumber(objCredit.ReceiptNumber.ToString());

                        ResourceManager.QueueManager.QueueProcess("ARPayments", false);
                    }
                    else
                    {
                        // Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, Please sync to continue" }, MessageToken);                        
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CreditProcessManager][AddCreditMemoNew][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][End:AddCreditMemoNew]");
        }

        // Get Branch Function - Base On JDE

        //public string GetBranch(string branch)
        //{
        //    string BrnchNo = branch;
        //    for (int i = BrnchNo.Length; i <= 11; i++)
        //    {
        //        BrnchNo = " " + BrnchNo;
        //    }
        //    return BrnchNo;
        //}

        // Update the status
        //public void UpdateCreditMemoStatus(int receiptNo, string customerNo, string routeID)
        //{
        //    Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:UpdateCreditMemoStatus]");

        //    try
        //    {
        //        string query = "update BUSDTA.Receipt_Header set StatusId = (select StatusTypeID FROM busdta.Status_Type where StatusTypeCD = '" + StatusTypesEnum.VOID + "'), ";
        //        query = query + " UpdatedBy = '" + UserManager.UserId + "', UpdatedDatetime= getdate()";
        //        query = query + " where CustShipToId = '" + customerNo + "' AND RouteId = '" + routeID + "' and ReceiptID = '" + receiptNo + "'";

        //        int result = Helpers.DbEngine.ExecuteNonQuery(query);

        //        ResourceManager.QueueManager.QueueProcess("ARPayments", false);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][CreditProcessManager][UpdateCreditMemoStatus][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
        //    }

        //    Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][End:UpdateCreditMemoStatus]");

        //}

        public int GetNewCreditMemoNumber()
        {
            int OrderNumber = 0;
            try
            {
                string val = new Managers.NumberManager().GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.CreditMemo);
                OrderNumber = Convert.ToInt32(val);
            }
            catch (Exception ex)
            {
                OrderNumber = -1;
                Logger.Error("[SalesLogicExpress.Application.Managers][CreditProcessManager][GetNewCreditMemoNumber][RouteID=" + CommonNavInfo.RouteID.ToString() + "][ForEntity=" + Managers.NumberManager.Entity.CreditMemo.ToString() + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            return OrderNumber;
        }

        // Get credit reasons
        public ObservableCollection<ReasonCode> GetCreditReasonCode()
        {
            ObservableCollection<ReasonCode> reasonCodeList = new ObservableCollection<ReasonCode>();
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:GetCreditReasonCode]");

            try
            {
                //DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Credit Memo'").Tables[0];
                DataTable dt = objReasonCodeManager.CreditProcessManager_GetCreditReasonCode();

                ReasonCode ReasonCode = new ReasonCode();

                foreach (DataRow dr in dt.Rows)
                {
                    ReasonCode = new ReasonCode();
                    ReasonCode.Id = Convert.ToInt32(dr["Id"].ToString());
                    ReasonCode.Code = dr["ReasonCodeDescription"].ToString();
                    reasonCodeList.Add(ReasonCode);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CreditProcessManager][GetCreditReasonCode " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][End:GetCreditReasonCode]");

            return reasonCodeList;
        }

        // Get void reasons
        public ObservableCollection<ReasonCode> GetVoidReasonCode()
        {
            ObservableCollection<ReasonCode> reasonCodeList = new ObservableCollection<ReasonCode>();
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:GetVoidReasonCode]");

            try
            {
                //DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Void Credit Memo'").Tables[0];

                DataTable dt = objReasonCodeManager.CreditProcessManager_GetVoidReasonCode();
                ReasonCode ReasonCode = new ReasonCode();

                foreach (DataRow dr in dt.Rows)
                {
                    ReasonCode = new ReasonCode();
                    ReasonCode.Id = Convert.ToInt32(dr["Id"].ToString());
                    ReasonCode.Code = dr["ReasonCodeDescription"].ToString();
                    reasonCodeList.Add(ReasonCode);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CreditProcessManager][GetVoidReasonCode " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][End:GetVoidReasonCode]");

            return reasonCodeList;
        }
        // Code written by Marimuthu for Performance change
        // Dispose implementation for Performance optimization
        // Date : 11/03/2016
        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
        //public int GetNewReceiptNumber()
        //{
        //    int ReceiptNum = 0;
        //    Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:GetNewReceiptNumber]");

        //    try
        //    {
        //        string val = new Managers.NumberManager().GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.Receipt);
        //        ReceiptNum = Convert.ToInt32(val);

        //        //int updateStopActivityFlag = DbEngine.ExecuteNonQuery("update busdta.m56m0004 set rpactid=null where rpstid = " + CommonNavInfo.Customer.StopID + " and rpan8=" + CommonNavInfo.Customer.CustomerNo + "");
        //        //int updatePreOrderFlag = DbEngine.ExecuteNonQuery("update BUSDTA.M4016 SET POSTFG = '1' where POAN8 = '" + CommonNavInfo.Customer.CustomerNo + "' and POSTDT = '" + CommonNavInfo.Customer.StopDate.Value.ToString("yyyy-MM-dd") + "'");
        //    }
        //    catch (Exception ex)
        //    {
        //        ReceiptNum = -1;
        //        Logger.Error("[SalesLogicExpress.Application.Managers][CreditProcessManager][GetNewReceiptNumber][RouteID=" + CommonNavInfo.RouteID.ToString() + "][ForEntity=" + Managers.NumberManager.Entity.SalesOrder.ToString() + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][End:GetNewReceiptNumber]");

        //    return ReceiptNum;
        //}
    }
}
