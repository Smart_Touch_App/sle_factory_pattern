﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using SalesLogicExpress.Application.ViewModelPayload;

namespace SalesLogicExpress.Application.Managers
{
    public class CustomerDashboardManager : IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.CustomerDashboardManager");    // CA1051 Do not declare visible instance fields  --Vignesh
       

        public string GetPaymentDetails(string customerID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][Start:GetPaymentDetails][customerID = " + customerID + "]");
            string paymentDetails = string.Empty;
            try
            {
                //string query = "select aitrar, PNPTD from busdta.F03012, busdta.f0014";
                //query = query + " where AIAN8 = '" + customerID + "' and pnptc = aitrar";

                //string Handling for Memory Optimization --Vignesh D
                string query = string.Empty;
                 query = "CALL BUSDTA.SP_GetPaymentDetails(@customerID='" + customerID + "')";

                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                query = string.Empty;
                if (result.HasData())
                {
                    DataRow row;
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        row = result.Tables[0].Rows[i];
                        paymentDetails = row["aitrar"].ToString().Trim() + "-" + row["PNPTD"].ToString().Trim();
                    }
                }

                //Dataset Disposing for Memory Optimization --Vignesh D
                result.Dispose();
                result = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][GetPaymentDetails][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][End:GetPaymentDetails][customerID = " + customerID + "]");
            return paymentDetails;
        }

        public Dictionary<int, string> GetNoSaleReason()
        {
            Dictionary<int, string> noSaleReasonList = new Dictionary<int, string>();
            try
            {
                //string query = "select ReasonCodeId, ReasonCode, rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription from busdta.ReasonCodeMaster";
                //query = query + " where ReasonCodeType = 'No Sale Reason'";

                //DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                ReasonCodeManager objReasonCodeManager = new ReasonCodeManager();
                DataSet result = objReasonCodeManager.CustomerDashboardManager_GetNoSaleReason();
                objReasonCodeManager = null;
                string reasonDescription = string.Empty;
                if (result.HasData())
                {
                    DataRow row;
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        row = result.Tables[0].Rows[i];
                        int reasonCodeID = Convert.ToInt32(row["ReasonCodeId"]);
                        //   string reasonCode = row["ReasonCode"].ToString();      // Unused Local   --Vignesh D
                         reasonDescription = row["ReasonCodeDescription"].ToString();

                        noSaleReasonList.Add(reasonCodeID, reasonDescription);
                    }
                }
                //Dataset Dispose for Memory Optimization --Vignesh D
                reasonDescription = string.Empty;
                result.Dispose();
                result = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][GetPaymentDetails " + ex.StackTrace + "]");
            }
            return noSaleReasonList;
        }

        public int UpdateNoSaleActivity(int noSaleReasonID, string stopID, string customerID, string routeId, string noSalereason, string stopDate)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][Start:UpdateNoSaleActivity][noSaleReason = " + noSaleReasonID + "]");
            int result = -1;
            try
            {
                //Check wether cust is allready no sale or not
                //ikf it is alraedy no sale, then dont update RPUPDT, else update RPUPDT. this logic is for Daily stop sequencing
                //string queryToCheckNoSale = "SELECT COUNT(*) FROM busdta.M56M0004 WHERE RPSTID='{0}' AND RPACTID = 'NOSALE'  ";
                string queryToCheckNoSale = "CALL BUSDTA.SP_GetNoSaleActivity(@StopID='{0}')";
                queryToCheckNoSale = string.Format(queryToCheckNoSale, stopID);
                int count = Convert.ToInt32(DbEngine.ExecuteScalar(queryToCheckNoSale));
                bool IsExistFlag = false;

                Activity ac = new Activity
                {
                    CustomerID = customerID,
                    RouteID = routeId,
                    ActivityType = ActivityKey.NoActivity.ToString(),
                    ActivityStart = DateTime.Now,
                    IsTxActivity = true,
                    ActivityDetailClass = "NoActivity",
                    ActivityDetails = noSalereason,// PayloadManager.OrderPayload.SerializeToJson(),
                    ActivityEnd = DateTime.Now,
                    ActivityStatus = "NoActivity",
                    StopInstanceID = stopID
                };

                //string query = "update busdta.M56M0004  set RPRCID = {0} , RPACTID = 'NOSALE',RPUPDT=GETDATE() where RPSTID = '{1}' AND RPAN8 ='{2}'";
                if (count > 0)
                {
                    //query = "update busdta.M56M0004  set RPRCID = {0} , RPACTID = 'NOSALE' where RPSTID = '{1}' AND RPAN8 ='{2}'";
                    IsExistFlag = true;
                    UpdateActivityNoSale(ac);
                }
                else
                {
                    LogActivityNoSale(ac);
                    new CustomerManager().UpdateSequenceNo(stopID, stopDate);
                    new CustomerManager().UpdateActivityCount(stopID, false, true);
                }

                //query = string.Format(query, noSaleReasonID, stopID, customerID);
                //result = Helpers.DbEngine.ExecuteNonQuery(query);

                // Call Common Function
                StopManager objStopMgr = new StopManager();
                result = objStopMgr.UpdateCustomerNoSaleActivity(noSaleReasonID, stopID, customerID, IsExistFlag);
                objStopMgr = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][UpdateNoSaleActivity][noSaleReason=" + noSaleReasonID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][End:UpdateNoSaleActivity][noSaleReason = " + noSaleReasonID + "]");

            return result;
        }

        public DateTime? GetRescheduledDateForFuture(string customerID, DateTime selectedDate)
        {
            //throw new NotImplementedException();
            DateTime rescheduleDate = new DateTime();
            try
            {
                //string query = "SELECT RPSTDT as 'ResceduledDate' FROM BUSDTA.M56M0004 WHERE RPSTID = (SELECT RPRSTID FROM BUSDTA.M56M0004 WHERE RPAN8= '" + customerID + "'";
                //query = query + " AND RPSTDT='" + selectedDate.ToString("yyyy-MM-dd") + "' AND RPSTTP = 'Moved')";
                string query = "CALL BUSDTA.SP_GetRescheduledDateForFuture(@customerID='" + customerID + "',@selectedDate='" + selectedDate.ToString("yyyy-MM-dd") + "')";

                try
                {
                    rescheduleDate = Convert.ToDateTime(Helpers.DbEngine.ExecuteScalar(query));
                }
                catch
                {
                    rescheduleDate = DateTime.Parse(Helpers.DbEngine.ExecuteScalar(query), CultureInfo.CreateSpecificCulture("en-US"));
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][GetRescheduledDateForFuture][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return rescheduleDate;
        }

        public int GetNoSaleReasonForNoActivity(string stopID, string customerID)
        {
            int result = -1;
            try
            {
                //string query = "select RPRCID from busdta.M56M0004 where RPSTID = '" + stopID + "' AND RPAN8 ='" + customerID + "'";
                string query = "CALL BUSDTA.SP_GetStatusForNoActivity (@customerID='" + customerID + "',@stopID='" + stopID + "')";

                // result = Convert.ToInt32(string.IsNullOrEmpty( Helpers.DbEngine.ExecuteScalar(query).ToString())  ? "0" : Helpers.DbEngine.ExecuteScalar(query).ToString());
                string _result = Helpers.DbEngine.ExecuteScalar(query).ToString();
                result = Convert.ToInt32(string.IsNullOrEmpty(_result) ? "0" : _result);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][GetNoSaleReasonForNoActivity][stopID=" + stopID + "][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }

        public string GetStatusUpdateForNoActivity(string stopID, string customerID)
        {
            string result = string.Empty;
            try
            {
                //string query = "select RPACTID from busdta.M56M0004 where RPSTID = '" + stopID + "' and RPAN8 ='" + customerID + "'";
                string query = "CALL BUSDTA.SP_GetStatusForNoActivity (@customerID='" + customerID + "',@stopID='" + stopID + "')";

                result = Helpers.DbEngine.ExecuteScalar(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][GetStatusUpdateForNoActivity][stopID=" + stopID + "][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            return result;
        }
        public void LogActivityNoSale(Activity ac)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][Start:LogActivityNoSale]");
            try
            {


                //Activity a = ResourceManager.Transaction.LogActivity(ac);
                //string query = "select GET_IDENTITY('busdta.M50012')";
                string query = string.Empty; //"CALL BUSDTA.SP_GetIdentity (@TableName='busdta.M50012')";
                //string activityID = DbEngine.ExecuteScalar(query).ToString();

                string NxtNumber = new Managers.NumberManager().GetNextNumberForEntity(SalesLogicExpress.Application.ViewModels.CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.TRANSACTID);

                if (!string.IsNullOrEmpty(NxtNumber))
                {
                    query = "INSERT INTO busdta.M50012(TDID,TDROUT,TDTYP,TDCLASS,TDDTLS,TDSTRTTM,TDENDTM,TDSTID,TDAN8,TDSTTLID,TDPNTID,TDSTAT)VALUES (";
                    query = query + NxtNumber + ",'";
                    query = query + SalesLogicExpress.Application.ViewModelPayload.PayloadManager.ApplicationPayload.Route + "','";
                    query = query + ac.ActivityType + "','";
                    query = query + ac.ActivityDetailClass + "',";
                    query = query + "?,";
                    //query = query + Activity.ActivityDetails + "',";
                    query = query + ac.ActivityStart.GetFormattedDateTimeForDb() + ",";
                    query = query + ac.ActivityEnd.GetFormattedDateTimeForDb() + ",'";
                    query = query + ac.StopInstanceID + "','";
                    query = query + ac.CustomerID + "','";
                    query = query + ac.SettlementID + "','";
                    query = query + ac.ActivityHeaderID + "'";
                    query = query + ",'" + ac.ActivityStatus + "'";
                    query = query + ");";

                    // used SACommand for inserting blob, inserting blobs needs to done using parameters
                    Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(query);
                    Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();
                    parm.SADbType = Sap.Data.SQLAnywhere.SADbType.LongNVarchar;
                    command.Parameters.Add(parm);
                    command.Parameters[0].Value = ac.ActivityDetails;
                    new DB_CRUD().InsertUpdateData(command);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][LogActivityNoSale][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][Start:LogActivityNoSale]");

        }
        void UpdateActivityNoSale(Activity ac)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][Start:UpdateActivityNoSale]");
            //Activity a = ResourceManager.Transaction.LogActivity(ac);
            try
            {
                string queryToUpdate = "UPDATE busdta.M50012 set  " +
                                        " TDENDTM=" + ac.ActivityEnd.GetFormattedDateTimeForDb() + ", " +
                                        " TDDTLS=" + "?" +
                                        " where TDAN8 ='" + ac.CustomerID.Trim() + "' AND TDSTAT='NOACTIVITY' AND TDSTID='" + ac.StopInstanceID + "'";
                // used SACommand for inserting blob, inserting blobs needs to done using parameters
                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(queryToUpdate);
                Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.LongNVarchar;
                command.Parameters.Add(parm);
                command.Parameters[0].Value = ac.ActivityDetails;
                new DB_CRUD().InsertUpdateData(command);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][UpdateActivityNoSale][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][End:UpdateActivityNoSale]");
        }

        //public void Dispose()
        //{
        //    // throw new NotImplementedException();
        //}

        public int GetStopCountOfCustomer(string deliveryCode)
        {
            int result = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][Start:GetStopCountOfCustomer]");
            try
            {
                //string query = "select count(1) from BUSDTA.M56M0002 where DCDDC = '" + deliveryCode + "'";
                // String Handling - Optimization (CA1820) for Memroy Optimization  By Vignesh D
                string query = string.Empty;
                 query = "CALL BUSDTA.SP_GetStopCountOfCustomer(@DeliveryCode='" + deliveryCode + "')";
                result = Convert.ToInt32(Helpers.DbEngine.ExecuteScalar(query));
                query = string.Empty;

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][GetStopCountOfCustomer][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][End:GetStopCountOfCustomer]");
            return result;
        }

        public string GetStopIDForCust(string custNo, string stopDate)
        {
            string stopID = string.Empty;
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][Start:GetStopIDForCust]");
            try
            {
                //string query = "SELECT TOP 1 RPSTID FROM BUSDTA.M56M0004 WHERE RPAN8 ='"+custNo+"' and RPSTDT ='"+stopDate+"' and RPSTTP <> 'Moved'";
                string query = "CALL BUSDTA.SP_GetStopIDForCustomer(@CustomerNo='" + custNo + "',@stopDate='" + stopDate + "')";
                stopID = DbEngine.ExecuteScalar(query).ToString();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][GetStopIDForCust][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][End:GetStopIDForCust]");
            return stopID;
        }

        public int GetPreOrderInformationForCustomer(string customerNo, DateTime stopDate)
        {
            int result = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][Start:GetPreOrderInformationForCustomer]");
            try
            {
                // string query = "select count(1) from BUSDTA.M4016 where POAN8 = '" + customerNo + "' and POSTDT ='" + stopDate.ToString("yyyy-MM-dd") + "' and ISNULL(POSTFG,0) = '0'";

                //string Handling for Memory Optimization --Vignesh D
                string query = string.Empty;
                query = "CALL BUSDTA.SP_GetStopIDForCustomer(@CustomerNo='" + customerNo + "',@stopDate='" + stopDate.ToString("yyyy-MM-dd") + "')";
                result = Convert.ToInt32(Helpers.DbEngine.ExecuteScalar(query));
                query = string.Empty;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][GetPreOrderInformationForCustomer][ExceptionStackTrace = " + ex.StackTrace + "]");
                result = 0;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][End:GetPreOrderInformationForCustomer]");
            return result;
        }


        //Disposable Implementation --Vignesh D
        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {

            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
               
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
