﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using log4net;
using System.Collections.ObjectModel;
using System.Data;
using System.ComponentModel;
using SalesLogicExpress.Application.ViewModels;
namespace SalesLogicExpress.Application.Managers
{
    public class NotificationManager : IDisposable
    {
        public enum NotificationType
        {
            [Description("26")]
            ActionInformation,
            [Description("27")]
            ActionWarning,
            [Description("28")]
            ActionOptional,
            [Description("29")]
            ActionMandatory
        }

        private readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);        
        public TrulyObservableCollection<Notification> GetNotifications()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][NotificationManager][Start:GetNotifications]");
            TrulyObservableCollection<Notification> result = new TrulyObservableCollection<Notification>();
            try
            {
                string query = string.Empty;
              //  query = "select * from busdta.Notification where Read='{0}'";
                query = "call BUSDTA.SP_GetNotifications()";
                query = string.Format(query, 'N');
                DataSet dbResult = DbEngine.ExecuteDataSet(query);
                if (dbResult.HasData())
                {
                    result = new TrulyObservableCollection<Notification>(dbResult.GetEntityList<Notification>());
                    foreach (Notification item in result)
                    {
                        item.NotificationAge = item.CreatedDateTime.GetPrettyDate();
                        if (item.TypeID == Managers.NotificationManager.NotificationType.ActionInformation.GetEnumDescription())
                        {
                            item.Indicator = "I";
                        }
                        if (item.TypeID == Managers.NotificationManager.NotificationType.ActionMandatory.GetEnumDescription())
                        {
                            item.Indicator = "M";
                        }
                        if (item.TypeID == Managers.NotificationManager.NotificationType.ActionOptional.GetEnumDescription())
                        {
                            item.Indicator = "O";
                        }
                        if (item.TypeID == Managers.NotificationManager.NotificationType.ActionWarning.GetEnumDescription())
                        {
                            item.Indicator = "W";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][NotificationManager][GetNotifications][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][NotificationManager][End:GetNotifications]");
            return result;
        }

        public TrulyObservableCollection<MasterNotification> GetAdjustmentNotifications()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][NotificationManager][Start:GetNotifications]");
            TrulyObservableCollection<MasterNotification> result = new TrulyObservableCollection<MasterNotification>();
            try
            {
                string query = string.Empty;
               // query = "select MN.* from busdta.MasterNotification MN inner join Busdta.M5001 MS on MN.Category = MS.TTID  where Read='{0}'";
                query = "CALL BUSDTA.SP_GetAdjustmentNotifications()";
                query = string.Format(query, 'N');
                DataSet dbResult = DbEngine.ExecuteDataSet(query);
                if (dbResult.HasData())
                {
                    result = new TrulyObservableCollection<MasterNotification>(dbResult.GetEntityList<MasterNotification>());
                    foreach (MasterNotification item in result)
                    {
                        item.NotificationAge = item.CreatedDateTime.GetPrettyDate();

                        item.Indicator = "I";
                        if (item.TypeID == Managers.NotificationManager.NotificationType.ActionInformation.GetEnumDescription())
                        {
                            item.Indicator = "I";
                        }
                        if (item.TypeID == Managers.NotificationManager.NotificationType.ActionMandatory.GetEnumDescription())
                        {
                            item.Indicator = "M";
                        }
                        if (item.TypeID == Managers.NotificationManager.NotificationType.ActionOptional.GetEnumDescription())
                        {
                            item.Indicator = "O";
                        }
                        if (item.TypeID == Managers.NotificationManager.NotificationType.ActionWarning.GetEnumDescription())
                        {
                            item.Indicator = "W";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][NotificationManager][GetNotifications][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][NotificationManager][End:GetNotifications]");
            return result;
        }

        public bool UpdateRejectedAdjustments(String TransactionID, string AdjStatus)
        {
            bool returnValue = false;
            InventoryManager objInvMgr = new InventoryManager();
            Logger.Info("[SalesLogicExpress.Application.Managers][NotificationManager][Start:UpdateRejectedAdjustments]");
            try
            {
                string query = string.Empty;
                query = "Update busdta.Inventory_Adjustment set AdjustmentStatus = " + AdjStatus + " where InventoryAdjustmentId='" + TransactionID + "'";
                query = string.Format(query);
                int result = DbEngine.ExecuteNonQuery(query);

                if (result == 1)
                {
                   // DataTable DtInvAdj = DbEngine.ExecuteDataSet("Select top 1 * from Busdta.Inventory_Ledger where TransactionId=" + TransactionID + "order by CreatedDatetime desc").Tables[0];
                    DataTable DtInvAdj = DbEngine.ExecuteDataSet("Call BUSDTA.SP_GetUpdateRejectedAdjustments(" + TransactionID + ")").Tables[0];
                    if (DtInvAdj.Rows.Count > 0)
                    {
                        DtInvAdj.Rows[0]["TransactionQty"] = Convert.ToInt16(DtInvAdj.Rows[0]["TransactionQty"].ToString()) * -1;

                        // Commented Start - 09/14/2016 - Call Common Function
                        //String UpdateInv = "Update Busdta.Inventory Set OnHandQuantity = OnHandQuantity + (" + DtInvAdj.Rows[0]["TransactionQty"] + ") Where ItemNumber = '" + DtInvAdj.Rows[0]["ItemNumber"] + "'";
                        //result = DbEngine.ExecuteNonQuery(UpdateInv);
                        // Commented End

                        // Call Common Function
                        objInvMgr.UpdateOnHandQuantityForNotification(Convert.ToInt32(DtInvAdj.Rows[0]["TransactionQty"]), DtInvAdj.Rows[0]["ItemNumber"].ToString().Trim(), CommonNavInfo.RouteID);

                        string logInvAdj = "INSERT INTO BUSDTA.Inventory_Ledger(ItemId,ItemNumber,RouteId,TransactionQty,TransactionQtyUM,TransactionQtyPrimaryUM,TransactionType,TransactionId,SettlementID,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime)";
                        logInvAdj += "VALUES(" + DtInvAdj.Rows[0]["ItemId"] + ",'" + DtInvAdj.Rows[0]["ItemNumber"] + "'," + CommonNavInfo.RouteID + "," + DtInvAdj.Rows[0]["TransactionQty"] + ",'" + DtInvAdj.Rows[0]["TransactionQtyUM"] + "'," +
                                 "'" + DtInvAdj.Rows[0]["TransactionQtyPrimaryUM"] + "',8," + TransactionID + ",0," + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate())";

                        DbEngine.ExecuteNonQuery(logInvAdj);
                    }
                }


                returnValue = result > 0 ? true : false;
                ResourceManager.QueueManager.QueueProcess("Inventory", false);

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][NotificationManager][UpdateRejectedAdjustments][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][NotificationManager][End:UpdateRejectedAdjustments]");
            return returnValue;
        }

        public bool UpdateCreatedAdjustments(String TransactionID, string AdjStatus)
        {
            bool returnValue = false;
            InventoryManager objInvMgr = new InventoryManager();
            Logger.Info("[SalesLogicExpress.Application.Managers][NotificationManager][Start:UpdateCreatedAdjustments]");

            try
            {
                //string CheckSource = DbEngine.ExecuteScalar("Select Source from  busdta.Inventory_Adjustment where InventoryAdjustmentId='" + TransactionID + "' ");
                string CheckSource = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetSourceAdjustmentId(@TransactionID ='" + TransactionID + "') ");
                if (CheckSource == "2")
                {
                    string query = string.Empty;
                    query = "Update busdta.Inventory_Adjustment set AdjustmentStatus = " + AdjStatus + " where InventoryAdjustmentId='" + TransactionID + "'";
                    query = string.Format(query);
                    int result = DbEngine.ExecuteNonQuery(query);

                    if (result == 1)
                    {
                        //DataTable DtInvAdj = DbEngine.ExecuteDataSet("Select top 1 * from Busdta.Inventory_Ledger where TransactionId=" + TransactionID + "order by CreatedDatetime desc").Tables[0];
                        DataTable DtInvAdj = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetAdjustmentInvLedger(@TransactionID = '" + TransactionID + "') ").Tables[0];
                        
                        if (DtInvAdj.Rows.Count > 0)
                        {
                            // Comment Start - 09/14/2016 - Call Common Function
                            // String UpdateInv = "Update Busdta.Inventory Set OnHandQuantity = OnHandQuantity + (" + DtInvAdj.Rows[0]["TransactionQty"] + ") Where ItemNumber = '" + DtInvAdj.Rows[0]["ItemNumber"] + "'";
                            // result = DbEngine.ExecuteNonQuery(UpdateInv);
                            // Comment End

                            // Call Common Function
                            objInvMgr.UpdateOnHandQuantityForNotification(Convert.ToInt32(DtInvAdj.Rows[0]["TransactionQty"]), DtInvAdj.Rows[0]["ItemNumber"].ToString().Trim(), CommonNavInfo.RouteID);

                            string logInvAdj = "INSERT INTO BUSDTA.Inventory_Ledger(ItemId,ItemNumber,RouteId,TransactionQty,TransactionQtyUM,TransactionQtyPrimaryUM,TransactionType,TransactionId,SettlementID,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime)";
                            logInvAdj += "VALUES(" + DtInvAdj.Rows[0]["ItemId"] + ",'" + DtInvAdj.Rows[0]["ItemNumber"] + "'," + CommonNavInfo.RouteID + "," + DtInvAdj.Rows[0]["TransactionQty"] + ",'" + DtInvAdj.Rows[0]["TransactionQtyUM"] + "'," +
                                     "'" + DtInvAdj.Rows[0]["TransactionQtyPrimaryUM"] + "',8," + TransactionID + ",0," + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate())";
                            result = DbEngine.ExecuteNonQuery(logInvAdj);

                        }
                        else
                        {
                            //DtInvAdj = DbEngine.ExecuteDataSet("Select top 1 * from Busdta.Inventory_Adjustment where InventoryAdjustmentID=" + TransactionID).Tables[0];
                            DtInvAdj = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetInvAdjustment(@TransactionID = '" + TransactionID + "') ").Tables[0];

                            // Comment Start - 09/14/2016 - Call Common Function
                            //String UpdateInv = "Update Busdta.Inventory Set OnHandQuantity = OnHandQuantity + (" + DtInvAdj.Rows[0]["TransactionQty"] + ") Where ItemNumber = '" + DtInvAdj.Rows[0]["ItemNumber"] + "'";
                            //result = DbEngine.ExecuteNonQuery(UpdateInv);
                            // Comment End

                            // Call Common Function
                            objInvMgr.UpdateOnHandQuantityForNotification(Convert.ToInt32(DtInvAdj.Rows[0]["TransactionQty"]), DtInvAdj.Rows[0]["ItemNumber"].ToString().Trim(), CommonNavInfo.RouteID);

                            string logInvAdj = "INSERT INTO BUSDTA.Inventory_Ledger(ItemId,ItemNumber,RouteId,TransactionQty,TransactionQtyUM,TransactionQtyPrimaryUM,TransactionType,TransactionId,SettlementID,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime)";
                            logInvAdj += "VALUES(" + DtInvAdj.Rows[0]["ItemId"] + ",'" + DtInvAdj.Rows[0]["ItemNumber"] + "'," + CommonNavInfo.RouteID + "," + DtInvAdj.Rows[0]["TransactionQty"] + ",'" + DtInvAdj.Rows[0]["TransactionQtyUM"] + "'," +
                                     "'" + DtInvAdj.Rows[0]["TransactionQtyPrimaryUM"] + "',8," + TransactionID + ",0," + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate())";
                            result = DbEngine.ExecuteNonQuery(logInvAdj);
                        }
                    }
                    returnValue = result > 0 ? true : false;

                    ResourceManager.QueueManager.QueueProcess("Inventory", false);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][NotificationManager][UpdateCreatedAdjustments][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][NotificationManager][End:UpdateCreatedAdjustments]");

            return returnValue;
        }
        //public bool MarkNotificationAsRead(Notification UpdatedNotification)
        //{
        //    bool returnValue = false;
        //    Logger.Info("[SalesLogicExpress.Application.Managers][NotificationManager][Start:MarkNotificationAsRead]");
        //    try
        //    {
        //        string query = string.Empty;
        //        query = "Update busdta.Notification set Read='{0}' where NotificationID='{1}'";
        //        query = string.Format(query, "Y", UpdatedNotification.NotificationID);
        //        int result = DbEngine.ExecuteNonQuery(query);
        //        returnValue = result > 0 ? true : false;
        //        ResourceManager.QueueManager.QueueProcess("Notification", false);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][NotificationManager][MarkNotificationAsRead][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][NotificationManager][End:MarkNotificationAsRead]");
        //    return returnValue;
        //}
        public bool MarkMasterNotificationAsRead(MasterNotification UpdatedNotification)
        {
            bool returnValue = false;
            Logger.Info("[SalesLogicExpress.Application.Managers][NotificationManager][Start:MarkNotificationAsRead]");
            try
            {
                string query = string.Empty;
                query = "Update busdta.MasterNotification set Read='Y' where MasterNotificationID='" + UpdatedNotification.MasterNotificationID + "'";
                int result = DbEngine.ExecuteNonQuery(query);
                returnValue = result > 0 ? true : false;
                ResourceManager.QueueManager.QueueProcess("Notification", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][NotificationManager][MarkNotificationAsRead][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][NotificationManager][End:MarkNotificationAsRead]");
            return returnValue;
        }

        //************************************************************************************************
        // Comment: Dispose Implementation for Peformance Improvement
        // Created: Nov 03, 2016
        // Author: TechUnison (Velmani Karnan)
        //*************************************************************************************************

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

    }
}
