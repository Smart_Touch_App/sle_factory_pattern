﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using log4net;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Application.Helpers;
using System.Data;
using SalesLogicExpress.Domain.Prospect_Models;
using SalesLogicExpress.Application.ViewModelPayload;

namespace SalesLogicExpress.Application.Managers.ProspectManagers
{
    class NotesManager :IDisposable
    {

        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.ProspectManagers");
        // Make the previous default note as non-default.
        // Insert the new note as default on dash-board
        #region AddNewProspectNote
        public void AddNewProspectNote(Domain.Prospect_Models.ProspectNoteModel noteProspect)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][NotesManager][Start:AddNewProspectNote][Note=" + noteProspect + "]");
            try
            {
                //int result = -1;//Remove Unused Locals CA1804 Vignesh.S
                string query = null;
                string updQuery = string.Empty;
                updQuery = "update busdta.Prospect_Note set IsDefault = 0 where ProspectId = '" + noteProspect.ProspectID + "'";
                //int updResult = Helpers.DbEngine.ExecuteNonQuery(updQuery);//Remove Unused Locals CA1804
                Helpers.DbEngine.ExecuteNonQuery(updQuery);
                query = "insert into busdta.Prospect_Note (prospectId, ProspectNoteId, RouteId, ProspectNoteType, ProspectNoteDetail, IsDefault,  CreatedBy, CreatedDateTime, UpdatedBy, UpdatedDateTime)";
                query = query + " values ('" + noteProspect.ProspectID + "', (select isnull(max(ProspectNoteId),0)+1 from busdta.Prospect_Note), ";
                query = query + " " + CommonNavInfo.RouteID + ", 1, ?,'1', " + "'" + UserManager.UserId + "', ";
                query = query + "getdate(), " + "'" + UserManager.UserId + "', " + "getdate());";

                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(query);
                Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.NVarChar;
                parm.Value = noteProspect.ProspectNoteDetails;
                command.Parameters.Add(parm);
                //result = new DB_CRUD().InsertUpdateData(command);//Remove Unused Locals CA1804 Vignesh.S
                new DB_CRUD().InsertUpdateData(command);
                //Commented On 30/09/2016
                //var enteredBy = DbEngine.ExecuteScalar("select DisplayName from BUSDTA.userprofilemaster where App_user_id =  " + UserManager.UserId + "");
                var enteredBy = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetProspectEnterby(@AppUserID ='" + UserManager.UserId + "')");
                noteProspect.ProspectNoteEnteredBy = enteredBy;
                LogActivity(noteProspect);
                ResourceManager.QueueManager.QueueProcess("ProspectManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers.ProspectManagers][NotesManager][AddNewProspectNote][Note=" + noteProspect + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers.ProspectManagers][NotesManager][End:AddNewProspectNote][Note=" + noteProspect + "]");

        }
        #endregion
        // Select all notes for the prospect to be shown
        #region GetProspectNotes
        public ObservableCollection<ProspectNoteModel> GetProspectNotes(string ProspectId)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][NotesManager][Start:GetProspectNotes][ProspectID = " + ProspectId + "]");
            ObservableCollection<ProspectNoteModel> prospectNoteList = new ObservableCollection<ProspectNoteModel>();
            try
            {
                //Commented on 30/09/2016
                //string query = "select prospectId, ProspectNoteId, RouteId, ProspectNoteType, ProspectNoteDetail, IsDefault, (select DisplayName from BUSDTA.userprofilemaster where App_user_id =  CreatedBy) as CreatedBy, CreatedDateTime, UpdatedBy, UpdatedDateTime";
                //query = query + " from busdta.Prospect_Note ";
                //query = query + " where prospectId ='" + ProspectId + "'";
               //query = query + " order by UpdatedDateTime desc;";
                string query = "CALL BUSDTA.SP_GetProspectNotes(@prospect='" + ProspectId + "')";

                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                ProspectNoteModel noteProspect;
                if (result.HasData())
                {
                    DataRow row;
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        row = result.Tables[0].Rows[i];
                        noteProspect = new ProspectNoteModel();
                        noteProspect.ProspectNoteID = row["ProspectNoteId"].ToString().Trim();
                        noteProspect.ProspectNoteDetails = row["ProspectNoteDetail"].ToString().Trim();
                        noteProspect.IsDefault = string.IsNullOrEmpty(row["IsDefault"].ToString()) ? false : Convert.ToBoolean(Convert.ToInt32(row["IsDefault"].ToString()));
                        noteProspect.ProspectID = row["prospectId"].ToString().Trim();
                        noteProspect.ProspectNoteEnteredBy = row["CreatedBy"].ToString().Trim();
                        DateTime dt = Convert.ToDateTime(row["UpdatedDateTime"]);
                        noteProspect.DateTime = String.Format("{0:MM/dd/yyyy hh:mmtt}", dt).Replace('-', '/').ToLower();
                        prospectNoteList.Add(noteProspect);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerNotesManager][GetCustomerNotes][customerID=" + ProspectId + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerNotesManager][End:GetCustomerNotes][customerID=" + ProspectId + "]");
            return prospectNoteList;
        }
        #endregion
        // If default note functionality is introduced by seleting radio button
        #region SetDefaultNote
        internal void SetDefaultNote(ProspectNoteModel selectedNote)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers.ProspectManagers][NotesManager][Start:SetDefaultNote][selectedNote=" + selectedNote.SerializeToJson() + "]");
            try
            {
                string query = string.Empty;
                query = "update busdta.Prospect_Note set IsDefault = 0 where ProspectId = '" + selectedNote.ProspectID + "'";
                //int result = Helpers.DbEngine.ExecuteNonQuery(query);//Remove Unused Locals CA1804
                Helpers.DbEngine.ExecuteNonQuery(query);
                query = "update busdta.Prospect_Note set IsDefault = 1 where ProspectNoteId = " + selectedNote.ProspectNoteID + "";
                //result = Helpers.DbEngine.ExecuteNonQuery(query);//Remove Unused Locals CA1804
                Helpers.DbEngine.ExecuteNonQuery(query);
                ResourceManager.QueueManager.QueueProcess("ProspectManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers.ProspectManagers][NotesManager][SetDefaultNote][selectedNote=" + selectedNote.SerializeToJson() + "][ExceptionStackTrace = " + ex.InnerException == null ? ex.Message : ex.InnerException.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers.ProspectManagers][NotesManager][End:SetDefaultNote][selectedNote=" + selectedNote.SerializeToJson() + "]");
        }
        #endregion
        // If delete note functinality is introduced
        #region DeleteSelectedNote
        public int DeleteSelectedNote(ProspectNoteModel note)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][NotesManager][Start:DeleteSelectedNote]");
            int result = -1;
            try
            {
                string query = "Delete from busdta.Prospect_Note where ProspectNoteId = " + note.ProspectNoteID + " AND ";
                query = query + "ProspectId = " + note.ProspectID + ";";

                result = Helpers.DbEngine.ExecuteNonQuery(query);
                ResourceManager.QueueManager.QueueProcess("ProspectManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers.ProspectManagers][NotesManager][DeleteSelectedNote][ExceptionStackTrace = " + ex.InnerException == null ? ex.Message : ex.InnerException.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers.ProspectManagers][NotesManager][End:DeleteSelectedNote]");
            return result;
        }
        #endregion
        #region LogActivity
        private void LogActivity(Domain.Prospect_Models.ProspectNoteModel note)
        {
            try
            {

                ProspectNoteListItem noteItem = new ProspectNoteListItem
                {
                    ProspectId = note.ProspectID.ToString(),
                    Date = DateTime.Now,
                    Note = note.ProspectNoteDetails,
                    ProsectNoteEnteredBy = note.ProspectNoteEnteredBy
                };

               
                SalesLogicExpress.Domain.Activity ac = new SalesLogicExpress.Domain.Activity
                    {
                        CustomerID = note.ProspectID.ToString(),
                        RouteID = CommonNavInfo.RouteID.ToString(),
                        StopInstanceID = PayloadManager.ProspectPayload.Prospect.StopID,
                        ActivityType = ActivityKey.ProspectNote.ToString(),
                        ActivityStart = DateTime.Now,
                        ActivityEnd = DateTime.Now,
                        IsTxActivity = false,
                        ActivityDetailClass = typeof(ProspectNoteListItem).FullName
                    };

                ac.ActivityDetails = noteItem.SerializeToJson();

                //SalesLogicExpress.Domain.Activity a = ResourceManager.Transaction.LogActivity(ac) //Remove Unused Locals CA1804;
                ResourceManager.Transaction.LogActivity(ac);

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers.ProspectManagers][NotesManager][LogActivity][ExceptionStackTrace = " + ex.InnerException == null ? ex.Message : ex.InnerException.Message + "]");
            }
        }
        #endregion
        //Add the Disposed function by Vignesh.S
        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
