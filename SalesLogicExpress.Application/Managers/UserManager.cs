﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = SalesLogicExpress.Domain;

namespace SalesLogicExpress.Application.Managers
{
    /// <summary>
    /// Static class for getting logged in user's Route and Branch
    /// </summary>
    public static class UserManager 
    {
        private static readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.CustomerManager");

        private static string _userRoute, _RouteEntered, _userBranch, _userName, _RouteName, _userDisplayName;

        private static int _userId;


        public static string UserRoute
        {
            get
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:UserRoute]");
                try
                {
                    if (string.IsNullOrEmpty(_userRoute))
                    {
                        _userRoute = _RouteEntered.Replace("FBM", "");
                        //_userRoute = DbEngine.ExecuteScalar("select RIGHT(RouteName,3) from busdta.Route_Master where RouteName =" + "'" + _RouteEntered + "'" + ";");
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][UserRoute][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:UserRoute]");
                return _userRoute.Trim();
            }
            set
            {
                _userRoute = value;
            }
        }

        private static string replenishmentToBranch = "101";

        public static string ReplenishmentToBranch
        {
            get { return replenishmentToBranch; }
            set { replenishmentToBranch = value; }
        }
        public static string RouteEntered
        {
            get { return _RouteEntered; }
            set { _RouteEntered = value; }
        }
        public static string RouteName
        {
            get
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:RouteName]");
                try
                {
                    if (string.IsNullOrEmpty(_RouteName))
                    {
                       // _RouteName = DbEngine.ExecuteScalar("select (select DRDL01 from BUSDTA.F0005 where  DRSY = '42' and DRRT = 'RT' and ltrim(DRKY) = '" + UserRoute + "') as 'RouteDescription' from dummy;");
                        _RouteName = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetRouteName('" + UserRoute + "')");
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][RouteName][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:RouteName]");
                return _RouteName.Trim();
            }
            set { _RouteName = value; }
        }
        public static string UserBranch
        {
            get
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:UserBranch]");
                try
                {
                    if (string.IsNullOrEmpty(_userBranch))
                    {
                      //  _userBranch = DbEngine.ExecuteScalar("SELECT BranchNumber from Busdta.Route_MASTER where RouteName =" + "'" + _RouteEntered + "'" + ";");
                        _userBranch = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetUserBranch('" + _RouteEntered + "')");
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][UserBranch][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:UserBranch]");
                return _userBranch.Trim();
            }
        }

        public static string UserDisplayName
        {
            get
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:UserDisplayName]");
                try
                {
                    if (string.IsNullOrEmpty(_userDisplayName))
                    {
                       // _userDisplayName = DbEngine.ExecuteScalar("select DisplayName from busdta.UserProfileMaster where NTUser='" + Environment.UserName + "' and Domain='" + Environment.UserDomainName + "'" + ";");
                        _userDisplayName = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetUserDisplayName('" + Environment.UserName + "','" + Environment.UserDomainName + "')");
                    }

                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][UserDisplayName][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:UserDisplayName]");
                return _userDisplayName.Trim();
            }
        }

        public static string UserName
        {
            get
            {
                return _userName.Trim();
            }
            set
            {
                _userName = value;
            }
        }

        public static int UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }

        private static string repnlBranchType = string.Empty;

        public static string RepnlBranchType
        {
            get { return repnlBranchType; }
            set { repnlBranchType = value; }
        }
        //public static string GetLoginIDofDomainUser(string DomainUserName)
        //{
        //    string query = string.Format("SELECT App_user FROM BUSDTA.user_master WHERE DomainUser='{0}'", DomainUserName);
        //    return Convert.ToString(DbEngine.ExecuteScalar(query));
        //}


        //Arvind
        /// <summary>
        /// domain user in the form 'DomainName\\DomainUserName'
        /// </summary>
        /// <param name="domainUserName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public static int GetUserId(string domainUserName, string Password = "") 
        {
            
                // Logger.Info("[CustomerManager][GetCustomersForRoute][DataInitializationStart:GetUserId]");
                int AppUserId = 0;
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:GetUserId]");
                try
                {
                    // AppUserId = Convert.ToInt32(DbEngine.ExecuteScalar(string.Format(" select App_user_id from SystemDB.BUSDTA.userprofilemaster where Domain = '" + Environment.UserDomainName + "' and NTUser= '" + Environment.UserName + "' ")));
                    string _appuserid = DbEngine.ExecuteScalar(string.Format("CALL BUSDTA.SP_GetUserId('" + Environment.UserDomainName + "','" + Environment.UserName + "') "));
                    AppUserId = string.IsNullOrEmpty(_appuserid) ? 0 : Convert.ToInt32(_appuserid);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetUserId][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:GetUserId]");
                return AppUserId;
            
        }

        public static int GetRouteId(string routeName)
        {
            int RouteId = 0;
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:GetRouteId]");
            try
            {
               // RouteId = Convert.ToInt32(DbEngine.ExecuteScalar(" select RouteMasterID from busdta.Route_Master where routeName ='" + routeName + "' "));
                string query = "CALL BUSDTA.SP_GetRouteId(@routeName = '" + routeName + "')";
                RouteId = Convert.ToInt32(DbEngine.ExecuteScalar(query));
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetRouteId][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:GetRouteId]");
            return RouteId;
        }

        public static string GetRepnBranchID(int routeID)
        {
            string strRouteID = "";
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:GetRepnBranchID]");
            try
            {
               // strRouteID = DbEngine.ExecuteScalar(" select RepnlBranch from busdta.Route_Master where  RouteMasterID= " + routeID + " ");
                strRouteID = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetRepnBranchID(" + routeID + ")");
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetRepnBranchID][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:GetRepnBranchID]");
            return strRouteID;
        }

        public static string GetRepnBranchType(int routeID)
        {
            string strRouteID = "";
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:GetRepnBranchType]");
            try
            {
               // strRouteID = Convert.ToString(DbEngine.ExecuteScalar(" select LTRIM(RTRIM(RepnlBranchType)) from busdta.Route_Master where  RouteMasterID= " + routeID + " "));
                strRouteID = Convert.ToString(DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetRepnBranchType(" + routeID + ")"));
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetRepnBranchType][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:GetRepnBranchType]");
            return strRouteID;
        }

        public static string GetUserSignature(int routeID)
        {
            string strRouteID = "";
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:GetUserSignature]");
            try
            {
               // strRouteID = Convert.ToString(DbEngine.ExecuteScalar(" select UserSignature from BUSDTA.UserProfile_Detail WHERE App_user_id='" + routeID + "'"));
                strRouteID = Convert.ToString(DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetUserSignature(" + routeID + ")"));
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetUserSignature][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:GetUserSignature]");
            return strRouteID;
        }

        public static ObservableCollection<Models.UserProfile> GetUserProfile(int RouteUserID)
        {
            System.Data.DataSet objUserProfile = null;
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:GetUserProfile] RouteUserID=" + RouteUserID);//unused parameters Temp Fixed.
            try
            {
                //StringBuilder MyQuery = new StringBuilder();
                //MyQuery.Append("SELECT up.App_User_ID AppUserID, up.FirstName,up.LastName,up.DisplayName,up.Domain,up.Email EmailAddress,");
                //MyQuery.Append(" up.Phone PhoneNumber,up.NTUser,up.Title SelectedTitle,CASE ud.ScannerMode WHEN 'A' THEN 'Auto' ELSE 'Manual' END SelectedMode ");
                //MyQuery.Append(" FROM BUSDTA.UserProfileMaster up LEFT JOIN BUSDTA.UserProfile_Detail ud ON up.App_User_ID=ud.App_User_Id ");
                //MyQuery.Append(" WHERE Domain='" + Environment.UserDomainName + "' AND NTUser='" + Environment.UserName + "' ");
                //objUserProfile = DbEngine.ExecuteDataSet(MyQuery.ToString(), "UserProfileData");

                string MyQuery = "CALL BUSDTA.SP_GetUserProfile(@UserDomainName='" + Environment.UserDomainName + "',@DomainUserName='" + Environment.UserName + "')";
                objUserProfile = DbEngine.ExecuteDataSet(MyQuery.ToString(), "UserProfileData");
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetUserProfile][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:GetUserProfile]");
            return objUserProfile.GetEntityList<Models.UserProfile>().ToObservableCollection();
        }


        public static List<string> GetUserTitle()
        {
            List<string> lstUserTitle = new List<string>();
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:GetUserTitle]");
            try
            {
               // System.Data.DataSet resultDataSet = DbEngine.ExecuteDataSet("SELECT DISTINCT(RTRIM(Title)) Title FROM BUSDTA.UserProfileMaster WHERE Active=1 AND Title !=''");
                System.Data.DataSet resultDataSet = DbEngine.ExecuteDataSet(" CALL BUSDTA.SP_GetUserTitle()");
                if (resultDataSet.HasData())
                {
                    foreach (System.Data.DataRow dataRow in resultDataSet.Tables[0].Rows)
                    {
                        lstUserTitle.Add(dataRow["Title"].ToString().Trim());
                    }
                }
                resultDataSet.Dispose();
                resultDataSet = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetUserTitle][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:GetUserTitle]");
            return lstUserTitle;
        }

        public static List<string> GetScannerMode()
        {
            List<string> lstScannerMode = new List<string>();
            try
            {
                lstScannerMode.Add("Auto");
                lstScannerMode.Add("Manual");
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetScannerMode][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                throw;
            }
            return lstScannerMode;
        }

        public static int GetProfileScannerMode()
        {
            int SelectedMode = 0; // 0-A | 1-M
            System.Data.DataSet objUserProfile = null;
            StringBuilder MyQuery = new StringBuilder();
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:GetProfileScannerMode]");
            try
            {
                //MyQuery.Append("SELECT ud.ScannerMode FROM BUSDTA.UserProfileMaster up LEFT JOIN BUSDTA.UserProfile_Detail ud ON up.App_User_ID=ud.App_User_Id ");
                //MyQuery.Append(" WHERE Domain='" + Environment.UserDomainName + "' AND NTUser='" + Environment.UserName + "' ");
                MyQuery.Append("CALL BUSDTA.SP_GetProfileScannerMode('" + Environment.UserDomainName + "','" + Environment.UserName + "')");
                objUserProfile = DbEngine.ExecuteDataSet(MyQuery.ToString(), "UserProfileData");

                if (objUserProfile.Tables[0].Rows.Count > 0)
                {
                    string mode = objUserProfile.Tables[0].Rows[0][0].ToString();
                    if (mode == "M")
                        SelectedMode = 1;
                }
                objUserProfile.Dispose();
                objUserProfile = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetProfileScannerMode][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:GetProfileScannerMode]");
            return SelectedMode;
        }
        //Add the Disposed function by Vignesh.S
        //bool disposed = false;
        //System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}
        //protected virtual void Dispose(bool disposing)
        //{
        //    if (disposed)
        //        return;
        //    if (disposing)
        //    {
        //        handle.Dispose();
        //    }
        //    disposed = true;
        //}

    }
}
