﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using SalesLogicExpress.Application.ViewModelPayload;

namespace SalesLogicExpress.Application.Managers
{
    class PreTripInspectionManager: IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.PreTripInspectionManager");

        public PreTripVehicleInspection GetVehicleDetails(string routeName, string vehicleMake)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][Start:GetVehicleDetails]");
            PreTripVehicleInspection vehicleDetails = new PreTripVehicleInspection();
            DataSet result = null;
            try
            {
                string query = string.Empty;
                //string query = "select v.VehicleID, v.VehicleNumber, v.VehicleMake from BUSDTA.Vehicle_Master v ";
                //query = query + " join BUSDTA.Route_Master r on r.VehicleID = v.VehicleID where r.RouteMasterID = '" + CommonNavInfo.RouteID + "'";

                //if (string.IsNullOrEmpty(vehicleMake))
                //{
                //     query = "select VehicleID, VehicleNumber, VehicleMake from BUSDTA.Vehicle_Master";
                //}
                //else
                //    query = "select VehicleID, VehicleNumber from BUSDTA.Vehicle_Master where VehicleMake = '" + vehicleMake + "'";

                query = "CALL BUSDTA.SP_GetVehicleDetails(@VehicleMake = '" + vehicleMake + "')";
                result = Helpers.DbEngine.ExecuteDataSet(query);
                vehicleDetails.VehicleNo = new ObservableCollection<VehicleNumber>();

                if (result.HasData())
                {
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        VehicleNumber obj = new VehicleNumber();

                        DataRow row = result.Tables[0].Rows[i];
                       // vm.MakeID = row["VehicleID"].ToString().Trim();
                        obj.VehicleID = row["VehicleID"].ToString().Trim();
                        obj.VehicleNo = row["VehicleNumber"].ToString().Trim();
                        vehicleDetails.VehicleNo.Add(obj);
                        if (string.IsNullOrEmpty(vehicleMake))
                        {
                            VehicleMake vm = new VehicleMake();
                            vm.Make = row["VehicleMake"].ToString().Trim();
                            vehicleDetails.VehicleMake.Add(vm);
                            vehicleDetails.VehicleModel = row["VehicleMake"].ToString().Trim();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][GetVehicleDetails][RouteName=" + routeName + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][End:GetVehicleDetails][RouteName=" + routeName + "]");
            return vehicleDetails;

        }

        public ObservableCollection<PreTripInspectionFormList> GetInspectionList()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][Start:GetInspectionList]");
            ObservableCollection<PreTripInspectionFormList> formList = new ObservableCollection<PreTripInspectionFormList>();
            DataSet result = null;
            try
            {
                //string query = "SELECT QuestionId, QuestionTitle, QuestionDescription FROM BUSDTA.Question_Master";
                string query = "CALL BUSDTA.SP_GetInspectionList()";
                result = Helpers.DbEngine.ExecuteDataSet(query);
                PreTripInspectionFormList inspectionList;
                if (result.HasData())
                {
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        DataRow row = result.Tables[0].Rows[i];
                        try
                        {
                            inspectionList = new PreTripInspectionFormList();

                            inspectionList.QuestionID = row["QuestionId"].ToString().Trim();
                            inspectionList.QuestionTitle = row["QuestionTitle"].ToString().Trim();
                            inspectionList.Description = row["QuestionDescription"].ToString().Trim();

                            formList.Add(inspectionList);
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][GetInspectionList][RouteID=" + CommonNavInfo.RouteID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][GetInspectionList][RouteID=" + CommonNavInfo.RouteID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][End:GetInspectionList][RouteID=" + CommonNavInfo.RouteID + "]");
            return formList;
        }
        public int AddVehicleInspectionDetails(PreTripVehicleInspection inspectionModel, ObservableCollection<PreTripInspectionFormList> inspectionQuestionList, string routeName)
        {
            int result = -1;
            int preTripInspectionHeaderID = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][Start:AddVehicleInspectionDetails]");
            try
            {
                //string query = "SELECT RouteMasterID from BUSDTA.Route_Master WHERE RouteName = '" + routeName + "'";
                //string routeID = Helpers.DbEngine.ExecuteScalar(query);

                //string query = "select TemplateID from BUSDTA.Question_Template where TemplateName = 'PRETRIPINSPECTION'";
                string query = "CALL BUSDTA.SP_GetQuestionTemplateID()";
                int templateID = Convert.ToInt32(Helpers.DbEngine.ExecuteScalar(query));

                //query = "select StatusTypeID  from busdta.Status_Type where StatusTypeCD = '" + inspectionModel.StatusCode + "'";
                query = "CALL BUSDTA.SP_GetStatusTypeID(@StatusCode = '" + inspectionModel.StatusCode + "')";
                inspectionModel.StatusID = Convert.ToInt32(Helpers.DbEngine.ExecuteScalar(query));

                query = "insert into BUSDTA.PreTrip_Inspection_Header(RouteId, TemplateId, PreTripDateTime, UserName, StatusId, ";
                query = query + "VehicleMake, VehicleNumber, OdoMmeterReading, \"Comment\", VerSignature, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)";
                query = query + " VALUES('" + CommonNavInfo.RouteID + "', '" + templateID + "', ";
                query = query + "getdate()" + ", '" + null + "', ";
                query = query + "'" + inspectionModel.StatusID + "', ";
                query = query + (string.IsNullOrEmpty(inspectionModel.VehicleModel) ? "null" : "'" + inspectionModel.VehicleModel + "'") + ", ";
                query = query + (string.IsNullOrEmpty(inspectionModel.VehicleNumber) ? "null" : "'" + inspectionModel.VehicleNumber + "'") + ", ";
                query = query + (string.IsNullOrEmpty(inspectionModel.OdometerReading) ? "null" : "'" + inspectionModel.OdometerReading + "'") + ", ";
                query = query + (string.IsNullOrEmpty(inspectionModel.AdditionalComments) ? "null" : "'" + inspectionModel.AdditionalComments + "'") + ", ";
                query = query + "?, '" + UserManager.UserId.ToString() + "', getdate(),'" + UserManager.UserId.ToString() + "',GETDATE())";

                //result = DbEngine.ExecuteNonQuery(query);
                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(query);
                Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.LongBinary;
                command.Parameters.Add(parm);
                command.Parameters[0].Value = inspectionModel.VerificationSignature;
                result = new DB_CRUD().InsertUpdateData(command);
                if (result > 0)
                {
                  //  string HeaderIDquery = "select isnull(max(PreTripInspectionHeaderId),1) from busdta.PreTrip_Inspection_Header";
                  string HeaderIDquery = ("CALL BUSDTA.SP_GetMaxPreTripId()");
                    preTripInspectionHeaderID = Convert.ToInt32(DbEngine.ExecuteScalar(HeaderIDquery));

                    Managers.PreTripInspectionManager inspectionManager = new PreTripInspectionManager();
                    inspectionManager.AddQuestionResponse(inspectionQuestionList, preTripInspectionHeaderID);
                }
                ResourceManager.QueueManager.QueueProcess("PreTrip", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][AddVehicleInspectionDetails][RouteID=" + CommonNavInfo.RouteID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][End:AddVehicleInspectionDetails][RouteID=" + CommonNavInfo.RouteID + "]");
            return preTripInspectionHeaderID;
        }
        private int AddQuestionResponse(ObservableCollection<PreTripInspectionFormList> inspectionQuestionList, int inspectionID)
        {
            int result = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][Start:AddQuestionResponse]");
            try
            {
               
                for (int i = 0; i < inspectionQuestionList.Count; i++)
                {
                    if (inspectionQuestionList[i].IsYesChecked)
                    {
                        inspectionQuestionList[i].ResponseID = 1;
                    }
                    else inspectionQuestionList[i].ResponseID = 2;

                    string query = "insert into BUSDTA.PreTrip_Inspection_Detail(PreTripInspectionHeaderId, RouteId, QuestionId, ResponseID, ResponseReason, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)";
                    query = query + "values( '" + inspectionID + "', '" + CommonNavInfo.RouteID + "', '" + inspectionQuestionList[i].QuestionID + "' , '" + inspectionQuestionList[i].ResponseID + "'";
                    query = query + ", '" + inspectionQuestionList[i].NotPassedReason + "', '" + UserManager.UserId.ToString() + "', getdate(),'" + UserManager.UserId.ToString() + "',GETDATE())";

                    result = Helpers.DbEngine.ExecuteNonQuery(query);
                }
                ResourceManager.QueueManager.QueueProcess("PreTrip", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][AddQuestionResponse][RouteID=" + CommonNavInfo.RouteID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][End:AddQuestionResponse][RouteID=" + CommonNavInfo.RouteID + "]");
            return result;
        }

        public Dictionary<int, string> CheckVerificationSignature(string route)
        {
            Dictionary<int, string> result = new Dictionary<int,string>();
            Logger.Info("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][Start:CheckVerificationSignature]");
            try
            {
                //string query = "SELECT p.PreTripInspectionHeaderId, s.StatusTypeCD FROM BUSDTA.PreTrip_Inspection_Header p join BUSDTA.Status_Type s on s.StatusTypeID = p.StatusId where RouteId = '" + CommonNavInfo.RouteID + "'  and DATEFORMAT(PreTripDateTime, 'YYYY-MM-DD') = DATEFORMAT(GETDATE(), 'YYYY-MM-DD') GROUP BY s.StatusTypeCD, p.PreTripInspectionHeaderId ";
                string query = "CALL BUSDTA.SP_GetPreTripInspectionDetail(@RouteID = '" + CommonNavInfo.RouteID.ToString() + "')";
                DataSet ds = Helpers.DbEngine.ExecuteDataSet(query);
                query = string.Empty;
                if (ds.HasData())
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        DataRow dr = ds.Tables[0].Rows[i];
                        int id = Convert.ToInt32(dr["PreTripInspectionHeaderId"]);
                        string status = dr["StatusTypeCD"].ToString().ToLower().Trim();
                        result.Add(id, status);
                        if (status == "pass") break;
                    }
                }
                ds.Dispose();
                ds = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][CheckVerificationSignature][RouteID=" + CommonNavInfo.RouteID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][End:CheckVerificationSignature][RouteID=" + CommonNavInfo.RouteID + "]");
            return result;
        }

        public ObservableCollection<PreTripVehicleInspection> GetPreTripInsectionHistory()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][Start:GetPreTripInsectionHistory]");
            ObservableCollection<PreTripVehicleInspection> inspectionHistory = new ObservableCollection<PreTripVehicleInspection>();
            try
            {
                //string query = "select h.PreTripInspectionHeaderId, h.VehicleMake, h.VehicleNumber, h.OdoMmeterReading, h.PreTripDateTime, h.StatusId, h.\"Comment\", h.DocumentId from BUSDTA.PreTrip_Inspection_Header h ";
                //query = query + " where RouteId = '" + CommonNavInfo.RouteID + "' order by PreTripDateTime desc";
                //query = query + " order by PreTripDateTime desc";
                //DataSet result = Helpers.DbEngine.ExecuteDataSet(query);

                string MyQuery = "CALL BUSDTA.SP_GetPreTripInsectionHistory(RouteID='" + CommonNavInfo.RouteID + "')";
                DataSet result = Helpers.DbEngine.ExecuteDataSet(MyQuery);
                if (result.HasData())
                {
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        PreTripVehicleInspection inspection = new PreTripVehicleInspection();
                        DataRow row = result.Tables[0].Rows[i];
                        inspection.PreTripInspectionID = row["PreTripInspectionHeaderId"].ToString().Trim();
                        inspection.VehicleModel = row["VehicleMake"].ToString().Trim();
                        inspection.VehicleNumber = row["VehicleNumber"].ToString().Trim();
                        inspection.OdometerReading = row["OdoMmeterReading"].ToString().Trim();
                        inspection.AdditionalComments = row["Comment"].ToString().Trim();
                        inspection.PreTripDateTime = Convert.ToDateTime(row["PreTripDateTime"].ToString());
                        inspection.StatusID = Convert.ToInt32(row["StatusId"]);
                        inspection.StatusTypeCD = row["StatusTypeCD"].ToString();
                        inspection.DocumentId = (!(string.IsNullOrEmpty(row["DocumentId"].ToString()))) ? Convert.ToDecimal(row["DocumentId"]) : 0; 
                        inspectionHistory.Add(inspection);
                    }
                }
                result.Dispose();
                result = null;  
                PreTripVehicleInspection preTripObj = new PreTripVehicleInspection();
                preTripObj = new PreTripInspectionManager().GetVehicleDetails(string.Empty, string.Empty);
                foreach (PreTripVehicleInspection item in inspectionHistory)
                {
                    item.VehicleMake = preTripObj.VehicleMake;
                    item.VehicleNo = preTripObj.VehicleNo;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][GetPreTripInsectionHistory][RouteID=" + CommonNavInfo.RouteID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][End:GetPreTripInsectionHistory][RouteID=" + CommonNavInfo.RouteID + "]");
            return inspectionHistory;
        }

        public static string GetStatusTypeForInspection(int statusID)
        {

            string result = null;
            //string query = "select StatusTypeCD from busdta.Status_Type where StatusTypeID = '" + statusID + "'";
            string query = ("CALL BUSDTA.SP_GetStatusTypeForInspection(@statusID ='" + statusID + "')");
            result = Helpers.DbEngine.ExecuteScalar(query);

            return result;
        }

        public ObservableCollection<PreTripInspectionFormList> GetDetailsForPreTrip(PreTripVehicleInspection Obj)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][Start:GetDetailsForPreTrip][RouteID=" + CommonNavInfo.RouteID + "]");
            ObservableCollection<PreTripInspectionFormList> inspectionList = new ObservableCollection<PreTripInspectionFormList>();
            try
            {
                //string query = "select d.questionId, d.responseid, d.ResponseReason, q.QuestionTitle, q.QuestionDescription from BUSDTA.PreTrip_Inspection_Header h ";
                //query = query + " join BUSDTA.PreTrip_Inspection_Detail d ";
                //query = query + " on h.PreTripInspectionHeaderId = d.PreTripInspectionHeaderId ";
                //query = query + " join BUSDTA.Question_Master q on d.QuestionId = q.QuestionId ";
                //query = query + " where cast(h.PreTripDateTime as date) = '" + Obj.PreTripDateTime.Date.ToString("yyyy-MM-dd") + "' AND ";
                //query = query + " h.PreTripInspectionHeaderId = '" + Obj.PreTripInspectionID + "'";

                string query = "CALL BUSDTA.SP_GetDetailsForPreTrip(@PreTripDateTime = '" + Obj.PreTripDateTime.Date.ToString("yyyy-MM-dd") + "',@PreTripInspectionID = '" + Obj.PreTripInspectionID + "')"; ;
                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    DataRow dr = null;
                   
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        dr = result.Tables[0].Rows[i];
                        PreTripInspectionFormList item = new PreTripInspectionFormList();

                        item.QuestionID = dr["questionId"].ToString();
                        item.ResponseID = Convert.ToInt32(dr["responseid"].ToString().Trim());
                        if (item.ResponseID == 1)
                        {
                            item.IsYesChecked = true;
                        }
                        else item.IsNoChecked = true;
                        item.IsCheckboxEnabled = false;
                        item.IsNoPassedReasonEnabled = false;

                        item.NotPassedReason = dr["ResponseReason"].ToString();
                        item.QuestionTitle = dr["QuestionTitle"].ToString().Trim();
                        item.Description = dr["QuestionDescription"].ToString().Trim();
                        inspectionList.Add(item);
                        item = null;
                    }
                    
                }
               
                result.Dispose();
                result = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][GetDetailsForPreTrip][RouteID=" + CommonNavInfo.RouteID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][PreTripInspectionManager][End:GetPreTripInsectionHistory][RouteID=" + CommonNavInfo.RouteID + "]");

            return inspectionList;
        }

        //************************************************************************************************
        // Comment: Dispose Implementation for Peformance Improvement
        // Created: Nov 03, 2016
        // Author: TechUnison (Velmani Karnan)
        //*************************************************************************************************

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
