﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Application.ViewModelPayload;
using System.Text;
namespace SalesLogicExpress.Application.Managers
{
    public class OrderManager : IDisposable
    {

        private static readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.OrderManager");
        private static PricingManager pricingManager = new PricingManager();
        ReasonCodeManager objReasonCodeManger = new ReasonCodeManager();

        //public OrderManager()
        //{

        //}

        //public ActivityKey GetOrderSubStatus(string orderId)
        //{
        //    string substate = DbEngine.ExecuteScalar("select TTKEY from busdta.ORDER_HEADER oh join BUSDTA.M5001  tt on tt.ttid=oh.OrderStateId WHERE orderid=" + orderId);

        //    if (substate.Contains("Void"))

        //        if (string.IsNullOrEmpty(substate))
        //        {
        //            return OrderSubState.None;
        //        }
        //        else
        //        {
        //            return OrderSubState.Hold.ToString() == substate ? OrderSubState.Hold : OrderSubState.Void;
        //        }

        //}

        public bool IsItemPickedForOrder(string orderId)
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:IsItemPickedForOrder]");
            try
            {

                // int itemCount = Convert.ToInt32(DbEngine.ExecuteScalar("select count(*) from busdta.pickOrder where order_id=" + orderId + " and Picked_Qty_Primary_UOM !=0"));
                int itemCount = Convert.ToInt32(DbEngine.ExecuteScalar("CALL busdta.SP_GetItemCount(@OrderID='" + orderId + "')"));
                //int itemExCount = Convert.ToInt32(DbEngine.ExecuteScalar("select count(*) from busdta.pickOrder_exception where order_id=" + orderId + " and exception_Qty !=0"));
                int itemExCount = Convert.ToInt32(DbEngine.ExecuteScalar("CALL busdta.SP_GetItemExCount(@OrderID='" + orderId + "')"));
                if (itemCount == 0 && itemExCount == 0)
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][IsItemPickedForOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:IsItemPickedForOrder]");
            return true;
        }


        public string GetCustomerPO(String OrderID)
        {
            string sCustomerPO = "";
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:GetCustomerPO]");
            try
            {
                sCustomerPO = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetCustomerPO (@OrderID='" + OrderID + "')");
                //sCustomerPO = DbEngine.ExecuteScalar("SELECT  Isnull(CustomerReference1,'') as 'CustmerPo' from busdta.Order_Header where OrderID ='" + OrderID + "'");
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][GetCustomerPO][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:GetCustomerPO]");
            return sCustomerPO;
        }
        public int GetNewOrderNum()
        {
            int OrderNum = 0;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:GetNewOrderNum]");
            try
            {
                //for (; ; )
                //{
                //Sathish 18/11/2016 Commenting this below code
                //Validation are done in GetNextNumberForEntity function itself
                string val = new Managers.NumberManager().GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.SalesOrder);
                OrderNum = Convert.ToInt32(val);
                //    //string OrderId = DbEngine.ExecuteScalar("select OrderId from busdta.order_Header where OrderId=" + OrderNum);
                //    string OrderId = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetForOrderID(@OrderId = '" + OrderNum + "')");
                //    if (string.IsNullOrEmpty(OrderId))
                //        break;
                //    else
                //        Logger.Error("Already Created Order ID:" + OrderId);
                //}

                //************************************************************************************************
                // Comment: Added to send pull the stopdate and stopId
                // Created: feb 10, 2016
                // Author: Vivensas (Rajesh,Yuvaraj)
                // Revisions: 
                //*************************************************************************************************

                Managers.CustomerDashboardManager dashboardManager = new CustomerDashboardManager();
                if (CommonNavInfo.Customer.StopID == null)
                {
                    CommonNavInfo.Customer.StopDate = DateTime.Now.Date;
                    CommonNavInfo.Customer.StopID = dashboardManager.GetStopIDForCust(CommonNavInfo.Customer.CustomerNo, CommonNavInfo.Customer.StopDate.Value.ToString("yyyy-MM-dd"));
                }

                //*************************************************************************************************
                // Vivensas changes ends over here
                //**************************************************************************************************

                StopManager objStopMgr = new StopManager();
                // Commented On - 09/17/2016
                //int updateStopActivityFlag = DbEngine.ExecuteNonQuery("update busdta.m56m0004 set rpactid=null where rpstid = " + CommonNavInfo.Customer.StopID + " and rpan8=" + CommonNavInfo.Customer.CustomerNo + "");
                // Comment End


                //int updateStopActivityFlag = objStopMgr.UpdateStopActivity(CommonNavInfo.Customer.StopID, CommonNavInfo.Customer.CustomerNo);// Remove unused locals variable
                // Call Common Function - Reuse 
                objStopMgr.UpdateStopActivity(CommonNavInfo.Customer.StopID, CommonNavInfo.Customer.CustomerNo);

                //int updatePreOrderFlag = DbEngine.ExecuteNonQuery("update BUSDTA.M4016 SET POSTFG = '1' where POAN8 = '" + CommonNavInfo.Customer.CustomerNo + "' and POSTDT = '" + CommonNavInfo.Customer.StopDate.Value.ToString("yyyy-MM-dd") + "'");// Remove unused locals variable
                DbEngine.ExecuteNonQuery("update BUSDTA.M4016 SET POSTFG = '1' where POAN8 = '" + CommonNavInfo.Customer.CustomerNo + "' and POSTDT = '" + CommonNavInfo.Customer.StopDate.Value.ToString("yyyy-MM-dd") + "'");
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                OrderNum = -1;
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetNewOrderNum][RouteID=" + CommonNavInfo.RouteID.ToString() + "][ForEntity=" + Managers.NumberManager.Entity.SalesOrder.ToString() + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:GetNewOrderNum]");
            return OrderNum;
        }

        //public int GetUncomletedLastOrderIdIfAny(string CustomerId)
        //{
        //    return Convert.ToInt32(DbEngine.ExecuteScalar("select isnull(max(OrderID),0) from BUSDTA.ORDER_HEADER where CustShipToId=" + CustomerId + " and isnull(OrderStateId,'') not in ( " +
        //        GetTerminatedOrderStatusIds() + " ) AND  OrderTypeId = " + StatusTypesEnum.SALORD.GetStatusIdFromDB() + ""));
        //}

        //public string GetTerminatedOrderStatusIds()
        //{
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:GetTerminatedOrderStatusIds]");
        //    String strids = "";
        //    try
        //    {

        //        DataTable dt = DbEngine.ExecuteDataSet("SELECT TTID FROM BUSDTA.M5001 where TTKEY in ("
        //              + "'" + ActivityKey.OrderDelivered.ToString() + "',"
        //              + "'" + ActivityKey.VoidAtOrderEntry.ToString() + "',"
        //              + "'" + ActivityKey.VoidAtDeliverCustomer.ToString() + "',"
        //              + "'" + ActivityKey.VoidAtCashCollection.ToString() + "',"
        //              + "'" + ActivityKey.HoldAtPick.ToString() + "',"
        //              + "'" + ActivityKey.HoldAtOrderEntry.ToString() + "',"
        //              + "'" + ActivityKey.HoldAtCashCollection.ToString() + "',"
        //              + "'" + ActivityKey.OrderReturned.ToString() + "',"
        //              + "'" + ActivityKey.HoldAtDeliverToCustomer.ToString() + "'"
        //            + ") ").Tables[0];

        //        foreach (DataRow item in dt.Rows)
        //        {
        //            strids = strids + item["TTID"].ToString() + ",";
        //        }

        //        strids = strids.Substring(0, strids.Length - 1);

        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][GetTerminatedOrderStatusIds][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:GetTerminatedOrderStatusIds]");
        //    return strids;
        //}

        public int SaveOrder(int OrderId, int CustomerId, ObservableCollection<OrderItem> OrderItems, string TotalCoffeeAmt = "", string TotalAlliedAmt = "")
        {
            int result = 0;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:SaveOrder]");
            try
            {
                //check for Order id
                // string strOrderId = DbEngine.ExecuteScalar("select OrderID from BUSDTA.ORDER_HEADER where OrderID='" + OrderId.ToString() + "'");
                string strOrderId = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetSaveOrderID (@OrderID='" + OrderId.ToString() + "')");

                //Add mode
                if (string.IsNullOrEmpty(strOrderId))
                {
                    //Insert in ORDER_HEADER
                    //result = SaveOrderHeader(OrderId, CustomerId, TotalCoffeeAmt, TotalAlliedAmt);
                    result = SaveOrderHeaderBeta(OrderId, CustomerId, TotalCoffeeAmt, TotalAlliedAmt);
                }
                else
                {
                    //Update ORDER_HEADER
                    UpdateOrderHeader(OrderId);

                }

                //Delete and Insert in Order_details
                //DeleteAndInsertOrderItems(OrderItems, OrderId);
                DeleteAndInsertOrderBetaItems(OrderItems, OrderId);

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][SaveOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:SaveOrder]");
            return result;
        }

        private void DeleteOrderItems(int OrderId)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:DeleteOrderItems]");
            try
            {
                DbEngine.ExecuteNonQuery("delete from BUSDTA.Order_Detail where OrderID = " + OrderId.ToString());
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][DeleteOrderItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:DeleteOrderItems]");
        }

        private void DeleteOrderPriceAdj(int orderID)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:DeleteOrderPriceAdj]");
            try
            {
                if (orderID == null) orderID = 0;

                DbEngine.ExecuteNonQuery("delete from BUSDTA.Order_PriceAdj where OrderID = " + orderID.ToString());
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][DeleteOrderPriceAdj][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:DeleteOrderPriceAdj]");
        }



        //        public int SaveOrderHeader(int OrderId, int CustomerId, string TotalCoffeeAmt = "", string TotalAlliedAmt = "")
        //        {
        //            int result = 0;
        //            try
        //            {
        //                result = DbEngine.ExecuteNonQuery(@"insert into BUSDTA.ORDER_HEADER (
        //                    OrderID,
        //                    OrderTypeId,
        //                    RouteId,
        //                    CustomerId,
        //                    OrderDate,
        //                    CreatedBy,
        //                    CreatedDatetime,
        //                    TotalCoffeeAmt,
        //                    TotalAlliedAmt,
        //                    OrderTotalAmt
        //                    )
        //                    values(
        //                    '" + OrderId + @"'," + StatusTypesEnum.SALORD.GetStatusIdFromDB()
        //                       + "," + ViewModels.CommonNavInfo.RouteID + "," +
        //                    "'" + CustomerId + @"',
        //                    date(now()),
        //                    " + Managers.UserManager.UserId + @",
        //                    now(),
        //                    " + TotalCoffeeAmt + "," +
        //                     TotalAlliedAmt + "," + ViewModelPayload.PayloadManager.OrderPayload.Amount + ");");
        //            }
        //            catch (Exception)
        //            {

        //                throw;
        //            }
        //            return result;
        //        }

        private int SaveOrderHeaderBeta(int OrderId, int CustomerId, string TotalCoffeeAmt = "", string TotalAlliedAmt = "")
        {
            int result = 0;
            CustomerManager cm = new CustomerManager();
            DataTable dt = cm.GetCustomerDetailsForOrder(CustomerId.ToString());

            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:SaveOrderHeaderBeta]");
            try
            {

                string JDEInvoiceType;
                if (dt.Rows[0]["PaymentTerms"].ToString().ToLower() == "csh")
                    JDEInvoiceType = "R3";
                else
                    JDEInvoiceType = "RI";


                result = DbEngine.ExecuteNonQuery(@"insert into Busdta.Order_Header (
                    OrderID,
                    OrderTypeId,
                    OriginatingRouteID,
                    Branch,
                    CustShipToId,
                    CustBillToId,
                    CustParentId,
                    OrderDate,
                    CreatedBy,
                    CreatedDatetime,
                    TotalCoffeeAmt,
                    TotalAlliedAmt,
                    OrderTotalAmt,
                    PaymentTerms,
                    JDEOrderNumber,JDEOrderCompany,JDEOrderType,JDEInvoiceCompany,
                    JDEOInvoiceType,JDEZBatchNumber,CustomerReference1,CustomerReference2,
                    JDEProcessID,SettlementID,VarianceAmt,SurchargeReasonCodeId,CreditReasonCodeId, CreditMemoNote,
                    AdjustmentSchedule,
                    TaxExplanationCode,
                    TaxArea
                    )
                    values(
                    '" + OrderId + @"'," + StatusTypesEnum.SALORD.GetStatusIdFromDB() + ","
                       + ViewModels.CommonNavInfo.RouteID +
                       ",'" + GetBranch(ViewModels.CommonNavInfo.UserBranch) + "',"
                       + "'" + dt.Rows[0]["ShipTo"] + @"',"
                       + "'" + dt.Rows[0]["BillTo"] + "',"
                       + "'" + pricingManager.GetShipToParent(Convert.ToInt64(ViewModels.CommonNavInfo.Customer.customerNo)) + "'"
                       + ", date(now()),"
                    + Managers.UserManager.UserId + @",
                    now(),
                    " + TotalCoffeeAmt + "," +
                     TotalAlliedAmt + "," +
                     ViewModelPayload.PayloadManager.OrderPayload.Amount + ", '" + dt.Rows[0]["PaymentTerms"] + "', " +
                     " '" + OrderId + "','00800','SO','00800','" + JDEInvoiceType + "','','','','0','0','0','0','0','', '" +
                     dt.Rows[0]["AdjustmentSchedule"] + "' ,'" + dt.Rows[0]["TaxExplanationCode"] + "','" + dt.Rows[0]["Taxarea"] + "' );");
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][SaveOrderHeaderBeta][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:SaveOrderHeaderBeta]");
            return result;
        }

        private string GetBranch(string branch)
        {
            string BrnchNo = branch;
            for (int i = BrnchNo.Length; i <= 11; i++)
            {
                BrnchNo = " " + BrnchNo;
            }
            return BrnchNo;
        }
        public int UpdateOrderHeader(int OrderId)
        {
            int result = 0;
            try
            {
                result = DbEngine.ExecuteNonQuery(@"update BUSDTA.ORDER_HEADER set                     
                    OrderDate = date(now()),
                    CreatedDatetime = date(now())
                    where  OrderID=" + OrderId.ToString());
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }


        public ObservableCollection<ViewModels.ReasonCode> GetReasonListForVoidOrder()
        {
            ObservableCollection<ViewModels.ReasonCode> reasonCodeList = new ObservableCollection<ViewModels.ReasonCode>();

            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:GetReasonListForVoidOrder]");
            try
            {
                //DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Void Order'").Tables[0];

                DataTable dt = objReasonCodeManger.OrderManager_GetReasonListForVoidOrder();

                ViewModels.ReasonCode ReasonCode = new ViewModels.ReasonCode();


                foreach (DataRow dr in dt.Rows)
                {
                    ReasonCode = new ViewModels.ReasonCode();
                    ReasonCode.Id = Convert.ToInt32(dr["Id"].ToString());
                    ReasonCode.Code = dr["ReasonCodeDescription"].ToString();
                    reasonCodeList.Add(ReasonCode);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][GetReasonListForVoidOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:GetReasonListForVoidOrder]");

            return reasonCodeList;
        }

        public ObservableCollection<ViewModels.ReasonCode> GetReasonListFor(string Type)
        {
            ObservableCollection<ViewModels.ReasonCode> reasonCodeList = new ObservableCollection<ViewModels.ReasonCode>();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:GetReasonListFor]");
            try
            {
                //DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='" + Type + "'").Tables[0];
                DataTable dt = objReasonCodeManger.OrderManager_GetReasonListFor(Type);

                ViewModels.ReasonCode ReasonCode = new ViewModels.ReasonCode();

                foreach (DataRow dr in dt.Rows)
                {
                    ReasonCode = new ViewModels.ReasonCode();
                    ReasonCode.Id = Convert.ToInt32(dr["Id"].ToString());
                    ReasonCode.Code = dr["ReasonCodeDescription"].ToString();
                    reasonCodeList.Add(ReasonCode);
                }

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][GetReasonListFor][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:GetReasonListFor]");

            return reasonCodeList;
        }

        private void UpdateInventoryForDeletedOrderItem(List<Item> OrderItems, int OrderId)
        {

            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:UpdateInventoryForDeletedOrderItem]");
            try
            {
                //string query = "select ItemID,OrderQty from busdta.Order_detail where OrderID='{0}'";

                string query = "CALL BUSDTA.SP_GetDeletedOrderItemByOrderId(@OrderID='{0}')";
                query = string.Format(query, OrderId);
                DataSet ds = DbEngine.ExecuteDataSet(query);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    // This is a new order, saved from order template screen
                }
                else
                {
                    Dictionary<string, int> ExistingItemWithQty = new Dictionary<string, int>();
                    // Existing order being saved again.
                    /* Cases : 
                    * Existing item was removed
                    * Existing item qty was altered
                    * New Item was added 
                    * Existing items were removed and new items were added with same qty
                    */
                    List<Item> itemsRemoved = new List<Item>();
                    foreach (DataRow itemRow in ds.Tables[0].Rows)
                    {
                        string ItemID = itemRow["ItemID"].ToString();
                        int OrderQty = Convert.ToInt32(itemRow["OrderQty"].ToString());
                        ExistingItemWithQty.Add(ItemID, OrderQty);
                        //Existing item was removed
                        if (OrderItems.FirstOrDefault(item => item.ItemId == ItemID) == null)
                        {
                            // Increase inventory qty for this item
                            Item itemM = new Item();
                            itemM.ItemId = ItemID;
                            itemM.OrderQty = OrderQty;
                            itemsRemoved.Add(itemM);
                            continue;
                        }
                    }
                    // new InventoryManager().UpdateCommittedQuantity(itemsRemoved, true);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][UpdateInventoryForDeletedOrderItem][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:UpdateInventoryForDeletedOrderItem]");
        }


        //        public void DeleteAndInsertOrderItems(ObservableCollection<OrderItem> OrderItems, int OrderId)
        //        {
        //            int iLineID = 0;
        //            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:DeleteAndInsertOrderItems]");
        //            try
        //            {
        //                UpdateInventoryForDeletedOrderItem(OrderItems.ToList<Item>(), OrderId);
        //                DeleteOrderItems(OrderId);
        //                DeleteOrderPriceAdj(OrderId);

        //                foreach (OrderItem orderItem in OrderItems)
        //                {
        //                    iLineID++;
        //                    var isTaxable = orderItem.IsTaxable ? 1 : 0;
        //                    DbEngine.ExecuteNonQuery(@"insert into BUSDTA.Order_Detail(
        //                        OrderID,
        //                        ItemId,
        //                        OrderQty,
        //                        OrderUM,
        //                        UnitPriceAmt,
        //                        ExtnPriceAmt,
        //                        ItemSalesTaxAmt,
        //                        IsTaxable,
        //                        PriceOverrideReasonCodeId
        //                        )values
        //                        ( '" + OrderId + @"'" +
        //                        "," + orderItem.ItemId + @" " +
        //                        "," + orderItem.OrderQty + @"," +
        //                        "'" + orderItem.UM + @"',
        //                        " + orderItem.UnitPrice + @",
        //                        " + orderItem.ExtendedPrice + @",
        //                        " + orderItem.TaxAmount + @",
        //                        " + isTaxable + @",
        //                        " + orderItem.ReasonCode + @"
        //                        );
        //                        ");
        //                }
        //                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
        //            }
        //            catch (Exception ex)
        //            {
        //                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][DeleteAndInsertOrderItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //            }
        //            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:DeleteAndInsertOrderItems]");
        //        }


        public void DeleteAndInsertOrderBetaItems(ObservableCollection<OrderItem> OrderItems, int OrderId)
        {
            int iLineID = 0;
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:DeleteAndInsertOrderBetaItems]");
            try
            {
                UpdateInventoryForDeletedOrderItem(OrderItems.ToList<Item>(), OrderId);
                DeleteOrderItems(OrderId);
                DeleteOrderPriceAdj(OrderId);


                //string JDEInvoiceType = DbEngine.ExecuteScalar("SELECT h.JDEOInvoiceType FROM BUSDTA.Order_Header h WHERE h.OrderID='" + OrderId + "'");

                string JDEInvoiceType = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetJDEOInvoiceTypeByOrderID(@OrderId ='" + OrderId + "')");

                CustomerManager cm = new CustomerManager();
                DataTable dt = cm.GetCustomerDetailsForOrder(PayloadManager.OrderPayload.Customer.CustomerNo.ToString());

                foreach (OrderItem orderItem in OrderItems)
                {
                    iLineID++;

                    DataTable vOrderItemdetails = ItemManager.GetItemDetails(orderItem.ItemId);
                    string JDEBranch = "00800";
                    var isTaxable = orderItem.IsTaxable ? 1 : 0;
                    string PriceOverrideFlag = orderItem.ReasonCode < 2 ? "0" : "1";
                    decimal conversionfactor = decimal.Parse(pricingManager.jdeUOMConversion(orderItem.UM, orderItem.UMPrice, int.Parse(orderItem.ItemId)).ToString());

                    string NewPriceAmtUom = "0";

                    // PricingUM Qty Changes
                    int conValue;
                    if (conversionfactor == 1)
                    {
                        conValue = Convert.ToInt32((conversionfactor * orderItem.OrderQty));
                        NewPriceAmtUom = (PriceOverrideFlag == "0" ? orderItem.UnitPriceByPricingUOM : (orderItem.UM == orderItem.UMPrice ? orderItem.UnitPrice : (orderItem.UnitPrice * conversionfactor))).ToString();
                    }
                    else
                    {
                        //conValue = Convert.ToInt32((conversionfactor / ((orderItem.OrderQty <= 0) ? 1 : orderItem.OrderQty)));
                        conValue = Convert.ToInt32((conversionfactor * orderItem.OrderQty));
                        if (conversionfactor == 0) conversionfactor = 1;
                        NewPriceAmtUom = (PriceOverrideFlag == "0" ? orderItem.UnitPriceByPricingUOM : (orderItem.UM == orderItem.UMPrice ? orderItem.UnitPrice : (orderItem.UnitPrice / conversionfactor))).ToString();
                    }

                    DbEngine.ExecuteNonQuery(@"insert into BUSDTA.Order_Detail(
                        OrderID,LineID,JDEOrderLine,CustShipToID,ItemId,LongItem,LineType,
                        IsNonStock
                        ,SalesCat1,SalesCat2 ,SalesCat3 ,SalesCat4 ,SalesCat5 ,PurchasingCat1 ,PurchasingCat2 ,PurchasingCat3 ,PurchasingCat4 ,PurchasingCat5 ,
                        JDEOrderNumber,JDEOrderCompany,JDEOrderType,JDEInvoiceCompany,JDEOInvoiceType,JDEZBatchNumber,
                        JDEProcessID,SettlementID,ReturnHeldQty,ExtendedAmtVariance,TaxAmountAmtVariance,HoldCode,
                        OrderQty,OrderQtyInPricingUM,OrderUM,PricingUM,UnitPriceAmtInPriceUoM,UnitPriceOriginalAmtInPriceUM ,UnitPriceAmt ,ExtnPriceAmt,ItemSalesTaxAmt,
                        IsTaxable,PriceOverriden,PriceOverrideReasonCodeId, CreatedBy,CreatedDatetime
                        )values
                        (
                        '" + OrderId + @"'," +
                        "'" + iLineID + "'," +
                        "'" + iLineID + "'," +
                        "'" + dt.Rows[0]["ShipTo"].ToString() + "'" +
                        "," + orderItem.ItemId + @",'" +
                        vOrderItemdetails.Rows[0]["LongItm"] + "'" +
                        ",'" + vOrderItemdetails.Rows[0]["IBLNTY"] + "'" +
                        ",'" + vOrderItemdetails.Rows[0]["IsNonStock"] + "'" +
                        ",'" + vOrderItemdetails.Rows[0]["SalesCat1"] + "'" +
                        ",'" + vOrderItemdetails.Rows[0]["SalesCat2"] + "'" +
                        ",'" + vOrderItemdetails.Rows[0]["SalesCat3"] + "'" +
                        ",'" + vOrderItemdetails.Rows[0]["SalesCat4"] + "'" +
                        ",'" + vOrderItemdetails.Rows[0]["SalesCat5"] + "'" +
                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat1"] + "'" +
                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat2"] + "'" +
                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat3"] + "'" +
                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat4"] + "'" +
                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat5"] + "'" +
                        ",'" + OrderId + "'" +
                        ",'" + JDEBranch + "'" +
                        ",'SO'" +
                        ",'" + JDEBranch + "'" +
                        ",'" + JDEInvoiceType + "'" +
                        ",''" +
                        ",''" +
                        ",'0'" +
                        ",'0'" +
                        ",'0'" +
                        ",'0'" +
                        ",''" +
                        "," + orderItem.OrderQty + @"" +
                        //"," + conversionfactor * orderItem.OrderQty + @"," +
                        "," + conValue + @"," +
                        "'" + orderItem.UM + @"'," +
                        "'" + orderItem.UMPrice + @"'," +
                        //"'" + (orderItem.OrderQty * decimal.Parse(pricingManager.jdeUOMConversion(orderItem.UM, orderItem.UMPrice, int.Parse(orderItem.ItemId)).ToString())) * Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(CommonNavInfo.UserBranch, CommonNavInfo.Customer.customerNo, orderItem.ItemNumber, orderItem.OrderQty.ToString())) + @"'," + 
                        // "'" + (orderItem.OrderQty * orderItem.UnitPrice ) + @"'," +                         
                        //"'" + (PriceOverrideFlag == "0" ? orderItem.UnitPriceByPricingUOM : (orderItem.UM == orderItem.UMPrice ? orderItem.UnitPrice : (orderItem.UnitPrice * conversionfactor))) + @"'," +
                        "'" + NewPriceAmtUom + "'," +
                        "'" + orderItem.UnitPriceByPricingUOM + @"',
                        " + orderItem.UnitPrice + @",
                        " + orderItem.ExtendedPrice + @",
                        " + orderItem.TaxAmount + @",
                        " + isTaxable + @",
                        '" + PriceOverrideFlag + @"',
                        " + orderItem.ReasonCode + @",
                        " + Managers.UserManager.UserId + @",
                        now()  
                        );
                        ");

                    pricingManager.ApplyPricingBetaDetails(CommonNavInfo.UserBranch, CommonNavInfo.Customer.customerNo, orderItem.ItemNumber, orderItem.OrderQty.ToString(), OrderId.ToString(), iLineID);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][DeleteAndInsertOrderBetaItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:DeleteAndInsertOrderBetaItems]");
        }


        //public ObservableCollection<OrderItem> GetOrderForCustomer()
        //{
        //    return null;
        //}
        //public void CreateOrderForCustomer()
        //{

        //}
        /// <summary>
        /// Gets the Order history for customer
        /// </summary>
        /// <param name="customerID">Cutomer No</param>
        /// <returns>DataSet with two tables, first table has OrderHistory Items and the second table includes Order Date headers</returns>
        public DataSet GetOrderHistoryForCustomer(string customerID)
        {
            DataSet dsOrderHistory = null;
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:GetOrderHistoryForCustomer]");
            try
            {
                Dictionary<string, object> procParamaters = new Dictionary<string, object>();
                procParamaters.Add("@shipTo", customerID);
                //int result = Helpers.DbEngine.ExecuteNonQuery("BUSDTA.prepareHistoryRecords", procParamaters, true);// Remove unused locals variable
                Helpers.DbEngine.ExecuteNonQuery("BUSDTA.prepareHistoryRecords", procParamaters, true);
                //string queryOrderHistory = "select *,0 as AverageQty from busdta.localOrderHistoryPanel";
                ////string queryOrderHistoryHeader = "select chronology,convert(varchar(15),SHDOCO) as OrderNumber,Dateformat(BUSDTA.DateG2J(SHTRDJ),'mm/dd/yyyy') as OrderDate from busdta.localLastOrders order by convert(varchar(15),SHDOCO) desc";
                //string queryOrderHistoryHeader = "select chronology,OrderID as OrderNumber, Dateformat(OrderDate,'mm/dd/yyyy') as OrderDate from busdta.localLastOrders order by OrderID desc";
                //// New Implement Date : July 27 2016 by Hari 
                //string queryNetQtySoldPerStop = "select ItemId,NetQtySoldPerStop as AverageStopQty from BUSDTA.Metric_CustHistDemand where CustomerId = '" + customerID + "'";
                string queryNetQtySoldPerStop = "CALL BUSDTA.SP_GetNetQtySoldPerStop(@CustomerId = '" + customerID + "')";
                string queryOrderHistoryHeader = "CALL BUSDTA.SP_GetOrderHistoryHeaderDetails()";
                string queryOrderHistory = "CALL BUSDTA.SP_GetOrderHistory()";

                Dictionary<string, string> orderHistoryAndDateQueryList = new Dictionary<string, string>();
                orderHistoryAndDateQueryList.Add("OrderHistory", queryOrderHistory);
                orderHistoryAndDateQueryList.Add("OrderHistoryHeaders", queryOrderHistoryHeader);
                orderHistoryAndDateQueryList.Add("MetricCustHistDemand", queryNetQtySoldPerStop);  //this line added newly by Hari
                dsOrderHistory = Helpers.DbEngine.ExecuteDataSet(orderHistoryAndDateQueryList);


                if (dsOrderHistory.HasData())
                {
                    if (dsOrderHistory.Tables[0].Columns["imlitm"] == null)
                        return dsOrderHistory;

                    dsOrderHistory.Tables[0].Columns["imlitm"].ColumnName = "ItemCode";
                    dsOrderHistory.Tables[0].Columns["imdsc1"].ColumnName = "ItemDesc";
                    int historyAvailableFor = dsOrderHistory.Tables[0].Columns.Count - 3;
                    foreach (DataRow orderHistoryItem in dsOrderHistory.Tables[0].Rows)
                    {
                        double average = 0;
                        historyAvailableFor = dsOrderHistory.Tables[0].Columns.Count - 3;

                        if (historyAvailableFor > 10)
                        {
                            historyAvailableFor = 10;
                        }

                        for (int i = 0; i < historyAvailableFor; i++)
                        {
                            average = average + (string.IsNullOrEmpty(orderHistoryItem["H" + (i + 1) + "_Qty"].ToString()) ? 0 : Convert.ToInt32(orderHistoryItem["H" + (i + 1) + "_Qty"]));
                            if (string.IsNullOrEmpty(orderHistoryItem["H" + (i + 1) + "_Qty"].ToString()))
                            {
                                orderHistoryItem["H" + (i + 1) + "_Qty"] = 0;
                            }
                        }
                        average = (average == 0) ? 0 : average / historyAvailableFor;
                        orderHistoryItem["AverageQty"] = average;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetOrderHistoryForCustomer][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:GetOrderHistoryForCustomer]");
            return dsOrderHistory;
        }

        //        public DataTable GetNewOrderItems(string CustomerId)
        //        {
        //            DataTable ExpectedDT = new DataTable();

        //            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:GetNewOrderItems]");
        //            try
        //            {
        //                DataTable dt = new DataTable();

        //                dt = DbEngine.ExecuteDataSet(@"select od.ItemId, im.imdsc1,od.OrderQty,oh.orderdate,oh.orderid
        // from BUSDTA.Order_Detail od join BUSDTA.ORDER_HEADER oh on od.orderid=oh.orderid join busdta.F4101 im on od.ItemId= im.IMITM where oh.customerid='" + CustomerId + "'" +
        //    " order by oh.orderid").Tables[0];

        //                string previousOrderId = "";

        //                ExpectedDT.Columns.Add("Item Number");
        //                ExpectedDT.Columns.Add("Item Desc");

        //                foreach (DataRow item in dt.Rows)
        //                {
        //                    if (item["order_id"].ToString() != previousOrderId)
        //                    {
        //                        previousOrderId = item["order_id"].ToString();
        //                        ExpectedDT.Columns.Add("H_Qty");
        //                    }
        //                    if (ExpectedDT.Select("Item_Number ='" + item["Item_Number"].ToString() + "'").CopyToDataTable().Rows.Count > 0) ;
        //                    {

        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetNewOrderItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //            }
        //            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:GetNewOrderItems]");
        //            return ExpectedDT;
        //        }
        //public void SaveInvoiceItemsForReport(ObservableCollection<OrderItem> orderItems, int invoiceNo)
        //{
        //    Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:SaveInvoiceItemsForReport]");
        //    try
        //    {
        //        string truncateInvoiceDetailsQuery = "truncate table BUSDTA.InvoiceDetails;";
        //        Helpers.DbEngine.ExecuteNonQuery(truncateInvoiceDetailsQuery);

        //        string insertInvoiceDetailsQuery = "BUSDTA.SaveMasterInvoice";
        //        Dictionary<string, object> parameters = new Dictionary<string, object>();
        //        foreach (OrderItem item in orderItems)
        //        {
        //            parameters.Clear();
        //            parameters.Add("@InvoiceNo", invoiceNo);
        //            parameters.Add("@OrderQty", item.OrderQty);
        //            parameters.Add("@UM", item.UM);
        //            parameters.Add("@ItemCode", item.ItemNumber);
        //            parameters.Add("@ProductDesc", item.ItemDescription);
        //            parameters.Add("@UnitPrice", item.UnitPrice);
        //            parameters.Add("@ExtendedPrice", item.ExtendedPrice);
        //            parameters.Add("@OrderDate", item.OrderDate);
        //            parameters.Add("@SalesCat1", item.SalesCat1);
        //            Helpers.DbEngine.ExecuteNonQuery(insertInvoiceDetailsQuery, parameters, true);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][SaveInvoiceItemsForReport][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:SaveInvoiceItemsForReport]");
        //}

        public OrderItem ApplyPricingToItem(Item item, string routeBranch, string shipToCustomerID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:ApplyPricingToItem]");
            try
            {
                decimal unitPrice = 0;
                Random randomProvider = new Random();

                //*******************************************************
                //Code Line for changed to store orginal price of variables 
                //OriginalPrice is a read-only variable and is assined a value in constructor of orderitem 
                //************************ START *******************************
                //OrderItem orderItem = new OrderItem(item);
                //PricingManager pricingManager = new PricingManager();
                //unitPrice = 0;
                //orderItem.QtyOnHand = randomProvider.Next(0, 50);
                //orderItem.OrderQty = 1;
                //unitPrice = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(routeBranch, shipToCustomerID, orderItem.ItemNumber, orderItem.OrderQty.ToString()));
                //orderItem.UnitPrice = unitPrice == 0 ? unitPrice : unitPrice;
                //orderItem.ExtendedPrice = (orderItem.UM == orderItem.UMPrice) ? decimal.Parse(orderItem.OrderQty.ToString()) * orderItem.UnitPrice : (decimal.Parse(orderItem.OrderQty.ToString()) * orderItem.UnitPrice * decimal.Parse(pricingManager.jdeUOMConversion(orderItem.UMPrice, orderItem.UM, int.Parse(orderItem.ItemId)).ToString()));
                //orderItem.AppliedUMS = new ItemManager().GetAppliedUMs(orderItem.ItemNumber).Count != 0 ? new ItemManager().GetAppliedUMs(orderItem.ItemNumber) : new List<string>() { orderItem.UM };


                item.QtyOnHand = randomProvider.Next(0, 50);
                item.OrderQty = 1;
                //Get Price By Pricing UOM

                /* 
                 * Changed By Dinesh - TUI
                 * Calculate UnitPrice by Unit Converstion Factor
                 */

                item.UnitPriceByPricingUOM = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(routeBranch, shipToCustomerID, item.ItemNumber, (item.OrderQty * item.UMConversionFactor).ToString()));
                //Get Price By UOM Factor
                unitPrice = GetPriceByUomFactor(item.ItemId, item.UMPrice, item.UM, item.UnitPriceByPricingUOM);
                item.UnitPrice = unitPrice;
                //item.ExtendedPrice = (item.UM == item.UMPrice) ? decimal.Parse(item.OrderQty.ToString()) * item.UnitPrice : (decimal.Parse(item.OrderQty.ToString()) * item.UnitPrice * decimal.Parse(pricingManager.jdeUOMConversion(item.UMPrice, item.UM, int.Parse(item.ItemId)).ToString()));
                item.ExtendedPrice = decimal.Parse(item.OrderQty.ToString()) * item.UnitPrice;
                item.AppliedUMS = new ItemManager().GetAppliedUMs(item.ItemNumber).Count != 0 ? new ItemManager().GetAppliedUMs(item.ItemNumber) : new List<string>() { item.UM };

                //************************ END *******************************
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][ApplyPricingToItem][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:ApplyPricingToItem]");
            return new OrderItem(item);
        }

        public static decimal GetPriceByUomFactor(string ItemId, string PriceUOM, string AppliedUOM, decimal UnitPriceForPricingUOM)
        {
            decimal PriceUom = 0;
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:GetPriceByUomFactor]");
            try
            {
                string ResultUnitPrice = "";

                if (PriceUOM != AppliedUOM)
                {
                    //************************************************************************************************
                    // Comment: Handling negative uomFactor
                    // Created: Feb 08, 2016
                    // Author: Vivensas (Rajesh,Yuvaraj)
                    // Revisions: 
                    //*************************************************************************************************
                    var uomFactor = decimal.Parse(pricingManager.jdeUOMConversion(AppliedUOM, PriceUOM, int.Parse(ItemId)).ToString());
                    // If uomFactor is less than 0, set it to 1
                    uomFactor = uomFactor < 0 ? 1 : uomFactor;

                    ResultUnitPrice = (UnitPriceForPricingUOM * uomFactor).ToString();
                    //*************************************************************************************************
                    // Vivensas changes ends over here
                    //**************************************************************************************************
                }

                if (string.IsNullOrEmpty(ResultUnitPrice))
                {
                    //Sathish commented the below part of code
                    //Though we are not calculaing the ResultUnitPrice we will be sending the UnitPriceForPricingUOM the which is already calculted for this item
                    //Logger.Error("UOM Convertion Factor is not Available for ItemId:" + ItemId + ",PriceUOM:" + PriceUOM + ",AppliedUOM:" + AppliedUOM + ",UnitPriceForPricingUOM:" + UnitPriceForPricingUOM.ToString());
                    return UnitPriceForPricingUOM;
                }
                else
                {
                    return Convert.ToDecimal(ResultUnitPrice);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetPriceByUomFactor][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:GetPriceByUomFactor]");
            return PriceUom;
        }

        public DataTable GetAllOpenOrdersForItem(string ItemNumber)
        {
            DataTable dt = new DataTable();
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:GetAllOpenOrdersForItem]");
            try
            {
                //unused code in inline
                //                dt = DbEngine.ExecuteDataSet(@"SELECT oh.Orderid,customerid,ct.ABALPH CustomerName,pk.Order_Qty_Primary_UOM,pk.Primary_UOM,pk.Picked_Qty_Primary_UOM FROM BUSDTA.Order_Detail od join BUSDTA.ORDER_HEADER oh on od.OrderID = oh.OrderID join BUSDTA.F0101 ct on ct.ABAN8=oh.CustomerId 
                //join busdta.PickOrder pk on oh.OrderID=pk.Order_ID and od.ItemId=pk.Item_Number where od.ItemId=" + ItemNumber + " and isnull(oh.OrderStateId,'')!=" + ActivityKey.OrderDelivered.GetStatusIdFromDB() + "").Tables[0];
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetAllOpenOrdersForItem][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:GetAllOpenOrdersForItem]");
            return dt;
        }


        public ObservableCollection<PriceOvrCodes> GetPriceOvrCodes(string UnitPriceFlag = "0")
        {
            ObservableCollection<PriceOvrCodes> codes = new ObservableCollection<PriceOvrCodes>();
            //codes.Add(new PriceOvrCodes { Id = 2, Name = "CPR" });
            //codes.Add(new PriceOvrCodes { Id = 3, Name = "PKN" });
            //codes.Add(new PriceOvrCodes { Id = 4, Name = "SMP" });
            //codes.Add(new PriceOvrCodes { Id = 5, Name = "TPR" });

            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:GetPriceOvrCodes]");
            try
            {
                DataTable dt = new DataTable();


                //                string query = @"select a.reasoncodeid,a.reasoncodedescription,b.drsphd from BUSDTA.ReasonCodeMaster a, BUSDTA.F0005 b where Trim(a.ReasonCode)=trim(b.DRKY) 
                //                                    and a.ReasonCodeType='PriceOverride' and b.DRSY='42' AND b.DRRT='RC' AND b.DRSPHD LIKE 'MOBILE%' ";
                //                if (UnitPriceFlag == "1")
                //                {
                //                    query += " and b.DRSPHD='mobilesamp'";
                //                }
                //                else
                //                {
                //                    query += " and b.DRSPHD <> 'mobilesamp'";
                //                }

                //                dt = DbEngine.ExecuteDataSet(query).Tables[0];

                dt = objReasonCodeManger.OrderManager_GetPriceOvrCodes(UnitPriceFlag);
                foreach (DataRow dr in dt.Rows)
                {
                    codes.Add(new PriceOvrCodes
                    {
                        Id = Convert.ToInt32(dr["ReasonCodeID"].ToString()),
                        Name = dr["reasoncodedescription"].ToString().Trim(),
                        Type = dr["drsphd"].ToString().Trim()
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetPriceOvrCodes][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:GetPriceOvrCodes]");
            return codes;
        }

        public void SaveVoidOrderReasonCode(string orderNo, int reasonId)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:SaveVoidOrderReasonCode]");
            try
            {
                string query = "update busdta.ORDER_HEADER set VoidReasonCodeId='{0}', UpdatedDatetime=now() where OrderID='{1}'";
                query = string.Format(query, reasonId.ToString(), orderNo.ToString());
                DbEngine.ExecuteNonQuery(query);
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][SaveVoidOrderReasonCode][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:SaveVoidOrderReasonCode]");
        }
        public static void UpdateOrderStatus(string orderNo, ActivityKey state, bool UpdateTable = true)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:UpdateOrderStatus]");
            try
            {
                if (UpdateTable)
                {
                    string query = "update busdta.ORDER_HEADER set OrderStateId={0}, UpdatedDatetime=now() where OrderID='{1}' and isnull(OrderStateId,'')!='" + ActivityKey.OrderDelivered.GetStatusIdFromDB() + "'";
                    query = string.Format(query, state.GetStatusIdFromDB(), orderNo);
                    DbEngine.ExecuteNonQuery(query);
                    query = string.Empty;
                    ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
                    SalesLogicExpress.Application.ViewModels.Order.OrderStatus = state;
                }

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][UpdateOrderStatus][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:UpdateOrderStatus]");
        }

        public List<string> ValidateUM(string ItemCode)
        {
            List<string> validUM = new List<string>();
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:ValidateUM]");
            try
            {
                DataTable dt = new DataTable();

                //dt = DbEngine.ExecuteDataSet("select   distinct(UMUM) UM from busdta.F41002,busdta.F4101 union " +
                //                                "select   distinct(UMRUM) UM from busdta.F41002,busdta.F4101 " +
                //                                "where umitm = imitm and rtrim(imlitm) = '" + ItemCode + "'").Tables[0];

                dt = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_ValidateUM(@ItemCode='" + ItemCode + "')").Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    validUM.Add(dr["UM"].ToString());

                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][ValidateUM][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:ValidateUM]");

            return validUM;
        }
        public string GetParent(int mnABNumber)
        {
            string txtParentDesc = null;
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:GetParent]");
            try
            {
                string txtShipTo = null;
                string txtParent = null;
                //txtParent = DbEngine.ExecuteScalar("SELECT AIAN8 from BUSDTA.F03012 where AIAN8 in (select MAPA8 from BUSDTA.F0150 where MAAN8=" + mnABNumber + " and MAOSTP='   ')");
                txtParent = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetParentDetails(@mnABNum = " + mnABNumber + ")");
                if (string.IsNullOrEmpty(txtParent))
                {
                    txtParent = txtShipTo;
                }

                //txtParentDesc = DbEngine.ExecuteScalar("select ABAN8 ||', '|| ABALPH AS ParentDesc from BUSDTA.F0101 join BUSDTA.F03012 on ABAN8=AIAN8 where rtrim(ltrim(ABAN8)) = '" + txtParent + "'");
                txtParentDesc = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetParentDescOrSoldToDetails(@StrValue = '" + txtParent + "',@ColumnDesc='ParentDesc')");
                if (string.IsNullOrEmpty(txtParentDesc))
                {
                    txtParentDesc = txtShipTo;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetParent][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:GetParent]");
            return txtParentDesc;
        }

        public string GetSoldTo(int mnABNumber)
        {
            string txtBillTo = null;
            string txtSoldTo = null;
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:GetSoldTo]");
            try
            { //txtBillTo = DbEngine.ExecuteScalar("SELECT AIAN8 from BUSDTA.F03012 where AIAN8 in (Select ABAN81 from BUSDTA.F0101 where ABAN8=" + mnABNumber + ")");
                txtBillTo = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetBillToDetail(@mnABNum = " + mnABNumber + ")");
                //cmd.Parameters.Add("@AN8", SADbType.Integer).Value = mnABNumber;

                //txtSoldTo = DbEngine.ExecuteScalar("select ABAN8 ||', '|| ABALPH AS SoldTo from BUSDTA.F0101 join BUSDTA.F03012 on ABAN8=AIAN8 where rtrim(ltrim(ABAN8)) = '" + txtBillTo + "'");
                txtSoldTo = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetParentDescOrSoldToDetails(@StrValue = '" + txtBillTo + "',@ColumnDesc='SoldTo')");

                if (string.IsNullOrEmpty(txtSoldTo))
                {
                    txtSoldTo = txtBillTo;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetSoldTo][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:GetSoldTo]");
            return txtSoldTo;
        }
        public static string GetOrderDetailLineNo(string OrderID, string ItemID)
        {

            string sOrderLineno = "";
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:GetOrderDetailLineNo]");
            try
            {
                //sOrderLineno = DbEngine.ExecuteScalar("SELECT LineID FROM BUSDTA.Order_Detail where orderId='" + OrderID + "' and LongItem='" + ItemID + "'");
                sOrderLineno = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetLineID(@OrderID = '" + OrderID + "',@ItemID = '" + ItemID + "')");
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetOrderDetailLineNo][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:GetOrderDetailLineNo]");
            return sOrderLineno;
        }
        public string GetHoldCode(string strBranchId)
        {
            string strBranch = "";
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:GetHoldCode]");
            try
            {
                //strBranch = DbEngine.ExecuteScalar("select AIHDAR from busdta.F03012 where aian8 = '" + strBranchId + "'");

                strBranch = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetHoldCode(@BranchId = '" + strBranchId + "')");
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetHoldCode][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:GetHoldCode]");
            return strBranch;
        }

        public string GetEnergySurcharge(string customerID, string invoiceTotal)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:GetEnergySurcharge]");
            string energySurcharge = null;
            try
            {
                energySurcharge = DbEngine.ExecuteScalar("CALL BUSDTA.calculateEnergySurcharge(CustomerNum = '" + customerID + "',OrderTotal =" + invoiceTotal + ")");
                //Test for empty string using string length
                //if (energySurcharge == null || energySurcharge == "")
                //{
                //    energySurcharge = "0.00";
                //}
                energySurcharge = string.IsNullOrEmpty(energySurcharge) ? "0.00" : energySurcharge;

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetEnergySurcharge][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:GetEnergySurcharge]");
            return energySurcharge;
        }

        public int UpdateNoSaleReason(string stopID, string customerID)
        {
            int result = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:UpdateNoSaleReason] [customerID=" + customerID + "]");
            try
            {
                string query = "update busdta.M56M0004  set RPRCID = NULL, RPACTID = '' where RPSTID = '{0}' ";
                query = string.Format(query, stopID);

                result = Helpers.DbEngine.ExecuteNonQuery(query);
                ResourceManager.QueueManager.QueueProcess("StopManagement", false);

            }
            catch (Exception ex)
            {
                Logger.Error("Page:OrderManager.cs,Method:UpdateNoSaleReason Parameter: stopID=" + stopID + "customerID=" + customerID + " Message: " + ex.StackTrace);
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:UpdateNoSaleReason][customerID=" + customerID + "]");
            return result;
        }

        public DataTable GetOrderInfo(string orderId)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:GetOrderInfo][Parameter:orderId:" + orderId + "]");
            DataTable items = new DataTable();
            try
            {
                //                items = DbEngine.ExecuteDataSet(
                //                               @"select oh.ordersignature, oh.OrderDate OrderDate,oh.EnergySurchargeAmt,oh.SalesTaxAmt,oh.OrderStateId,oh.CustomerReference1 CustomerPo,
                //                                im.IMSRP1  as SalesCat1, im.IMSRP5 as SalesCat5, im.IMSRP4 as SalesCat4,
                //                                od.OrderQty OrderQty,od.OrderUM UM,
                //                                im.IMLITM ItemNumber,im.IMDSC1 ItemDescription,od.UnitPriceAmt UnitPrice,od.ExtnPriceAmt ExtendedPrice, case od.PriceOverrideReasonCodeId when 0 then '0' else '1' end as PriceOverrideFlag
                //                                ,isnull(relatedorderid,0)  relatedorderid
                //                                from busdta.Order_Header oh 
                //                                join BUSDTA.Order_Detail od on oh.orderid=od.orderid 
                //                                join busdta.F4101 im on od.ItemId= im.IMITM
                //                                where oh.orderid=" + orderId).Tables[0];

                items = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetOrderInfoDetails(@orderid =" + orderId + ")").Tables[0];

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetOrderInfo][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:GetOrderInfo]");
            return items;
        }
        internal void UpdateOrderItemQty(List<OrderItem> orderItems, string OrderID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:UpdateOrderItemQty]");
            try
            {
                string query = string.Empty;
                foreach (OrderItem item in orderItems)
                {
                    try
                    {
                        query = "update busdta.order_Detail  set OrderQty ={0}, OrderUM='{1}' where OrderID='{2}' and ItemID='{3}';";
                        query = string.Format(query, item.OrderQty, item.UM, OrderID, item.ItemId);
                        Helpers.DbEngine.ExecuteNonQuery(query);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][UpdateOrderItemQty][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                        continue;
                    }
                }
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][UpdateOrderItemQty][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:UpdateOrderItemQty]");
        }
        public List<OrderItem> GetOrderItems(string OrderID)
        {
            List<OrderItem> items = new List<OrderItem>();
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:GetOrderItems]");
            try
            {
                //                string query = @"SELECT rtrim(IM.IMLITM) AS ItemNumber,IC.PrimaryUM,OD.ORDERUM AS UM, OD.* FROM BUSDTA.ORDER_detail OD 
                //                                LEFT OUTER JOIN BUSDTA.F4101 IM ON OD.ItemId= IM.IMITM 
                //                                left outer join busdta.ItemConfiguration IC on OD.ItemID = IC.ItemId 
                //                                where OrderID='{0}'";
                //string query = "SELECT rtrim(IM.IMLITM) AS ItemNumber,PO.Primary_UOM AS PrimaryUM,OD.ORDERUM AS UM--, OD.*";
                //query = "\n, (CASE WHEN isnull(PO.Order_Qty,0)>isnull(PO.Picked_Qty_Primary_UOM,0) THEN isnull(PO.Order_Qty,0) ELSE isnull(PO.Picked_Qty_Primary_UOM,0) END ) AS OrderQty";
                //query = query + "\nFROM BUSDTA.ORDER_detail OD ";
                //query = query + "\nLEFT OUTER JOIN BUSDTA.F4101 IM ON OD.ItemId= IM.IMITM ";
                //query = query + "\nLEFT OUTER JOIN busdta.ItemConfiguration IC on OD.ItemID = IC.ItemId ";
                //query = query + "\nLEFT OUTER JOIN BUSDTA.PickOrder PO ON PO.Order_Id=OD.OrderID AND IM.IMLITM=PO.Item_Number";
                //query = query + "\nwhere OrderID='" + OrderID + "'";
                string query = "CALL BUSDTA.SP_GetOrderItems(@OrderID='" + OrderID + "')";
                //query = string.Format(query, OrderID);
                DataSet dsOrderDetails = DbEngine.ExecuteDataSet(query);
                items = dsOrderDetails.GetEntityList<OrderItem>();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetOrderItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:GetOrderItems]");
            return items;
        }
        //internal static void UpdateOrderItemsAndInventory(string p, List<OrderItem> list)
        //{

        //}

        // Update JDE Information in Order Header & Order_Detail
        public int UpdateJDEInforAfterDelivery(string OrderNumber, Int32 JDEInvoiceNumber, string JDEInvoiceType)
        {
            int TransactionStatus = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:GetOrderItems]");
            try
            {
                // Update Order Header
                string MySQLQuery = "UPDATE BUSDTA.Order_Header oh SET oh.JDEInvoiceNumber='" + JDEInvoiceNumber + "'," +
                                     " oh.JDEOInvoiceType='" + JDEInvoiceType + "', oh.UpdatedDatetime=NOW(), oh.UpdatedBy='" + UserManager.UserId + "' " +
                                     " WHERE oh.OrderID='" + OrderNumber + "' AND oh.OrderStateId IS NOT NULL";
                TransactionStatus = Helpers.DbEngine.ExecuteNonQuery(MySQLQuery);
                MySQLQuery = string.Empty;

                if (TransactionStatus > 0)
                {
                    // Update Order Detail
                    MySQLQuery = "UPDATE BUSDTA.Order_Detail od SET od.JDEInvoiceNumber='" + JDEInvoiceNumber + "'," +
                                     " od.JDEOInvoiceType='" + JDEInvoiceType + "', od.UpdatedBy='" + UserManager.UserId + "', od.UpdatedDatetime=NOW()" +
                                     " WHERE od.OrderID='" + OrderNumber + "'";
                    TransactionStatus = Helpers.DbEngine.ExecuteNonQuery(MySQLQuery);

                    //// //Update Invoice Header
                    ////MySQLQuery = string.Empty;
                    ////MySQLQuery = "UPDATE BUSDTA.Invoice_Header ih SET ih.ARInvoiceNumber='" + JDEInvoiceNumber + "', " +
                    ////             " ih.UpdatedDatetime=NOW() WHERE ih.OrderID='" + OrderNumber + "'";
                    ////TransactionStatus = Helpers.DbEngine.ExecuteNonQuery(MySQLQuery);
                }
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetOrderItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:GetOrderItems]");
            return TransactionStatus;

        }

        public Int32 GetInvoiceNextNumber()
        {
            int lnval = 0;
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:GetOrderItems]");
            try
            {
                string val = new Managers.NumberManager().GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.JDEInvoiceNumber);
                lnval = Convert.ToInt32(val);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetOrderItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:GetOrderItems]");
            return lnval;
        }

        public string GetJDEOrderType()
        {
            string strJDEOrderType = string.Empty;
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:GetJDEOrderType]");
            try
            {
                Int32 CustomerId = Convert.ToInt32(PayloadManager.OrderPayload.Customer.CustomerNo);
                CustomerManager objCustMgr = new CustomerManager();

                DataTable dt = objCustMgr.GetCustomerDetailsForOrder(CustomerId.ToString());
                if (dt.Rows.Count > 0)
                {
                    string PaymentTerms = dt.Rows[0]["PaymentTerms"].ToString().Trim().ToLower();
                    if (PaymentTerms == "csh")
                        strJDEOrderType = "R3";
                    else
                        strJDEOrderType = "RI";
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetJDEOrderType][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:GetJDEOrderType]");
            return strJDEOrderType;
        }

        /* Centralization Business Order from various screens - 17.09.2016*/
        #region
        public void ARPay_VoidReceiptNew(string strVoidStatusId, string reasonCodeId, string customerId, string receiptId, string routeId)
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:ARPayVoidReceiptNew]");
            try
            {
                string strQuery = string.Format("UPDATE BUSDTA.Order_Header SET OrderStateId=" + strVoidStatusId + ", " +
                           " VoidReasonCodeId=(SELECT TOP 1 ReasonCodeId FROM BUSDTA.ReasonCodeMaster WHERE TRIM(ReasonCodeDescription)=TRIM('{0}')) " +
                           " WHERE CustShipToId={1} AND OrderID={2} AND OriginatingRouteID={3}", reasonCodeId, customerId, receiptId, routeId);
                DbEngine.ExecuteNonQuery(strQuery);
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][ARPayVoidReceiptNew][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:ARPayVoidReceiptNew]");
        }

        public void CreditProcess_AddCreditMemoNew(int ReceiptID, string StatusTypeID, string CustomerNo, string AdjustmentSchedule, string TaxExplanationCode, string Taxarea, string CreditReasonID, string CreditNote, decimal ConvertedCreditAmount, int StatusID)
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CreditProcess_AddCreditMemoNew]");
            try
            {

                // Order Header Table
                StringBuilder MyQuery = new StringBuilder();
                MyQuery.Append(" INSERT INTO BUSDTA.Order_Header(OrderID,OrderTypeId,OriginatingRouteID,Branch,CustShipToId,CustBillToId,CustParentId, ");
                MyQuery.Append(" TotalCoffeeAmt,TotalAlliedAmt,OrderTotalAmt,SalesTaxAmt,EnergySurchargeAmt,VarianceAmt,SurchargeReasonCodeId,JDEProcessID,SettlementID, ");
                MyQuery.Append(" JDEOrderNumber,JDEOrderCompany,JDEOrderType,JDEInvoiceCompany,JDEOInvoiceType,JDEZBatchNumber,PaymentTerms,CustomerReference1,CustomerReference2,");
                MyQuery.Append(" AdjustmentSchedule,TaxExplanationCode,TaxArea,ChargeOnAccount,HoldCommitted, ");
                MyQuery.Append(" OrderDate,CreditReasonCodeId, CreditMemoNote,InvoiceTotalAmt,OrderStateId,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime) ");
                MyQuery.Append(" VALUES('" + ReceiptID + "','" + StatusTypeID + "','" + CommonNavInfo.RouteID + "','" + GetBranch(ViewModels.CommonNavInfo.UserBranch) + "','" + CustomerNo + "','" + CustomerManager.GetCustomerBillTo(CommonNavInfo.Customer.customerNo) + "','" + CustomerNo + "', ");
                MyQuery.Append(" 0,0,0,0,0,0,0,0,0,");
                MyQuery.Append(" '" + ReceiptID + "','00800','C5','00800','CN','','N07','','','" + AdjustmentSchedule + "','" + TaxExplanationCode + "','" + Taxarea + "','','', ");
                MyQuery.Append(" GETDATE(), '" + CreditReasonID + "','" + CreditNote + "','" + ConvertedCreditAmount + "','" + StatusID + "','" + UserManager.UserId + "',GETDATE(),'" + UserManager.UserId + "',GETDATE())");
                DbEngine.ExecuteNonQuery(MyQuery.ToString());
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CreditProcess_AddCreditMemoNew][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CreditProcess_AddCreditMemoNew]");
        }
        public void CreditProcess_AddCreditMemoNewDetails(int ReceiptID, DataTable vOrderItemdetails, string CustomerNo, string CreditOrderItem, string JDEBranch, int isTaxable, string PriceOverrideFlag, string CreditReasonID, decimal ConvertedCreditAmount)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CreditProcess_AddCreditMemoNewDetails]");
            try
            {
                string strQuery = @"insert into BUSDTA.Order_Detail(
                        OrderID,LineID,JDEOrderLine,CustShipToID,ItemId,LongItem,LineType,
                        IsNonStock
                        ,SalesCat1,SalesCat2 ,SalesCat3 ,SalesCat4 ,SalesCat5 ,PurchasingCat1 ,PurchasingCat2 ,PurchasingCat3 ,PurchasingCat4 ,PurchasingCat5 ,
                        JDEOrderNumber,JDEOrderCompany,JDEOrderType,JDEInvoiceCompany,JDEOInvoiceType,JDEZBatchNumber,
                        JDEProcessID,SettlementID,ReturnHeldQty,ExtendedAmtVariance,TaxAmountAmtVariance,HoldCode,
                        OrderQty,OrderQtyInPricingUM,OrderUM,PricingUM,UnitPriceAmtInPriceUoM,UnitPriceOriginalAmtInPriceUM ,UnitPriceAmt ,ExtnPriceAmt,ItemSalesTaxAmt,
                        IsTaxable,PriceOverriden,PriceOverrideReasonCodeId, CreatedBy,UpdatedBy,CreatedDatetime,UpdatedDatetime
                        )values
                        (
                        '" + ReceiptID + @"'," +
                    "'" + 1 + "'," +
                    "'" + 1 + "'," +
                    "'" + CustomerNo + "'" +
                    "," + CreditOrderItem + @",'" +
                    vOrderItemdetails.Rows[0]["LongItm"] + "'" +
                    ",'" + vOrderItemdetails.Rows[0]["IBLNTY"] + "'" +
                    ",'" + vOrderItemdetails.Rows[0]["IsNonStock"] + "'" +
                    ",'" + vOrderItemdetails.Rows[0]["SalesCat1"] + "'" +
                    ",'" + vOrderItemdetails.Rows[0]["SalesCat2"] + "'" +
                    ",'" + vOrderItemdetails.Rows[0]["SalesCat3"] + "'" +
                    ",'" + vOrderItemdetails.Rows[0]["SalesCat4"] + "'" +
                    ",'" + vOrderItemdetails.Rows[0]["SalesCat5"] + "'" +
                    ",'" + vOrderItemdetails.Rows[0]["PurchasingCat1"] + "'" +
                    ",'" + vOrderItemdetails.Rows[0]["PurchasingCat2"] + "'" +
                    ",'" + vOrderItemdetails.Rows[0]["PurchasingCat3"] + "'" +
                    ",'" + vOrderItemdetails.Rows[0]["PurchasingCat4"] + "'" +
                    ",'" + vOrderItemdetails.Rows[0]["PurchasingCat5"] + "'" +
                    ",'" + ReceiptID + "'" +
                    ",'" + JDEBranch + "'" +
                    ",'C5'" +
                    ",'" + JDEBranch + "'" +
                    ",'CN'" +
                    ",''" +
                    ",''" +
                    ",'0'" +
                    ",'0'" +
                    ",'0'" +
                    ",'0'" +
                    ",''" +
                    "," + -1 + @"" +
                    "," + -1 + @"," +
                    "'" + vOrderItemdetails.Rows[0]["IMUOM1"] + @"'," +
                    "'" + vOrderItemdetails.Rows[0]["IMUOM1"] + @"'," +
                    "'" + ConvertedCreditAmount + @"'," +
                    "'" + ConvertedCreditAmount + @"',
                        " + ConvertedCreditAmount + @",
                        " + ConvertedCreditAmount + @",
                        " + 0 + @",         
                        " + isTaxable + @",
                        '" + PriceOverrideFlag + @"',
                        " + CreditReasonID + @",    
                        " + Managers.UserManager.UserId + @",   
                        " + Managers.UserManager.UserId + @",                        
                        now(),now()  
                        );
                        ";
                DbEngine.ExecuteNonQuery(strQuery);
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CreditProcess_AddCreditMemoNewDetails][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CreditProcess_AddCreditMemoNewDetails]");
        }

        public string CustActivity_GetInvoiceAmountTotal(string days, string RouteId, string customerID)
        {
            string ageAmount = string.Empty;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustActivity_GetInvoiceAmountTotal]");
            try
            {

                //string query = "select isnull(sum(InvoiceTotalAmt),0) InvoiceTotalAmt FROM BUSDTA.ORDER_HEADER \nwhere OriginatingRouteID={0} AND CustShipToID='{1}' and OrderStateId ={2} \n AND CreatedDatetime BETWEEN DATEADD(day, {3}, getdate()) AND DATEADD(day, {4}, getdate())";

                string query = "CALL BUSDTA.SP_GetInvoiceAmountTotaly({0},'{1}',{2} ,{3})";
                query = string.Format(query, RouteId, customerID, ActivityKey.OrderDelivered.GetStatusIdFromDB(), days);
                //switch (days)
                //{
                //    case "30":
                //        query = string.Format(query, RouteId, customerID, ActivityKey.OrderDelivered.GetStatusIdFromDB(), "-30", "0");
                //        break;
                //    case "90":
                //        query = string.Format(query, RouteId, customerID, ActivityKey.OrderDelivered.GetStatusIdFromDB(), "-90", "-31");
                //        break;
                //    default:
                //        break;
                //}
                ageAmount = DbEngine.ExecuteScalar(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustActivity_GetInvoiceAmountTotal][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustActivity_GetInvoiceAmountTotal]");
            return ageAmount;
        }


        public void ReplenishAddLoadPickViewModel_CompletePickExecute(int PickedQty, string OrderID, string ItemId, int ReplnDetailId)
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:ReplenishAddLoadPickViewModel_CompletePickExecute]");
            try
            {
                string queryUpdate = "update busdta.Order_Detail  set  ReturnHeldQty = ReturnHeldQty - '" + PickedQty + "' where orderid='" + OrderID + "' and itemid = '" + ItemId + "' and RelatedLineID = '" + ReplnDetailId + "'";
                DbEngine.ExecuteNonQuery(queryUpdate);
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][ReplenishAddLoadPickViewModel_CompletePickExecute][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:ReplenishAddLoadPickViewModel_CompletePickExecute]");
        }

        public void PreviewOrder_UpdateOrderAndNavigate(double TotalCoffeeAmt, double TotalAlliedAmt, double energysurcharge, double ordertotal, double salestax, double invoiceTotal, string strSurchargeReasonCode, int OrderId)
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:PreviewOrder_UpdateOrderAndNavigate]");
            try
            {
                //String Handling for memory Optimization  --Vignesh D
                string query = string.Empty; 
                 query = "update busdta.ORDER_HEADER set TotalCoffeeAmt =" + TotalCoffeeAmt + ", TotalAlliedAmt =" + TotalAlliedAmt +
                                     ", EnergySurchargeAmt =" + energysurcharge + ", OrderTotalAmt =" + ordertotal + ", SalesTaxAmt = " + salestax +
                                     ", InvoiceTotalAmt =" + invoiceTotal + ", SurchargeReasonCodeId ="
                                     + strSurchargeReasonCode
                                     + " where OrderID =" + OrderId;
                DbEngine.ExecuteNonQuery(query);
                query = string.Empty;
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][PreviewOrder_UpdateOrderAndNavigate][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:PreviewOrder_UpdateOrderAndNavigate]");
        }

        public void PreviewOrder_CalculateTax(string strItemTax, int OrderId, string itemId, string strFlag)
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:PreviewOrder_CalculateTax]");
            try
            {
                string updateTaxStatus = "";
                switch (strFlag)
                {
                    case "1":
                        updateTaxStatus = "update BUSDTA.Order_Detail set IsTaxable =1, ItemSalesTaxAmt =" + strItemTax + " where OrderID=" + "'" + OrderId + "'" + " and ItemId=" + "" + itemId + "";
                        break;
                    case "0":
                        updateTaxStatus = "update BUSDTA.Order_Detail set IsTaxable =0 where OrderID=" + "'" + OrderId + "'" + " and ItemId=" + "" + itemId + "";
                        break;
                    default:
                        break;
                }
                DbEngine.ExecuteNonQuery(updateTaxStatus);
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][PreviewOrder_CalculateTax][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:PreviewOrder_CalculateTax]");
        }

        public void PickOrder_DeliverToCustomerUpdateAndNavigate(int OrderId)
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:PickOrder_DeliverToCustomerUpdateAndNavigate]");
            try
            {
                DbEngine.ExecuteNonQuery("Update Busdta.Order_Header Set PaymentTerms='N30' where OrderID='" + OrderId + "'");
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][PickOrder_DeliverToCustomerUpdateAndNavigate][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:PickOrder_DeliverToCustomerUpdateAndNavigate]");
        }

        public DataSet OrderTemplate_AppendOrderHistory()
        {
            DataSet ds = new DataSet();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:OrderTemplate_AppendOrderHistory]");
            try
            {
                string query = "CALL BUSDTA.SP_GetOrderHeaderHistory(@CustomerNo = '" + SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo + "',@OrderStateId = " + ActivityKey.OrderDelivered.GetStatusIdFromDB() + ")";
                ds = DbEngine.ExecuteDataSet(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][OrderTemplate_AppendOrderHistory][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:OrderTemplate_AppendOrderHistory]");
            return ds;
        }

        public DataTable Delivery_GetAllTotals(int OrderId)
        {
            DataTable dt = new DataTable();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:Delivery_GetAllTotals]");
            try
            {
                //dt = DbEngine.ExecuteDataSet("select * from BUSDTA.ORDER_HEADER where  OrderID =" + "'" + OrderId + "'").Tables[0];
                dt = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetAllTotalsAmtToDelivery(@OrderID='" + OrderId + "')").Tables[0];
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][Delivery_GetAllTotals][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:Delivery_GetAllTotals]");
            return dt;
        }

        public void Settlement_UpdateSettlmentStatusAfterVerification(string settlementID)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:Settlement_UpdateSettlmentStatusAfterVerification]");
            try
            {
                DbEngine.ExecuteNonQuery("Update Busdta.Order_Header Set Busdta.Order_Header.UpdatedBy =" + UserManager.UserId + ",Busdta.Order_Header.UpdatedDatetime = getdate(),Busdta.Order_Header.OrderStateID=(SELECT DISTINCT  StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='STTLD')  " +
                " from busdta.M50012 inner join Busdta.Order_Header on busdta.M50012.TDREFHDRID = Busdta.Order_Header.OrderID and Busdta.Order_Header.OrderStateID NOT IN(SELECT DISTINCT TTID FROM BUSDTA.M5001 WHERE TTKEY IN ('VoidAtCashCollection ','VoidAtOrderEntry','VoidAtDeliverCustomer         ')) " +
                " where TDSTTLID   = " + settlementID + " and TDTYP in('Order','PickOrder','PickItem','Payment','PickComplete','OrderDelivered','CreditMemo','ReturnOrder')");
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][Settlement_UpdateSettlmentStatusAfterVerification][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:Settlement_UpdateSettlmentStatusAfterVerification]");
        }

        public void Settlement_UpdateSettlementIdForTransaction(string settlementID)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:Settlement_UpdateSettlementIdForTransaction]");
            try
            {
                DbEngine.ExecuteNonQuery("Update Busdta.Order_Header Set Busdta.Order_Header.SettlementID=busdta.M50012.TDSTTLID  " +
                         " from busdta.M50012 inner join Busdta.Order_Header on busdta.M50012.TDREFHDRID = Busdta.Order_Header.OrderID " +
                         " where TDSTTLID   = " + settlementID + " and TDTYP in('Order','PickOrder','PickItem','Payment','PickComplete','OrderDelivered','CreditMemo','ReturnOrder')");

                DbEngine.ExecuteNonQuery("Update Busdta.Order_Detail Set Busdta.Order_DETAIL.SettlementID=busdta.M50012.TDSTTLID  " +
                       " from busdta.M50012 inner join Busdta.Order_Detail on busdta.M50012.TDREFHDRID = Busdta.Order_Detail.OrderID " +
                       " where TDSTTLID   = " + settlementID + " and TDTYP in('Order','PickOrder','PickItem','Payment','PickComplete','OrderDelivered','CreditMemo','ReturnOrder')");
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][Settlement_UpdateSettlementIdForTransaction][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:Settlement_UpdateSettlementIdForTransaction]");
        }

        public float ServiceRouteManager_GetAmount(int RouteID, DateTime date, int VoidAtROEntry,
            int VoidAtROPick, int VoidCreditMemo, int SettledID, int JDInprog, int JDZFile, int JDOrdProc, int JDHistory, string strFlag)
        {
            float totAmt = 0;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustActivity_GetInvoiceAmountTotal]");
            try
            {
                System.Text.StringBuilder MyQuery = new System.Text.StringBuilder();
                MyQuery.Append("CALL BUSDTA.SP_GetTotalAmount(@RouteID='" + RouteID + "',@DT='" + date.ToString("yyyy-MM-dd") + "',@OrderStateIDs='" + VoidAtROEntry + "," + VoidAtROPick + "," + VoidCreditMemo + "," + SettledID + "," + JDInprog + "," + JDZFile + "," + JDOrdProc + "," + JDHistory + "',@flag='" + strFlag + "')");
                //switch (strFlag)
                //{
                //    case "Credit":
                //        MyQuery.Append("	SELECT ISNULL(SUM(InvoiceTotalAmt * -1),0) InvoiceTotalAmt FROM BUSDTA.Order_Header 	");
                //        MyQuery.Append("	WHERE OriginatingRouteID='" + RouteID + "' AND OrderDate='" + date.ToString("yyyy-MM-dd") + "' 	");
                //        MyQuery.Append("	AND OrderTypeId=(SELECT ISNULL(StatusTypeID,0) FROM BUSDTA.Status_Type WHERE StatusTypeCD='INMOBL')	");
                //        MyQuery.Append("    AND OrderStateID NOT IN(" + VoidAtROEntry + "," + VoidAtROPick + "," + SettledID + ") ");
                //        break;
                //    case "Allied":
                //        MyQuery.Append("	SELECT ISNULL(SUM(TotalAlliedAmt),0) TotalAlliedAmt FROM BUSDTA.Order_Header 	");
                //        MyQuery.Append("	WHERE OriginatingRouteID='" + RouteID + "' AND OrderDate='" + date.ToString("yyyy-MM-dd") + "' 	");
                //        MyQuery.Append("	AND OrderTypeId=(SELECT ISNULL(StatusTypeID,0) FROM BUSDTA.Status_Type WHERE StatusTypeCD='RETORD')	");
                //        MyQuery.Append("    AND OrderStateID NOT IN(" + VoidAtROEntry + "," + VoidAtROPick + "," + SettledID + ") ");
                //        break;
                //    case "Coffee":
                //        MyQuery.Append("	SELECT ISNULL(SUM(TotalCoffeeAmt),0) TotalCoffeeAmt FROM BUSDTA.Order_Header 	");
                //        MyQuery.Append("	WHERE OriginatingRouteID='" + RouteID + "' AND OrderDate='" + date.ToString("yyyy-MM-dd") + "' 	");
                //        MyQuery.Append("	AND OrderTypeId=(SELECT ISNULL(StatusTypeID,0) FROM BUSDTA.Status_Type WHERE StatusTypeCD='RETORD')	");
                //        MyQuery.Append("    AND OrderStateID NOT IN(" + VoidAtROEntry + "," + VoidAtROPick + "," + SettledID + ") ");
                //        break;
                //    case "Return":
                //        MyQuery.Append("	SELECT ISNULL(SUM(InvoiceTotalAmt),0) InvoiceTotalAmt FROM BUSDTA.Order_Header 	");
                //        MyQuery.Append("	WHERE OriginatingRouteID='" + RouteID + "' AND OrderDate='" + date.ToString("yyyy-MM-dd") + "' 	");
                //        MyQuery.Append("	AND OrderTypeId=(SELECT ISNULL(StatusTypeID,0) FROM BUSDTA.Status_Type WHERE StatusTypeCD='RETORD')	");
                //        MyQuery.Append("    AND OrderStateID NOT IN(" + VoidAtROEntry + "," + VoidAtROPick + "," + SettledID + ") ");
                //        break;
                //}

                totAmt = (float)Convert.ToDouble((Helpers.DbEngine.ExecuteScalar(MyQuery.ToString())));
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustActivity_GetInvoiceAmountTotal][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustActivity_GetInvoiceAmountTotal]");
            return totAmt;
        }

        public DataTable ServiceRoute_GetTotalSalesAmountIncludeCredit(int RouteID, DateTime date, int SettledID,
            int VoidAtCashCollection, int VoidAtOrderEntry, int VoidAtDeliverCustomer, int VoidCreditMemo,
            int JDInprog, int JDZFile, int JDOrdProc, int JDHistory)
        {
            DataTable dt = new DataTable();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:ServiceRoute_GetTotalSalesAmountIncludeCredit]");
            try
            {
                System.Text.StringBuilder MyQuery = new System.Text.StringBuilder();
                //MyQuery.Append("	SELECT ISNULL(SUM(InvoiceTotalAmt),0) TotalSales,ISNULL(SUM(TotalCoffeeAmt),0) TotalCoffeeAmt,ISNULL(SUM(TotalAlliedAmt),0) TotalAlliedAmt FROM BUSDTA.Order_Header 	");
                //MyQuery.Append("	WHERE OriginatingRouteID='" + RouteID + "' AND OrderDate='" + date.ToString("yyyy-MM-dd") + "' 	");
                //MyQuery.Append("	AND OrderTypeId=(SELECT ISNULL(StatusTypeID,0) FROM BUSDTA.Status_Type WHERE StatusTypeCD='SALORD')	");
                //MyQuery.Append("    AND OrderStateID NOT IN(" + SettledID + "," + VoidAtCashCollection + "," + VoidAtOrderEntry + "," + VoidAtDeliverCustomer + ") ");
                MyQuery.Append("CALL BUSDTA.SP_GetTotalSalesAmountIncludeCredit(@RouteID='" + RouteID + "',@DT='" + date.ToString("yyyy-MM-dd") + "' ,@OrderStateIDs='" + SettledID + "," + VoidAtCashCollection + "," + VoidAtOrderEntry + "," + VoidAtDeliverCustomer + "," + VoidCreditMemo + "," + JDInprog + "," + JDZFile + "," + JDOrdProc + "," + JDHistory + "') ");
                dt = Helpers.DbEngine.ExecuteDataSet(MyQuery.ToString()).Tables[0];
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][ServiceRoute_GetTotalSalesAmountIncludeCredit][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:ServiceRoute_GetTotalSalesAmountIncludeCredit]");
            return dt;
        }
        /// <summary>
        /// Unused the function 
        /// Commented on 01-10-2016
        /// </summary>
        /// <param name="settlementID"></param>
        /// <returns></returns>
        //public DataSet ServiceRoute_GetTotalSalesAmountIncludeCredit(string settlementID)
        //{
        //    DataSet ds = new DataSet();
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:ServiceRoute_GetTotalSalesAmountIncludeCredit]");
        //    try
        //    {
        //        StringBuilder objQueryBuilder = new StringBuilder();
        //        objQueryBuilder.Append("select * from busdta.Order_Header where SettlementID = " + settlementID);
        //        ds = DbEngine.ExecuteDataSet(objQueryBuilder.ToString());

        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][ServiceRoute_GetTotalSalesAmountIncludeCredit][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:ServiceRoute_GetTotalSalesAmountIncludeCredit]");
        //    return ds;
        //}

        /// <summary>
        /// Unused the function 
        /// Commented on 01-10-2016
        /// </summary>
        /// <param name="settlementID"></param>
        /// <returns></returns>
        //public DataSet CustomerReturnsManager_GetOrderDetails(int OrderId)
        //{
        //    DataSet ds = new DataSet();
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_GetOrderDetails]");
        //    try
        //    {
        //        //                string query = @"select oh.*, (SELECT if(isnull(LTRIM(RTRIM(M.TDSTTLID)),0) > 0) then 1 else 0 AS STTID FROM BUSDTA.M50012 M WHERE 
        //        //                                 TDSTAT='ORDERRETURNED' AND TDREFHDRID = " + OrderId + " ) as IsSettled " +
        //        //                                 " from busdta.Order_Header oh where orderid = " + OrderId + "";
        //        //                ds = DbEngine.ExecuteDataSet(query);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_GetOrderDetails][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_GetOrderDetails]");
        //    return ds;
        //}

        //public bool CustomerReturnsManager_CreateReturnOrder(string ReturnOrderID)
        //{
        //    bool orderExists = false;
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_CreateReturnOrder]");
        //    try
        //    {
        //        string query = "select count(OrderID) from BUSDTA.ORDER_HEADER where OrderID='{0}'";
        //        query = string.Format(query, ReturnOrderID);
        //        orderExists = Convert.ToBoolean(Convert.ToInt32(DbEngine.ExecuteScalar(query)));
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_CreateReturnOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_CreateReturnOrder]");
        //    return orderExists;
        //}

        public void CustomerReturnsManager_CreateReturnOrderUpdate(decimal TotalCoffeeAmount, decimal TotalAlliedAmount, decimal OrderTotalAmount, float ROSalesTax, decimal InvoiceTotalAmt, decimal EnergySurcharge, int UserId, string ReturnOrderID)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_CreateReturnOrderUpdate]");
            try
            {
                string query = @"Update BUSDTA.ORDER_HEADER set 
                                    TotalCoffeeAmt='{0}'
                                    ,TotalAlliedAmt='{1}'
                                    ,OrderTotalAmt='{2}'
                                    ,SalesTaxAmt='{3}'
                                    ,InvoiceTotalAmt='{4}'
                                    ,EnergySurchargeAmt='{5}'
                                    ,HoldCommitted='0'
                                    ,UpdatedBy='{6}'
                                    ,UpdatedDatetime=getdate() 
                                    ,OrderStateId='{8}'
                                    where OrderID='{7}';";
                query = string.Format(query, TotalCoffeeAmount, TotalAlliedAmount, OrderTotalAmount,
                    ROSalesTax, InvoiceTotalAmt, EnergySurcharge,
                    UserId, ReturnOrderID, ActivityKey.CreateReturnOrder.GetStatusIdFromDB());
                DbEngine.ExecuteNonQuery(query);
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_CreateReturnOrderUpdate][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_CreateReturnOrderUpdate]");
        }

        public void CustomerReturnsManager_CreateReturnOrderInsert_details(string ReturnOrderID, short ival, string CustomerNo, DataTable vOrderItemdetails, ReturnItem item, string JDEBranch, decimal ldcjdeUOMConv, decimal ldcloadprice, string isTaxable)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_CreateReturnOrderInsert_details]");
            try
            {
                string query = @"INSERT INTO BUSDTA.Order_Detail (OrderID,LineID,JDEOrderLine,CustShipToID,ItemId,LongItem,LineType,IsNonStock
                                     ,SalesCat1,SalesCat2 ,SalesCat3 ,SalesCat4 ,SalesCat5 ,PurchasingCat1 ,PurchasingCat2 ,PurchasingCat3 ,PurchasingCat4 ,PurchasingCat5 ,RelatedOrderID,RelatedLineID,
                                     JDEOrderNumber,JDEOrderCompany,JDEOrderType,JDEInvoiceCompany,JDEOInvoiceType,JDEZBatchNumber,
                                    JDEProcessID,SettlementID,ExtendedAmtVariance,TaxAmountAmtVariance,HoldCode,
                                    OrderQty,OrderQtyInPricingUM,OrderUM,PricingUM,UnitPriceAmtInPriceUoM,UnitPriceOriginalAmtInPriceUM ,UnitPriceAmt,
                                    ExtnPriceAmt,ItemSalesTaxAmt,PriceOverriden,PriceOverrideReasonCodeId,IsTaxable,
                                    ReturnHeldQty,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime) " +
                                     " VALUES( " + ReturnOrderID + "," + ival + "," + ival + "," + CustomerNo + "," + item.ItemId + ",'" +
                                      vOrderItemdetails.Rows[0]["LongItm"] + "'" +
                                        ",'" + vOrderItemdetails.Rows[0]["IBLNTY"] + "'" +
                                        ",'" + vOrderItemdetails.Rows[0]["IsNonStock"] + "'" +
                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat1"] + "'" +
                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat2"] + "'" +
                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat3"] + "'" +
                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat4"] + "'" +
                                        ",'" + vOrderItemdetails.Rows[0]["SalesCat5"] + "'" +
                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat1"] + "'" +
                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat2"] + "'" +
                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat3"] + "'" +
                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat4"] + "'" +
                                        ",'" + vOrderItemdetails.Rows[0]["PurchasingCat5"] + "'" +
                                        ",'" + item.OrderID + "'" +
                                        ",'" + ival + "'" +
                                        ",'" + ReturnOrderID + "'" +
                                        ",'" + JDEBranch + "'" +
                                        ",'CO'" +
                                        ",'" + JDEBranch + "'" +
                                        ",'RM'" +
                                        ",''" +
                                        ",''" +
                                        ",'0'" +
                                        ",'0'" +
                                        ",'0'" +
                                        ",''," +
                                        item.ReturnQty +
                                        ",'" + ldcjdeUOMConv * item.ReturnQty + "','" +
                                         item.UM + "'," +
                                         "'" + item.UMPrice + "'," +
                                          "'" + (item.OrderQty * ldcjdeUOMConv) * ldcloadprice + @"'," +
                                        item.UnitPrice + "," + item.UnitPrice + "," + item.ROExtendedPrice + "," + item.TaxAmount + "," +
                                      " 1,5," + isTaxable + "," + item.NonSellableQty + "," + UserManager.UserId + "  ,getdate()," + UserManager.UserId + ",getdate())";
                DbEngine.ExecuteNonQuery(query);
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_CreateReturnOrderInsert_details][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_CreateReturnOrderInsert_details]");
        }

        public void CustomerReturnsManager_CreateReturnOrderUpdate_Detail(string UnitPriceOriginalAmtInPriceUM, string UnitPriceAmtInPriceUoM, string UnitPriceAmt, string ReturnOrderID, string ItemId, int OrderID)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_CreateReturnOrderUpdate_Detail]");
            try
            {
                StringBuilder MyQuery = new StringBuilder();
                MyQuery.Append(" UPDATE BUSDTA.Order_Detail SET UnitPriceOriginalAmtInPriceUM ='" + UnitPriceOriginalAmtInPriceUM + "', ");
                MyQuery.Append(" UnitPriceAmtInPriceUoM ='" + UnitPriceAmtInPriceUoM + "', ");
                MyQuery.Append(" UnitPriceAmt ='" + UnitPriceAmt + "' ");
                MyQuery.Append(" WHERE OrderID='" + ReturnOrderID + "' AND ItemID='" + ItemId + "' AND RelatedOrderID='" + OrderID + "'");
                //Remove Unused locals
                //int TranResult = DbEngine.ExecuteNonQuery(MyQuery.ToString());
                DbEngine.ExecuteNonQuery(MyQuery.ToString());
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_CreateReturnOrderUpdate_Detail][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_CreateReturnOrderUpdate_Detail]");
        }

        public DataTable CustomerReturnsManager_UpdatePriceDetails(int OrderID, string ItemId)
        {
            DataTable dt = new DataTable();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_UpdatePriceDetails]");
            try
            {
                dt = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetUpdatePriceDetails(@OrderId='" + OrderID + "',@ItemID='" + ItemId + "')").Tables[0];
                //dt = DbEngine.ExecuteDataSet("SELECT UnitPriceAmtInPriceUoM,UnitPriceOriginalAmtInPriceUM ,UnitPriceAmt FROM BUSDTA.Order_Detail WHERE OrderID='" + OrderID + "' AND ItemID='" + ItemId + "' ").Tables[0];
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_UpdatePriceDetails][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_UpdatePriceDetails]");
            return dt;
        }
        public void CustomerReturnsManager_CreateReturnOrderInsert_Header(string ReturnOrderID, ReturnOrderPayload clsReturnOrder, DataTable dt, decimal ldcShipToParent)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_CreateReturnOrderInsert_Header]");
            try
            {
                var Oh_query = @" INSERT INTO BUSDTA.ORDER_HEADER (OrderID,OriginatingRouteID,Branch,OrderTypeId,CustShipToId,CustBillToId,
                                      CustParentId,OrderDate,TotalCoffeeAmt,TotalAlliedAmt," +
                                    " OrderTotalAmt,SalesTaxAmt,InvoiceTotalAmt,EnergySurchargeAmt,SurchargeReasonCodeId,OrderStateId,OrderSignature,ChargeOnAccountSignature,VoidReasonCodeId,ChargeOnAccount,HoldCommitted," +
                                    " JDEOrderNumber,JDEOrderCompany,JDEOrderType,JDEInvoiceCompany,JDEOInvoiceType,JDEZBatchNumber,CustomerReference1,CustomerReference2, JDEProcessID,SettlementID,VarianceAmt, " +
                                    " CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime,  PaymentTerms,  AdjustmentSchedule,TaxExplanationCode,TaxArea)" +
                                    " VALUES(" + ReturnOrderID + "," + CommonNavInfo.RouteID + ",'" + GetBranch(ViewModels.CommonNavInfo.UserBranch) + "'," + StatusTypesEnum.RETORD.GetStatusIdFromDB() + "," +
                                    clsReturnOrder.Customer.CustomerNo + "," + "'" + dt.Rows[0]["BillTo"] + "'," +
                                    "'" + ldcShipToParent + "'," +
                                              "getdate()," + clsReturnOrder.TotalCoffeeAmount + "," + clsReturnOrder.TotalAlliedAmount + "," +
                                              "" + clsReturnOrder.OrderTotalAmount + "," + clsReturnOrder.ROSalesTax + "," + clsReturnOrder.InvoiceTotalAmt + "" +
                                              "," + clsReturnOrder.EnergySurcharge + ",0,null,NULL,NULL,NULL,NULL,'0'," +
                                              " '" + ReturnOrderID + "','00800','CO','00800','RM','','','','0','0','0', " +
                                              " " + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate() " + ", '" + dt.Rows[0]["PaymentTerms"] + "', '" +
                                                dt.Rows[0]["AdjustmentSchedule"] + "' ,'" + dt.Rows[0]["TaxExplanationCode"] + "','" + dt.Rows[0]["Taxarea"] + "') ";
                DbEngine.ExecuteNonQuery(Oh_query);
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_CreateReturnOrderInsert_Header][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_CreateReturnOrderInsert_Header]");
        }

        public void CustomerReturnsManager_StatusVsOrder(ReturnOrderPayload clsReturnOrder, int lnStateID, string VoidReasonCodeID, string strStatus, decimal EnergyCharge = 0, string ReturnOrderID = "")
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_StatusVsOrder]");
            try
            {
                switch (strStatus)
                {
                    case "Void":
                        DbEngine.ExecuteNonQuery(@"update busdta.ORDER_HEADER set OrderStateId = " + lnStateID + "," +
                                                                            "TotalCoffeeAmt = " + clsReturnOrder.TotalCoffeeAmount + "," +
                                                                            "TotalAlliedAmt = " + clsReturnOrder.TotalAlliedAmount + "," +
                                                                            "OrderTotalAmt = " + clsReturnOrder.OrderTotalAmount + "," +
                                                                            "SalesTaxAmt = " + clsReturnOrder.ROSalesTax + "," +
                                                                            "InvoiceTotalAmt = " + clsReturnOrder.InvoiceTotalAmt + "," +
                                                                            "EnergySurchargeAmt = " + (clsReturnOrder.IsReturnOrder == true ? clsReturnOrder.EnergySurcharge : 0) + "," +
                                                                                                   "VoidReasonCodeId = '" + VoidReasonCodeID + "' " +
                                                                                                   " where OrderID = " + clsReturnOrder.ReturnOrderID + " and OriginatingRouteID = " + CommonNavInfo.RouteID + "");
                        break;
                    case "Hold":
                        DbEngine.ExecuteNonQuery(@"update busdta.ORDER_HEADER set 
                                                                            OrderStateId = " + lnStateID + ", " +
                                                                            "TotalCoffeeAmt = " + clsReturnOrder.TotalCoffeeAmount + "," +
                                                                            "TotalAlliedAmt = " + clsReturnOrder.TotalAlliedAmount + "," +
                                                                            "OrderTotalAmt = " + clsReturnOrder.OrderTotalAmount + "," +
                                                                            "SalesTaxAmt = " + clsReturnOrder.ROSalesTax + "," +
                                                                            "InvoiceTotalAmt = " + clsReturnOrder.InvoiceTotalAmt + ", " +
                                                                            "EnergySurchargeAmt = " + (clsReturnOrder.IsReturnOrder == true ? clsReturnOrder.EnergySurcharge : 0) + " " +
                                                                                             " where OrderID = " + clsReturnOrder.ReturnOrderID + " and OriginatingRouteID = " + CommonNavInfo.RouteID + "");
                        break;
                    case "Save":
                        var OH_query = @"update busdta.ORDER_HEADER set  OrderStateId =" + lnStateID + "," +
                                  "TotalCoffeeAmt = " + clsReturnOrder.TotalCoffeeAmount + "," +
                                  "TotalAlliedAmt = " + clsReturnOrder.TotalAlliedAmount + "," +
                                  "OrderTotalAmt = " + clsReturnOrder.OrderTotalAmount + "," +
                                  "SalesTaxAmt = " + clsReturnOrder.ROSalesTax + "," +
                                  "InvoiceTotalAmt = " + clsReturnOrder.InvoiceTotalAmt + "," +
                                  "EnergySurchargeAmt = " + EnergyCharge + "" +
                                 " where OrderID = " + ReturnOrderID + " and OriginatingRouteID = " + CommonNavInfo.RouteID + "";
                        int affectedRecords = DbEngine.ExecuteNonQuery(OH_query);
                        break;
                }


                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_StatusVsOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_StatusVsOrder]");
        }


        public void CustomerReturnsManager_VoidOrHoldOrder_Detail(ReturnItem item, string ReturnOrderID)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_CreateReturnOrderUpdate_Detail]");
            try
            {
                var OD_query = @"update busdta.order_detail set " +
                             "OrderQty = " + item.ReturnQty + "," +
                             "OrderUM = '" + item.UM + "'," +
                             "ExtnPriceAmt = " + item.ROExtendedPrice + "," +
                             "IsTaxable = " + (item.ROTaxAmount > 0 ? '1' : '0') + "," +
                             "ReturnHeldQty = " + item.NonSellableQty + ", " +
                             "ItemSalesTaxAmt = " + item.TaxAmount + " " +
                             "where OrderID=" + ReturnOrderID + " and itemid =  " + item.ItemId + " and relatedorderid = " + item.TransactionDetailID + "";
                DbEngine.ExecuteNonQuery(OD_query);
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_CreateReturnOrderUpdate_Detail][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_CreateReturnOrderUpdate_Detail]");
        }

        public void CustomerReturnsManager_SaveReturnOrder(ReturnItem item, string ReturnOrderID, decimal ldcUOMConv)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_SaveReturnOrder]");
            try
            {
                var OD_query = @"update busdta.order_detail set " +
                       "OrderQty = " + item.ReturnQty + "," +
                       "OrderUM = '" + item.UM + "'," +
                       "ExtnPriceAmt = " + item.ROExtendedPrice + "," +
                       "IsTaxable = " + (item.ROTaxAmount > 0 ? '1' : '0') + "," +
                       "ReturnHeldQty = " + item.NonSellableQty + ", " +
                       "ItemSalesTaxAmt = " + item.TaxAmount +
                       ",OrderQtyInPricingUM ='" + ldcUOMConv * item.ReturnQty + "' " +
                       "where ItemID =" + item.ItemId + " and OrderID=" + ReturnOrderID + " and RelatedOrderID ='" + item.TransactionDetailID + "'";
                DbEngine.ExecuteNonQuery(OD_query);
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_SaveReturnOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_SaveReturnOrder]");
        }

        public void CustomerReturnsManager_SaveReturnOrder_Header(int lnStateID, int OrderID)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_SaveReturnOrder_Header]");
            try
            {
                var chngQuery = @"update busdta.ORDER_HEADER set OrderStateId = " + lnStateID + "" +
                               " where OrderID = " + OrderID + " and OriginatingRouteID = " + CommonNavInfo.RouteID + "";
                DbEngine.ExecuteNonQuery(chngQuery);
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_SaveReturnOrder_Header][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_SaveReturnOrder_Header]");
        }
        /// <summary>
        /// Unused the function 
        /// Commented on 01-10-2016
        /// </summary>
        /// <param name="ReturnOrderID"></param>
        /// <returns></returns>
        //public string CustomerReturnsManager_SaveOrHoldReturnOrder(int ReturnOrderID)
        //{
        //    string OrderStateID = string.Empty;
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_SaveOrHoldReturnOrder]");
        //    try
        //    {
        //        OrderStateID = DbEngine.ExecuteScalar(@"select OrderStateId from BUSDTA.ORDER_HEADER where OrderID = " + ReturnOrderID + " and RouteID = " + CommonNavInfo.RouteID + "").Trim();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_SaveOrHoldReturnOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_SaveOrHoldReturnOrder]");
        //    return OrderStateID;
        //}

        //public void CustomerReturnsManager_SaveOrHoldReturnOrder_Update(int ReturnOrderID, int lnStateID)
        //{
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_SaveOrHoldReturnOrder]");
        //    try
        //    {
        //        DbEngine.ExecuteNonQuery(@"update busdta.ORDER_HEADER set OrderStateId = " + lnStateID + " where OrderID = " + ReturnOrderID + " and RouteID = " + CommonNavInfo.RouteID + "");
        //        ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_SaveOrHoldReturnOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_SaveOrHoldReturnOrder]");
        //}

        //        public void CustomerReturnsManager_VoidReturnOrder_UpdateDetail(int ReturnOrderID, int OrderDetailID)
        //        {
        //            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_SaveOrHoldReturnOrder]");
        //            try
        //            {
        //                var r_query = @"update busdta.order_detail set OrderQty = 0 , ReturnHeldQty = 0 where OrderDetailID = '{0}' and OrderID = '{1}' 
        //                                        and RouteID = '{2}'";
        //                var query = string.Format(r_query, OrderDetailID, ReturnOrderID, CommonNavInfo.RouteID);
        //                DbEngine.ExecuteDataSet(query);
        //                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
        //            }
        //            catch (Exception ex)
        //            {
        //                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_SaveOrHoldReturnOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //            }
        //            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_SaveOrHoldReturnOrder]");
        //        }

        //public void CustomerReturnsManager_VoidReturnOrder_InsertHeader(int ReturnOrderID, int StatusID, ReturnOrderPayload clsReturnOrderPayload, int lnActivity)
        //{
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_SaveOrHoldReturnOrder]");
        //    try
        //    {
        //        var Oh_query = @" INSERT INTO BUSDTA.ORDER_HEADER (OrderID,RouteID,OrderTypeId,CustomerId,OrderDate,TotalCoffeeAmt,TotalAlliedAmt," +
        //                               " OrderTotalAmt,SalesTaxAmt,InvoiceTotalAmt,EnergySurchargeAmt,SurchargeReasonCodeId,OrderStateId,OrderSign,ChargeOnAccountSign,VoidReasonCodeId,ChargeOnAccount," +
        //                               " HoldCommitted,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime)" +
        //                               " VALUES(" + ReturnOrderID + "," + CommonNavInfo.RouteID + "," + StatusID + "," + clsReturnOrderPayload.Customer.CustomerNo + "," +
        //                                         "getdate()," + clsReturnOrderPayload.TotalCoffeeAmount + "," + clsReturnOrderPayload.TotalAlliedAmount + "," +
        //                                         "" + clsReturnOrderPayload.OrderTotalAmount + "," + clsReturnOrderPayload.ROSalesTax + "," + clsReturnOrderPayload.InvoiceTotalAmt + "" +
        //                                         "," + clsReturnOrderPayload.EnergySurcharge + ",null," + lnActivity + ",NULL,NULL,NULL,NULL,'0'," +
        //                                         "" + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate()) ";

        //        DbEngine.ExecuteNonQuery(Oh_query);
        //        ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_SaveOrHoldReturnOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_SaveOrHoldReturnOrder]");
        //}


        //public void CustomerReturnsManager_VoidReturnOrder_InsertDetail(int ReturnOrderID, string isTaxable, ReturnItem item)
        //{
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_VoidReturnOrder_InsertDetail]");
        //    try
        //    {
        //        var query = @"INSERT INTO BUSDTA.Order_Detail (OrderID,RouteID,ItemId,OrderQty,OrderUM,UnitPriceAmt,ExtnPriceAmt,ItemSalesTaxAmt,PriceOverrideReasonCodeId,IsTaxable,ReturnHeldQty,CreatedDatetime,UpdatedBy,UpdatedDatetime) " +
        //                             " VALUES( " + ReturnOrderID + "," + CommonNavInfo.RouteID + "," + item.ItemId + "," + item.ReturnQty + ",'" + item.UM + "'," + item.UnitPrice + "," + item.ExtendedPrice + "," + item.TaxAmount + "," +
        //                             " NULL," + isTaxable + "," + item.NonSellableQty + ",getdate()," + UserManager.UserId + ",getdate())";
        //        DbEngine.ExecuteNonQuery(query);
        //        ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_VoidReturnOrder_InsertDetail][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_VoidReturnOrder_InsertDetail]");
        //}

        //public void CustomerReturnsManager_UpdateOrderStatus(int lnReturnActivity, int lnActivity, string orderNo)
        //{
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_UpdateOrderStatus]");
        //    try
        //    {
        //        string query = "update busdta.ORDER_HEADER set OrderStateId={0}, UpdatedDatetime=now() where OrderID='{1}' and isnull(OrderStateId,'')!='" + lnReturnActivity + "'";
        //        query = string.Format(query, lnActivity, orderNo);
        //        DbEngine.ExecuteNonQuery(query);
        //        ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_UpdateOrderStatus][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_UpdateOrderStatus]");
        //}

        //Unused function Vignesh.S
        //public string CustomerReturnsManager_CheckROItems(int ReturnOrderID)
        //{
        //    string lnOrderDetCount = string.Empty;
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_CheckROItems]");
        //    try
        //    {
        //        //Unused the Function
        //        ///lnOrderDetCount = DbEngine.ExecuteScalar(@"select count(1) from busdta.Order_detail where orderid = " + ReturnOrderID + "");
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_CheckROItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_CheckROItems]");
        //    return lnOrderDetCount;
        //}

        //public void CustomerReturnsManager_UpdateCommitted(int ReturnOrderID)
        //{
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_UpdateCommitted]");
        //    try
        //    {
        //        var OH_query = @"update busdta.Order_Header set HoldCommitted = 0 where orderid = " + ReturnOrderID + " AND RouteId='" + CommonNavInfo.RouteID + "'";
        //        DbEngine.ExecuteNonQuery(OH_query);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_UpdateCommitted][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_UpdateCommitted]");
        //}
        //unused function commented by Vignesh.S
        //public string CustomerReturnsManager_GetROStatusTypeId(int salesOrderID)
        //{
        //    string result = string.Empty;
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_GetROStatusTypeId]");
        //    try
        //    {
        //        //Unused the  the Function
        //        //string query = @"select OrderTypeId from busdta.order_header where orderid = (select top 1 OrderID from BUSDTA.Order_Detail where RelatedOrderID = " + salesOrderID + ")";
        //        //result = DbEngine.ExecuteScalar(query);

        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_GetROStatusTypeId][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_GetROStatusTypeId]");
        //    return result;
        //}

        public string CustomerReturnsManager_IsHoldReturnItems(int OrderID, string Itemnum)
        {
            string lnResult = string.Empty;
            Logger.Info("[SalesLogicExpress.Application.Manager][OrderManager][Start:CustomerReturnsManager_IsHoldReturnItems]");
            try
            {
                lnResult = DbEngine.ExecuteScalar(@"CALL BUSDTA.SP_GetOrderStateForReturnItems(" + OrderID + "," + ActivityKey.HoldAtROEntry.GetStatusIdFromDB() + ",'" + Itemnum + "')");
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Manager][OrderManager][CustomerReturnsManager_IsHoldReturnItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Manager][OrderManager][End:CustomerReturnsManager_IsHoldReturnItems]");
            return lnResult;
        }

        public string CustomerReturnsManager_DeleteHoldReturnItems(int ReturnOrderID)
        {
            string lnResult = string.Empty;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_UpdateCommitted]");
            try
            {
                lnResult = DbEngine.ExecuteScalar(@"DELETE FROM BUSDTA.Order_Detail  WHERE  ORDERID=" + ReturnOrderID + "");
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_UpdateCommitted][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_UpdateCommitted]");
            return lnResult;
        }

        // Return Order - Delete Order Details before Insert.
        public int CustomerReturnsManager_DeleteReturnOrderItems(int OrderId)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_DeleteReturnOrderItems]");
            int Result = 0;
            try
            {
                Result = DbEngine.ExecuteNonQuery("delete from BUSDTA.Order_Detail where OrderID = " + OrderId.ToString());
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_DeleteReturnOrderItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_DeleteReturnOrderItems]");
            return Result;
        }

        public DataSet CustomerReturnsManager_IsOrderPartiallyReturn(int salesOrderID)
        {
            DataSet ds = new DataSet();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:CustomerReturnsManager_IsOrderPartiallyReturn]");
            try
            {
                //var query = "select OrderID,RelatedLineID,RelatedOrderID,orderqty FROM BUSDTA.Order_Detail where RelatedOrderID = " + salesOrderID + "";
                var query = "CALL BUSDTA.SP_GetOrderPartiallyReturn(@OrderID = " + salesOrderID + ")";
                ds = DbEngine.ExecuteDataSet(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][CustomerReturnsManager_IsOrderPartiallyReturn][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:CustomerReturnsManager_IsOrderPartiallyReturn]");
            return ds;
        }

        //public string InventoryManager_UpdateCommittedQuantity(int OrderId)
        //{
        //    string lnResult = string.Empty;
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:InventoryManager_UpdateCommittedQuantity]");
        //    try
        //    {
        //        string checkHold = "select HoldCommitted From busdta.ORDER_HEADER where OrderID = " + OrderId;
        //        lnResult = DbEngine.ExecuteScalar(checkHold);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][InventoryManager_UpdateCommittedQuantity][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:InventoryManager_UpdateCommittedQuantity]");
        //    return lnResult;
        //}

        public string InvoiceManager_CreateInvoice(string OrderId)
        {
            string lnResult = string.Empty;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:InvoiceManager_CreateInvoice]");
            try
            {
                lnResult = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetLowerPaymentTerms(@OrderId='" + OrderId + "')");
                /// lnResult = DbEngine.ExecuteScalar("SELECT LOWER(oh.PaymentTerms) FROM BUSDTA.Order_Header oh WHERE LOWER(oh.OrderId)='" + OrderId + "'");
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][InvoiceManager_CreateInvoice][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:InvoiceManager_CreateInvoice]");
            return lnResult;
        }

        public void InvoiceManager_UpdateInvoiceNumber(string InvoiceNumber, string OrderID)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][Start:InvoiceManager_CreateInvoice]");
            try
            {
                string Query = "UPDATE BUSDTA.Order_Header SET JDEInvoiceNumber ='" + InvoiceNumber + "' WHERE OrderID='" + OrderID + "'";
                DbEngine.ExecuteNonQuery(Query);
                Query = "UPDATE BUSDTA.Order_Detail SET JDEInvoiceNumber ='" + InvoiceNumber + "' WHERE OrderID='" + OrderID + "'";
                DbEngine.ExecuteNonQuery(Query);
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderManager][InvoiceManager_CreateInvoice][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderManager][End:InvoiceManager_CreateInvoice]");
        }
        #endregion
        //Add the Disposed function by Vignesh.S
        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                if (objReasonCodeManger != null)
                    objReasonCodeManger.Dispose();
                handle.Dispose();
            }
            disposed = true;
        }


    }
}
