﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;

namespace SalesLogicExpress.Application.Managers
{
    public static class RouteSettlementManager
    {
        private static readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.RouteSettlementManager");

        public static ObservableCollection<RouteSettlementModel> GetTransactions(bool showAllActivities)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][Start:GetTransactions]");
            //StringBuilder objQueryBuilder = new StringBuilder();
            //string strVerifyCodeId = DbEngine.ExecuteScalar("SELECT StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VERF'");

            //objQueryBuilder.Append("SELECT  ACT.TDPNTID, ACT.TDID AS ActivityID, ltrim(RTRIM(ACT.TDSTTLID)) AS  SettlementID, ACT.TDROUT AS  RouteID, ACT.TDTYP AS  ActivityType, ACT.TDCLASS AS  ActivityDetailClass, ");
            //objQueryBuilder.Append("ACT.TDDTLS AS  ActivityDetails, ACT.TDSTRTTM AS  ActivityStart, ACT.TDENDTM AS  ActivityEnd, ACT.TDSTID AS  StopInstanceID, ACT.TDAN8 AS  CustomerID, ");
            //objQueryBuilder.Append("ACT.TDPNTID AS  ActivityHeaderID, (CASE RS.Status WHEN " + strVerifyCodeId + " THEN (CASE WHEN ACT.TDSTAT IN ('VoidAtCashCollection', 'VoidAtDeliverCustomer', 'VoidAtOrderEntry', 'PaymentVoid', 'VoidCreditMemo', 'VoidExpense','VoidMoneyOrder') THEN 'VOIDED / SETTLED' ELSE 'SettlementVerified' END) ");
            //objQueryBuilder.Append("ELSE ACT.TDSTAT END) AS  ActivityStatus FROM busdta.M50012 ACT LEFT OUTER JOIN BUSDTA.Route_Settlement RS ON ACT.TDSTTLID=RS.SettlementId ");
            //objQueryBuilder.Append("WHERE ACT.TDTYP in ('Order','CreditMemo','OrderReturned','Expense','MoneyOrder','Quote','InvAdjustment') " + (showAllActivities ? "" : "AND ISNULL(RS.Status,0)<>" + strVerifyCodeId) + " AND ACT.TDPNTID=0 ");
            //objQueryBuilder.Append("UNION ");
            //objQueryBuilder.Append("SELECT  ACT.TDPNTID, ACT.TDID AS ActivityID, ltrim(RTRIM(ACT.TDSTTLID)) AS  SettlementID, ACT.TDROUT AS  RouteID, ACT.TDTYP AS  ActivityType, ACT.TDCLASS AS  ActivityDetailClass, ");
            //objQueryBuilder.Append("ACT.TDDTLS AS  ActivityDetails, ACT.TDSTRTTM AS  ActivityStart, ACT.TDENDTM AS  ActivityEnd, ACT.TDSTID AS  StopInstanceID, ACT.TDAN8 AS  CustomerID, ");
            //objQueryBuilder.Append("ACT.TDPNTID AS  ActivityHeaderID, (CASE RS.Status WHEN " + strVerifyCodeId + " THEN (CASE WHEN ACT.TDSTAT IN ('VoidAtCashCollection', 'VoidAtDeliverCustomer', 'VoidAtOrderEntry','PaymentVoid', 'VoidCreditMemo','VoidExpense','VoidMoneyOrder') THEN 'VOIDED / SETTLED' ELSE 'SettlementVerified' END) ");
            //objQueryBuilder.Append("ELSE ACT.TDSTAT END) AS  ActivityStatus FROM busdta.M50012 ACT LEFT OUTER JOIN BUSDTA.Route_Settlement RS ON ACT.TDSTTLID=RS.SettlementId ");
            //objQueryBuilder.Append("WHERE ACT.TDTYP in ('Payment','ReturnOrder','HeldReturn') " + (showAllActivities ? "" : "AND ISNULL(RS.Status,0)<>" + strVerifyCodeId) + " ORDER BY ActivityEnd DESC");

            List<RouteSettlementModel> activities = new List<RouteSettlementModel>();
            try
            {
                //DataSet result = Helpers.DbEngine.ExecuteDataSet(objQueryBuilder.ToString());
                int lnBool = 0;
                if (showAllActivities) lnBool = 1;
                DataSet result = Helpers.DbEngine.ExecuteDataSet("call busdta.SP_GetTransactionsInSettlement (" + lnBool + ")");

                if (result.HasData())
                {
                    activities = result.GetEntityList<RouteSettlementModel>();
                }

                result.Dispose();
                result = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteSettlementManager][GetTransactions][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][End:GetTransactions]");

            return new ObservableCollection<RouteSettlementModel>(activities);
        }

        public static ObservableCollection<RouteSettlementSettlementsModel> GetAllSettlements
        {
            get
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][Start:GetAllSettlements]");

                // TransactionID as ActivityID, RouteID,TransactionType as ActivityType,TransactionDetailClass as ActivityDetailClass,TransactionDetails as ActivityDetails,TransactionStart as ActivityStart,TransactionEnd as ActivityEnd,StopInstanceID,CustomerID,SettlementID,ParentTransactionID as ActivityHeaderID, TransactionStatus as ActivityStatus
                //            string query = @"select SettlementID ,SettlementNo,LTRIM(RTRIM(d.StatusTypeDESC)) as  [Status] , SettlementDateTime as 'Date',
                //                             Originator,Verifier,SettlementAmount,ExceptionAmount,[Comment] 
                //                             ,b.NTUser as'OriginatorName', c.NTUser as 'VerifierName' 
                //                             from busdta.Route_Settlement a LEFT join busdta.userprofilemaster b ON a.Originator =b.App_user_id 
                //                            LEFT join busdta.userprofilemaster c ON a.Verifier =c.App_user_id 
                //                            LEFT JOIN BUSDTA.Status_Type d ON a.Status=d.StatusTypeID order by SettlementDateTime DESC";
                List<RouteSettlementSettlementsModel> activities = new List<RouteSettlementSettlementsModel>();
                try
                {
                    //DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                    DataSet result = Helpers.DbEngine.ExecuteDataSet("call busdta.SP_GetAllSettlements");
                    if (result.HasData())
                    {
                        activities = result.GetEntityList<RouteSettlementSettlementsModel>();
                    }

                    result.Dispose();
                    result = null;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][RouteSettlementManager][GetAllSettlements][ExceptionStackTrace = " + ex.StackTrace + "]");

                }
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][End:GetAllSettlements]");

                return new ObservableCollection<RouteSettlementSettlementsModel>(activities);
            }
        }

        public static ObservableCollection<RouteSettlementModel> GetTransactionsSettlementID(string settlementID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][Start:GetTransactionsSettlementID][SettlementID='" + settlementID + "']");

            // TransactionID as ActivityID, RouteID,TransactionType as ActivityType,TransactionDetailClass as ActivityDetailClass,TransactionDetails as ActivityDetails,TransactionStart as ActivityStart,TransactionEnd as ActivityEnd,StopInstanceID,CustomerID,SettlementID,ParentTransactionID as ActivityHeaderID, TransactionStatus as ActivityStatus
            //string query = "select TDID as ActivityID,TDROUT as RouteID,TDTYP as ActivityType, " +
            //                              "  TDCLASS as ActivityDetailClass,TDDTLS as ActivityDetails," +
            //                               " TDSTRTTM as ActivityStart,TDENDTM as ActivityEnd," +
            //                               " TDSTID as StopInstanceID, TDAN8 as CustomerID," +
            //                               " ltrim(rtrim(TDSTTLID)) as SettlementID,TDPNTID as ActivityHeaderID," +
            //                               " TDSTAT as ActivityStatus from busdta.M50012 " +
            //                               " where  ActivityType in ('Order','Payment','MoneyOrder') and TDSTTLID= " + settlementID + "  " +
            //                               " ORDER BY TDENDTM DESC";
            string query = "CALL BUSDTA.SP_GetTransactionsSettlement(@SettlementId='" + settlementID + "')";
            List<RouteSettlementModel> activities = new List<RouteSettlementModel>();
            try
            {
                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    activities = result.GetEntityList<RouteSettlementModel>();
                }

                result.Dispose();
                result = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteSettlementManager][GetTransactionsSettlementID][SettlementID='" + settlementID + "'][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][End:GetTransactionsSettlementID][SettlementID='" + settlementID + "']");

            return new ObservableCollection<RouteSettlementModel>(activities);
        }

        public static DateTime? GetTransactionBeginningDateForSettlement(string settlementID, bool showAllActivities = false)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][Start:GetTransactionBeginningDateForSettlement]");
            StringBuilder objQueryBuilder = new StringBuilder();
            //string strVerifyCodeId = DbEngine.ExecuteScalar("SELECT StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VERF'");
            //objQueryBuilder.Append("SELECT  ACT.TDPNTID, ACT.TDID AS ActivityID, ltrim(RTRIM(ACT.TDSTTLID)) AS  SettlementID, ACT.TDROUT AS  RouteID, ACT.TDTYP AS  ActivityType, ACT.TDCLASS AS  ActivityDetailClass, ");
            //objQueryBuilder.Append("ACT.TDDTLS AS  ActivityDetails, ACT.TDSTRTTM AS  ActivityStart, ACT.TDENDTM AS  ActivityEnd, ACT.TDSTID AS  StopInstanceID, ACT.TDAN8 AS  CustomerID, ");
            //objQueryBuilder.Append("ACT.TDPNTID AS  ActivityHeaderID, (CASE RS.Status WHEN " + strVerifyCodeId + " THEN (CASE WHEN ACT.TDSTAT IN ('VoidAtCashCollection', 'VoidAtDeliverCustomer', 'VoidAtOrderEntry', 'PaymentVoid', 'VoidCreditMemo', 'VoidExpense','VoidMoneyOrder') THEN 'VOIDED / SETTLED' ELSE 'SettlementVerified' END) ");
            //objQueryBuilder.Append("ELSE ACT.TDSTAT END) AS  ActivityStatus FROM busdta.M50012 ACT LEFT OUTER JOIN BUSDTA.Route_Settlement RS ON ACT.TDSTTLID=RS.SettlementId ");
            //objQueryBuilder.Append("WHERE ACT.TDTYP in ('Order','CreditMemo','OrderReturned','Expense','MoneyOrder','Quote','InvAdjustment') " + (showAllActivities ? "" : "AND ISNULL(RS.Status,0)<>" + strVerifyCodeId) + " AND ACT.TDPNTID=0 ");
            //objQueryBuilder.Append("UNION ");
            //objQueryBuilder.Append("SELECT  ACT.TDPNTID, ACT.TDID AS ActivityID, ltrim(RTRIM(ACT.TDSTTLID)) AS  SettlementID, ACT.TDROUT AS  RouteID, ACT.TDTYP AS  ActivityType, ACT.TDCLASS AS  ActivityDetailClass, ");
            //objQueryBuilder.Append("ACT.TDDTLS AS  ActivityDetails, ACT.TDSTRTTM AS  ActivityStart, ACT.TDENDTM AS  ActivityEnd, ACT.TDSTID AS  StopInstanceID, ACT.TDAN8 AS  CustomerID, ");
            //objQueryBuilder.Append("ACT.TDPNTID AS  ActivityHeaderID, (CASE RS.Status WHEN " + strVerifyCodeId + " THEN (CASE WHEN ACT.TDSTAT IN ('VoidAtCashCollection', 'VoidAtDeliverCustomer', 'VoidAtOrderEntry','PaymentVoid', 'VoidCreditMemo','VoidExpense','VoidMoneyOrder') THEN 'VOIDED / SETTLED' ELSE 'SettlementVerified' END) ");
            //objQueryBuilder.Append("ELSE ACT.TDSTAT END) AS  ActivityStatus FROM busdta.M50012 ACT LEFT OUTER JOIN BUSDTA.Route_Settlement RS ON ACT.TDSTTLID=RS.SettlementId ");
            //objQueryBuilder.Append("WHERE ACT.TDTYP in ('Payment','ReturnOrder','HeldReturn') " + (showAllActivities ? "" : "AND ISNULL(RS.Status,0)<>" + strVerifyCodeId) + " and  SettlementID = '" + settlementID + "' ORDER BY ActivityStart asc");
            string strVerifyCodeId = DbEngine.ExecuteScalar(" CALL BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD('VERF')");
            objQueryBuilder.Append("CALL BUSDTA.SP_GetTransBeginDateForSettlement (@showAllActivities=" + (showAllActivities ? "1" : "0").ToString() + ",@settlementID='" + settlementID + "',@CodeId='" + strVerifyCodeId + "')");

            List<RouteSettlementModel> activities = new List<RouteSettlementModel>();
            try
            {
                DataSet result = Helpers.DbEngine.ExecuteDataSet(objQueryBuilder.ToString());
                if (result.HasData())
                {
                    activities = result.GetEntityList<RouteSettlementModel>();
                    RouteSettlementModel rsm = activities.FirstOrDefault();
                    return rsm.ActivityStart;
                }

                result.Dispose();
                result = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteSettlementManager][GetTransactionBeginningDateForSettlement][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            finally
            {
                objQueryBuilder = null;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][End:GetTransactionBeginningDateForSettlement]");
            return null;
        }

        public static string GetCoffeePoundsSoldForSettlement(string settlementID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][Start:GetCoffeePoundsSoldForSettlement]");
            //StringBuilder objQueryBuilder = new StringBuilder();
            //objQueryBuilder.Append("SELECT od.OrderQty,od.OrderUM,od.Itemid FROM BUSDTA.Order_Header oh INNER JOIN  BUSDTA.Order_Detail od on oh.orderID=od.orderID INNER JOIN busdta.F4102 f on od.ItemId=f.ibitm where f.IBSRP1 ='COF' and oh.SettlementID =" + settlementID);
            //try
            //{
            //    DataSet result = Helpers.DbEngine.ExecuteDataSet(objQueryBuilder.ToString());
            //    DataTable resultDT = new DataTable();1
            //    DataColumn dc = new DataColumn();
            //    dc.ColumnName = "OrderQty";
            //    dc.DataType = typeof(decimal);
            //    resultDT.Columns.Add(dc);
            //    PricingManager pm = new PricingManager();
            //    if (result.HasData())
            //    {
            //        foreach (DataRow dr in result.Tables[0].Rows)
            //        {
            //            //UOM conversion            
            //            decimal LbValue = pm.jdeUOMConversion(Convert.ToString(dr["OrderUM"]), "LB", Convert.ToInt32(dr["Itemid"]));
            //            DataRow newDataRow = resultDT.NewRow();
            //            newDataRow["OrderQty"] = LbValue;
            //            resultDT.Rows.Add(newDataRow);
            //        }
            //        return Convert.ToString(resultDT.Compute("Sum(OrderQty)", "")) + " lbs";
            //    }
            //}

            StringBuilder objQueryBuilder1 = new StringBuilder();
            StringBuilder objQueryBuilder2 = new StringBuilder();
            //            objQueryBuilder1.Append("SELECT oh.OrderID,od.OrderQty,od.OrderUM,od.Itemid FROM BUSDTA.Order_Header oh	INNER JOIN  BUSDTA.Order_Detail od on oh.orderID=od.orderID"+
            //" INNER JOIN busdta.F4102 f ON od.ItemId=f.ibitm where f.IBSRP1 ='COF' AND oh.OrderTypeId=(SELECT StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='SALORD')" +
            //" and oh.SettlementID =" + settlementID + "");

            //            objQueryBuilder2.Append("SELECT oh.OrderID,od.OrderQty,od.OrderUM,od.Itemid FROM BUSDTA.Order_Header oh	INNER JOIN  BUSDTA.Order_Detail od on oh.orderID=od.orderID" +
            //" INNER JOIN busdta.F4102 f ON od.ItemId=f.ibitm where f.IBSRP1 ='COF' AND oh.OrderTypeId=(SELECT StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='RETORD')" +
            //" and oh.SettlementID =" + settlementID + "");
            objQueryBuilder1.Append("CALL BUSDTA.SP_GetCoffeePoundsSoldForSettlement(@settlementID='" + settlementID + "',@StatusType='SALORD')");
            objQueryBuilder2.Append("CALL BUSDTA.SP_GetCoffeePoundsSoldForSettlement(@settlementID='" + settlementID + "',@StatusType='RETORD')");

            try
            {
                DataSet result1 = Helpers.DbEngine.ExecuteDataSet(objQueryBuilder1.ToString());
                DataSet result2 = Helpers.DbEngine.ExecuteDataSet(objQueryBuilder2.ToString());
                DataTable resultDT1 = new DataTable();
                DataTable resultDT2 = new DataTable();
                DataColumn dc1 = new DataColumn();
                dc1.ColumnName = "OrderQty";
                dc1.DataType = typeof(decimal);
                resultDT1.Columns.Add(dc1);
                DataColumn dc2 = new DataColumn();
                dc2.ColumnName = "OrderQty";
                dc2.DataType = typeof(decimal);
                resultDT2.Columns.Add(dc2);
                PricingManager pm = new PricingManager();
                decimal salesorderqty = 0, returnorderqty = 0;
                if (result1.HasData())
                {
                    foreach (DataRow dr in result1.Tables[0].Rows)
                    {
                        //UOM conversion            
                        decimal LbValue = pm.jdeUOMConversion(Convert.ToString(dr["OrderUM"]), "LB", Convert.ToInt32(dr["Itemid"]));
                        LbValue = LbValue * Convert.ToInt32(dr["OrderQty"].ToString());
                        DataRow newDataRow = resultDT1.NewRow();
                        newDataRow["OrderQty"] = LbValue;
                        resultDT1.Rows.Add(newDataRow);
                    }
                    salesorderqty = Convert.ToDecimal(resultDT1.Compute("Sum(OrderQty)", ""));
                }

                if (result2.HasData())
                {
                    foreach (DataRow dr in result2.Tables[0].Rows)
                    {
                        //UOM conversion            
                        decimal LbValue = pm.jdeUOMConversion(Convert.ToString(dr["OrderUM"]), "LB", Convert.ToInt32(dr["Itemid"]));
                        LbValue = LbValue * Convert.ToInt32(dr["OrderQty"].ToString());
                        DataRow newDataRow = resultDT2.NewRow();
                        newDataRow["OrderQty"] = LbValue;
                        resultDT2.Rows.Add(newDataRow);
                    }
                    returnorderqty = Convert.ToDecimal(resultDT2.Compute("Sum(OrderQty)", ""));

                }

                result1.Dispose();
                result2.Dispose();
                resultDT1.Dispose();
                resultDT2.Dispose();
                dc1.Dispose();
                dc2.Dispose();
                pm.Dispose();

                result1 = null;
                result2 = null;
                resultDT1 = null;
                resultDT2 = null;
                dc1 = null;
                dc2 = null;
                pm = null;

                return Convert.ToString(salesorderqty - returnorderqty) + " lbs";
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteSettlementManager][GetCoffeePoundsSoldForSettlement][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            finally
            {
                objQueryBuilder1 = null;
                objQueryBuilder2 = null;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][End:GetCoffeePoundsSoldForSettlement]");
            return "";
        }

        public static DataSet GetRouteSettlement(string settlementID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][Start:GetRouteSettlement]");
            StringBuilder objQueryBuilder = new StringBuilder();
            //objQueryBuilder.Append("SELECT * FROM BUSDTA.ROUTE_SETTLEMENT_detail where SettlementId = " + settlementID);
            objQueryBuilder.Append("CALL BUSDTA.SP_GetRouteSettlement(@settlementID=" + settlementID + ")");
            try
            {
                DataSet result = Helpers.DbEngine.ExecuteDataSet(objQueryBuilder.ToString());
                if (result.HasData())
                {
                    return result;
                }

                result.Dispose();
                result = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteSettlementManager][GetRouteSettlement][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            finally
            {
                objQueryBuilder = null;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][End:GetRouteSettlement]");
            return null;
        }

        public static DataSet GetOrderHeaderForSettlement(string settlementID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][Start:GetOrderHeaderForSettlement]");
            StringBuilder objQueryBuilder = new StringBuilder();
            //objQueryBuilder.Append("select * from busdta.Order_Header where SettlementID = " + settlementID);
            objQueryBuilder.Append("CALL BUSDTA.SP_GetOrderHeaderForSettlement(@settlementID='" + settlementID + "')");
            try
            {
                DataSet result = Helpers.DbEngine.ExecuteDataSet(objQueryBuilder.ToString());
                if (result.HasData())
                {
                    return result;
                }

                result.Dispose();
                result = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteSettlementManager][GetOrderHeaderForSettlement][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            finally
            {
                objQueryBuilder = null;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][End:GetOrderHeaderForSettlement]");
            return null;
        }

        public static string GetAlliedPoundsSoldForSettlement(string settlementID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][Start:GetAlliedPoundsSoldForSettlement]");
            StringBuilder objQueryBuilder = new StringBuilder();
            PricingManager pm = new PricingManager();
            //objQueryBuilder.Append("SELECT od.OrderQty,od.OrderUM,od.Itemid FROM BUSDTA.Order_Header oh INNER JOIN  BUSDTA.Order_Detail od on oh.orderID=od.orderID INNER JOIN busdta.F4102 f on od.ItemId=f.ibitm where f.IBSRP1 ='ALL' and oh.SettlementID =" + settlementID);

            try
            {
                objQueryBuilder.Append("CALL BUSDTA.SP_GetAlliedPoundsSoldForSettlement(" + settlementID + ")");
                DataSet result = Helpers.DbEngine.ExecuteDataSet(objQueryBuilder.ToString());
                DataTable resultDT = new DataTable();
                DataColumn dc = new DataColumn();
                dc.ColumnName = "OrderQty";
                dc.DataType = typeof(decimal);
                resultDT.Columns.Add(dc);

                if (result.HasData())
                {
                    foreach (DataRow dr in result.Tables[0].Rows)
                    {
                        //UOM conversion            
                        decimal LbValue = pm.jdeUOMConversion(Convert.ToString(dr["OrderUM"]), "LB", Convert.ToInt32(dr["Itemid"]));
                        DataRow newDataRow = resultDT.NewRow();
                        newDataRow["OrderQty"] = LbValue;
                        resultDT.Rows.Add(newDataRow);
                    }
                }

                result.Dispose();
                result = null;
                //Remove Allied LBS
                //return Convert.ToString(resultDT.Compute("Sum(OrderQty)", "")) + " lbs";
                return Convert.ToString(resultDT.Compute("Sum(OrderQty)", ""));
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteSettlementManager][GetAlliedPoundsSoldForSettlement][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            finally
            {
                objQueryBuilder = null;
                pm.Dispose();
                pm = null;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][End:GetAlliedPoundsSoldForSettlement]");
            return "";
        }

        public static string GetNetCashSalesForSettlement(string settlementID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][Start:GetNetCashSalesForSettlement]");
            StringBuilder objQueryBuilder = new StringBuilder();
            //objQueryBuilder.Append("SELECT OrderTotalamt,aitrar FROM BUSDTA.Order_Header join busdta.F03012 on CustShipToId = aian8 where SettlementID = " + settlementID);

            try
            {
                objQueryBuilder.Append("CALL BUSDTA.SP_GetNetCashSalesForSettlement(@settlementID='" + settlementID + "')");
                DataSet result = Helpers.DbEngine.ExecuteDataSet(objQueryBuilder.ToString());
                if (result.HasData())
                {
                    return Convert.ToString(result.Tables[0].Compute("Sum(OrderTotalamt)", ""));
                }

                result.Dispose();
                result = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteSettlementManager][GetNetCashSalesForSettlement][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            finally
            {
                objQueryBuilder = null;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][End:GetNetCashSalesForSettlement]");
            return "";
        }

        public static DataSet GetOrderHeaderWithStatusType(string settlementID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][Start:GeOrderHeaderDetailsWithStatusType]");
            StringBuilder objQueryBuilder = new StringBuilder();  // st.StatusTypeDESC
            //objQueryBuilder.Append("select oh.OrderID,oh.CustBillToId,oh.CustShipToId, oh.OrderDate," +
            //"case when oh.OrderTypeid = 37 then 'SO' when oh.OrderTypeid = 38 then 'CO' when oh.OrderTypeid = 32 then 'C5' ELSE '' END  AS 'StatusTypeDESC' " +
            //",oh.OrderTotalAmt,oh.SalesTaxAmt,oh.InvoiceTotalAmt,oh.EnergySurchargeAmt,BUSDTA.GetDefaultPhone(B.ABAN8) as Phone," +
            //                       "B.abalph,C.ALaDD1,C.ALaDD2,C.ALADD3,C.ALADD4,C.ALCTY1,C.aladds,m.RPSN, " +
            //                       "BUSDTA.GetDefaultPhone(B2.ABAN8) as Phone2,B2.abalph abalph2,C2.ALaDD1 ALaDD12,C2.ALaDD2 ALaDD22,C2.ALADD3 ALADD32,C2.ALADD4 ALADD42,C2.ALCTY1 ALCTY12,C2.aladds aladds2 " +
            //                       "from BUSDTA.Order_header oh left Join BUSDTA.Status_Type st on oh.OrderTypeid = st.StatusTypeID " +
            //                       "JOIN BUSDTA.F0101 B ON oh.CustShipToId = B.ABAN8  JOIN BUSDTA.F0116 C ON oh.CustShipToId = C.AlAN8 " +
            //                         "JOIN BUSDTA.F0101 B2 ON oh.CustBillToId = B2.ABAN8  JOIN BUSDTA.F0116 C2 ON oh.CustBillToId = C2.AlAN8 " +
            //                       " left join  BUSDTA.M56M0004 m on oh.CustShipToId = m.RPAN8 and  m.RPSTDT = oh.OrderDate where oh.SettlementID = " + settlementID + " order by m.RPSN ");
            objQueryBuilder.Append("CALL BUSDTA.SP_GetOrderHeaderWithStatusType(@settlementID='" + settlementID + "')");
            try
            {
                DataSet result = Helpers.DbEngine.ExecuteDataSet(objQueryBuilder.ToString());
                if (result.HasData())
                {
                    return result;
                }

                result.Dispose();
                result = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteSettlementManager][GeOrderHeaderDetailsWithStatusType][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            finally
            {
                objQueryBuilder = null;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][End:GeOrderHeaderDetailsWithStatusType]");
            return null;
        }

        //public static DataSet GetOrderHeaderWithStatusType(string settlementID)
        //{
        //    Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][Start:GeOrderHeaderDetailsWithStatusType]");
        //    StringBuilder objQueryBuilder = new StringBuilder();
        //    objQueryBuilder.Append("select oh.OrderID,oh.CustBillToId,oh.CustShipToId, oh.OrderDate,st.StatusTypeDESC,oh.OrderTotalAmt,oh.SalesTaxAmt,oh.InvoiceTotalAmt,oh.EnergySurchargeAmt,BUSDTA.GetDefaultPhone(B.ABAN8) as Phone," +
        //                           "B.abalph,C.ALaDD1,C.ALaDD2,C.ALADD3,C.ALADD4,C.ALCTY1,C.aladds,m.RPSN, " +
        //                           "BUSDTA.GetDefaultPhone(B2.ABAN8) as Phone2,B2.abalph abalph2,C2.ALaDD1 ALaDD12,C2.ALaDD2 ALaDD22,C2.ALADD3 ALADD32,C2.ALADD4 ALADD42,C2.ALCTY1 ALCTY12,C2.aladds aladds2 " +
        //                           "from BUSDTA.Order_header oh left Join BUSDTA.Status_Type st on oh.OrderTypeid = st.StatusTypeID " +
        //                           "JOIN BUSDTA.F0101 B ON oh.CustShipToId = B.ABAN8  JOIN BUSDTA.F0116 C ON oh.CustShipToId = C.AlAN8 " +
        //                             "JOIN BUSDTA.F0101 B2 ON oh.CustBillToId = B2.ABAN8  JOIN BUSDTA.F0116 C2 ON oh.CustBillToId = C2.AlAN8 " +
        //                           " left join  BUSDTA.M56M0004 m on oh.CustShipToId = m.RPAN8 and  m.RPSTDT = oh.OrderDate where oh.SettlementID = " + settlementID + " order by m.RPSN ");
        //    try
        //    {
        //        DataSet result = Helpers.DbEngine.ExecuteDataSet(objQueryBuilder.ToString());
        //        if (result.HasData())
        //        {
        //            return result;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][RouteSettlementManager][GeOrderHeaderDetailsWithStatusType][ExceptionStackTrace = " + ex.StackTrace + "]");

        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][End:GeOrderHeaderDetailsWithStatusType]");
        //    return null;
        //}



        public static string GetSampleCoffeePoundsSoldForSettlement(string settlementID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][Start:GetSampleCoffeePoundsSoldForSettlement]");
            StringBuilder objCustQueryBuilder = new StringBuilder();
            //objCustQueryBuilder.Append("SELECT cqd.QuoteQty, cqd.ItemID, cqd.TransactionUM from BUSDTa.Customer_Quote_Detail cqd inner join busdta.F4102 f4  on cqd.ItemId=f4.ibitm ");
            //objCustQueryBuilder.Append("inner join  BUSDTa.Customer_Quote_Header cqh on cqd.CustomerQuoteId = cqh.CustomerQuoteId and  f4.IBSRP1 ='COF' and cqh.SettlementId = " + settlementID);

            StringBuilder objProsQueryBuilder = new StringBuilder();
            //objProsQueryBuilder.Append("SELECT cqd.QuoteQty, cqd.ItemID, cqd.TransactionUM from BUSDTa.Prospect_Quote_Detail cqd inner join busdta.F4102 f4  on cqd.ItemId=f4.ibitm ");
            //objProsQueryBuilder.Append("inner join  BUSDTA.Prospect_Quote_Header cqh on cqd.ProspectQuoteId = cqh.ProspectQuoteId  and f4.IBSRP1 ='COF' and cqh.SettlementId = " + settlementID);

            Decimal custLbs = new Decimal();
            Decimal prosLbs = new Decimal();

            PricingManager pm = new PricingManager();

            try
            {
                objCustQueryBuilder.Append("CALL BUSDTA.SP_GetSampleCoffeeSoldForSettlement(@settlementID='" + settlementID + "',@CustOrProspect=1,@AlliedorCoffee='COF')");
                objProsQueryBuilder.Append("CALL BUSDTA.SP_GetSampleCoffeeSoldForSettlement(@settlementID='" + settlementID + "',@CustOrProspect=2,@AlliedorCoffee='COF')");

                DataSet result = Helpers.DbEngine.ExecuteDataSet(objCustQueryBuilder.ToString());
                DataTable resultDT = new DataTable();
                DataColumn dc = new DataColumn();
                dc.ColumnName = "QuoteQty";
                dc.DataType = typeof(decimal);
                resultDT.Columns.Add(dc);

                if (result.HasData())
                {
                    foreach (DataRow dr in result.Tables[0].Rows)
                    {
                        //UOM conversion            
                        decimal LbValue = pm.jdeUOMConversion(Convert.ToString(dr["TransactionUM"]), "LB", Convert.ToInt32(dr["ItemID"]));
                        DataRow newDataRow = resultDT.NewRow();
                        newDataRow["QuoteQty"] = LbValue;
                        resultDT.Rows.Add(newDataRow);
                    }
                    custLbs = Convert.ToDecimal(resultDT.Compute("Sum(QuoteQty)", ""));
                }

                DataSet prosResult = Helpers.DbEngine.ExecuteDataSet(objProsQueryBuilder.ToString());
                DataTable prosResultDT = new DataTable();
                DataColumn prosdc = new DataColumn();
                prosdc.ColumnName = "QuoteQty";
                prosdc.DataType = typeof(decimal);
                prosResultDT.Columns.Add(prosdc);
                if (prosResult.HasData())
                {
                    foreach (DataRow dr in prosResult.Tables[0].Rows)
                    {
                        //UOM conversion            
                        decimal LbValue = pm.jdeUOMConversion(Convert.ToString(dr["TransactionUM"]), "LB", Convert.ToInt32(dr["ItemID"]));
                        DataRow prosnewDataRow = prosResultDT.NewRow();
                        prosnewDataRow["QuoteQty"] = LbValue;
                        prosResultDT.Rows.Add(prosnewDataRow);
                    }
                    prosLbs = Convert.ToDecimal(prosResultDT.Compute("Sum(QuoteQty)", ""));
                }

                result.Dispose();
                resultDT.Dispose();
                dc.Dispose();
                prosResult.Dispose();
                prosResultDT.Dispose();
                prosdc.Dispose();

                result = null;
                resultDT = null;
                dc = null;
                prosResult = null;
                prosResultDT = null;
                prosdc = null;

                return Convert.ToString(custLbs + prosLbs) + " lbs";
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteSettlementManager][GetSampleCoffeePoundsSoldForSettlement][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            finally
            {
                objCustQueryBuilder = null;
                objProsQueryBuilder = null;
                pm.Dispose();
                pm = null;
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][End:GetSampleCoffeePoundsSoldForSettlement]");
            return "";
        }

        public static string GetSampleAlliedPoundsSoldForSettlement(string settlementID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][Start:GetSampleCoffeePoundsSoldForSettlement]");
            StringBuilder objCustQueryBuilder = new StringBuilder();
            //objCustQueryBuilder.Append("SELECT cqd.QuoteQty, cqd.ItemID, cqd.TransactionUM from BUSDTa.Customer_Quote_Detail cqd inner join busdta.F4102 f4  on cqd.ItemId=f4.ibitm ");
            //objCustQueryBuilder.Append("inner join  BUSDTa.Customer_Quote_Header cqh on cqd.CustomerQuoteId = cqh.CustomerQuoteId and  f4.IBSRP1 ='ALL' and cqh.SettlementId = " + settlementID);

            StringBuilder objProsQueryBuilder = new StringBuilder();
            //objProsQueryBuilder.Append("SELECT cqd.QuoteQty, cqd.ItemID, cqd.TransactionUM from BUSDTa.Prospect_Quote_Detail cqd inner join busdta.F4102 f4  on cqd.ItemId=f4.ibitm ");
            //objProsQueryBuilder.Append("inner join  BUSDTA.Prospect_Quote_Header cqh on cqd.ProspectQuoteId = cqh.ProspectQuoteId  and f4.IBSRP1 ='ALL' and cqh.SettlementId = " + settlementID);

            Decimal custLbs = new Decimal();
            Decimal prosLbs = new Decimal();

            PricingManager pm = new PricingManager();

            try
            {
                objCustQueryBuilder.Append("CALL BUSDTA.SP_GetSampleCoffeeSoldForSettlement(@settlementID='" + settlementID + "',@CustOrProspect=1,@AlliedorCoffee ='ALL')");
                objProsQueryBuilder.Append("CALL BUSDTA.SP_GetSampleCoffeeSoldForSettlement(@settlementID='" + settlementID + "',@CustOrProspect=2,@AlliedorCoffee ='ALL')");

                DataSet result = Helpers.DbEngine.ExecuteDataSet(objCustQueryBuilder.ToString());
                DataTable resultDT = new DataTable();
                DataColumn dc = new DataColumn();
                dc.ColumnName = "QuoteQty";
                dc.DataType = typeof(decimal);
                resultDT.Columns.Add(dc);

                if (result.HasData())
                {
                    foreach (DataRow dr in result.Tables[0].Rows)
                    {
                        //UOM conversion            
                        decimal LbValue = pm.jdeUOMConversion(Convert.ToString(dr["TransactionUM"]), "LB", Convert.ToInt32(dr["ItemID"]));
                        DataRow newDataRow = resultDT.NewRow();
                        newDataRow["QuoteQty"] = LbValue;
                        resultDT.Rows.Add(newDataRow);
                    }
                    custLbs = Convert.ToDecimal(resultDT.Compute("Sum(QuoteQty)", ""));
                }

                DataSet prosResult = Helpers.DbEngine.ExecuteDataSet(objProsQueryBuilder.ToString());
                DataTable prosResultDT = new DataTable();
                DataColumn prosdc = new DataColumn();
                prosdc.ColumnName = "QuoteQty";
                prosdc.DataType = typeof(decimal);
                prosResultDT.Columns.Add(prosdc);

                if (prosResult.HasData())
                {
                    foreach (DataRow dr in prosResult.Tables[0].Rows)
                    {
                        //UOM conversion            
                        decimal LbValue = pm.jdeUOMConversion(Convert.ToString(dr["TransactionUM"]), "LB", Convert.ToInt32(dr["ItemID"]));
                        DataRow prosnewDataRow = prosResultDT.NewRow();
                        prosnewDataRow["QuoteQty"] = LbValue;
                        prosResultDT.Rows.Add(prosnewDataRow);
                    }
                    prosLbs = Convert.ToDecimal(prosResultDT.Compute("Sum(QuoteQty)", ""));
                }

                result.Dispose();
                resultDT.Dispose();
                dc.Dispose();
                prosResult.Dispose();
                prosResultDT.Dispose();
                prosdc.Dispose();

                result = null;
                resultDT = null;
                dc = null;
                prosResult = null;
                prosResultDT = null;
                prosdc = null;
                //Remove Allied LBS
                //return Convert.ToString(custLbs + prosLbs) + " lbs";
                return Convert.ToString(custLbs + prosLbs);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteSettlementManager][GetSampleCoffeePoundsSoldForSettlement][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            finally
            {
                objCustQueryBuilder = null;
                objProsQueryBuilder = null;
                pm.Dispose();
                pm = null;
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][End:GetSampleCoffeePoundsSoldForSettlement]");
            return "";
        }

    }
}
