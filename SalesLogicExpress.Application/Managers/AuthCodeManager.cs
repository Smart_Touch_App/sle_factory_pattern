﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Managers
{
    public class AuthCodeManager:IDisposable
    {
        public class Log
        {
            public int LogID { get; set; }
            public string ApprovalCode { get; set; }
            public string RequestCode { get; set; }
            public string RouteID { get; set; }
            public string Feature { get; set; }
            public string Amount { get; set; }
            public string RequestedByUser { get; set; }
            public string RequestedForCustomer { get; set; }
            public string Payload { get; set; }
        }
        private readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public enum Feature
        {
            ChargeOnAccount,
            VoidReceipt,
            OutOfBalanceApproval,
            CreditHold
        }
        enum Key
        {
            F,
            U,
            A
        }
        Feature feature { get; set; }
        decimal amount { get; set; }
        string userID { get; set; }
        string featureCodeID { get; set; }
        string amountCodeID { get; set; }
        string formatCodeID { get; set; }
        public bool VerifyAuthCode(string RequestCode, string AuthCode)
        {
            bool returnValue = false;
            Logger.Info("[SalesLogicExpress.Application.Managers][AuthCodeManager][Start:VerifyAuthCode]");
            try
            {
                returnValue = AuthCode.Trim().Equals(GetAuthCode(RequestCode).Trim());
            }
            catch (Exception ex)
            {
                returnValue = false;
                Logger.Error("[SalesLogicExpress.Application.Managers][AuthCodeManager][VerifyAuthCode][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][AuthCodeManager][End:VerifyAuthCode]");
            return returnValue;
        }
        public void LogAuthCode(Log CodeLog)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][AuthCodeManager][Start:LogAuthCode]");
            try
            {
                string query = @"insert into BUSDTA.ApprovalCodeLog 
                    (ApprovalCode,RequestCode,RouteID,Feature,Amount,RequestedByUser,RequestedForCustomer,Payload,CreatedBy,CreatedDateTime) 
                    values
                    ('{0}','{1}','{2}','{3}','{4}','{5}','{6}',?,'{7}',{8})";
                // string featureDescription = DbEngine.ExecuteScalar(string.Format("select description from BUSDTA.feature_code_mapping where feature='{0}'",CodeLog.Feature)).ToString();
                string featureDescription = DbEngine.ExecuteScalar(string.Format("CALL BUSDTA.SP_GetLogAuthCode('{0}')", CodeLog.Feature).ToString());
                query = string.Format(query, CodeLog.ApprovalCode, CodeLog.RequestCode, CodeLog.RouteID, featureDescription.Trim(), CodeLog.Amount, CodeLog.RequestedByUser, CodeLog.RequestedForCustomer, CodeLog.RequestedByUser, DateTime.Now.GetFormattedDateTimeForDb());
                // used SACommand for inserting blob, inserting blobs needs to done using parameters
                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(query);
                Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.LongNVarchar;
                command.Parameters.Add(parm);
                command.Parameters[0].Value = '0';// CodeLog.Payload;
                new DB_CRUD().InsertUpdateData(command);

                ResourceManager.QueueManager.QueueProcess("Logging", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][AuthCodeManager][LogAuthCode][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][AuthCodeManager][End:LogAuthCode]");
        }
        public string GetAuthCode(string RequestCode)
        {
            string returnValue = string.Empty;
            Logger.Info("[SalesLogicExpress.Application.Managers][AuthCodeManager][Start:GetAuthCode]");
            try
            {
                string resultValue = string.Empty, authCodeFormat = string.Empty, authCodeFormatID = string.Empty, formatCodeID = string.Empty;
                string amountCodeID = string.Empty, featureCodeID = string.Empty, userID = string.Empty;
                RequestAuthorizationFormat format = null;
                // Get the details of the request code format
                formatCodeID = RequestCode.Substring(0, 1);
                // string query = "SELECT * FROM BUSDTA.Request_Authorization_Format where RequestFormatCode = {0}";
                string query = ("CALL BUSDTA.SP_GetAuthCode ({0})");
                query = string.Format(query, formatCodeID);
                DataSet dsData = new DataSet();
                dsData = DbEngine.ExecuteDataSet(query);
                if (dsData.HasData())
                {
                    #region Get the Codes for the feature/amount/userid from the given RequestCode
                    format = dsData.GetEntityList<RequestAuthorizationFormat>()[0];
                    switch (format.Key1.Trim().ParseEnum<Key>())
                    {
                        case Key.F:
                            featureCodeID = RequestCode.Substring(1, 2);
                            break;
                        case Key.A:
                            amountCodeID = RequestCode.Substring(1, 2);
                            break;
                        case Key.U:
                            userID = RequestCode.Substring(1, 4);
                            break;
                    }
                    switch (format.Key2.Trim().ParseEnum<Key>())
                    {
                        case Key.F:
                            featureCodeID = string.IsNullOrEmpty(userID) ? RequestCode.Substring(3, 2) : RequestCode.Substring(5, 2);
                            break;
                        case Key.A:
                            amountCodeID = string.IsNullOrEmpty(userID) ? RequestCode.Substring(4, 2) : RequestCode.Substring(5, 2);
                            break;
                        case Key.U:
                            userID = RequestCode.Substring(3, 4);
                            break;
                    }
                    switch (format.Key3.Trim().ParseEnum<Key>())
                    {
                        case Key.F:
                            featureCodeID = string.IsNullOrEmpty(userID) ? RequestCode.Substring(6, 2) : RequestCode.Substring(7, 2);
                            break;
                        case Key.A:
                            amountCodeID = string.IsNullOrEmpty(userID) ? RequestCode.Substring(6, 2) : RequestCode.Substring(7, 2);
                            break;
                        case Key.U:
                            userID = RequestCode.Substring(5, 4);
                            break;
                    }
                    #endregion

                    #region Build the new AuthCode based on the AuthorizationFormatCode of the RequestCode
                    //query = "SELECT AuthorizationFormatCode FROM BUSDTA.Feature_Code_Mapping where RequestCode={0}";
                    query = ("Call BUSDTA.SP_GetAuthoFormatCode({0})");
                    query = string.Format(query, featureCodeID);
                    featureCodeID = Convert.ToString(DbEngine.ExecuteScalar(query)).PadLeft(2, '0');

                    //query = "SELECT AuthorizationFormatCode FROM BUSDTA.Amount_Range_Index where RequestCode={0}";
                    query = ("Call BUSDTA.SP_GetAuthFormatRangeCode({0})");
                    query = string.Format(query, amountCodeID);
                    amountCodeID = Convert.ToString(DbEngine.ExecuteScalar(query)).PadLeft(2, '0');
                    // Get the details of the request code format pointed by the value in AuthorizationFormatCode
                    //query = "SELECT * FROM BUSDTA.Request_Authorization_Format where RequestFormatCode = {0}";
                    query = ("Call BUSDTA.SP_GetReqAuthFormat({0})");
                    query = string.Format(query, format.AuthorizationFormatCode);
                    dsData = DbEngine.ExecuteDataSet(query);
                    if (dsData.HasData())
                    {
                        format = dsData.GetEntityList<RequestAuthorizationFormat>()[0];
                        returnValue = returnValue + format.AuthorizationFormatCode;
                        switch (format.Key1.Trim().ParseEnum<Key>())
                        {
                            case Key.F:
                                returnValue = returnValue + featureCodeID;
                                break;
                            case Key.A:
                                returnValue = returnValue + amountCodeID;
                                break;
                            case Key.U:
                                returnValue = returnValue + userID.PadLeft(3, '0');
                                break;
                        }
                        switch (format.Key2.Trim().ParseEnum<Key>())
                        {
                            case Key.F:
                                returnValue = returnValue + featureCodeID;
                                break;
                            case Key.A:
                                returnValue = returnValue + amountCodeID;
                                break;
                            case Key.U:
                                returnValue = returnValue + userID.PadLeft(3, '0');
                                break;
                        }
                        switch (format.Key3.Trim().ParseEnum<Key>())
                        {
                            case Key.F:
                                returnValue = returnValue + featureCodeID;
                                break;
                            case Key.A:
                                returnValue = returnValue + amountCodeID;
                                break;
                            case Key.U:
                                returnValue = returnValue + userID.PadLeft(3, '0');
                                break;
                        }

                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                returnValue = string.Empty;
                Logger.Error("[SalesLogicExpress.Application.Managers][AuthCodeManager][GetAuthCode][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][AuthCodeManager][End:GetAuthCode]");
            return returnValue;
        }
        public string GetRequestCode(Feature Feature, decimal Amount, string UserID)
        {
            string returnValue = string.Empty;
            Logger.Info("[SalesLogicExpress.Application.Managers][AuthCodeManager][Start:GetRequestCode]");
            try
            {
                this.feature = Feature;
                this.amount = Amount;
                this.userID = UserID;
                RequestAuthorizationFormat format = null;
                formatCodeID = Helpers.Utils.RandomGenHelper.Next(1, 5).ToString();
                //  string query = "SELECT * FROM BUSDTA.Request_Authorization_Format where RequestFormatCode = {0}";
                string query = ("CALL BUSDTA.SP_GetAuthCode({0})");
                query = string.Format(query, formatCodeID);
                DataSet dsData = new DataSet();
                dsData = DbEngine.ExecuteDataSet(query);
                if (dsData.HasData())
                {
                    format = dsData.GetEntityList<RequestAuthorizationFormat>()[0];
                }
                // query = "SELECT RequestCode FROM BUSDTA.Feature_Code_Mapping where Feature='{0}'";
                query = ("CALL BUSDTA.SP_GetRequestCodeFeature('{0}')");
                query = string.Format(query, Feature.ToString());
                featureCodeID = Convert.ToString(DbEngine.ExecuteScalar(query)).PadLeft(2, '0');

                //  query = "SELECT * FROM BUSDTA.Amount_Range_Index where rangestart <= {0} and rangeEnd >={0}";
                query = (" CALL  BUSDTA.SP_GetRequestCodeAmount({0})");
                query = string.Format(query, Amount);
                amountCodeID = Convert.ToString(DbEngine.ExecuteScalar(query)).PadLeft(2, '0');

                returnValue = returnValue + formatCodeID;
                switch (format.Key1.Trim().ParseEnum<Key>())
                {
                    case Key.F:
                        returnValue = returnValue + featureCodeID;
                        break;
                    case Key.A:
                        returnValue = returnValue + amountCodeID;
                        break;
                    case Key.U:
                        returnValue = returnValue + userID.PadLeft(3, '0');
                        break;
                }
                switch (format.Key2.Trim().ParseEnum<Key>())
                {
                    case Key.F:
                        returnValue = returnValue + featureCodeID;
                        break;
                    case Key.A:
                        returnValue = returnValue + amountCodeID;
                        break;
                    case Key.U:
                        returnValue = returnValue + userID.PadLeft(3, '0');
                        break;
                }
                switch (format.Key3.Trim().ParseEnum<Key>())
                {
                    case Key.F:
                        returnValue = returnValue + featureCodeID;
                        break;
                    case Key.A:
                        returnValue = returnValue + amountCodeID;
                        break;
                    case Key.U:
                        returnValue = returnValue + userID.PadLeft(3, '0');
                        break;
                }
            }
            catch (Exception ex)
            {
                returnValue = string.Empty;
                Logger.Error("[SalesLogicExpress.Application.Managers][AuthCodeManager][GetRequestCode][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][AuthCodeManager][End:GetRequestCode]");
            return returnValue;
        }
        //public RequestCodeDetails GetRequestDetails(string RequestCode)
        //{
        //    RequestCodeDetails details = null;
        //    Logger.Info("[SalesLogicExpress.Application.Managers][AuthCodeManager][Start:GetRequestDetails]");
        //    try
        //    {

        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][AuthCodeManager][GetRequestDetails][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][AuthCodeManager][End:GetRequestDetails]");
        //    return details;
        //}

        public class RequestCodeDetails
        {
            public string Feature { get; set; }
            public string Amount { get; set; }
            public string User { get; set; }
            public string Route { get; set; }
        }
        public class RequestAuthorizationFormat
        {
            public string RequestFormatCode { get; set; }
            public string AuthorizationFormatCode { get; set; }
            public string Key1 { get; set; }
            public string Key2 { get; set; }
            public string Key3 { get; set; }
        }

        // Code written by Marimuthu for Performance change
        // Dispose implementation for Performance optimization
        // Date : 11/03/2016
        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
