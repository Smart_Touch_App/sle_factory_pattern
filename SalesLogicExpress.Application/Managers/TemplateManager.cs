﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using SalesLogicExpress.Application.ViewModels;
using System.Globalization;
using System.Linq;

namespace SalesLogicExpress.Application.Managers
{
    public class TemplateManager: IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.TemplateManager");
        /// <summary>
        /// Get all items in the template defined for the customer
        /// </summary>
        /// <param name="customerID">Invoice Number</param>
        /// <returns>Observable collection of TemplateItem class object</returns>

        public ObservableCollection<TemplateItem> GetTemplateItemsForCustomer(string customerID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][Start:GetTemplateItemsForCustomer][customerID=" + customerID + "]");
            ObservableCollection<TemplateItem> result = new ObservableCollection<TemplateItem>();
            try
            {
                //string Query = "select OTORTP as Template_Name , OTan8 as Invoice#, OTOSEQ as Sequence#, OTLITM as Item#, IMDSC1 as ItemDescription, IMITM as ItemId " +
                //    ",0 as OrderQty, OTQTYU/10000 as UsualQty, 0 as PreOrderQty,OTUOM as UM,IMUOM4 as UM_Price,IMLNTY as StkType,IMSRP1  as SalesCat1, " +
                //    "IMSRP5 as SalesCat5, Dateformat(BUSDTA.DateG2J(OTEFTJ),'mm/dd/yyyy') as EffectiveDate, Dateformat(BUSDTA.DateG2J(OTEXDJ),'mm/dd/yyyy') as ExpiredDate ,imuom1 as PrimaryUOM " +
                //    "from busdta.F4015 ,busdta.F4101 where OTAn8  = " + customerID + " and OTORTP = 'S" + customerID + "' and ltrim(rtrim(otlitm)) = ltrim(rtrim(imlitm)) Order by OTOSEQ";

                //string Query = "select OTORTP as Template_Name , OTan8 as Invoice#, OTOSEQ as Sequence#, OTLITM as Item#, IMDSC1 as ItemDescription, IMITM as ItemId ,0 as OrderQty, OTQTYU/10000 as UsualQty, " +
                //               "0 as PreOrderQty,otUOM as UM,IMUOM4 as UM_Price,OTLNTY as StkType,IMSRP1  as SalesCat1, IMSRP5 as SalesCat5, Dateformat(BUSDTA.DateG2J(OTEFTJ),'mm/dd/yyyy') as EffectiveDate," +
                //               "Dateformat(BUSDTA.DateG2J(OTEXDJ),'mm/dd/yyyy') as ExpiredDate ,imuom1 as PrimaryUOM,  isnull(OnHandQuantity,0) as OnHandQty, isnull(CommittedQuantity,0) as CommittedQty, " +
                //               "isnull(HeldQuantity,0) as HeldQty, (isnull(OnHandQuantity,0)-isnull(CommittedQuantity,0)-isnull(HeldQuantity,0)) as AvailableQty" +
                //               " from busdta.F4015 join busdta.F4101 on otitm = imitm  join BUSDTA.Inventory inv on otitm = inv.itemId join busdta.ItemConfiguration ic on imitm = ic.itemId JOIN busdta.ItemUoMs iu on ic.itemid=iu.itemid " +
                //               "where OTAn8  =" + customerID + " and OTORTP = 'S" + customerID + "' and RouteEnabled = 1 and allowSearch=1 and iu.cansell=1 and iu.DisplaySeq=(select min(DisplaySeq) from busdta.itemUOMs where itemid=ic.itemid and cansell=1) Order by OTOSEQ";

                //Comment by TUI-CBE

                //string Query = "SELECT TemplateType as Template_Name , ShipToID as Invoice#, TemplateDetailID, ord.DisplaySeq as Sequence#, " +
                //           " ord.LongItem as Item#, IMDSC1 as ItemDescription, IMITM as ItemId ,0 as OrderQty, TemplateQuantity/10000 as UsualQty, 0 as PreOrderQty," +
                //           " TemplateUOM as UM,IMUOM4 as UM_Price,IMLNTY as StkType,IMSRP1  as SalesCat1, IMSRP5 as SalesCat5,imuom1 as PrimaryUOM,  isnull(OnHandQuantity,0) as OnHandQty," +
                //            " isnull(CommittedQuantity,0) as CommittedQty, isnull(HeldQuantity,0) as HeldQty, (isnull(OnHandQuantity,0)-isnull(CommittedQuantity,0)-isnull(HeldQuantity,0)) as AvailableQty " +
                //            " FROM Busdta.Order_Templates ord" +
                //            " inner join busdta.F4101 itm on ord.ItemID = itm.imitm " +
                //            " inner join BUSDTA.Inventory inv on ord.ItemID = inv.itemId " +
                //            " inner join busdta.ItemConfiguration ic on ord.itemid = ic.itemId " +
                //            " inner JOIN busdta.ItemUoMs iu on ord.itemid=iu.itemid and ord.TemplateUOM = iu.UOM " +
                //               " where ShipToID  =" + customerID + " and TemplateType = 'BUILDUP' and RouteEnabled = 1 and allowSearch=1 and iu.cansell=1 and iu.DisplaySeq=(select min(DisplaySeq) from busdta.itemUOMs where itemid=ic.itemid and cansell=1) Order by ord.DisplaySeq";
                //DataSet templateItems = Helpers.DbEngine.ExecuteDataSet(Query);

                //string Query = "SELECT TemplateType as Template_Name , ShipToID as Invoice#, TemplateDetailID, ord.DisplaySeq as Sequence#, " +
                //           " ord.LongItem as Item#, IMDSC1 as ItemDescription, IMITM as ItemId ,0 as OrderQty, TemplateQuantity as UsualQty, 0 as PreOrderQty," +
                //           " TemplateUOM as UM,IMUOM4 as UM_Price,IMLNTY as StkType,IMSRP1  as SalesCat1, IMSRP5 as SalesCat5,imuom1 as PrimaryUOM,  isnull(OnHandQuantity,0) as OnHandQty," +
                //            " isnull(CommittedQuantity,0) as CommittedQty, isnull(HeldQuantity,0) as HeldQty, (isnull(OnHandQuantity,0)-isnull(CommittedQuantity,0)-isnull(HeldQuantity,0)) as AvailableQty " +
                //            " FROM Busdta.Order_Templates ord" +
                //            " inner join busdta.F4101 itm on ord.ItemID = itm.imitm " +
                //            " inner join BUSDTA.Inventory inv on ord.ItemID = inv.itemId " +
                //            " inner join busdta.ItemConfiguration ic on ord.itemid = ic.itemId " +
                //            " inner JOIN busdta.ItemUoMs iu on ord.itemid=iu.itemid and ord.TemplateUOM = iu.UOM " +
                //               " where ShipToID  =" + customerID + " and TemplateType = 'BUILDUP' and RouteEnabled = 1 and allowSearch=1 and iu.cansell=1 and iu.DisplaySeq in (select DisplaySeq from busdta.itemUOMs where itemid=ic.itemid and cansell=1) Order by ord.DisplaySeq";
                string Query = "CALL BUSDTA.SP_GetTemplateItemsForCustomer(@customerID =" + customerID + ")";
                DataSet templateItems = Helpers.DbEngine.ExecuteDataSet(Query);

                //ItemManager itemManager = new ItemManager();
                //Random qtyOnHand = new Random();

                if (templateItems.HasData())
                {
                    Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][End:GetTemplateItemsForCustomer][customerID=" + customerID + "]");
                    //DataTable templateUms = DbEngine.ExecuteDataSet(@"select distinct(UMUM) UMUM,UMITM from BUSDTA.F41002 union select distinct(UMRUM) UMUM,UMITM from BUSDTA.F41002").Tables[0];
                    // Dictionary<string, List<string>> templateUms = ResourceManager.GetItemUMList;
                    Dictionary<string, List<string>> templateUms = UoMManager.GetItemUMList;
                    foreach (DataRow templateItem in templateItems.Tables[0].Rows)
                    {
                        TemplateItem order = new TemplateItem();
                        order.OrderQty = Convert.ToInt32(templateItem["OrderQty"].ToString().Trim());
                        //double UsualQty = (double)templateItem.ItemArray[7];
                        //if (UsualQty.ToString().Split('.').Length == 2)
                        //{
                        //    UsualQty = Convert.ToDouble(UsualQty.ToString().Split('.')[1].Substring(0, UsualQty.ToString().Split('.')[1].Length));
                        //}
                        //double PreOrderQty = (double)templateItem.ItemArray[8];
                        //if (PreOrderQty.ToString().Split('.').Length == 2)
                        //{
                        //    PreOrderQty = Convert.ToDouble(PreOrderQty.ToString().Split('.')[1].Substring(0, PreOrderQty.ToString().Split('.')[1].Length));
                        //}
                        order.UsualQty = Convert.ToInt32(Convert.ToDouble(templateItem["UsualQty"].ToString().Trim()));
                        order.PreOrderQty = Convert.ToInt32(Convert.ToDouble(templateItem["PreOrderQty"].ToString().Trim()));
                        order.UM = templateItem["UM"].ToString().Trim();
                        order.SelectedUM = order.UM;
                        order.UMPrice = templateItem["UM_Price"].ToString().Trim();
                        order.SeqNo = Convert.ToInt32(templateItem["Sequence#"].ToString().Trim());
                        order.ItemNumber = templateItem["Item#"].ToString().Trim();
                        order.ItemDescription = templateItem["ItemDescription"].ToString().Trim();
                        order.TemplateDetailID = templateItem["TemplateDetailID"].ToString().Trim();
                        //order.EffectiveFrom = templateItem["EffectiveDate"].ToString().Trim();
                        // order.EffectiveThru = templateItem["ExpiredDate"].ToString().Trim();
                        order.StkType = templateItem["StkType"].ToString().Trim();
                        order.SalesCat1 = templateItem["SalesCat1"].ToString().Trim();
                        order.SalesCat5 = templateItem["SalesCat5"].ToString().Trim();
                        order.ItemId = templateItem["ItemId"].ToString().Trim();
                        order.PrimaryUM = templateItem["PrimaryUOM"].ToString().Trim();
                        order.InclOnTmplt = true;
                        order.AppliedUMS = templateUms.ContainsKey(order.ItemNumber.Trim()) ? templateUms[order.ItemNumber.Trim()] : new List<string>() { order.UM };
                        //order.AppliedUMS = itemManager.GetAppliedUMs(order.ItemNumber).Count != 0 ? itemManager.GetAppliedUMs(order.ItemNumber) : new List<string>() { order.UM };
                        order.AvailableQty = Convert.ToInt32(templateItem["AvailableQty"].ToString().Trim());
                        order.QtyOnHand = Convert.ToInt32(templateItem["OnHandQty"].ToString().Trim());
                        order.UMConversionFactor = SetConversionFactorForItem(order.PrimaryUM, order.UM, order.ItemId, order.ItemNumber);
                        result.Add(order);
                    }
                    templateUms = null;
                }
                templateItems.Dispose();
                templateItems = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TemplateManager][GetTemplateItemsForCustomer][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][End:GetTemplateItemsForCustomer][customerID=" + customerID + "]");
            return result;
        }

        public Dictionary<string, List<string>> GetTemplateUms
        {
            get
            {
                #region OldCodeForGettingItemUoM's
                //DataTable templateUms = DbEngine.ExecuteDataSet(@"select distinct(UMUM) UMUM, IMLITM from BUSDTA.F41002 INNER JOIN BUSDTA.F4101 ON UMITM = imitm union select distinct(UMRUM) UMUM,IMLITM from BUSDTA.F41002 INNER JOIN BUSDTA.F4101 ON UMITM = imitm").Tables[0];
                #endregion
                Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][Start:GetTemplateUms]");
                Dictionary<string, List<string>> templateUmDic = new Dictionary<string, List<string>>();

                try
                {
                    //DataTable templateUms = DbEngine.ExecuteDataSet(@"select distinct im.IMLITM as ItemNumber , iu.UOM as UOM from busdta.ItemUoMs iu  inner join  busdta.F4101 im  on iu.ItemID = im.IMITM").Tables[0];
                    DataTable templateUms = DbEngine.ExecuteDataSet(@"CALL BUSDTA.SP_GetTemplateUms()").Tables[0];
                    foreach (DataRow dr in templateUms.Rows)
                    {
                        if (templateUmDic.ContainsKey(dr["ItemNumber"].ToString().Trim()))
                        {
                            templateUmDic[dr["ItemNumber"].ToString().Trim()].Add(dr["UOM"].ToString().Trim());
                        }
                        else
                        {
                            templateUmDic.Add(dr["ItemNumber"].ToString().Trim(), new List<string>());
                            templateUmDic[dr["ItemNumber"].ToString().Trim()].Add(dr["UOM"].ToString().Trim());
                        }
                    }
                    //sathish assigning to null once the opeation is done
                    //so that memory will be consumed
                    templateUms = null;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][TemplateManager][GetTemplateItemsForCustomer][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][End:GetTemplateUms]");
                return templateUmDic;
            }
        }

        /// <summary>
        /// Applies Pricing algorithm to Items in templates based on OrderQuantity and Unit of measure
        /// </summary>
        /// <param name="templateItems">ObservableCollection of Template Items</param>
        /// <param name="routeBranch">Route branch</param>
        /// <param name="shipToCustomerID">Invoice No to whom the items are to be delivered</param>
        /// <returns>ObservableCollection of Order items</returns>
        public SalesLogicExpress.Application.Helpers.TrulyObservableCollection<OrderItem> ApplyPricingToTemplates(ObservableCollection<TemplateItem> templateItems, string routeBranch, string shipToCustomerID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][Start:ApplyPricingToTemplates][routeBranch=" + routeBranch + ",shipToCustomerID=" + shipToCustomerID + "]");
            SalesLogicExpress.Application.Helpers.TrulyObservableCollection<OrderItem> orderItemFromTemplate = new SalesLogicExpress.Application.Helpers.TrulyObservableCollection<OrderItem>();
            try
            {
                //decimal unitPrice = 0;
                //Random randomProvider = new Random();
                PricingManager pricingManager = new PricingManager();
                //Random qtyOnHand = new Random();
                foreach (TemplateItem item in templateItems)
                {
                    if (item.OrderQty <= 0)
                    {
                        continue;
                    }
                    //unitPrice = 0;

                    //Code Line for setting unitprice is commented as the original price is stored as 
                    //read-only variable and assined a value in constructor of orderitem 

                    /* 
                        * Changed By Dinesh - TUI
                        * Calculate UnitPrice by Unit Converstion Factor
                    */
                    //if (item.UnitPriceByPricingUOM == 0 || Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(routeBranch, shipToCustomerID, item.ItemNumber, (item.OrderQty * item.UMConversionFactor).ToString())) == item.UnitPriceByPricingUOM)
                    //{
                    item.UnitPriceByPricingUOM = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(routeBranch, shipToCustomerID, item.ItemNumber, (item.OrderQty * item.UMConversionFactor).ToString()));
                    //}

                    //if (item.UnitPrice == 0 || OrderManager.GetPriceByUomFactor(item.ItemId, item.UMPrice, item.UM, item.UnitPriceByPricingUOM) == item.UnitPrice)
                    //{
                    //    if (item.ItemReasonCode < 2)
                    //        item.UnitPrice = OrderManager.GetPriceByUomFactor(item.ItemId, item.UMPrice, item.UM, item.UnitPriceByPricingUOM);
                    //}
                    //***************************************//
                    //Reduce  call of the GetPriceByUomFactor function.
                    //if (item.UnitPrice == 0 || OrderManager.GetPriceByUomFactor(item.ItemId, item.UMPrice, item.UM, item.UnitPriceByPricingUOM) != item.UnitPrice)
                    //{
                    //    if (item.ItemReasonCode < 2)
                    //        item.UnitPrice = OrderManager.GetPriceByUomFactor(item.ItemId, item.UMPrice, item.UM, item.UnitPriceByPricingUOM);
                    //}
                    //***************************************//
                    decimal _UnitPrice = OrderManager.GetPriceByUomFactor(item.ItemId, item.UMPrice, item.UM, item.UnitPriceByPricingUOM);
                    if (item.UnitPrice == 0 || _UnitPrice != item.UnitPrice)
                    {
                        if (item.ItemReasonCode < 2)
                            item.UnitPrice = _UnitPrice;
                    }

                    OrderItem orderItem = new OrderItem(item);
                    //orderItem.ExtendedPrice = (orderItem.UM == orderItem.UMPrice) ? decimal.Parse(orderItem.OrderQty.ToString()) * orderItem.UnitPrice : (decimal.Parse(orderItem.OrderQty.ToString()) * orderItem.UnitPrice * decimal.Parse(pricingManager.jdeUOMConversion(orderItem.UM, orderItem.UMPrice, int.Parse(orderItem.ItemId)).ToString()));
                    // vivensas round up added for calculating extended price
                    orderItem.ExtendedPrice = decimal.Parse(orderItem.OrderQty.ToString()) * Math.Round(orderItem.UnitPrice, 2, MidpointRounding.AwayFromZero);
                    orderItem.ItemId = item.ItemId;
                    orderItem.QtyOnHand = item.QtyOnHand;
                    orderItem.AvailableQty = item.AvailableQty;
                    orderItem.AverageStopQty = item.AverageStopQty;
                    orderItem.ActualQtyOnHand = item.ActualQtyOnHand;
                    orderItem.InclOnDemand = true;
                    orderItem.IsValidForOrder = item.IsValidForOrder;
                    orderItem.LastComittedQty = item.LastComittedQty;
                    orderItem.UMConversionFactor = item.UMConversionFactor;
                    //Issue ID - 42: Reason code was not provided in Create Order screen but the reason code number is displayed in Preview order Screen - Zakir
                    orderItem.ResonCodeDescription = item.ReasonCodeDescription;
                    orderItem.StkType = item.StkType;
                    orderItemFromTemplate.Add(orderItem);
                    orderItem = null;
                }
                pricingManager = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TemplateManager][ApplyPricingToTemplates][routeBranch=" + routeBranch + ",shipToCustomerID=" + shipToCustomerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");

            }            
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][End:ApplyPricingToTemplates][routeBranch=" + routeBranch + ",shipToCustomerID=" + shipToCustomerID + "]");
            return orderItemFromTemplate;
        }
        /// <summary>
        /// Save Template modifications for the given customer
        /// </summary>
        /// <param name="templateItems">List of Template Items</param>
        /// <param name="customerNumber">Invoice Number</param>
        //public void SaveTemplate(List<TemplateItem> templateItems, string customerNumber)
        //{
        //    Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][Start:SaveTemplate][customerNumber=" + customerNumber + ",templateItems=" + templateItems.ToString() + "]");
        //    try
        //    {
        //        string cmdDeleteTemplate = "Delete from Busdta.Order_Templates where ShipToId = " + customerNumber + "";
        //        DbEngine.ExecuteNonQuery(cmdDeleteTemplate);
        //        Int64 iSeqCount = 5;
        //        foreach (TemplateItem templateItem in templateItems)
        //        {
        //            string cmdSaveTemplate = "insert into Busdta.Order_Templates (TemplateType, ShipToID, TemplateDetailID, DisplaySeq, ItemID, LongItem, TemplateQuantity, TemplateUoM) " +
        //                "values " +
        //                "('BUILDUP', " + customerNumber + ", "+ templateItem.TemplateDetailID+" ," + iSeqCount + ", " + templateItem.ItemId + ", '" + templateItem.ItemNumber + "' " +
        //                ", " + templateItem.UsualQty * 10000 + ", '" + templateItem.UM + "');";

        //            DbEngine.ExecuteNonQuery(cmdSaveTemplate);
        //            iSeqCount = iSeqCount + 5;
        //        }
        //        //ResourceManager.Transaction.AddTransactionInQueueForSync(Transaction.SaveTemplate, SyncQueueManager.Priority.everything);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][TemplateManager][SaveTemplate][customerNumber=" + customerNumber + ",templateItems=" + templateItems.ToString() + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //        throw;
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][End:SaveTemplate][customerNumber=" + customerNumber + ",templateItems=" + templateItems.ToString() + "]");
        //}


        // Save Template Functionality Changes

        public void SaveTemplate(List<TemplateItem> templateItems, string customerNumber)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][Start:SaveTemplate][customerNumber=" + customerNumber + ",templateItems=" + templateItems.ToString() + "]");
            try
            {
                IEnumerable<TemplateItem> obsCollection = (IEnumerable<TemplateItem>)GetTemplateItemsForCustomer(customerNumber);
                var list1 = new List<TemplateItem>(obsCollection);
                obsCollection = null;
                var list2 = templateItems;
                //var resultlist = list1.Concat(list2).OrderBy(x=>x.TemplateDetailID).ToList();

                var merged = new List<TemplateItem>(list2);
                merged.AddRange(list1.Where(p2 =>
                                list2.All(p1 => p1.TemplateDetailID != p2.TemplateDetailID)));

                Int64 iSeqCount = 5;

                foreach (var i in merged)
                {

                    var s = list2.Where(x => x.TemplateDetailID == i.TemplateDetailID).FirstOrDefault();
                    var db = list1.Where(x => x.TemplateDetailID == i.TemplateDetailID).FirstOrDefault();


                    if (db != null && s == null)
                    {
                        //Delete
                        string cmdSaveTemplate = "Delete from Busdta.Order_Templates where ShipToId = " + customerNumber + " and TemplateDetailID=" +
                            i.TemplateDetailID + "";

                        DbEngine.ExecuteNonQuery(cmdSaveTemplate);
                    }
                    else if (db == null && s != null)
                    {
                        //Insert
                        string cmdSaveTemplate = "insert into Busdta.Order_Templates (TemplateType, ShipToID, TemplateDetailID, DisplaySeq, ItemID, LongItem, TemplateQuantity, TemplateUoM) " +
                        "values " +
                        "('BUILDUP   ', " + customerNumber + ", " + i.TemplateDetailID + " ," + iSeqCount + ", " + i.ItemId + ", '" + i.ItemNumber + "' " +
                        ", " + i.UsualQty + ", '" + i.UM + "');";

                        DbEngine.ExecuteNonQuery(cmdSaveTemplate);
                    }
                    else if (i.TemplateDetailID == s.TemplateDetailID && i.TemplateDetailID == db.TemplateDetailID)
                    {
                        if (s != db)
                        {
                            //Update


                            string cmdSaveTemplate = "update Busdta.Order_Templates set " +
                                " DisplaySeq=" + iSeqCount + ", ItemID=" + s.ItemId + ", LongItem='" + s.ItemNumber + "', TemplateQuantity=" +
                                s.UsualQty + ",TemplateUOM='" + s.UM + "' where ShipToID=" + customerNumber + " and TemplateDetailID=" +
                                i.TemplateDetailID + "";

                            DbEngine.ExecuteNonQuery(cmdSaveTemplate);
                        }
                    }

                    iSeqCount = iSeqCount + 5;
                }
                //sathish assigning to null once the opeation is done
                //so that memory will be consumed
                merged = null;
                list2 = null;
                list1 = null;
                //string cmdDeleteTemplate = "Delete from Busdta.Order_Templates where ShipToId = " + customerNumber + "";
                //DbEngine.ExecuteNonQuery(cmdDeleteTemplate);
                //Int64 iSeqCount = 5;
                //foreach (TemplateItem templateItem in templateItems)
                //{
                //    string cmdSaveTemplate = "insert into Busdta.Order_Templates (TemplateType, ShipToID, TemplateDetailID, DisplaySeq, ItemID, LongItem, TemplateQuantity, TemplateUoM) " +
                //        "values " +
                //        "('BUILDUP', " + customerNumber + ", " + templateItem.TemplateDetailID + " ," + iSeqCount + ", " + templateItem.ItemId + ", '" + templateItem.ItemNumber + "' " +
                //        ", " + templateItem.UsualQty * 10000 + ", '" + templateItem.UM + "');";

                //    DbEngine.ExecuteNonQuery(cmdSaveTemplate);
                //    iSeqCount = iSeqCount + 5;
                //}
                //ResourceManager.Transaction.AddTransactionInQueueForSync(Transaction.SaveTemplate, SyncQueueManager.Priority.everything);
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TemplateManager][SaveTemplate][customerNumber=" + customerNumber + ",templateItems=" + templateItems.ToString() + "][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][End:SaveTemplate][customerNumber=" + customerNumber + ",templateItems=" + templateItems.ToString() + "]");
        }







        /// <summary>
        /// Save PreOrder for the given customer
        /// </summary>
        /// <param name="preOrderItems">List of Template Items</param>
        /// <param name="customerNumber">Invoice Number</param>
        //public void SavePreOrder(ObservableCollection<TemplateItem> preOrderItems, string customerNumber, string StopDate)
        //{
        //    Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][Start:SavePreOrder][customerNumber=" + customerNumber + ",templateItems=" + preOrderItems.ToString() + "]");
        //    try
        //    {
        //        //DateTime dtStop = Convert.ToDateTime(StopDate);
        //        //string stopdate = dtStop.ToString("yyyy-MM-dd");
        //        string cmdDeletePreOrder = "Delete from busdta.M4016 where POORTP = 'S" + customerNumber + "' and POAN8 = " + customerNumber + "";
        //        DbEngine.ExecuteNonQuery(cmdDeletePreOrder);

        //        foreach (TemplateItem templateItem in preOrderItems)
        //        {

        //            if (templateItem.PreOrderQty > 0)
        //            {
        //                string cmdSavePreOrder = "insert into busdta.M4016 (POORTP, POAN8, POOSEQ, POITM, POLITM, POQTYU, POUOM, POLNTY, POSRP1, POSRP5, POCRBY, POCRDT, POUPBY, POUPDT) " +
        //                    "values " +
        //                    "('S" + customerNumber + "', " + customerNumber + ", " + templateItem.SeqNo + ", " + templateItem.ItemId + ", '" + templateItem.ItemNumber + "' " +
        //                    ", " + templateItem.PreOrderQty * 10000 + ", '" + templateItem.UM + "', 'S', " + "'" + templateItem.SalesCat1 + "'" +
        //                    "," + "'" + templateItem.SalesCat5 + "'" + "," + "'" + CommonNavInfo.UserName + "'" + "," + "'" + DateTime.Now.ToString("yyyy-MM-dd") + "'" + "," + "'" + CommonNavInfo.UserName + "'" + "," + "'" + DateTime.Now.ToString("yyyy-MM-dd") + "'" + ");";
        //                DbEngine.ExecuteNonQuery(cmdSavePreOrder);
        //            }
        //        }
        //        ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
        //        // //ResourceManager.Transaction.AddTransactionInQueueForSync(Transaction.SaveTemplate, SyncQueueManager.Priority.everything);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][TemplateManager][SavePreOrder][customerNumber=" + customerNumber + ",templateItems=" + preOrderItems.ToString() + "][ExceptionStackTrace = " + ex.StackTrace + "]");

        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][End:SavePreOrder][customerNumber=" + customerNumber + ",templateItems=" + preOrderItems.ToString() + "]");

        //}

        public double SetConversionFactorForItem(string ToUM, string FromUM, string ItemId, string ItemNo)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][Start:SetConversionFactorForItem]");

            double ConvesionFactor = 1.0;
            //string secondaryUM = PrimaryUOM.Trim().ToUpper() == "EA" ? "CS" : "EA";

            try
            {
                List<ItemUOMConversion> lst1 = new List<ItemUOMConversion>();

                try
                {
                    lst1 = UoMManager.ItemUoMFactorList.ToList();
                }
                catch (Exception) { }

                if (lst1 != null)
                {
                    var lst = lst1.FirstOrDefault(x => (x.ItemID == (ItemId.Trim()) && (x.FromUOM == (FromUM.ToString().Trim()) && (x.ToUOM == (ToUM.Trim())))));
                    lst1 = null;
                    if (lst == null)
                        ConvesionFactor = UoMManager.GetUoMFactor(FromUM, ToUM, Convert.ToInt32(ItemId), ItemNo.Trim());
                    else
                        ConvesionFactor = lst.ConversionFactor;
                    lst = null;
                }
                else
                {
                    ConvesionFactor = UoMManager.GetUoMFactor(FromUM, ToUM, Convert.ToInt32(ItemId), ItemNo.Trim());
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TemplateManager][SetConversionFactorForItem][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][End:SetConversionFactorForItem]");
            return ConvesionFactor;
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}






