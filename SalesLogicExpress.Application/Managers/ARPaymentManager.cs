﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;

namespace SalesLogicExpress.Application.Managers
{
    public class ARPaymentManager : IDisposable
    {

        #region Variables and object declaration

        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.ARPaymentManager");

        // Remove Global Vaiable - For Memroy Optimization at 12/24/2016 - By Velmani
        // ReasonCodeManager objReasonCodeManager = new ReasonCodeManager();
        // OrderManager objOrderManger = new OrderManager();

        // String Handling - Optimization (CA1820) for Memroy Optimization at 12/24/2016 - By Velmani
        private string customerId = string.Empty;
        private string companyId = string.Empty;

        #endregion

        #region Construtor

        public ARPaymentManager(string customerId, string companyId)
        {
            this.customerId = customerId;
            this.companyId = companyId;
        }

        #endregion

        #region Methods

        public List<Dictionary<string, decimal>> GetAgeingSummary(ObservableCollection<PaymentARModel.InvoiceDetails> OpenInvoices)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:GetAgeingSummary]");
            List<Dictionary<string, decimal>> AgeingSummary = new List<Dictionary<string, decimal>>();
            try
            {
                if (OpenInvoices == null)
                    //Changed by Marimuthu for AR Aging issue. Changed on 10/12/2016
                    // OpenInvoices = new Managers.InvoiceManager().GetInvoicesByShipTo(this.customerId).Where(s => s.OpenAmountInvDtl > 0).ToObservableCollection();
                    OpenInvoices = new Managers.InvoiceManager().GetInvoicesByShipTo(this.customerId).ToObservableCollection();

                if (OpenInvoices != null && OpenInvoices.Count > 0)
                {
                    //AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR30Days", OpenInvoices.Where(x => (x.AgingInvDtl <= 30) && (x.OpenAmountInvDtl > 0)).Sum(x => x.OpenAmountInvDtl) } });
                    //AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR60Days", OpenInvoices.Where(x => (x.AgingInvDtl <= 60 && x.AgingInvDtl > 30) && (x.OpenAmountInvDtl > 0)).Sum(x => x.OpenAmountInvDtl) } });
                    //AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR90Days", OpenInvoices.Where(x => (x.AgingInvDtl <= 90 && x.AgingInvDtl > 60) && (x.OpenAmountInvDtl > 0)).Sum(x => x.OpenAmountInvDtl) } });
                    //AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR90MoreDays", OpenInvoices.Where(x => (x.AgingInvDtl > 90) && (x.OpenAmountInvDtl > 0)).Sum(x => x.OpenAmountInvDtl) } });

                    AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR30Days", OpenInvoices.Where(x => (x.AgingInvDtl <= 30)).Sum(x => x.OpenAmountInvDtl) } });
                    AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR60Days", OpenInvoices.Where(x => (x.AgingInvDtl <= 60 && x.AgingInvDtl > 30)).Sum(x => x.OpenAmountInvDtl) } });
                    AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR90Days", OpenInvoices.Where(x => (x.AgingInvDtl <= 90 && x.AgingInvDtl > 60)).Sum(x => x.OpenAmountInvDtl) } });
                    AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR90MoreDays", OpenInvoices.Where(x => (x.AgingInvDtl > 90)).Sum(x => x.OpenAmountInvDtl) } });
                }
                else
                {
                    AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR30Days", 0 } });
                    AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR60Days", 0 } });
                    AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR90Days", 0 } });
                    AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR90MoreDays", 0 } });
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][GetAgeingSummary][ExceptionStackTrace = " + ex.Message + "]");
                throw;
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:GetAgeingSummary]");
            return AgeingSummary;
        }

        private List<SalesLogicExpress.Domain.PaymentARModel.InvoiceDetails> getInvoiceDetailsNew(string id)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:getInvoiceDetailsNew]");
            // String Handling - Optimization (CA1820) for Memroy Optimization at 12/26/2016 - By Velmani
            string query = string.Empty;
            var b = new List<SalesLogicExpress.Domain.PaymentARModel.InvoiceDetails>();
            try
            {

                // query = "SELECT AppliedDocumentID as InvoiceNumber, Convert(VARCHAR(20), ReceiptDate, 101) , ReciptJEDocType, (AppliedAmount)*-1 as AppliedAmount FROM BUSDTA.Receipt_Ledger WHERE VoidCode=' ' and ReceiptID=" + id;
                query = "CALL BUSDTA.SP_GetNewInvoiceDetails(@ReceiptID=" + id + ")";
                DataSet ds = DbEngine.ExecuteDataSet(query);
                query = string.Empty;//Sathish Changed this code.Disposing the objects
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var a = new PaymentARModel.InvoiceDetails();
                    a.InvoiceNoInvDtl = Convert.ToString(ds.Tables[0].Rows[i]["ReciptJEDocType"]).Trim() == "RU" ? "Unapplied" : Convert.ToString(ds.Tables[0].Rows[i]["InvoiceNumber"]).Trim();
                    a.DateInvDtl = Convert.ToString(ds.Tables[0].Rows[i]["ReciptJEDocType"]).Trim() == "RU" ? "" : Convert.ToString(ds.Tables[0].Rows[i]["ReceiptDate"]).Replace("1/1/0001", "");
                    a.OpenAmountInvDtl = Convert.ToDecimal(ds.Tables[0].Rows[i]["AppliedAmount"]);
                    b.Add(a);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][getInvoiceDetailsNew][ExceptionStackTrace = " + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:getInvoiceDetailsNew]");
            return b;
        }

        /// <summary>
        /// Returns updated list of Receipt History 
        /// </summary>
        /// <returns>List Of Receipt History</returns>
        public List<Domain.PaymentARModel.ReceiptHistory> GetReceiptHistory()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:GetReceiptHistory]");
            List<Domain.PaymentARModel.ReceiptHistory> receiptHistoryList = new List<PaymentARModel.ReceiptHistory>();
            //StringBuilder objQueryBuilder = new StringBuilder();//Sathish Changed this code.Disposing the objects
            string BillTo = CustomerManager.GetCustomerBillTo(customerId);
            try
            {
                //            string sql = "SELECT distinct AppliedCustBillToID,ReceiptID,Convert(VARCHAR(20),ChequeDate,101) as ChequeDate, PaymentType, CASE WHEN PaymentType='1' THEN ChequeNum ELSE ' ' END as ChequeNum,Convert(VARCHAR(20),ReceiptDate,101) as ReceiptDate,CASE WHEN PaymentType='1' THEN 'CHECK' ELSE 'CASH' END as Type,ReceiptAmount" +
                //                ",(isnull((SELECT upper(trim(StatusTypeDESC)) FROM BUSDTA.Status_Type WHERE StatusTypeId=RH.StatusId),'UNKNOWN')) AS Status " +
                //" FROM BUSDTA.Receipt_Ledger RH WHERE AppliedCustBillToId=" + BillTo + " and VoidCode=' ' " +
                //" Group By AppliedCustBillToID,ReceiptID,ChequeDate,ChequeNum,ReceiptDate,PaymentType,ReceiptAmount,ReceiptOpenAmount,Status " +
                //" Order By ReceiptID DESC";

                //                string sql = @"SELECT distinct AppliedCustBillToID,ReceiptID,Convert(VARCHAR(20),ChequeDate,101) as ChequeDate, PaymentType, CASE WHEN PaymentType='1' THEN ChequeNum ELSE ' ' END as ChequeNum,
                //SettelmentId AS SettlementId, isnull(RH.VoidReasonCode,'0') AS ReasonCode,
                //Convert(VARCHAR(20),ReceiptDate,101) as ReceiptDate,CASE WHEN PaymentType='1' THEN 'CHECK' ELSE 'CASH' END as Type,ReceiptAmount
                //,(isnull((SELECT upper(trim(StatusTypeDESC)) FROM BUSDTA.Status_Type WHERE StatusTypeId=RH.StatusId),'UNKNOWN')) AS Status
                //FROM BUSDTA.Receipt_Ledger RH WHERE AppliedCustBillToId=" + BillTo + @" 
                //Group By AppliedCustBillToID,ReceiptID,ChequeDate,ChequeNum,ReceiptDate,PaymentType,ReceiptAmount,Status, SettlementId, ReasonCode
                //Order By ReceiptId DESC, ReceiptDate DESC";

                //                string sql = @"SELECT distinct rl.AppliedCustBillToID,rl.ReceiptID,Convert(VARCHAR(20),rl.ChequeDate,101) as ChequeDate, rl.PaymentType, CASE WHEN rl.PaymentType='1' THEN rl.ChequeNum ELSE ' ' END as ChequeNum,
                //rl.SettelmentId AS SettlementId, isnull(rl.VoidReasonCode,'0') AS ReasonCode,
                //Convert(VARCHAR(20),rl.ReceiptDate,101) as ReceiptDate,CASE WHEN rl.PaymentType='1' THEN 'CHECK' ELSE 'CASH' END as Type,rl.ReceiptAmount
                //,(isnull((SELECT upper(trim(StatusTypeDESC)) FROM BUSDTA.Status_Type WHERE StatusTypeId=rl.StatusId),'UNKNOWN')) AS Status, rh.DocumentID
                //FROM BUSDTA.Receipt_Ledger Rl 
                //left join BUSDTA.Receipt_Header rh on rh.ReceiptID=rl.ReceiptID  
                //WHERE AppliedCustBillToId=" + BillTo + @"
                //Group By rl.AppliedCustBillToID,rl.ReceiptID,rl.ChequeDate,rl.ChequeNum,rl.ReceiptDate,rl.PaymentType,rl.ReceiptAmount,Status, SettlementId, ReasonCode, DocumentID
                //Order By rl.ReceiptId DESC, ReceiptDate DESC";
                string sql = "CALL BUSDTA.SP_GetReceiptHistory(@BillTo=" + BillTo + ")";

                //string strReturnOrderStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='RETORD'");
                string strReturnOrderStatusId = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='RETORD')");
                DataSet drReceiptHistory = null; // DbEngine.ExecuteDataSet(sql);
                drReceiptHistory = DbEngine.ExecuteDataSet(sql, "ReceiptHistory");
                for (int i = 0; i < drReceiptHistory.Tables[0].Rows.Count; i++)
                {
                    Domain.PaymentARModel.ReceiptHistory obj = new PaymentARModel.ReceiptHistory();
                    obj.ReceiptId = Convert.ToString(drReceiptHistory.Tables[0].Rows[i]["ReceiptID"]);
                    //obj.IsCreditMemo = Convert.ToInt32(string.IsNullOrEmpty(drReceiptHistory.Tables[0].Rows[i]["CreditReasonCodeId"].ToString().Trim()) ? "0" : drReceiptHistory.Tables[0].Rows[i]["CreditReasonCodeId"]) != 0 ? true : false;
                    //obj.CreditReason = drReceiptHistory.Tables[0].Rows[i]["CreditReason"].ToString();
                    obj.DocumentId = string.IsNullOrEmpty(drReceiptHistory.Tables[0].Rows[i]["DocumentID"].ToString()) ? 0 : Convert.ToDecimal(drReceiptHistory.Tables[0].Rows[i]["DocumentID"]);
                    obj.ReceiptNo = Convert.ToString(drReceiptHistory.Tables[0].Rows[i]["ReceiptID"]);
                    obj.ReceiptType = Convert.ToString(drReceiptHistory.Tables[0].Rows[i]["Type"]);
                    obj.ReceiptAmount = Convert.ToDecimal(drReceiptHistory.Tables[0].Rows[i]["ReceiptAmount"]);
                    obj.Status = Convert.ToString(drReceiptHistory.Tables[0].Rows[i]["Status"]);
                    obj.SettlementId = Convert.ToString(drReceiptHistory.Tables[0].Rows[i]["SettlementId"]);
                    obj.ReasonCode = (drReceiptHistory.Tables[0].Rows[i]["ReasonCode"].ToString().Trim().Equals(strReturnOrderStatusId) ? "return" : "");
                    obj.ReceiptDate = Convert.ToString(drReceiptHistory.Tables[0].Rows[i]["ReceiptDate"]);
                    obj.CheckNo = Convert.ToString(drReceiptHistory.Tables[0].Rows[i]["ChequeNum"]);
                    obj.CheckDate = Convert.ToString(drReceiptHistory.Tables[0].Rows[i]["ChequeDate"]);
                    obj.IsCash = !Convert.ToBoolean(Convert.ToInt16(drReceiptHistory.Tables[0].Rows[i]["PaymentType"].ToString().Trim()));
                    obj.IsCheck = Convert.ToBoolean(Convert.ToInt16(drReceiptHistory.Tables[0].Rows[i]["PaymentType"].ToString().Trim()));

                    obj.PaidInvoices = getInvoiceDetailsNew(obj.ReceiptNo);
                    receiptHistoryList.Add(obj);
                }

                // DataSet Handling - For Memroy Optimization at 12/26/2016 - By Velmani   
                drReceiptHistory = null;

                //string strUnknownStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='UNKNOWN'");

                ////Used on xaml trigger for displaying the return on screen
                //string strReturnOrderStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='RETORD'");

                ////Write query to get list of receipt history 
                //objQueryBuilder.Append("SELECT RH.CustShipToId, RH.SettelmentId AS SettlementId, isnull(RH.CreditReasonCodeId, 0) AS CreditReasonCodeId, ISNULL(RM.ReasonCodeDescription,'') AS CreditReason, TRIM(ReceiptID) AS ReceiptId, ReceiptNumber, ROUND(TransactionAmount,2) AS PaymentAmount, case when PaymentMode= '0' then 0 else 1 end AS PaymentType, ");
                //objQueryBuilder.Append("(CASE PaymentMode WHEN '0' THEN 'Cash' WHEN '1' THEN 'Check' END) AS ReceiptType,trim( ISNULL(ChequeNum,'')) AS ChequeNum,  Convert(VARCHAR(20), ChequeDate, 101) AS ChequeDate, ");
                //objQueryBuilder.Append(" Convert(VARCHAR(20), ReceiptDate, 101) AS ReceiptDate, StatusId, isnull(rh.ReasonCodeId,0) AS ReasonCode, ");
                //objQueryBuilder.Append(" (isnull((SELECT upper(trim(StatusTypeDESC)) FROM BUSDTA.Status_Type WHERE StatusTypeId=RH.StatusId),'UNKNOWN')) AS Status ");
                //objQueryBuilder.Append(" FROM BUSDTA.Receipt_Header RH ");
                //objQueryBuilder.Append(" LEFT OUTER JOIN BUSDTA.ReasonCodeMaster RM on RM.ReasonCodeId = RH.CreditReasonCodeId ");
                //objQueryBuilder.Append(" WHERE RH.CustBillToId=" + BillTo + " ORDER BY RH.ReceiptDate DESC, RH.ReceiptId DESC");
                //receiptHistory = DbEngine.ExecuteDataSet(objQueryBuilder.ToString(), "ReceiptHistory");

                ////If Invoice Status is null, then it will treated as open. 
                //objQueryBuilder.Clear();
                //objQueryBuilder.Append("SELECT CL.CustShipToID AS CustomerId, TRIM(CL.ReceiptId) AS ReceiptId,  CL.InvoiceId AS InvoiceId,");
                //objQueryBuilder.Append(" TRIM(ISNULL(IH.DeviceInvoiceNumber,'')) +'/SO' AS InvoiceNumber, ROUND(CL.ReceiptAppliedAmt,2) AS InvoiceAmt, ");
                //objQueryBuilder.Append(" (SELECT trim(ucase(StatusTypeDesc)) FROM BUSDTA.Status_Type WHERE StatusTypeId=isnull(isnull(NULLIF(IH.ARStatusID, " + strUnknownStatusId + "), IH.DeviceStatusID),1)) AS Status,");
                //objQueryBuilder.Append(" convert(VARCHAR(20), IH.DeviceInvoiceDate, 101) AS InvoiceDate FROM BUSDTA.Customer_Ledger CL");
                //objQueryBuilder.Append(" INNER JOIN BUSDTA.Invoice_Header IH ON CL.InvoiceId=IH.InvoiceId AND CL.CustBillToID=IH.CustBillToID");
                //objQueryBuilder.Append(" WHERE ISNULL(CL.InvoiceId,0)!=0 AND ISNULL(CL.ReceiptId,0)!=0 AND CL.CustBillToID=" + BillTo + " AND CL.IsActive=1");
                //objQueryBuilder.Append(" UNION ALL");
                //objQueryBuilder.Append(" SELECT CL.CustShipToID AS CustomerId, TRIM(CL.ReceiptId) AS ReceiptId, '' AS InvoiceId, 'Unapplied Cash'");
                //objQueryBuilder.Append(" AS InvoiceNumber,  ROUND(RH.TransactionAmount-SUM(CASE CL.IsActive WHEN 1 THEN ISNULL(CL.ReceiptAppliedAmt,0) ELSE 0 END),2) AS InvoiceAmt ,");
                //objQueryBuilder.Append(" '' AS Status,  '1/1/0001' AS InvoiceDate FROM BUSDTA.Customer_Ledger CL ");
                //objQueryBuilder.Append(" INNER JOIN BUSDTA.Receipt_Header RH ON CL.ReceiptId=RH.ReceiptId WHERE ISNULL(CL.InvoiceId,0)!=0 ");
                //objQueryBuilder.Append(" AND ISNULL(CL.ReceiptId,0)!=0 AND CL.CustBillToID=" + BillTo + " AND (RH.StatusId NOT IN (SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD IN ('VOID', 'VDSTLD')))");
                //objQueryBuilder.Append(" GROUP BY CL.CustShipToID, ReceiptId, InvoiceNumber, InvoiceDate, StatusId, Status, RH.TransactionAmount HAVING InvoiceAmt<>0 ");
                //objQueryBuilder.Append(" UNION ALL");
                //objQueryBuilder.Append(" SELECT CL.CustShipToID AS CustomerId, TRIM(CL.ReceiptId) AS ReceiptId, '' AS InvoiceId,");
                //objQueryBuilder.Append(" 'Unapplied Cash'  AS InvoiceNumber, ROUND(CL.ReceiptUnAppliedAmt,2) AS InvoiceAmt ,'' AS Status, ");
                //objQueryBuilder.Append(" '1/1/0001' AS InvoiceDate  FROM BUSDTA.Customer_Ledger CL WHERE CL.ReceiptId NOT IN");
                //objQueryBuilder.Append(" (SELECT isnull(ReceiptId,0) FROM BUSDTA.Customer_Ledger WHERE ISNULL(InvoiceId,0)!=0)");
                //objQueryBuilder.Append(" AND ISNULL(InvoiceId,0)=0  AND ROUND(CL.ReceiptUnAppliedAmt,2)<>0 AND CL.CustBillToID=" + BillTo);
                //receiptHistory = DbEngine.ExecuteDataSet(objQueryBuilder.ToString(), null, "PaidInvoices", receiptHistory);

                //receiptHistoryList = receiptHistory.Tables["ReceiptHistory"].AsEnumerable()
                //        .Select(drReceiptHistory =>
                //         new SalesLogicExpress.Domain.PaymentARModel.ReceiptHistory
                //         {
                //             ReceiptId = Convert.ToString(drReceiptHistory["ReceiptId"]),
                //             IsCreditMemo = Convert.ToInt32(drReceiptHistory["CreditReasonCodeId"]) != 0 ? true : false,
                //             CreditReason = drReceiptHistory["CreditReason"].ToString(),
                //             ReceiptNo = Convert.ToString(drReceiptHistory["ReceiptNumber"]),
                //             ReceiptType = Convert.ToString(drReceiptHistory["ReceiptType"]),
                //             ReceiptAmount = Convert.ToDecimal(drReceiptHistory["PaymentAmount"]),
                //             Status = Convert.ToString(drReceiptHistory["Status"]),
                //             SettlementId = Convert.ToString(drReceiptHistory["SettlementId"]),
                //             ReasonCode = (drReceiptHistory["ReasonCode"].ToString().Trim().Equals(strReturnOrderStatusId)?"return":""),
                //             ReceiptDate = Convert.ToString(drReceiptHistory["ReceiptDate"]),
                //             CheckNo = Convert.ToString(drReceiptHistory["ChequeNum"]),
                //             CheckDate = Convert.ToString(drReceiptHistory["ChequeDate"]),
                //             IsCash = !Convert.ToBoolean(drReceiptHistory["PaymentType"]),
                //             IsCheck = Convert.ToBoolean(drReceiptHistory["PaymentType"]),
                //             PaidInvoices = (receiptHistory.Tables["PaidInvoices"].AsEnumerable()
                //                                .Where(dr => dr["ReceiptId"].Equals(drReceiptHistory["ReceiptId"]))
                //                                .Select(dr =>
                //                                    new SalesLogicExpress.Domain.PaymentARModel.InvoiceDetails
                //                                    {
                //                                        InvoiceNoInvDtl = (Convert.ToInt32(drReceiptHistory["CreditReasonCodeId"]) != 0 ? true : false) ? "" : Convert.ToString(dr["InvoiceNumber"]),
                //                                        DateInvDtl = Convert.ToString(dr["InvoiceDate"]).Replace("1/1/0001", ""),
                //                                        OpenAmountInvDtl = Convert.ToDecimal(dr["InvoiceAmt"]),
                //                                        StatusInvDtl = Convert.ToString(dr["Status"]).Trim()
                //                                    }).ToList<SalesLogicExpress.Domain.PaymentARModel.InvoiceDetails>())
                //         }
                //        ).ToList<SalesLogicExpress.Domain.PaymentARModel.ReceiptHistory>();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][GetReceiptHistory][ExceptionStackTrace = " + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:GetReceiptHistory]");

            // String Builder Handling - For Memroy Optimization at 12/26/2016 - By Velmani            
            //objQueryBuilder = null;
            return receiptHistoryList;
        }

        /// <summary>
        /// Updates the payment type 
        /// </summary>
        /// <param name="isCash">True if payment type is cash, false if check</param>
        /// <param name="receiptNo">Receipt number</param>
        /// <param name="checkNo">Check no</param>
        /// <param name="receiptId">Autogenerated receipt Id</param>
        public void UpdateReceiptType(bool isCash, string receiptNo, string checkNo, string receiptId)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:UpdateReceiptType]");
            try
            {
                // String Handling - For Memroy Optimization at 12/26/2016 - By Velmani
                string strSetQuery = string.Empty;

                if (isCash)
                    strSetQuery = "PaymentType='0', ChequeNum=''";
                else
                    strSetQuery = "PaymentType='1', ChequeNum=" + checkNo;

                //ToDo - Write Code to update the Receipt Type
                //string updateQuery = string.Format("UPDATE BUSDTA.Receipt_Header SET {0} WHERE CustShipToId={1} AND ReceiptNumber='{2}' AND RouteId={3}", strSetQuery, customerId, receiptNo, CommonNavInfo.RouteID.ToString());
                string updateQuery = string.Empty;
                 updateQuery= string.Format("UPDATE BUSDTA.Receipt_Ledger SET {0} WHERE ReceiptID='{1}'", strSetQuery, receiptNo);
                DbEngine.ExecuteNonQuery(updateQuery);
                updateQuery = string.Empty;
                ResourceManager.QueueManager.QueueProcess("ARPayments", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][UpdateReceiptType][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:UpdateReceiptType]");
        }

        /// <summary>
        /// Returns available un-applied amount of the receipt 
        /// </summary>
        /// <param name="receiptId">Receipt id</param>
        /// <returns>un-applied receipt amount</returns>
        public string GetReceiptUnAppliedAmt(string receiptId)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:GetReceiptUnAppliedAmt]");
            // String Handling - For Memroy Optimization at 12/26/2016 - By Velmani
            string query = string.Empty;
            string unappliedAmt = string.Empty;
            try
            {
                //query = "SELECT A.UnappliedAmount FROM (SELECT TRIM(CL.ReceiptId) AS ReceiptId, ROUND((RH.TransactionAmount-SUM(isnull(CL.ReceiptAppliedAmt,0))-SUM(isnull(CL.ConcessionAmt,0))),2) AS UnappliedAmount ";
                //query = query + " FROM BUSDTA.Customer_Ledger CL INNER JOIN BUSDTA.Receipt_Header RH ON CL.ReceiptId=RH.ReceiptId";
                //query = query + " WHERE ISNULL(CL.InvoiceId,'')!='' AND ISNULL(CL.ReceiptId,'')!='' AND CL.ReceiptId=" + receiptId;
                //query = query + " GROUP BY ReceiptId, RH.TransactionAmount HAVING UnappliedAmount<>0) A";

                //query = "select SUM(AppliedAmount) as UnappliedReceipt from BUSDTA.Receipt_Ledger where ReceiptId='" + receiptId + "' and AppliedDocType='RU'";
                query = "Call BUSDTA.SP_GetReceiptUnAppliedAmt(@ReceiptId='" + receiptId + "')";
                unappliedAmt = DbEngine.ExecuteScalar(query);
                query = string.Empty;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][GetReceiptUnAppliedAmt][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:GetReceiptUnAppliedAmt]");
            return unappliedAmt;
        }

        /// <summary>
        /// Updates receipt status into database 
        /// </summary>
        /// <param name="receiptId">receipt id to be updated</param>
        /// <param name="receiptStatus">receipt status</param>
        public void UpdateReceiptStatus(string receiptId, string receiptStatus)
        {
            // String Handling - For Memroy Optimization at 12/26/2016 - By Velmani
            string query = string.Empty;
            string strVoidStatusId = string.Empty;
            string strVoidSettledStatusId = string.Empty;
            string strSettledStatusId = string.Empty;

            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:UpdateReceiptStatus]");
            try
            {
                //Get status id
                //strVoidStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VOID'");
                //strVoidSettledStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VDSTLD'");
                //strSettledStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='STTLD'");
                strVoidStatusId = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='VOID')");
                strVoidStatusId = strVoidStatusId.Length != 0 ? strVoidStatusId : "0";
                strVoidSettledStatusId = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='VDSTLD')");
                strVoidSettledStatusId = strVoidSettledStatusId.Length != 0 ? strVoidSettledStatusId : "0";
                strSettledStatusId = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='STTLD')");
                strSettledStatusId = strSettledStatusId.Length != 0 ? strSettledStatusId : "0";

                if (!string.IsNullOrEmpty(ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID))
                    query = ", SettelmentId=" + ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID;

                //query = "UPDATE BUSDTA.Receipt_Header SET StatusId=(CASE StatusId WHEN 6 THEN 26 ELSE 3 END) " + query + " WHERE receiptId IN (" + receiptId + ") AND RouteId=" + CommonNavInfo.RouteID.ToString();
                query = "UPDATE BUSDTA.Receipt_Header SET UpdatedBy =" + UserManager.UserId + ",UpdatedDatetime = getdate(),StatusId=(CASE StatusId WHEN " + strVoidStatusId + " THEN " + strVoidSettledStatusId + " ELSE " + strSettledStatusId + " END) " + query + " WHERE receiptId IN (" + receiptId + ") AND RouteId=" + CommonNavInfo.RouteID.ToString();
                Logger.Info(" SettlementUpdateQuery = " + query);
                DbEngine.ExecuteNonQuery(query);

                query = string.Empty;
                // Changes On - 09/07/16
                // Update Settlement ID - For Receipt_Ledger Table
                if (!string.IsNullOrEmpty(ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID))
                    query = ", SettelmentId=" + ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID;

                query = "UPDATE BUSDTA.Receipt_Ledger SET UpdatedBy =" + UserManager.UserId + ",UpdatedDatetime = getdate(),StatusId=(CASE StatusId WHEN " + strVoidStatusId + " THEN " + strVoidSettledStatusId + " ELSE " + strSettledStatusId + " END) " + query + " WHERE ReceiptID IN (" + receiptId + ") AND SourceId <> 0";
                Logger.Info(" SettlementUpdateQuery = " + query);
                DbEngine.ExecuteNonQuery(query);
                ResourceManager.QueueManager.QueueProcess("ARPayments", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][UpdateReceiptStatus][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:UpdateReceiptStatus]");
        }

        public void UpdateExpenseStatus(string expenseId, string expenseStatus)
        {
            // String Handling - For Memroy Optimization at 12/26/2016 - By Velmani
            string query = string.Empty;
            string strVoidStatusId = string.Empty;
            string strVoidSettledStatusId = string.Empty;
            string strSettledStatusId = string.Empty;

            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:UpdateExpenseStatus]");
            try
            {
                //Get status id
                //strVoidStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VOID'");
                //strVoidSettledStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VDSTLD'");
                //strSettledStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='STTLD'");
                strVoidStatusId = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='VOID')");
                strVoidStatusId = strVoidStatusId.Length != 0 ? strVoidStatusId : "0";
                strVoidSettledStatusId = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='VDSTLD')");
                strVoidSettledStatusId = strVoidSettledStatusId.Length != 0 ? strVoidSettledStatusId : "0";
                strSettledStatusId = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='STTLD')");
                strSettledStatusId = strSettledStatusId.Length != 0 ? strSettledStatusId : "0";

                //if (!string.IsNullOrEmpty(ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID))
                //    query = ", SettelmentId=" + ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID;

                //query = "UPDATE BUSDTA.Receipt_Header SET StatusId=(CASE StatusId WHEN 6 THEN 26 ELSE 3 END) " + query + " WHERE receiptId IN (" + receiptId + ") AND RouteId=" + CommonNavInfo.RouteID.ToString();
                query = "UPDATE BUSDTA.ExpenseDetails SET RouteSettlementId = '" + ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID + "', StatusId=(CASE StatusId WHEN " + strVoidStatusId + " THEN " + strVoidSettledStatusId + " ELSE " + strSettledStatusId + " END) WHERE ExpenseId IN (" + expenseId + ") AND RouteId=" + CommonNavInfo.RouteID.ToString();

                Logger.Info(" SettlementUpdateQuery = " + query);
                DbEngine.ExecuteNonQuery(query);
                ResourceManager.QueueManager.QueueProcess("EndOfDay", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][UpdateExpenseStatus][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:UpdateExpenseStatus]");
        }

        public void UpdateMoneyOrderStatus(string mOID, string mOStatus)
        {
            // String Handling - For Memroy Optimization at 12/26/2016 - By Velmani
            string query = string.Empty;
            string strVoidStatusId = string.Empty;
            string strVoidSettledStatusId = string.Empty;
            string strSettledStatusId = string.Empty;

            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:UpdateMoneyOrderStatus]");
            try
            {
                //Get status id
                //strVoidStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VOID'");
                //strVoidSettledStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VDSTLD'");
                //strSettledStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='STTLD'");
                strVoidStatusId = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='VOID')");
                strVoidStatusId = strVoidStatusId.Length != 0 ? strVoidStatusId : "0";
                strVoidSettledStatusId = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='VDSTLD')");
                strVoidSettledStatusId = strVoidSettledStatusId.Length != 0 ? strVoidSettledStatusId : "0";
                strSettledStatusId = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='STTLD')");
                strSettledStatusId = strSettledStatusId.Length != 0 ? strSettledStatusId : "0";

                //if (!string.IsNullOrEmpty(ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID))
                //    query = ", SettelmentId=" + ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID;

                //query = "UPDATE BUSDTA.Receipt_Header SET StatusId=(CASE StatusId WHEN 6 THEN 26 ELSE 3 END) " + query + " WHERE receiptId IN (" + receiptId + ") AND RouteId=" + CommonNavInfo.RouteID.ToString();
                query = "UPDATE BUSDTA.MoneyOrderDetails SET RouteSettlementId = '" + ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID + "', StatusId=(CASE StatusId WHEN " + strVoidStatusId + " THEN " + strVoidSettledStatusId + " ELSE " + strSettledStatusId + " END) WHERE MoneyOrderId IN (" + mOID + ") AND RouteId=" + CommonNavInfo.RouteID.ToString();

                Logger.Info(" SettlementUpdateQuery = " + query);
                DbEngine.ExecuteNonQuery(query);
                ResourceManager.QueueManager.QueueProcess("EndOfDay", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][UpdateMoneyOrderStatus][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:UpdateMoneyOrderStatus]");
        }

        public decimal GetUnAppliedAmtForCustomer()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:GetUnAppliedAmtForCustomer]");
            decimal result = 0;
            try
            {
                //                string strAmount = DbEngine.ExecuteScalar(@"
                //                                    select round(sum(isnull(cl.receiptunappliedamt,0)) - sum(isnull(cl.receiptappliedamt,0)),2)  UnappliedAmount
                //                                    from BUSDTA.Customer_Ledger cl join busdta.Receipt_Header RH on cl.Receiptid=RH.Receiptid
                //                                    where RH.CustBillToID=" + customerId + " and cl.isactive='1' ");
                //                if (!string.IsNullOrEmpty(strAmount))
                //                {
                //                    result = Convert.ToDecimal(strAmount);
                //                }

                string BillTo = CustomerManager.GetCustomerBillTo(customerId);

                // String Handling - For Memroy Optimization at 12/26/2016 - By Velmani
                string strAmount = string.Empty;//DbEngine.ExecuteScalar(@"
                //select SUM(OpenAmt) as UnappliedReceipt from BUSDTA.Customer_Ledger_R1 where CustBillToId=" + BillTo + " and TransactionTypeID>=100 ");

                //                strAmount = DbEngine.ExecuteScalar(@"
                //                        select SUM(AppliedAmount) as UnappliedReceipt from BUSDTA.Receipt_Ledger where SourceID<>0 and ReceiptCustBillToId=" + BillTo + " and ReciptJEDocType='RU'");
                strAmount = DbEngine.ExecuteScalar(@"CALL BUSDTA.SP_GetUnAppliedAmtForCustomer(@BillTo= " + BillTo + " )");
                if (!string.IsNullOrEmpty(strAmount))
                {
                    result = Convert.ToDecimal(strAmount);
                }

                strAmount = string.Empty;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][GetUnAppliedAmtForCustomer][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:GetUnAppliedAmtForCustomer]");
            return result;
        }

        //        public decimal GetInvoiceDetailsForCustomer()
        //        {
        //            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:GetInvoiceDetailsForCustomer]");
        //            decimal result = 0;
        //            try
        //            {
        //                String BillTo = CustomerManager.GetCustomerBillTo(customerId);
        //                string strAmount = DbEngine.ExecuteScalar(@"
        //                                    select SUM(OpenAmt) as OpenInvoices from BUSDTA.Customer_Ledger_R1 where CustBillToId=" + BillTo + " and TransactionTypeID<100 and ARDocumentId not in (select applieddocumentid from busdta.Receipt_Ledger where ReceiptCustBillToId=" + BillTo + ")");
        //                if (!string.IsNullOrEmpty(strAmount))
        //                {
        //                    result = Convert.ToDecimal(strAmount);
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][GetInvoiceDetailsForCustomer][ExceptionStackTrace = " + ex.StackTrace + "]");
        //            }
        //            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:GetInvoiceDetailsForCustomer]");
        //            return result;
        //        }

        //        public decimal GetTotalBalanceForCustomer()
        //        {
        //            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:GetTotalBalanceForCustomer]");
        //            decimal result = 0;
        //            try
        //            {
        //                String BillTo = CustomerManager.GetCustomerBillTo(customerId);
        //                string strAmount = DbEngine.ExecuteScalar(@"
        //                                    select SUM(OpenAmt) as TotalBalance from BUSDTA.Customer_Ledger_R1 where CustBillToId=" + BillTo + " and ARDocumentId not in (select applieddocumentid from busdta.Receipt_Ledger where ReceiptCustBillToId=" + BillTo + ")");
        //                if (!string.IsNullOrEmpty(strAmount))
        //                {
        //                    result = Convert.ToDecimal(strAmount);
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][GetTotalBalanceForCustomer][ExceptionStackTrace = " + ex.StackTrace + "]");
        //            }
        //            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:GetTotalBalanceForCustomer]");
        //            return result;
        //        }
        /// <summary>
        /// Retrieves list of available reason code
        /// </summary>
        /// <returns>List of reason code</returns>
        public List<string> GetVoidReasonList()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:GetVoidReasonList]");
            try
            {
                //string strQuery = "SELECT ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster WHERE ReasonCodeType='Void Receipt'";
                //DataTable dtVoidReasonList = DbEngine.ExecuteDataSet(strQuery, "VoidReasonList").Tables[0];

                // Global Variable Handling - Optimization (CA1820) for Memroy Optimization at 12/24/2016 - By Velmani
                ReasonCodeManager objReasonCodeManager = new ReasonCodeManager();
                DataTable dtVoidReasonList = objReasonCodeManager.ARPaymentManager_GetVoidReasonList();
                objReasonCodeManager.Dispose();
                objReasonCodeManager = null;

                return (from o in dtVoidReasonList.AsEnumerable()
                        select o.Field<string>("ReasonCodeDescription")).ToList<string>();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][GetVoidReasonList][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            finally
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:GetVoidReasonList]");
            }
        }

        /// <summary>
        /// Voids a specied receipt 
        /// </summary>
        /// <param name="receiptId">Receipt Id to be voided</param>
        /// <param name="reasonCodeId">Reason code provided for voiding receipt</param>
        public void VoidReceipt(string receiptId, string reasonCodeId, string routeId, bool reasonCodeTypeFlag)
        {
            // String Handling - For Memroy Optimization at 12/26/2016 - By Velmani
            //string strQuery = "";
            string strVoidStatusId = string.Empty;
            string reasonCodeType = string.Empty;
            if (reasonCodeTypeFlag)
            {
                reasonCodeType = "Void Receipt";
            }
            else reasonCodeType = "Void Credit Memo";

            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:VoidReceipt]");
            try
            {
                //strVoidStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VOID'");
                strVoidStatusId = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='VOID')");
                strVoidStatusId = strVoidStatusId.Length != 0 ? strVoidStatusId : "0";

                //strQuery = string.Format("UPDATE BUSDTA.Receipt_Header SET StatusId=" + strVoidStatusId + ", ReasonCodeId=(SELECT ReasonCodeId FROM BUSDTA.ReasonCodeMaster WHERE TRIM(ReasonCodeDescription)=TRIM('{0}') AND ReasonCodeType='" + reasonCodeType + "') WHERE CustBillToId={1} AND ReceiptId={2} AND RouteId={3}", reasonCodeId, customerId, receiptId, routeId);
                //DbEngine.ExecuteNonQuery(strQuery);

                //strQuery = string.Format("UPDATE BUSDTA.Receipt_Ledger SET StatusID='{0}', VoidCode='V', VoidReasonCode=(SELECT ReasonCodeId FROM BUSDTA.ReasonCodeMaster WHERE TRIM(ReasonCodeDescription)=TRIM('{2}')), AppliedAmount='0' WHERE ReceiptId={1}", strVoidStatusId, receiptId, reasonCodeId);
                //DbEngine.ExecuteNonQuery(strQuery);

                // Object Handling & Memory Disposal - For Memroy Optimization at 12/24/2016 - By Velmani
                ReasonCodeManager objReasonCodeManager = new ReasonCodeManager();
                objReasonCodeManager.ARPaymentManager_VoidReceipt(strVoidStatusId, reasonCodeType, reasonCodeId, customerId, receiptId, routeId);
                objReasonCodeManager.Dispose();
                objReasonCodeManager = null;


                //strQuery = string.Format("UPDATE BUSDTA.Customer_Ledger_R1 SET StatusID='" + strVoidStatusId + "' WHERE CustBillToId={0} AND " +
                //          " ARDocumentId=(SELECT AppliedDocumentID FROM BUSDTA.Receipt_Ledger WHERE ReceiptID='" + receiptId + "') ", customerId);
                //DbEngine.ExecuteNonQuery(strQuery);



                //strVoidStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VOID'");

                //strQuery = string.Format("UPDATE BUSDTA.Receipt_Header SET StatusId=" + strVoidStatusId + ", ReasonCodeId=(SELECT ReasonCodeId FROM BUSDTA.ReasonCodeMaster WHERE TRIM(ReasonCodeDescription)=TRIM('{0}') AND ReasonCodeType='" + reasonCodeType + "') WHERE CustBillToId={1} AND ReceiptId={2} AND RouteId={3}", reasonCodeId, customerId, receiptId, routeId);
                //DbEngine.ExecuteNonQuery(strQuery);

                //strQuery = string.Format("UPDATE BUSDTA.Customer_Ledger SET IsActive='0' WHERE CustBillToId={0} AND ReceiptId={1} AND RouteId={2}", customerId, receiptId, routeId);
                //DbEngine.ExecuteNonQuery(strQuery);

                ////ToDo - Update Invoice Status as Open 
                //strQuery = string.Format("UPDATE BUSDTA.Invoice_Header SET DeviceStatusId=1,InvoicePaymentType='Credit' WHERE InvoiceId IN (SELECT InvoiceId FROM BUSDTA.Customer_Ledger WHERE CustBillToId={0} AND ReceiptId={1} AND ISNULL(InvoiceId,0)<>0) AND RouteId={2}", customerId, receiptId, routeId);
                //DbEngine.ExecuteNonQuery(strQuery);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][VoidReceipt][ExceptionStackTrace = " + ex.StackTrace + "]");                
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:VoidReceipt]");
        }

        public void VoidReceiptNew(string receiptId, string reasonCodeId, string routeId, bool reasonCodeTypeFlag)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:VoidReceiptNew]");
            // String Handling - For Memroy Optimization at 12/26/2016 - By Velmani
            string strQuery = string.Empty;
            string strVoidStatusId = string.Empty;
            //string reasonCodeType = "";
            //if (reasonCodeTypeFlag)
            //{
            //    reasonCodeType = "Void Receipt";
            //}
            //else reasonCodeType = "Void Credit Memo";

            try
            {
                //strVoidStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VOID'");
                // strVoidStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(TTID,0) TTID FROM BUSDTA.M5001 WHERE RTRIM(TTKEY)='VoidCreditMemo'");
                strVoidStatusId = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetNewVoidReceipt(@TTKEY='VoidCreditMemo')");

                // Reason Code Update - New Field or Sub Field
                //strQuery = string.Format("UPDATE BUSDTA.Order_Header SET OrderStateId=" + strVoidStatusId + ", " +
                //            " VoidReasonCodeId=(SELECT TOP 1 ReasonCodeId FROM BUSDTA.ReasonCodeMaster WHERE TRIM(ReasonCodeDescription)=TRIM('{0}')) " +
                //            " WHERE CustShipToId={1} AND OrderID={2} AND OriginatingRouteID={3}", reasonCodeId, customerId, receiptId, routeId);
                //DbEngine.ExecuteNonQuery(strQuery);

                // Object Handling & Memory Disposal - Optimization (CA1820) for Memroy Optimization at 12/24/2016 - By Velmani
                OrderManager objOrderManger = new OrderManager();
                objOrderManger.ARPay_VoidReceiptNew(strVoidStatusId, reasonCodeId, customerId, receiptId, routeId);
                objOrderManger.Dispose();
                objOrderManger = null;

                // Customer Ledger R1 Table Void Status
                strQuery = string.Format("UPDATE BUSDTA.Customer_Ledger_R1 SET StatusID='" + strVoidStatusId + "' WHERE CustBillToId={0} AND " +
                          " ARDocumentId=(SELECT InvoiceID FROM BUSDTA.Invoice_Header WHERE OrderID='" + receiptId + "') ", customerId, receiptId);
                DbEngine.ExecuteNonQuery(strQuery);
                ResourceManager.QueueManager.QueueProcess("ARPayments", false);

                // Invoice Header Table Changes
                //strQuery = string.Format("UPDATE BUSDTA.Invoice_Header SET DeviceStatusId=1,InvoicePaymentType='Credit' " +
                //            " WHERE InvoiceId IN (SELECT InvoiceId FROM BUSDTA.Customer_Ledger_R1 WHERE CustBillToId={0} AND ARDocumentId={1} AND ISNULL(ARDocumentId,0)<>0)", customerId, receiptId);
                //DbEngine.ExecuteNonQuery(strQuery);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][VoidReceiptNew][ExceptionStackTrace = " + ex.StackTrace + "]");                
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:VoidReceiptNew]");
        }

        /// <summary>
        /// Updates activities of the customer 
        /// </summary>
        /// <param name="activity">Activity object</param>
        public void UpdateReceiptActivity(Activity activity)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:UpdateReceiptActivity]");
            // String Handling - For Memroy Optimization at 12/26/2016 - By Velmani
            string query = string.Empty;
            try
            {
                query = string.Format("UPDATE BUSDTA.M50012 SET TDDTLS='{0}', TDSTAT='{1}' WHERE TDID={2}", activity.ActivityDetails.Replace("'", "''"), activity.ActivityStatus, activity.ActivityID);
                DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][UpdateReceiptActivity][ExceptionStackTrace = " + ex.StackTrace + "]");                
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:UpdateReceiptActivity]");
        }

        #endregion

        // Memory Dispose Handling - For Memroy Optimization at 12/26/2016 - By Velmani
        #region IDispose Handling

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        #endregion // IDispose Handling

    }
}
