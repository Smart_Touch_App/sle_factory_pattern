﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.ObjectModel;
using System.Data;
using Models = SalesLogicExpress.Domain;

namespace SalesLogicExpress.Application.Managers
{
    /// <summary>
    /// Provides all the support for the extracting and handling Aging records 
    /// </summary>

    public class ARAgingManager : IDisposable
    {
        #region Variable and object declaration

        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.ARAgingManager");

        #endregion

        #region Properties

        #endregion

        #region Methods

        /// <summary>
        /// Provides aging details of the customer 
        /// </summary>
        /// <returns>Aging details</returns>
        public ObservableCollection<Models.ARAgingSummary> GetAgingDetails()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ARAgingManager][Start:GetAgingDetails]");
            DataSet objAgingData = null;
            //StringBuilder objQueryBuilder = new StringBuilder();

            try
            {
                //objQueryBuilder.Append("Select cl.CustBillToID CustomerId,cma.abalph CustomerName, trim(pn.pnptd) +' ('+ cm.AITRAR + ')' PaymentTerm,count(cl.InvoiceId) OpenInvoiceCount, ");
                //objQueryBuilder.Append("sum(cl.InvoiceGrossAmt) OpenInvoiceGrossAmt, ");
                //objQueryBuilder.Append("sum(isnull(cl.InvoiceOpenAmt,0)) - sum(isnull(cl.ConcessionAmt,0)) - ");
                //objQueryBuilder.Append("sum(isnull(cl.receiptappliedamt,0))  OpenInvoiceOpenAmt, case when DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) <= 30 then '30 Days' ");
                //objQueryBuilder.Append("when DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 30 and ");
                //objQueryBuilder.Append("DATEDIFF(day,ih.DeviceInvoiceDate,Getdate())<=60 then '60 Days' ");
                //objQueryBuilder.Append("when DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 60 and ");
                //objQueryBuilder.Append("DATEDIFF(day,ih.DeviceInvoiceDate,Getdate())<=90 then '90 Days' ");
                //objQueryBuilder.Append("when DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 90 then '90+ Days' end AS Age from busdta.Customer_Ledger  cl join busdta.F03012 cm on ");
                //objQueryBuilder.Append("cl.CustBillToId=cm.aian8 join busdta.F0101 cma on cm.aian8 = cma.ABAN8 join busdta.Invoice_Header ih on cl.InvoiceId=ih.InvoiceID AND cl.CustBillToId=ih.CustBillToId and cl.routeid=ih.routeid ");
                //objQueryBuilder.Append("join busdta.Order_Header oh on oh.OrderId=ih.OrderID AND oh.CustBillToId=ih.CustBillToId and cl.routeid=ih.routeid ");
                //objQueryBuilder.Append("join BUSDTA.f0014 pn on cm.AITRAR= pn.pnptc where cl.IsActive=1 and ih.ARStatusID NOT IN (select statustypeid from busdta.Status_Type ");
                //objQueryBuilder.Append("where statustypecd IN ('VOID','CLSD')) and ih.DeviceStatusID NOT IN (select statustypeid from busdta.Status_Type where statustypecd IN ('VOID','CLSD')) ");
                //objQueryBuilder.Append("and oh.OrderTypeId NOT IN (SELECT StatusTypeID FROM BUSDTA.Status_Type WHERE BUSDTA.Status_Type.StatusTypeCD='RETORD') ");
                //objQueryBuilder.Append("group by cl.CustBillToId,cma.abalph,cm.AITRAR,Age,pn.pnptd ");
                //objQueryBuilder.Append("HAVING OpenInvoiceOpenAmt>0 order by OpenInvoiceOpenAmt desc ");

                // Commented On - 09/18/2016
                //                string sql = @"Select cl.CustShipToID CustomerId,
                //                cma.abalph CustomerName, 
                //                 trim(pn.pnptd) +' ('+ cm.AITRAR + ')' PaymentTerm,
                //                count(cl.ARDocumentId) OpenInvoiceCount, 
                //                 sum(cl.GrossAmt) OpenInvoiceGrossAmt, sum(isnull(cl.OpenAmt,0))   
                //                OpenInvoiceOpenAmt, 
                //                case  when DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) <= 30 then '30 Days' 
                //                                when DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 30 and DATEDIFF(day,ih.DeviceInvoiceDate,Getdate())<=60 then '60 Days' 
                //                                when DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 60 and DATEDIFF(day,ih.DeviceInvoiceDate,Getdate())<=90 then '90 Days' 
                //                                when DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 90 then '90+ Days' end AS Age   
                //                from BUSDTA.Customer_Ledger_R1 cl 
                //                join busdta.F03012 cm on cl.CustShipToID = cm.aian8
                //                join busdta.F0101 cma on cl.CustShipToID = cma.ABAN8 
                //                join BUSDTA.f0014 pn on cm.AITRAR= pn.pnptc 
                //                join busdta.Invoice_Header ih on cl.ARDocumentId=ih.InvoiceID
                //                where ARDocumentType in ('RI')
                //                GROUP BY cl.CustShipToID,cma.abalph,pn.pnptd,cm.AITRAR,Age
                //                order by OpenInvoiceOpenAmt desc";

                string MyQuery = "CALL BUSDTA.SP_GetAgingDetails()";
                objAgingData = DbEngine.ExecuteDataSet(MyQuery, "AgingData");
                MyQuery = string.Empty;//Sathish Changed this code.Disposing the objects
                //objAgingData = DbEngine.ExecuteDataSet(objQueryBuilder.ToString(), "AgingData");
                Logger.Info("[SalesLogicExpress.Application.Managers][ARAgingManager][End:GetAgingDetails]");
                return objAgingData.GetEntityList<Models.ARAgingSummary>().ToObservableCollection();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARAgingManager][GetAgingDetails][ExceptionStackTrace = " + ex.Message + "]");
                throw;
            }

        }

        public Models.Customer GetCustomerByIdWithStopDate(string customerId)
        {
            Models.Customer customer = new Models.Customer();

            Logger.Info("[SalesLogicExpress.Application.Managers][ARAgingManager][Start:GetCustomerByIdWithStopDate]");
            try
            {
                string query = string.Empty;

                //                query =
                //                        @"SELECT isnull(rss.RPSTID,'') 'StopID', rss.RPSTDT as 'StopDate', AIZON,AISTOP as DeliveryCode,(SELECT DMDDCD from busdta.M56M0001 
                //                        where DMDDC = cs.AISTOP) as DeliveryCodeDesc,a.aban8 AS CustomerNo, a.abalph AS Name, a.abat1 AS Address_type,
                //                        (SELECT top 1 RPSTDT FROM BUSDTA.M56M0004 WHERE RPAN8 = a.aban8 AND RPSTTP <> 'Moved' AND RPSTDT >= cast(getdate() as date) order by 1) as NextStop,
                //                        (SELECT top 1 RPSTDT FROM BUSDTA.M56M0004 WHERE RPAN8 = a.aban8 AND RPSTTP <> 'Moved' AND RPSTDT < cast(getdate() as date) order by 1 desc) as LastStop, 
                //                        a.abac03 AS Route, a.abmcu AS Route_Branch , a.aban81 AS Bill_To , cs.AIHDAR as CreditHold,  b.abalph AS Bill_To_Name, b.abat1 AS Billto_Type,
                //                        BUSDTA.GetDefaultPhone(a.aban8) AS Phone , (rtrim(d.aladd1) + ' ' +  d.aladd2 +' '+ d.aladd3 + ' ' + d.aladd4) as Address , d. alcty1 as City, d.aladds as State, d. aladdz as Zip,AITRAR as TransMode,
                //                        AICO Company  ,cs.aitrar as 'Payment Code',f.PNPTD  as 'Payment Desc'
                //                        FROM busdta.f0101 AS a JOIN busdta.F03012 AS cs ON cs.aian8 =a.aban8 JOIN busdta.f0101 AS b ON a.aban81 =b.aban8  join busdta.F0116 as d on a.aban8 =d.alan8 
                //                        LEFT OUTER JOIN busdta.F0014 f ON cs.aitrar =f.pnptc
                //                        left JOIN BUSDTA.M56M0004 rss ON rss.RPAN8 = cS.AIAN8 AND stopdate='" + DateTime.Today.ToString("yyyy-MM-dd") + "'" +
                //                        "where AIZON>' ' and AISTOP>' ' and cs.AISTOP <> '7'  AND AIAN8='" + customerId + "'";
                query = "Call BUSDTA.SP_GetCustomerDetailForARAging(@CustomerId='" + customerId + "')";

                DataSet ds = DbEngine.ExecuteDataSet(query);
                query = string.Empty;//Sathish Changed this code.Disposing the objects

                if (ds.HasData())
                {
                    DataRow customerDataRow = ds.Tables[0].Rows[0];
                    customer.StopID = customerDataRow["StopID"].ToString().Trim();
                    customer.NextStop = (string.IsNullOrEmpty(customerDataRow["NextStop"].ToString()) ? "" : Convert.ToDateTime(customerDataRow["NextStop"]).ToString("MM/dd/yyyy"));
                    customer.PreviousStop = (string.IsNullOrEmpty(customerDataRow["LastStop"].ToString()) ? "" : Convert.ToDateTime(customerDataRow["LastStop"]).ToString("MM/dd/yyyy"));
                    customer.StopDate = string.IsNullOrEmpty(customerDataRow["StopDate"].ToString()) ? new DateTime() : Convert.ToDateTime(customerDataRow["StopDate"]);
                    customer.IsTodaysStop = (DateTime.Compare(customer.StopDate.Value, DateTime.Today.Date) == 0) ? true : false;
                    customer.CustomerNo = customerDataRow["CustomerNo"].ToString().Trim();
                    customer.Phone = customerDataRow["Phone"].ToString().Trim();
                    customer.Address = customerDataRow["Address"].ToString().Trim();
                    customer.City = customerDataRow["City"].ToString().Trim();
                    customer.Name = customerDataRow["Name"].ToString().Trim();
                    customer.Zip = customerDataRow["Zip"].ToString().Trim();
                    customer.State = customerDataRow["State"].ToString().Trim();
                    customer.Shop = customerDataRow["Bill_To_Name"].ToString().Trim();
                    customer.DeliveryCode = customerDataRow["DeliveryCode"].ToString().Trim();
                    customer.DeliveryCodeDescription = customerDataRow["DeliveryCodeDesc"].ToString().Trim();
                    customer.Route = customerDataRow["Route"].ToString().Trim();
                    customer.RouteBranch = customerDataRow["Route_Branch"].ToString().Trim();
                    customer.BillType = customerDataRow["BillTo_Type"].ToString().Trim();
                    customer.IsCustomerOnAccount = !string.IsNullOrEmpty(customerDataRow["TransMode"].ToString()) && customerDataRow["TransMode"].ToString().Trim() == "CSH" ? false : true;
                    customer.Company = customerDataRow["Company"].ToString().Trim();
                    customer.PaymentModeForList = customerDataRow["Payment Code"].ToString().Trim();
                    customer.PaymentModeDescriptionForList = customerDataRow["Payment Desc"].ToString().Trim();
                    customer.PaymentModeDescriptionForList = "(" + customer.PaymentModeForList + ") " + customer.PaymentModeDescriptionForList + "";
                }

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARAgingManager][GetCustomerByIdWithStopDate][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ARAgingManager][End:GetCustomerByIdWithStopDate]");
            return customer;
        }

        //        public decimal GetUnappliedAmountForRoute()
        //        {
        //            Logger.Info("[SalesLogicExpress.Application.Managers][ARAgingManager][Start:GetUnappliedAmountForRoute]");
        //            decimal unppliedAmount = 0;

        //            try
        //            {
        //                string strAmount = DbEngine.ExecuteScalar(@"
        //                                    select round(sum(isnull(cl.receiptunappliedamt,0)) - sum(isnull(cl.receiptappliedamt,0)),2)  UnappliedAmount
        //                                    from BUSDTA.Customer_Ledger cl join busdta.Receipt_Header RH on cl.Receiptid=RH.Receiptid AND cl.RouteId=RH.RouteId
        //                                    where cl.routeId=" + ViewModels.CommonNavInfo.RouteID.ToString() + " and cl.isactive='1' ");
        //                if (!string.IsNullOrEmpty(strAmount))
        //                {
        //                    unppliedAmount = Convert.ToDecimal(strAmount);
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                Logger.Error("[SalesLogicExpress.Application.Managers][ARAgingManager][GetUnappliedAmountForRoute][ExceptionStackTrace = " + ex.Message + "]");
        //                throw;
        //            }

        //            Logger.Info("[SalesLogicExpress.Application.Managers][ARAgingManager][End:GetUnappliedAmountForRoute]");

        //            return unppliedAmount;
        //        }
        // Code written by Marimuthu for Performance change
        // Dispose implementation for Performance optimization
        // Date : 11/03/2016

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        #endregion
    }
}
