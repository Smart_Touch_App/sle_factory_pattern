﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Domain;
using log4net;

namespace SalesLogicExpress.Application.Managers
{
    public class MoneyOrderManager : IDisposable
    {
        private readonly ILog logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.MoneyOrderManager");
        private readonly ReasonCodeManager objReasonCodeManager = new ReasonCodeManager();

        private readonly System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        //************************************************************************************************
        // Comment: Dispose Implementation for Peformance Improvement
        // Created: Nov 03, 2016
        // Author: TechUnison (Velmani Karnan)
        //*************************************************************************************************
        private bool disposed = false;

        public ObservableCollection<MoneyOrder> GetMoneyOrderList()
        {
            var moneyOrderList = new ObservableCollection<MoneyOrder>();
            this.logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][Start:GetMoneyOrderList]");

            try
            {
                //string query = "select MO.MoneyOrderId, MO.RouteId, MO.MoneyOrderNumber, MO.MoneyOrderAmount, MO.MoneyOrderFeeAmount, MO.StatusId, MO.MoneyOrderDatetime, MO.VoidReasonId, MO.RouteSettlementId, ";
                //query = query + " MO.UpdatedDatetime, ST.StatusTypeCD as StatusCode, ST.StatusTypeDESC as Status from BUSDTA.MoneyOrderDetails MO ";
                //query = query + " join BUSDTA.Status_Type ST on ST.StatusTypeID = MO.StatusId ";
                //query = query + " where MO.RouteId = '" + CommonNavInfo.RouteID + "'";
                //query = query + " ORDER BY MO.UpdatedDatetime DESC";
                //DataSet result = DbEngine.ExecuteDataSet(query);
                DataSet result = DbEngine.ExecuteDataSet(string.Format("call busdta.SP_GetSettlementMoneyOrderList('{0}')", CommonNavInfo.RouteID));
                if (result.HasData())
                {
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        DataRow row = result.Tables[0].Rows[i];
                        var order = new MoneyOrder();
                        order.MoneyOrderID = row["MoneyOrderId"].ToString().Trim();
                        order.RouteID = row["RouteId"].ToString().Trim();
                        order.MoneyOrderNumber = row["MoneyOrderNumber"].ToString().Trim();
                        order.MoneyOrderAmount = Convert.ToDecimal(row["MoneyOrderAmount"]).ToString("F2").Trim();
                        order.FeeAmount = row["MoneyOrderFeeAmount"].ToString() != null ? Convert.ToDecimal(row["MoneyOrderFeeAmount"]).ToString("F2").Trim() : "0.00";
                        order.TotalAmount = (Convert.ToDecimal(order.MoneyOrderAmount) + Convert.ToDecimal(order.FeeAmount)).ToString("F2");
                        order.StatusID = row["StatusId"].ToString().Trim();
                        order.StatusCode = row["StatusCode"].ToString().Trim();
                        order.Status = row["Status"].ToString().ToLower().ToUpper().Trim();
                        order.MoneyOrderDate = Convert.ToDateTime(row["UpdatedDatetime"]);
                        order.VoidReasonID = row["VoidReasonId"].ToString() != null ? row["VoidReasonId"].ToString().Trim() : string.Empty;
                        order.RouteSettlementId = row["RouteSettlementId"].ToString() != null ? row["RouteSettlementId"].ToString().Trim() : string.Empty;
                        moneyOrderList.Add(order);

                        order = null;
                    }
                }
            }
            catch (Exception ex)
            {
                this.logger.Error(string.Format("[SalesLogicExpress.Application.Managers][ExpensesManager][GetMoneyOrderList][ExceptionStackTrace = {0}][Exception Message={1}]", ex.StackTrace, ex.Message));                
            }
            this.logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][End:GetMoneyOrderList]");
            return moneyOrderList;
        }

        public ObservableCollection<ReasonCode> GetVoidReasonCode()
        {
            var reasonCodeList = new ObservableCollection<ReasonCode>();
            this.logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:GetVoidReasonCode]");

            try
            {
                //DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Void MoneyOrder'").Tables[0];
                DataTable dt = this.objReasonCodeManager.MoneyOrderManager_GetVoidReasonCode();
                var reasonCode = new ReasonCode();

                foreach (DataRow dr in dt.Rows)
                {
                    reasonCode = new ReasonCode();
                    reasonCode.Id = Convert.ToInt32(dr["Id"].ToString().Trim());
                    reasonCode.Code = dr["ReasonCodeDescription"].ToString().Trim();
                    reasonCodeList.Add(reasonCode);

                    reasonCode = null;
                }

                dt.Dispose();
                dt = null;
            }
            catch (Exception ex)
            {
                this.logger.Error(string.Format("[SalesLogicExpress.Application.Managers][ExpensesManager][GetVoidReasonCode {0}]", ex.StackTrace));
            }
            this.logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][End:GetVoidReasonCode]");

            return reasonCodeList;
        }

        public MoneyOrder AddMoneyOrder(MoneyOrder order)
        {
            this.logger.Info("[SalesLogicExpress.Application.Managers][MoneyOrderManager][Start:AddMoneyOrder]");
            //Expenses exp = null;
            try
            {
                string query = "insert into BUSDTA.MoneyOrderDetails(MoneyOrderId, RouteId, MoneyOrderNumber, MoneyOrderAmount, MoneyOrderFeeAmount, StatusId, MoneyOrderDatetime, VoidReasonId, RouteSettlementId, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)";
                query = string.Format("{0} VALUES((select isnull(max(MoneyOrderId),0)+1 from BUSDTA.MoneyOrderDetails), '{1}', '{2}', '{3}', ", query, CommonNavInfo.RouteID, string.IsNullOrEmpty(order.MoneyOrderNumber) ? "null" : order.MoneyOrderNumber, string.IsNullOrEmpty(order.MoneyOrderAmount) ? "null" : order.MoneyOrderAmount);
                query = string.Format("{0} '{1}', (select StatusTypeID from busdta.Status_Type where StatusTypeCD = 'USTLD'), ", query, string.IsNullOrEmpty(order.FeeAmount) ? "null" : order.FeeAmount);
                query = string.Format("{0} getdate(), {1}, null, ", query, string.IsNullOrEmpty(order.VoidReasonID) ? "null" : order.VoidReasonID);
                query = string.Format("{0}'{1}', getdate(), '{2}', getdate())", query, UserManager.UserId, UserManager.UserId);

                try
                {
                    int result = DbEngine.ExecuteNonQuery(query);

                    if (result == 1)
                    {
                        //query = "select top 1 MoneyOrderId, StatusId from BUSDTA.MoneyOrderDetails order by UpdatedDatetime DESC";
                        query = "CALL BUSDTA.SP_GetMoneyOrderIDwithStatus()";
                        DataSet obj = DbEngine.ExecuteDataSet(query);
                        if (obj.HasData())
                        {
                            DataRow dr = obj.Tables[0].Rows[0];
                            //order = new MoneyOrder();
                            order.StatusID = dr["StatusId"].ToString();
                            order.MoneyOrderID = dr["MoneyOrderId"].ToString();
                        }

                        obj.Dispose();
                        obj = null;
                    }
                }
                catch (Exception ex)
                {
                    this.logger.Error(string.Format("[SalesLogicExpress.Application.Managers][MoneyOrderManager][AddMoneyOrder][ExceptionStackTrace = {0}][Exception Message={1}]", ex.StackTrace, ex.Message));
                    order = null;
                    return order;
                }
            }
            catch (Exception ex)
            {
                this.logger.Error(string.Format("[SalesLogicExpress.Application.Managers][MoneyOrderManager][AddMoneyOrder][ExceptionStackTrace = {0}][Exception Message={1}]", ex.StackTrace, ex.Message));                
            }
            ResourceManager.QueueManager.QueueProcess("EndOfDay", false);
            this.logger.Info("[SalesLogicExpress.Application.Managers][MoneyOrderManager][End:AddMoneyOrder]");

            return order;
        }

        public int UpdateMoneyOrder(MoneyOrder order, bool flag)
        {
            this.logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][Start:UpdateExpenses]");
            int statusID = -1;
            try
            {
                string query = string.Empty;
                if (flag)
                {
                    query = string.Format("update BUSDTA.MoneyOrderDetails set MoneyOrderNumber = '{0}', MoneyOrderAmount = '{1}', MoneyOrderFeeAmount='{2}', ", order.MoneyOrderNumber, order.MoneyOrderAmount, order.FeeAmount);
                    query = string.Format("{0} UpdatedDatetime = getdate() where RouteID = '{1}' and MoneyOrderId = '{2}'", query, order.RouteID, order.MoneyOrderID);
                }
                else
                {
                    query = string.Format("update BUSDTA.MoneyOrderDetails set StatusId = (select StatusTypeID from busdta.Status_Type where StatusTypeCD = 'VOID'), VoidReasonId = '{0}', ", order.VoidReasonID);
                    query = string.Format("{0} UpdatedDatetime = getdate() where RouteID = '{1}' and MoneyOrderId = '{2}'", query, order.RouteID, order.MoneyOrderID);
                }

                int result = DbEngine.ExecuteNonQuery(query);

                if (result == 1)
                {
                    //query = "select StatusId from BUSDTA.MoneyOrderDetails where RouteID = '" + order.RouteID + "' and MoneyOrderId = '" + order.MoneyOrderID + "'";
                    query = string.Format("CALL BUSDTA.SP_GetMoneyOrderStatus(@RouteID = '{0}', @MoneyOrderID = '{1}' )", order.RouteID, order.MoneyOrderID);
                    statusID = Convert.ToInt32(DbEngine.ExecuteScalar(query));
                }

                ResourceManager.QueueManager.QueueProcess("EndOfDay", false);
            }
            catch (Exception ex)
            {
                this.logger.Error(string.Format("[SalesLogicExpress.Application.Managers][ExpensesManager][UpdateExpenses][ExceptionStackTrace = {0}][Exception Message={1}]", ex.StackTrace, ex.Message));                
            }
            this.logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][End:UpdateExpenses]");
            return statusID;
        }

        public Expenses UpdateExpense(MoneyOrder order, bool flag)
        {
            var expense = new Expenses();
            this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][Start:UpdateExpense]");
            try
            {
                int result = -1;
                if (flag)
                {
                    result = DbEngine.ExecuteNonQuery(string.Format("Update BUSDTA.ExpenseDetails set ExpenseAmount = '{0}', ExpensesExplanation = 'Money Order Charges for {1}',  UpdatedDatetime = getdate() where RouteID = '{2}' and TransactionId = '{3}'", order.FeeAmount, order.MoneyOrderNumber, order.RouteID, order.MoneyOrderID));
                }
                else
                {
                    result = DbEngine.ExecuteNonQuery(string.Format("Update BUSDTA.ExpenseDetails set StatusId = (select StatusTypeID from busdta.Status_Type where StatusTypeCD = 'VOID'), VoidReasonId = '{0}', UpdatedDatetime = getdate() where RouteID = '{1}' and TransactionId = '{2}'", order.VoidReasonID, order.RouteID, order.MoneyOrderID));
                }

                if (result == 1)
                {
                    DataSet exp = DbEngine.ExecuteDataSet(string.Format("select ED.*, CT.CategoryTypeDESC as Category, ST.StatusTypeCD as StatusCode, ST.StatusTypeDESC as Status  from BUSDTA.ExpenseDetails ED join BUSDTA.Category_Type CT on ED.CategoryId = CT.CategoryTypeID  join BUSDTA.Status_Type ST on ST.StatusTypeID = ED.StatusId where ED.TransactionId = '{0}'", order.MoneyOrderID));

                    if (exp.HasData())
                    {
                        DataRow dr = exp.Tables[0].Rows[0];
                        expense.ExpenseID = dr["ExpenseId"].ToString().Trim();
                        expense.RouteID = dr["RouteId"].ToString().Trim();
                        expense.CategotyID = dr["CategoryId"].ToString().Trim();
                        // expense.CategoryCode = string.IsNullOrEmpty(dr["CategoryCode"].ToString()) ? string.Empty : dr["CategoryCode"].ToString().Trim();
                        /// if (expense.CategoryCode.ToLower().ToUpper() == "MOCHR")
                        /// {
                        //     expense.IsMoneyOrderCharge = true;
                        // }
                        expense.ExpenseCategory = string.IsNullOrEmpty(dr["Category"].ToString()) ? string.Empty : dr["Category"].ToString().Trim();
                        expense.StatusID = dr["StatusId"].ToString().Trim();
                        expense.StatusCode = dr["StatusCode"].ToString().Trim();
                        expense.Status = dr["Status"].ToString().Trim().ToLower().ToUpper();
                        expense.VoidReasonID = string.IsNullOrEmpty(dr["VoidReasonId"].ToString()) ? string.Empty : dr["VoidReasonId"].ToString().Trim();
                        expense.ExpenseAmount = (Convert.ToDecimal(dr["ExpenseAmount"])).ToString("F2").Trim();
                        expense.ExpenseDate = Convert.ToDateTime(dr["UpdatedDatetime"]);
                        expense.Explanation = dr["ExpensesExplanation"].ToString().Trim();
                        expense.TransactionId = string.IsNullOrEmpty(dr["TransactionId"].ToString()) ? string.Empty : dr["TransactionId"].ToString();
                        expense.RouteSettlementId = string.IsNullOrEmpty(dr["RouteSettlementId"].ToString()) ? string.Empty : dr["RouteSettlementId"].ToString();
                    }

                    exp.Dispose();
                    exp = null;
                }
                ResourceManager.QueueManager.QueueProcess("EndOfDay", false);
            }
            catch (System.Exception ex)
            {
                this.logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][UpdateExpense][ExceptionStackTrace = {0}][Exception Message={1}]", ex.StackTrace, ex.Message));                
            }
            this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][End:UpdateExpense]");
            return expense;
        }

        public bool CheckMONumber(string moneyOrderNumber, bool isEdit)
        {
            bool flag = true;
            this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][Start:CheckMONumber]");
            try
            {
                //string query = "SELECT mo.MoneyOrderNumber, st.StatusTypeCD FROM BUSDTA.MoneyOrderDetails mo join BUSDTA.Status_Type st on mo.StatusId = st.StatusTypeID where MoneyOrderNumber = '" + MoneyOrderNumber + "'";
                //DataSet ds = DbEngine.ExecuteDataSet(query);
                DataSet ds = DbEngine.ExecuteDataSet(string.Format("call busdta.SP_GetSettlementCheckMONumber ('{0}')", moneyOrderNumber));
                if (ds.HasData())
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        DataRow dr = ds.Tables[0].Rows[i];
                        if (!isEdit)
                        {
                            if ((dr["StatusTypeCD"].ToString() != "VOID") && (dr["StatusTypeCD"].ToString() != "VDSTLD") && (dr["StatusTypeCD"].ToString() != "USTLD"))
                            {
                                flag = false;
                                break;
                            }
                        }
                        else if ((dr["StatusTypeCD"].ToString() != "VOID") && (dr["StatusTypeCD"].ToString() != "VDSTLD"))
                        {
                            flag = false;
                            break;
                        }
                    }
                }

                ds.Dispose();
                ds = null;
            }
            catch (Exception ex)
            {
                this.logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][CheckMONumber][ExceptionStackTrace = {0}][Exception Message={1}]", ex.StackTrace, ex.Message));
            }
            this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][End:CheckMONumber]");

            return flag;
        }
        
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            if (disposing)
            {
                this.handle.Dispose();

                if (objReasonCodeManager != null)
                    objReasonCodeManager.Dispose();
            }
            this.disposed = true;
        }
    }
}