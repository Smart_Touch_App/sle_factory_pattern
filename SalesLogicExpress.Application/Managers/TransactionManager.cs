﻿using System;
using System.Collections.Generic;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using log4net;
using System.Data;
using System.Linq;
using System.Diagnostics;
using System.Collections.ObjectModel;
namespace SalesLogicExpress.Application.Managers
{
    public class TransactionManager : IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.TransactionManager");
        public Dictionary<string, ActivityTypeInfo> ActivityKeys { get; set; }
        public TransactionManager()
        {
            ActivityKeys = new Dictionary<string, ActivityTypeInfo>();
            LoadActivityKeys();
        }
        void LoadActivityKeys()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][Start:LoadActivityKeys]");
            try
            {
                //string query = "select  ntx.TNTYP as 'TransactionType',ntx.TNKEY as ActivityKey ,'NonTransactionActivity' as [Type], ntx.TNILDGRD as IsLedgered, ntx.TNACTN as 'Description', 0 as 'IsStart', 0 as 'IsEnd'    from busdta.M5002 ntx ";
                //query = query + " union select  tx.TTTYP as 'TransactionType', tx.TTKEY as ActivityKey ,'TransactionActivity' as [Type],1 as 'IsLedgered' , tx.TTACTN as 'Description', tx.TTISTRT as IsStart, tx.TTIEND as IsEnd    from busdta.M5001 tx";
                string query = ("CALL BUSDTA.SP_GetLoadActivityKeys()");
                DataSet result = DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    foreach (DataRow item in result.Tables[0].Rows)
                    {
                        string ttkey = item["ActivityKey"].ToString().Trim();

                        if (!ActivityKeys.ContainsKey(ttkey))
                        {
                            ActivityKeys.Add(item["ActivityKey"].ToString().Trim(), new ActivityTypeInfo
                            {
                                ActivityKey = item["ActivityKey"].ToString().Trim().ParseEnum<ActivityKey>(ActivityKey.None),
                                IsLedgered = item["IsLedgered"].ToString().Trim().Length > 0 ? Convert.ToBoolean(Convert.ToInt32(item["IsLedgered"].ToString().Trim())) : false,
                                Description = item["Description"].ToString().Trim(),
                                TransactionType = item["TransactionType"].ToString().Trim(),
                                ActivityType = item["Type"].ToString().Trim().Equals("NonTransactionActivity") ? ActivityType.NonTransaction : ActivityType.Transaction,
                                IsStart = item["IsStart"].ToString().Trim().Length > 0 ? Convert.ToBoolean(Convert.ToInt32(item["IsStart"].ToString().Trim())) : false,
                                IsEnd = item["IsEnd"].ToString().Trim().Length > 0 ? Convert.ToBoolean(Convert.ToInt32(item["IsEnd"].ToString().Trim())) : false,
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TransactionManager][LoadActivityKeys][ExceptionStackTrace = " + ex.InnerException.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][End:LoadActivityKeys]");
        }

        public void Initialize()
        {
        }

        public Activity LogActivity(Activity Activity)
        {
            //Stopwatch LogActivityTimer = Stopwatch.StartNew();

            Activity returnActivity = null;
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][Start:LogActivity][Activity.ActivityType=" + Activity.ActivityType + "]");
                Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][Start:LogActivity][Activity.ActivityEnd=" + Activity.ActivityEnd + "]");
                //Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][Start:LogActivity][Activity=" + Activity.SerializeToJson() + "]");
                string query = string.Empty;
                string activityID = string.Empty;
                //Dictionary<string, object> parameters = new Dictionary<string, object>();//Remove Unused Local variable By Vignesh.S

                if (ActivityKeys.Count == 0) //It Should never be 0, check sync operation if it is zero - Arvind
                    return returnActivity;

                ActivityTypeInfo activityInfo = ActivityKeys[Activity.ActivityType.ToString().Trim()];
                //Activity.ActivityEnd = DateTime.Now;
                if (activityInfo.ActivityType == ActivityType.Transaction)
                {
                    bool addNewActivity = true;
                    if (activityInfo.IsStart && activityInfo.IsEnd && !string.IsNullOrEmpty(Activity.StopInstanceID))
                    {
                        //query = string.Format("select count(*) from busdta.M50012 where TDTYP='{0}' and TDSTID='{1}' and TDAN8='{2}' and TDTYP='PreOrder'", Activity.ActivityType, Activity.StopInstanceID, Activity.CustomerID);
                        query = string.Format("CALL BUSDTA.SP_GetLogActivity(@ActivityType= '{0}' ,@StopInstanceID ='{1}' , @CustomerID ='{2}', @PreOrder='PreOrder')", Activity.ActivityType, Activity.StopInstanceID, Activity.CustomerID);
                        string count = DbEngine.ExecuteScalar(query).ToString();
                        if (count.ToString() != "0")
                        {
                            query = "Update busdta.M50012 set TDENDTM=" + Activity.ActivityEnd.GetFormattedDateTimeForDb() + " " +
                                     " ,TDSTAT = '" + activityInfo.ActivityKey.ToString() + "', TDDTLS=?,TDCLASS='" + Activity.ActivityDetailClass + "',TDREFHDRID='" + Activity.ActivityFlowID + "', TDUPBY='" + Environment.UserName + "', TDUPDT=getdate() " +
                                     " where TDSTID='" + Activity.StopInstanceID + "'  AND TDTYP!='NOACTIVITY'";
                            // used SACommand for inserting blob, inserting blobs needs to done using parameters
                            Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(query);
                            Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();
                            parm.SADbType = Sap.Data.SQLAnywhere.SADbType.LongNVarchar;
                            command.Parameters.Add(parm);
                            command.Parameters[0].Value = Activity.ActivityDetails;
                            new DB_CRUD().InsertUpdateData(command);
                            addNewActivity = false;
                        }
                    }
                    if (addNewActivity && activityInfo.IsStart || (activityInfo.IsEnd && Activity.ActivityHeaderID != null))
                    {
                        activityID = new Managers.NumberManager().GetNextNumberForEntity(SalesLogicExpress.Application.ViewModels.CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.TRANSACTID);

                        //query = "select GET_IDENTITY('busdta.M50012')";
                        //activityID = DbEngine.ExecuteScalar(query).ToString();
                        if (!string.IsNullOrEmpty(activityID))
                        {
                            query = "INSERT INTO busdta.M50012(TDID,TDROUT,TDTYP,TDCLASS,TDDTLS,TDSTRTTM,TDENDTM,TDSTID,TDAN8,TDSTTLID,TDPNTID,TDSTAT,TDREFHDRID,TDCRBY,TDCRDT,TDUPBY,TDUPDT)VALUES (";
                            query = query + activityID + ",'";
                            query = query + SalesLogicExpress.Application.ViewModelPayload.PayloadManager.ApplicationPayload.Route + "','";
                            query = query + (!string.IsNullOrEmpty(Activity.ActivityHeaderID) ? Activity.ActivityType : activityInfo.TransactionType.ToString()) + "','";
                            //query = query + activityInfo.TransactionType.ToString() + "','"; //Stores the actvity entity type
                            query = query + Activity.ActivityDetailClass + "',";
                            query = query + "?,";
                            //query = query + Activity.ActivityDetails + "',";
                            query = query + Activity.ActivityStart.GetFormattedDateTimeForDb() + ",";
                            query = query + Activity.ActivityEnd.GetFormattedDateTimeForDb() + ",'";
                            query = query + Activity.StopInstanceID + "','";
                            query = query + Activity.CustomerID + "','";
                            query = query + Activity.SettlementID + "','";
                            query = query + Activity.ActivityHeaderID + "'";
                            query = query + ",''";
                            query = query + ",'" + Activity.ActivityFlowID + "'";
                            query = query + ", '" + Environment.UserName + "', getdate(), '" + Environment.UserName + "', getdate()";
                            query = query + ");";

                            // used SACommand for inserting blob, inserting blobs needs to done using parameters
                            Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(query);
                            Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();
                            parm.SADbType = Sap.Data.SQLAnywhere.SADbType.LongNVarchar;
                            command.Parameters.Add(parm);
                            command.Parameters[0].Value = Activity.ActivityDetails;
                            new DB_CRUD().InsertUpdateData(command);
                            //DbEngine.ExecuteNonQuery(query);

                            //query = "select TDID as ActivityID, 1 as IsTxActivity,TDID as ActivityHeaderID, TDROUT AS RouteID,TDTYP as ActivityType,TDCLASS as ActivityDetailClass,TDDTLS as ActivityDetails,TDSTRTTM as ActivityStart,TDENDTM as ActivityEnd,TDSTID as StopInstanceID, TDAN8 as CustomerID,TDSTTLID as SettlementID,TDPNTID as ParentID,TDSTAT as ActivityStatus from busdta.M50012 where TDID='" + activityID + "'";
                            query = ("CALL BUSDTA.SP_GetActivityID( @activityID='" + activityID + "')");
                            DataSet ds = DbEngine.ExecuteDataSet(query);

                            if (ds.HasData())
                            {
                                returnActivity = ds.Tables[0].Rows[0].GetEntity<Activity>();
                            }
                            Activity.ActivityID = activityID;
                            if (activityInfo.IsEnd)
                            {
                                query = "Update busdta.M50012 set TDENDTM=" + Activity.ActivityEnd.GetFormattedDateTimeForDb() + ",TDSTAT = '" + activityInfo.ActivityKey.ToString() + "', TDUPBY='" + Environment.UserName + "', TDUPDT=getdate()  where TDID='" + activityID + "'";
                                // used SACommand for inserting blob, inserting blobs needs to done using parameters
                                Sap.Data.SQLAnywhere.SACommand cmd = new Sap.Data.SQLAnywhere.SACommand(query);
                                Sap.Data.SQLAnywhere.SAParameter param = new Sap.Data.SQLAnywhere.SAParameter();
                                param.SADbType = Sap.Data.SQLAnywhere.SADbType.LongNVarchar;
                                cmd.Parameters.Add(param);
                                cmd.Parameters[0].Value = Activity.ActivityDetails;
                                new DB_CRUD().InsertUpdateData(cmd);
                            }
                        }
                    }
                    if (activityInfo.IsEnd && !string.IsNullOrEmpty(Activity.ActivityHeaderID))
                    {
                        query = "Update busdta.M50012 set TDENDTM=" + Activity.ActivityEnd.GetFormattedDateTimeForDb() + ",TDSTAT = '" + activityInfo.ActivityKey.ToString() + "', TDDTLS=?,TDCLASS='" + Activity.ActivityDetailClass + "', TDUPBY='" + Environment.UserName + "', TDUPDT=getdate()  where TDID='" + Activity.ActivityHeaderID + "'";
                        // used SACommand for inserting blob, inserting blobs needs to done using parameters
                        Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(query);
                        Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();
                        parm.SADbType = Sap.Data.SQLAnywhere.SADbType.LongNVarchar;
                        command.Parameters.Add(parm);
                        command.Parameters[0].Value = Activity.ActivityDetails;
                        new DB_CRUD().InsertUpdateData(command);
                    }
                    else if (!string.IsNullOrEmpty(Activity.ActivityHeaderID))
                    {
                        query = "Update busdta.M50012 set  TDENDTM=" + Activity.ActivityEnd.GetFormattedDateTimeForDb() + ",  TDSTAT = '" + activityInfo.ActivityKey.ToString() + "', TDDTLS=?,TDCLASS='" + Activity.ActivityDetailClass + "', TDUPBY='" + Environment.UserName + "', TDUPDT=getdate() where TDID='" + Activity.ActivityHeaderID + "'";
                        // used SACommand for inserting blob, inserting blobs needs to done using parameters
                        Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(query);
                        Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();
                        parm.SADbType = Sap.Data.SQLAnywhere.SADbType.LongNVarchar;
                        command.Parameters.Add(parm);
                        command.Parameters[0].Value = Activity.ActivityDetails;
                        new DB_CRUD().InsertUpdateData(command);
                    }


                }
                // Get the ActivityID that will be assigned to this entry, Will be used as HeaderID
                //query = "select GET_IDENTITY('busdta.ActivityLedger')";
                //activityID = DbEngine.ExecuteScalar(query).ToString();

                // Insert into TransactionActivityDetail

                string NxtNumber1 = new Managers.NumberManager().GetNextNumberForEntity(SalesLogicExpress.Application.ViewModels.CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.SYSTEMACTID);
                if (!string.IsNullOrEmpty(NxtNumber1))
                {
                    query = "INSERT INTO busdta.M5003(ALID,ALHDID,ALTMSTMP,ALDSC,ALTYP,ALITX,ALROUTE)VALUES ('";
                    query = query + NxtNumber1 + "','";
                    query = query + Activity.ActivityHeaderID + "',";
                    query = query + Activity.ActivityStart.GetFormattedDateTimeForDb() + ",'";
                    query = query + Activity.ActivityDescription + "','";
                    query = query + Activity.ActivityType + "',";
                    query = query + Convert.ToInt32(Activity.IsTxActivity) + ",'";
                    query = query + SalesLogicExpress.Application.ViewModelPayload.PayloadManager.ApplicationPayload.Route + "',";
                    query = query + ");";
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TransactionManager][LogActivity][Activity=" + Activity.SerializeToJson() + "][ExceptionStackTrace = " + ex.InnerException.Message + "]");
                return returnActivity;
            }

            ResourceManager.QueueManager.QueueProcess("Logging", false);

            //Removed by Dinesh - Action Based sync moved to Daily stops
            //EventArgs e = new EventArgs();
            //OnTransactionLogged(e);


            //Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][End:LogActivity][Activity=" + Activity.SerializeToJson() + "]");
            //LogActivityTimer.Stop();
            //if (Activity.ActivityType == ActivityKey.PickItem.ToString())
            //{
            //    Logger.Error("TransactionManager LogActivity Time taken :" + LogActivityTimer.Elapsed.TotalMilliseconds + "ms");
            //}
            return returnActivity;
        }


        //public void SyncActionBasedManually(string QueueName)
        //{
        //    Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][Start:SyncActionBasedManually]");
        //    EventArgs e = new EventArgs();
        //    OnTransactionLogged(e);
        //    Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][End:SyncActionBasedManually]");
        //}

        public void AddNonTxActivity(Activity Activity)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][Start:AddNonTxActivity]");
            try
            {
                string NxtNumber1 = new Managers.NumberManager().GetNextNumberForEntity(SalesLogicExpress.Application.ViewModels.CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.SYSTEMACTID);
                if (!string.IsNullOrEmpty(NxtNumber1))
                {
                    string query = "INSERT INTO busdta.M5003(ALID,ALHDID,ALTMSTMP,ALDSC,ALTYP,ALITX,ALROUTE)VALUES ('";
                    query = query + NxtNumber1 + "','";
                    query = query + Activity.ActivityHeaderID + "',";
                    query = query + Activity.ActivityStart.GetFormattedDateTimeForDb() + ",'";
                    query = query + Activity.ActivityDescription + "','";
                    query = query + Activity.ActivityType + "',";
                    query = query + Convert.ToInt32(Activity.IsTxActivity) + ",'";
                    query = query + SalesLogicExpress.Application.ViewModelPayload.PayloadManager.ApplicationPayload.Route + "',";
                    query = query + ");";
                    DbEngine.ExecuteNonQuery(query);

                    //Removed by DInesh - 10/26/2016
                    //Non Transaction Log it hitting lot of times so removed
                    //ResourceManager.QueueManager.QueueProcess("Logging", false);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TransactionManager][LogActivity][Activity=" + Activity.SerializeToJson() + "][ExceptionStackTrace = " + ex.InnerException.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][End:AddNonTxActivity]");
        }
        public void ResetActivity(string transactionID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][Start:ResetActivity][transactionID=" + transactionID + "]");
            try
            {
                string query = string.Format("update busdta.M50012 set TDENDTM=getdate() WHERE TDID='{0}'", transactionID);
                DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TransactionManager][ResetActivity][ExceptionStackTrace = " + ex.Message == null ? ex.InnerException.Message : ex.Message + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][End:ResetActivity][transactionID=" + transactionID + "]");
        }
        //Issue ID - 102 -1)Create Sales order for 2EA items and Pick the items in pick screen click Deliver to Customer navigate to deliver screen void the Sales order. 2)Voided Sales Order Amount Affect in Open Invoice in Payment for Net 30Days Customer Fixed by Zakir
        public static void VoidPayment(string tdid, string reasonCodeId, string customerId, string routeId, string orderId)
        {
            TransactionManager tran = new TransactionManager();
            tran.Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][Start:ResetActivity]");
            try
            {
                string query = "  update BUSDTA.M50012 set tdstat ='PaymentVoid' where tdtyp='payment' and tdid=  '" + tdid + "' ";
                DbEngine.ExecuteNonQuery(query);

                // string receiptId = DbEngine.ExecuteScalar("SELECT REPLACE(REPLACE(LEFT(TDDTLS, CHARINDEX(',',TDDTLS)),'{\"TransactionID\":\"receipt#',''),'\",','') TDDTLS FROM BUSDTA.M50012 WHERE TDID='" + tdid + "'").ToString();
                string receiptId = DbEngine.ExecuteScalar("SELECT TDPNTID FROM BUSDTA.M50012 WHERE TDID='" + tdid + "'").ToString();
                if (!string.IsNullOrEmpty(receiptId))
                {
                    query = string.Format("UPDATE BUSDTA.Receipt_Header SET StatusId=18, ReasonCodeId=(SELECT ReasonCodeId FROM BUSDTA.ReasonCodeMaster WHERE ReasonCodeDescription='{0}') WHERE CustShipToId={1} AND ReceiptId={2} AND RouteId={3}", reasonCodeId, customerId, receiptId, routeId);
                    DbEngine.ExecuteNonQuery(query);

                    query = string.Format("UPDATE BUSDTA.Customer_Ledger_R1 SET StatusID= 18 WHERE CustBillToId={0} AND SourceID= {1} AND SourceDocID = {2}", customerId, routeId, orderId);
                    DbEngine.ExecuteNonQuery(query);

                    ResourceManager.QueueManager.QueueProcess("ARPayments", false);
                }
            }
            catch (Exception ex)
            {
                tran.Logger.Error("[SalesLogicExpress.Application.Managers][TransactionManager][VoidPayment][ExceptionStackTrace = " + ex.Message == null ? ex.InnerException.Message : ex.Message + "]");

            }
            tran.Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][End:VoidPayment]");

        }
        //protected virtual void OnTransactionLogged(EventArgs e)
        //{
        //    EventHandler<EventArgs> handler = TransactionLogged;
        //    if (handler != null)
        //    {
        //        handler(this, e);
        //    }
        //}
        //public event EventHandler<EventArgs> TransactionLogged;

        public static string GetPaymentTransactionID(string orderTransID)
        {
            TransactionManager tran = new TransactionManager();
            tran.Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][Start:GetPaymentTransactionID]");
            string transID = string.Empty;
            try
            {

                //string query = "  select TDID from  BUSDTA.M50012  where TDTYP='payment'   and TDPNTID='" + orderTransID + "' ORDER BY TDID DESC  ";
                string query = ("CALL BUSDTA.SP_GetPaymentTransactionID( @orderTransID = '" + orderTransID + "')");
                transID = DbEngine.ExecuteScalar(query);
                tran.Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][End:GetPaymentTransactionID]");

            }
            catch (Exception ex)
            {
                tran.Logger.Error("[SalesLogicExpress.Application.Managers][TransactionManager][GetPaymentTransactionID][ExceptionStackTrace = " + ex.Message == null ? ex.InnerException.Message : ex.Message + "]");
            }
            return transID;
        }

        /// <summary>
        /// Voids the invoices generated from the specified order 
        /// </summary>
        /// <param name="orderId">Order id</param>
        public void UpdateOrderInvoiceStatus(string orderId)
        {
            string query = "";
            Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][Start:UpdateOrderInvoiceStatus]");
            try
            {
                if (string.IsNullOrEmpty(orderId))
                    return;

                query = "UPDATE BUSDTA.Invoice_Header SET DeviceStatusID=(SELECT StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VOID')";
                query = query + " FROM BUSDTA.Invoice_Header IH INNER JOIN BUSDTA.ORDER_HEADER OH";
                query = query + " ON IH.CustShipToId=OH.CustShipToId AND IH.OrderId=OH.OrderID";
                query = query + " AND IH.RouteId=OH.OriginatingRouteID ";
                query = query + " WHERE OH.OrderID=" + orderId;

                DbEngine.ExecuteNonQuery(query);
                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TransactionManager][UpdateOrderInvoiceStatus][OrderNo=" + orderId + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][End:UpdateOrderInvoiceStatus]");
        }

        /// <summary>
        /// Gets the complete status of EOD activity 
        /// </summary>
        /// <returns>True if EOD completed, blank as otherwise</returns>
        public string GetEODCompleteStatus()
        {
            string EODStatus = "";
            Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][Start:GetEODCompleteStatus]");
            try
            {
                //string query = "SELECT 'TRUE' AS ISEODDONETODAY FROM BUSDTA.Route_Settlement";
                //query = query + "\nWHERE DATEFORMAT(SettlementDateTime, 'dd-mmm-yyyy')=DATEFORMAT(getdate(), 'dd-mmm-yyyy')";
                //query = query + "\nAND Status IN (SELECT StatusTypeId FROM BUSDTA.Status_Type WHERE StatusTypeCD='VERF')";
                string query = ("CALL  BUSDTA.SP_GetEODCompleteStatus()");
                EODStatus = DbEngine.ExecuteScalar(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TransactionManager][GetEODCompleteStatus][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][End:GetEODCompleteStatus]");
            return EODStatus;
        }
        //Add the Disposed function by Vignesh.S
        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

    }

    public class ActivityTypeInfo
    {
        public ActivityType ActivityType { get; set; }
        public ActivityKey ActivityKey { get; set; }
        public bool IsLedgered { get; set; }
        public bool IsStart { get; set; }
        public bool IsEnd { get; set; }
        public string Description { get; set; }
        public string ParentTransaction { get; set; }
        public string TransactionType { get; set; }
    }
}
