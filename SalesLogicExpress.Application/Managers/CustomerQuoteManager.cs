﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Managers
{
    public class CustomerQuoteManager : IDisposable
    {
        #region Variable and object declaration

        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.CustomerQuoteManager");

        #endregion

        #region Methods

        /// <summary>
        /// Retrieves a list of quote summary 
        /// </summary>
        /// <returns>quote summary list</returns>
        public ObservableCollection<QuoteSummary> GetQuoteSummaryList()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:GetQuoteSummaryList]");
            ObservableCollection<QuoteSummary> quoteList = null;
            DataSet quoteSummaryDS = null;
            StringBuilder quoteListQueryBuilder = new StringBuilder();
            try
            {
                //if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                //{
                //    quoteListQueryBuilder.Append("SELECT ProspectQuoteId  as QuoteId");
                //    quoteListQueryBuilder.Append("\n,ProspectQuoteId ");
                //}
                //else
                //{
                //    quoteListQueryBuilder.Append("SELECT CustomerQuoteId as QuoteId");
                //    quoteListQueryBuilder.Append("\n, CustomerQuoteId");
                //}

                //quoteListQueryBuilder.Append("\n,RouteId");
                //quoteListQueryBuilder.Append("\n,ISNULL(AIAC24PriceProtection,'') AS PriceProtection");
                //quoteListQueryBuilder.Append("\n,ISNULL(AIAC27AlliedDiscount,'') AS AlliedProductLineOverall");
                //quoteListQueryBuilder.Append("\n,ISNULL(AIAC15CategoryCode15,'') AS AlliedProductLine1");
                //quoteListQueryBuilder.Append("\n,ISNULL(AIAC16CategoryCode16,'') AS AlliedProductLine2");
                //quoteListQueryBuilder.Append("\n,ISNULL(AIAC17CategoryCode17,'') AS AlliedProductLine3");
                //quoteListQueryBuilder.Append("\n,ISNULL(AIAC18CategoryCode18,'') AS AlliedProductLine4");
                //quoteListQueryBuilder.Append("\n,ISNULL(AIAC23LiquidCoffee,'') AS LiquidBracket");
                //quoteListQueryBuilder.Append("\n,ISNULL(AIAC28CoffeeVolume,'') AS CoffVolume");
                //quoteListQueryBuilder.Append("\n,ISNULL(AIAC29EquipmentProgPts,'') AS EquipProg");
                //quoteListQueryBuilder.Append("\n,ISNULL(AIAC22POSUpCharge,'') AS POSUpCharge");
                //quoteListQueryBuilder.Append("\n,ISNULL(AIAC30SpecialCCP,'') AS SpecialCCP");
                //quoteListQueryBuilder.Append("\n,(CASE AIAC27AlliedDiscount WHEN AIAC27AlliedDiscountMst THEN 'FALSE' ELSE 'TRUE' END) AS IsAlliedProductLineOverallOverriden");
                //quoteListQueryBuilder.Append("\n,(CASE AIAC15CategoryCode15 WHEN AIAC15CategoryCode15Mst THEN 'FALSE' ELSE 'TRUE' END) AS IsAlliedProductLine1Overriden");
                //quoteListQueryBuilder.Append("\n,(CASE AIAC16CategoryCode16 WHEN AIAC16CategoryCode16Mst THEN 'FALSE' ELSE 'TRUE' END) AS IsAlliedProductLine2Overriden");
                //quoteListQueryBuilder.Append("\n,(CASE AIAC17CategoryCode17 WHEN AIAC17CategoryCode17Mst THEN 'FALSE' ELSE 'TRUE' END) AS IsAlliedProductLine3Overriden");
                //quoteListQueryBuilder.Append("\n,(CASE AIAC18CategoryCode18 WHEN AIAC18CategoryCode18Mst THEN 'FALSE' ELSE 'TRUE' END) AS IsAlliedProductLine4Overriden");
                //quoteListQueryBuilder.Append("\n,(CASE AIAC23LiquidCoffee WHEN AIAC23LiquidCoffeeMst THEN 'FALSE' ELSE 'TRUE' END) AS IsLiquidBracketOverriden");
                //quoteListQueryBuilder.Append("\n,(CASE AIAC28CoffeeVolume WHEN AIAC28CoffeeVolumeMst THEN 'FALSE' ELSE 'TRUE' END) AS IsCoffVolumeOverriden");
                //quoteListQueryBuilder.Append("\n,(CASE AIAC29EquipmentProgPts WHEN AIAC29EquipmentProgPtsMst THEN 'FALSE' ELSE 'TRUE' END) AS IsEquipProgOverriden");
                //quoteListQueryBuilder.Append("\n,(CASE AIAC22POSUpCharge WHEN AIAC22POSUpChargeMst THEN 'FALSE' ELSE 'TRUE' END) AS IsPOSUpChargeOverriden");
                //quoteListQueryBuilder.Append("\n,(CASE AIAC30SpecialCCP WHEN AIAC30SpecialCCPMst THEN 'FALSE' ELSE 'TRUE' END) AS IsSpecialCCPOverriden");
                if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                {
                    //quoteListQueryBuilder.Append("\nFROM BUSDTA.Prospect_Quote_Header");
                    //quoteListQueryBuilder.Append("\nWHERE ProspectId={0} AND RouteId={1}");
                    //quoteSummaryDS = DbEngine.ExecuteDataSet(string.Format(quoteListQueryBuilder.ToString(), ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID, CommonNavInfo.RouteID));
                    quoteSummaryDS = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetQuoteSummaryListForProspectPriceList(" + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID + "," + CommonNavInfo.RouteID + ")");

                }
                else
                {
                    //quoteListQueryBuilder.Append("\nFROM BUSDTA.Customer_Quote_Header");
                    //quoteListQueryBuilder.Append("\nWHERE CustomerId={0} AND RouteId={1}");
                    //quoteSummaryDS = DbEngine.ExecuteDataSet(string.Format(quoteListQueryBuilder.ToString(), CommonNavInfo.Customer.CustomerNo, CommonNavInfo.RouteID));
                    quoteSummaryDS = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetQuoteSummaryListForCustPriceList(" + CommonNavInfo.Customer.CustomerNo + "," + CommonNavInfo.RouteID + ")");

                }



                List<PricingDetails> priceSetupList = quoteSummaryDS.GetEntityList<PricingDetails>();

                quoteListQueryBuilder.Clear();
                if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                {
                    //quoteListQueryBuilder.Append("SELECT CQH.ProspectQuoteId AS QuoteId");
                    //quoteListQueryBuilder.Append("\n,CAST(CQH.IsPrinted AS INT) AS IsPrinted");
                    //quoteListQueryBuilder.Append("\n,CQH.PriceDate AS QouteDate");
                    //quoteListQueryBuilder.Append("\n,COUNT(CQD.ProspectQuoteId ) AS QuotedItemCount");
                    //quoteListQueryBuilder.Append("\n,SUM(CQD.SampleQty ) AS SampleQty");
                    //quoteListQueryBuilder.Append("\nFROM BUSDTA.Prospect_Quote_Header CQH");
                    //quoteListQueryBuilder.Append("\nINNER JOIN BUSDTA.Prospect_Quote_Detail CQD ");
                    //quoteListQueryBuilder.Append("\nON CQH.ProspectQuoteId=CQD.ProspectQuoteId AND CQH.RouteId=CQD.RouteId");
                    //quoteListQueryBuilder.Append("\nWHERE CQH.ProspectId={0} ");
                    //quoteListQueryBuilder.Append("\nGROUP BY CQH.ProspectQuoteId, CQH.IsPrinted, CQH.PriceDate");
                    //quoteListQueryBuilder.Append("\nORDER BY CQH.PriceDate DESC");
                    //string QUERY = string.Format(quoteListQueryBuilder.ToString(), ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID);
                    //quoteSummaryDS = DbEngine.ExecuteDataSet(QUERY);
                    quoteSummaryDS = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetQuoteSummaryListIsProspect(" + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID + ")");
                }
                else
                {
                    //quoteListQueryBuilder.Append("SELECT CQH.CustomerQuoteId AS QuoteId");
                    //quoteListQueryBuilder.Append("\n,CAST(CQH.IsPrinted AS INT) AS IsPrinted");
                    //quoteListQueryBuilder.Append("\n,CQH.PriceDate AS QouteDate");
                    //quoteListQueryBuilder.Append("\n,COUNT(CQD.CustomerQuoteId) AS QuotedItemCount");
                    //quoteListQueryBuilder.Append("\nFROM BUSDTA.Customer_Quote_Header CQH");
                    //quoteListQueryBuilder.Append("\nINNER JOIN BUSDTA.Customer_Quote_Detail CQD ");
                    //quoteListQueryBuilder.Append("\nON CQH.CustomerQuoteId=CQD.CustomerQuoteId AND CQH.RouteId=CQD.RouteId");
                    //quoteListQueryBuilder.Append("\nWHERE CQH.CustomerId={0}");
                    //quoteListQueryBuilder.Append("\nGROUP BY CQH.CustomerQuoteId, CQH.IsPrinted, CQH.PriceDate");
                    //quoteListQueryBuilder.Append("\nORDER BY CQH.PriceDate DESC");
                    //quoteSummaryDS = DbEngine.ExecuteDataSet(string.Format(quoteListQueryBuilder.ToString(), CommonNavInfo.Customer.CustomerNo));
                    quoteSummaryDS = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetQuoteSummaryList(" + CommonNavInfo.Customer.CustomerNo + ")");

                }


                List<QuoteSummary> quoteSummaryList = quoteSummaryDS.GetEntityList<QuoteSummary>();

                for (int i = 0; i < quoteSummaryList.Count; i++)
                {
                    PricingDetails priceDetail = priceSetupList.Where(o => o.QuoteId.Equals(quoteSummaryList[i].QuoteId)).FirstOrDefault();

                    quoteSummaryList[i].PricingSetup = PricingSetup.Master.ToString();

                    if (priceDetail != null)
                    {

                        if (priceDetail.IsAlliedProductLine1Overriden || priceDetail.IsAlliedProductLine2Overriden || priceDetail.IsAlliedProductLine3Overriden
                            || priceDetail.IsAlliedProductLine4Overriden || priceDetail.IsAlliedProductLineOverallOverriden
                            || priceDetail.IsCoffVolumeOverriden || priceDetail.IsEquipProgOverriden || priceDetail.IsLiquidBracketOverriden
                            || priceDetail.IsPOSUpChargeOverriden || priceDetail.IsSpecialCCPOverriden)
                        {
                            quoteSummaryList[i].PricingSetup = PricingSetup.Override.ToString();
                        }
                    }

                    quoteSummaryList[i].PriceDetail = priceDetail;
                }
                quoteList = quoteSummaryList.ToObservableCollection<QuoteSummary>();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][GetQuoteSummaryList][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:GetQuoteSummaryList]");
            return quoteList;
        }

        /// <summary>
        /// Retrieves a overriden pricing detail 
        /// </summary>
        /// <param name="customerQuoteId">Optional, fetch master price details of existing prices</param>
        /// <returns>Overriden pricing detail</returns>
        private PricingDetails GetOverridenPriceDetails(int customerQuoteId)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:GetOverridenPriceList]");

            PricingDetails priceDetails = null;
            //    StringBuilder quoteListQueryBuilder = new StringBuilder();        // Unused Local --Vignesh D

            try
            {
                DataSet quoteSummaryDS = new DataSet();
                if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                {
                    //quoteListQueryBuilder.Append("SELECT ProspectQuoteId");
                    //quoteListQueryBuilder.Append("\n,RouteId");
                    //quoteListQueryBuilder.Append("\n,AIAC24PriceProtection AS PriceProtection");
                    //quoteListQueryBuilder.Append("\n,AIAC27AlliedDiscount AS AlliedProductLineOverall");
                    //quoteListQueryBuilder.Append("\n,AIAC15CategoryCode15 AS AlliedProductLine1");
                    //quoteListQueryBuilder.Append("\n,AIAC16CategoryCode16 AS AlliedProductLine2");
                    //quoteListQueryBuilder.Append("\n,AIAC17CategoryCode17 AS AlliedProductLine3");
                    //quoteListQueryBuilder.Append("\n,AIAC18CategoryCode18 AS AlliedProductLine4");
                    //quoteListQueryBuilder.Append("\n,AIAC23LiquidCoffee AS LiquidBracket");
                    //quoteListQueryBuilder.Append("\n,AIAC28CoffeeVolume AS CoffVolume");
                    //quoteListQueryBuilder.Append("\n,AIAC29EquipmentProgPts AS EquipProg");
                    //quoteListQueryBuilder.Append("\n,AIAC22POSUpCharge AS POSUpCharge");
                    //quoteListQueryBuilder.Append("\n,AIAC30SpecialCCP AS SpecialCCP");
                    //quoteListQueryBuilder.Append("\n,(CASE AIAC27AlliedDiscount WHEN AIAC27AlliedDiscountMst THEN 'FALSE' ELSE 'TRUE' END) AS IsAlliedProductLineOverallOverriden");

                    //quoteListQueryBuilder.Append("\n,(CASE AIAC15CategoryCode15 WHEN AIAC15CategoryCode15Mst THEN 'FALSE' ELSE 'TRUE' END) AS IsAlliedProductLine1Overriden");

                    //quoteListQueryBuilder.Append("\n,(CASE AIAC16CategoryCode16 WHEN AIAC16CategoryCode16Mst THEN 'FALSE' ELSE 'TRUE' END) AS IsAlliedProductLine2Overriden");

                    //quoteListQueryBuilder.Append("\n,(CASE AIAC17CategoryCode17 WHEN AIAC17CategoryCode17Mst THEN 'FALSE' ELSE 'TRUE' END) AS IsAlliedProductLine3Overriden");

                    //quoteListQueryBuilder.Append("\n,(CASE AIAC18CategoryCode18 WHEN AIAC18CategoryCode18Mst THEN 'FALSE' ELSE 'TRUE' END) AS IsAlliedProductLine4Overriden");

                    //quoteListQueryBuilder.Append("\n,(CASE AIAC23LiquidCoffee WHEN AIAC23LiquidCoffeeMst THEN 'FALSE' ELSE 'TRUE' END) AS IsLiquidBracketOverriden");

                    //quoteListQueryBuilder.Append("\n,(CASE AIAC28CoffeeVolume WHEN AIAC28CoffeeVolumeMst THEN 'FALSE' ELSE 'TRUE' END) AS IsCoffVolumeOverriden");

                    //quoteListQueryBuilder.Append("\n,(CASE AIAC29EquipmentProgPts WHEN AIAC29EquipmentProgPtsMst THEN 'FALSE' ELSE 'TRUE' END) AS IsEquipProgOverriden");

                    //quoteListQueryBuilder.Append("\n,(CASE AIAC22POSUpCharge WHEN AIAC22POSUpChargeMst THEN 'FALSE' ELSE 'TRUE' END) AS IsPOSUpChargeOverriden");

                    //quoteListQueryBuilder.Append("\n,(CASE AIAC30SpecialCCP WHEN AIAC30SpecialCCPMst THEN 'FALSE' ELSE 'TRUE' END) AS IsSpecialCCPOverriden");
                    //quoteListQueryBuilder.Append("\nFROM BUSDTA.Prospect_Quote_Header");
                    //quoteListQueryBuilder.Append("\nWHERE ProspectId={0} AND RouteId={1} AND ProspectQuoteId={2}");
                    //quoteSummaryDS = DbEngine.ExecuteDataSet(string.Format(quoteListQueryBuilder.ToString(), ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID, CommonNavInfo.RouteID, customerQuoteId));

                    quoteSummaryDS = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetOverridenPriceDetForProspectQuote(" + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID + ", " + CommonNavInfo.RouteID + "," + customerQuoteId + ")");
                }
                else
                {
                    //quoteListQueryBuilder.Append("SELECT CustomerQuoteId");
                    //quoteListQueryBuilder.Append("\nCustomerQuoteId");
                    //quoteListQueryBuilder.Append("\n,RouteId");
                    //quoteListQueryBuilder.Append("\n,AIAC24PriceProtection AS PriceProtection");
                    //quoteListQueryBuilder.Append("\n,AIAC27AlliedDiscount AS AlliedProductLineOverall");
                    //quoteListQueryBuilder.Append("\n,AIAC15CategoryCode15 AS AlliedProductLine1");
                    //quoteListQueryBuilder.Append("\n,AIAC16CategoryCode16 AS AlliedProductLine2");
                    //quoteListQueryBuilder.Append("\n,AIAC17CategoryCode17 AS AlliedProductLine3");
                    //quoteListQueryBuilder.Append("\n,AIAC18CategoryCode18 AS AlliedProductLine4");
                    //quoteListQueryBuilder.Append("\n,AIAC23LiquidCoffee AS LiquidBracket");
                    //quoteListQueryBuilder.Append("\n,AIAC28CoffeeVolume AS CoffVolume");
                    //quoteListQueryBuilder.Append("\n,AIAC29EquipmentProgPts AS EquipProg");
                    //quoteListQueryBuilder.Append("\n,AIAC22POSUpCharge AS POSUpCharge");
                    //quoteListQueryBuilder.Append("\n,AIAC30SpecialCCP AS SpecialCCP");
                    //quoteListQueryBuilder.Append("\n,(CASE AIAC27AlliedDiscount WHEN AIAC27AlliedDiscountMst THEN 'FALSE' ELSE 'TRUE' END) AS IsAlliedProductLineOverallOverriden");

                    //quoteListQueryBuilder.Append("\n,(CASE AIAC15CategoryCode15 WHEN AIAC15CategoryCode15Mst THEN 'FALSE' ELSE 'TRUE' END) AS IsAlliedProductLine1Overriden");

                    //quoteListQueryBuilder.Append("\n,(CASE AIAC16CategoryCode16 WHEN AIAC16CategoryCode16Mst THEN 'FALSE' ELSE 'TRUE' END) AS IsAlliedProductLine2Overriden");

                    //quoteListQueryBuilder.Append("\n,(CASE AIAC17CategoryCode17 WHEN AIAC17CategoryCode17Mst THEN 'FALSE' ELSE 'TRUE' END) AS IsAlliedProductLine3Overriden");

                    //quoteListQueryBuilder.Append("\n,(CASE AIAC18CategoryCode18 WHEN AIAC18CategoryCode18Mst THEN 'FALSE' ELSE 'TRUE' END) AS IsAlliedProductLine4Overriden");

                    //quoteListQueryBuilder.Append("\n,(CASE AIAC23LiquidCoffee WHEN AIAC23LiquidCoffeeMst THEN 'FALSE' ELSE 'TRUE' END) AS IsLiquidBracketOverriden");

                    //quoteListQueryBuilder.Append("\n,(CASE AIAC28CoffeeVolume WHEN AIAC28CoffeeVolumeMst THEN 'FALSE' ELSE 'TRUE' END) AS IsCoffVolumeOverriden");

                    //quoteListQueryBuilder.Append("\n,(CASE AIAC29EquipmentProgPts WHEN AIAC29EquipmentProgPtsMst THEN 'FALSE' ELSE 'TRUE' END) AS IsEquipProgOverriden");

                    //quoteListQueryBuilder.Append("\n,(CASE AIAC22POSUpCharge WHEN AIAC22POSUpChargeMst THEN 'FALSE' ELSE 'TRUE' END) AS IsPOSUpChargeOverriden");

                    //quoteListQueryBuilder.Append("\n,(CASE AIAC30SpecialCCP WHEN AIAC30SpecialCCPMst THEN 'FALSE' ELSE 'TRUE' END) AS IsSpecialCCPOverriden");
                    //quoteListQueryBuilder.Append("\nFROM BUSDTA.Customer_Quote_Header");
                    //quoteListQueryBuilder.Append("\nWHERE CustomerId={0} AND RouteId={1} AND CustomerQuoteId={2}");
                    //quoteSummaryDS = DbEngine.ExecuteDataSet(string.Format(quoteListQueryBuilder.ToString(), CommonNavInfo.Customer.CustomerNo, CommonNavInfo.RouteID, customerQuoteId));
                    quoteSummaryDS = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetOverridenPriceDetForCustQuote(" + CommonNavInfo.Customer.CustomerNo + "," + CommonNavInfo.RouteID + "," + customerQuoteId + ")");

                }



                priceDetails = quoteSummaryDS.GetEntityList<PricingDetails>().FirstOrDefault();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][GetOverridenPriceList][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:GetOverridenPriceList]");
            return priceDetails;
        }

        /// <summary>
        /// Gets price details for use in the quote 
        /// </summary>
        /// <param name="IsNewQuote">Indiactes whther to fetch price for new quote or existing quote </param>
        /// <param name="quoteId">Optional, fetch master price details of existing prices</param>
        /// <returns>Master price details</returns>
        public PricingDetails GetMasterPriceDetails(bool IsNewQuote, int quoteId = 0)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:GetMasterPriceDetails]");

            PricingDetails pricingDetails = null;
            //  StringBuilder queryBuilder = new StringBuilder();     // Unused Local --Vignesh D
            DataSet result = null;

            try
            {
                if (IsNewQuote)
                {
                    if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    {
                        //Get Pricing from Master
                        //queryBuilder.Append("SELECT CategoryCode15 AS AlliedProductLine1");
                        //queryBuilder.Append("\n,CategoryCode16 AS AlliedProductLine2");
                        //queryBuilder.Append("\n,CategoryCode17 AS AlliedProductLine3");
                        //queryBuilder.Append("\n,CategoryCode18 AS AlliedProductLine4");
                        //queryBuilder.Append("\n,POSUpCharge AS POSUpCharge");
                        //queryBuilder.Append("\n,LiquidCoffee AS LiquidBracket");
                        //queryBuilder.Append("\n,PriceProtection AS PriceProtection");
                        //queryBuilder.Append("\n,AlliedDiscount AS AlliedProductLineOverall");
                        //queryBuilder.Append("\n,CoffeeVolume AS CoffVolume");
                        //queryBuilder.Append("\n,EquipmentProgPts AS EquipProg");
                        //queryBuilder.Append("\n,SpecialCCP AS SpecialCCP");
                        //queryBuilder.Append("\n,TaxGroup AS TaxGroup");
                        //queryBuilder.Append("\n,0 AS CustomerQuoteId");
                        //queryBuilder.Append("\n,0 AS ProspectQuoteId");
                        //queryBuilder.Append("\nFROM BUSDTA.Prospect_Master");
                        //queryBuilder.Append("\nWHERE ProspectId={0}");
                        //string query1 = string.Format(queryBuilder.ToString(), ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID);
                        //result = DbEngine.ExecuteDataSet(query1);
                        result = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetMasterPriceDetForProspectQuote(" + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID + ")");


                    }
                    else
                    {
                        //Get Pricing from Master
                        //queryBuilder.Append("SELECT AIAC15 AS AlliedProductLine1");
                        //queryBuilder.Append("\n,AIAC16 AS AlliedProductLine2");
                        //queryBuilder.Append("\n,AIAC17 AS AlliedProductLine3");
                        //queryBuilder.Append("\n,AIAC18 AS AlliedProductLine4");
                        //queryBuilder.Append("\n,AIAC22 AS POSUpCharge");
                        //queryBuilder.Append("\n,AIAC23 AS LiquidBracket");
                        //queryBuilder.Append("\n,AIAC24 AS PriceProtection");
                        //queryBuilder.Append("\n,AIAC27 AS AlliedProductLineOverall");
                        //queryBuilder.Append("\n,AIAC28 AS CoffVolume");
                        //queryBuilder.Append("\n,AIAC29 AS EquipProg");
                        //queryBuilder.Append("\n,AIAC30 AS SpecialCCP");
                        //queryBuilder.Append("\n,0 AS CustomerQuoteId");
                        //queryBuilder.Append("\n,0 AS ProspectQuoteId");
                        //queryBuilder.Append("\nFROM BUSDTA.F03012");
                        //queryBuilder.Append("\nWHERE AIAN8={0}");

                        //result = ViewModelPayload.PayloadManager.ProspectPayload.IsProspect ?
                        //    DbEngine.ExecuteDataSet(string.Format(queryBuilder.ToString(), ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID)) :
                        //    DbEngine.ExecuteDataSet(string.Format(queryBuilder.ToString(), CommonNavInfo.Customer.CustomerNo));


                        //result = DbEngine.ExecuteDataSet(string.Format(queryBuilder.ToString(), CommonNavInfo.Customer.CustomerNo));
                        result = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetMasterPriceDetailsForQuote(" + CommonNavInfo.Customer.CustomerNo + ")");
                    }
                }
                else
                {
                    //queryBuilder.Append("SELECT AIAC15CategoryCode15Mst AS AlliedProductLine1");
                    //queryBuilder.Append("\n,AIAC16CategoryCode16Mst AS AlliedProductLine2");
                    //queryBuilder.Append("\n,AIAC17CategoryCode17Mst AS AlliedProductLine3");
                    //queryBuilder.Append("\n,AIAC18CategoryCode18Mst AS AlliedProductLine4");
                    //queryBuilder.Append("\n,AIAC22POSUpChargeMst AS POSUpCharge");
                    //queryBuilder.Append("\n,AIAC23LiquidCoffeeMst AS LiquidBracket");
                    //queryBuilder.Append("\n,AIAC24PriceProtectionMst AS PriceProtection");
                    //queryBuilder.Append("\n,AIAC27AlliedDiscountMst AS AlliedProductLineOverall");
                    //queryBuilder.Append("\n,AIAC28CoffeeVolumeMst AS CoffVolume");
                    //queryBuilder.Append("\n,AIAC29EquipmentProgPtsMst AS EquipProg");
                    //queryBuilder.Append("\n,AIAC30SpecialCCPMst AS SpecialCCP");
                    if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    {
                        //queryBuilder.Append("\n,TaxGroup AS TaxGroup");
                        //queryBuilder.Append("\n,ProspectQuoteId");//
                        //queryBuilder.Append("\nFROM BUSDTA.Prospect_Quote_Header PQH JOIN BUSDTA.Prospect_Master PM ON PQH.ProspectId=PM.ProspectId");
                        //queryBuilder.Append("\nWHERE PQH.ProspectId={0} AND PQH.RouteId={1} AND ProspectQuoteId={2}");
                        //result = DbEngine.ExecuteDataSet(string.Format(queryBuilder.ToString(), ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID, CommonNavInfo.RouteID, quoteId));
                        result = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetMasterPriceDetForExistProspectQuote(" + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID + "," + CommonNavInfo.RouteID + "," + quoteId + ")");
                    }
                    else
                    {
                        //queryBuilder.Append("\n,CustomerQuoteId");
                        //queryBuilder.Append("\nFROM BUSDTA.Customer_Quote_Header");
                        //queryBuilder.Append("\nWHERE CustomerId={0} AND RouteId={1} AND CustomerQuoteId={2}");

                        //result = DbEngine.ExecuteDataSet(string.Format(queryBuilder.ToString(), CommonNavInfo.Customer.CustomerNo, CommonNavInfo.RouteID, quoteId));

                        result = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetMasterPriceDetailsForExistQuote(" + CommonNavInfo.Customer.CustomerNo + ", " + CommonNavInfo.RouteID + ", " + quoteId + ")");
                    }

                }
                pricingDetails = result.GetEntityList<PricingDetails>().FirstOrDefault();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][GetMasterPriceDetails][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:GetMasterPriceDetails]");
            return pricingDetails;
        }

        /// <summary>
        /// Retrives the customer quote 
        /// </summary>
        /// <param name="quoteID">Customer quote id</param>
        /// <returns>Customer quote</returns>
        public CustomerQuoteHeader GetCustomerQuote(int quoteID = 0)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:GetCustomerQuote]");
            CustomerQuoteHeader customerQuote = new CustomerQuoteHeader();

            try
            {
                customerQuote.CustomerId = ViewModelPayload.PayloadManager.ProspectPayload.IsProspect ? -1 : Convert.ToInt32(CommonNavInfo.Customer.CustomerNo);
                customerQuote.QuoteId = ViewModelPayload.PayloadManager.QuotePayload.QuoteId == "NEW" ? 0 : Convert.ToInt32(ViewModelPayload.PayloadManager.QuotePayload.QuoteId);
                quoteID = customerQuote.QuoteId;

                if (quoteID == 0)
                {
                    customerQuote.IsNewQuote = true;
                    customerQuote.QuoteDate = DateTime.Now;
                    customerQuote.Parent = ViewModelPayload.PayloadManager.ProspectPayload.IsProspect ?
                        (ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID + ", " + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.Name) :
                        this.GetAddressInfo(quoteID, AddressInfo.Parent);
                    customerQuote.BillTo = ViewModelPayload.PayloadManager.ProspectPayload.IsProspect ? customerQuote.Parent :
                        this.GetAddressInfo(quoteID, AddressInfo.BillTo);
                    customerQuote.MasterPriceSetup = this.GetMasterPriceDetails(true);
                    //customerQuote.MasterPriceCopySetup = customerQuote.MasterPriceSetup.Clone();
                    customerQuote.OverridePriceSetup = customerQuote.MasterPriceSetup.Clone();
                    customerQuote.IsSettled = false;
                }
                else
                {
                    customerQuote.IsNewQuote = false;
                    this.UpdateQuoteWithCustomerPriceSetup(quoteID.ToString());

                    //string strIsPrinted = ViewModelPayload.PayloadManager.ProspectPayload.IsProspect ?
                    //    DbEngine.ExecuteScalar("SELECT IsPrinted FROM BUSDTA.Prospect_Quote_Header WHERE ProspectId=" + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID + " AND RouteId=" + CommonNavInfo.RouteID.ToString() + " AND ProspectQuoteId=" + quoteID) :
                    //    DbEngine.ExecuteScalar("SELECT IsPrinted FROM BUSDTA.Customer_Quote_Header WHERE CustomerId=" + CommonNavInfo.Customer.CustomerNo + " AND RouteId=" + CommonNavInfo.RouteID.ToString() + " AND CustomerQuoteId=" + quoteID);
                    string strIsPrinted = "";
                    if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    {
                        //strIsPrinted = DbEngine.ExecuteScalar("SELECT IsPrinted FROM BUSDTA.Prospect_Quote_Header WHERE ProspectId=" + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID + " AND RouteId=" + CommonNavInfo.RouteID.ToString() + " AND ProspectQuoteId=" + quoteID);
                        strIsPrinted = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetProspectExistQuote (" + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID + "," + CommonNavInfo.RouteID.ToString() + "," + quoteID + ")");
                    }
                    else
                    {
                        //strIsPrinted = DbEngine.ExecuteScalar("SELECT IsPrinted FROM BUSDTA.Customer_Quote_Header WHERE CustomerId=" + CommonNavInfo.Customer.CustomerNo + " AND RouteId=" + CommonNavInfo.RouteID.ToString() + " AND CustomerQuoteId=" + quoteID);
                        strIsPrinted = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetCustomerExistQuote(" + CommonNavInfo.Customer.CustomerNo + "," + CommonNavInfo.RouteID.ToString() + "," + quoteID + ")");
                    }

                    customerQuote.IsPrinted = Convert.ToBoolean(Convert.ToInt16(string.IsNullOrEmpty(strIsPrinted) ? "0" : strIsPrinted));
                    customerQuote.Parent = ViewModelPayload.PayloadManager.ProspectPayload.IsProspect ?
                        (ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID + ", " + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.Name) :
                        this.GetAddressInfo(quoteID, AddressInfo.Parent);
                    customerQuote.BillTo = ViewModelPayload.PayloadManager.ProspectPayload.IsProspect ? customerQuote.Parent :
                        this.GetAddressInfo(quoteID, AddressInfo.BillTo);
                    customerQuote.MasterPriceSetup = this.GetMasterPriceDetails(false, quoteID);
                    customerQuote.OverridePriceSetup = this.GetOverridenPriceDetails(quoteID);

                    string resultCount = "";

                    //if(!ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    //{
                    //    resultCount=DbEngine.ExecuteScalar("SELECT COUNT(*) AS TotalCount FROM BUSDTA.Customer_Quote_Header WHERE StatusId=(SELECT StatusTypeId FROM BUSDTA.Status_Type WHERE StatusTypeCD='STTLD') AND CustomerQuoteId="+ quoteID);
                    //}
                    //else
                    //{
                    //    resultCount = DbEngine.ExecuteScalar("SELECT COUNT(*) AS TotalCount FROM BUSDTA.Prospect_Quote_Header WHERE StatusId=(SELECT StatusTypeId FROM BUSDTA.Status_Type WHERE StatusTypeCD='STTLD') AND ProspectQuoteId=" + quoteID);
                    //}

                    if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    {
                        resultCount = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetQuoteTotalCount (" + quoteID + ",'P')");
                    }
                    else
                    {
                        resultCount = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetQuoteTotalCount (" + quoteID + ",'C')");
                    }

                    if (string.IsNullOrEmpty(resultCount))
                    {
                        resultCount = "0";
                    }

                    customerQuote.IsSettled = Convert.ToBoolean(Convert.ToByte(resultCount));

                    string quoteDate = "";

                    //if (!ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    //{
                    //    quoteDate = DbEngine.ExecuteScalar("SELECT pricedate  FROM BUSDTA.Customer_Quote_Header WHERE CustomerQuoteId=" + quoteID);
                    //}
                    //else
                    //{
                    //    quoteDate = DbEngine.ExecuteScalar("SELECT pricedate FROM BUSDTA.Prospect_Quote_Header WHERE ProspectQuoteId=" + quoteID);
                    //}

                    if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    {
                        quoteDate = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetQuotePriceDate(" + quoteID + ",'P')");
                    }
                    else
                    {
                        quoteDate = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetQuotePriceDate(" + quoteID + ",'C')");
                    }


                    if (string.IsNullOrEmpty(quoteDate))
                    {
                        quoteDate = DateTime.Now.ToString();
                    }

                    customerQuote.QuoteDate = Convert.ToDateTime(quoteDate);

                    if (customerQuote.IsPrinted || ((DateTime.Now.Date - customerQuote.QuoteDate.Date).TotalDays != 0) || customerQuote.IsSettled)
                    {
                        customerQuote.Items = this.GetQuoteItems(quoteID, false);

                    }
                    else
                    {
                        customerQuote.Items = this.GetQuoteItems(quoteID, true);

                    }

                }


            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][GetCustomerQuote][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:GetCustomerQuote]");

            return customerQuote;
        }

        /// <summary>
        /// Retrieves items for the specifed quote 
        /// </summary>
        /// <param name="quoteId">Customer quote id</param>
        /// <returns>Collection of items, null if not available </returns>
        public ObservableCollection<CustomerQuoteDetail> GetQuoteItems(int quoteId, bool editableQuote = true)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:GetQuoteItems]");
            ObservableCollection<CustomerQuoteDetail> quoteItems = null;
            // StringBuilder queryBuilder = new StringBuilder();   //Unused Local --Vignesh D

            try
            {
                DataSet result = new DataSet();
                if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                {
                    //queryBuilder.Append("SELECT ProspectQuoteId AS QuoteHeaderId");
                    //queryBuilder.Append("\n,ProspectQuoteDetailId AS QuoteDetailId");
                    //queryBuilder.Append("\n,CQD.RouteId AS RouteId");
                    //queryBuilder.Append("\n,CQD.ItemId AS ItemId");
                    //queryBuilder.Append("\n,IMUOM1 as UOM");
                    //queryBuilder.Append("\n,LTRIM(RTRIM(ITM.IMLITM)) AS ItemNumber");
                    //queryBuilder.Append("\n,LTRIM(RTRIM(IMDSC1)) AS ItemDescription");
                    //queryBuilder.Append("\n,QuoteQty AS QuoteQuantity");
                    //queryBuilder.Append("\n,CQD.TransactionUM AS TransactionUOM");
                    //queryBuilder.Append("\n,PricingAmt AS PricingUOMPrice");
                    //queryBuilder.Append("\n,CQD.PricingUM AS PricingUOM");
                    //queryBuilder.Append("\n,TransactionAmt AS TransactionUOMPrice");
                    //queryBuilder.Append("\n,OtherAmt AS OtherUOMPrice");
                    //queryBuilder.Append("\n,OtherUM AS OtherUOM");
                    //queryBuilder.Append("\n,CompetitorName");
                    //queryBuilder.Append("\n,UnitPriceAmt AS CompetitorPrice");
                    //queryBuilder.Append("\n,UnitPriceUM AS CompetitorUOM");
                    //queryBuilder.Append("\n,SampleQty AS SampleQty");
                    //queryBuilder.Append("\n,isnull(SampleUM ,'')AS SampleUOM");
                    //queryBuilder.Append("\n,isnull (iconfig.PrimaryUM ,'') as 'PrimaryUOM'");
                    //queryBuilder.Append("\n,isnull(PD.PickedQty,0) as 'PickedQty',PD.TransactionUOM  AS 'PickedUOM' ");
                    //if (editableQuote)
                    //{
                    //    queryBuilder.Append("\n,case lfivi when 'Y' then (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) +isnull(PD.PickQtyPrimaryUOM,0) else 0 end  as 'AvailableQty'");

                    //}
                    //else
                    //{
                    //    queryBuilder.Append("\n, case lfivi when 'Y' then isnull(CQD.AvailableQty,0) else 0 end   as 'AvailableQty'");
                    //}
                    //queryBuilder.Append("\nFROM BUSDTA.Prospect_Quote_Detail CQD");
                    //queryBuilder.Append("\nLEFT OUTER JOIN BUSDTA.F4101 ITM ON LTRIM(RTRIM(CQD.ItemId))= RTRIM(LTRIM(ITM.IMITM))");
                    //queryBuilder.Append("\ninner join  busdta.F4102 ITM2 on ITM2.IBLITM = ITM.IMLITM");
                    //queryBuilder.Append("\ninner join Busdta.F40205 IT3 on IT3.LFLNTY = ITM2.IBLNTY");
                    //queryBuilder.Append("\nLEFT OUTER JOIN busdta.Inventory inventory  ON LTRIM(RTRIM(inventory.ItemNumber)) = ITM.IMLITM");
                    //queryBuilder.Append("\nLEFT OUTER JOIN BUSDTA.Pick_Detail PD ON PD.ItemID=CQD.ItemId AND PD.TransactionID=CQD.ProspectQuoteId  ");
                    //queryBuilder.Append("\nAND CQD.SampleUM=PD.TransactionUOM AND PD.TransactionID=CQD.ProspectQuoteId  ");
                    //queryBuilder.Append("\nLEFT OUTER JOIN busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID ");
                    ////queryBuilder.Append("\nLEFT OUTER JOIN BUSDTA.ItemUoMs UOM ON LTRIM(RTRIM(ITM.IMITM))= RTRIM(LTRIM(UOM.ItemID))");
                    //queryBuilder.Append("\nWHERE ProspectQuoteId=" + quoteId + " AND RouteId=" + CommonNavInfo.RouteID.ToString() + " AND CQD.ProspectQuoteId =" + quoteId + " order by ProspectQuoteDetailId");
                    //string query = string.Format(queryBuilder.ToString(), quoteId, CommonNavInfo.RouteID);

                    //result = DbEngine.ExecuteDataSet(query);
                    int IsEdit = 0;
                    if (editableQuote) IsEdit = 1;
                    string query = "CALL BUSDTA.SP_GetProspectQuoteItems(" + quoteId + "," + CommonNavInfo.RouteID.ToString() + "," + IsEdit + ")";
                    result = DbEngine.ExecuteDataSet(query);
                }
                else
                {
                    //queryBuilder.Append("SELECT CustomerQuoteId AS QuoteHeaderId");
                    //queryBuilder.Append("\n,CustomerQuoteDetailId AS QuoteDetailId");
                    //queryBuilder.Append("\n,RouteId AS RouteId");
                    //queryBuilder.Append("\n,CQD.ItemId AS ItemId");
                    //queryBuilder.Append("\n,IMUOM1 AS UOM");
                    //queryBuilder.Append("\n,LTRIM(RTRIM(ITM.IMLITM)) AS ItemNumber");
                    //queryBuilder.Append("\n,LTRIM(RTRIM(IMDSC1)) AS ItemDescription");
                    //queryBuilder.Append("\n,QuoteQty AS QuoteQuantity");
                    //queryBuilder.Append("\n,TransactionUM AS TransactionUOM");
                    //queryBuilder.Append("\n,PricingAmt AS PricingUOMPrice");
                    //queryBuilder.Append("\n,PricingUM AS PricingUOM");
                    //queryBuilder.Append("\n,TransactionAmt AS TransactionUOMPrice");
                    //queryBuilder.Append("\n,OtherAmt AS OtherUOMPrice");
                    //queryBuilder.Append("\n,OtherUM AS OtherUOM");
                    //queryBuilder.Append("\n,CompetitorName");
                    //queryBuilder.Append("\n,UnitPriceAmt AS CompetitorPrice");
                    //queryBuilder.Append("\n,UnitPriceUM AS CompetitorUOM");
                    //queryBuilder.Append("\nFROM BUSDTA.Customer_Quote_Detail CQD");
                    //queryBuilder.Append("\nLEFT OUTER JOIN BUSDTA.F4101 ITM ON LTRIM(RTRIM(CQD.ItemId))= RTRIM(LTRIM(ITM.IMITM))");
                    ////queryBuilder.Append("\nLEFT OUTER JOIN BUSDTA.ItemUoMs UOM ON LTRIM(RTRIM(ITM.IMITM))= RTRIM(LTRIM(UOM.ItemID))");
                    //queryBuilder.Append("\nWHERE CustomerQuoteId=" + quoteId + " AND RouteId=" + CommonNavInfo.RouteID.ToString());

                    //result = DbEngine.ExecuteDataSet(string.Format(queryBuilder.ToString(), quoteId, CommonNavInfo.RouteID));
                    result = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetCustomerQuoteItems(" + quoteId + "," + CommonNavInfo.RouteID + ")");
                }


                if (result.HasData())
                {
                    quoteItems = result.GetEntityList<CustomerQuoteDetail>().ToObservableCollection();

                    if (PayloadManager.ProspectPayload.IsProspect)
                    {
                        //Get Picked Qty for items
                        //
                        //string queryQty = "SELECT PickedQty,TransactionUOM ,ITEMID FROM BUSDTA.Pick_Detail  WHERE TransactionID=" + quoteId + "  AND PickedQty > 0 ";
                        string queryQty = "CALL BUSDTA.SP_GetPickedQtyForItem(" + quoteId + ")";
                        DataSet ds = DbEngine.ExecuteDataSet(queryQty);
                        if (ds.HasData())
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                string pickQty = ds.Tables[0].Rows[i]["PickedQty"].ToString();
                                pickQty = string.IsNullOrEmpty(pickQty) ? "0" : pickQty;
                                string pickUOM = ds.Tables[0].Rows[i]["TransactionUOM"].ToString();
                                string itemID = ds.Tables[0].Rows[i]["ITEMID"].ToString();
                                quoteItems.Where(x => x.ItemId == itemID.Trim()).ToList().ForEach(x => { x.PickedQty = Convert.ToInt32(pickQty); x.PickedUOM = pickUOM.Trim(); });
                            }
                            //PayloadManager.ProspectPayload.IsPickedItemRemoved = quoteItems.Where(x => x.SampleUOM.Trim() != "loose" && x.SampleUOM.Trim() != "").ToList().Count != ds.Tables[0].Rows.Count ? true : false;    //CA1820: Test for empty strings using string length   --Vignesh D
                            PayloadManager.ProspectPayload.IsPickedItemRemoved = quoteItems.Where(x => x.SampleUOM.Trim() != "loose" && !String.IsNullOrEmpty(x.SampleUOM.Trim())).ToList().Count != ds.Tables[0].Rows.Count ? true : false;   //Added by Vignesh D
                        }
                    }
                    foreach (CustomerQuoteDetail item in quoteItems)
                    {
                        try
                        {
                            #region UOM Collection
                            //string uomQuery = "SELECT UOM as 'UMCode', 1 as 'UMMultipler' FROM BUSDTA.ItemUoMs  WHERE ITEMID='" + item.ItemId.Trim() + "' and  cansell=1 order by DisplaySeq";
                            string uomQuery = "CALL BUSDTA.SP_GetUomDetailsForQuote('" + item.ItemId.Trim() + "')";
                            List<QtyUOMClass> qtyUOM = new List<QtyUOMClass>();
                            DataSet resultUOM = DbEngine.ExecuteDataSet(uomQuery);
                            if (resultUOM.HasData())
                            {
                                qtyUOM = resultUOM.GetEntityList<QtyUOMClass>();

                            }
                            item.QtyUOMCollection = new ObservableCollection<QtyUOMClass>(qtyUOM);

                            item.SelectedQtyUOM = item.QtyUOMCollection.FirstOrDefault(x => x.UMCode == item.TransactionUOM);
                            item.QtyUOMCollectionSample = (item).QtyUOMCollection.Clone();
                            item.QtyUOMCollectionSample.Add(new QtyUOMClass() { UMCode = "loose", UMMultipler = 1 });
                            var ele = item.QtyUOMCollectionSample.FirstOrDefault(x => x.UMCode == item.SampleUOM.Trim());
                            if (ele != null)
                            {
                                item.SelectedQtyUOMSample = item.QtyUOMCollectionSample.FirstOrDefault(x => x.UMCode == item.SampleUOM.Trim());
                            }
                            else
                            {
                                item.SelectedQtyUOMSample = new QtyUOMClass();

                            }

                            #endregion

                            #region Set Conversion Factor
                            try
                            {
                                //Set conversion factor
                                List<QtyUOMClass> objRemove = new List<QtyUOMClass>();

                                foreach (QtyUOMClass x in item.QtyUOMCollectionSample)
                                {
                                    x.UMMultipler = new TemplateManager().SetConversionFactorForItem(item.PrimaryUOM, x.UMCode, item.ItemId, item.ItemNumber);
                                    if (x.UMMultipler > item.AvailableQty && x.UMCode != "loose")
                                    {
                                        objRemove.Add(x);
                                    }
                                }
                                foreach (var item1 in objRemove)
                                {
                                    item.QtyUOMCollectionSample.Remove(item1);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error("[SalesLogicExpress.Application.Managers][ItemManager][SearchItem][Set Conversion Factor Region][ExceptionStackTrace = " + ex.StackTrace + "]");

                            }

                            #endregion

                            #region set UOM visibility

                            if (!string.IsNullOrEmpty(item.TransactionUOM) && !(item.TransactionUOM.Equals(Convert.ToString(item.PricingUOM))))
                            {
                                item.IsVisibleTransactionUM = true;
                            }

                            if (!string.IsNullOrEmpty(item.OtherUOM) && !string.IsNullOrWhiteSpace(item.OtherUOM))
                            {
                                item.IsVisibleOtherUM = true;
                            }

                            item.MaxSampleQty = item.SelectedQtyUOMSample.UMCode == "loose" || string.IsNullOrEmpty(item.SelectedQtyUOMSample.UMCode) ? 9999 : Convert.ToInt32(item.AvailableQty / item.SelectedQtyUOMSample.UMMultipler);

                            //if (item.SampleUOM=="loose")
                            //{
                            //    item.PickedQty = item.SampleQty;
                            //}
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][GetQuoteItems][Set UM Conversion][ExceptionStackTrace = " + ex.ToString() + "]");
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][GetQuoteItems][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:GetQuoteItems]");
            return quoteItems;
        }

        /// <summary>
        /// Retrieves address info for customer 
        /// </summary>
        /// <param name="quoteId">Customer quote id</param>
        /// <param name="addressInfoType">Address information type</param>
        /// <returns>Address information</returns>
        public string GetAddressInfo(int quoteId, AddressInfo addressInfoType)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:GetAddressInfo]");
            StringBuilder addressInfoQueryBuilder = new StringBuilder();
            string addressInfo = string.Empty;
            string query = "";
            try
            {
                addressInfoQueryBuilder.Append("SELECT");

                if (quoteId == 0)
                {
                    switch (addressInfoType)
                    {
                        case AddressInfo.Parent:
                            addressInfoQueryBuilder.Append("\n(CASE ISNULL(o.MAPA8,'') WHEN '' THEN '' ELSE CAST(o.MAPA8 AS VARCHAR(20))+', '+ BUSDTA.GetABADescriptionFromABAId(o.MAPA8) END) AS 'Parent'");
                            break;
                        case AddressInfo.BillTo:
                            addressInfoQueryBuilder.Append("\n(CASE ISNULL(a.aban81,'') WHEN '' THEN '' ELSE CAST(a.aban81 AS VARCHAR(20))+', '+ BUSDTA.GetABADescriptionFromABAId(a.aban81) END) AS 'BillTo'");
                            break;
                    }

                    addressInfoQueryBuilder.Append("\nFROM busdta.F0101 a left join busdta.F0116 d ON a.aban8 =d.alan8");
                    addressInfoQueryBuilder.Append("\nleft join busdta.F0150 o ON a.ABAN8 = o.MAAN8");
                    addressInfoQueryBuilder.Append("\nleft  join busdta.F03012 c ON a.ABAN8 = c.AIAN8");
                    if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    {
                        addressInfoQueryBuilder.Append("\nWHERE a.aban81=" + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID);
                    }
                    else
                    {
                        //addressInfoQueryBuilder.Append("\nWHERE a.aban8=" + CommonNavInfo.Customer.CustomerNo);

                        switch (addressInfoType)
                        {
                            case AddressInfo.Parent:
                                query = "CALL BUSDTA.SP_GetAddressInfoForQuote(" + CommonNavInfo.Customer.CustomerNo + ")";

                                break;
                            case AddressInfo.BillTo:
                                query = "CALL BUSDTA.SP_GetAddressBillToInfoForQuote(" + CommonNavInfo.Customer.CustomerNo + ")";
                                break;
                        }
                    }
                }
                else
                {
                    switch (addressInfoType)
                    {
                        case AddressInfo.Parent:
                            addressInfoQueryBuilder.Append("\n(CASE ISNULL(ParentId,'') WHEN '' THEN '' ELSE CAST(ParentId AS VARCHAR(20))+', '+ BUSDTA.GetABADescriptionFromABAId(ParentId) END) AS 'Parent'");
                            break;
                        case AddressInfo.BillTo:
                            addressInfoQueryBuilder.Append("\n(CASE ISNULL(BillToId,'') WHEN '' THEN '' ELSE CAST(BillToId AS VARCHAR(20))+', '+ BUSDTA.GetABADescriptionFromABAId(BillToId) END) AS 'BillTo'");
                            break;
                    }

                    if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    {
                        addressInfoQueryBuilder.Append("\nFROM BUSDTA.Prospect_Quote_Header");
                        addressInfoQueryBuilder.Append("\nWHERE ProspectId=" + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID + "AND RouteId=" + CommonNavInfo.RouteID.ToString() + " AND ProspectQuoteId=" + quoteId);
                        query = addressInfoQueryBuilder.ToString();
                    }
                    else
                    {

                        switch (addressInfoType)
                        {
                            case AddressInfo.Parent:
                                query = "CALL BUSDTA.SP_GetAddressInfoForExistQuote(" + CommonNavInfo.Customer.CustomerNo + "," + CommonNavInfo.RouteID.ToString() + "," + quoteId + ")";
                                break;
                            case AddressInfo.BillTo:
                                query = "CALL BUSDTA.SP_GetAddressInfoBillToForExistQuote(" + CommonNavInfo.Customer.CustomerNo + "," + CommonNavInfo.RouteID.ToString() + "," + quoteId + ")";
                                break;
                        }
                    }
                    //query = addressInfoQueryBuilder.ToString();
                }

                //addressInfo = DbEngine.ExecuteScalar(addressInfoQueryBuilder.ToString());
                addressInfo = DbEngine.ExecuteScalar(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][GetAddressInfo][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:GetAddressInfo]");

            return addressInfo;
        }

        /// <summary>
        /// Retrives the pricing list 
        /// </summary>
        /// <param name="pricingDetailCategory">pricing category</param>
        /// <returns>List cotaining the category</returns>
        public ObservableCollection<PriceDetails> GetPricingList(PricingDetailCategory pricingDetailCategory)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:GetPricingList]");
            ObservableCollection<PriceDetails> categoryList = new ObservableCollection<PriceDetails>();

            try
            {
                //string query = "SELECT TRIM(DRKY) AS [Key], TRIM(DRDL01) AS [Description] FROM BUSDTA.F0005 WHERE DRSY='01' AND DRRT='" + Convert.ToByte(pricingDetailCategory).ToString() + "'";
                //query += " AND TRIM(ISNULL(DRKY,''))<>'' UNION SELECT '---' AS [Key], '' AS [Description] ORDER BY [Key] ";
                string query = "CALL BUSDTA.SP_GetPricingListForQuote('" + Convert.ToByte(pricingDetailCategory).ToString() + "')";
                DataSet result = DbEngine.ExecuteDataSet(query);

                if (result.HasData())
                {
                    //Resultset may contain data with blank keys and hence written the separate implementation 
                    foreach (DataRow row in result.Tables[0].Rows)
                    {
                        PriceDetails priceDetail = new PriceDetails();
                        priceDetail.Key = Convert.ToString(row["Key"]);
                        priceDetail.Description = Convert.ToString(row["Description"]);
                        priceDetail.IsVisible = true;
                        if (!string.IsNullOrEmpty(priceDetail.Key))
                        {
                            categoryList.Add(priceDetail);
                        }
                    }

                    //categoryList.Add(new PriceDetails() { Key ="---",Description="",IsVisible=true  });
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][GetPricingList][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:GetPricingList]");
            return categoryList;
        }

        /// <summary>
        /// Insert/update Squote data into database 
        /// </summary>
        /// <param name="quoteHeader">Customer quote header object</param>
        public bool SaveQuote(CustomerQuoteHeader quoteHeader)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:SaveQuote]");
            // StringBuilder queryString = new StringBuilder();   //Unused Local  --Vignesh D

            try
            {
                // If printed, do not proceed for update
                if (quoteHeader.IsPrinted)
                {
                    return true;
                }

                //If It is new quote generate New ID - Fixed by dinesh
                if (quoteHeader.IsNewQuote)
                {
                    string strNewQuoteId = string.Empty;

                    //Entity for prospect quote and customer quote is different - fixed by dinesh
                    if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                        strNewQuoteId = new Managers.NumberManager().GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.Prospect);
                    else
                        strNewQuoteId = new Managers.NumberManager().GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.Quote);

                    if (!string.IsNullOrEmpty(strNewQuoteId))
                    {
                        quoteHeader.QuoteId = (!string.IsNullOrEmpty(strNewQuoteId) && !strNewQuoteId.Equals("0") ? Convert.ToInt32(strNewQuoteId) : this.GetLastMaxQuoteId() + 1);
                        quoteHeader.CustomerId = ViewModelPayload.PayloadManager.ProspectPayload.IsProspect ? Convert.ToInt32(ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID) : Convert.ToInt32(CommonNavInfo.Customer.CustomerNo);
                        ViewModelPayload.PayloadManager.QuotePayload.QuoteId = quoteHeader.QuoteId.ToString();

                        if (!this.CreateHeader(quoteHeader))
                        {
                            //ToDo - throw exception as unable to created record 
                            return true;
                        }
                    }
                    else
                        return false;
                }

                this.UpdateHeader(quoteHeader);
                if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect && !quoteHeader.IsNewQuote)
                {
                    this.UpdatePricingSetup(quoteHeader.MasterPriceSetup, quoteHeader.QuoteId, PricingSetup.Master);
                }
                this.UpdatePricingSetup(quoteHeader.OverridePriceSetup, quoteHeader.QuoteId, PricingSetup.Override);

                this.SaveItems(quoteHeader.Items, quoteHeader.QuoteId);

                this.LogActivity(quoteHeader);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][SaveQuote][ExceptionStackTrace = " + ex.ToString() + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:SaveQuote]");
            return true;
        }

        /// <summary>
        /// Updates header information of the quote 
        /// </summary>
        /// <param name="quoteHeader">Customer quote header</param>
        public void UpdateHeader(CustomerQuoteHeader quoteHeader)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:UpdateHeader]");

            StringBuilder queryBuilder = new StringBuilder();
            //  string strStatusCode = string.Empty;    // Unused Local --Vignesh D
            int stringLength = 0;
            try
            {
                if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                {
                    int sampleQty = quoteHeader.Items.Sum(x => x.SampleQty);
                    quoteHeader.IsSampled = sampleQty > 0 ? true : false;
                    //ToDo - Get the status code id-
                    queryBuilder.Append("UPDATE BUSDTA.Prospect_Quote_Header SET ");
                    queryBuilder.Append("\nIsPrinted='" + Convert.ToByte(quoteHeader.IsPrinted) + "', ");
                    queryBuilder.Append("\nPriceDate=(CASE WHEN DATEDIFF( DAY, GetDate(), PriceDate)=0 AND IsPrinted<>'1' THEN getdate() ELSE pricedate END),");
                    queryBuilder.Append("\nIsSampled='" + Convert.ToByte(quoteHeader.IsSampled) + "' ");
                    queryBuilder.Append("\nWHERE RouteId=" + CommonNavInfo.RouteID.ToString() + " AND ProspectId=" + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID + " AND ProspectQuoteId=" + quoteHeader.QuoteId.ToString());
                }
                else
                {
                    //ToDo - Get the status code id-
                    queryBuilder.Append("UPDATE BUSDTA.Customer_Quote_Header SET ");

                    if (!string.IsNullOrEmpty(quoteHeader.BillTo))
                    {
                        stringLength = (quoteHeader.BillTo.IndexOf(',') < 0 ? 0 : quoteHeader.BillTo.IndexOf(','));
                    }

                    queryBuilder.Append((string.IsNullOrEmpty(quoteHeader.BillTo) ? "" : "\nBillToId=" + quoteHeader.BillTo.Substring(0, stringLength) + ", "));

                    if (!string.IsNullOrEmpty(quoteHeader.Parent))
                    {
                        stringLength = 0;
                        stringLength = (quoteHeader.Parent.IndexOf(',') < 0 ? 0 : quoteHeader.Parent.IndexOf(','));
                    }
                    queryBuilder.Append((string.IsNullOrEmpty(quoteHeader.Parent) ? "" : "\nParentId=" + quoteHeader.Parent.Substring(0, stringLength) + ", "));
                    queryBuilder.Append("\nIsPrinted='" + Convert.ToByte(quoteHeader.IsPrinted) + "', ");
                    queryBuilder.Append("\nPriceDate=(CASE WHEN DATEDIFF( DAY, GetDate(), PriceDate)=0 AND IsPrinted<>'1' THEN getdate() ELSE pricedate END),");

                    //Remove last character
                    if (queryBuilder.ToString().Substring(queryBuilder.ToString().Length - 1, 1).Equals(","))
                    {
                        queryBuilder.Remove(queryBuilder.Length - 1, 1);
                    }

                    queryBuilder.Append("\nWHERE RouteId=" + CommonNavInfo.RouteID.ToString() + " AND CustomerId=" + CommonNavInfo.Customer.CustomerNo + " AND CustomerQuoteId=" + quoteHeader.QuoteId.ToString());

                }

                //  int cnt = DbEngine.ExecuteNonQuery(queryBuilder.ToString());   // Unused Local --Vignesh D
                DbEngine.ExecuteNonQuery(queryBuilder.ToString());               // Added by Vignesh D
                if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    ResourceManager.QueueManager.QueueProcess("ProspectManagement", false);
                else
                    ResourceManager.QueueManager.QueueProcess("Quotes", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][UpdateHeader][ExceptionStackTrace = " + ex.ToString() + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:UpdateHeader]");

        }

        /// <summary>
        /// Create a new quote record
        /// </summary>
        /// <param name="quoteHeader">Customer quote header object</param>
        /// <returns>True if record created successfully, false as otherwise </returns>
        private bool CreateHeader(CustomerQuoteHeader quoteHeader)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:CreateHeader]");
            string queryString = string.Empty;
            bool IsRecordCreated = false;
            try
            {
                //string openStatusId = DbEngine.ExecuteScalar("SELECT StatusTypeId FROM BUSDTA.Status_Type WHERE StatusTypeCD='OPEN'");
                string openStatusId = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetCreateHeader()");
                if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                {
                    queryString = "INSERT INTO BUSDTA.Prospect_Quote_Header (ProspectQuoteId, ProspectId, RouteId, StatusId, PriceDate,SettlementId)  VALUES ({0}, {1}, {2}, {3}, getdate(),{4})";
                    queryString = string.Format(queryString, quoteHeader.QuoteId, ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID, CommonNavInfo.RouteID, openStatusId, "-1");
                }
                else
                {
                    string BillToId = "0";
                    string ParentId = "0";

                    if (!string.IsNullOrEmpty(quoteHeader.Parent))
                    {
                        if (quoteHeader.Parent.IndexOf(',') > 0)
                        {
                            ParentId = quoteHeader.Parent.Substring(0, quoteHeader.Parent.IndexOf(','));
                        }
                    }

                    if (!string.IsNullOrEmpty(quoteHeader.BillTo))
                    {
                        if (quoteHeader.BillTo.IndexOf(',') > 0)
                        {
                            BillToId = quoteHeader.BillTo.Substring(0, quoteHeader.BillTo.IndexOf(','));
                        }
                    }
                    queryString = "INSERT INTO BUSDTA.Customer_Quote_Header (CustomerQuoteId, CustomerId, RouteId, StatusId, BillToId, ParentId, PriceDate,SettlementId)  VALUES ({0}, {1}, {2}, {3}, {4}, {5}, getdate(),{6})";
                    queryString = string.Format(queryString, quoteHeader.QuoteId, quoteHeader.CustomerId, CommonNavInfo.RouteID, openStatusId, BillToId, ParentId, "-1");
                }


                //    int cnt = DbEngine.ExecuteNonQuery(queryString);   //Unused Local --Vignesh D
                DbEngine.ExecuteNonQuery(queryString);              // Added by Vignesh D
                if (PayloadManager.ProspectPayload.IsProspect)
                {
                    this.UpdatePricingSetup(quoteHeader.MasterPriceSetup, quoteHeader.QuoteId, PricingSetup.Master);
                }
                else
                {
                    this.UpdatePricingSetup(quoteHeader.MasterPriceSetup, quoteHeader.QuoteId, PricingSetup.Master);
                }

                IsRecordCreated = true;

                if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    ResourceManager.QueueManager.QueueProcess("ProspectManagement", false);
                else
                    ResourceManager.QueueManager.QueueProcess("Quotes", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][CreateHeader][ExceptionStackTrace = " + ex.ToString() + "]");
                IsRecordCreated = false;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:CreateHeader]");
            return IsRecordCreated;
        }

        /// <summary>
        /// Updates master pricing details into database 
        /// </summary>
        /// <param name="pricingDetail">Master pricing details</param>
        /// <param name="quoteHeaderId">Quote header id</param>
        /// <param name="pricingSetup">Pricing setup information</param>
        private void UpdatePricingSetup(PricingDetails pricingDetail, int quoteHeaderId, PricingSetup pricingSetup)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:UpdatePricingSetup]");

            StringBuilder queryBuilder = new StringBuilder();

            try
            {
                if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                {
                    queryBuilder.Append("UPDATE BUSDTA.Prospect_Quote_Header SET");
                }
                else
                {
                    queryBuilder.Append("UPDATE BUSDTA.Customer_Quote_Header SET");

                }

                switch (pricingSetup)
                {
                    case PricingSetup.Master:
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.AlliedProductLine1) ? "\nAIAC15CategoryCode15Mst='', " : "\nAIAC15CategoryCode15Mst='" + pricingDetail.AlliedProductLine1.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.AlliedProductLine2) ? "\nAIAC16CategoryCode16Mst='', " : "\nAIAC16CategoryCode16Mst='" + pricingDetail.AlliedProductLine2.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.AlliedProductLine3) ? "\nAIAC17CategoryCode17Mst='', " : "\nAIAC17CategoryCode17Mst='" + pricingDetail.AlliedProductLine3.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.AlliedProductLine4) ? "\nAIAC18CategoryCode18Mst='', " : "\nAIAC18CategoryCode18Mst='" + pricingDetail.AlliedProductLine4.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.POSUpCharge) ? "\nAIAC22POSUpChargeMst='' ," : "\nAIAC22POSUpChargeMst='" + pricingDetail.POSUpCharge.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.LiquidBracket) ? "\nAIAC23LiquidCoffeeMst='', " : "\nAIAC23LiquidCoffeeMst='" + pricingDetail.LiquidBracket.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.PriceProtection) ? "\nAIAC24PriceProtection='', " : "\nAIAC24PriceProtection='" + pricingDetail.PriceProtection.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.AlliedProductLineOverall) ? "\nAIAC27AlliedDiscountMst='', " : "\nAIAC27AlliedDiscountMst='" + pricingDetail.AlliedProductLineOverall.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.CoffVolume) ? "\nAIAC28CoffeeVolumeMst='', " : "\nAIAC28CoffeeVolumeMst='" + pricingDetail.CoffVolume.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.EquipProg) ? "\nAIAC29EquipmentProgPtsMst='', " : "\nAIAC29EquipmentProgPtsMst='" + pricingDetail.EquipProg.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.SpecialCCP) ? "\nAIAC30SpecialCCPMst='' " : "\nAIAC30SpecialCCPMst='" + pricingDetail.SpecialCCP.Trim() + "'"));
                        break;
                    case PricingSetup.Override:
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.AlliedProductLine1) ? "\nAIAC15CategoryCode15='', " : "\nAIAC15CategoryCode15='" + pricingDetail.AlliedProductLine1.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.AlliedProductLine2) ? "\nAIAC16CategoryCode16='', " : "\nAIAC16CategoryCode16='" + pricingDetail.AlliedProductLine2.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.AlliedProductLine3) ? "\nAIAC17CategoryCode17='', " : "\nAIAC17CategoryCode17='" + pricingDetail.AlliedProductLine3.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.AlliedProductLine4) ? "\nAIAC18CategoryCode18='', " : "\nAIAC18CategoryCode18='" + pricingDetail.AlliedProductLine4.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.POSUpCharge) ? "\nAIAC22POSUpCharge='', " : "\nAIAC22POSUpCharge='" + pricingDetail.POSUpCharge.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.LiquidBracket) ? "\nAIAC23LiquidCoffee='', " : "\nAIAC23LiquidCoffee='" + pricingDetail.LiquidBracket.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.PriceProtection) ? "\nAIAC24PriceProtection='', " : "\nAIAC24PriceProtection='" + pricingDetail.PriceProtection.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.AlliedProductLineOverall) ? "\nAIAC27AlliedDiscount='', " : "\nAIAC27AlliedDiscount='" + pricingDetail.AlliedProductLineOverall.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.CoffVolume) ? "\nAIAC28CoffeeVolume='', " : "\nAIAC28CoffeeVolume='" + pricingDetail.CoffVolume.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.EquipProg) ? "\nAIAC29EquipmentProgPts='', " : "\nAIAC29EquipmentProgPts='" + pricingDetail.EquipProg.Trim() + "',"));
                        queryBuilder.Append((string.IsNullOrEmpty(pricingDetail.SpecialCCP) ? "\nAIAC30SpecialCCP='' " : "\nAIAC30SpecialCCP='" + pricingDetail.SpecialCCP.Trim() + "'"));
                        break;
                    default:
                        break;
                }

                //Remove last character
                //if (queryBuilder.ToString().Substring(queryBuilder.ToString().Length - 1, 1).Equals(","))
                //{
                //    queryBuilder.Remove(queryBuilder.Length - 1, 1);
                //}
                if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                {
                    queryBuilder.Append("\nWHERE RouteId=" + CommonNavInfo.RouteID.ToString() + " AND ProspectId=" + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID + " AND ProspectQuoteId=" + quoteHeaderId);

                }
                else
                {
                    queryBuilder.Append("\nWHERE RouteId=" + CommonNavInfo.RouteID.ToString() + " AND CustomerId=" + CommonNavInfo.Customer.CustomerNo + " AND CustomerQuoteId=" + quoteHeaderId);


                }

                string query = queryBuilder.ToString();
                //  int count = DbEngine.ExecuteNonQuery(query);   // Unused local --Vignesh D
                DbEngine.ExecuteNonQuery(query);            // Added by Vignesh D

                if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    ResourceManager.QueueManager.QueueProcess("ProspectManagement", false);
                else
                    ResourceManager.QueueManager.QueueProcess("Quotes", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][UpdatePricingSetup][ExceptionStackTrace = " + ex.ToString() + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:UpdatePricingSetup]");
        }

        /**/
        /// <summary>
        /// Insert/updates the quote details into database 
        /// </summary>
        /// <param name="quoteDetails">Quote items</param>
        /// <param name="QuoteHeaderId">Quote header id</param>
        private void SaveItems(ObservableCollection<CustomerQuoteDetail> quoteDetails, int QuoteHeaderId)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:SaveItems]");

            ItemManager itmngr = new ItemManager();
            StringBuilder queryBuilder = new StringBuilder();
            try
            {
                //ToDo - Write code to check if items collection has any data exists 
                if (quoteDetails.Count == 0)
                {
                    return;
                }

                //Delete all items, it is done to keep items as visible on screen . 
                if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                {
                    DbEngine.ExecuteNonQuery("DELETE FROM BUSDTA.Prospect_Quote_Detail WHERE ProspectQuoteId=" + QuoteHeaderId);
                }
                else
                {
                    DbEngine.ExecuteNonQuery("DELETE FROM BUSDTA.Customer_Quote_Detail WHERE CustomerQuoteId=" + QuoteHeaderId);
                }

                for (int i = 0; i < quoteDetails.Count; i++)
                {
                    quoteDetails[i].QuoteHeaderId = QuoteHeaderId;




                    queryBuilder.Clear();
                    if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    {
                        queryBuilder.Append("INSERT INTO BUSDTA.Prospect_Quote_Detail(");
                        queryBuilder.Append("\nProspectQuoteId, ProspectQuoteDetailId,");
                        queryBuilder.Append("\nRouteId, ItemId,");
                        queryBuilder.Append("\nQuoteQty, TransactionUM,");
                        queryBuilder.Append("\nPricingAmt, PricingUM,");
                        queryBuilder.Append("\nTransactionAmt, OtherAmt,");
                        queryBuilder.Append("\nOtherUM, CompetitorName,");
                        queryBuilder.Append("\nUnitPriceAmt, UnitPriceUM,");
                        queryBuilder.Append("\nAvailableQty,  SampleQty, SampleUM)");
                        queryBuilder.Append("\nVALUES (");
                        queryBuilder.Append("\n" + quoteDetails[i].QuoteHeaderId.ToString() + ", " + quoteDetails[i].QuoteDetailId.ToString());
                        queryBuilder.Append("\n, " + CommonNavInfo.RouteID.ToString() + ", " + quoteDetails[i].ItemId);
                        queryBuilder.Append("\n, " + quoteDetails[i].QuoteQuantity + ", '" + quoteDetails[i].SelectedQtyUOM.UMCode + "'");
                        queryBuilder.Append("\n, " + quoteDetails[i].PricingUOMPrice + ", '" + quoteDetails[i].PricingUOM + "'");
                        queryBuilder.Append("\n, '" + quoteDetails[i].TransactionUOMPrice + "', " + quoteDetails[i].OtherUOMPrice);
                        queryBuilder.Append("\n, '" + quoteDetails[i].OtherUOM + "', '" + quoteDetails[i].CompetitorName + "'");
                        queryBuilder.Append("\n, " + quoteDetails[i].CompetitorPrice + ", '" + quoteDetails[i].CompetitorUOM + "' , '" + (itmngr.ValidateNonStockItem(quoteDetails[i].ItemNumber) != false ? 0 : quoteDetails[i].AvailableQty) + "'");
                        queryBuilder.Append("\n, " + quoteDetails[i].SampleQty + ", '" + (quoteDetails[i].SelectedQtyUOMSample != null ? quoteDetails[i].SelectedQtyUOMSample.UMCode : "") + "' ) ");
                    }
                    else
                    {

                        queryBuilder.Append("INSERT INTO BUSDTA.Customer_Quote_Detail(");
                        queryBuilder.Append("\nCustomerQuoteId, CustomerQuoteDetailId,");
                        queryBuilder.Append("\nRouteId, ItemId,");
                        queryBuilder.Append("\nQuoteQty, TransactionUM,");
                        queryBuilder.Append("\nPricingAmt, PricingUM,");
                        queryBuilder.Append("\nTransactionAmt, OtherAmt,");
                        queryBuilder.Append("\nOtherUM, CompetitorName,");
                        queryBuilder.Append("\nUnitPriceAmt, UnitPriceUM)");
                        queryBuilder.Append("\nVALUES (");
                        queryBuilder.Append("\n" + quoteDetails[i].QuoteHeaderId.ToString() + ", " + quoteDetails[i].QuoteDetailId.ToString());
                        queryBuilder.Append("\n, " + CommonNavInfo.RouteID.ToString() + ", " + quoteDetails[i].ItemId);
                        queryBuilder.Append("\n, " + quoteDetails[i].QuoteQuantity + ", '" + quoteDetails[i].SelectedQtyUOM.UMCode + "'");
                        queryBuilder.Append("\n, " + quoteDetails[i].PricingUOMPrice + ", '" + quoteDetails[i].PricingUOM + "'");
                        queryBuilder.Append("\n, " + quoteDetails[i].TransactionUOMPrice + ", " + quoteDetails[i].OtherUOMPrice);
                        queryBuilder.Append("\n, '" + quoteDetails[i].OtherUOM + "', '" + quoteDetails[i].CompetitorName + "'");
                        queryBuilder.Append("\n, " + quoteDetails[i].CompetitorPrice + ", '" + quoteDetails[i].CompetitorUOM + "')");
                    }

                    //    int cnt = DbEngine.ExecuteNonQuery(queryBuilder.ToString());    // Unused Local --Vignesh D
                    DbEngine.ExecuteNonQuery(queryBuilder.ToString());
                }

                if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    ResourceManager.QueueManager.QueueProcess("ProspectManagement", false);
                else
                    ResourceManager.QueueManager.QueueProcess("Quotes", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][SaveItems][ExceptionStackTrace = " + ex.ToString() + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:SaveItems]");
        }

        /// <summary>
        /// Checks if the specifed detail exists 
        /// Now this function is unused 
        /// </summary>
        /// <param name="details">Customer quote detail</param>
        /// <returns>True- if record exists, false as otherwise</returns>
        /// 

        // Unused function commented for performance   --Vignesh D

        //private bool QuoteDetailExists(CustomerQuoteDetail details)
        //{
        //    bool IsRecordExists = false;
        //    int itemCount = 0;

        //    try
        //    {
        //        string query = string.Empty;
        //        if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
        //        {
        //            query = "SELECT COUNT(*) FROM BUSDTA.Prospect_Quote_Detail WHERE RouteId=" + CommonNavInfo.RouteID.ToString();
        //            query = query + " AND ProspectQuoteId=" + details.QuoteHeaderId.ToString() + " AND ProspectQuoteDetailId=" + details.QuoteDetailId.ToString();
        //        }
        //        else
        //        {
        //            query = "SELECT COUNT(*) FROM BUSDTA.Customer_Quote_Detail WHERE RouteId=" + CommonNavInfo.RouteID.ToString();
        //            query = query + " AND CustomerQuoteId=" + details.QuoteHeaderId.ToString() + " AND CustomerQuoteDetailId=" + details.QuoteDetailId.ToString();
        //        }


        //        itemCount = Convert.ToInt16(DbEngine.ExecuteScalar(query));

        //        if (itemCount > 0)
        //        {
        //            IsRecordExists = true;
        //        }
        //        else
        //        {
        //            IsRecordExists = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][QuoteDetailExists][ExceptionStackTrace = " + ex.ToString() + "]");
        //        IsRecordExists = false;
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:QuoteDetailExists]");

        //    return IsRecordExists;
        //}
        /// <summary>
        /// Retrieves last maximum quote id 
        /// </summary>
        /// <returns>Last maximum quote id </returns>
        private int GetLastMaxQuoteId()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:GetLastMaxQuoteId]");
            int lastQuoteId = 0;

            try
            {
                string strQuoteId = string.Empty;
                if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                {
                    //strQuoteId = DbEngine.ExecuteScalar("SELECT MAX(ProspectQuoteId) AS QuoteId FROM BUSDTA.Prospect_Quote_Header WHERE RouteId=" + CommonNavInfo.RouteID.ToString());
                    strQuoteId = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetMaxQuoteId (@RouteId='" + CommonNavInfo.RouteID.ToString() + "',@IsProspect=2)");
                }
                else
                {
                    //strQuoteId = DbEngine.ExecuteScalar("SELECT MAX(CustomerQuoteId) AS QuoteId FROM BUSDTA.Customer_Quote_Header WHERE RouteId=" + CommonNavInfo.RouteID.ToString());
                    strQuoteId = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetMaxQuoteId (@RouteId='" + CommonNavInfo.RouteID.ToString() + "',@IsProspect=1)");
                }

                lastQuoteId = Convert.ToInt16(string.IsNullOrEmpty(strQuoteId) ? "0" : strQuoteId);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][GetLastMaxQuoteId][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:GetLastMaxQuoteId]");
            return lastQuoteId;
        }

        private void LogActivity(CustomerQuoteHeader quote)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:LogActivity]");
            try
            {

                QuoteActivityListItem quoteItem = new QuoteActivityListItem
                {
                    QuoteId = quote.QuoteId.ToString(),
                    Invoice = quote.QuoteId.ToString(),
                    Date = quote.QuoteDate,
                    ItemsCount = quote.Items.Count,
                    IsPrinted = quote.IsPrinted,
                    IsProspect = PayloadManager.ProspectPayload.IsProspect,
                    ProspectOrCustId = PayloadManager.ProspectPayload.IsProspect ?
                        PayloadManager.ProspectPayload.Prospect.ProspectID : quote.CustomerId.ToString()
                };

                PricingDetails priceDetail = quote.OverridePriceSetup;
                if (priceDetail != null)
                {

                    if (priceDetail.IsAlliedProductLine1Overriden || priceDetail.IsAlliedProductLine1Overriden || priceDetail.IsAlliedProductLine3Overriden
                        || priceDetail.IsAlliedProductLine4Overriden || priceDetail.IsAlliedProductLineOverallOverriden
                        || priceDetail.IsCoffVolumeOverriden || priceDetail.IsEquipProgOverriden || priceDetail.IsLiquidBracketOverriden
                        || priceDetail.IsPOSUpChargeOverriden || priceDetail.IsSpecialCCPOverriden)
                    {
                        quoteItem.PriceSetup = PricingSetup.Override.ToString();
                    }
                    else
                    {
                        quoteItem.PriceSetup = PricingSetup.Master.ToString();
                    }
                }
                SalesLogicExpress.Domain.Activity ac = new SalesLogicExpress.Domain.Activity();
                if (PayloadManager.ProspectPayload.IsProspect)
                {

                    ac = new SalesLogicExpress.Domain.Activity
                    {
                        CustomerID = quote.CustomerId.ToString(),
                        RouteID = CommonNavInfo.RouteUser.ToString(),                   //vivensas change to take the route user and pass to the logactivity
                        StopInstanceID = PayloadManager.ProspectPayload.Prospect.StopID,
                        ActivityType = ActivityKey.Quote.ToString(),
                        ActivityStart = DateTime.Now,
                        ActivityEnd = DateTime.Now,
                        IsTxActivity = true,
                        ActivityDetailClass = typeof(QuoteActivityListItem).FullName

                    };
                }
                else
                {
                    ac = new SalesLogicExpress.Domain.Activity
                   {
                       CustomerID = quote.CustomerId.ToString(),
                       RouteID = CommonNavInfo.RouteUser.ToString(),                    //vivensas change to take the route user and pass to the logactivity
                       StopInstanceID = CommonNavInfo.Customer.StopID,
                       ActivityType = ActivityKey.Quote.ToString(),
                       ActivityStart = DateTime.Now,
                       ActivityEnd = DateTime.Now,
                       IsTxActivity = true,
                       ActivityDetailClass = typeof(QuoteActivityListItem).FullName
                   };
                }

                //quoteItem.ProspectPayload = PayloadManager.ProspectPayload;
                //quoteItem.QuotePayload = PayloadManager.QuotePayload;

                ac.ActivityDetails = quoteItem.SerializeToJson();

                if (!quote.IsNewQuote || quote.IsPrinted)
                {
                    //Get the customer activity 
                    //string headerId = DbEngine.ExecuteScalar("SELECT TDID FROM busdta.M50012 WHERE TDREFHDRId='" + quote.QuoteId + "' AND TDTYP='Quote' AND TDROUT='" + CommonNavInfo.RouteID.ToString() + "' AND TDPNTID=0 AND TDAN8=" + (PayloadManager.ProspectPayload.IsProspect ? PayloadManager.ProspectPayload.Prospect.ProspectID : CommonNavInfo.Customer.CustomerNo));
                    string headerId = "";
                    string TDROUT = "FBM" + Managers.UserManager.UserRoute.ToString();
                    if (PayloadManager.ProspectPayload.IsProspect)
                    {
                        //headerId = DbEngine.ExecuteScalar("SELECT TDID FROM busdta.M50012 WHERE TDREFHDRId='" + quote.QuoteId + "' AND TDTYP='Quote' AND TDROUT='" + CommonNavInfo.RouteID.ToString() + "' AND TDPNTID=0 AND TDAN8=" + (PayloadManager.ProspectPayload.IsProspect ? PayloadManager.ProspectPayload.Prospect.ProspectID : CommonNavInfo.Customer.CustomerNo));                        
                        // Prospect Duplicate Activity - Remove (12/12/2016)
                        headerId = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetLogActivityOfCustomerQuote('" + quote.QuoteId + "','" + TDROUT + "'," + PayloadManager.ProspectPayload.Prospect.ProspectID + ")");

                    }
                    else
                    {
                        // Customer Duplicate Activity - Remove (12/12/2016)
                        headerId = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetLogActivityOfCustomerQuote('" + quote.QuoteId + "','" + TDROUT + "'," + CommonNavInfo.Customer.CustomerNo + ")");

                    }

                    ac.ActivityHeaderID = headerId;
                }

                SalesLogicExpress.Domain.Activity a = ResourceManager.Transaction.LogActivity(ac);
                ViewModelPayload.PayloadManager.PaymentPayload.TransactionID = a.ActivityHeaderID;

                //  int resultCount = DbEngine.ExecuteNonQuery("UPDATE busdta.M50012 SET TDREFHDRID='" + quote.QuoteId.ToString() + "' WHERE TDID=" + a.ActivityHeaderID);   //Unused Local --Vignesh D
                DbEngine.ExecuteNonQuery("UPDATE busdta.M50012 SET TDREFHDRID='" + quote.QuoteId.ToString() + "' WHERE TDID=" + a.ActivityHeaderID);      // Added by Vignesh D
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][LogActivity][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:LogActivity]");
        }

        private void UpdateQuoteWithCustomerPriceSetup(string quoteId)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:UpdateQuoteWithCustomerPriceSetup]");
            StringBuilder queryBuilder = new StringBuilder();

            try
            {
                queryBuilder.Append("UPDATE BUSDTA.Customer_Quote_Header SET");
                queryBuilder.Append("\nCQH.AIAC15CategoryCode15Mst=CUST.AIAC15");
                queryBuilder.Append("\n,CQH.AIAC16CategoryCode16Mst=CUST.AIAC16");
                queryBuilder.Append("\n,CQH.AIAC17CategoryCode17Mst=CUST.AIAC17");
                queryBuilder.Append("\n,CQH.AIAC18CategoryCode18Mst=CUST.AIAC18");
                queryBuilder.Append("\n,CQH.AIAC22POSUpChargeMst=CUST.AIAC22");
                queryBuilder.Append("\n,CQH.AIAC23LiquidCoffeeMst=CUST.AIAC23");
                queryBuilder.Append("\n,CQH.AIAC24PriceProtectionMst=CUST.AIAC24");
                queryBuilder.Append("\n,CQH.AIAC27AlliedDiscountMst=CUST.AIAC27");
                queryBuilder.Append("\n,CQH.AIAC28CoffeeVolumeMst=CUST.AIAC28");
                queryBuilder.Append("\n,CQH.AIAC29EquipmentProgPtsMst=CUST.AIAC29");
                queryBuilder.Append("\n,CQH.AIAC30SpecialCCPMst=CUST.AIAC30");
                queryBuilder.Append("\nFROM BUSDTA.F03012 CUST INNER JOIN BUSDTA.Customer_Quote_Header CQH ON CUST.AIAN8=CQH.CustomerId");
                queryBuilder.Append("\nWHERE CQH.RouteId={0} AND CQH.CustomerQuoteId={1} AND CQH.IsPrinted='0' AND (getdate()-PriceDate)=0");

                //   int resultCount = DbEngine.ExecuteNonQuery(string.Format(queryBuilder.ToString(), CommonNavInfo.RouteID.ToString(), quoteId));     // Unused Local --Vignesh D
                DbEngine.ExecuteNonQuery(string.Format(queryBuilder.ToString(), CommonNavInfo.RouteID.ToString(), quoteId));                         // Added by Vignesh D
                ResourceManager.QueueManager.QueueProcess("Quotes", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][UpdateQuoteWithCustomerPriceSetup][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:UpdateQuoteWithCustomerPriceSetup]");
        }

        public void UpdateIsPrinted(CustomerQuoteHeader customerQuote)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:UpdateIsPrinted]");

            //   StringBuilder queryBuilder = new StringBuilder();         //Unused Local --Vignesh D
            //   string strStatusCode = string.Empty;           //Unused Local --Vignesh D   
            try
            {
                string query = string.Empty;
                if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                {
                    query = "UPDATE BUSDTA.Prospect_Quote_Header SET IsPrinted='" + Convert.ToByte(true) + "',PRICEDATE=(CASE WHEN DATEDIFF( DAY, GetDate(), PriceDate)=0 THEN getdate() ELSE pricedate END)  where ProspectQuoteId ='" + ViewModelPayload.PayloadManager.QuotePayload.QuoteId + "' and  " +
                               "ProspectId=" + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID;
                    customerQuote.CustomerId = int.Parse(ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID);
                }
                else
                {
                    query = "UPDATE BUSDTA.Customer_Quote_Header SET IsPrinted='" + Convert.ToByte(true) + "' ,PRICEDATE=(CASE WHEN DATEDIFF( DAY, GetDate(), PriceDate)=0 THEN getdate() ELSE pricedate END) where CustomerQuoteId ='" + ViewModelPayload.PayloadManager.QuotePayload.QuoteId + "' and  " +
                               "CustomerId=" + CommonNavInfo.Customer.CustomerNo;
                }


                //    int cnt = DbEngine.ExecuteNonQuery(query);         //Unused Local --Vignesh D
                DbEngine.ExecuteNonQuery(query);                         // Added by Vignesh D
                customerQuote.IsPrinted = true;

                // No Log - Activity For Print Only Update
                this.LogActivity(customerQuote);

                if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    ResourceManager.QueueManager.QueueProcess("ProspectManagement", false);
                else
                    ResourceManager.QueueManager.QueueProcess("Quotes", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][UpdateIsPrinted][ExceptionStackTrace = " + ex.ToString() + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:UpdateIsPrinted]");
        }

        public void SettleQuotes(string settlementId, string quoteId, bool isProspect)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:SettleQuotes]");

            StringBuilder queryBuilder = new StringBuilder();
            //  string strStatusCode = string.Empty;  // Unused Local --Vignesh D
            try
            {

                if (string.IsNullOrEmpty(settlementId) || string.IsNullOrEmpty(quoteId))
                {
                    return;
                }
                //ToDo - Get the status Id 
                // string settledStatusId = DbEngine.ExecuteScalar("SELECT StatusTypeId FROM BUSDTA.Status_Type WHERE StatusTypeCD='STTLD'");
                string settledStatusId = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='STTLD')");
                if (isProspect)
                {
                    queryBuilder.Append("UPDATE BUSDTA.Prospect_Quote_Header SET SettlementId=" + settlementId + ", StatusId=" + settledStatusId);
                    queryBuilder.Append("\nWHERE ProspectQuoteId IN (" + quoteId + ")");
                    queryBuilder.Append("\nAND StatusId <> " + settledStatusId);
                }
                else
                {
                    queryBuilder.Append("UPDATE BUSDTA.Customer_Quote_Header SET SettlementId=" + settlementId + ", StatusId=" + settledStatusId);
                    queryBuilder.Append("\nWHERE CustomerQuoteId IN (" + quoteId + ")");
                    queryBuilder.Append("\nAND StatusId <> " + settledStatusId);
                }

                //  int cnt = DbEngine.ExecuteNonQuery(queryBuilder.ToString());       //Unused Local --Vignesh D
                DbEngine.ExecuteNonQuery(queryBuilder.ToString());                   // Added by Vignesh D
                if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    ResourceManager.QueueManager.QueueProcess("ProspectManagement", false);
                else
                    ResourceManager.QueueManager.QueueProcess("Quotes", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][SettleQuotes][ExceptionStackTrace = " + ex.ToString() + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:SettleQuotes]");
        }

        public int GetAvailableQtyForItem(string itemID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][Start:GetAvailableQtyForItem][itemID=" + itemID + "]");
            int qty = 0;

            try
            {
                //string query = "select (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty' " +
                //" from busdta.Inventory inventory where inventory.ItemNumber=" + itemID + "";

                //qty = Convert.ToInt32(DbEngine.ExecuteScalar(query));
                qty = Convert.ToInt32(DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetAvailableQtyForItemAtQuote('" + itemID + "')"));
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TemplateManager][GetAvailableQtyForItem][itemID=" + itemID + "][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][End:GetAvailableQtyForItem][itemID=" + itemID + "]");
            return qty;


        }
        #endregion

        #region Pricing Methods

        /// <summary>
        /// Applies Pricing algorithm to Items in templates based on OrderQuantity and Unit of measure
        /// </summary>
        /// <param name="templateItems">ObservableCollection of Template Items</param>
        /// <param name="routeBranch">Route branch</param>
        /// <param name="shipToCustomerID">Invoice No to whom the items are to be delivered</param>
        /// <param name="ApplyNewPricing">Optional parameter, used only when the price is overriden in prospect quote</param>
        /// <returns>ObservableCollection of Order items</returns>
        public SalesLogicExpress.Application.Helpers.TrulyObservableCollection<OrderItem> ApplyPricingToTemplates(ObservableCollection<CustomerQuoteDetail> templateItems, string routeBranch, string shipToCustomerID, PricingDetails overridePriceDetails, bool ApplyNewPricing = false)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][Start:ApplyPricingToTemplates][routeBranch=" + routeBranch + ",shipToCustomerID=" + shipToCustomerID + "]");
            SalesLogicExpress.Application.Helpers.TrulyObservableCollection<OrderItem> orderItemFromTemplate = new SalesLogicExpress.Application.Helpers.TrulyObservableCollection<OrderItem>();
            StringBuilder queryBuilder = new StringBuilder();

            try
            {
                decimal unitPrice = 0;
                // Random randomProvider = new Random();       //Unused local --Vignesh D
                QuotePricingManager pricingManager = new QuotePricingManager();
                //   Random qtyOnHand = new Random();        //Unused local --Vignesh D
                foreach (QuoteDetail item in templateItems)
                {
                    if (item.QuoteQuantity <= 0)
                    {
                        continue;
                    }
                    unitPrice = 0;

                    //Clear query string 
                    queryBuilder.Clear();

                    // Check whether the price is to be calculated for Customer or Prospect
                    if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    {
                        // Query for Prospect Quote process Pricing
                        //queryBuilder.Append("SELECT [ProspectId] AS AIAN8 ");
                        //queryBuilder.Append("\n  ,[District] AS AIAC05 ");
                        //queryBuilder.Append("\n  ,[Region] AS AIAC06 ");
                        //queryBuilder.Append("\n  ,[Reporting] AS AIAC07 ");
                        //queryBuilder.Append("\n  ,[Chain] AS AIAC08 ");
                        //queryBuilder.Append("\n  ,[BrewmaticAgentCode] AS AIAC09 ");
                        //queryBuilder.Append("\n  ,[NTR] AS AIAC10 ");
                        //queryBuilder.Append("\n  ," + (ApplyNewPricing ? "'" + Convert.ToString(overridePriceDetails.TaxGroup) + "'" : "[ProspectTaxGrp]") + " AS AIAC11 ");
                        //queryBuilder.Append("\n  ,[CategoryCode12] AS AIAC12 ");
                        //queryBuilder.Append("\n  ,[APCheckCode] AS AIAC13 ");
                        //queryBuilder.Append("\n  ,[CategoryCode14] AS AIAC14 ");
                        //queryBuilder.Append("\n  ," + (ApplyNewPricing ? "'"+Convert.ToString(overridePriceDetails.AlliedProductLine1)+"'" : "[CategoryCode15]") + " AS AIAC15 ");
                        //queryBuilder.Append("\n  ," + (ApplyNewPricing ?  "'"+Convert.ToString(overridePriceDetails.AlliedProductLine2)+"'" : "[CategoryCode16]") + " AS AIAC16 ");
                        //queryBuilder.Append("\n  ," + (ApplyNewPricing ?  "'"+Convert.ToString(overridePriceDetails.AlliedProductLine3)+"'" : "[CategoryCode17]") + " AS AIAC17 ");
                        //queryBuilder.Append("\n  ," + (ApplyNewPricing ?  "'"+Convert.ToString(overridePriceDetails.AlliedProductLine4)+"'" : "[CategoryCode18]") + " AS AIAC18 ");
                        //queryBuilder.Append("\n  ,[CategoryCode19] AS AIAC19 ");
                        //queryBuilder.Append("\n  ,[CategoryCode20] AS AIAC20 ");
                        //queryBuilder.Append("\n  ,[CategoryCode21] AS AIAC21 ");
                        //queryBuilder.Append("\n  ," + (ApplyNewPricing ?  "'"+Convert.ToString(overridePriceDetails.AlliedProductLineOverall)+"'" : "[AlliedDiscount]") + " AS AIAC27 ");
                        //queryBuilder.Append("\n  ," + (ApplyNewPricing ?  "'"+Convert.ToString(overridePriceDetails.CoffVolume)+"'" : "[CoffeeVolume]") + " AS AIAC28 ");
                        //queryBuilder.Append("\n  ," + (ApplyNewPricing ?  "'"+Convert.ToString(overridePriceDetails.EquipProg)+"'" : "[EquipmentProgPts]") + " AS AIAC29 ");
                        //queryBuilder.Append("\n  ," + (ApplyNewPricing ?  "'"+Convert.ToString(overridePriceDetails.LiquidBracket)+"'" : "[LiquidCoffee]") + " AS AIAC23 ");
                        //queryBuilder.Append("\n  ," + (ApplyNewPricing ?  "'"+Convert.ToString(overridePriceDetails.PriceProtection)+"'" : "[PriceProtection]") + " AS AIAC24 ");
                        //queryBuilder.Append("\n  ," + (ApplyNewPricing ?  "'"+Convert.ToString(overridePriceDetails.POSUpCharge)+"'" : "[POSUpCharge]") + " AS AIAC22 ");
                        //queryBuilder.Append("\n  ," + (ApplyNewPricing ?  "'"+Convert.ToString(overridePriceDetails.SpecialCCP)+"'" : "[SpecialCCP]") + " AS AIAC30 ");
                        //queryBuilder.Append("\n  ,[TaxGroup] AS AITXA1 ");
                        //queryBuilder.Append("\n   FROM [BUSDTA].[Prospect_Master]");
                        //queryBuilder.Append("\n   where [ProspectId] = " + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID);

                        int lnIsApplyNewPricing = 0;
                        if (ApplyNewPricing) lnIsApplyNewPricing = 1;
                        string Query = "CALL BUSDTA.SP_GetApplyPricingToTemplatesForProspect(" + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID + "," + lnIsApplyNewPricing + ",'" + Convert.ToString(overridePriceDetails.TaxGroup) + "','" + Convert.ToString(overridePriceDetails.AlliedProductLine1) + "','" + Convert.ToString(overridePriceDetails.AlliedProductLine2) + "','" + Convert.ToString(overridePriceDetails.AlliedProductLine3) + "','" + Convert.ToString(overridePriceDetails.AlliedProductLine4) + "','" + Convert.ToString(overridePriceDetails.AlliedProductLineOverall) + "','" + Convert.ToString(overridePriceDetails.CoffVolume) + "','" + Convert.ToString(overridePriceDetails.EquipProg) + "','" + Convert.ToString(overridePriceDetails.LiquidBracket) + "','" + Convert.ToString(overridePriceDetails.PriceProtection) + "','" + Convert.ToString(overridePriceDetails.POSUpCharge) + "','" + Convert.ToString(overridePriceDetails.SpecialCCP) + "')";
                        queryBuilder.Append(Query);
                    }
                    else
                    {
                        // Query for Customer Quote process Pricing. UNION is done to combine the ShipTo overridden values with BillTo and Parent.
                        //The UNION part of the query is in QuotePricingManager.cs
                        //The UNION is responsible for combinations of BillTo/ShipTo/Parent of the Customer.
                        //queryBuilder.Append("SELECT AB.*,  AIAN8 AS [AIAN8]");
                        //queryBuilder.Append("\n, AICO AS [AICO], AIMCUR AS [AIMCUR], AITXA1 AS [AITXA1], AIEXR1 AS [AIEXR1]");
                        //queryBuilder.Append("\n, AIACL AS [AIACL], AIHDAR AS [AIHDAR], AITRAR AS [AITRAR], AISTTO AS [AISTTO]");
                        //queryBuilder.Append("\n, AIRYIN AS [AIRYIN], AISTMT AS [AISTMT], AIARPY AS [AIARPY], AISITO AS [AISITO]");
                        //queryBuilder.Append("\n, AICYCN AS [AICYCN], AIBO AS [AIBO], AITSTA AS [AITSTA], AICKHC AS [AICKHC]");
                        //queryBuilder.Append("\n, AIDLC AS [AIDLC], AIDNLT AS [AIDNLT], AIPLCR AS [AIPLCR], AIRVDJ AS [AIRVDJ]");
                        //queryBuilder.Append("\n, AIDSO AS [AIDSO], AICMGR AS [AICMGR], AICLMG AS [AICLMG], AIAB2 AS [AIAB2]");
                        //queryBuilder.Append("\n, AIDT1J AS [AIDT1J], AIDFIJ AS [AIDFIJ], AIDLIJ AS [AIDLIJ], AIDLP AS [AIDLP]");
                        //queryBuilder.Append("\n, AIASTY AS [AIASTY], AISPYE AS [AISPYE], AIAHB AS [AIAHB], AIALP AS [AIALP]");
                        //queryBuilder.Append("\n, AIABAM AS [AIABAM], AIABA1 AS [AIABA1], AIAPRC AS [AIAPRC], AIMAXO AS [AIMAXO]");
                        //queryBuilder.Append("\n, AIMINO AS [AIMINO], AIOYTD AS [AIOYTD], AIOPY AS [AIOPY], AIPOPN AS [AIPOPN]");
                        //queryBuilder.Append("\n, AIDAOJ AS [AIDAOJ], AIAN8R AS [AIAN8R], AIBADT AS [AIBADT], AICPGP AS [AICPGP]");
                        //queryBuilder.Append("\n, AIORTP AS [AIORTP], AITRDC AS [AITRDC], AIINMG AS [AIINMG] ");
                        //queryBuilder.Append("\n, AIHOLD AS [AIHOLD], AIROUT AS [AIROUT], AISTOP AS [AISTOP], AIZON AS [AIZON]");
                        //queryBuilder.Append("\n, AICARS AS [AICARS], AIDEL1 AS [AIDEL1], AIDEL2 AS [AIDEL2], AILTDT AS [AILTDT]");
                        //queryBuilder.Append("\n, AIFRTH AS [AIFRTH], AIAFT AS [AIAFT], AIAPTS AS [AIAPTS], AISBAL AS [AISBAL]");
                        //queryBuilder.Append("\n, AIBACK AS [AIBACK], AIPORQ AS [AIPORQ], AIPRIO AS [AIPRIO], AIARTO AS [AIARTO]");
                        //queryBuilder.Append("\n, AIINVC AS [AIINVC], AIICON AS [AIICON], AIBLFR AS [AIBLFR], AINIVD AS [AINIVD]");
                        //queryBuilder.Append("\n, AILEDJ AS [AILEDJ], AIPLST AS [AIPLST], AIEDF1 AS [AIEDF1], AIEDF2 AS [AIEDF2]");
                        //queryBuilder.Append("\n, AIASN AS [AIASN], AIDSPA AS [AIDSPA], AICRMD AS [AICRMD], AIAMCR AS [AIAMCR]");
                        //queryBuilder.Append("\n, AIAC01 AS [AIAC01], AIAC02 AS [AIAC02], AIAC03 AS [AIAC03], AIAC04 AS [AIAC04]");
                        //queryBuilder.Append("\n, AIAC05 AS [AIAC05], AIAC06 AS [AIAC06], AIAC07 AS [AIAC07], AIAC08 AS [AIAC08]");
                        //queryBuilder.Append("\n, AIAC09 AS [AIAC09], AIAC10 AS [AIAC10], AIAC11 AS [AIAC11], AIAC12 AS [AIAC12]");
                        //queryBuilder.Append("\n, AIAC13 AS [AIAC13], AIAC14 AS [AIAC14], '" + overridePriceDetails.AlliedProductLine1 + "' AS [AIAC15], '" + overridePriceDetails.AlliedProductLine2 + "' AS [AIAC16]");
                        //queryBuilder.Append("\n, '" + overridePriceDetails.AlliedProductLine3 + "' AS [AIAC17], '" + overridePriceDetails.AlliedProductLine4 + "' AS [AIAC18], AIAC19 AS [AIAC19]");
                        //queryBuilder.Append("\n, AIAC20 AS [AIAC20], AIAC21 AS [AIAC21], '" + overridePriceDetails.POSUpCharge + "' AS [AIAC22]");
                        //queryBuilder.Append("\n, '" + overridePriceDetails.LiquidBracket + "' AS [AIAC23], '" + overridePriceDetails.PriceProtection + "' AS [AIAC24], AIAC25 AS [AIAC25]");
                        //queryBuilder.Append("\n, AIAC26 AS [AIAC26], '" + overridePriceDetails.AlliedProductLineOverall + "' AS [AIAC27]");
                        //queryBuilder.Append("\n, '" + overridePriceDetails.CoffVolume + "' AS [AIAC28], '" + overridePriceDetails.EquipProg + "' AS [AIAC29]");
                        //queryBuilder.Append("\n, '" + overridePriceDetails.SpecialCCP + "' AS [AIAC30], AIPRSN AS [AIPRSN], AIOPBO AS [AIOPBO], AITIER1 AS [AITIER1]");
                        //queryBuilder.Append("\n, AIPWPCP AS [AIPWPCP], AICUSTS AS [AICUSTS], AISTOF AS [AISTOF], AITERRID AS [AITERRID]");
                        //queryBuilder.Append("\n, AICIG AS [AICIG], AITORG AS [AITORG] FROM BUSDTA.F0101 AB INNER JOIN BUSDTA.F03012 CST ON AB.ABAN8=CST.AIAN8");
                        //queryBuilder.Append("\n WHERE ABAN8 = {0} "); // ShipTo

                        queryBuilder.Append("CALL BUSDTA.SP_GetPriceDetails (@AllPro1='" + overridePriceDetails.AlliedProductLine1 + "',@AllPro2='" + overridePriceDetails.AlliedProductLine2 + "',@AllPro3='" + overridePriceDetails.AlliedProductLine3 + "',@AllPro4='" + overridePriceDetails.AlliedProductLine4 + "',@POSUp='" + overridePriceDetails.POSUpCharge + "',@Liquid='" + overridePriceDetails.LiquidBracket + "'");
                        queryBuilder.Append(",@PricePro='" + overridePriceDetails.PriceProtection + "',@AlliedProduct='" + overridePriceDetails.AlliedProductLineOverall + "',@CoffVolume='" + overridePriceDetails.CoffVolume + "',@EquipProg='" + overridePriceDetails.EquipProg + "',@SpecialCCP='" + overridePriceDetails.SpecialCCP + "',@ShipTo='{0}',@ShipTo1='{1}',@ParentInt='{2}',@Flag={3})");
                        //The UNION part of the query is in QuotePricingManager.cs
                        //The UNION is responsible for combinations of BillTo/ShipTo/Parent of the Customer.
                    }
                    //************************************************************************************************
                    // Comment: Changed the type from double to decimal similar with the pricing manager calculation
                    // Created: feb 21, 2016
                    // Author: Vivensas (Rajesh,Yuvaraj)
                    // Revisions: 
                    //*************************************************************************************************

                    item.PricingUOMPrice = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(routeBranch, shipToCustomerID, item.ItemNumber, item.QuoteQuantity.ToString(), queryBuilder.ToString()));
                    item.UnitPrice = Convert.ToDecimal(OrderManager.GetPriceByUomFactor(item.ItemId, item.PricingUOM, item.UOM, Convert.ToDecimal(item.PricingUOMPrice)));

                    if (!string.IsNullOrEmpty(item.TransactionUOM))
                    {
                        item.TransactionUOMPrice = Convert.ToDecimal(OrderManager.GetPriceByUomFactor(item.ItemId, item.UOM, item.TransactionUOM, Convert.ToDecimal(item.UnitPrice)));
                    }

                    if (!string.IsNullOrEmpty(item.OtherUOM))
                    {
                        item.OtherUOMPrice = Convert.ToDecimal(OrderManager.GetPriceByUomFactor(item.ItemId, item.UOM, item.OtherUOM, Convert.ToDecimal(item.UnitPrice)));
                    }

                    //*************************************************************************************************
                    // Vivensas changes ends over here
                    //**************************************************************************************************
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TemplateManager][ApplyPricingToTemplates][routeBranch=" + routeBranch + ",shipToCustomerID=" + shipToCustomerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][TemplateManager][End:ApplyPricingToTemplates][routeBranch=" + routeBranch + ",shipToCustomerID=" + shipToCustomerID + "]");
            return orderItemFromTemplate;
        }

        #endregion

        #region Prospect Methods

        public bool UpdatePricingSetupProspectMaster(PricingDetails pricingDetail)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:UpdatePricingSetupProspectMaster]");

            //   StringBuilder queryBuilder = new StringBuilder(); //Unused local --Vignesh D
            bool flag = false;
            try
            {
                string query = "update busdta.Prospect_Master  set " +
                    " CategoryCode15=" + (string.IsNullOrEmpty(pricingDetail.AlliedProductLine1) ? "\n'', " : "\n'" + pricingDetail.AlliedProductLine1.Trim() + "',") + "  " +
                    " CategoryCode16=" + (string.IsNullOrEmpty(pricingDetail.AlliedProductLine2) ? "\n'', " : "\n'" + pricingDetail.AlliedProductLine2.Trim() + "',") + " " +
                    " CategoryCode17=" + (string.IsNullOrEmpty(pricingDetail.AlliedProductLine3) ? "\n'', " : "\n'" + pricingDetail.AlliedProductLine3.Trim() + "',") + " " +
                    " CategoryCode18=" + (string.IsNullOrEmpty(pricingDetail.AlliedProductLine4) ? "\n'', " : "\n'" + pricingDetail.AlliedProductLine4.Trim() + "',") + "" +
                    " POSUpCharge=" + (string.IsNullOrEmpty(pricingDetail.POSUpCharge) ? "\n'' ," : "\n'" + pricingDetail.POSUpCharge.Trim() + "',") + " " +
                    " LiquidCoffee=" + (string.IsNullOrEmpty(pricingDetail.LiquidBracket) ? "\n'', " : "\n'" + pricingDetail.LiquidBracket.Trim() + "',") + " " +
                    " PriceProtection=" + (string.IsNullOrEmpty(pricingDetail.PriceProtection) ? "\n'', " : "\n'" + pricingDetail.PriceProtection.Trim() + "',") + "" +
                    " AlliedDiscount=" + (string.IsNullOrEmpty(pricingDetail.AlliedProductLineOverall) ? "\n'', " : "\n'" + pricingDetail.AlliedProductLineOverall.Trim() + "',") + " " +
                    " CoffeeVolume=" + (string.IsNullOrEmpty(pricingDetail.CoffVolume) ? "\n'', " : "\n'" + pricingDetail.CoffVolume.Trim() + "',") + " " +
                    " EquipmentProgPts=" + (string.IsNullOrEmpty(pricingDetail.EquipProg) ? "\n'', " : "\n'" + pricingDetail.EquipProg.Trim() + "',") + " " +

                    " SpecialCCP=" + (string.IsNullOrEmpty(pricingDetail.SpecialCCP) ? "\n'', " : "\n'" + pricingDetail.SpecialCCP.Trim() + "',") + " " +
                    " IsPricesetup='" + Convert.ToByte(true) + "' ," +
                    " TaxGroup=" + (string.IsNullOrEmpty(pricingDetail.TaxGroup) ? "\n'' " : "\n'" + pricingDetail.TaxGroup.Trim() + "'") + " " +
                    " where ProspectId=" + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID + "";
                int count = DbEngine.ExecuteNonQuery(query);
                if (count > 0)
                {
                    flag = true;
                }
                ResourceManager.QueueManager.QueueProcess("ProspectManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][UpdatePricingSetupProspectMaster][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:UpdatePricingSetupProspectMaster]");
            return flag;

        }
        /// <summary>
        /// Unused Function
        /// 10-13-2016
        /// </summary>
        /// <returns></returns>
        /// 

        //Unused function commented for Performance  --Vignesh D
        //public bool HasPricingPerformed()
        //{
        //    Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][Start:HasPricingPerformed]");


        //    //  StringBuilder queryBuilder = new StringBuilder();   //Unused local --Vignesh D
        //    bool flag = true;
        //    try
        //    {
        //        string query = " SELECT IsPricesetup FROM BUSDTA.Prospect_Master WHERE ProspectId=" + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID + " ";

        //        string val = DbEngine.ExecuteScalar(query);
        //        if (string.IsNullOrEmpty(val))
        //        {
        //            flag = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][HasPricingPerformed][ExceptionStackTrace = " + ex.ToString() + "]");
        //        throw;
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][CustomerQuoteManager][End:HasPricingPerformed]");
        //    return flag;
        //}
        #endregion


        //Disposable Implementation --Vignesh D
        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
