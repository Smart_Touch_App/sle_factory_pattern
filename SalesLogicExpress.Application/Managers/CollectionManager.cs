﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using System.Data;
using System.Collections.ObjectModel;
using SalesLogicExpress.Application.ViewModels;
using System.Collections;

namespace SalesLogicExpress.Application.Managers
{
    public class CollectionManager:IDisposable
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.CollectionManager");


        public CashDelivery GetCashDeliveryInfo(string CustmerId, string orderId, bool ShowLastPaymentAmountEntered = false)
        {
            log.Info("[SalesLogicExpress.Application.ViewModels][CollectionManager][Start:GetCashDeliveryInfo]");
            try
            {
                CashDelivery cashDelivery = new CashDelivery();
                ObservableCollection<PaymentARModel.InvoiceDetails> InvoiceDetailsList = new ObservableCollection<PaymentARModel.InvoiceDetails>();

                // Get Customer Bill to ID
                string sBillTo = CustomerManager.GetCustomerBillTo(CustmerId);

                //                string sql = @"BEGIN
                //DECLARE PrvBal numeric(8,2);
                //select 
                //ISNULL(SUM(OpenAmt),0) into PrvBal
                //from BUSDTA.Customer_Ledger_R1 where CustBillToId=" + CustmerId + @" and TransactionTypeID<100;
                //select isnull(PrvBal,0)PrvBal,isnull(oh.InvoiceTotalAmt,0) Invoice_Total,isnull(PrvBal,0) + isnull(oh.InvoiceTotalAmt,0) as Total ,
                //0 as PDPAMT,
                //0 as PDPMODE,
                //'' as PDCHQNO from busdta.ORDER_HEADER oh where oh.OrderID=" + orderId + @"
                //END";
                //string query = "SELECT CASE WHEN ARDocumentType='RI' THEN 'Charge Inv' " +
                //           " WHEN ARDocumentType='RM' THEN 'Credit ' " +
                //           " WHEN ARDocumentType='R3' THEN 'Cash Inv'  ELSE ARDocumentType END as mType ,  " +
                //           " ARDocumentID as InvoiceNo,SourceDocID as OrderNo,CustShipToId as ShipTo,InvoiceDate,GrossAmt, " +
                //           " OpenAmt+(select ISNULL(SUM(AppliedAmount),0) from busdta.Receipt_Ledger where AppliedDocumentID=ARDocumentID and SourceID<>0) as OpenAmt, DATEDIFF(day,BUSDTA.Customer_Ledger_R1.InvoiceDate,Getdate()) AS Age " +
                //           " FROM BUSDTA.Customer_Ledger_R1 where CustBillToId=" + sBillTo + " and TransactionTypeID<100 " +
                //           " Order By InvoiceDate ";
                string sql = ("CALL BUSDTA.SP_GetCashDeliveryInfo(" + CustmerId + @"," + orderId + @")");

                string query = ("CALL BUSDTA.SP_GetCashDeliveryInfoCustBillToId (" + sBillTo + ")");
                DataSet result = DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {

                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        PaymentARModel.InvoiceDetails obj = new PaymentARModel.InvoiceDetails();
                        obj.InvoiceNoInvDtl = result.Tables[0].Rows[i]["InvoiceNo"].ToString();
                        obj.OpenAmountInvDtl = Convert.ToDecimal(string.IsNullOrEmpty(result.Tables[0].Rows[i]["OpenAmt"].ToString().Trim()) ? "0" : result.Tables[0].Rows[i]["OpenAmt"].ToString().Trim());
                        InvoiceDetailsList.Add(obj);
                    }

                }
                var lst = InvoiceDetailsList.Where(x => x.OpenAmountInvDtl > 0).ToList();

                try
                {
                    if (lst.Count > 0)
                    {
                        cashDelivery.PreviousBalanceAmount = lst.Sum(x => x.OpenAmountInvDtl);
                        cashDelivery.ARDocumentId.AddRange(lst);
                    }
                }
                catch (Exception) { }


                DataSet cashDeliveryResult = DbEngine.ExecuteDataSet(sql);

                if (cashDeliveryResult.HasData())
                {
                    // cashDelivery.PreviousBalanceAmount = Convert.ToDecimal(cashDeliveryResult.Tables[0].Rows[0]["PrvBal"].ToString());
                    cashDelivery.CurrentInvoiceAmount = Convert.ToDecimal(cashDeliveryResult.Tables[0].Rows[0]["Invoice_Total"].ToString());
                    //cashDelivery.TotalBalanceAmount = Convert.ToDecimal(cashDeliveryResult.Tables[0].Rows[0]["Total"].ToString());
                    cashDelivery.TotalBalanceAmount = cashDelivery.PreviousBalanceAmount + cashDelivery.CurrentInvoiceAmount;
                    if (ShowLastPaymentAmountEntered)
                    {
                        cashDelivery.PaymentAmount = cashDeliveryResult.Tables[0].Rows[0]["PDPAMT"].ToString();
                        cashDelivery.PaymentMode = Convert.ToBoolean(cashDeliveryResult.Tables[0].Rows[0]["PDPMODE"]);
                        cashDelivery.ChequeNo = cashDeliveryResult.Tables[0].Rows[0]["PDCHQNO"].ToString();
                    }
                    cashDelivery.ChequeDate = DateTime.Today;
                    cashDelivery.CreditLimit = 100; // TODO: Hard Coded Value - CHANGE!

                }

                return cashDelivery;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CollectionManager][GetCashDeliveryInfo][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            log.Info("[SalesLogicExpress.Application.ViewModels][CollectionManager][End:GetCashDeliveryInfo]");
        }
        public CashDelivery GetCreditHoldInfo(string CustmerId)
        {
            log.Info("[SalesLogicExpress.Application.ViewModels][CollectionManager][Start:GetCreditHoldInfo]");
            try
            {
                CashDelivery cashDelivery = new CashDelivery();

                //                DataSet cashDeliveryResult = DbEngine.ExecuteDataSet(@" 
                //                                                BEGIN
                //                                                DECLARE PrvBal numeric(8,4);
                //                                                select 
                //                                                (sum(isnull(cl.InvoiceOpenAmt,0)) - sum(isnull(cl.ConcessionAmt,0)) - sum(isnull(cl.receiptappliedamt,0)) - sum(isnull(cl.receiptunappliedamt,0) ))  into PrvBal
                //                                                from BUSDTA.Customer_Ledger cl join busdta.Invoice_Header ih on cl.InvoiceId=ih.InvoiceID
                //                                                where IH.CustShipToId=" + CustmerId + @" and cl.isactive='1' and ih.DeviceStatusID!=6 and ih.ARStatusID!=6;
                //                                                select isnull(PrvBal,0)PrvBal,isnull(oh.InvoiceTotalAmt,0) Invoice_Total,isnull(PrvBal,0) + isnull(oh.InvoiceTotalAmt,0) as Total ,
                //                                                0 as PDPAMT,
                //                                                0 as PDPMODE,
                //                                                '' as PDCHQNO from busdta.ORDER_HEADER oh  where oh.CustShipToId=" + CustmerId + @" END");
                //                string sql = @"BEGIN
                //DECLARE PrvBal numeric(8,2);
                //select 
                //ISNULL(SUM(OpenAmt),0) into PrvBal
                //from BUSDTA.Customer_Ledger_R1 where CustBillToId=" + CustmerId + @" and TransactionTypeID<100;
                //select isnull(PrvBal,0)PrvBal,isnull(oh.InvoiceTotalAmt,0) Invoice_Total,isnull(PrvBal,0) + isnull(oh.InvoiceTotalAmt,0) as Total ,
                //0 as PDPAMT,
                //0 as PDPMODE,
                //'' as PDCHQNO from busdta.ORDER_HEADER oh where oh.CustBillToId=" + CustmerId + @"
                //END";
                string sql = ("CALL BUSDTA.SP_GetCreditHoldInfo(" + CustmerId + @")");
                DataSet cashDeliveryResult = DbEngine.ExecuteDataSet(sql);

                if (cashDeliveryResult.HasData())
                {
                    cashDelivery.PreviousBalanceAmount = Convert.ToDecimal(cashDeliveryResult.Tables[0].Rows[0]["PrvBal"].ToString());
                    cashDelivery.CurrentInvoiceAmount = Convert.ToDecimal(cashDeliveryResult.Tables[0].Rows[0]["Invoice_Total"].ToString());
                    cashDelivery.TotalBalanceAmount = Convert.ToDecimal(cashDeliveryResult.Tables[0].Rows[0]["Total"].ToString());
                    //if (ShowLastPaymentAmountEntered)
                    //{
                    //    cashDelivery.PaymentAmount = cashDeliveryResult.Tables[0].Rows[0]["PDPAMT"].ToString();
                    //    cashDelivery.PaymentMode = Convert.ToBoolean(cashDeliveryResult.Tables[0].Rows[0]["PDPMODE"]);
                    //    cashDelivery.ChequeNo = cashDeliveryResult.Tables[0].Rows[0]["PDCHQNO"].ToString();
                    //}
                    cashDelivery.ChequeDate = DateTime.Today;
                    cashDelivery.CreditLimit = 100; // TODO: Hard Coded Value - CHANGE!

                }
                log.Info("[SalesLogicExpress.Application.ViewModels][CollectionManager][End:GetCashDeliveryInfo]");
                return cashDelivery;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CollectionManager][GetCashDeliveryInfo][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
        

        }

        public ChargeOnAccount GetChargeOnAccountInfo(string CustmerId, string orderId, decimal TotalBalanceAmount, decimal PreviousBalanceAmount, decimal CurrentInvoiceAmount)
        {
            log.Info("[SalesLogicExpress.Application.ViewModels][CollectionManager][Start:GetChargeOnAccountInfo]");
            try
            {
                ChargeOnAccount chargeOnAccount = new ChargeOnAccount();
                chargeOnAccount.PreviousBalanceAmount = PreviousBalanceAmount;
                chargeOnAccount.TotalBalanceAmount = TotalBalanceAmount;
                chargeOnAccount.CurrentInvoiceAmount = CurrentInvoiceAmount;
                chargeOnAccount.TemporaryCharge = 2;
                chargeOnAccount.RequestCode = "78949875";   // TODO: Hard Coded Value - CHANGE!
                //chargeOnAccount.ApprovalCode = "1234-1234";
                chargeOnAccount.CreditLimit = 100;   // TODO: Hard Coded Value - CHANGE!

                return chargeOnAccount;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CollectionManager][GetChargeOnAccountInfo][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            log.Info("[SalesLogicExpress.Application.ViewModels][CollectionManager][End:GetChargeOnAccountInfo]");
        }

        public int SaveCashDeliveryInfo(CashDelivery cashDelivery, string customerId, string routeId, string InvoiceId, ref string newReceiptId)
        {

            DataTable dt = new DataTable();

            String CustBillTo = CustomerManager.GetCustomerBillTo(customerId);
            log.Info("[SalesLogicExpress.Application.ViewModels][CollectionManager][Start:SaveCashDeliveryInfo]");
            try
            {
                if (string.IsNullOrEmpty(cashDelivery.PaymentAmount))
                {
                    cashDelivery.PaymentAmount = 0.ToString();
                }


                string chequeNo = string.IsNullOrEmpty(cashDelivery.ChequeNo) ? "" : cashDelivery.ChequeNo;
                string chequeDate = cashDelivery.ChequeDate.ToString("yyyy-MM-dd");

                int NewReceiptId = InsertReceiptHeader(cashDelivery, customerId, CustBillTo, routeId, chequeNo, chequeDate);
                if (NewReceiptId != 0)
                {
                    newReceiptId = NewReceiptId.ToString();

                    //Receipt entry in Customer ledger
                    //InsertReceiptInCustomerLedger(routeId, NewReceiptId, customerId, CustBillTo, Convert.ToDecimal(cashDelivery.PaymentAmount));

                    //InsertReceiptInvoiceMappingInCustomerLedger(
                    //         CommonNavInfo.RouteID.ToString(),
                    //         CommonNavInfo.Customer.CustomerNo, CustBillTo,
                    //         InvoiceId.ToString(),
                    //         cashDelivery.CurrentInvoiceAmount,
                    //         NewReceiptId,
                    //         cashDelivery.CurrentInvoiceAmount
                    //         );
                    int DetailLineNumber = 1;
                    InsertintoReceiptLedger(
                                    NewReceiptId.ToString(), DetailLineNumber.ToString(), CommonNavInfo.RouteID.ToString(),
                                    CommonNavInfo.RouteID.ToString(), CustBillTo, chequeNo, chequeDate,
                                    cashDelivery.PaymentMode.ToString(), DateTime.Now.ToString("yyyy-MM-dd"), cashDelivery.PaymentAmount,
                                    (Convert.ToDecimal(cashDelivery.PaymentAmount) - Convert.ToDecimal(cashDelivery.PaymentAmount)).ToString(), "0", "0", "RC", "Mobile Receipt", InvoiceId.ToString(),
                                    "", "", "-" + cashDelivery.CurrentInvoiceAmount.ToString(), CustBillTo,
                                    CommonNavInfo.Customer.CustomerNo, "Mobile Receipt", "", "", "4", ""
                                    );

                    if (cashDelivery.ARDocumentId != null)
                    {
                        if (cashDelivery.ARDocumentId.Count() > 0)
                        {
                            if (cashDelivery.CurrentInvoiceAmount != Convert.ToDecimal(cashDelivery.PaymentAmount))
                            {
                                foreach (var item in cashDelivery.ARDocumentId)
                                {
                                    DetailLineNumber += DetailLineNumber;
                                    InsertintoReceiptLedger(
                                                NewReceiptId.ToString(), DetailLineNumber.ToString(), CommonNavInfo.RouteID.ToString(),
                                                CommonNavInfo.RouteID.ToString(), CustBillTo, chequeNo, chequeDate,
                                                cashDelivery.PaymentMode.ToString(), DateTime.Now.ToString("yyyy-MM-dd"), cashDelivery.PaymentAmount,
                                                (Convert.ToDecimal(cashDelivery.PaymentAmount) - item.OpenAmountInvDtl).ToString(), "0", "0", "RC", "Mobile Receipt", item.InvoiceNoInvDtl.ToString(),
                                                "", "", "-" + item.OpenAmountInvDtl.ToString(), CustBillTo,
                                                CommonNavInfo.Customer.CustomerNo, "Mobile Receipt", "", "", "4", ""
                                                );
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CollectionManager][SaveCashDeliveryInfo][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            log.Info("[SalesLogicExpress.Application.ViewModels][CollectionManager][End:SaveCashDeliveryInfo]");
            return 1;
        }

        public int InsertReceiptHeader(CashDelivery cashDelivery, string customerId, string CustBillTo, string routeId, string chequeNo, string chequeDate, int receiptId = 0, string reasonCodeId = "")
        {
            int NewReceiptId = 0;
            log.Info("[SalesLogicExpress.Application.ViewModels][CollectionManager][Start:InsertReceiptHeader]");
            try
            {
                //When receipt id is zero, number pool will be used to fetch the next receipt number, 
                //else passed receipt id will be used
                if (receiptId == 0)
                {
                    //NewReceiptId = Convert.ToInt32(new Managers.NumberManager().GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.Receipt));
                    string NxtNumber = new Managers.NumberManager().GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.Receipt);
                    if (!string.IsNullOrEmpty(NxtNumber))
                        NewReceiptId = Convert.ToInt32(NxtNumber);
                }
                else
                {
                    NewReceiptId = receiptId;
                }

                //Save in Payment Details

                if (NewReceiptId != 0)
                    DbEngine.ExecuteNonQuery(@"insert into BUSDTA.Receipt_Header  (JDEStatusId,StatusId,CustShipToId,CustBillToId ,RouteId ,ReceiptID,TransactionAmount ,PaymentMode ,ChequeNum ,ChequeDate ,CreatedBy,CreatedDatetime,TransactionMode,ReceiptNumber,ReceiptDate, ReasonCodeId)
                                        values('0',4," + customerId + "," + CustBillTo + "," + routeId + "," + NewReceiptId.ToString() + "," + cashDelivery.PaymentAmount + ",'" + Convert.ToInt32(cashDelivery.PaymentMode) + "','" + chequeNo + "','" + chequeDate + "',NULL,now(),'0'," + NewReceiptId.ToString() + ",'" + chequeDate + "'," + (string.IsNullOrEmpty(reasonCodeId) ? "NULL" : reasonCodeId) + ")");

                ResourceManager.QueueManager.QueueProcess("ARPayments", false);
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CollectionManager][InsertReceiptHeader][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            log.Info("[SalesLogicExpress.Application.ViewModels][CollectionManager][End:InsertReceiptHeader]");
            return NewReceiptId;
        }

        //        public void InsertReceiptInCustomerLedger(string routeId, int NewReceiptId, string customerId, String CustBillTo, decimal receiptUnAppliedAmt)
        //        {
        //            DbEngine.ExecuteNonQuery(@"insert into BUSDTA.Customer_Ledger (ReceiptID,CustShipToId,CustBillToId,RouteId,ReceiptUnAppliedAmt,IsActive) 
        //                                       values(" + NewReceiptId.ToString() + "," + customerId + "," + CustBillTo + "," + routeId + "," + receiptUnAppliedAmt.ToString() + ",'1')");
        //        }

//        public void InsertReceiptInvoiceMappingInCustomerLedger(string routeId, string customerId, string CustBillTo, string InvoiceId, decimal InvoiceGrossAmt, int ReceiptId, decimal InvOpenAmt)
//        {

//            log.Info("[SalesLogicExpress.Application.ViewModels][CollectionManager][Start:InsertReceiptInvoiceMappingInCustomerLedger]");
//            try
//            {
//                DbEngine.ExecuteNonQuery(@"insert into BUSDTA.Customer_Ledger  (RouteId,CustShipToId,CustBillToId,ReceiptID ,InvoiceId,InvoiceGrossAmt,InvoiceOpenAmt,ReceiptUnAppliedAmt,ReceiptAppliedAmt,  IsActive) 
//                                       values(" + routeId + "," + customerId + "," + CustBillTo + "," + ReceiptId.ToString() + "," + InvoiceId + "," + InvoiceGrossAmt.ToString() + ",0,0," + InvOpenAmt.ToString() + ",'1')");

//                //Update Invoice Status if Invoice Open Amt =< Payment Amt
//                DbEngine.ExecuteNonQuery(@"Update BUSDTA.Invoice_Header set DeviceStatusID=2 where InvoiceID=" + InvoiceId + " and RouteId=" + routeId);

//                ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
//            }
//            catch (Exception ex)
//            {
//                log.Error("[SalesLogicExpress.Application.Managers][CollectionManager][InsertReceiptInvoiceMappingInCustomerLedger][ExceptionStackTrace = " + ex.StackTrace + "]");
//                throw;
//            }
//            log.Info("[SalesLogicExpress.Application.ViewModels][CollectionManager][End:InsertReceiptInvoiceMappingInCustomerLedger]");

//        }

        public void InsertintoReceiptLedger(string ReceiptID, string DetailLineNumber, string SourceId, string OriginId, string ReceiptCustBillToId, string ChequeNum, string ChequeDate, string PaymentType, string ReceiptDate, string ReceiptAmount, string ReceiptOpenAmount, string AdjustmentAmt, string VarianceAmt, string ReciptJEDocType, string Explaination, string AppliedDocumentID, string AppliedDocType, string AppliedDocCompany, string AppliedAmount, string AppliedCustBillToId, string AppliedCustShipToId, string Remark, string VoidCode, string VoidReasonCode, string StatusId, string SettelmentId)
        {
            log.Info("[SalesLogicExpress.Application.ViewModels][CollectionManager][Start:InsertintoReceiptLedger]");
            try
            {
                int a = Convert.ToBoolean(PaymentType) == false ? 0 : 1;
                string sql = " Insert into busdta.Receipt_Ledger (ReceiptID,DetailLineNumber,SourceId,OriginId,ReceiptCustBillToId,ChequeNum,ChequeDate," +
                            " PaymentType,ReceiptDate,ReceiptAmount,ReceiptOpenAmount,AdjustmentAmt,VarianceAmt,ReciptJEDocType,Explaination,AppliedDocumentID," +
                            " AppliedDocType,AppliedDocCompany,AppliedAmount,AppliedCustBillToId,AppliedCustShipToId,Remark,VoidCode,VoidReasonCode,StatusId," +
                            " SettelmentId,CreatedBy,CreatedDatetime) values(" + ReceiptID + "," + DetailLineNumber + "," + SourceId + "," + OriginId + "," + ReceiptCustBillToId +
                            ",'" + ChequeNum + "','" + ChequeDate + "'," + a + ",'" + ReceiptDate + "'," + ReceiptAmount + "," + ReceiptOpenAmount + "," + AdjustmentAmt + "," +
                            VarianceAmt + ",'" + ReciptJEDocType + "','" + Explaination + "','" + AppliedDocumentID + "','" + AppliedDocType + "','" + AppliedDocCompany + "'," +
                            AppliedAmount + "," + AppliedCustBillToId + "," + AppliedCustShipToId + ",'" + Remark + "','" + VoidCode + "','" + VoidReasonCode + "','" + StatusId + "','" +
                            SettelmentId + "','" + SourceId + "','" + DateTime.Now.ToString("yyyy-MM-dd") + "')";
                DbEngine.ExecuteNonQuery(sql);

                ResourceManager.QueueManager.QueueProcess("ARPayments", false);
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CollectionManager][InsertintoReceiptLedger][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            log.Info("[SalesLogicExpress.Application.ViewModels][CollectionManager][End:InsertintoReceiptLedger]");

        }

        // Code written by Marimuthu for Performance change
        // Dispose implementation for Performance optimization
        // Date : 11/03/2016
        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

    }
}