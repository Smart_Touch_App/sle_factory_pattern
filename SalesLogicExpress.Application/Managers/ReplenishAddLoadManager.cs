﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;

namespace SalesLogicExpress.Application.Managers
{
    //Sathish Removed Unwanted static variables and static class
    public class ReplenishAddLoadManager
    {
        private static readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.ReplenishAddLoadManager");

        public static bool SaveAddLoad(ObservableCollection<AddLoadDetails> AddLoadDetailsCollection, string replenID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:SaveAddLoad]");
            bool flag = false;

            string query = "";

            try
            {
                //Delete respective replenishentIDs rows from Route_Replenishment_Detail 

                query = "delete from BUSDTA.Route_Replenishment_Detail where ReplenishmentID ='" + replenID + "'";
                DbEngine.ExecuteNonQuery(query);

                string status = PayloadManager.RouteReplenishmentPayload.IsSalesBranch ? "RTP" : "INPRS";
                //query = "select StatusTypeID from busdta.Status_Type where StatusTypeCD='" + status + "'";
                query = "CALL BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD = '" + status + "')";
                string StatusId = DbEngine.ExecuteScalar(query);
                int detailID = 1;
                foreach (AddLoadDetails item in AddLoadDetailsCollection)
                {
                    int loadQty = item.LoadQuantity > item.MaxLoadQty ? item.MaxLoadQty : item.LoadQuantity;
                    string openQty = string.IsNullOrEmpty(item.OpenReplnQty) ? "0" : item.OpenReplnQty;

                    query = " INSERT INTO  BUSDTA.Route_Replenishment_Detail (ReplenishmentID,ReplenishmentDetailID,ItemId,ReplenishmentQtyUM,  " +
                            " ReplenishmentQty,PickedQty,CreatedBy,CreatedDatetime ,AdjustmentQty,PickedStatusTypeID, " +
                            " AvailaibilityAtTime,ParLevelQty,DemandQty,OpenReplnQty, RouteId,UpdatedBy, UpdatedDatetime) " +
                            " VALUES('" + replenID + "','" + detailID + "','" + item.ItemId + "','" + item.QtyUOM + "','" + loadQty + "', " +
                            " '" + item.PickedQty + "','" + UserManager.UserId + "',getdate(),'" + item.SuggestedQty + "' ,'" + StatusId + "', " +
                            " '" + item.AvailableQty + "', '" + item.ParLevel + "','" + item.DemandQty + "','" + openQty + "','" + CommonNavInfo.RouteID + "','" + Managers.UserManager.UserId + "',getdate())";
                    DbEngine.ExecuteNonQuery(query);
                    detailID++;
                }

                flag = true;
                ResourceManager.QueueManager.QueueProcess("Replenishments", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][SaveAddLoad][ExceptionStackTrace = " + ex.StackTrace + "]");
                ////throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:SaveAddLoad]");

            return flag;
        }

        public static int GetNewAddLoadID
        {
            get
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:GetNewAddLoadID]");
                string val = new Managers.NumberManager().GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.Replenishment);
                Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:GetNewAddLoadID]");
                if (!string.IsNullOrEmpty(val))
                    return Convert.ToInt32(val);
                else
                    return 0;
            }
        }

        public static bool SaveHeldReturn(ObservableCollection<AddLoadDetails> AddLoadDetailsCollection, string replenID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:SaveHeldReturn]");
            bool flag = false;

            //string query = "",queryUpdate = "";
            string query = "";
            try
            {
                string status = "RTP";
                //query = "select StatusTypeID from busdta.Status_Type where StatusTypeCD='" + status + "'";
                query = "CALL BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD = '" + status + "')";

                string StatusId = DbEngine.ExecuteScalar(query);
                int detailID = 1;
                foreach (AddLoadDetails item in AddLoadDetailsCollection)
                {
                    int loadQty = item.LoadQuantity;
                    string openQty = string.IsNullOrEmpty(item.OpenReplnQty) ? "0" : item.OpenReplnQty;
                    query = " INSERT INTO  BUSDTA.Route_Replenishment_Detail (ReplenishmentID,ReplenishmentDetailID,ItemId,ReplenishmentQtyUM,  " +
                            " ReplenishmentQty,PickedQty,CreatedBy,CreatedDatetime ,AdjustmentQty,PickedStatusTypeID, " +
                            " AvailaibilityAtTime,ParLevelQty,DemandQty,OpenReplnQty, RouteId,OrderID) " +
                            " VALUES('" + replenID + "','" + detailID + "','" + item.ItemId + "','" + item.QtyUOM + "','" + loadQty + "', " +
                            " '" + item.PickedQty + "','" + UserManager.UserId + "',getdate(),'" + item.SuggestedQty + "' ,'" + StatusId + "', " +
                            " '" + item.AvailableQty + "', '" + item.ParLevel + "','" + item.DemandQty + "','" + openQty + "','" + CommonNavInfo.RouteID + "','" + item.OrderID + "' )";
                    DbEngine.ExecuteNonQuery(query);
                    detailID++;

                    
                }

                flag = true;
                ResourceManager.QueueManager.QueueProcess("Replenishments", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][SaveHeldReturn][ExceptionStackTrace = " + ex.StackTrace + "]");
                ////throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:SaveHeldReturn]");

            return flag;
        }

        public static bool UpdateAddLoad(List<AddLoadDetails> AddLoadDetailsCollection, string replenID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:UpdateAddLoad]");
            bool flag = false;
            try
            {
                foreach (AddLoadDetails item in AddLoadDetailsCollection)
                {
                    int AvailaibilityAtTime = item.OnHandQty;//Saving onhand quantity in this for completed replenishment
                    int CurrentAvailability = item.CommittedQty;  //Saving onhand quantity in this for completed replenishment
                    int pickedQty = item.PickedQty;
                    int SequenceNumber = item.HeldQty;
                    int parlevelqty = item.ParLevel;
                    string queryToUpdate = "update BUSDTA.Route_Replenishment_Detail A set A.CurrentAvailability = '" + CurrentAvailability + "',A.PickedQty ='" + pickedQty + "'  " +
                            " ,A.AvailaibilityAtTime=" + AvailaibilityAtTime + ",SequenceNumber =" + SequenceNumber + "   " +
                            " ,A.parlevelqty =" + parlevelqty + ",UpdatedBy =" + Managers.UserManager.UserId + ",UpdatedDatetime = GETDATE()   " +
                            " where A.ItemId= '" + item.ItemId + "' and A.ReplenishmentID='" + replenID + "'  and A.ReplenishmentDetailID =" + item.ReplnDetailId + "";

                    DbEngine.ExecuteNonQuery(queryToUpdate);
                }

                flag = true;
                ResourceManager.QueueManager.QueueProcess("Replenishments", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][UpdateAddLoad][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:UpdateAddLoad]");

            return flag;
        }

        public static ObservableCollection<AddLoadDetails> GetAddLoadSearchItemsDetails(List<string> itemsToEscape)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:GetAddLoadSearchItemsDetails]");
            ObservableCollection<AddLoadDetails> searchData = new ObservableCollection<AddLoadDetails>();
            string itemNoString = itemsToEscape.Count == 0 ? "" : itemsToEscape.Aggregate((oldData, newData) => oldData + "," + newData);

            string query = string.Empty;
            query = "CALL BUSDTA.SP_GetAddLoadDetailsForBranchToTruck(@itemNo = '" + itemNoString + "',@IsFromBranchToTruck = " + (PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck ? "1" : "0").ToString() + ")";

            //            if (PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck)
            //            {
            //                query = @"select inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo', inventory.RouteId, inventory.OnHandQuantity AS 'OnHandQty',
            //                                inventory.CommittedQuantity AS 'CommittedQty', 
            //                                inventory.HeldQuantity AS 'HeldQty',inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription',
            //                                 (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty',
            //                                isnull(im.IMLNTY ,'')as 'StkType', isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4',
            //                                isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM'
            //                                from busdta.Inventory inventory left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
            //                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID 
            //                                where inventory.ItemId not in (" + itemNoString + @") AND iconfig.PrimaryUM IS NOT NULL and iconfig.RouteEnabled = 1 
            //                                and iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 order by ItemNumber ";
            //            }
            //            else
            //            {
            //                query = @"select inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo', inventory.RouteId, inventory.OnHandQuantity AS 'OnHandQty',
            //                                inventory.CommittedQuantity AS 'CommittedQty', 
            //                                inventory.HeldQuantity AS 'HeldQty',inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription',
            //                                 (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty',
            //                                isnull(im.IMLNTY ,'')as 'StkType', isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4',
            //                                isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM'
            //                                from busdta.Inventory inventory left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
            //                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID 
            //                                where inventory.ItemId not in (" + itemNoString + @") AND iconfig.PrimaryUM IS NOT NULL and iconfig.RouteEnabled = 1 
            //                                and (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) > 0  
            //                                and iconfig.RouteEnabled = 1 and iconfig.allowSearch=1  order by ItemNumber ";
            //            }

            List<AddLoadDetails> activities = new List<AddLoadDetails>();
            try
            {
                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    activities = result.GetEntityList<AddLoadDetails>();
                }
                searchData = new ObservableCollection<AddLoadDetails>(activities);
                foreach (AddLoadDetails item in searchData)
                {
                    //string uomQuery = "Call BUSDTA.SP_GetUMCode(@itemid='" + item.ItemId + "')";

                    ////string uomQuery = "SELECT UOM as 'UMCode', 1 as 'UMMultipler' FROM BUSDTA.ItemUoMs  WHERE ITEMID='" + item.ItemId + "' and  cansell=1 order by DisplaySeq";
                    //List<QtyUOMClass> qtyUOM = new List<QtyUOMClass>();
                    //DataSet resultUOM = DbEngine.ExecuteDataSet(uomQuery);
                    //if (resultUOM.HasData())
                    //{
                    //    qtyUOM = resultUOM.GetEntityList<QtyUOMClass>();

                    //}
                    //item.QtyUOMCollection = new ObservableCollection<QtyUOMClass>(qtyUOM);

                    item.QtyUOMCollection = GetUOMCode(item.ItemId);

                    //Set selected item in dropdown
                    QtyUOMClass objUM = item.QtyUOMCollection.FirstOrDefault(x => x.UMCode.Trim() == item.PrimaryUOM.Trim());
                    if (objUM != null)
                    {
                        item.SelectedQtyUOM = item.QtyUOMCollection.FirstOrDefault(x => x.UMCode.Trim() == item.PrimaryUOM.Trim());
                        item.QtyUOM = item.PrimaryUOM.Trim();
                    }
                    else
                    {
                        item.SelectedQtyUOM = item.QtyUOMCollection[0];
                        item.QtyUOM = item.QtyUOMCollection[0].UMCode;
                        item.MaxLoadQty = item.AvailableQty / Convert.ToInt32(item.QtyUOMCollection[0].UMMultipler);
                    }

                    //Set conversion factor
                    List<QtyUOMClass> objRemove = new List<QtyUOMClass>();

                    foreach (QtyUOMClass x in item.QtyUOMCollection)
                    {
                        x.UMMultipler = new TemplateManager().SetConversionFactorForItem(item.PrimaryUOM, x.UMCode, item.ItemId, item.ItemNo);
                        if (x.UMMultipler > item.AvailableQty)
                        {
                            objRemove.Add(x);
                        }
                    }

                    //Check wwhether  Conversion Factor > AvailableQty. If true then remove UM from QtyUOMCollection 
                    foreach (QtyUOMClass y in objRemove)
                    {
                        item.QtyUOMCollection.Remove(y);
                    }

                    //item.ConvesionFactor = SetConversionFactorForItem(item);


                    //Set maxload quantity 
                    item.MaxLoadQty = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck ? 9999 : item.MaxLoadQty;
                    item.IsFromBranchToTruck = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck;

                    //Disable check for max load if we are showing completed replenishment
                    item.IsFromBranchToTruck = PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet ? true : item.IsFromBranchToTruck;
                    item.MaxLoadQty = PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet ? 9999 : item.MaxLoadQty;

                    //Get Open Repln Qty for respected item 
                    #region Get Open Repln

                    string replnID = PayloadManager.RouteReplenishmentPayload.SelectedReplnId == "New" ? "0" : PayloadManager.RouteReplenishmentPayload.SelectedReplnId;
                    if (!PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                    {
//                        string queryOpenRepln = @"SELECT A.ReplenishmentID ,A.ReplenishmentQty FROM BUSDTA.Route_Replenishment_Detail  A 
//                                            WHERE  A.ReplenishmentID 
//                                            IN  (SELECT B.ReplenishmentID FROM BUSDTA.Route_Replenishment_Header B 
//                                                    WHERE  B.StatusId NOT IN (SELECT C.StatusTypeID FROM BUSDTA.Status_Type C 
//                                                    WHERE C.StatusTypeCD IN ('CMPLT','VOID')) and B.ToBranchId ='" + UserManager.UserRoute + @"'
//                                                    )
//                                            AND A.ItemId='" + item.ItemId.Trim() + "' and A.ReplenishmentID != '" + replnID + "'";

                        //                        string queryOpenRepln = @"SELECT A.ReplenishmentID ,A.ReplenishmentQty FROM BUSDTA.Route_Replenishment_Detail  A 
                        //                                            WHERE  A.ReplenishmentID 
                        //                                            IN  (SELECT B.ReplenishmentID FROM BUSDTA.Route_Replenishment_Header B 
                        //                                                    WHERE  B.StatusId NOT IN (SELECT C.StatusTypeID FROM BUSDTA.Status_Type C 
                        //                                                    WHERE C.StatusTypeCD IN ('CMPLT','VOID')) and Trim(B.ToBranch) ='" + UserManager.UserBranch.Trim() + @"'
                        //                                                    )
                        //                                            AND A.ItemId='" + item.ItemId.Trim() + "' and A.ReplenishmentID != '" + replnID + "'";
                        DataSet ds = GetReplenQty(item.ItemId.Trim(), replnID);
                        if (ds.HasData())
                        {
                            int sum = 0;
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                //int replenId = Convert.ToInt32(ds.Tables[0].Rows[i]["ReplenishmentID"].ToString());
                                int loadQty = Convert.ToInt32(ds.Tables[0].Rows[i]["ReplenishmentQty"].ToString());
                                sum += loadQty;
                            }
                            item.OpenReplnQty = sum.ToString();
                        }
                        else
                        {
                            item.OpenReplnQty = "0";
                        }
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][GetAddLoadSearchItemsDetails][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:GetAddLoadSearchItemsDetails]");


            return searchData;
        }

        public static ObservableCollection<AddLoadDetails> GetAddLoadSearchItemsDetailsForBranchToTruck(List<string> itemsToEscape)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:GetAddLoadSearchItemsDetailsForBranchToTruck]");
            ObservableCollection<AddLoadDetails> searchData = new ObservableCollection<AddLoadDetails>();
            string itemNoString = itemsToEscape.Count == 0 ? "" : itemsToEscape.Aggregate((oldData, newData) => oldData + "," + newData);

            string query = string.Empty;
            query = "CALL BUSDTA.SP_GetAddLoadDetailsForBranchToTruck(@itemNo = '" + itemNoString + "' ,@IsFromBranchToTruck = " + (PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck ? "1" : "0").ToString() + ")";
            //            if (PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck)
            //            {
            //                query = @"select inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo', inventory.RouteId, inventory.OnHandQuantity AS 'OnHandQty',
            //                                inventory.CommittedQuantity AS 'CommittedQty', 
            //                                inventory.HeldQuantity AS 'HeldQty',inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription',
            //                                 (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty',
            //                                isnull(im.IMLNTY ,'')as 'StkType', isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4',
            //                                isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM'
            //                                from busdta.Inventory inventory left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
            //                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID 
            //                                LEFT JOIN BUSDTA.F4102 ib ON im.imitm = ib.ibitm
            //                                LEFT JOIN BUSDTA.F40205 lyf ON IBLNTY=LFLNTY
            //                                where inventory.ItemId not in (" + itemNoString + @") AND iconfig.PrimaryUM IS NOT NULL AND lyf.LFIVI='Y' and iconfig.RouteEnabled = 1 
            //                                and iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 order by ItemNumber ASC";
            //            }
            //            else
            //            {
            //                query = @"select inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo', inventory.RouteId, inventory.OnHandQuantity AS 'OnHandQty',
            //                                inventory.CommittedQuantity AS 'CommittedQty', 
            //                                inventory.HeldQuantity AS 'HeldQty',inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription',
            //                                 (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty',
            //                                isnull(im.IMLNTY ,'')as 'StkType', isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4',
            //                                isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM'
            //                                from busdta.Inventory inventory left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
            //                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID 
            //                                LEFT JOIN BUSDTA.F4102 ib ON im.imitm = ib.ibitm
            //                                LEFT JOIN BUSDTA.F40205 lyf ON IBLNTY=LFLNTY
            //                                where inventory.ItemId not in (" + itemNoString + @") AND iconfig.PrimaryUM IS NOT NULL AND lyf.LFIVI='Y' and iconfig.RouteEnabled = 1 
            //                                and (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) > 0  
            //                                and iconfig.RouteEnabled = 1 and iconfig.allowSearch=1  order by ItemNumber ASC";
            //            }

            List<AddLoadDetails> activities = new List<AddLoadDetails>();
            try
            {
                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    activities = result.GetEntityList<AddLoadDetails>();
                }
                searchData = new ObservableCollection<AddLoadDetails>(activities);
                foreach (AddLoadDetails item in searchData)
                {
                    //string uomQuery = "SELECT UOM as 'UMCode', 1 as 'UMMultipler' FROM BUSDTA.ItemUoMs  WHERE ITEMID='" + item.ItemId + "' and  cansell=1 order by DisplaySeq";
                    //string uomQuery = "Call BUSDTA.SP_GetUMCode(@itemid='" + item.ItemId + "')";
                    //List<QtyUOMClass> qtyUOM = new List<QtyUOMClass>();
                    //DataSet resultUOM = DbEngine.ExecuteDataSet(uomQuery);
                    //if (resultUOM.HasData())
                    //{
                    //    qtyUOM = resultUOM.GetEntityList<QtyUOMClass>();

                    //}
                    //item.QtyUOMCollection = new ObservableCollection<QtyUOMClass>(qtyUOM);
                    item.QtyUOMCollection = GetUOMCode(item.ItemId);

                    //Set selected item in dropdown
                    QtyUOMClass objUM = item.QtyUOMCollection.FirstOrDefault(x => x.UMCode.Trim() == item.PrimaryUOM.Trim());
                    if (objUM != null)
                    {
                        item.SelectedQtyUOM = item.QtyUOMCollection.FirstOrDefault(x => x.UMCode.Trim() == item.PrimaryUOM.Trim());
                        item.QtyUOM = item.PrimaryUOM.Trim();
                    }
                    else
                    {
                        item.SelectedQtyUOM = item.QtyUOMCollection[0];
                        item.QtyUOM = item.QtyUOMCollection[0].UMCode;
                    }


                    //Set conversion factor

                    foreach (QtyUOMClass x in item.QtyUOMCollection)
                    {
                        x.UMMultipler = new TemplateManager().SetConversionFactorForItem(item.PrimaryUOM, x.UMCode, item.ItemId, item.ItemNo);
                    }


                    //Set maxload quantity 
                    item.MaxLoadQty = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck ? 9999 : item.MaxLoadQty;
                    item.IsFromBranchToTruck = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck;

                    //Disable check for max load if we are showing completed replenishment
                    item.IsFromBranchToTruck = PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet ? true : item.IsFromBranchToTruck;
                    item.MaxLoadQty = PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet ? 9999 : item.MaxLoadQty;

                    //Get Open Repln Qty for respected item 
                    #region Get Open Repln

                    string replnID = PayloadManager.RouteReplenishmentPayload.SelectedReplnId == "New" ? "0" : PayloadManager.RouteReplenishmentPayload.SelectedReplnId;
                    if (!PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                    {
                        //  string queryOpenRepln = @"SELECT A.ReplenishmentID ,A.ReplenishmentQty FROM BUSDTA.Route_Replenishment_Detail  A 
                        //                                            WHERE  A.ReplenishmentID 
                        //                                            IN  (SELECT B.ReplenishmentID FROM BUSDTA.Route_Replenishment_Header B 
                        //                                                    WHERE  B.StatusId NOT IN (SELECT C.StatusTypeID FROM BUSDTA.Status_Type C 
                        //                                                    WHERE C.StatusTypeCD IN ('CMPLT','VOID')) and B.ToBranchId ='" + UserManager.UserRoute + @"'
                        //                                                    )
                        //                                            AND A.ItemId='" + item.ItemId.Trim() + "' and A.ReplenishmentID != '" + replnID + "'";

                        //                        string queryOpenRepln = @"SELECT A.ReplenishmentID ,A.ReplenishmentQty FROM BUSDTA.Route_Replenishment_Detail  A 
                        //                                            WHERE  A.ReplenishmentID 
                        //                                            IN  (SELECT B.ReplenishmentID FROM BUSDTA.Route_Replenishment_Header B 
                        //                                                    WHERE  B.StatusId NOT IN (SELECT C.StatusTypeID FROM BUSDTA.Status_Type C 
                        //                                                    WHERE C.StatusTypeCD IN ('CMPLT','VOID')) and Trim(B.ToBranch) ='" + UserManager.UserBranch.Trim() + @"'
                        //                                                    )
                        //                                            AND A.ItemId='" + item.ItemId.Trim() + "' and A.ReplenishmentID != '" + replnID + "'";

                        // DataSet ds = DbEngine.ExecuteDataSet(queryOpenRepln);
                        DataSet ds = GetReplenQty(item.ItemId.Trim(), replnID);
                        if (ds.HasData())
                        {
                            int sum = 0;
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                //int replenId = Convert.ToInt32(ds.Tables[0].Rows[i]["ReplenishmentID"].ToString());
                                int loadQty = Convert.ToInt32(ds.Tables[0].Rows[i]["ReplenishmentQty"].ToString());
                                sum += loadQty;
                            }
                            item.OpenReplnQty = sum.ToString();
                        }
                        else
                        {
                            item.OpenReplnQty = "0";
                        }
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][GetAddLoadSearchItemsDetailsForBranchToTruck][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:GetAddLoadSearchItemsDetailsForBranchToTruck]");


            return searchData;
        }

        public static ObservableCollection<AddLoadDetails> GetSuggestionsReplnFromAlgo
        {
            get
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:GetSuggestionsReplnFromAlgo]");
                ObservableCollection<AddLoadDetails> searchData = new ObservableCollection<AddLoadDetails>();
                try
                {
                    SuggestionReplenishmentAlgo obj = new SuggestionReplenishmentAlgo();
                    searchData = new ObservableCollection<AddLoadDetails>(obj.GenerateReplishmentSuggestionList());
                    foreach (AddLoadDetails item in searchData)
                    {
                        //string uomQuery = "Call BUSDTA.SP_GetUMCode(@itemid='" + item.ItemId + "')";
                        ////string uomQuery = "SELECT UOM as 'UMCode', 1 as 'UMMultipler' FROM BUSDTA.ItemUoMs  WHERE ITEMID='" + item.ItemId + "' and    cansell=1 order by DisplaySeq";
                        //List<QtyUOMClass> qtyUOM = new List<QtyUOMClass>();
                        //DataSet resultUOM = DbEngine.ExecuteDataSet(uomQuery);
                        //if (resultUOM.HasData())
                        //{
                        //    qtyUOM = resultUOM.GetEntityList<QtyUOMClass>();

                        //}
                        //item.QtyUOMCollection = new ObservableCollection<QtyUOMClass>(qtyUOM);
                        item.QtyUOMCollection = GetUOMCode(item.ItemId);

                        //Set selected item in dropdown
                        QtyUOMClass objUM = item.QtyUOMCollection.FirstOrDefault(x => x.UMCode.Trim() == item.PrimaryUOM.Trim());
                        if (objUM != null)
                        {
                            item.SelectedQtyUOM = item.QtyUOMCollection.FirstOrDefault(x => x.UMCode.Trim() == item.PrimaryUOM.Trim());
                            item.QtyUOM = item.PrimaryUOM.Trim();
                        }
                        else
                        {
                            item.SelectedQtyUOM = item.QtyUOMCollection[0];
                            item.QtyUOM = item.QtyUOMCollection[0].UMCode;
                        }

                        //Set conversion factor

                        foreach (QtyUOMClass x in item.QtyUOMCollection)
                        {
                            x.UMMultipler = new TemplateManager().SetConversionFactorForItem(item.PrimaryUOM, x.UMCode, item.ItemId, item.ItemNo);
                        }



                        //Set maxload quantity 
                        item.MaxLoadQty = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck ? 9999 : item.MaxLoadQty;
                        item.IsFromBranchToTruck = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck;

                        //Disable check for max load if we are showing completed replenishment
                        item.IsFromBranchToTruck = PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet ? true : item.IsFromBranchToTruck;
                        item.MaxLoadQty = PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet ? 9999 : item.MaxLoadQty;

                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][GetSuggestionsReplnFromAlgo][ExceptionStackTrace = " + ex.StackTrace + "]");
                    //throw;
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:GetSuggestionsReplnFromAlgo]");


                return searchData;
            }
        }

        public static ObservableCollection<AddLoadDetails> GetSuggestionsReturnFromAlgo
        {
            get
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:GetSuggestionsReturnFromAlgo]");
                ObservableCollection<AddLoadDetails> searchData = new ObservableCollection<AddLoadDetails>();

                try
                {
                    SuggestionReturnAlgo obj = new SuggestionReturnAlgo();
                    searchData = new ObservableCollection<AddLoadDetails>(obj.GenerateSuggestionReturnList());
                    foreach (AddLoadDetails item in searchData)
                    {
                        string uomQuery = "SELECT UOM as 'UMCode', 1 as 'UMMultipler' FROM BUSDTA.ItemUoMs  WHERE ITEMID='" + item.ItemId + "' and    cansell=1 order by DisplaySeq";
                        List<QtyUOMClass> qtyUOM = new List<QtyUOMClass>();
                        DataSet resultUOM = DbEngine.ExecuteDataSet(uomQuery);
                        if (resultUOM.HasData())
                        {
                            qtyUOM = resultUOM.GetEntityList<QtyUOMClass>();

                        }
                        item.QtyUOMCollection = new ObservableCollection<QtyUOMClass>(qtyUOM);

                        //Set selected item in dropdown
                        QtyUOMClass objUM = item.QtyUOMCollection.FirstOrDefault(x => x.UMCode.Trim() == item.PrimaryUOM.Trim());
                        if (objUM != null)
                        {
                            item.SelectedQtyUOM = item.QtyUOMCollection.FirstOrDefault(x => x.UMCode.Trim() == item.PrimaryUOM.Trim());
                            item.QtyUOM = item.PrimaryUOM.Trim();
                        }
                        else
                        {
                            item.SelectedQtyUOM = item.QtyUOMCollection[0];
                            item.QtyUOM = item.QtyUOMCollection[0].UMCode;
                        }


                        //Set conversion factor
                        List<QtyUOMClass> objRemove = new List<QtyUOMClass>();

                        foreach (QtyUOMClass x in item.QtyUOMCollection)
                        {
                            x.UMMultipler = new TemplateManager().SetConversionFactorForItem(item.PrimaryUOM, x.UMCode, item.ItemId, item.ItemNo);
                            if (x.UMMultipler > item.AvailableQty)
                            {
                                objRemove.Add(x);
                            }
                        }
                        foreach (QtyUOMClass y in objRemove)
                        {
                            item.QtyUOMCollection.Remove(y);
                        }

                        if (item.QtyUOM != item.PrimaryUOM)
                        {
                            item.MaxLoadQty = item.AvailableQty / Convert.ToInt32(item.SelectedQtyUOM.UMMultipler);
                        }
                        //Set maxload quantity 
                        item.MaxLoadQty = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck ? 9999 : item.MaxLoadQty;
                        item.IsFromBranchToTruck = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck;

                        //Disable check for max load if we are showing completed replenishment
                        item.IsFromBranchToTruck = PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet ? true : item.IsFromBranchToTruck;
                        item.MaxLoadQty = PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet ? 9999 : item.MaxLoadQty;

                        //************************************************************************************************
                        // Comment: Added to updated the suggested return qty to the return qty
                        // Created: Jan 24, 2016
                        // Author: Vivensas (Rajesh,Yuvaraj)
                        // Revisions: 
                        //*************************************************************************************************

                        if (PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                        {
                            item.MaxLoadQty = item.LoadQuantity;
                        }
                        //*************************************************************************************************
                        // Vivensas changes ends over here
                        //**************************************************************************************************
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][GetSuggestionsReturnFromAlgo][ExceptionStackTrace = " + ex.StackTrace + "]");
                    //throw;
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:GetSuggestionsReturnFromAlgo]");


                return searchData;
            }
        }

        public static ObservableCollection<AddLoadDetails> GetAddLoadDetails(string replnID, bool isCompletedReplenishment)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:GetAddLoadDetails]");
            ObservableCollection<AddLoadDetails> searchData = new ObservableCollection<AddLoadDetails>();
            string query = string.Empty;
            query = "CALL BUSDTA.SP_GetAddLoadDetails(@replnID = '" + replnID + "',@isCompletedRepln = " + (isCompletedReplenishment.ToString() == "True" ? "1" : "0").ToString() + ")";
            //DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
            //            if (isCompletedReplenishment)
            //            {
            //                //Here i have used A.SequenceNumber for stored held qty of respective replenishment. Which is used for one time only
            //                //A.CurrentAvailability persists committed qty
            //                query = @"select inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
            //                                , inventory.RouteId,A.parlevelqty AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription',
            //                                ISNULL(A.ShippedQty,0) as 'ShippedQty',
            //                                isnull(A.AvailaibilityAtTime,0)-isnull(A.CurrentAvailability,0)-isnull(A.SequenceNumber ,0) as 'AvailableQty'
            //                                ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', A.PickedQty as 'LoadQuantity',A.AdjustmentQty as 'SuggestedQty', 
            //                                A.ReplenishmentQtyUM as 'QtyUOM',A.PickedQty as 'PickedQty'
            //                                ,A.DemandQty AS 'DemandQty',A.OpenReplnQty AS  'OpenReplnQty'
            //                                , A.AvailaibilityAtTime AS 'OnHandQty',ISNULL(A.CurrentAvailability,0) AS 'CommittedQty'
            //                                ,A.SequenceNumber AS 'HeldQty'
            //                                ,isnull(im.IMLNTY ,'')as 'StkType'
            //                                , isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4'
            //                                from busdta.Route_Replenishment_Detail A 
            //                                left join busdta.Inventory inventory on A.ItemId=inventory.ItemId
            //                                left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
            //                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and A.ReplenishmentID='" + replnID + @"'
            //                                order by A.ReplenishmentDetailID asc";

            //            }
            //            else
            //            {

            //                query = @"select inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo', inventory.RouteId,inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription',
            //                                                ISNULL(A.ShippedQty,0) as 'ShippedQty'
            //                                                , (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty',
            //                                                isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', A.ReplenishmentQty as 'LoadQuantity',A.AdjustmentQty as 'SuggestedQty',A.ReplenishmentQtyUM as 'QtyUOM',A.PickedQty as 'PickedQty'
            //                                                ,A.DemandQty AS 'DemandQty',A.OpenReplnQty AS  'OpenReplnQty'
            //                                                , inventory.OnHandQuantity AS 'OnHandQty',inventory.CommittedQuantity AS 'CommittedQty'
            //                                                ,inventory.HeldQuantity AS 'HeldQty'
            //                                                ,isnull(im.IMLNTY ,'')as 'StkType', isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4'
            //                                                from busdta.Route_Replenishment_Detail A 
            //                                                left join busdta.Inventory inventory on A.ItemId=inventory.ItemId
            //                                                left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
            //                                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1  and A.ReplenishmentID='" + replnID + @"'
            //                                                order by A.ReplenishmentDetailID asc";
            //            }

            List<AddLoadDetails> activities = new List<AddLoadDetails>();
            try
            {
                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    activities = result.GetEntityList<AddLoadDetails>();
                }
                searchData = new ObservableCollection<AddLoadDetails>(activities);
                foreach (AddLoadDetails item in searchData)
                {
                    //string uomQuery = "SELECT UOM as 'UMCode', 1 as 'UMMultipler' FROM BUSDTA.ItemUoMs  WHERE ITEMID='" + item.ItemId + "' AND   cansell=1 order by DisplaySeq ";
                    //string uomQuery = "Call BUSDTA.SP_GetUMCode(@itemid='" + item.ItemId + "')";
                    //List<QtyUOMClass> qtyUOM = new List<QtyUOMClass>();
                    //DataSet resultUOM = DbEngine.ExecuteDataSet(uomQuery);
                    //if (resultUOM.HasData())
                    //{
                    //    qtyUOM = resultUOM.GetEntityList<QtyUOMClass>();

                    //}
                    //item.QtyUOMCollection = new ObservableCollection<QtyUOMClass>(qtyUOM);

                    //****************
                    //create the new Function for UOMCode Deatils
                    item.QtyUOMCollection = GetUOMCode(item.ItemId);

                    //Set selected item in dropdown
                    QtyUOMClass objUM = item.QtyUOMCollection.FirstOrDefault(x => x.UMCode.Trim() == (string.IsNullOrEmpty(item.QtyUOM) ? item.PrimaryUOM.Trim() : item.QtyUOM));
                    if (objUM != null)
                    {
                        item.SelectedQtyUOM = item.QtyUOMCollection.FirstOrDefault(x => x.UMCode.Trim() == (string.IsNullOrEmpty(item.QtyUOM) ? item.PrimaryUOM.Trim() : item.QtyUOM));
                        item.QtyUOM = item.SelectedQtyUOM.UMCode; //item.PrimaryUOM.Trim();
                    }
                    else
                    {
                        item.SelectedQtyUOM = item.QtyUOMCollection[0];
                        item.QtyUOM = item.QtyUOMCollection[0].UMCode;
                    }

                    if (!isCompletedReplenishment && item.OpenReplnQty == "0")
                    {
                        //Get Open Repln Qty for respected item 
                        #region Get Open Repln
                        //                        string queryOpenRepln = @"SELECT A.ReplenishmentID ,A.ReplenishmentQty FROM BUSDTA.Route_Replenishment_Detail  A 
                        //                                            WHERE  A.ReplenishmentID 
                        //                                            IN  (SELECT B.ReplenishmentID FROM BUSDTA.Route_Replenishment_Header B 
                        //                                                    WHERE  B.StatusId NOT IN (SELECT C.StatusTypeID FROM BUSDTA.Status_Type C 
                        //                                                    WHERE C.StatusTypeCD IN ('CMPLT','VOID')) and B.ToBranchId ='" + UserManager.UserRoute + @"'
                        //                                                    )
                        //                                            AND A.ItemId='" + item.ItemId.Trim() + "' and A.ReplenishmentID != '" + replnID + "'";


                        //string queryOpenRepln = @"CALL BUSDTA.SP_GetReplenQty(@ToBranch = '" + UserManager.UserBranch.Trim() + @"',@ItemId =" + item.ItemId.Trim() + ",@ReplenishmentID = '" + replnID + "')";
                        //                        string queryOpenRepln = @"SELECT A.ReplenishmentID ,A.ReplenishmentQty FROM BUSDTA.Route_Replenishment_Detail  A 
                        //                                            WHERE  A.ReplenishmentID 
                        //                                            IN  (SELECT B.ReplenishmentID FROM BUSDTA.Route_Replenishment_Header B 
                        //                                                    WHERE  B.StatusId NOT IN (SELECT C.StatusTypeID FROM BUSDTA.Status_Type C 
                        //                                                    WHERE C.StatusTypeCD IN ('CMPLT','VOID')) and Trim(B.ToBranch) ='" + UserManager.UserBranch.Trim() + @"'
                        //                                                    )
                        //                                            AND A.ItemId='" + item.ItemId.Trim() + "' and A.ReplenishmentID != '" + replnID + "'";

                        //DataSet ds = DbEngine.ExecuteDataSet(queryOpenRepln);
                        DataSet ds = GetReplenQty(item.ItemId.Trim(), replnID);
                        if (ds.HasData())
                        {
                            int sum = 0;
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                int replenId = Convert.ToInt32(ds.Tables[0].Rows[i]["ReplenishmentID"].ToString());
                                int loadQty = Convert.ToInt32(ds.Tables[0].Rows[i]["ReplenishmentQty"].ToString());
                                if (replenId < Convert.ToInt32(replnID))
                                {
                                    sum += loadQty;
                                }
                            }
                            item.OpenReplnQty = sum.ToString();
                        }
                        else
                        {
                            item.OpenReplnQty = "0";
                        }
                        #endregion
                    }



                    //Set conversion factor

                    foreach (QtyUOMClass x in item.QtyUOMCollection)
                    {
                        x.UMMultipler = new TemplateManager().SetConversionFactorForItem(item.PrimaryUOM, x.UMCode, item.ItemId, item.ItemNo);
                    }

                    //Set maxload quantity 
                    item.MaxLoadQty = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck ? 9999 : item.MaxLoadQty;
                    item.IsFromBranchToTruck = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck;

                    //Disable check for max load if we are showing completed replenishment
                    item.IsFromBranchToTruck = PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet ? true : item.IsFromBranchToTruck;
                    item.MaxLoadQty = PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet ? 9999 : item.MaxLoadQty;

                    //Held quantity adjustment in case of completed replenishment
                    if (PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet && item.ShippedQty == 0 && !PayloadManager.RouteReplenishmentPayload.IsSalesBranch
                        && (PayloadManager.RouteReplenishmentPayload.CurrentViewName.ToString() == ViewModelMappings.View.ReplenishAddLoadView.ToString()
                        || PayloadManager.RouteReplenishmentPayload.CurrentViewName.ToString() == ViewModelMappings.View.ReplenishSggestionView.ToString()))
                    {
                        item.HeldQty = 0;
                        item.AvailableQty = item.OnHandQty - item.CommittedQty;
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][GetAddLoadDetails][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:GetAddLoadDetails]");


            return searchData;
        }

        public static ObservableCollection<AddLoadDetails> GetItemDetailsForReplenishment
        {
            get
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:GetItemDetailsForReplenishment]");
                ObservableCollection<AddLoadDetails> searchData = new ObservableCollection<AddLoadDetails>();
                string query = string.Empty;

                //            query = @"SELECT INV.OnHandQuantity AS 'OnHandQty'
                //                        ,INV.CommittedQuantity  AS 'CommittedQty'
                //                        ,INV.HeldQuantity AS 'HeldQty',INV.ParLevel AS 'ParLevel'
                //                        ,PD.PickedQty 
                //                        ,PD.ItemID AS 'ItemId' 
                //                        ,PD.TransactionDetailID AS 'ReplnDetailId'
                //                        ,PD.TransactionID
                //                        from BUSDTA.Inventory  INV 
                //                        JOIN   BUSDTA.Pick_Detail PD ON INV.ItemId=PD.ItemID  
                //                        WHERE PD.TransactionID=" + PayloadManager.RouteReplenishmentPayload.SelectedReplnId + " ";
                query = "CALL BUSDTA.SP_GetItemDetailsForReplen(@TransactionID = " + PayloadManager.RouteReplenishmentPayload.SelectedReplnId + ")";



                List<AddLoadDetails> activities = new List<AddLoadDetails>();
                try
                {
                    DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                    if (result.HasData())
                    {
                        activities = result.GetEntityList<AddLoadDetails>();
                    }
                    searchData = new ObservableCollection<AddLoadDetails>(activities);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][GetItemDetailsForReplenishment][ExceptionStackTrace = " + ex.StackTrace + "]");
                    //throw;
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:GetItemDetailsForReplenishment]");


                return searchData;
            }
        }

        public static ObservableCollection<AddLoadDetails> GetHeldReturnDetails(string replnID, bool isCompletedReplenishment)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:GetHeldReturnDetails]");
            ObservableCollection<AddLoadDetails> searchData = new ObservableCollection<AddLoadDetails>();
            string query = string.Empty;
            query = " CALL BUSDTA.SP_GetHeldReturnDetailsByReplnID(@isCompletedReplenishment =" + (isCompletedReplenishment ? "1" : "0").ToString() + ",@ReplenishmentID = " + replnID + ")";
            //            if (isCompletedReplenishment)
            //            {
            //            query = @"SELECT OH.OrderDate, OD.OrderID,  RRD.ReplenishmentDetailID AS 'TransactionDetailID',            
            //                        isnull( im.IMDSC1,'') as 'ItemDescription',
            //                        isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM',
            //                        OD.ReturnHeldQty 'LoadQuantity',OD.OrderUM as 'QtyUOM',OD.ItemId,
            //                        LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo',
            //                        inventory.ItemId AS 'ItemId',inventory.RouteId,
            //                        RRD.AvailaibilityAtTime AS 'OnHandQty'
            //                        ,RRD.CurrentAvailability AS 'CommittedQty', 
            //                        inventory.ParLevel AS 'ParLevel'
            //                        ,RRD.SequenceNumber AS 'HeldQty',
            //                        isnull(RRD.AvailaibilityAtTime,0)-isnull(RRD.CurrentAvailability,0)-isnull(RRD.SequenceNumber ,0)  as 'AvailableQty'
            //                        , isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4'
            //                        FROM BUSDTA.Route_Replenishment_Detail  RRD
            //                        JOIN busdta.Order_Detail OD ON RRD.OrderID=OD.OrderID  AND RRD.ItemId=OD.ItemId
            //                        JOIN busdta.ORDER_HEADER OH on OD.OrderID=OH.OrderID
            //                        join BUSDTA.Inventory inventory on inventory.ItemId=OD.ItemId
            //                        join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
            //                        join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID where iconfig.RouteEnabled = 1 
            //                        and iconfig.allowSearch=1  AND RRD.ReplenishmentID=" + replnID + "";
            //            }
            //            else
            //            {
            //            query = @"SELECT distinct OH.OrderDate, OD.OrderID,  RRD.ReplenishmentDetailID AS 'TransactionDetailID',            
            //                        isnull( im.IMDSC1,'') as 'ItemDescription',
            //                        isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM',
            //                        OD.ReturnHeldQty 'LoadQuantity',OD.OrderUM as 'QtyUOM',OD.ItemId,
            //                        LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo',
            //                        inventory.ItemId AS 'ItemId',inventory.RouteId,
            //                        inventory.OnHandQuantity AS 'OnHandQty',inventory.CommittedQuantity AS 'CommittedQty', 
            //                        inventory.ParLevel AS 'ParLevel',inventory.HeldQuantity AS 'HeldQty',
            //                        (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
            //                        , isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4'
            //                        FROM BUSDTA.Route_Replenishment_Detail  RRD
            //                        JOIN busdta.Order_Detail OD ON RRD.OrderID=OD.OrderID  AND RRD.ItemId=OD.ItemId
            //                        JOIN busdta.ORDER_HEADER OH on OD.OrderID=OH.OrderID
            //                        join BUSDTA.Inventory inventory on inventory.ItemId=OD.ItemId
            //                        join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
            //                        join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID where iconfig.RouteEnabled = 1 
            //                        and iconfig.allowSearch=1  AND RRD.ReplenishmentID=" + replnID + "";
            //            }


            List<AddLoadDetails> activities = new List<AddLoadDetails>();
            try
            {
                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    activities = result.GetEntityList<AddLoadDetails>();
                }
                searchData = new ObservableCollection<AddLoadDetails>(activities);
                foreach (AddLoadDetails item in searchData)
                {
                    //Set conversion factor

                    foreach (QtyUOMClass x in item.QtyUOMCollection)
                    {
                        x.UMMultipler = new TemplateManager().SetConversionFactorForItem(item.PrimaryUOM, x.UMCode, item.ItemId, item.ItemNo);
                    }

                    //Set maxload quantity 
                    item.MaxLoadQty = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck ? 9999 : item.MaxLoadQty;
                    item.IsFromBranchToTruck = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck;

                    //Disable check for max load if we are showing completed replenishment
                    item.IsFromBranchToTruck = PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet ? true : item.IsFromBranchToTruck;
                    item.MaxLoadQty = PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet ? 9999 : item.MaxLoadQty;
                }

                //Check if any new held return which  are not present in current Held Return Replenishment. If found then add then in collection

                ObservableCollection<AddLoadDetails> newSearchData = new ObservableCollection<AddLoadDetails>();
                if (!isCompletedReplenishment)
                {
//                    query = @"select OH.OrderDate, OD.OrderID,
//                        isnull( im.IMDSC1,'') as 'ItemDescription',
//                        isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM',
//                        ISNULL(OD.ReturnHeldQty,0) 'LoadQuantity',OD.OrderUM as 'QtyUOM',OD.ItemId,
//                        LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo',
//                        inventory.ItemId AS 'ItemId',inventory.RouteId,
//                        inventory.OnHandQuantity AS 'OnHandQty',inventory.CommittedQuantity AS 'CommittedQty', 
//                        inventory.ParLevel AS 'ParLevel',inventory.HeldQuantity AS 'HeldQty',
//                        (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
//                        from busdta.Order_Detail OD 
//                        join busdta.ORDER_HEADER OH on OD.OrderID=OH.OrderID
//                        join BUSDTA.Inventory inventory on inventory.ItemId=OD.ItemId
//                        join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
//                        join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 
//                        AND inventory.HeldQuantity!=0 AND ISNULL(OD.ReturnHeldQty,0)!=0 
//                        AND OH.OrderTypeId=( SELECT STATUSTYPEID FROM BUSDTA.Status_Type  WHERE STATUSTYPECD='RETORD') 
//                        AND OD.ORDERID NOT IN (SELECT distinct(isnull(ORDERID,0)) FROM BUSDTA.Route_Replenishment_Detail )    ";

                    query = " CALL BUSDTA.SP_GetHeldReturnDetails()";

                    DataSet newHeldReturn = Helpers.DbEngine.ExecuteDataSet(query);
                    List<AddLoadDetails> newactivities = new List<AddLoadDetails>();
                    if (newHeldReturn.HasData())
                    {
                        newactivities = newHeldReturn.GetEntityList<AddLoadDetails>();
                    }
                    newSearchData = new ObservableCollection<AddLoadDetails>(newactivities);
                    foreach (AddLoadDetails item in newSearchData)
                    {
                        //Set conversion factor

                        foreach (QtyUOMClass x in item.QtyUOMCollection)
                        {
                            x.UMMultipler = new TemplateManager().SetConversionFactorForItem(item.PrimaryUOM, x.UMCode, item.ItemId, item.ItemNo);
                        }

                        //Set maxload quantity 
                        item.MaxLoadQty = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck ? 9999 : item.MaxLoadQty;
                        item.IsFromBranchToTruck = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck;

                        //Disable check for max load if we are showing completed replenishment
                        item.IsFromBranchToTruck = PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet ? true : item.IsFromBranchToTruck;
                        item.MaxLoadQty = PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet ? 9999 : item.MaxLoadQty;
                    }
                }

                //Add new held return order to exsting collection
                foreach (AddLoadDetails item in newSearchData)
                {
                    searchData.Add(item);
                }
                //Insert new records in BUSDTA.ROUTE_REPLENISHMENT_DETAILS
                if (newSearchData.Count > 0)
                {
                    SaveHeldReturn(newSearchData, replnID);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][GetHeldReturnDetails][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:GetHeldReturnDetails]");


            return searchData;
        }

        public static ObservableCollection<AddLoadDetails> GetNewHeldReturn
        {
            get
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:GetNewHeldReturn]");
                ObservableCollection<AddLoadDetails> searchData = new ObservableCollection<AddLoadDetails>();
                string query = string.Empty;

                //            query = @"select OH.OrderDate, OD.OrderID,
                //                        isnull( im.IMDSC1,'') as 'ItemDescription',
                //                        isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM',
                //                        ISNULL(OD.ReturnHeldQty,0) 'LoadQuantity',OD.OrderUM as 'QtyUOM',OD.ItemId,
                //                        LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo',
                //                        inventory.ItemId AS 'ItemId',inventory.RouteId,
                //                        inventory.OnHandQuantity AS 'OnHandQty',inventory.CommittedQuantity AS 'CommittedQty', 
                //                        inventory.ParLevel AS 'ParLevel',inventory.HeldQuantity AS 'HeldQty',
                //                        (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
                //                        from busdta.Order_Detail OD 
                //                        join busdta.ORDER_HEADER OH on OD.OrderID=OH.OrderID
                //                        join BUSDTA.Inventory inventory on inventory.ItemId=OD.ItemId
                //                        join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
                //                        join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 
                //                        AND inventory.HeldQuantity!=0 AND ISNULL(OD.ReturnHeldQty,0)!=0 AND  OH.OrderTypeId=( SELECT STATUSTYPEID FROM BUSDTA.Status_Type  WHERE STATUSTYPECD='RETORD')
                //                        AND OD.ORDERID NOT IN (SELECT distinct(isnull(ORDERID,0)) FROM BUSDTA.Route_Replenishment_Detail ) ";

                query = " CALL BUSDTA.SP_GetHeldReturnDetails()";
                List<AddLoadDetails> activities = new List<AddLoadDetails>();
                try
                {
                    DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                    if (result.HasData())
                    {
                        activities = result.GetEntityList<AddLoadDetails>();
                    }
                    searchData = new ObservableCollection<AddLoadDetails>(activities);
                    foreach (AddLoadDetails item in searchData)
                    {
                        //Set conversion factor
                        foreach (QtyUOMClass x in item.QtyUOMCollection)
                        {
                            x.UMMultipler = new TemplateManager().SetConversionFactorForItem(item.PrimaryUOM, x.UMCode, item.ItemId, item.ItemNo);
                        }

                        //Set maxload quantity 
                        item.MaxLoadQty = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck ? 9999 : item.MaxLoadQty;
                        item.IsFromBranchToTruck = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck;

                        //Disable check for max load if we are showing completed replenishment
                        item.IsFromBranchToTruck = PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet ? true : item.IsFromBranchToTruck;
                        item.MaxLoadQty = PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet ? 9999 : item.MaxLoadQty;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][GetNewHeldReturn][ExceptionStackTrace = " + ex.StackTrace + "]");
                    //throw;
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:GetNewHeldReturn]");


                return searchData;
            }
        }

        public static string GetBranch(string branch)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:GetBranch]");
            string BrnchNo = branch;
            for (int i = BrnchNo.Length; i <= 11; i++)
            {
                BrnchNo = " " + BrnchNo;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:GetBranch]");
            return BrnchNo;
        }

        public static string InsertReplenishmentHeader(string status, string fromBranch, string toBranch, string replnType)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:InsertAddLoadHeader]");
            //bool flag = false;
            string replnID = string.Empty;
            string StatusId = "0";
            string replnTypeID = "0";
            string fromDate = PayloadManager.RouteReplenishmentPayload.FromDate.ToString("yyyy-MM-dd");
            string toDate = PayloadManager.RouteReplenishmentPayload.ToDate.ToString("yyyy-MM-dd");
            string query = "CALL BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD = '" + status + "')";
            try
            {
                StatusId = DbEngine.ExecuteScalar(query);
                //query = "select TTID from BUSDTA.M5001 where TTKEY='" + replnType + "'";
                query = "CALL BUSDTA.SP_GetTTIDByTTKEY(@TTKEY = '" + replnType + "')";
                replnTypeID = DbEngine.ExecuteScalar(query);
                int NextNo = GetNewAddLoadID;
                if (NextNo != 0)
                {
                    replnID = Convert.ToString(NextNo);
                    string queryToInsert = "insert into BUSDTA.Route_Replenishment_Header(ReplenishmentID, RouteId,StatusId,TransDateFrom,TransDateTo, " +
                        " RequestedBy,FromBranch,ToBranch,CreatedBy,CreatedDatetime,ReplenishmentTypeId,UpdatedBy, UpdatedDatetime)  " +
                        " VALUES ('" + replnID + "', '" + CommonNavInfo.RouteID + "', '" + StatusId + "','" + fromDate + "','" + toDate + "','" + Managers.UserManager.UserId + "','" + GetBranch(fromBranch) + "', '" + GetBranch(toBranch) + "','" + Managers.UserManager.UserId + "',getdate(),'" + replnTypeID + "','" + Managers.UserManager.UserId + "',getdate())";


                    //s
                    int i = DbEngine.ExecuteNonQuery(queryToInsert);
                    if (i > 0)
                    {
                        //flag = true;

                        //string queryReplnID = "SELECT A.ReplenishmentID FROM MobileDataModel.BUSDTA.Route_Replenishment_Header  A  " +
                        //                        " where A.StatusId='" + StatusId + "' and A.ReplenishmentTypeId='" + replnTypeID + "'";

                        string queryReplnID = "CALL BUSDTA.SP_GetReplenishmentID(@StatusId = '" + StatusId + "',@ReplenishmentTypeId = '" + replnTypeID + "')";
                        replnID = DbEngine.ExecuteScalar(queryReplnID);


                    }
                }
                ResourceManager.QueueManager.QueueProcess("Replenishments", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][InsertAddLoadHeader][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:InsertAddLoadHeader]");

            return replnID;
        }

        public static ObservableCollection<AddLoadPick> GetAddLoadPickCollection(string replnID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:GetAddLoadPickCollection]");
            ObservableCollection<AddLoadPick> searchData = new ObservableCollection<AddLoadPick>();
            //string transQty = PayloadManager.RouteReplenishmentPayload.IsSalesBranch ? "A.ReplenishmentQty" : "A.ShippedQty";
            //string pickedQty = "isnull(pd.PickQtyPrimaryUOM, 0)";
            //string onHandQty = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck ? "isnull(inventory.OnHandQuantity,0) + " + pickedQty + "" : "isnull(inventory.OnHandQuantity,0) - " + pickedQty + "";
//            string query = @"select A.ReplenishmentDetailID as 'TransactionDetailID'
//                                , inventory.ItemId AS 'ItemId'
//                                , LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNumber'
//                                ,isnull( im.IMDSC1,'') as 'ItemDescription'
//                                , ISNULL(A.ShippedQty,0) AS 'ShippedQty'
//                                ," + onHandQty + @" as 'ActualQtyOnHand'
//                                ,(" + onHandQty + @" -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
//                                ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUM'
//                                ," + transQty + @" as 'TransactionQty'
//                                ,A.ReplenishmentQty as 'TransactionQtyDisplay'
//                                ,A.ReplenishmentQtyUM as 'TransactionUOM'
//                                ,pd.PickQtyPrimaryUOM
//                                ,(A.PickedQty -A.ReplenishmentQty) as 'ExceptionCount'
//                                ,'" + PayloadManager.RouteReplenishmentPayload.SelectedReplnId.ToString() + @"' as ReplnId   
//                                ,pd.TransactionQtyPrimaryUOM  AS 'TransactionQtyPrimaryUOM',inventory.ParLevel
//                                ,A.ReplenishmentQtyUM  AS 'UM', isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4'
//                                from busdta.Route_Replenishment_Detail A 
//                                left join busdta.PICK_DETAIL pd on pd.TransactionDetailID=A.ReplenishmentDetailID and A.ItemId=pd.ItemID  and pd.TransactionID=A.ReplenishmentID 
//                                left join busdta.Inventory inventory on A.ItemId=inventory.ItemId
//                                left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
//                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID 
//                                left outer join busdta.Status_Type ST on A.PickedStatusTypeID = ST.StatusTypeID
//                                where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and A.ReplenishmentID='" + replnID + @"'  
//                                AND A.ReplenishmentQty!=0 and " + transQty + @"!=0 
//                                order by  A.ReplenishmentDetailID asc ";
            string query = "CALL BUSDTA.SP_GetAddLoadPickCollection (@ReplnId='" + replnID + "',@IsSalesBranch=" + (PayloadManager.RouteReplenishmentPayload.IsSalesBranch ? "1" : "0").ToString() + ",@IsFromBranchToTruck=" + (PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck ? "1" : "0").ToString() + ")";
            List<AddLoadPick> activities = new List<AddLoadPick>();
            try
            {
                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    activities = result.GetEntityList<AddLoadPick>();
                }
                searchData = new ObservableCollection<AddLoadPick>(activities);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][GetAddLoadPickCollection][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:GetAddLoadPickCollection]");


            return searchData;
        }

        public static ObservableCollection<AddLoadPick> GetUnloadPickCollection(string replnID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:GetUnloadPickCollection]");
            ObservableCollection<AddLoadPick> searchData = new ObservableCollection<AddLoadPick>();
            //string transQty = "A.ReplenishmentQty";
            //            string query = @"select A.ReplenishmentDetailID as 'TransactionDetailID'
            //                                , inventory.ItemId AS 'ItemId'
            //                                , LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNumber'
            //                                ,isnull( im.IMDSC1,'') as 'ItemDescription'
            //                                , ISNULL(A.ShippedQty,0) AS 'ShippedQty'
            //                                ,isnull(inventory.OnHandQuantity,0)  as 'ActualQtyOnHand'
            //                                ,isnull(inventory.CommittedQuantity,0)  as 'CommittedQty'
            //                                ,isnull(inventory.HeldQuantity,0)  as 'HeldQty'
            //                                ,(isnull(inventory.OnHandQuantity,0)  -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
            //                                ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUM'
            //                                ,A.ReplenishmentQty as 'TransactionQty'
            //                                ,A.ReplenishmentQty as 'TransactionQtyDisplay'
            //                                ,A.ReplenishmentQtyUM as 'TransactionUOM'
            //                                ,pd.PickQtyPrimaryUOM
            //                                ,(A.PickedQty -A.ReplenishmentQty) as 'ExceptionCount'
            //                                ,'" + PayloadManager.RouteReplenishmentPayload.SelectedReplnId.ToString() + @"' as ReplnId   
            //                                ,'0' AS 'TransactionQtyPrimaryUOM',inventory.ParLevel
            //                                ,A.ReplenishmentQtyUM  AS 'UM', isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4'
            //                                from busdta.Route_Replenishment_Detail A 
            //                                left join busdta.PICK_DETAIL pd on pd.TransactionDetailID=A.ReplenishmentDetailID and A.ItemId=pd.ItemID  and pd.TransactionID=A.ReplenishmentID 
            //                                left join busdta.Inventory inventory on A.ItemId=inventory.ItemId
            //                                left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
            //                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID 
            //                                left outer join busdta.Status_Type ST on A.PickedStatusTypeID = ST.StatusTypeID
            //                                where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and A.ReplenishmentID='" + replnID + @"'  
            //                                AND A.ReplenishmentQty!=0 
            //                                order by  A.ReplenishmentDetailID asc ";

            string query = "CALL BUSDTA.SP_GetUnloadPickCollection(@replnID = " + replnID + ",@SelectedReplnId = '" + PayloadManager.RouteReplenishmentPayload.SelectedReplnId.ToString() + "')";
            List<AddLoadPick> activities = new List<AddLoadPick>();
            try
            {
                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    activities = result.GetEntityList<AddLoadPick>();
                }
                searchData = new ObservableCollection<AddLoadPick>(activities);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][GetUnloadPickCollection][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:GetUnloadPickCollection]");


            return searchData;
        }

        public static ObservableCollection<AddLoadPick> GetSuggReturnPickCollection(string replnID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:GetSuggReturnPickCollection]");
            ObservableCollection<AddLoadPick> searchData = new ObservableCollection<AddLoadPick>();
//            string transQty = "A.ReplenishmentQty";
//            string onHandQty = "isnull(inventory.OnHandQuantity,0) ";
//            string query = @"select A.ReplenishmentDetailID as 'TransactionDetailID'
//                                , inventory.ItemId AS 'ItemId'
//                                , LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNumber'
//                                ,isnull( im.IMDSC1,'') as 'ItemDescription'
//                                , ISNULL(A.ShippedQty,0) AS 'ShippedQty'
//                                ," + onHandQty + @" as 'ActualQtyOnHand'
//                                ,(" + onHandQty + @" -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
//                                ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUM'
//                                ," + transQty + @" as 'TransactionQty'
//                                ,A.ReplenishmentQty as 'TransactionQtyDisplay'
//                                ,A.ReplenishmentQtyUM as 'TransactionUOM'
//                                 ,pd.PickQtyPrimaryUOM
//                                ,(A.PickedQty -A.ReplenishmentQty) as 'ExceptionCount'
//                                ,'" + PayloadManager.RouteReplenishmentPayload.SelectedReplnId.ToString() + @"' as ReplnId   
//                                ,'0' AS 'TransactionQtyPrimaryUOM',inventory.ParLevel
//                                ,A.ReplenishmentQtyUM  AS 'UM', isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4'
//                                from busdta.Route_Replenishment_Detail A 
//                                left join busdta.PICK_DETAIL pd on pd.TransactionDetailID=A.ReplenishmentDetailID and A.ItemId=pd.ItemID  and pd.TransactionID=A.ReplenishmentID 
//                                left join busdta.Inventory inventory on A.ItemId=inventory.ItemId
//                                left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
//                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID 
//                                left outer join busdta.Status_Type ST on A.PickedStatusTypeID = ST.StatusTypeID
//                                where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and A.ReplenishmentID='" + replnID + @"'  
//                                AND A.ReplenishmentQty!=0 
//                                order by  A.ReplenishmentDetailID asc ";
            string query = "CALL BUSDTA.SP_GetSuggReturnPickCollection(@replnID='" + replnID + @"',@SelectedReplnId='" + PayloadManager.RouteReplenishmentPayload.SelectedReplnId.ToString() + @"')";
            List<AddLoadPick> activities = new List<AddLoadPick>();
            try
            {
                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    activities = result.GetEntityList<AddLoadPick>();
                }
                searchData = new ObservableCollection<AddLoadPick>(activities);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][GetSuggReturnPickCollection][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:GetSuggReturnPickCollection]");


            return searchData;
        }

        public static ObservableCollection<AddLoadPick> GetHeldReturnPickCollection(string replnID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:GetHeldReturnPickCollection]");
            ObservableCollection<AddLoadPick> searchData = new ObservableCollection<AddLoadPick>();


            //            string query = @"Select distinct A.ReplenishmentDetailID as 'TransactionDetailID',A.OrderID
            //                                , inventory.ItemId AS 'ItemId'
            //                                , LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNumber'
            //                                ,isnull( im.IMDSC1,'') as 'ItemDescription'
            //                                ,isnull(inventory.OnHandQuantity,0) as 'ActualQtyOnHand'
            //                                ,(isnull(inventory.OnHandQuantity,0)   -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
            //                                , isnull(inventory.HeldQuantity,0) as 'HeldQty'
            //                                ,isnull(inventory.CommittedQuantity ,0) as 'CommittedQty'       
            //                                ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUM'
            //                                ,A.ReplenishmentQty as 'TransactionQty'
            //                                ,A.ReplenishmentQty as 'TransactionQtyDisplay'
            //                                ,A.ReplenishmentQtyUM as 'TransactionUOM'
            //                                ,pd.PickQtyPrimaryUOM
            //                                ,(A.PickedQty -A.ReplenishmentQty) as 'ExceptionCount'
            //                                ,'" + PayloadManager.RouteReplenishmentPayload.SelectedReplnId.ToString() + @"' as ReplnId   
            //                                ,'0' AS 'TransactionQtyPrimaryUOM' ,inventory.ParLevel
            //                                ,A.ReplenishmentQtyUM  AS 'UM'
            //                                ,  inventory.ItemId AS 'ShortDesc',pd.PickedQty,pd.PickDetailID
            //                                , isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4'
            //                                from busdta.Route_Replenishment_Detail A 
            //                                left join busdta.PICK_DETAIL pd on pd.TransactionDetailID=A.ReplenishmentDetailID and A.ItemId=pd.ItemID  and pd.TransactionID=A.ReplenishmentID  
            //                                left join busdta.Inventory inventory on A.ItemId=inventory.ItemId
            //                                left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
            //                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID 
            //                                left outer join busdta.Status_Type ST on A.PickedStatusTypeID = ST.StatusTypeID
            //                                where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and A.ReplenishmentID='" + replnID + @"'  
            //                                AND A.ReplenishmentQty!=0 
            //                                order by  A.ReplenishmentDetailID asc ";

            string query = "CALL BUSDTA.SP_GetHeldReturnPickCollection(@replnID =" + replnID + ",@SelectedReplnId = '" + PayloadManager.RouteReplenishmentPayload.SelectedReplnId.ToString() + "')";
            List<AddLoadPick> activities = new List<AddLoadPick>();
            try
            {
                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    activities = result.GetEntityList<AddLoadPick>();
                }

                foreach (AddLoadPick item in activities)
                {
                    //Get Item Collection
                    if (!item.HasUpdateOnHand)
                    {
                        List<AddLoadPick> listOFSameItem = new List<AddLoadPick>();
                        listOFSameItem = activities.Where(x => x.ItemId == item.ItemId).ToList<AddLoadPick>();

                        int onhand = item.ActualQtyOnHand;
                        int heldQty = item.HeldQty;
                        int pickedQtyForItem = 0;

                        if (listOFSameItem.Count > 1)
                        {
                            //Calculate OnhandQty 
                            pickedQtyForItem = listOFSameItem.Sum(x => x.PickedQty);
                            onhand = onhand - pickedQtyForItem;
                            heldQty = heldQty - pickedQtyForItem;
                        }
                        else if (listOFSameItem.Count == 1)
                        {
                            onhand = onhand - item.PickedQty;
                            heldQty = heldQty - item.PickedQty;

                        }

                        //Update  ActualQtyOnHand for item
                        foreach (AddLoadPick item1 in listOFSameItem)
                        {
                            item1.HasUpdateOnHand = true;
                        }
                    }

                }
                searchData = new ObservableCollection<AddLoadPick>(activities);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][GetHeldReturnPickCollection][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:GetHeldReturnPickCollection]");


            return searchData;
        }

        public static void UpdateAddLoadHeader(string status, string replnID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][Start:UpdateAddLoadHeader]");
            string StatusId = "0";
            //string query = "select StatusTypeID from busdta.Status_Type where StatusTypeCD='" + status + "'";
            string query = "CALL BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD = '" + status + "')";
            StatusId = DbEngine.ExecuteScalar(query);

            string queryToUpdate = "update  BUSDTA.Route_Replenishment_Header set StatusId= '" + StatusId + "',UpdatedBy =" + Managers.UserManager.UserId + ",UpdatedDatetime = getdate() where ReplenishmentID= '" + replnID + "'";
            try
            {
                int i = DbEngine.ExecuteNonQuery(queryToUpdate);
                if (i > 0)
                {
                    //Update Commited quantity for picking from truck flow
                }
                ResourceManager.QueueManager.QueueProcess("Replenishments", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][UpdateAddLoadHeader][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReplenishAddLoadManager][End:UpdateAddLoadHeader]");

        }

        public static int DeleteSavedItem(ObservableCollection<AddLoadDetails> AddLoadCollection, string replenID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:UpdateSequenceNo]");
            int id = 1;
            try
            {
                foreach (AddLoadDetails item in AddLoadCollection)
                {
                    string deleteQuery = "delete from BUSDTA.Route_Replenishment_Detail where ReplenishmentID= '" + replenID + "' and ItemId='" + item.ItemId + "'";
                    //int i = DbEngine.ExecuteNonQuery(deleteQuery);
                    DbEngine.ExecuteNonQuery(deleteQuery);
                    id++;
                }

                ResourceManager.QueueManager.QueueProcess("Replenishments", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][UpdateSequenceNo][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:UpdateSequenceNo]");
            return id;
        }

        public static void UpdateCommitedInventory(string CommittedQty, string HeldQty, string ItemId)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:UpdateCommitedInventory]");
                if (Convert.ToInt32(CommittedQty) < 0)
                {
                    CommittedQty = "0";
                }

                // Comment Start On - 09/15/2016
                //Update on hand quantity
                //string query = @"update busdta.Inventory set CommittedQuantity= '{0}' , HeldQuantity='{1}',UpdatedBy =" + Managers.UserManager.UserId + ",UpdatedDatetime = getdate() where ItemId='{2}'";
                //query = string.Format(query, CommittedQty, HeldQty, ItemId);
                //int count = DbEngine.ExecuteNonQuery(query);

                // Call Common Function
                InventoryManager objInvMgr = new InventoryManager();
                objInvMgr.UpdateReplnCommitedHeldInventory(CommittedQty, HeldQty, ItemId);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][UpdateCommitedInventory][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:UpdateCommitedInventory]");
        }
        //Sathish Removed Unwanted static variables and static class
        public void UpdateAvailableInventory(string CommittedQty, string ItemId)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:UpdateCommitedInventory]");
                if (Convert.ToInt32(CommittedQty) < 0)
                {
                    CommittedQty = "0";
                }

                // Comment Start On - 09/15/2016
                //Update on hand quantity
                //string query = @"update busdta.Inventory set CommittedQuantity= '{0}'  where ItemId='{1}'";
                //query = string.Format(query, CommittedQty, ItemId);
                //int count = DbEngine.ExecuteNonQuery(query);

                // Call Common Function
                InventoryManager objInvMgr = new InventoryManager();
                objInvMgr.UpdateReplnCommitedInventory(CommittedQty, ItemId);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][UpdateCommitedInventory][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:UpdateCommitedInventory]");
        }

        public static void UpdateCommittedQtyForReplenishment(string ReplnId)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:UpdateCommittedQtyForReplenishment]");
                //Get item IDs of replenishment
                //string queryForItemID = "SELECT PD.ItemID,SUM( PD.PickQtyPrimaryUOM) as 'Qty', INV.CommittedQuantity,INV.onhandquantity FROM BUSDTA.Pick_Detail PD  JOIN BUSDTA.INVENTORY INV    " +
                //                        " ON PD.ITEMID=INV.ITEMID WHERE PD.TransactionID=" + ReplnId + "  GROUP BY PD.ItemID, INV.CommittedQuantity,INV.onhandquantity ";
                string queryForItemID = "Call BUSDTA.SP_GetCommittedQtyForReplen(@ReplnId=" + ReplnId + " )";
                DataSet ds = new DataSet();
                ds = DbEngine.ExecuteDataSet(queryForItemID);

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    int qty = Convert.ToInt32(ds.Tables[0].Rows[i]["Qty"].ToString());
                    string itemID = ds.Tables[0].Rows[i]["ItemID"].ToString();
                    int onhandQtyInMaster = Convert.ToInt32(ds.Tables[0].Rows[i]["onhandquantity"].ToString());
                    int commitedInMaster = Convert.ToInt32(ds.Tables[0].Rows[i]["CommittedQuantity"].ToString());

                    if (commitedInMaster < qty)
                    {
                        qty = commitedInMaster;
                    }

                    // Comment Start On - 09/15/2016
                    // Update on Committed quantity
                    // string query = @"update busdta.Inventory set CommittedQuantity= CommittedQuantity-'{0}',UpdatedBy =" + Managers.UserManager.UserId + ",UpdatedDatetime = GETDATE()  where ItemId='{1}'";
                    // query = string.Format(query, qty, itemID);
                    // int cnt = DbEngine.ExecuteNonQuery(query);

                    // Call Common Function
                    InventoryManager objInvMgr = new InventoryManager();
                    int cnt = objInvMgr.UpdateReplnCommittedQtyReduce(qty, itemID);

                    if (cnt > 0)
                    {
                        if (onhandQtyInMaster < qty)
                        {
                            qty = onhandQtyInMaster;
                        }

                        // Comment Start On - 09/15/2016
                        //query = @"update busdta.Inventory set onhandquantity= onhandquantity-'{0}',UpdatedBy =" + Managers.UserManager.UserId + ",UpdatedDatetime = GETDATE()  where ItemId='{1}'";
                        //query = string.Format(query, qty, itemID);
                        //cnt = DbEngine.ExecuteNonQuery(query);

                        // Call Common Function
                        cnt = objInvMgr.UpdateReplnOnHandQtyReduce(qty, itemID);
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][UpdateCommittedQtyForReplenishment][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:UpdateCommittedQtyForReplenishment]");
        }

        public static string GetReplnIdForOpenHeldReturn
        {
            get
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:GetReplnIdForOpenHeldReturn]");
                string id = string.Empty;
                try
                {
                    //                string updateQuery = @"SELECT ReplenishmentID FROM BUSDTA.Route_Replenishment_Header  WHERE 
                    //                                        ReplenishmentTypeId =(SELECT TTID FROM BUSDTA.M5001 WHERE TTKEY='HELD RETURN') AND 
                    //                                        STATUSID IN (SELECT StatusTypeID FROM BUSDTA.Status_Type  WHERE StatusTypeCD  IN ('PIC','RTP','HOP'))";

                    string updateQuery = "CALL BUSDTA.SP_GetReplnIdForOpenHeldReturn()";
                    id = DbEngine.ExecuteScalar(updateQuery);
                    id = string.IsNullOrEmpty(id) ? "New" : id;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][GetReplnIdForOpenHeldReturn][ExceptionStackTrace = " + ex.StackTrace + "]");
                    //throw;
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:GetReplnIdForOpenHeldReturn]");
                return id;
            }
        }

        public static void UpdateOnhandInventory(string OnhandQty, string ItemId)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:UpdateOnhandInventory]");
                if (Convert.ToInt32(OnhandQty) < 0)
                {
                    OnhandQty = "0";
                }

                // Comment Start On - 09/15/2016
                //Update on hand quantity
                //string query = @"update busdta.Inventory set OnHandQuantity= '{0}'  where ItemId='{1}'";
                //query = string.Format(query, OnhandQty, ItemId);
                //int count = DbEngine.ExecuteNonQuery(query);

                // Call Common Function
                InventoryManager objInvMgr = new InventoryManager();
                objInvMgr.UpdateReplnOnHandInventory(OnhandQty, ItemId);
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:UpdateOnhandInventory]");

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][UpdateOnhandInventory][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            
        }

        //public static void UpdateCommittedQtyForReplenishment()
        //{
        //    try
        //    {
        //        Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:UpdateCommittedQtyForReplenishment]");

        //        //Get items selected in settlement
        //        string query = @"select * from busdta.Route_Replenishment_Detail where ReplenishmentID =" + PayloadManager.RouteReplenishmentPayload.SelectedReplnId;
        //        DataSet ds = DbEngine.ExecuteDataSet(query);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][UpdateCommittedQtyForReplenishment][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:UpdateCommittedQtyForReplenishment]");
        //}

        //public static void GetItemsWithReplenishQtyForReplenishment()
        //{
        //    try
        //    {
        //        Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:GetItemsWithReplenishQtyForReplenishment]");

        //        //Get items selected in settlement
        //        //string query = @"select * from busdta.Route_Replenishment_Detail where ReplenishmentID =" + PayloadManager.RouteReplenishmentPayload.SelectedReplnId;
        //        string query = "CALL BUSDTA.SP_GetUpdateCommittedQtyForReplen(@ReplenishmentID = " + PayloadManager.RouteReplenishmentPayload.SelectedReplnId + ")";
        //        DataSet ds = DbEngine.ExecuteDataSet(query);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][GetItemsWithReplenishQtyForReplenishment][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:GetItemsWithReplenishQtyForReplenishment]");
        //}

        public static ObservableCollection<QtyUOMClass> GetUOMCode(string ItemId)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:GetUOMCode]");
                string uomQuery = "Call BUSDTA.SP_GetUMCode(@itemid='" + ItemId + "')";
                List<QtyUOMClass> qtyUOM = new List<QtyUOMClass>();
                DataSet resultUOM = DbEngine.ExecuteDataSet(uomQuery);
                if (resultUOM.HasData())
                {
                    qtyUOM = resultUOM.GetEntityList<QtyUOMClass>();

                }
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:GetUOMCode]");
                return new ObservableCollection<QtyUOMClass>(qtyUOM);
            }
            catch (OutOfMemoryException ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][GetUOMCode][ExceptionStackTrace = " + ex.StackTrace + "]");
                return new ObservableCollection<QtyUOMClass>();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][GetUOMCode][ExceptionStackTrace = " + ex.StackTrace + "]");
                return new ObservableCollection<QtyUOMClass>();
                //throw;
            }


        }

        public static DataSet GetReplenQty(string ItemId, string replnID)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:GetReplenQty]");
                string queryOpenRepln = @"CALL BUSDTA.SP_GetReplenQty(@ToBranch = '" + UserManager.UserBranch.Trim() + @"',@ItemId =" + ItemId.Trim() + ",@ReplenishmentID = '" + replnID + "')";
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:GetReplenQty]");
                return DbEngine.ExecuteDataSet(queryOpenRepln);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][GetReplenQty][ExceptionStackTrace = " + ex.StackTrace + "]");
                return new DataSet();
                //throw;
            }
        }
    }
}
