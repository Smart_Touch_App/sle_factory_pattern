﻿using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Application.Helpers;
using log4net;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Application.ViewModelPayload;


namespace SalesLogicExpress.Application.Managers
{
    public static class RouteReplenishmentTabManagercs
    {
        public static readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.RouteReplenishmentTabManagercs");

        public static ObservableCollection<ReplenishmentDetails> GetReplenishments
        {
            get
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:GetReplenishments]");
                DeleteReplnWithZeroDetails();
                ObservableCollection<ReplenishmentDetails> searchData = new ObservableCollection<ReplenishmentDetails>();
                // TransactionID as ActivityID, RouteID,TransactionType as ActivityType,TransactionDetailClass as ActivityDetailClass,TransactionDetails as ActivityDetails,TransactionStart as ActivityStart,TransactionEnd as ActivityEnd,StopInstanceID,CustomerID,SettlementID,ParentTransactionID as ActivityHeaderID, TransactionStatus as ActivityStatus
                //            string query = @"SELECT A.ReplenishmentID as 'ReplnID',d.NTUser as 'RequestedBy' , B.StatusTypeCD as 'StatusCode',LTRIM(RTRIM( B.StatusTypeDESC)) as 'Status' 
                //                            ,A.FromBranchId AS 'FromBranch', a.ToBranchId AS 'ToBranch', a.TransDateFrom AS 'TransDateFrom' , a.TransDateTo AS 'TransDateTo', a.DocumentId
                //                            , LTRIM(RTRIM(C.TTADSC))  AS 'Type', C.TTID as 'TypeCode'
                //                            FROM BUSDTA.Route_Replenishment_Header  A 
                //                            left join BUSDTA.Status_Type b on A.StatusId=B.StatusTypeID
                //                            left join BUSDTA.M5001 C on A.ReplenishmentTypeId=C.TTID
                //                            LEFT join busdta.userprofilemaster d ON A.RequestedBy =d.App_user_id ORDER BY a.ReplenishmentID  DESC ";

                //            string query = @"SELECT A.ReplenishmentID as 'ReplnID',d.NTUser as 'RequestedBy' , B.StatusTypeCD as 'StatusCode',LTRIM(RTRIM( B.StatusTypeDESC)) as 'Status' 
                //                            ,Trim(A.FromBranch) AS 'FromBranch', Trim(a.ToBranch) AS 'ToBranch', a.TransDateFrom AS 'TransDateFrom' , a.TransDateTo AS 'TransDateTo', a.DocumentId
                //                            , LTRIM(RTRIM(C.TTADSC))  AS 'Type', C.TTID as 'TypeCode'
                //                            FROM BUSDTA.Route_Replenishment_Header  A 
                //                            left join BUSDTA.Status_Type b on A.StatusId=B.StatusTypeID
                //                            left join BUSDTA.M5001 C on A.ReplenishmentTypeId=C.TTID
                //                            LEFT join busdta.userprofilemaster d ON A.RequestedBy =d.App_user_id ORDER BY a.ReplenishmentID  DESC ";



                //String Handling for Memory Optimization  --Vignesh D
                string query = string.Empty;
                 query = @"Call BUSDTA.SP_GetReplenishments()";

                List<ReplenishmentDetails> activities = new List<ReplenishmentDetails>();
                try
                {
                    DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                    query = string.Empty;
                    if (result.HasData())
                    {
                        activities = result.GetEntityList<ReplenishmentDetails>();
                    }


                    //Dataset Disopse for memory optimization --Vignesh D
                    result.Dispose();
                    result = null;

                    searchData = new ObservableCollection<ReplenishmentDetails>(activities);

                    activities = null;  //Object Disposing --Vignesh D

                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][GetReplenishments][ExceptionStackTrace = " + ex.StackTrace + "]");

                }
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:GetReplenishments]");


                return searchData;
            }
        }

        public static bool IsAllowAddLoad(string loadOrUnload)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:IsAllowAddLoad]");

            bool flag = true;
            string query = string.Empty;
            try
            {
//                if (PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
//                {
//                    query = @"SELECT COUNT(*) FROM BUSDTA.Route_Replenishment_Header  WHERE StatusId IN (select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD in ('RTP','HOP'))
//                                AND ReplenishmentTypeId  IN (SELECT TTID FROM BUSDTA.M5001 WHERE TTKEY IN ('" + loadOrUnload + "'))  ";
//                }
//                else
//                {
//                    query = @"SELECT COUNT(*) FROM BUSDTA.Route_Replenishment_Header  WHERE StatusId IN (select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD in ('INPRS','RTP','HOP','RELS'))
//                                AND ReplenishmentTypeId  IN (SELECT TTID FROM BUSDTA.M5001 WHERE TTKEY IN ('" + loadOrUnload + "'))  AND   CONVERT(VARCHAR(10), CreatedDatetime,103)=CONVERT(VARCHAR(10), GETDATE(),103)";
//                }
                query = "CALL BUSDTA.SP_ValidateAllowAddLoad(@IsloadOrUnload='" + loadOrUnload + "',@IsSalesBranch=" + (PayloadManager.RouteReplenishmentPayload.IsSalesBranch ? "1" : "0").ToString() + ")";
                int count = Convert.ToInt32(DbEngine.ExecuteScalar(query));
                query = string.Empty;
                if (count > 0)
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][IsAllowAddLoad][ExceptionStackTrace = " + ex.StackTrace + "]");
                
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:IsAllowAddLoad]");
            
            return flag;
        }

        public static bool IsAllowSuggestion()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:IsAllowSuggestion]");

            bool flag = true;

            string query = string.Empty;   //String Handling for memory optimization  --Vignesh D
            try
          {
//                if (PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
//                {
//                    query = @"SELECT COUNT(*) FROM BUSDTA.Route_Replenishment_Header  WHERE StatusId IN (select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD in ('INPRS','RTP','HOP','RELS'))
//                                AND ReplenishmentTypeId  IN (SELECT TTID FROM BUSDTA.M5001 WHERE TTKEY IN ('suggestion'))   ";
//                }
//                else
//                {
//                    query = @"SELECT COUNT(*) FROM BUSDTA.Route_Replenishment_Header  WHERE StatusId IN (select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD in ('INPRS','RTP','HOP','RELS'))
//                                AND ReplenishmentTypeId  IN (SELECT TTID FROM BUSDTA.M5001 WHERE TTKEY IN ('suggestion'))   AND CONVERT(VARCHAR(10), CreatedDatetime,103)=CONVERT(VARCHAR(10), GETDATE(),103)";
//                }
                query = "CALL BUSDTA.SP_ValidateAllowSuggestion(@IsSalesBranch=" + (PayloadManager.RouteReplenishmentPayload.IsSalesBranch ? "1" : "0").ToString() + ")";
                int count = Convert.ToInt32(DbEngine.ExecuteScalar(query));
                query = string.Empty;
                if (count > 0)
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][IsAllowSuggestion][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:IsAllowSuggestion]");

            return flag;
        }

        public static bool IsAllowSuggestionReturn()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:IsAllowSuggestionReturn]");

            bool flag = true;

            string query = string.Empty;  //string handling for memory optimization   --Vignesh D
            try
            {
//                if (PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
//                {
//                    query = @"SELECT COUNT(*) FROM BUSDTA.Route_Replenishment_Header  WHERE StatusId IN (select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD in ('INPRS','RTP','HOP','RELS'))
//                                AND ReplenishmentTypeId  IN (SELECT TTID FROM BUSDTA.M5001 WHERE TTKEY IN ('sugg. return'))   ";
//                }
//                else
//                {
//                    query = @"SELECT COUNT(*) FROM BUSDTA.Route_Replenishment_Header  WHERE StatusId IN (select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD in ('INPRS','RTP','HOP','RELS'))
//                                AND ReplenishmentTypeId  IN (SELECT TTID FROM BUSDTA.M5001 WHERE TTKEY IN ('sugg. return'))   AND CONVERT(VARCHAR(10), CreatedDatetime,103)=CONVERT(VARCHAR(10), GETDATE(),103)";
//                }
                query = "CALL BUSDTA.SP_ValidateAllowSuggestionReturn(@IsSalesBranch=" + (PayloadManager.RouteReplenishmentPayload.IsSalesBranch ? "1" : "0").ToString() + ")";
                int count = Convert.ToInt32(DbEngine.ExecuteScalar(query));
                query = string.Empty;
                if (count > 0)
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][IsAllowSuggestionReturn][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:IsAllowSuggestionReturn]");

            return flag;
        }

        public static bool CanAllowOtherReplenishment()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:CanAllowOtherReplenishment]");
            //Check for held return replenishment.
            //If there is any open held return repln then , other repln can not be possible
            bool flag = true;

            string query = string.Empty;   //string handling for memory optimization  --Vignesh D
            try
           {
//                if (PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
//                {
//                    query = @"SELECT COUNT(*) FROM BUSDTA.Route_Replenishment_Header  WHERE StatusId IN (select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD in ('INPRS','RTP','HOP','PIC'))
//                                AND ReplenishmentTypeId  IN (SELECT TTID FROM BUSDTA.M5001 WHERE TTKEY IN ('held return'))   ";
//                }
//                else
//                {
//                    query = @"SELECT COUNT(*) FROM BUSDTA.Route_Replenishment_Header  WHERE StatusId IN (select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD in ('INPRS','RTP','HOP','PIC'))
//                                AND ReplenishmentTypeId  IN (SELECT TTID FROM BUSDTA.M5001 WHERE TTKEY IN ('held return'))   )";
//                }
               query = @"Call  BUSDTA.SP_GetHeldReturnscount()";
                int count = Convert.ToInt32(DbEngine.ExecuteScalar(query));
                query = string.Empty;
                if (count > 0)
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][CanAllowOtherReplenishment][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:CanAllowOtherReplenishment]");

            return flag;
        }

        public static bool IsOpenHeldReturn()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:IsOpenHeldReturn]");
            //Check for held return replenishment.
            //If there is any open held return repln then , other repln can not be possible
            bool flag = false;

            string query = string.Empty;  //String handling for memory optimization  --Vignesh D
            try
            {
//                if (PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
//                {
//                    query = @"SELECT COUNT(*) FROM BUSDTA.Route_Replenishment_Header  WHERE StatusId IN (select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD in ('INPRS','RTP','HOP','PIC'))
//                                AND ReplenishmentTypeId  IN (SELECT TTID FROM BUSDTA.M5001 WHERE TTKEY IN ('held return'))   ";
//                }
//                else
//                {
//                    query = @"SELECT COUNT(*) FROM BUSDTA.Route_Replenishment_Header  WHERE StatusId IN (select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD in ('INPRS','RTP','HOP','PIC'))
//                                AND ReplenishmentTypeId  IN (SELECT TTID FROM BUSDTA.M5001 WHERE TTKEY IN ('held return'))   ";
//                }
                query = @"Call  BUSDTA.SP_GetHeldReturnscount()";
                int count = Convert.ToInt32(DbEngine.ExecuteScalar(query));
                query = string.Empty;
                if (count > 0)
                {
                    flag = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][IsOpenHeldReturn][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:IsOpenHeldReturn]");

            return flag;
        }
        public static int GetTypeCode(string key)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:GetTypeCode]");
            int id = 0;
            // TransactionID as ActivityID, RouteID,TransactionType as ActivityType,TransactionDetailClass as ActivityDetailClass,TransactionDetails as ActivityDetails,TransactionStart as ActivityStart,TransactionEnd as ActivityEnd,StopInstanceID,CustomerID,SettlementID,ParentTransactionID as ActivityHeaderID, TransactionStatus as ActivityStatus
            //string query = @"select TTID  from BUSDTA.M5001 where TTKEY='" + key + "' ";

            //string Handling for memory optimization  --Vignesh D
            string query = string.Empty;
             query = @"Call BUSDTA.SP_GetTypeCode(@TTkey='" + key + "')";
            try
            {
                id = Convert.ToInt32(DbEngine.ExecuteScalar(query));
                query = string.Empty;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][GetTypeCode][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:GetTypeCode]");


            return id;
        }

        public static int GetHeldReturnItemCount
        {
            get
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:GetHeldReturnItemCount]");
                int id = 0;
                //string query = @"SELECT count(*) AS 'HeldCount' FROM BUSDTA.Inventory WHERE  RouteId='" + CommonNavInfo.RouteID + "' and HeldQuantity >0";

                //String Handling for Memory Optimization --Vignesh D
                string query = string.Empty;
                 query = "CALL BUSDTA.SP_GetHeldReturnItemCount(@RouteId='" + CommonNavInfo.RouteID + "')";
                try
                {
                    id = Convert.ToInt32(DbEngine.ExecuteScalar(query));
                    query = string.Empty;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][GetHeldReturnItemCount][ExceptionStackTrace = " + ex.StackTrace + "]");

                }
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:GetHeldReturnItemCount]");


                return id;
            }
        }

        public static int GetStatusTypeCode(string code)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:GetTypeCode]");
            int id = 0;
            // TransactionID as ActivityID, RouteID,TransactionType as ActivityType,TransactionDetailClass as ActivityDetailClass,TransactionDetails as ActivityDetails,TransactionStart as ActivityStart,TransactionEnd as ActivityEnd,StopInstanceID,CustomerID,SettlementID,ParentTransactionID as ActivityHeaderID, TransactionStatus as ActivityStatus
            //string query = "select StatusTypeID from busdta.Status_Type where StatusTypeCD='" + code + "'";

            string query = string.Empty; //String handling for memory optimization  --Vignesh D
             query = "Call BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='" + code + "')";
            try
            {
                string statusId = DbEngine.ExecuteScalar(query);
                query = string.Empty;
                id = Convert.ToInt32(string.IsNullOrEmpty(statusId) ? "0" : statusId);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][GetTypeCode][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:GetTypeCode]");


            return id;
        }

        public static bool DeleteReplnWithZeroDetails()
        {
            bool flag = true;
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:DeleteReplnWithZeroDetails]");
//            string query = @"DELETE FROM BUSDTA.Route_Replenishment_Header C 
//                            WHERE  C.ReplenishmentID 
//                            IN 
//                            (
//                                SELECT DISTINCT (A.ReplenishmentID) FROM busdta.Route_Replenishment_Header A
//                                EXCEPT 
//                                SELECT DISTINCT (B.ReplenishmentID) FROM busdta.Route_Replenishment_Detail B
//                            ) ";

            int count = 1;// Convert.ToInt32(DbEngine.ExecuteNonQuery(query));
            if (count > 0)
            {
                flag = false;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:DeleteReplnWithZeroDetails]");
            return flag;
        }

        public static int GetPickedQty
        {
            get
            {
                int count = 0;
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][Start:GetPickedQty][parameter= ]");
                //ObservableCollection<object> result = new ObservableCollection<object>();
                try
                {

                    //                string query = @"select sum(isnull(PD.PickedQty,0))  as 'PickedQty' from busdta.Pick_Detail PD JOIN  BUSDTA.Route_Replenishment_Detail RRD
                    //                                    ON PD.TransactionID= RRD.ReplenishmentID where RRD.ReplenishmentID='" + PayloadManager.RouteReplenishmentPayload.SelectedReplnId + "' ";


                    //string handling for memory optimization  --Vignesh D
                    string query = string.Empty;   
                     query = @"Call BUSDTA.SP_GetPickedQty(@SelectedReplnId='" + PayloadManager.RouteReplenishmentPayload.SelectedReplnId + "')";                   

                    count = Convert.ToInt32(DbEngine.ExecuteScalar(query));
                    query = string.Empty;


                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][GetPickedQty][parameter= ][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][RouteReplenishmentTabManagercs][End:GetPickedQty][parameter= ]");
                return count;
            }
        }
    }
}
