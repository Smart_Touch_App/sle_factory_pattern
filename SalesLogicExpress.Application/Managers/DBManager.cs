﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.SLEStatService;



namespace SalesLogicExpress.Application.Managers
{
    public class DBManager:IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.DBManager");

        #region Methods
        public bool TruncateDBTables(List<TruncateTable> tablesToTruncate)
        {
            try
            {
                Logger.Info(@"[SalesLogicExpress.Application.Managers][DBManager][Start:TruncateDBTables][tablesToTruncate=" + tablesToTruncate.SerializeToJson() + "]"+
                                                                                                        "[TotalTablesToTruncate=" + tablesToTruncate.Count() + "]");
                foreach (var tableName in tablesToTruncate)
                {
                    Logger.Info(@"[SalesLogicExpress.Application.Managers][DBManager][TruncateDBTables][SchemaName=" + tableName.SchemaName + "]"+
                                                                                                      "[TableName=" + tableName.TableName + "]");
                    Logger.Info(@"[SalesLogicExpress.Application.Managers][DBManager][TruncateDBTables][TruncateOerationStartTime=" + DateTime.Now + "]");

                    DbEngine.ExecuteNonQuery("truncate table ["+ tableName.SchemaName +"].["+tableName.TableName+"]");
                    Logger.Info(@"[SalesLogicExpress.Application.Managers][DBManager][TruncateDBTables][TruncateOerationEndTime=" + DateTime.Now + "]");

                }
                Logger.Info("[SalesLogicExpress.Application.Managers][DBManager][End:TruncateDBTables][tablesToTruncate=" + tablesToTruncate.SerializeToJson() + "]");
                Logger.Info("[SalesLogicExpress.Application.Managers][DBManager][TruncateDBTables][TablesTruncatedSuccessfully]");

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][ShutDownSyncClient][ExceptionStackTrace = " + ex.StackTrace + "]\n\n[ExceptionMessage = " + ex.Message + "]");
                return false;
            }
        }
        #endregion

        //public void Dispose()
        //{
        //    //throw new NotImplementedException();
        //}


        //Disposable Implementation --Vignesh D
        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
