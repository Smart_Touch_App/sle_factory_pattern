﻿﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using System.Data;
using System.Collections.ObjectModel;
using SalesLogicExpress.Application.ViewModels;

namespace SalesLogicExpress.Application.Managers
{
    public class InvoiceManager : IDisposable
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.InvoiceManager");
        OrderManager objOrderManager = new OrderManager();
        public int CreateInvoice(string InvoicePaymentType, string RouteId, string InvoiceNumber, string CustomerId, DateTime InvoiceDate, decimal InvoiceAmt, decimal OpenAmt, string OrderId)
        {
            int InvoiceId = 0;

            String CustBillTo = CustomerManager.GetCustomerBillTo(CustomerId);
            try
            {
                int intRouteId = string.IsNullOrEmpty(RouteId) ? 0 : Convert.ToInt32(RouteId);
                OrderManager objOrderMgr = new OrderManager();
                Int32 JDEInvoiceNumber = objOrderMgr.GetInvoiceNextNumber();
                if (JDEInvoiceNumber > 0)
                {
                    //string strOpenStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='OPEN'");
                    //string strUnknownStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='UNKNOWN'");
                    //string IsInvoiceExists = DbEngine.ExecuteScalar("select TRIM(ISNULL(DeviceInvoiceNumber,'')) DeviceInvoiceNumber from BUSDTA.Invoice_Header where DeviceInvoiceNumber=" + JDEInvoiceNumber);
                   string strOpenStatusId = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetCreateInvoiceStatusId('OPEN')");
                   string strUnknownStatusId = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetCreateInvoiceStatusId('UNKNOWN')");
                   string IsInvoiceExists = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetCreateInvoiceExists (@JDEInvoiceNumber='" + JDEInvoiceNumber + "')"); 
                    if (string.IsNullOrEmpty(IsInvoiceExists))
                    {
                        string InvPayType = InvoicePaymentType;
                        string ARDocumentType = "RI";
                        if (InvPayType.ToLower() != "return")
                        {
                            //string PaymentTerm = DbEngine.ExecuteScalar("SELECT LOWER(oh.PaymentTerms) FROM BUSDTA.Order_Header oh WHERE LOWER(oh.OrderId)='" + OrderId + "'");
                            string PaymentTerm = objOrderManager.InvoiceManager_CreateInvoice(OrderId);
                            if (PaymentTerm == "csh")
                            {
                                InvPayType = "Cash Inv";
                                ARDocumentType = "R3";
                            }
                            else
                            {
                                InvPayType = "Charge Inv";
                                ARDocumentType = "RI";
                            }
                        }
                        
                        //InvoiceId = Convert.ToInt32(DbEngine.ExecuteScalar(
                        //      @" Insert into BUSDTA.Invoice_Header (InvoiceID,InvoicePaymentType,DeviceStatusID,ARStatusID,OrderId,RouteId,DeviceInvoiceNumber,CustShipToId,CustBillToId,DeviceInvoiceDate,GrossAmt,CreatedBy,CreatedDatetime) "
                        //      + " values ('" + JDEInvoiceNumber + "','" + InvPayType + "'," + strOpenStatusId + "," + strUnknownStatusId + "," + OrderId + "," + RouteId + "," + InvoiceNumber + "," + CustomerId + "," + CustBillTo + ",'" + InvoiceDate.ToString("yyyy-MM-dd") + "'," + InvoiceAmt.ToString() + ",'" + UserManager.UserId + "',GETDATE())"
                        //      + " SELECT @@Identity"));

                        string MyQuery = string.Empty;
                        MyQuery = " Insert into BUSDTA.Invoice_Header (InvoiceID,InvoicePaymentType,DeviceStatusID,ARStatusID,OrderId,RouteId,DeviceInvoiceNumber,CustShipToId,CustBillToId,DeviceInvoiceDate,GrossAmt,CreatedBy,CreatedDatetime,ARInvoiceNumber,UpdatedBy,UpdatedDatetime) "
                              + " values ('" + JDEInvoiceNumber + "','" + InvPayType + "'," + strOpenStatusId + "," + strUnknownStatusId + "," + OrderId + "," + intRouteId + "," + InvoiceNumber + "," + CustomerId + "," + CustBillTo + ",'" + InvoiceDate.ToString("yyyy-MM-dd") + "'," + InvoiceAmt.ToString() + ",'" + UserManager.UserId + "',GETDATE()," + JDEInvoiceNumber + ",'" + UserManager.UserId + "',GETDATE())";

                        //int TranStatus = DbEngine.ExecuteNonQuery(MyQuery);   //Unused Local --Vignesh D
                        DbEngine.ExecuteNonQuery(MyQuery);                     //Added by Vignesh D
                        ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
                        InvoiceId = JDEInvoiceNumber;
                        //TranStatus = DbEngine.ExecuteNonQuery("update Busdta.Invoice_Header set DeviceInvoiceNumber=" + JDEInvoiceNumber + " , ARInvoiceNumber=" + JDEInvoiceNumber + ",UpdatedBy='" + UserManager.UserId + "',UpdatedDatetime=GETDATE() where InvoiceId=" + JDEInvoiceNumber + " AND DeviceInvoiceDate='" + InvoiceDate.ToString("yyyy-MM-dd") + "'");
                        //MapInvoiceOrder(RouteId, InvoiceId.ToString(), CustomerId, OrderId, StatusId);

                        // Retrun Status & Others                    
                        string SourceDocType = "SO";
                        if (InvoicePaymentType.ToLower() == "return")
                        {
                            ARDocumentType = "RM";
                            SourceDocType = "CO";
                        }

                        // Source Document Type



                        string ssql = "insert into BUSDTA.Customer_Ledger_R1 ([ARDocumentId],[ARDocumentType],[ARPayItem],[ARCompany],[TransactionTypeID],[SourceID],[CustBillToId] " +
                                       " ,[CustShipToId],[GrossAmt],[OpenAmt],[AdjustmentAmt],[VarianceAmt],[InvoiceDate],[DueDate],[SourceDocID],[SourceDocType],[Archive],CreatedBy,UpdatedBy,CreatedDatetime) " +
                                       " (select InvoiceID as [ARDocumentId],'" + ARDocumentType + "' as [ARDocumentType] ,'001' as [ARPayItem],'00800' as [ARCompany] " +
                                       " ,1 as [TransactionTypeID],RouteID as [SourceID],[CustBillToId],[CustShipToId],[GrossAmt],GrossAmt as [OpenAmt] " +
                                       " ,0 as [AdjustmentAmt],0 as [VarianceAmt],DeviceInvoiceDate as [InvoiceDate],NULL [DueDate],OrderID as [SourceDocID] " +
                                       " ,'" + SourceDocType + "' as [SourceDocType],0 as [Archive], '" + UserManager.UserId + "' as CreatedBy,'" + UserManager.UserId + "' as UpdatedBy, GETDATE() AS CreatedDatetime from BUSDTA.Invoice_Header where CustShipToId=" + CustomerId + " and InvoiceID=" + InvoiceId + ") ";

                     //   TranStatus = DbEngine.ExecuteNonQuery(ssql);    //Unused local  --Vignesh D
                        DbEngine.ExecuteNonQuery(ssql);                   //Added by Vignesh D
                        //InsertInvoiceInCustomerLedger(RouteId, InvoiceId, CustomerId, CustBillTo, InvoiceAmt);

                        // JDEInvoiceNumber & JDEInvoiceType Update
                        if (InvoicePaymentType.Trim().ToLower() == "credit" || InvoicePaymentType.Trim().ToLower() == "cash")
                        {
                            int JDETrans = 0;
                            string JDEInvoiceType = objOrderMgr.GetJDEOrderType();
                            JDETrans = objOrderMgr.UpdateJDEInforAfterDelivery(Order.OrderId.ToString(), JDEInvoiceNumber, JDEInvoiceType);
                        }
                        if (InvoicePaymentType.ToLower() == "return")
                        {
                            UpdateInvoiceNumber(OrderId);
                        }

                        ResourceManager.QueueManager.QueueProcess("ARPayments", false);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][InvoiceManager][CreateInvoice][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            return InvoiceId;
        }

        //        public void InsertInvoiceInCustomerLedger(string routeId, int InvoiceId, string customerId, string CustBillTo, decimal invoiceOpenAmt)
        //        {
        //            DbEngine.ExecuteNonQuery(@"insert into BUSDTA.Customer_Ledger (InvoiceId,CustShipToId,CustBillToId,RouteId,InvoiceGrossAmt,InvoiceOpenAmt,IsActive) 
        //                                     values(" + InvoiceId.ToString() + "," + customerId + "," + CustBillTo + "," + routeId + "," + invoiceOpenAmt.ToString() + "," + invoiceOpenAmt.ToString() + ",'1')");
        //        }

        public ObservableCollection<PaymentARModel.InvoiceDetails> GetInvoices(string CustomerId)
        {
            ObservableCollection<PaymentARModel.InvoiceDetails> InvoiceDetailsList = new ObservableCollection<PaymentARModel.InvoiceDetails>();
           // StringBuilder queryBuilder = new StringBuilder();   //Unused Local --Vignesh D

            string sBillTo = CustomerManager.GetCustomerBillTo(CustomerId);
            try
            {
                //string voidStatusId = DbEngine.ExecuteScalar("SELECT StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VOID'");

                //queryBuilder.Append("select IH.CustBillToId,ih.CustShipToID,ih.invoiceid,TRIM(ISNULL(ih.DeviceInvoiceNumber,'')) InvoiceNumber,ih.DeviceInvoiceDate InvoiceDate,ih.OrderID InvoiceOrderNumber,");
                //queryBuilder.Append("\n(case lower(InvoicePaymentType) when 'return' then (-1)*ih.GrossAmt else ih.GrossAmt end) GrossAmount,");
                //queryBuilder.Append("\n(case lower(InvoicePaymentType) when 'return' then (-1)*SUM(isnull(cl.InvoiceOpenAmt,0) + (SELECT (-1)*ISNULL(SUM(ISNULL(receiptappliedamt,0)),0) AS Amount FROM BUSDTA.Customer_Ledger ");
                //queryBuilder.Append("\nWHERE ReceiptID=ih.OrderId AND ISNULL(InvoiceId,0)<>0 AND RouteId=ih.RouteId)) else sum(isnull(cl.InvoiceOpenAmt,0)) - sum(isnull(cl.ConcessionAmt,0)) - sum(isnull(cl.receiptappliedamt,0)) end)  OpenAmount,");
                //queryBuilder.Append("\nDATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) AS Age ,InvoicePaymentType");
                //queryBuilder.Append("\nfrom BUSDTA.Customer_Ledger cl join busdta.Invoice_Header ih on cl.InvoiceId=ih.InvoiceID");
                //queryBuilder.Append("\nwhere IH.CustBillToId=" + sBillTo + " and cl.isactive='1' and ih.DeviceStatusId<>" + (string.IsNullOrEmpty(voidStatusId) ? "0" : voidStatusId));
                //queryBuilder.Append("\ngroup by IH.CustBillToId,ih.CustShipToID,ih.invoiceid,ih.OrderID,ih.DeviceInvoiceNumber,ih.DeviceInvoiceDate, ih.GrossAmt,InvoicePaymentType Order by ih.DeviceInvoiceNumber desc ");
                //DataSet result1 = DbEngine.ExecuteDataSet(queryBuilder.ToString());

                //for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                //{
                //    PaymentARModel.InvoiceDetails obj = new PaymentARModel.InvoiceDetails();
                //    obj.AgingInvDtl = Convert.ToInt32(result.Tables[0].Rows[i]["Age"].ToString());
                //    obj.InvoiceShipTo = result.Tables[0].Rows[i]["CustShipToID"].ToString();
                //    obj.GrossAmountInvDtl = Convert.ToDecimal(result.Tables[0].Rows[i]["GrossAmount"].ToString());
                //    obj.OpenAmountInvDtl = Convert.ToDecimal(result.Tables[0].Rows[i]["OpenAmount"].ToString());
                //    obj.DateInvDtl = Convert.ToDateTime(result.Tables[0].Rows[i]["InvoiceDate"]).ToString("MM/dd/yyyy");
                //    obj.InvoiceNoInvDtl = result.Tables[0].Rows[i]["InvoiceNumber"].ToString();
                //    obj.InvoiceIdInvDtl = Convert.ToInt32(result.Tables[0].Rows[i]["invoiceid"].ToString());
                //    obj.InvoicePaymentType = result.Tables[0].Rows[i]["InvoicePaymentType"].ToString();
                //    obj.InvoiceOrderNumber = result.Tables[0].Rows[i]["InvoiceOrderNumber"].ToString();
                //    InvoiceDetailsList.Add(obj);
                //}

                // Previous Balance Changes
                //string sql = "SELECT CASE WHEN ARDocumentType='RI' THEN 'Charge Inv' " +
                //             " WHEN ARDocumentType='RM' THEN 'Credit ' " +
                //             " WHEN ARDocumentType='R3' THEN 'Cash Inv'  ELSE ARDocumentType END as mType ,  " +
                //             " ARDocumentID as InvoiceNo,SourceDocID as OrderNo,CustShipToId as ShipTo,InvoiceDate,GrossAmt, " +
                //             " OpenAmt+ (select ISNULL(SUM(AppliedAmount),0) from busdta.Receipt_Ledger where AppliedDocumentID=ARDocumentID and SourceID<>0 and ARDocumentID<>0) as OpenAmt, DATEDIFF(day,BUSDTA.Customer_Ledger_R1.InvoiceDate,Getdate()) AS Age " +
                //             " FROM BUSDTA.Customer_Ledger_R1 where CustBillToId=" + sBillTo + " and TransactionTypeID<100 AND ISNULL(StatusID,0) NOT IN (SELECT ISNULL(TTID,0) TTID FROM BUSDTA.M5001 WHERE RTRIM(TTKEY) IN ('VoidCreditMemo','VoidAtDeliverCustomer')) and ARDocumentID<>0" +
                //             "  Order By InvoiceDate DESC ";

                if (sBillTo != string.Empty)
                {
                    string sql = ("CALL BUSDTA.SP_GetInvoices(" + sBillTo + ")");
                    DataSet result = DbEngine.ExecuteDataSet(sql);
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        PaymentARModel.InvoiceDetails obj = new PaymentARModel.InvoiceDetails();
                        obj.AgingInvDtl = Convert.ToInt32(result.Tables[0].Rows[i]["Age"].ToString());
                        obj.InvoiceShipTo = result.Tables[0].Rows[i]["ShipTo"].ToString();
                        obj.GrossAmountInvDtl = Convert.ToDecimal(result.Tables[0].Rows[i]["GrossAmt"].ToString());

                        obj.OpenAmountInvDtl = Convert.ToDecimal(string.IsNullOrEmpty(result.Tables[0].Rows[i]["OpenAmt"].ToString().Trim()) ? "0" : result.Tables[0].Rows[i]["OpenAmt"].ToString().Trim());

                        obj.DateInvDtl = Convert.ToDateTime(result.Tables[0].Rows[i]["InvoiceDate"]).ToString("MM/dd/yyyy");

                        obj.InvoiceNoInvDtl = result.Tables[0].Rows[i]["InvoiceNo"].ToString();
                        //Invoice ID
                        obj.InvoiceIdInvDtl = Convert.ToInt32(result.Tables[0].Rows[i]["InvoiceNo"].ToString());
                        obj.InvoicePaymentType = result.Tables[0].Rows[i]["mType"].ToString();

                        obj.InvoiceOrderNumber = result.Tables[0].Rows[i]["OrderNo"].ToString();

                        InvoiceDetailsList.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][InvoiceManager][GetOpenInvoices][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            return InvoiceDetailsList;
        }

        public ObservableCollection<PaymentARModel.InvoiceDetails> GetInvoicesByShipTo(string CustomerId)
        {
            ObservableCollection<PaymentARModel.InvoiceDetails> InvoiceDetailsList = new ObservableCollection<PaymentARModel.InvoiceDetails>();
          //  StringBuilder queryBuilder = new StringBuilder();   //Unused Local --Vignesh D

            string sBillTo = CustomerManager.GetCustomerBillTo(CustomerId);
            try
            {

                //string sql = "SELECT CASE WHEN ARDocumentType='RI' THEN 'Charge Inv' " +
                //             " WHEN ARDocumentType='RM' THEN 'Credit ' " +
                //             " WHEN ARDocumentType='R3' THEN 'Cash Inv'  ELSE ARDocumentType END as mType ,  " +
                //             " ARDocumentID as InvoiceNo,SourceDocID as OrderNo,CustShipToId as ShipTo,InvoiceDate,GrossAmt, " +
                //             " OpenAmt+(select ISNULL(SUM(AppliedAmount),0) from busdta.Receipt_Ledger where AppliedDocumentID=ARDocumentID and SourceID<>0) as OpenAmt, DATEDIFF(day,BUSDTA.Customer_Ledger_R1.InvoiceDate,Getdate()) AS Age " +
                //             " FROM BUSDTA.Customer_Ledger_R1 where CustBillToId=" + sBillTo + " and TransactionTypeID<100 " +
                //             " Order By InvoiceDate DESC ";
                string sql = string.Empty;
                 sql = ("CALL BUSDTA.SP_GetInvoicesByShipTo(" + sBillTo + ")");
                DataSet result = DbEngine.ExecuteDataSet(sql);
                sql = string.Empty;
                for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                {
                    PaymentARModel.InvoiceDetails obj = new PaymentARModel.InvoiceDetails();
                    obj.AgingInvDtl = Convert.ToInt32(result.Tables[0].Rows[i]["Age"].ToString());
                    obj.InvoiceShipTo = result.Tables[0].Rows[i]["ShipTo"].ToString();
                    obj.GrossAmountInvDtl = Convert.ToDecimal(result.Tables[0].Rows[i]["GrossAmt"].ToString());

                    obj.OpenAmountInvDtl = Convert.ToDecimal(result.Tables[0].Rows[i]["OpenAmt"].ToString());

                    obj.DateInvDtl = Convert.ToDateTime(result.Tables[0].Rows[i]["InvoiceDate"]).ToString("MM/dd/yyyy");

                    obj.InvoiceNoInvDtl = result.Tables[0].Rows[i]["InvoiceNo"].ToString();

                    obj.InvoiceIdInvDtl = Convert.ToInt32(result.Tables[0].Rows[i]["InvoiceNo"].ToString());
                    obj.InvoicePaymentType = result.Tables[0].Rows[i]["mType"].ToString();

                    obj.InvoiceOrderNumber = result.Tables[0].Rows[i]["OrderNo"].ToString();

                    InvoiceDetailsList.Add(obj);
                    obj = null;  //Disposing Objects Vignesh D
                }


                //Dataset Dispose for Memory optimization --Vignesh D
                result.Dispose();
                result = null;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][InvoiceManager][GetInvoicesByShipTo][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            return InvoiceDetailsList;
        }

        private string GetInvoiceNumber(string OrderID)
        {
            log.Info("[SalesLogicExpress.Application.Managers][InvoiceManager][Start:GetInvoiceNumber]");
            string InvoiceNo = "";
            try
            {
              //  InvoiceNo = DbEngine.ExecuteScalar("SELECT ISNULL(InvoiceID,0) InvoiceID FROM BUSDTA.Invoice_Header i WHERE i.OrderID='" + OrderID + "'");
                InvoiceNo = DbEngine.ExecuteScalar("CALL  BUSDTA.SP_GetInvoiceNumber('" + OrderID + "')");
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][InvoiceManager][GetInvoiceNumber][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            log.Info("[SalesLogicExpress.Application.Managers][InvoiceManager][End:GetInvoiceNumber]");
            return InvoiceNo;
        }

        public void UpdateInvoiceNumber(string OrderID)
        {
            log.Info("[SalesLogicExpress.Application.Managers][InvoiceManager][Start:UpdateInvoiceNumber]");
            try
            {
                string InvoiceNumber = GetInvoiceNumber(OrderID);
                //string Query = "UPDATE BUSDTA.Order_Header SET JDEInvoiceNumber ='" + InvoiceNumber + "' WHERE OrderID='" + OrderID + "'";
                //DbEngine.ExecuteNonQuery(Query);
                //Query = "UPDATE BUSDTA.Order_Detail SET JDEInvoiceNumber ='" + InvoiceNumber + "' WHERE OrderID='" + OrderID + "'";
                //DbEngine.ExecuteNonQuery(Query);
                objOrderManager.InvoiceManager_UpdateInvoiceNumber(InvoiceNumber, OrderID);
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][InvoiceManager][UpdateInvoiceNumber][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            log.Info("[SalesLogicExpress.Application.Managers][InvoiceManager][End:UpdateInvoiceNumber]");
        }

        //Disposable Implementation --Vignesh D
        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
                if (objOrderManager != null)
                    objOrderManager.Dispose();
            }
            disposed = true;
        }
    }
}