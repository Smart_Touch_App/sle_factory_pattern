﻿using System;
using System.ComponentModel;
using SalesLogicExpress.Application.Helpers;
using log4net;

namespace SalesLogicExpress.Application.Managers
{
    public class NumberManager : IDisposable
    {
        private readonly ILog logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.NumberManager");

        public string GetNextNumberForEntity(string RouteID, Entity ForEntity)
        {
            string returnValue = string.Empty;
            string OrderId = string.Empty;
            try
            {
                for (;;)
                {
                    string query = string.Format("SELECT BUSDTA.GetNextNumber({0},{1})", RouteID, ForEntity.GetEnumDescription());
                    returnValue = DbEngine.ExecuteScalar(query);
                    returnValue = string.IsNullOrEmpty(returnValue) ? null : returnValue;

                    if (returnValue != null)
                    {
                        if (ForEntity.ToString() == "SalesOrder" || ForEntity.ToString() == "ReturnOrder" || ForEntity.ToString() == "CreditMemo")
                        {
                            OrderId = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetForOrderID(@OrderId = '" + returnValue + "')");
                            if (string.IsNullOrEmpty(OrderId))
                                OrderId = DbEngine.ExecuteScalar("SELECT OrderId FROM BUSDTA.Order_Detail where OrderId = " + returnValue);
                        }
                        if (ForEntity.ToString() == "Quote")
                        {
                            OrderId = DbEngine.ExecuteScalar("select CustomerQuoteId from busdta.Customer_Quote_Header where CustomerQuoteId=" + returnValue);
                        }
                        if (ForEntity.ToString() == "JDEInvoiceNumber")
                        {
                            OrderId = DbEngine.ExecuteScalar("select InvoiceId from busdta.Invoice_Header where InvoiceId=" + returnValue);
                        }
                        if (ForEntity.ToString() == "Prospect")
                        {
                            OrderId = DbEngine.ExecuteScalar("select ProspectQuoteId from busdta.Prospect_Quote_Header where ProspectQuoteId=" + returnValue);
                        }
                        if (ForEntity.ToString() == "Receipt")
                        {
                            OrderId = DbEngine.ExecuteScalar("SELECT ReceiptID FROM BUSDTA.Receipt_Header WHERE ReceiptID=" + returnValue);
                            if (string.IsNullOrEmpty(OrderId))
                                OrderId = DbEngine.ExecuteScalar("SELECT ReceiptID FROM BUSDTA.Receipt_Ledger WHERE ReceiptID=" + returnValue);
                        }
                        if (ForEntity.ToString() == "Replenishment")
                        {
                            OrderId = DbEngine.ExecuteScalar("select ReplenishmentId from busdta.Route_Replenishment_Header where ReplenishmentId=" + returnValue);
                        }
                        if (ForEntity.ToString() == "Settlement")
                        {
                            OrderId = DbEngine.ExecuteScalar("select SettlementId from busdta.Route_Settlement where SettlementId=" + returnValue);
                        }
                        if (ForEntity.ToString() == "SYSTEMACTID")
                        {
                            OrderId = DbEngine.ExecuteScalar("SELECT ALID FROM BUSDTA.M5003 WHERE ALID=" + returnValue);
                        }
                        if (ForEntity.ToString() == "TRANSACTID")
                        {
                            OrderId = DbEngine.ExecuteScalar("SELECT TDID FROM BUSDTA.M50012 WHERE TDID=" + returnValue);
                        }
                        if (string.IsNullOrEmpty(OrderId))
                            break;
                        else
                            logger.Info("The Entity Number [ " + OrderId + " ] already create for : " + ForEntity.ToString());
                    }
                    else
                    {
                        return returnValue;
                    }
                }
                ResourceManager.QueueManager.QueueProcess("DistributedNextNumber", false);
            }
            catch (Exception ex)
            {
                returnValue = null;
                logger.Error("[SalesLogicExpress.Application.Managers][NumberManager][GetNextNumberForEntity][RouteID=" + RouteID + "][ForEntity=" + ForEntity.ToString() + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            return returnValue;
        }

        // The description value is derived from Entity_Bucket_Master table in the remote database
        public enum Entity
        {
            [Description("1")]
            Receipt,
            [Description("2")]
            SalesOrder,
            [Description("3")]
            Settlement,
            [Description("4")]
            ReturnOrder,
            [Description("5")]
            CreditMemo,
            [Description("6")]
            Quote,
            [Description("7")]
            Prospect,
            [Description("8")]
            TEMPLATEID,
            [Description("9")]
            JDEInvoiceNumber,
            [Description("12")]
            Replenishment = 12,
            [Description("13")]
            TRANSACTID,
            [Description("14")]
            SYSTEMACTID
        }

        //************************************************************************************************
        // Comment: Dispose Implementation for Peformance Improvement
        // Created: Nov 03, 2016
        // Author: TechUnison (Velmani Karnan)
        //*************************************************************************************************

        bool disposed = false;
        private readonly System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}