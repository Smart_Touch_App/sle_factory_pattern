﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using SalesLogicExpress.Application.Helpers;
using System.Threading.Tasks;
using SalesLogicExpress.Domain;
using log4net;

namespace SalesLogicExpress.Application.Managers
{
    public class SuggestionReplenishmentAlgo : IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.SuggestionReplenishmentAlgo");

        string Route; DateTime FromDate; DateTime ToDate; string RouteID;


        private List<StopListClass> stopList = new List<StopListClass>();
        public List<StopListClass> StopList
        {
            get { return stopList; }
            set { stopList = value; }
        }



        private List<ItemDemandListClass> itemDemandList = new List<ItemDemandListClass>();
        public List<ItemDemandListClass> ItemDemandList
        {
            get { return itemDemandList; }
            set { itemDemandList = value; }
        }



        private List<string> replenishmentItemList = new List<string>();
        public List<string> ReplenishmentItemList
        {
            get { return replenishmentItemList; }
            set { replenishmentItemList = value; }
        }




        private List<AddLoadDetails> inventoryClassList = new List<AddLoadDetails>();
        public List<AddLoadDetails> InventoryClassList
        {
            get { return inventoryClassList; }
            set { inventoryClassList = value; }
        }



        private List<RouteReplenishmentDetailsClass> routeReplenishmentDetailsClassList = new List<RouteReplenishmentDetailsClass>();
        public List<RouteReplenishmentDetailsClass> RouteReplenishmentDetailsClassList
        {
            get { return routeReplenishmentDetailsClassList; }
            set { routeReplenishmentDetailsClassList = value; }
        }



        private List<AddLoadDetails> replishmentSuggestionList = new List<AddLoadDetails>();
        public List<AddLoadDetails> ReplishmentSuggestionList
        {
            get { return replishmentSuggestionList; }
            set { replishmentSuggestionList = value; }
        }

        public SuggestionReplenishmentAlgo()
        {

        }
        //route='FBM598'
        void GenerateCustomerStopList()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SuggestionReplenishmentAlgo][Start:GenerateCustomerStopList]");
            try
            {

//                string query = @"select 
//                                A.RPAN8 AS 'CustomerNumber'
//                                ,A.RPSTDT AS 'StopDate'
//                                ,isnull(A.RPACTID,'') as 'NoActivity'
//                                from busdta.M56M0004 A 
//                                where A.RPSTTP='PLANNED'  
//                                AND A.RPSTDT BETWEEN '" + FromDate.ToString("yyyy-MM-dd") + "' AND '" + ToDate.ToString("yyyy-MM-dd") + @"' 
//                                AND A.RPROUT='" + Route + "'  ";
                //                string query = @"select 
                //                                A.RPAN8 AS 'CustomerNumber'
                //                                ,A.RPSTDT AS 'StopDate'
                //                                ,A.RPACTID as 'NoActivity'
                //                                ,CASE WHEN A.RPACTID='NOSALE' and B.POAN8 IS NOT NULL  THEN 1 ELSE    0 END AS 'HasPreOrder'
                //                                ,B.POAN8   
                //                                ,ISNULL(B.POUOM,'') AS 'PreOrderUOM'
                //                                ,ISNULL( B.POQTYU / 10000 ,0)AS 'PreOrderQty'
                //                                , B.POITM AS 'ItemID'
                //                                from busdta.M56M0004 A 
                //                                LEFT JOIN BUSDTA.M4016 B ON A.RPAN8 =B.POAN8                              
                //                                where A.RPSTTP='PLANNED'  
                //                                AND A.RPSTDT BETWEEN '" + FromDate.ToString("yyyy-MM-dd") + "' AND '" + ToDate.ToString("yyyy-MM-dd") + @"' 
                //                                AND A.RPROUT='" + Route + "'  ";

                string query = "Call BUSDTA.SP_GetCustomerStopList (@Route='" + Route + "',@FromDate='" + FromDate.ToString("yyyy-MM-dd") + "',@ToDate='" + ToDate.ToString("yyyy-MM-dd") + "')";
                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                query = string.Empty;
                if (result.HasData())
                {
                    StopList = result.GetEntityList<StopListClass>();

                    foreach (StopListClass item in StopList)
                    {

                        if (item.NoActivity != "NOSALE") // VIVENSAS  UPDATED TO RETURN PREORDER FOR ALL EXCEPT FOR NOSALE
                        {
                            //Check for preorder
                            //string queryCheckPreorder = "select count(*) from BUSDTA.M4016  WHERE  POAN8 ='" + item.CustomerNumber.Trim() + "' AND POSTDT='" + item.StopDate.ToString("yyyy-MM-dd") + "'";
                            string queryCheckPreorder = "Call BUSDTA.SP_GetCountPreorder (@CustomerNum='" + item.CustomerNumber.Trim() + "',@StopDate='" + item.StopDate.ToString("yyyy-MM-dd") + "')";
                            int count = Convert.ToInt32(DbEngine.ExecuteScalar(queryCheckPreorder));
                            if (count > 0)
                            {
                                item.HasPreOrder = true;
                            }
                        }
                    }
                }
                result.Dispose();
                result = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SuggestionReplenishmentAlgo][GenerateCustomerStopList][ExceptionStackTrace = " + ex.StackTrace + "]");


            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SuggestionReplenishmentAlgo][End:GenerateCustomerStopList]");
        }

        void GenerateItemDemandList()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SuggestionReplenishmentAlgo][Start:GenerateItemDemandList]");
            try
            {
                foreach (StopListClass item in StopList)
                {
                    if (item.HasPreOrder)
                    {
                        // Get preorder item list
//                        string queryGetPreorderItem = @"select  B.POAN8  as 'CustomerNumber' ,ISNULL(B.POUOM,'') AS 'PreOrderUOM'
//                                                        ,ISNULL( B.POQTYU / 10000 ,0)AS 'PreOrderQty', B.POITM AS 'ItemID' 
//                                                        from BUSDTA.M4016  B where  B.POAN8    ='" + item.CustomerNumber + "' AND B.POSTDT='" + item.StopDate.ToString("yyyy-MM-dd") + "'  ";
                        string queryGetPreorderItem = "Call BUSDTA.SP_GetPreorderItem (@CustNum='" + item.CustomerNumber + "',@stopdate='" + item.StopDate.ToString("yyyy-MM-dd") + "')";

                        DataSet result1 = Helpers.DbEngine.ExecuteDataSet(queryGetPreorderItem);
                        queryGetPreorderItem = string.Empty;
                        List<PreOrderItems> PreOrderItems = new List<PreOrderItems>();
                        if (result1.HasData())
                        {

                            PreOrderItems = result1.GetEntityList<PreOrderItems>();
                            result1.Dispose();
                            result1 = null;
                            foreach (PreOrderItems item1 in PreOrderItems)
                            {
                                //string queryPrimaryUM = " select PRIMARYUM from   busdta.ItemConfiguration  WHERE ITEMID='" + item1.ItemID + "'";
                                string queryPrimaryUM = "Call BUSDTA.SP_GetPrimaryUM (@ItemID='" + item1.ItemID + "')";
                                item1.PrimaryUM = DbEngine.ExecuteScalar(queryPrimaryUM);
                                queryPrimaryUM = string.Empty;
                                //string queryItemNo = "     SELECT LTRIM(RTRIM(C.IMLITM)) AS 'ItemNumber' FROM busdta.F4101 C WHERE IMITM='" + item1.ItemID + "'";
                                string queryItemNo = "Call BUSDTA.SP_GetItemNumber (@ItemID='" + item1.ItemID + "')";
                                item1.ItemNumber = DbEngine.ExecuteScalar(queryItemNo);
                                queryItemNo = string.Empty;
                                item1.ConvesionFactor = SetConversionFactorForItem(item1.ItemID, item1.PreOrderUOM, item1.ItemNumber, item1.PrimaryUM);
                                item1.PreOrderQtyInPrimaryUM = Convert.ToInt32(item1.PreOrderQty * item1.ConvesionFactor);

                                ItemDemandListClass o = new ItemDemandListClass();
                                o = ItemDemandList.FirstOrDefault(x => x.ItemID == item1.ItemID);
                                if (ItemDemandList.Contains(o))
                                {
                                    o.DemandQuantity += item1.PreOrderQtyInPrimaryUM;
                                }
                                else
                                {
                                    o = new ItemDemandListClass();
                                    o.ItemID = item1.ItemID;
                                    o.DemandQuantity = item1.PreOrderQtyInPrimaryUM;
                                    ItemDemandList.Add(o);
                                }
                                o = null;
                            }


                        }


                    }
                    else
                    {
                        //string queryCustHistDemand = "select ItemID,NetQtySoldPerStop  from busdta.Metric_CustHistDemand A  where A.CustomerID= '" + item.CustomerNumber + "'";
                        string queryCustHistDemand = "Call BUSDTA.SP_GetCustHistDemand(@CustomerID='" + item.CustomerNumber + "')";
                        List<MetricCustHistDemandClass> MetricCustHistDemandList = new List<MetricCustHistDemandClass>();

                        DataSet result = Helpers.DbEngine.ExecuteDataSet(queryCustHistDemand);
                        queryCustHistDemand = string.Empty;
                        if (result.HasData())
                        {
                            MetricCustHistDemandList = result.GetEntityList<MetricCustHistDemandClass>();
                        }
                        result.Dispose();
                        result = null;
                        foreach (MetricCustHistDemandClass item1 in MetricCustHistDemandList)
                        {

                            ItemDemandListClass o = new ItemDemandListClass();
                            o = ItemDemandList.FirstOrDefault(x => x.ItemID == item1.ItemID);
                            if (ItemDemandList.Contains(o))
                            {
                                o.DemandQuantity += item1.NetQtySoldPerStop;
                            }
                            else
                            {
                                o = new ItemDemandListClass();
                                o.ItemID = item1.ItemID;
                                o.DemandQuantity = item1.NetQtySoldPerStop;
                                ItemDemandList.Add(o);
                            }
                            o = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.Managers][SuggestionReplenishmentAlgo][GenerateItemDemandList][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SuggestionReplenishmentAlgo][End:GenerateItemDemandList]");
        }

        void GenerateReplenishmentItemList()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SuggestionReplenishmentAlgo][Start:GenerateReplenishmentItemList]");
            try
            {
                foreach (ItemDemandListClass item in ItemDemandList)
                {
                    ReplenishmentItemList.Add(item.ItemID.Trim());
                }

                //Query Inventory

//                string queryInventory = @"select A.ItemId from busdta.Inventory  A 
//                                        LEFT JOIN busdta.ItemConfiguration D ON D.ItemId=A.ItemId  left join BUSDTA.ItemUoMs F ON F.ItemID=A.ItemId  where  A.RouteId='" + RouteID + @"'  
//                                        and (A.OnHandQuantity -A.CommittedQuantity-A.HeldQuantity)< A.ParLevel and D.RouteEnabled = 1 and D.allowSearch=1   AND F.CanSell=1  ";
                string queryInventory = @"Call BUSDTA.SP_GetReplenishmentItemList (@RouteID='" + RouteID + "', @Isflag=1)";

                DataSet result = Helpers.DbEngine.ExecuteDataSet(queryInventory);
                queryInventory = string.Empty;
                if (result.HasData())
                {
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        string itemId = result.Tables[0].Rows[i][0].ToString();
                        if (!ReplenishmentItemList.Contains(itemId))
                        {
                            ReplenishmentItemList.Add(itemId);
                        }
                    }
                }
                result.Dispose();
                result = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SuggestionReplenishmentAlgo][GenerateReplenishmentItemList][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SuggestionReplenishmentAlgo][End:GenerateReplenishmentItemList]");
        }


        public List<AddLoadDetails> GenerateReplishmentSuggestionList()
        {

            Logger.Info("[SalesLogicExpress.Application.Managers][SuggestionReplenishmentAlgo][Start:GenerateReplishmentSuggestionList]");
            Route = ViewModels.CommonNavInfo.RouteUser; FromDate = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.FromDate; ToDate = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.ToDate; RouteID = ViewModels.CommonNavInfo.RouteID.ToString();
            GenerateCustomerStopList();
            GenerateItemDemandList();
            GenerateReplenishmentItemList();

            string separatedItems = ReplenishmentItemList.Count > 1 ? ReplenishmentItemList.Aggregate<string>((old, newstring) => old + ',' + newstring) : (ReplenishmentItemList.Count == 1) ? ReplenishmentItemList[0] : "''";

            try
            {

                //Query Inventory
//                string queryInventory = @"select   inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo', inventory.RouteId, inventory.OnHandQuantity AS 'OnHandQty',
//                                inventory.CommittedQuantity AS 'CommittedQty', 
//                                inventory.HeldQuantity AS 'HeldQty',inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription',
//                                (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty',
//                                isnull(im.IMLNTY ,'')as 'StkType', isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4',
//                                isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM'
//                                from busdta.Inventory inventory 
//                                left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
//                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID
//                                LEFT JOIN BUSDTA.F4102 ib ON im.imitm = ib.ibitm
//                                LEFT JOIN BUSDTA.F40205 lyf ON IBLNTY=LFLNTY   
//                                left outer join BUSDTA.ItemUoMs iu on inventory.ItemId = iu.ItemID  
//                                where iu.CanSell=1 AND lyf.LFIVI='Y' and inventory.ItemId in (" + separatedItems + ") and iconfig.RouteEnabled = 1  and iconfig.allowSearch=1 order by ItemNumber ASC";
                string queryInventory = "Call BUSDTA.SP_GetReplishmentSuggestionList (@ItemIDs='" + separatedItems + "')";
                DataSet resultInventory = Helpers.DbEngine.ExecuteDataSet(queryInventory);
                queryInventory = string.Empty;
                if (resultInventory.HasData())
                {
                    InventoryClassList = resultInventory.GetEntityList<AddLoadDetails>();
                }
                resultInventory.Dispose();
                resultInventory = null;

                //Query RouteReplenishmentDetails


                //                string queryDetail = @"select X.AdjustedQty ,A.ReplenishmentQty,A.ReplenishmentQtyUM ,F.PrimaryUM,ltrim(rtrim( A.ItemID ))  as 'ItemID',
                //                                    LTRIM(RTRIM( H.IMLITM)) AS 'ItemNumber' ,X.PickedQty
                //                                    from busdta.Route_Replenishment_Detail     A 
                //                                    JOIN BUSDTA.Pick_Detail X ON A.ITEMID =X.ITEMID AND A.ReplenishmentID=X.TransactionID
                //                                    JOIN BUSDTA.ItemConfiguration F ON A.ItemID=F.ItemID
                //                                    JOIN BUSDTA.F4101 H ON H.IMITM=F.ItemID
                //                                    WHERE F.RouteEnabled = 1 and F.allowSearch=1 AND A.ItemId in (" + separatedItems + @")  AND  A.ReplenishmentID IN  
                //                                    (   SELECT B.ReplenishmentID FROM BUSDTA.Route_Replenishment_Header B 
                //                                        WHERE B.StatusId  IN (select b.StatusTypeID from busdta.Status_Type B 
                //                                        WHERE  B.StatusTypeCD in ('RELS','RTP','HOP')) and B.ToBranchId ='" + UserManager.UserRoute + @"')";

//                string queryDetail = @"select X.AdjustedQty ,A.ReplenishmentQty,A.ReplenishmentQtyUM ,F.PrimaryUM,ltrim(rtrim( A.ItemID ))  as 'ItemID',
//                                    LTRIM(RTRIM( H.IMLITM)) AS 'ItemNumber' ,X.PickedQty
//                                    from busdta.Route_Replenishment_Detail     A 
//                                    JOIN BUSDTA.Pick_Detail X ON A.ITEMID =X.ITEMID AND A.ReplenishmentID=X.TransactionID
//                                    JOIN BUSDTA.ItemConfiguration F ON A.ItemID=F.ItemID
//                                    JOIN BUSDTA.F4101 H ON H.IMITM=F.ItemID
//                                    WHERE F.RouteEnabled = 1 and F.allowSearch=1 AND A.ItemId in (" + separatedItems + @")  AND  A.ReplenishmentID IN  
//                                    (   SELECT B.ReplenishmentID FROM BUSDTA.Route_Replenishment_Header B 
//                                        WHERE B.StatusId  IN (select b.StatusTypeID from busdta.Status_Type B 
//                                        WHERE  B.StatusTypeCD in ('RELS','RTP','HOP')) and Trim(B.ToBranch) ='" + UserManager.UserBranch.Trim() + @"') ORDER BY H.IMLITM";

                string queryDetail = "Call BUSDTA.SP_GetRouteReplenishmentDetails (@ItemIDs='" + separatedItems + "',@ToBranch='" + UserManager.UserBranch.Trim() + "')";

                DataSet resultRouteReplenishmentDetails = Helpers.DbEngine.ExecuteDataSet(queryDetail);
                queryDetail = string.Empty;
                if (resultRouteReplenishmentDetails.HasData())
                {
                    RouteReplenishmentDetailsClassList = resultRouteReplenishmentDetails.GetEntityList<RouteReplenishmentDetailsClass>();
                }
                resultRouteReplenishmentDetails.Dispose();
                resultRouteReplenishmentDetails = null;

                foreach (var itemIdInList in ReplenishmentItemList)
                {
                    AddLoadDetails objToAdd = new AddLoadDetails();

                    objToAdd = InventoryClassList.FirstOrDefault(x => x.ItemId == itemIdInList.ToString());
                    if (objToAdd == null)
                    {
                        continue;
                    }
                    objToAdd.AvailableQty = objToAdd.AvailableQty;
                    objToAdd.ParLevel = objToAdd.ParLevel;

                    #region OnOrderQty
                    RouteReplenishmentDetailsClass obj = RouteReplenishmentDetailsClassList.FirstOrDefault(x => x.ItemID == itemIdInList.ToString());
                    if (obj == null)
                    {
                        objToAdd.OnOrderQtyInPrimaryUM = 0;
                    }
                    else
                    {
                        //double ConvesionFactor = SetConversionFactorForItem(obj.ItemID, obj.ReplenishmentQtyUM, obj.ItemNumber.ToString(), obj.PrimaryUM);

                        List<RouteReplenishmentDetailsClass> objList = RouteReplenishmentDetailsClassList.Where(x => x.ItemID == itemIdInList.ToString()).ToList<RouteReplenishmentDetailsClass>();

                        foreach (RouteReplenishmentDetailsClass item11 in objList)
                        {
                            double cFactor = SetConversionFactorForItem(item11.ItemID, item11.ReplenishmentQtyUM, item11.ItemNumber.ToString(), item11.PrimaryUM);
                            item11.OnOrderQty = (item11.ReplenishmentQty > item11.AdjustedQty ? item11.ReplenishmentQty : item11.AdjustedQty) - item11.PickedQty;
                            item11.OnOrderQtyInPrimaryUM = Convert.ToInt32(item11.OnOrderQty * cFactor);
                            objToAdd.OnOrderQtyInPrimaryUM = (objToAdd.OnOrderQtyInPrimaryUM + item11.OnOrderQtyInPrimaryUM);
                        }
                    }

                    objToAdd.OpenReplnQty = objToAdd.OnOrderQtyInPrimaryUM.ToString();
                    #endregion


                    ItemDemandListClass objDemand = ItemDemandList.FirstOrDefault(x => x.ItemID == itemIdInList.ToString());
                    objToAdd.DemandQty = objDemand == null ? 0 : objDemand.DemandQuantity;


                    //Set Load Quantity
                    if (((objToAdd.AvailableQty + objToAdd.OnOrderQtyInPrimaryUM) < objToAdd.ParLevel) || ((objToAdd.AvailableQty + objToAdd.OnOrderQtyInPrimaryUM) < objToAdd.DemandQty))
                    {
                        if (objToAdd.ParLevel > objToAdd.DemandQty)
                        {
                            objToAdd.LoadQuantity = objToAdd.ParLevel - (objToAdd.AvailableQty + objToAdd.OnOrderQtyInPrimaryUM);
                        }
                        else if (objToAdd.ParLevel <= objToAdd.DemandQty)
                        {
                            objToAdd.LoadQuantity = objToAdd.DemandQty - (objToAdd.AvailableQty + objToAdd.OnOrderQtyInPrimaryUM);
                        }
                        objToAdd.SuggestedQty = objToAdd.LoadQuantity;
                        ReplishmentSuggestionList.Add(objToAdd);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SuggestionReplenishmentAlgo][GenerateReplishmentSuggestionList][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SuggestionReplenishmentAlgo][End:GenerateReplishmentSuggestionList]");
            return ReplishmentSuggestionList.OrderBy(x => x.ItemNo).ToList();
        }
        




        static double SetConversionFactorForItem(string ItemID, string secondUM, string ItemNumber, string PrimaryUM)
        {
            double ConvesionFactor = 1.0;
            
            try
            {
                if (UoMManager.ItemUoMFactorList != null)
                {

                    var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == (ItemID.Trim()) && (x.FromUOM == (secondUM.ToString().Trim()) && (x.ToUOM == (PrimaryUM.Trim())))));
                    if (itemUomConversion == null)
                        ConvesionFactor = UoMManager.GetUoMFactor(secondUM, PrimaryUM, Convert.ToInt32(ItemID), ItemNumber.Trim());
                    else
                        ConvesionFactor = itemUomConversion.ConversionFactor;
                }
                else
                {
                    ConvesionFactor = UoMManager.GetUoMFactor(secondUM, PrimaryUM, Convert.ToInt32(ItemID), ItemNumber.Trim());
                }
            }
            catch (Exception)
            {

            }

            return ConvesionFactor;
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }

    public class StopListClass
    {
        private string customerNumber = string.Empty;
        public string CustomerNumber
        {
            get { return customerNumber; }
            set { customerNumber = value; }
        }



        private DateTime stopDate = new DateTime();
        public DateTime StopDate
        {
            get { return stopDate; }
            set { stopDate = value; }
        }


        private bool hasPreOrder = false;
        public bool HasPreOrder
        {
            get { return hasPreOrder; }
            set { hasPreOrder = value; }
        }



        private string noActivity = string.Empty;
        public string NoActivity
        {
            get { return noActivity; }
            set { noActivity = value; }
        }




    }

    public class ItemDemandListClass
    {
        private string itemID = string.Empty;
        public string ItemID
        {
            get { return itemID; }
            set { itemID = value; }
        }




        private int demandQuantity = 0;
        public int DemandQuantity
        {
            get { return demandQuantity; }
            set { demandQuantity = value; }
        }
    }

    public class MetricCustHistDemandClass
    {
        private string itemID = string.Empty;
        public string ItemID
        {
            get { return itemID; }
            set { itemID = value; }
        }



        private int netQtySoldPerStop = 0;
        public int NetQtySoldPerStop
        {
            get { return netQtySoldPerStop; }
            set { netQtySoldPerStop = value; }
        }
    }

    public class InventoryClass
    {
        private string itemId = string.Empty;
        public string ItemId
        {
            get { return itemId; }
            set { itemId = value; }
        }


        private int availableQty = 0;
        public int AvailableQty
        {
            get { return availableQty; }
            set { availableQty = value; }
        }




        private int parLevel = 0;
        public int ParLevel
        {
            get { return parLevel; }
            set { parLevel = value; }
        }

    }

    public class RouteReplenishmentDetailsClass
    {
        private string replenishmentQtyUM = string.Empty;
        public string ReplenishmentQtyUM
        {
            get { return replenishmentQtyUM; }
            set { replenishmentQtyUM = value; }
        }


        private string primaryUM = string.Empty;
        public string PrimaryUM
        {
            get { return primaryUM; }
            set { primaryUM = value; }
        }



        private string itemID = string.Empty;
        public string ItemID
        {
            get { return itemID; }
            set { itemID = value; }
        }



        private int itemNumber = 0;
        public int ItemNumber
        {
            get { return itemNumber; }
            set { itemNumber = value; }
        }



        private int pickedQty = 0;
        public int PickedQty
        {
            get { return pickedQty; }
            set { pickedQty = value; }
        }




        private int replenishmentQty = 0;
        public int ReplenishmentQty
        {
            get { return replenishmentQty; }
            set { replenishmentQty = value; }
        }




        private int onOrderQty = 0;
        public int OnOrderQty
        {
            get { return onOrderQty; }
            set { onOrderQty = value; }
        }



        private int onOrderQtyInPrimaryUM = 0;
        public int OnOrderQtyInPrimaryUM
        {
            get { return onOrderQtyInPrimaryUM; }
            set { onOrderQtyInPrimaryUM = value; }
        }


        private int adjustedQty = 0;
        public int AdjustedQty
        {
            get { return adjustedQty; }
            set { adjustedQty = value; }
        }

    }


    public class PreOrderItems
    {
        //                string query = @"select 
        //                                A.RPAN8 AS 'CustomerNumber'
        //                                ,A.RPSTDT AS 'StopDate'
        //                                ,A.RPACTID as 'NoActivity'
        //                                ,CASE WHEN A.RPACTID='NOSALE' and B.POAN8 IS NOT NULL  THEN 1 ELSE    0 END AS 'HasPreOrder'
        //                                ,B.POAN8   
        //                                ,ISNULL(B.POUOM,'') AS 'PreOrderUOM'
        //                                ,ISNULL( B.POQTYU / 10000 ,0)AS 'PreOrderQty'
        //                                , B.POITM AS 'ItemID'
        //                                from busdta.M56M0004 A 
        //                                LEFT JOIN BUSDTA.M4016 B ON A.RPAN8 =B.POAN8                              
        //                                where A.RPSTTP='PLANNED'  
        //                                AND A.RPSTDT BETWEEN '" + FromDate.ToString("yyyy-MM-dd") + "' AND '" + ToDate.ToString("yyyy-MM-dd") + @"' 
        //          

        private string customerNumber = string.Empty;

        public string CustomerNumber
        {
            get { return customerNumber; }
            set { customerNumber = value; }
        }

        private string preOrderUOM = string.Empty;

        public string PreOrderUOM
        {
            get { return preOrderUOM; }
            set { preOrderUOM = value; }
        }


        private int preOrderQty = 0;

        public int PreOrderQty
        {
            get { return preOrderQty; }
            set { preOrderQty = value; }
        }

        private string itemID = string.Empty;

        public string ItemID
        {
            get { return itemID; }
            set { itemID = value; }
        }










        private int preOrderQtyInPrimaryUM = 0;
        public int PreOrderQtyInPrimaryUM
        {
            get { return preOrderQtyInPrimaryUM; }
            set { preOrderQtyInPrimaryUM = value; }
        }






        private string itemNumber = string.Empty;
        public string ItemNumber
        {
            get { return itemNumber; }
            set { itemNumber = value; }
        }



        private string primaryUM = string.Empty;
        public string PrimaryUM
        {
            get { return primaryUM; }
            set { primaryUM = value; }
        }




        private double convesionFactor = 1;
        public double ConvesionFactor
        {
            get { return convesionFactor; }
            set { convesionFactor = value; }
        }


    }
}
