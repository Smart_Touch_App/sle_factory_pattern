﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Managers
{
    public class ItemRestrictionManager : IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.ItemRestrictionManager");     //  Do not declare visible instance fields   --Vignesh D

        public string GetFlag(string customerID)
        {
            string str = string.Empty;
            Logger.Info("[SalesLogicExpress.Application.Managers][ItemRestrictionManager][Start:GetFlag][CustomerID = " + customerID + "]");

            try
            {
                // Commented On; 10/11/2016
                //string query = "select P.MAPA8 Parent,P.MAAN8 BillTo, C.AIAN8 ShipTo from";
                //query = query + " BUSDTA.F0150 P join BUSDTA.F0101 B on P.MAAN8=B.ABAN81 and P.MAOSTP='   '";
                //query = query + " join BUSDTA.F03012 C on B.ABAN8=C.AIAN8 ";
                //query = query + " where C.AIAN8 = '" + customerID + "';";

               //  if (customerID != string.Empty && customerID != "''")   //CA1820: Test for empty strings using string length   --Vignesh D
                if(!string.IsNullOrEmpty(customerID))                      //Added by Vignesh D
                {

                    //String Handling for Memory Optimization -- Vignesh D
                    string query = string.Empty;
                     query = "CALL BUSDTA.SP_GetParentID (@customerId='" + customerID + "')";
                     string parentCust = string.Empty;
                     parentCust = Helpers.DbEngine.ExecuteScalar(query);
                    query = string.Empty;

                    // Commented on: 10/11/2016
                    // query = "select AIEDF2, AIAN8 from busdta.F03012 where AIAN8 = '" + parentCust + "'";

                  //  string query = string.Empty;
                    query = "CALL BUSDTA.SP_GetParentIDFlag(@ParentCust='" + parentCust + "') ";

                    //String Handling for Memory Optimization --Vignesh D
                    string flag = string.Empty;
                     flag = Helpers.DbEngine.ExecuteScalar(query);
                    query = string.Empty;
                    str = parentCust + "-" + flag;
                    flag = string.Empty;
                    parentCust = string.Empty;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ItemRestrictionManager][GetFlag][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ItemRestrictionManager][End:GetFlag][CustomerID = " + customerID + "]");
            return str;
        }
        public ObservableCollection<ItemRestriction> GetItemRestrictionList(string customerID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ItemRestrictionManager][Start:GetItemRestrictionList][CustomerID = " + customerID + "]");
            ObservableCollection<ItemRestriction> restrictedItemList = new ObservableCollection<ItemRestriction>();
            try
            {
                //String handling for memory Optimization  --Vignesh D
                string str=string.Empty;
                 str = GetFlag(customerID);
               // if (str != string.Empty && str != null)     //CA1820: Test for empty strings using string length   --Vignesh D
                if(!string.IsNullOrEmpty(str))               //Added by Vignesh D
                {
                    string flag = string.Empty;
                     flag = str.Split('-')[1].Trim();
                     string parentCust = string.Empty;
                     parentCust = str.Split('-')[0];
                     
                    if (!string.IsNullOrEmpty(flag))
                    {
                        // Commented On; 10/11/2016
                        //string query = "select IMLITM,IMDSC1 ";
                        //query = query + " from BUSDTA.F4013 JOIN BUSDTA.F4101 on CAST(SXXRVT as INT)=IMITM JOIN BUSDTA.F4102 on IMITM = IBITM JOIN  busdta.ItemConfiguration ic on imitm = ic.itemId ";
                        //query = query + " where SXXRTC='IR' and rtrim(SXXRVF) like right('00000000'+ '" + parentCust + "', 8)  and RouteEnabled = 1 and allowSearch=1 ";
                     
                        //string handliing for memory optimization  --Vignesh D
                        string query = string.Empty;                        
                         query = "CALL BUSDTA.SP_GetItemRestrictionList(@ParentCust='" + parentCust + "')";
                        DataSet itemList = Helpers.DbEngine.ExecuteDataSet(query);
                        query = string.Empty;
                        ItemRestriction restrictedItem;
                        if (itemList.HasData())
                        {
                            DataRow row;
                            for (int i = 0; i < itemList.Tables[0].Rows.Count; i++)
                            {
                                row = itemList.Tables[0].Rows[i];
                                restrictedItem = new ItemRestriction();
                                restrictedItem.ItemNo = row["IMLITM"].ToString().Trim();
                                restrictedItem.Description = row["IMDSC1"].ToString();
                                restrictedItem.RestrictionFlag = flag;
                                restrictedItemList.Add(restrictedItem);
                            }
                        }

                        restrictedItem = null;  //Object Dispose for memory optimization   --Vignesh D

                        //Dataset disposing for memory optimization  --Vignesh D
                        itemList.Dispose();
                        itemList = null;
                    }

                    flag = string.Empty;
                    parentCust = string.Empty;
                }
                else
                {
                    restrictedItemList = null;
                }
                str = string.Empty;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ItemRestrictionManager][GetItemRestrictionList][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ItemRestrictionManager][End:GetItemRestrictionList][CustomerID = " + customerID + "]");

            return restrictedItemList;
        }

        //Disposable Implementation --Vignesh D
        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
