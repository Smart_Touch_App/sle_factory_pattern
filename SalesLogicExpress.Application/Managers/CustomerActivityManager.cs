﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using log4net;
namespace SalesLogicExpress.Application.Managers
{
    public class CustomerActivityManager : IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.CustomerActivityManager");
        OrderManager objOrderManger = new OrderManager();

        public ObservableCollection<Activity> GetActivities(string customerID)
        {



            // TransactionID as ActivityID, RouteID,TransactionType as ActivityType,TransactionDetailClass as ActivityDetailClass,TransactionDetails as ActivityDetails,TransactionStart as ActivityStart,TransactionEnd as ActivityEnd,StopInstanceID,CustomerID,SettlementID,ParentTransactionID as ActivityHeaderID, TransactionStatus as ActivityStatus
            StringBuilder objQueryBuilder = new StringBuilder();
            //string strVerifyCodeId = DbEngine.ExecuteScalar("SELECT StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VERF'");

            //objQueryBuilder.Append("SELECT  ACT.TDPNTID, ACT.TDID AS ActivityID, ltrim(RTRIM(ACT.TDSTTLID)) AS  SettlementID, ACT.TDROUT AS  RouteID, ACT.TDTYP AS  ActivityType, ACT.TDCLASS AS  ActivityDetailClass, ");
            //objQueryBuilder.Append("ACT.TDDTLS AS  ActivityDetails, ACT.TDSTRTTM AS  ActivityStart, ACT.TDENDTM AS  ActivityEnd, ACT.TDSTID AS  StopInstanceID, ACT.TDAN8 AS  CustomerID, ");
            //objQueryBuilder.Append("ACT.TDPNTID AS  ActivityHeaderID, (CASE RS.Status WHEN " + strVerifyCodeId + " THEN (CASE WHEN ACT.TDSTAT IN ('VoidAtCashCollection', 'VoidAtDeliverCustomer', 'VoidAtOrderEntry', 'PaymentVoid', 'VoidCreditMemo') THEN 'VOIDED / SETTLED' ELSE 'SettlementVerified' END) ");
            //objQueryBuilder.Append("ELSE ACT.TDSTAT END) AS  ActivityStatus FROM busdta.M50012 ACT LEFT OUTER JOIN BUSDTA.Route_Settlement RS ON ACT.TDSTTLID=RS.SettlementId ");
            //objQueryBuilder.Append("WHERE ACT.TDTYP in ('Order','CreditMemo','OrderReturned','Quote','PreOrder','PreOrderChange','Change','ProspectNote','NoActivity') AND TDAN8='" + customerID + "' AND ACT.TDPNTID=0 ");
            //objQueryBuilder.Append("UNION ");
            //objQueryBuilder.Append("SELECT  ACT.TDPNTID, ACT.TDID AS ActivityID, ltrim(RTRIM(ACT.TDSTTLID)) AS  SettlementID, ACT.TDROUT AS  RouteID, ACT.TDTYP AS  ActivityType, ACT.TDCLASS AS  ActivityDetailClass, ");
            //objQueryBuilder.Append("ACT.TDDTLS AS  ActivityDetails, ACT.TDSTRTTM AS  ActivityStart, ACT.TDENDTM AS  ActivityEnd, ACT.TDSTID AS  StopInstanceID, ACT.TDAN8 AS  CustomerID, ");
            //objQueryBuilder.Append("ACT.TDPNTID AS  ActivityHeaderID, (CASE RS.Status WHEN " + strVerifyCodeId + " THEN (CASE WHEN ACT.TDSTAT IN ('VoidAtCashCollection', 'VoidAtDeliverCustomer', 'VoidAtOrderEntry','PaymentVoid', 'VoidCreditMemo') THEN 'VOIDED / SETTLED' ELSE 'SettlementVerified' END) ");
            //objQueryBuilder.Append("ELSE ACT.TDSTAT END) AS  ActivityStatus FROM busdta.M50012 ACT LEFT OUTER JOIN BUSDTA.Route_Settlement RS ON ACT.TDSTTLID=RS.SettlementId ");
            //objQueryBuilder.Append("WHERE ACT.TDTYP in ('Payment','ReturnOrder','HeldReturn') AND TDAN8='" + customerID + "' ORDER BY ActivityEnd DESC");

            //            string query = string.Format(@"select TDID as ActivityID,TDROUT as RouteID,TDTYP as ActivityType,
            //                                            TDCLASS as ActivityDetailClass,TDDTLS as ActivityDetails,
            //                                            TDSTRTTM as ActivityStart,TDENDTM as ActivityEnd,
            //                                            TDSTID as StopInstanceID, TDAN8 as CustomerID,
            //                                            ISNULL(TDSTTLID,'') as SettlementID,TDPNTID as ActivityHeaderID,
            //                                            TDSTAT as ActivityStatus from busdta.M50012 where TDAN8='{0}' and (ISNULL(TDPNTID,0)=0 OR ActivityType='Payment')  ORDER BY TDENDTM DESC", customerID);
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerActivityManager][Start:GetActivities]");
            string strVerifyCodeId = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='VERF')");
            objQueryBuilder.Append("Call BUSDTA.SP_GetActivities(@customerID='" + customerID + "',@VerifyCodeId=" + strVerifyCodeId + ")");
            List<Activity> activities = new List<Activity>();
            try
            {
                DataSet result = Helpers.DbEngine.ExecuteDataSet(objQueryBuilder.ToString());
                if (result.HasData())
                {
                    activities = result.GetEntityList<Activity>();
                    activities = activities.OrderByDescending(a => a.ActivityEnd).ToList<Activity>();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerActivityManager][GetActivities][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerActivityManager][End:GetActivities]");

            return new ObservableCollection<Activity>(activities);
        }

        public string GetInvoiceAmountTotal(string days, string RouteId, string customerID)
        {
            //  string query = string.Empty;   // Unused Local -Vignesh
            string ageAmount = string.Empty;
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerActivityManager][Start:GetInvoiceAmountTotal]");
            try
            {
                //query = "select isnull(sum(InvoiceTotalAmt),0)  FROM BUSDTA.ORDER_HEADER \nwhere OriginatingRouteID={0} AND CustShipToID='{1}' and OrderStateId ={2} \n AND CreatedDatetime BETWEEN DATEADD(day, {3}, getdate()) AND DATEADD(day, {4}, getdate())";
                //switch (days)
                //{
                //    case "30":
                //        query = string.Format(query, RouteId, customerID, ActivityKey.OrderDelivered.GetStatusIdFromDB(), "-30", "0");
                //        break; 
                //    case "90":
                //        query = string.Format(query, RouteId, customerID, ActivityKey.OrderDelivered.GetStatusIdFromDB(), "-90", "-31");
                //        break; 
                //    default:
                //        break;
                //}

                //ageAmount= DbEngine.ExecuteScalar(query);

                ageAmount = objOrderManger.CustActivity_GetInvoiceAmountTotal(days, RouteId, customerID);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerActivityManager][GetInvoiceAmountTotal][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerActivityManager][End:GetInvoiceAmountTotal]");


            return ageAmount;
        }


       
        //Disposable Implementation --Vignesh D
        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                if (objOrderManger != null)
                    objOrderManger.Dispose();
                handle.Dispose();
            }
            disposed = true;
        }


    }
}
