﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Managers
{
    public class ReasonCodeManager : IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("");
        public List<ReasonCodeVM.ReasonCode> GetReasonCodes()
        {

            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][Start:GetReasonCode]");
            List<ReasonCodeVM.ReasonCode> reasonCodes = new List<ReasonCodeVM.ReasonCode>();
            try
            {
                //Commented on 28/09/2016
                //DataTable dtReasonCodes = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Void Order'").Tables[0];
                DataTable dtReasonCodes = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetReasonCodes()").Tables[0];
                foreach (DataRow drReasonCode in dtReasonCodes.Rows)
                {
                    reasonCodes.Add(new ReasonCodeVM.ReasonCode() { ID = Convert.ToInt32(drReasonCode["Id"].ToString()), Code = drReasonCode["ReasonCodeDescription"].ToString() });
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReasonCodeManager][GetReasonCode][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][End:GetReasonCode]");
            
            return reasonCodes;
        }

        public DataTable ARPaymentManager_GetVoidReasonList()
        {
            DataTable dt = new DataTable();
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][Start:ARPaymentManager_GetVoidReasonList]");
            try
            {
                //Commented on:28/09/2016
                //string strQuery = "SELECT ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster WHERE ReasonCodeType='Void Receipt'";
                string strQuery = "CALL BUSDTA.SP_GetVoidReasonList()";
                dt = DbEngine.ExecuteDataSet(strQuery, "VoidReasonList").Tables[0];
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReasonCodeManager][ARPaymentManager_GetVoidReasonList][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][End:ARPaymentManager_GetVoidReasonList]");
            return dt;
        }

        public void ARPaymentManager_VoidReceipt(string strVoidStatusId, string reasonCodeType, string reasonCodeId, string customerId, string receiptId, string routeId)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][Start:ARPaymentManager_VoidReceipt]");
            try
            {
                string strQuery = string.Format("UPDATE BUSDTA.Receipt_Header SET StatusId=" + strVoidStatusId + ", ReasonCodeId=(SELECT ReasonCodeId FROM BUSDTA.ReasonCodeMaster WHERE TRIM(ReasonCodeDescription)=TRIM('{0}') AND ReasonCodeType='" + reasonCodeType + "') WHERE CustBillToId={1} AND ReceiptId={2} AND RouteId={3}", reasonCodeId, customerId, receiptId, routeId);
                DbEngine.ExecuteNonQuery(strQuery);

                strQuery = string.Format("UPDATE BUSDTA.Receipt_Ledger SET StatusID='{0}', VoidCode='V', VoidReasonCode=(SELECT ReasonCodeId FROM BUSDTA.ReasonCodeMaster WHERE TRIM(ReasonCodeDescription)=TRIM('{2}')), AppliedAmount='0' WHERE ReceiptId={1}", strVoidStatusId, receiptId, reasonCodeId);
                DbEngine.ExecuteNonQuery(strQuery);

                ResourceManager.QueueManager.QueueProcess("ARPayments", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReasonCodeManager][ARPaymentManager_VoidReceipt][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][End:ARPaymentManager_VoidReceipt]");
        }

        public DataTable CreditProcessManager_GetCreditReasonCode()
        {
            DataTable dt = new DataTable();
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][Start:CreditProcessManager_GetCreditReasonCode]");
            try
            {
                //Commented on 28/09/2016
                //dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Credit Memo'").Tables[0];
                dt = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetCreditReasonCode()").Tables[0];
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReasonCodeManager][CreditProcessManager_GetCreditReasonCode][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][End:CreditProcessManager_GetCreditReasonCode]");
            return dt;
        }
        public DataTable CreditProcessManager_GetVoidReasonCode()
        {
            DataTable dt = new DataTable();
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][Start:CreditProcessManager_GetVoidReasonCode]");
            try
            {
                dt = DbEngine.ExecuteDataSet("call busdta.SP_GetCreditProcessVoidReasonCode").Tables[0];
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReasonCodeManager][CreditProcessManager_GetVoidReasonCode][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][End:CreditProcessManager_GetVoidReasonCode]");
            return dt;
        }

        public DataSet CustomerDashboardManager_GetNoSaleReason()
        {
            DataSet ds = new DataSet();
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][Start:CustomerDashboardManager_GetNoSaleReason]");
            try
            {
                //Commented on 29/09/2016
                //string query = "select ReasonCodeId, ReasonCode, rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription from busdta.ReasonCodeMaster";
                //query = query + " where ReasonCodeType = 'No Sale Reason'";

                //string Handling for Memory optimization --Vignesh D
                string query = string.Empty;
                 query = "CALL BUSDTA.SP_GetNoSaleReason()";
                ds = Helpers.DbEngine.ExecuteDataSet(query);
                query = string.Empty;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReasonCodeManager][CustomerDashboardManager_GetNoSaleReason][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][End:CustomerDashboardManager_GetNoSaleReason]");
            return ds;
        }

        //public DataSet CycleCountManager_GetReasonCode()
        //{
        //    DataSet ds = new DataSet();
        //    Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][Start:CycleCountManager_GetReasonCode]");
        //    try
        //    {
        //        // Commented On 29/09/2016
        //        //    string query = "select ReasonCodeId, ReasonCode, ReasonCodeDescription from busdta.ReasonCodeMaster";
        //        //    query = query + " where ReasonCodeType = 'Manual Pick'";
        //        string query = "CALL BUSDTA.SP_GetCycleCountReasonCode()";
        //        ds = Helpers.DbEngine.ExecuteDataSet(query);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][ReasonCodeManager][CycleCountManager_GetReasonCode][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][End:CycleCountManager_GetReasonCode]");
        //    return ds;
        //}

        public DataTable CycleCountManager_GetReasonCodeForVoid()
        {
            DataTable dt = new DataTable();
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][Start:CycleCountManager_GetReasonCodeForVoid]");
            try
            {

                //Commented on 29/09/2016
                //dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Void Cycle Count'").Tables[0];
                dt = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetCycleCountReasonCodeForVoid()").Tables[0];

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReasonCodeManager][CycleCountManager_GetReasonCodeForVoid][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][End:CycleCountManager_GetReasonCodeForVoid]");
            return dt;
        }

        public DataTable ExpensesManager_GetVoidReasonCode()
        {
            DataTable dt = new DataTable();
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][Start:ExpensesManager_GetVoidReasonCode]");
            try
            {
                //Commented On 29/09/2016
                //dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Void Expenses'").Tables[0];
                dt = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetExpenseVoidReasonCode()").Tables[0];
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReasonCodeManager][ExpensesManager_GetVoidReasonCode][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][End:ExpensesManager_GetVoidReasonCode]");
            return dt;
        }

        public DataSet InventoryManager_GetReasonCodesForInventoryAdjustment()
        {
            DataSet ds = new DataSet();
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][Start:InventoryManager_GetReasonCodesForInventoryAdjustment]");
            try
            {
                // Commented on :28/9/2016
                //string query = "select ReasonCodeId, ReasonCode,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription from busdta.ReasonCodeMaster";
                //query = query + " where ReasonCodeType = 'Inventory Adjustment'"; 

                string query = "CALL BUSDTA.SP_GetReasonCodesForInventoryAdjustment()";
                ds = DbEngine.ExecuteDataSet(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReasonCodeManager][InventoryManager_GetReasonCodesForInventoryAdjustment][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][End:InventoryManager_GetReasonCodesForInventoryAdjustment]");
            return ds;
        }

        public DataTable MoneyOrderManager_GetVoidReasonCode()
        {
            DataTable dt = new DataTable();
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][Start:MoneyOrderManager_GetVoidReasonCode]");
            try
            {
                //Commented on: 28/9/2016
                // dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Void MoneyOrder'").Tables[0];
                dt = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetVoidReasonCode()").Tables[0];
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReasonCodeManager][MoneyOrderManager_GetVoidReasonCode][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][End:MoneyOrderManager_GetVoidReasonCode]");
            return dt;
        }

        public DataTable OrderManager_GetReasonListForVoidOrder()
        {
            DataTable dt = new DataTable();
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][Start:OrderManager_GetReasonListForVoidOrder]");
            try
            {
                // DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Void Order'").Tables[0];
                dt = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetReasonCodeDetailsByReasonCodeType(@ReasonCodeType = 'Void Order')").Tables[0];
               
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReasonCodeManager][OrderManager_GetReasonListForVoidOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][End:OrderManager_GetReasonListForVoidOrder]");
            return dt;
        }

        public DataTable OrderManager_GetReasonListFor(string Type)
        {
            DataTable dt = new DataTable();
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][Start:OrderManager_GetReasonListFor]");
            try
            {
                //Commented on: 28/9/2016
                // dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster WHERE ReasonCodeType='"+Type+"'").Tables[0];
                dt = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetReasonListForType('" + Type + "')").Tables[0];
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReasonCodeManager][OrderManager_GetReasonListFor][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][End:OrderManager_GetReasonListFor]");
            return dt;
        }

        public DataTable OrderManager_GetPriceOvrCodes(string UnitPriceFlag)
        {
            DataTable dt = new DataTable();
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][Start:OrderManager_GetPriceOvrCodes]");
            try
            {
                //Commented On:4/10/2016
                //                string query = @"select a.reasoncodeid,a.reasoncodedescription,b.drsphd from BUSDTA.ReasonCodeMaster a, BUSDTA.F0005 b where Trim(a.ReasonCode)=trim(b.DRKY) 
                //                                    and a.ReasonCodeType='PriceOverride' and b.DRSY='42' AND b.DRRT='RC' AND b.DRSPHD LIKE 'MOBILE%' ";
                //                if (UnitPriceFlag == "1")
                //                {
                //                    query += " and b.DRSPHD='mobilesamp'";
                //                }
                //                else
                //                {
                //                    query += " and b.DRSPHD <> 'mobilesamp'";
                //                }
                string query = "Call BUSDTA.SP_GetPriceOvrCodes(@unitpriceflag='" + UnitPriceFlag + "')";
                dt = DbEngine.ExecuteDataSet(query).Tables[0];
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReasonCodeManager][OrderManager_GetPriceOvrCodes][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][End:OrderManager_GetPriceOvrCodes]");
            return dt;
        }

        public DataTable PickManager_GetReasonListForManualPick()
        {
            DataTable dt = new DataTable();
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][Start:PickManager_GetReasonListForManualPick]");
            try
            {

                //Commented on: 28/9/2016
                // dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Manual Pick'").Tables[0];
                dt = DbEngine.ExecuteDataSet("CALL BUSDTA.SP_GetReasonListForManualPick()").Tables[0];
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReasonCodeManager][PickManager_GetReasonListForManualPick][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][End:PickManager_GetReasonListForManualPick]");
            return dt;
        }

        public DataTable SettlementConfirmationManager_GetReasonListForVoid(string ReasonCodeType)
        {
            DataTable dt = new DataTable();
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][Start:SettlementConfirmationManager_GetReasonListForVoid]");
            try
            {
                //dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='" + ReasonCodeType + "'").Tables[0];
                dt = DbEngine.ExecuteDataSet("call busdta.SP_GetSettlementReasonListForVoid('" + ReasonCodeType + "')").Tables[0];
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReasonCodeManager][SettlementConfirmationManager_GetReasonListForVoid][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][End:SettlementConfirmationManager_GetReasonListForVoid]");
            return dt;
        }

        public DataTable SettlementConfirmationManager_GetRejectionReasons()
        {
            DataTable dt = new DataTable();
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][Start:SettlementConfirmationManager_GetRejectionReasons]");
            try
            {

                //Commented On: 29/9/2016
                //string query = "SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Reject Settlement'";
                string query = "Call BUSDTA.SP_GetRejectionReasons()";
                dt = DbEngine.ExecuteDataSet(query).Tables[0];
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReasonCodeManager][SettlementConfirmationManager_GetRejectionReasons][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][End:SettlementConfirmationManager_GetRejectionReasons]");
            return dt;
        }

        //************************************************************************************************
        // Comment: Dispose Implementation for Peformance Improvement
        // Created: Nov 03, 2016
        // Author: TechUnison (Velmani Karnan)
        //*************************************************************************************************

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
