﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Application.ViewModels;

namespace SalesLogicExpress.Application.Managers
{
    public class InventoryManager : IDisposable
    {
        public class ItemQty
        {
            public int HeldQuantity { get; set; }
            public int OnHandQty { get; set; }
            public int ComittedQty { get; set; }
        }
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.InventoryManager");      //Do not declare visible instance fields   --Vignesh D
        

        public List<InventoryItem> GetInventoryItems(string Route)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:GetInventoryItems][routeID=" + Route + "]");
            List<InventoryItem> inventoryItems = new List<InventoryItem>();
            try
            {
                //Base derivedInstance = new Derived();
                //Base baseInstance = new Base();

                //Derived good = (Derived)derivedInstance; // OK
                //Derived fail = (Derived)baseInstance; // Throws InvalidCastException

                #region MockDataForInventory
                /*
                System.Collections.ObjectModel.ObservableCollection<TemplateItem> InventoryItems = new TemplateManager().GetTemplateItemsForCustomer("7434430");
                foreach (var item in InventoryItems)
                {
                    InventoryItem inItem = new InventoryItem(item);
                    inItem.QtyOnHand = new Random().Next(1, 50);
                    inItem.AvailableQty = new Random().Next(1, 50);
                    inItem.CommittedQty = new Random().Next(1, 50);
                    inItem.HeldQty = new Random().Next(1, 50);
                    inItem.ParLevel = new Random().Next(1, 300);
                    inItem.OtherUOM = item.AppliedUMS;
                    inventoryItems.Add(inItem);

                }
                if (InventoryItems.Any())
                {
                    inventoryItems[0].IsApproved = true;
                    inventoryItems[0].AvailableQty = 0;
                    inventoryItems[1].IsApproved = true;
                    inventoryItems[1].AvailableQty = 0;
                }
                 */
                #endregion

                // Comment Start On - 09/18/2016
                //var query = "select inventory.ItemId, inventory.ItemNumber, inventory.RouteId, inventory.OnHandQuantity, inventory.CommittedQuantity, inventory.HeldQuantity, inventory.ParLevel,inventory.LastReceiptDate, inventory.LastConsumeDate, inventory.CreatedBy,";
                //query += " inventory.CreatedDatetime, inventory.UpdatedBy, inventory.UpdatedDatetime, im.IMDSC1 as ItemDescription, im.IMLNTY as StkType, im.IMSRP1  as SalesCat1, im.IMSRP5 as SalesCat5, im.IMSRP4 as SalesCat4, iconfig.PrimaryUM as PrimaryUM, ";
                //query += " iconfig.PricingUM,iconfig.TransactionUM as DefaultUM, iconfig.OtherUM1 as OtherUM1, iconfig.OtherUM2,isnull((select top 1 IsApproved from busdta.Inventory_Adjustment where ItemNumber = inventory.ItemNumber order by UpdatedDatetime desc),1) as IsApproved,";
                //query += " isnull((select top 1 AdjustmentStatus from busdta.Inventory_Adjustment where ItemNumber = inventory.ItemNumber order by UpdatedDatetime desc),0) as AdjustmentStatus,isnull((select top 1 IsApplied from busdta.Inventory_Adjustment where ItemNumber = inventory.ItemNumber order by UpdatedDatetime desc),1) as  IsApplied ";
                //query += " from busdta.Inventory inventory join busdta.F4101 im on inventory.ItemNumber = im.IMLITM join busdta.F4102 ib on ib.IBLITM = im.IMLITM join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID ";
                //query += " where iconfig.Sellable = 1 and iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 order by ItemNumber";
                //var dbset = DbEngine.ExecuteDataSet(query);
                // Comment End

                // Call Inventory Inline Query to SP
                string MyQuery = "CALL BUSDTA.SP_GetInventoryItems()";
                DataSet dbset = DbEngine.ExecuteDataSet(MyQuery);
                MyQuery = string.Empty;

                if (dbset.HasData())
                {
                    foreach (DataRow row in dbset.Tables[0].Rows)
                    {
                        var inventoryItem = new InventoryItem();
                        //ItemData
                        inventoryItem.ItemNumber = string.IsNullOrEmpty(row["ItemNumber"].ToString()) ? string.Empty : row["ItemNumber"].ToString().Trim();
                        inventoryItem.ItemDescription = string.IsNullOrEmpty(row["ItemDescription"].ToString()) ? string.Empty : row["ItemDescription"].ToString().Trim();
                        inventoryItem.StkType = string.IsNullOrEmpty(row["StkType"].ToString()) ? string.Empty : row["StkType"].ToString().Trim();
                        inventoryItem.SalesCat1 = string.IsNullOrEmpty(row["SalesCat1"].ToString()) ? string.Empty : row["SalesCat1"].ToString().Trim();
                        inventoryItem.SalesCat5 = string.IsNullOrEmpty(row["SalesCat5"].ToString()) ? string.Empty : row["SalesCat5"].ToString().Trim();
                        inventoryItem.SalesCat4 = string.IsNullOrEmpty(row["SalesCat4"].ToString()) ? string.Empty : row["SalesCat4"].ToString().Trim();
                        inventoryItem.ItemId = string.IsNullOrEmpty(row["ItemId"].ToString()) ? string.Empty : row["ItemId"].ToString().Trim();
                        // Flags
                        // Status implementation on the approval datatype changes
                        //inventoryItem.IsApproved = !string.IsNullOrEmpty(row["IsApproved"].ToString()) && Convert.ToBoolean(row["IsApproved"]);
                        inventoryItem.IsApproved = Convert.ToInt16(row["IsApproved"]);
                        inventoryItem.IsApplied = !string.IsNullOrEmpty(row["IsApplied"].ToString()) && Convert.ToBoolean(row["IsApplied"]);
                        inventoryItem.AdjustmentStatus = Convert.ToInt16(row["AdjustmentStatus"]);
                        // Quantities
                        inventoryItem.CommittedQty = string.IsNullOrEmpty(row["CommittedQuantity"].ToString()) ? 0 : Convert.ToInt32(row["CommittedQuantity"].ToString().Trim());
                        inventoryItem.OnHandQty = string.IsNullOrEmpty(row["OnHandQuantity"].ToString()) ? 0 : Convert.ToInt32(row["OnHandQuantity"].ToString().Trim());
                        inventoryItem.HeldQty = string.IsNullOrEmpty(row["OnHandQuantity"].ToString()) ? 0 : Convert.ToInt32(row["HeldQuantity"].ToString().Trim());
                        inventoryItem.AvailableQty = inventoryItem.OnHandQty - (inventoryItem.CommittedQty + inventoryItem.HeldQty);
                        inventoryItem.ParLevel = string.IsNullOrEmpty(row["OnHandQuantity"].ToString()) ? 0 : Convert.ToInt32(row["ParLevel"].ToString().Trim());

                        // UoMs
                        inventoryItem.DefaultUOM = string.IsNullOrEmpty(row["DefaultUM"].ToString()) ? string.Empty : row["DefaultUM"].ToString().Trim();
                        var otherUoM1 = string.IsNullOrEmpty(row["OtherUM1"].ToString()) ? null : row["OtherUM1"].ToString().Trim();
                        var otherUoM2 = string.IsNullOrEmpty(row["OtherUM2"].ToString()) ? null : row["OtherUM2"].ToString().Trim();
                        inventoryItem.PrimaryUOM = string.IsNullOrEmpty(row["PrimaryUM"].ToString()) ? string.Empty : row["PrimaryUM"].ToString().Trim();
                        inventoryItem.PricingUOM = string.IsNullOrEmpty(row["PricingUM"].ToString()) ? string.Empty : row["PricingUM"].ToString().Trim();
                        if (!string.IsNullOrEmpty(otherUoM1)) inventoryItem.OtherUOM.Add(otherUoM1);
                        if (!string.IsNullOrEmpty(otherUoM2)) inventoryItem.OtherUOM.Add(otherUoM2);
                        //Dates
                        if (!string.IsNullOrEmpty(row["LastReceiptDate"].ToString()))
                            inventoryItem.LastReceiptDate = Convert.ToDateTime(row["LastReceiptDate"]);
                        else
                        {
                            inventoryItem.LastReceiptDate = null;
                        }
                        if (!string.IsNullOrEmpty(row["LastConsumeDate"].ToString()))
                            inventoryItem.LastConsumeDate = Convert.ToDateTime(row["LastConsumeDate"]);
                        else
                        {
                            inventoryItem.LastConsumeDate = null;
                        }
                        inventoryItems.Add(inventoryItem);
                        inventoryItem = null;
                    }
                }
                dbset.Dispose();
                dbset = null;
            }
            catch (Exception ex)
            {
                inventoryItems = null;
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][GetInventoryItems][routeID=" + Route + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:GetInventoryItems][routeID=" + Route + "]");
            return inventoryItems;
        }
        public bool SaveInventoryAdjustment(InventoryAdjustment Adjustment)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:SaveInventoryAdjustment][Adjustment=" + Adjustment.SerializeToJson() + "]");
            try
            {
                //** Query needs to be changed after to get transaction updation
                // Query to log adjustment entry in Inventory_Ledger
                // Here transactionId passed as "101" as inventory adjustment is route level activity.



                string saveInvAdj = "INSERT INTO BUSDTA.Inventory_Adjustment(ItemId,ItemNumber,RouteId,TransactionQty,TransactionQtyUM,TransactionQtyPrimaryUM,ReasonCode,IsApproved,IsApplied,AdjustmentStatus,Source,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime)" +
                                    " VALUES(" + Adjustment.Item.ItemId + ",'" + Adjustment.Item.ItemNumber + "'," + CommonNavInfo.RouteID + "," + Adjustment.AdjustmentQuantity + ",'" + Adjustment.AdjustmentUOM + "'," +
                                    "'" + Adjustment.Item.PrimaryUOM + "'," + Adjustment.SelectedReasonCode.ReasonCodeID + ",0,0,1,1," + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate())";
                DbEngine.ExecuteNonQuery(saveInvAdj);
                saveInvAdj = string.Empty;
                var updateInventory = " update busdta.inventory set OnHandQuantity = OnHandQuantity + " + Adjustment.AdjustmentQuantity + ",UpdatedBy='" + UserManager.UserId.ToString() + "',UpdatedDatetime=GETDATE() where ItemId = " + Adjustment.Item.ItemId.Trim() + " and RouteId = " + CommonNavInfo.RouteID;

                DbEngine.ExecuteNonQuery(updateInventory);
                updateInventory = string.Empty;
                Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:SaveInventoryAdjustment][Adjustment=" + Adjustment.SerializeToJson() + "]");

                Adjustment.AdjustmentDate = DateTime.Now; // To show in EOD Transactions

                var maxId = DbEngine.ExecuteScalar("select max(InventoryAdjustmentId) from busdta.Inventory_Adjustment");
                Adjustment.InventoryAdjustmentID = string.IsNullOrEmpty(maxId) ? 0 : Convert.ToInt32(maxId);


                string logInvAdj = "INSERT INTO BUSDTA.Inventory_Ledger(ItemId,ItemNumber,RouteId,TransactionQty,TransactionQtyUM,TransactionQtyPrimaryUM,TransactionType,TransactionId,SettlementID,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime)";
                logInvAdj += "VALUES(" + Adjustment.Item.ItemId + ",'" + Adjustment.Item.ItemNumber + "'," + CommonNavInfo.RouteID + "," + Adjustment.AdjustmentQuantity + ",'" + Adjustment.AdjustmentUOM + "'," +
                         "'" + Adjustment.Item.PrimaryUOM + "',8," + Adjustment.InventoryAdjustmentID + ",0," + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate())";
                DbEngine.ExecuteNonQuery(logInvAdj);
                logInvAdj = string.Empty;

                LogActivityForInventoryAdjustment(ActivityKey.InvAdjustment, Adjustment);

                ResourceManager.QueueManager.QueueProcess("Inventory", false);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][SaveInventoryAdjustment][Adjustment=" + Adjustment.SerializeToJson() + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                return false;
            }
        }

        /// <summary>
        /// Code Replaced By GetUoMFactor() from inventory manager
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private void LogActivityForInventoryAdjustment(ActivityKey key, InventoryAdjustment Adjustment)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryManager][Start:LogActivityForInventoryAdjustment]");
            try
            {
                Activity ac = new Activity();
                {
                    ac.ActivityType = key.ToString();
                    ac.ActivityStart = DateTime.Now;
                    ac.ActivityEnd = DateTime.Now;
                    ac.IsTxActivity = true;
                    ac.ActivityDetailClass = Adjustment.ToString();
                    ac.ActivityDetails = Adjustment.SerializeToJson();
                    ac.RouteID = CommonNavInfo.RouteID.ToString();
                }


                //Get the parent activity id
               // Activity activity = ResourceManager.Transaction.LogActivity(ac);
                ResourceManager.Transaction.LogActivity(ac);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][InventoryManager][LogActivityForInventoryAdjustment][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryManager][End:LogActivityForInventoryAdjustment]");
        }
        /// <summary>
        /// Code Replaced By GetUoMFactor() from inventory manager
        /// </summary>
        /// <param name="ItemID"></param>
        /// <returns></returns>
        #region Deprecated
        /* public List<ItemUOMConversion> GetUOMConversionList(string ItemID)
        {
            List<ItemUOMConversion> itemUOMList = new List<ItemUOMConversion>();
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:GetUOMConversionList]");
            try
            {
                //string query = string.Empty;
                //DbEngine.ExecuteNonQuery(query);

                // Mock Data for Conversion
                // Item Number is not set for now, until we have the data in DB
                //inventoryItems.Add(new ItemUOMConversion { FromUOM = "CS", ToUOM = "EA", ConversionFactor = 6 });
                //inventoryItems.Add(new ItemUOMConversion { FromUOM = "EA", ToUOM = "CS", ConversionFactor = .6 });

                string query = string.Format("select im.IMLITM 'ItemNumber', ItemID, FromUOM, ToUOM, ConversionFactor from busdta.UoMFactorReference umf inner join busdta.F4101 im on umf.ItemID = im.imitm where ItemID = {0}", ItemID);
                DataSet dbset = DbEngine.ExecuteDataSet(query);
                if (dbset.HasData())
                {
                    foreach (DataRow dataRow in dbset.Tables[0].Rows)
                    {
                        var UoMConversion = new ItemUOMConversion();

                        UoMConversion.FromUOM = string.IsNullOrEmpty(dataRow["FromUOM"].ToString()) ? null : dataRow["FromUOM"].ToString().Trim();
                        UoMConversion.ToUOM = string.IsNullOrEmpty(dataRow["ToUOM"].ToString()) ? null : dataRow["ToUOM"].ToString().Trim();
                        UoMConversion.ItemNumber = string.IsNullOrEmpty(dataRow["ItemNumber"].ToString()) ? null : dataRow["ItemNumber"].ToString().Trim();
                        UoMConversion.ConversionFactor = string.IsNullOrEmpty(dataRow["ConversionFactor"].ToString()) ? 0 : Convert.ToDouble(dataRow["ConversionFactor"].ToString().Trim());
                        UoMConversion.ItemID = string.IsNullOrEmpty(dataRow["ItemID"].ToString()) ? string.Empty : dataRow["ItemID"].ToString().Trim();

                        itemUOMList.Add(UoMConversion);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][GetUOMConversionList][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                return null;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:GetUOMConversionList]");
            return itemUOMList;
        }*/
        #endregion
        public List<ReasonCodeItem> GetReasonCodesForInventoryAdjustment()
        {
            List<ReasonCodeItem> reasonCodeList = new List<ReasonCodeItem>();
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:GetReasonCodesForInventoryAdjustment]");
            try
            {
                //string query = "select ReasonCodeId, ReasonCode,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription from busdta.ReasonCodeMaster";
                //query = query + " where ReasonCodeType = 'Inventory Adjustment'"; ;
                //DataSet dbSet = DbEngine.ExecuteDataSet(query);
                ReasonCodeManager objReasonCodeManager = new ReasonCodeManager();
                DataSet dbSet = objReasonCodeManager.InventoryManager_GetReasonCodesForInventoryAdjustment();
                if (dbSet.HasData())
                {
                    reasonCodeList.AddRange(from DataRow dataRow in dbSet.Tables[0].Rows
                                            select new ReasonCodeItem
                                            {
                                                ReasonCodeID = Convert.ToInt32(dataRow["ReasonCodeId"].ToString().Trim()),
                                                ReasonCodeDescription = dataRow["ReasonCodeDescription"].ToString().Trim()
                                            });
                }
                objReasonCodeManager.Dispose();
                objReasonCodeManager = null;
                dbSet.Dispose();
                dbSet = null;
                // Mock Data
                //reasonCodeList.Add(new ReasonCodeItem { ReasonCodeID = 1, ReasonCodeDescription = "Reason 1" });
                //reasonCodeList.Add(new ReasonCodeItem { ReasonCodeID = 2, ReasonCodeDescription = "Reason 2" });
                //reasonCodeList.Add(new ReasonCodeItem { ReasonCodeID = 3, ReasonCodeDescription = "Reason 3" });
                //reasonCodeList.Add(new ReasonCodeItem { ReasonCodeID = 4, ReasonCodeDescription = "Reason 4" });
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][GetReasonCodesForInventoryAdjustment][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                reasonCodeList = null;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:GetReasonCodesForInventoryAdjustment]");
            return reasonCodeList;
        }
        public bool SaveParLevelAdjustment(ParLevelAdjustment Adjustment)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:SaveInventoryAdjustment][Adjustment=" + Adjustment.SerializeToJson() + "]");
                string query = "UPDATE BUSDTA.Inventory SET ParLevel = " + Adjustment.ModifiedParlevel + ",UpdatedBy =" + Managers.UserManager.UserId + ",UpdatedDatetime = getdate() WHERE ItemId=" + Adjustment.Item.ItemId + "";
                DbEngine.ExecuteNonQuery(query);
                query = string.Empty;
                ResourceManager.QueueManager.QueueProcess("Inventory", false);
                Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:SaveInventoryAdjustment][Adjustment=" + Adjustment.SerializeToJson() + "]");
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][SaveParLevelAdjustment][Adjustment=" + Adjustment.SerializeToJson() + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                return false;
            }

        }

        //Unused Function Commented for performance --Vignesh D

        //public bool UpdateDeletedComittedQuantity(string ItemId, int Qty, string OrderId)
        //{
        //    bool result = false;
        //    Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:UpdateComittedQuantity]");
        //    try
        //    {
        //        string query = string.Empty;

        //        query = @"update busdta.Inventory set CommittedQuantity= CommittedQuantity {0} {1},UpdatedBy =" + Managers.UserManager.UserId + ",UpdatedDatetime = getdate()  where ItemId='{2}'";
        //        query = string.Format(query, "-", Qty, ItemId);
        //        int count = DbEngine.ExecuteNonQuery(query);

        //        var updateLedger = "UPDATE BUSDTA.Inventory_Ledger SET TransactionQty=0 ,UpdatedBy =" + UserManager.UserId + ",UpdatedDatetime = getdate() WHERE ItemId=" + ItemId + " AND RouteId='" + CommonNavInfo.RouteID + "' AND TransactionId = " + OrderId;
        //        DbEngine.ExecuteNonQuery(updateLedger);

        //        ResourceManager.QueueManager.QueueProcess("Inventory", false);
        //    }
        //    catch (Exception ex)
        //    {
        //        result = false;
        //        Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][UpdateComittedQuantity][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:UpdateComittedQuantity]");
        //    return result;
        //}

        //Unused function commented for performance --Vignesh D

        //public bool UpdateCommittedQuantity(List<Item> ItemList, bool NegateQuantity = false)
        //{
        //    bool result = false;
        //    Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:UpdateComittedQuantity]");
        //    try
        //    {
        //        string query = string.Empty;
        //        foreach (Item item in ItemList)
        //        {
        //            query = @"update busdta.Inventory set CommittedQuantity= CommittedQuantity {0} {1},UpdatedBy =" + Managers.UserManager.UserId + ",UpdatedDatetime = getdate()  where ItemId='{2}'";
        //            query = string.Format(query, NegateQuantity ? "-" : "+", item.OrderQty, item.ItemId);
        //            int count = DbEngine.ExecuteNonQuery(query);
        //        }

        //        ResourceManager.QueueManager.QueueProcess("Inventory", false);
        //    }
        //    catch (Exception ex)
        //    {
        //        result = false;
        //        Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][UpdateComittedQuantity][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:UpdateComittedQuantity]");
        //    return result;
        //}

        private int GetOrderQtyInPrimaryUM(Item item)
        {
            int orderQty = 0;
            int pickedOrderQty = 0;
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:GetOrderQtyInPrimaryUM]");
            try
            {
                orderQty = Convert.ToInt32(item.OrderQty * UoMManager.GetUoMFactor(item.UM, item.PrimaryUM, Convert.ToInt32(item.ItemId), item.ItemNumber));
                var pickedItems = (new PickManager()).GetPickItemsFromDB(PayloadManager.OrderPayload.OrderID);

                if (pickedItems.Count > 0)
                {
                    var pickedItem = pickedItems.Where(x => x.ItemId == item.ItemId && x.UM == item.UM).FirstOrDefault();

                    if (pickedItem != null)
                    {
                        //if (item.UM == "CS")
                        //    pickedItem.PickedQuantityInPrimaryUOM = pickedItem.PickedQuantityInPrimaryUOM / item.LastComittedQty;
                        pickedOrderQty = Convert.ToInt32(pickedItem.PickedQuantityInPrimaryUOM);
                        //* UoMManager.GetUoMFactor(pickedItem.UM, pickedItem.PrimaryUM, Convert.ToInt32(pickedItem.ItemId), pickedItem.ItemNumber));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][GetOrderQtyInPrimaryUM][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:GetOrderQtyInPrimaryUM]");
            return (pickedOrderQty > orderQty ? pickedOrderQty : orderQty);
        }
        public bool UpdateCommittedQuantity(List<OrderItem> updateItemList, StatusTypesEnum activity)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:UpdateCommittedQuantity]");
                //  string query = string.Empty;   //Unused Local --Vignesh D
                int transactionCode = 0;

                if (ResourceManager.StatusTypes == null || !ResourceManager.StatusTypes.Any())
                    ResourceManager.StatusTypes = GetStatusTypes();

                var statusType = ResourceManager.StatusTypes.FirstOrDefault(x => string.Equals(x.StatusDesc, activity.ToString(), StringComparison.CurrentCultureIgnoreCase));
                if (statusType != null)
                {
                    transactionCode = statusType.ID;
                }


                //Check whether order is already on hold or not.
                //string checkHold = "select HoldCommitted From busdta.ORDER_HEADER where OrderID = " + PayloadManager.OrderPayload.OrderID;

                string checkHold = "CALL BUSDTA.SP_GetHoldCommitted(@OrderID = '" + PayloadManager.OrderPayload.OrderID + "')";
                var checkHoldResult = DbEngine.ExecuteScalar(checkHold);
                checkHold = string.Empty;
                if (string.IsNullOrEmpty(checkHoldResult)) checkHoldResult = "0";

                if (Convert.ToBoolean(Convert.ToInt32(checkHoldResult.Trim())) && activity == StatusTypesEnum.HOLD)
                {
                    try
                    {


                        //                      var checkExistingInv = @"SELECT * FROM BUSDTA.Inventory_Ledger IL WHERE 
                        //                                                IL.TransactionId ='{0}'
                        //                                                AND IL.InventoryLedgerID = (SELECT MAX(InventoryLedgerID) FROM BUSDTA.Inventory_Ledger IL1 WHERE IL1.ITEMID=IL.ItemId)
                        //                                                ORDER BY TransactionId";
                        //checkExistingInv = string.Format(checkExistingInv, PayloadManager.OrderPayload.OrderID);

                        var checkExistingInv = "CALL BUSDTA.SP_GetInventoryLedger(@OrderID = '" + PayloadManager.OrderPayload.OrderID + "')";
                        var invDbSet = DbEngine.ExecuteDataSet(checkExistingInv);
                        checkExistingInv = string.Empty;
                        if (invDbSet.HasData())
                        {

                            List<Item> ledgerEntryList = new List<Item>();
                            var invRows = invDbSet.Tables[0].Rows;
                            foreach (DataRow dataRow in invRows)
                            {

                                Item dummyItem = new Item
                                                         {
                                                             ItemNumber = dataRow["ItemNumber"].ToString().Trim(),
                                                             OrderQty = Convert.ToInt32(dataRow["TransactionQty"].ToString().Trim()),
                                                             ItemId = dataRow["ItemId"].ToString().Trim(),
                                                             UM = dataRow["TransactionQtyUM"].ToString().Trim()
                                                         };
                                ledgerEntryList.Add(dummyItem);
                            }

                            foreach (Item i in ledgerEntryList)
                            {
                                if (!updateItemList.Any(x => x.ItemNumber.Trim() == i.ItemNumber.Trim()))
                                {
                                    var updateInv = "UPDATE BUSDTA.Inventory SET CommittedQuantity= CommittedQuantity - " + i.OrderQty + " WHERE ItemId=" + i.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                                    DbEngine.ExecuteNonQuery(updateInv);

                                    var updateLedger = "DELETE FROM BUSDTA.Inventory_Ledger WHERE ItemId=" + i.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "' AND TransactionId = " + PayloadManager.OrderPayload.OrderID;
                                    DbEngine.ExecuteNonQuery(updateLedger);

                                    string count1 = DbEngine.ExecuteScalar("Select COUNT(*) from busdta.PickOrder where order_id = '" + PayloadManager.OrderPayload.OrderID + "' and item_number='" + i.ItemNumber + "' and Picked_Qty_Primary_UOM>0");

                                    if (count1.Trim() == "0")
                                        DbEngine.ExecuteNonQuery("delete from busdta.PickOrder where order_id = '" + PayloadManager.OrderPayload.OrderID + "' and item_number='" + i.ItemNumber + "'");
                                }
                            }

                            foreach (OrderItem o_Item in updateItemList)
                            {
                                if (ledgerEntryList.Any(x => x.ItemNumber.Trim() == o_Item.ItemNumber.Trim()))
                                {
                                    var ledgerItem = ledgerEntryList.First(x => x.ItemNumber.Trim() == o_Item.ItemNumber.Trim());
                                    if (ledgerItem.OrderQty != o_Item.OrderQty)
                                    {
                                        // remove previously added qty
                                        var updateInv = "UPDATE BUSDTA.Inventory SET CommittedQuantity= CommittedQuantity - " + ledgerItem.OrderQty + " WHERE ItemId=" + o_Item.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                                        DbEngine.ExecuteNonQuery(updateInv);
                                        // updates modified qty
                                        var o_ItemInv = "UPDATE BUSDTA.Inventory SET CommittedQuantity= CommittedQuantity + " + GetOrderQtyInPrimaryUM(o_Item) + " WHERE ItemId=" + o_Item.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                                        DbEngine.ExecuteNonQuery(o_ItemInv);

                                        var updateLedger = "UPDATE BUSDTA.Inventory_Ledger SET TransactionQty=" + GetOrderQtyInPrimaryUM(o_Item) + ",TransactionQtyUM='" + o_Item.UM.Trim() + "',UpdatedBy =" + UserManager.UserId + ",UpdatedDatetime = getdate() WHERE ItemId=" + o_Item.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "' AND TransactionId = " + PayloadManager.OrderPayload.OrderID;
                                        DbEngine.ExecuteNonQuery(updateLedger);
                                    }
                                    else if (ledgerItem.UM != o_Item.UM)
                                    {
                                        if (ledgerItem.OrderQty < GetOrderQtyInPrimaryUM(o_Item))
                                        {
                                            // remove previously added qty
                                            var updateInv = "UPDATE BUSDTA.Inventory SET CommittedQuantity= CommittedQuantity - " + ledgerItem.OrderQty + " WHERE ItemId=" + o_Item.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                                            DbEngine.ExecuteNonQuery(updateInv);
                                            // updates modified qty
                                            var o_ItemInv = "UPDATE BUSDTA.Inventory SET CommittedQuantity= CommittedQuantity + " + GetOrderQtyInPrimaryUM(o_Item) + " WHERE ItemId=" + o_Item.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                                            DbEngine.ExecuteNonQuery(o_ItemInv);

                                            var updateLedger = "UPDATE BUSDTA.Inventory_Ledger SET TransactionQty=" + GetOrderQtyInPrimaryUM(o_Item) + ",TransactionQtyUM='" + o_Item.UM.Trim() + "',UpdatedBy =" + UserManager.UserId + ",UpdatedDatetime = getdate() WHERE ItemId=" + o_Item.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "' AND TransactionId = " + PayloadManager.OrderPayload.OrderID;
                                            DbEngine.ExecuteNonQuery(updateLedger);
                                        }
                                    }
                                    else
                                    {

                                    }
                                }
                                else
                                {
                                    // if item is newly added to order update its inventory 
                                    string updateQuery = "UPDATE BUSDTA.Inventory SET CommittedQuantity= CommittedQuantity + " + GetOrderQtyInPrimaryUM(o_Item) + " WHERE ItemId=" + o_Item.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                                    DbEngine.ExecuteNonQuery(updateQuery);

                                    // add item entry to inventory ledger
                                    string ledgerEntry = "INSERT INTO BUSDTA.Inventory_Ledger (ItemId,ItemNumber,RouteId,TransactionQty,TransactionQtyUM,TransactionQtyPrimaryUM,TransactionType,TransactionId,SettlementID," +
                                         "CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime) VALUES(" + o_Item.ItemId.Trim() + ",'" + o_Item.ItemNumber.Trim() + "'," + CommonNavInfo.RouteID + "," + GetOrderQtyInPrimaryUM(o_Item) + "," +
                                         "'" + o_Item.UM.Trim() + "','" + o_Item.PrimaryUM.Trim() + "'," + transactionCode + "," + PayloadManager.OrderPayload.OrderID + ",0," + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate())";
                                    DbEngine.ExecuteNonQuery(ledgerEntry);
                                }
                            }
                        }
                        //string updateOrderStatus = "update busdta.ORDER_HEADER set HoldCommitted = '0' where OrderID = " + PayloadManager.OrderPayload.OrderID;
                        //DbEngine.ExecuteNonQuery(updateOrderStatus);

                        ResourceManager.QueueManager.QueueProcess("Inventory", false);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][UpdateCommittedQuantity][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                        return false;
                    }
                }


                // decides increment and decrement of committed qty according "Hold" or "Void"
                var sign = activity == StatusTypesEnum.HOLD ? "+" : "-";

                // Check for previous inventory change and first void , No inventory manipulation for first time 'Void order'
                var count = DbEngine.ExecuteScalar("select count(1) from busdta.Inventory_Ledger where TransactionId = " + PayloadManager.OrderPayload.OrderID);
                if (activity == StatusTypesEnum.VOID && Convert.ToInt32(count) == 0) return true;

                foreach (var inventoryItem in updateItemList)
                {
                    string factor;
                    // string getUmFactor = string.Empty;    //Unused local --Vignesh D

                    if (!string.Equals(inventoryItem.UM.Trim(), inventoryItem.PrimaryUM.Trim()))
                    {
                        /**Code Commented as table is deprecated**/
                        //getUmFactor = "select ConversionFactor from busdta.UoMFactorReference where FromUOM = '" + inventoryItem.UM.Trim() + "' and ToUOM = '" + inventoryItem.PrimaryUM.Trim() + "' and ItemID =" + inventoryItem.ItemId.Trim();
                        //factor = DbEngine.ExecuteScalar(getUmFactor);

                        if (UoMManager.ItemUoMFactorList != null)
                        {
                            var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == inventoryItem.ItemId.Trim()) && (x.FromUOM == inventoryItem.UM.Trim().ToString()) && (x.ToUOM == inventoryItem.PrimaryUM.Trim()));
                            factor = itemUomConversion == null ? Convert.ToDecimal(UoMManager.GetUoMFactor(inventoryItem.UM.Trim(), inventoryItem.PrimaryUM.Trim(), Convert.ToInt32(inventoryItem.ItemId.Trim()), inventoryItem.ItemNumber.Trim().ToString())).ToString() : Convert.ToDecimal(itemUomConversion.ConversionFactor).ToString();
                        }
                        else
                        {
                            factor = Convert.ToDecimal(UoMManager.GetUoMFactor(inventoryItem.UM.Trim(), inventoryItem.PrimaryUM.Trim(), Convert.ToInt32(inventoryItem.ItemId.Trim()), inventoryItem.ItemNumber.Trim())).ToString();
                        }
                        if (string.IsNullOrEmpty(factor)) factor = "1";
                    }
                    else
                    {
                        factor = "1";
                    }

                    var commitedQty = Convert.ToInt32(inventoryItem.OrderQty * Convert.ToDouble(factor.Trim()));
                    commitedQty = GetOrderQtyInPrimaryUM(inventoryItem);
                    switch (activity)
                    {
                        case StatusTypesEnum.DLVRD:
                            string updateQueryDelivered = "UPDATE BUSDTA.Inventory SET CommittedQuantity= CommittedQuantity - " + commitedQty + ",OnHandQuantity = OnHandQuantity - " + commitedQty + ",UpdatedBy =" + Managers.UserManager.UserId + ",UpdatedDatetime = GETDATE() WHERE ItemId=" + inventoryItem.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                            DbEngine.ExecuteNonQuery(updateQueryDelivered);
                            break;
                        default:
                            string updateQuery = "UPDATE BUSDTA.Inventory SET CommittedQuantity= CommittedQuantity " + sign + " " + commitedQty + ",UpdatedBy =" + Managers.UserManager.UserId + ",UpdatedDatetime = GETDATE() WHERE ItemId=" + inventoryItem.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                            DbEngine.ExecuteNonQuery(updateQuery);
                            break;
                    }


                    string ledgerEntry = "INSERT INTO BUSDTA.Inventory_Ledger (ItemId,ItemNumber,RouteId,TransactionQty,TransactionQtyUM,TransactionQtyPrimaryUM,TransactionType,TransactionId,SettlementID," +
                                         "CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime) VALUES(" + inventoryItem.ItemId.Trim() + ",'" + inventoryItem.ItemNumber.Trim() + "'," + CommonNavInfo.RouteID + "," + sign + GetOrderQtyInPrimaryUM(inventoryItem) + "," +
                                         "'" + inventoryItem.UM.Trim() + "','" + inventoryItem.PrimaryUM.Trim() + "'," + transactionCode + "," + PayloadManager.OrderPayload.OrderID + ",0," + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate())";
                    DbEngine.ExecuteNonQuery(ledgerEntry);

                }

                if (activity == StatusTypesEnum.HOLD)
                {
                    string updateOrderStatus = "update busdta.ORDER_HEADER set HoldCommitted = '1',UpdatedBy =" + Managers.UserManager.UserId + ",UpdatedDatetime = GETDATE() where OrderID = " + PayloadManager.OrderPayload.OrderID;
                    DbEngine.ExecuteNonQuery(updateOrderStatus);
                    ResourceManager.QueueManager.QueueProcess("OrderManagement", false);
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:UpdateCommittedQuantity]");

                ResourceManager.QueueManager.QueueProcess("Inventory", false);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][UpdateCommittedQuantity][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                return false;
            }
        }

        //Unused function commented for performance --Vignesh D 
        //public int GetAvailableQuantity(string ItemID)
        //{
        //    int AvailableQty = 0;
        //    Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:GetAvailableQuantity]");
        //    try
        //    {
        //        string query = string.Empty;
        //        query = @"SELECT (isnull(OnHandQuantity,0)-isnull(CommittedQuantity,0)-isnull(HeldQuantity,0)) as AvailableQty FROM BUSDTA.Inventory inv WHERE INV.ITEMID='{0}' AND ROUTEid='{1}'";
        //        query = string.Format(query, ItemID, CommonNavInfo.RouteID);
        //        AvailableQty = Convert.ToInt32(DbEngine.ExecuteScalar(query));
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][GetAvailableQuantity][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:GetAvailableQuantity]");
        //    return AvailableQty;
        //}


        public ItemQty GetItemQuantity(string ItemID)
        {
            ItemQty item = new ItemQty();
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:GetItemQuantity]");
            try
            {
                //string query = string.Empty;
                //query = @"SELECT CAST(isnull(OnHandQuantity,0) AS VARCHAR) +'|' + CAST(isnull(CommittedQuantity,0) AS VARCHAR) + '|' + CAST(isnull(HeldQuantity,0) AS VARCHAR) AS QTY FROM BUSDTA.Inventory inv WHERE INV.ITEMID='{0}' AND ROUTEid='{1}'";
                //query = string.Format(query, ItemID, CommonNavInfo.RouteID);

                string MyQuery = "CALL BUSDTA.SP_GetItemQuantity(@ItemID='" + ItemID + "', @RouteID = '" + CommonNavInfo.RouteID + "')";
                string result = Convert.ToString(DbEngine.ExecuteScalar(MyQuery));
                MyQuery = string.Empty;
                if (!string.IsNullOrEmpty(result))
                {
                    item.OnHandQty = Convert.ToInt32(result.Split('|')[0]);
                    item.ComittedQty = Convert.ToInt32(result.Split('|')[1]);
                    item.HeldQuantity = Convert.ToInt32(result.Split('|')[2]);
                }
                result = string.Empty;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][GetItemQuantity][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:GetItemQuantity]");
            return item;
        }
        internal List<StatusType> GetStatusTypes()
        {
            var statusTypeList = new List<StatusType>();
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:GetStatusTypes]");
                //string query = "Select * from busdta.Status_Type";

                string MyQuery = "CALL BUSDTA.SP_GetStatusTypes()";
                var dbSet = DbEngine.ExecuteDataSet(MyQuery);
                if (dbSet.HasData())
                {
                    statusTypeList.AddRange(from DataRow row in dbSet.Tables[0].Rows
                                            select new StatusType
                                            {
                                                ID = Convert.ToInt32(row["StatusTypeID"].ToString().Trim()),
                                                StatusCode = row["StatusTypeCD"].ToString().Trim(),
                                                StatusDesc = row["StatusTypeDESC"].ToString().Trim()
                                            });
                }
                dbSet.Dispose();
                dbSet = null;
                Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:GetStatusTypes]");
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][GetStatusTypes][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                statusTypeList = null;
            }
            return statusTypeList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetItemIdFromNumber(string ItemNumber)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:GetItemIdFromNumber]");
            string itemId = "";
            try
            {

                itemId = DbEngine.ExecuteScalar("SELECT ItemId  FROM BUSDTA.Inventory WHERE ItemNumber='" + ItemNumber + "'");
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][GetItemIdFromNumber][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:GetItemIdFromNumber]");
            return itemId;

        }

        //In case of Unload Replenishment and SuggestionReturn, Update All Items Replenishment Qty as Committed in inventory once we land on Pick Screen.
        public void UpdateCommittedQtyInTruckToBranchFlow(string ItemID, int CommittedQtyToAddOrSubstract, bool isAddQty)
        {
            // ItemQty item = new ItemQty();   //Unused Local 
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:UpdateCommittedQtyInTruckToBranchFlow]");
            try
            {
                string query = string.Empty;
                if (isAddQty)
                {
                    query = @"UPDATE BUSDTA.Inventory SET  CommittedQuantity =CommittedQuantity + " + CommittedQtyToAddOrSubstract + "  WHERE ITEMID='" + ItemID + "'";
                }
                else
                {
                    query = @"UPDATE BUSDTA.Inventory SET  CommittedQuantity =CommittedQuantity - " + CommittedQtyToAddOrSubstract + "  WHERE ITEMID='" + ItemID + "'";
                }
                //  int cnt = DbEngine.ExecuteNonQuery(query);    //UNused Local --Vignesh D
                DbEngine.ExecuteNonQuery(query);               // Added by Vignesh D
                query = string.Empty;
                ResourceManager.QueueManager.QueueProcess("Inventory", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][UpdateCommittedQtyInTruckToBranchFlow][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:UpdateCommittedQtyInTruckToBranchFlow]");
        }

        public string UpdateTransactionQtyInItemLedger(Item item, string orderId, int quantity)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:UpdateTransactionQtyInItemLedger]");
            string itemId = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(item.ItemId))
                {
                    var updateLedger = "UPDATE BUSDTA.Inventory_Ledger SET TransactionQty=TransactionQty + " + quantity + ", UpdatedBy =" + UserManager.UserId + ", UpdatedDatetime = getdate() WHERE ItemId=" + item.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "' AND TransactionId = " + orderId;
                    DbEngine.ExecuteNonQuery(updateLedger);
                    updateLedger = string.Empty;
                    ResourceManager.QueueManager.QueueProcess("Inventory", false);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][UpdateTransactionQtyInItemLedger][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:UpdateTransactionQtyInItemLedger]");
            return itemId;

        }

        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/
        /* Return Manager - Common Inventory Function */
        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/


        //Unused function commented for performance --Vignesh D
        // Update Return Inventory for Save or Hold
        //public void UpdateReturnInventoryForSaveOrHold(int ReturnQty, int NonSellableQty, string ReturnItemID, int RouteID)
        //{
        //    Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:UpdateReturnInventoryForSaveOrHold]");
        //    try
        //    {
        //        System.Text.StringBuilder MyQuery = new System.Text.StringBuilder();
        //        MyQuery.Append(" UPDATE BUSDTA.Inventory SET OnHandQuantity = OnHandQuantity + " + ReturnQty + ", HeldQuantity = HeldQuantity +'" + NonSellableQty + "' ");
        //        MyQuery.Append(" WHERE ItemId = " + ReturnItemID + " AND RouteId = " + RouteID + " ");
        //        DbEngine.ExecuteNonQuery(MyQuery.ToString());

        //        ResourceManager.QueueManager.QueueProcess("Inventory", false);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][UpdateReturnInventoryForSaveOrHold][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:UpdateReturnInventoryForSaveOrHold]");
        //}

        // Update Committed Quantity
        public void UpdateCommittedQuantityForReturn(int CommittedQty, string ReturnItemID, int RouteID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:UpdateCommittedQuantityForReturn]");
            try
            {
                System.Text.StringBuilder MyQuery = new System.Text.StringBuilder();
                MyQuery.Append("UPDATE BUSDTA.Inventory SET CommittedQuantity = CommittedQuantity - " + CommittedQty + " WHERE ItemId=" + ReturnItemID + " AND RouteId='" + RouteID + "'");
                DbEngine.ExecuteNonQuery(MyQuery.ToString());
                MyQuery.Clear();
                ResourceManager.QueueManager.QueueProcess("Inventory", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][UpdateCommittedQuantityForReturn][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:UpdateCommittedQuantityForReturn]");
        }

        // Update Held Quantity
        public void UpdateHeldQuantityForReturn(int HeldQty, string ReturnItemID, int RouteID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:UpdateHeldQuantityForReturn]");
            try
            {
                System.Text.StringBuilder MyQuery = new System.Text.StringBuilder();
                MyQuery.Append("UPDATE BUSDTA.Inventory SET HeldQuantity = HeldQuantity + " + HeldQty + " WHERE ItemId=" + ReturnItemID + " AND RouteId='" + RouteID + "'");
                DbEngine.ExecuteNonQuery(MyQuery.ToString());
                MyQuery.Clear();
                ResourceManager.QueueManager.QueueProcess("Inventory", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][UpdateHeldQuantityForReturn][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:UpdateHeldQuantityForReturn]");
        }

        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/
        /* Notification Manager - Common Inventory Function */
        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/

        // Update On Hand Quantity for Notification
        public void UpdateOnHandQuantityForNotification(int OnHandQty, string ItemNumber, int RouteID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:UpdateOnHandQuantityForNotification]");
            try
            {
                System.Text.StringBuilder MyQuery = new System.Text.StringBuilder();
                MyQuery.Append(" UPDATE BUSDTA.Inventory SET OnHandQuantity = OnHandQuantity + " + OnHandQty + " ");
                MyQuery.Append(" WHERE ItemNumber = " + ItemNumber + " AND RouteId = " + RouteID + " ");
                DbEngine.ExecuteNonQuery(MyQuery.ToString());
                MyQuery.Clear();
                ResourceManager.QueueManager.QueueProcess("Inventory", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][UpdateOnHandQuantityForNotification][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:UpdateOnHandQuantityForNotification]");
        }

        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/
        /* Replineshment Add Load Manager - Common Inventory Function */
        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/

        public void UpdateReplnCommitedHeldInventory(string CommittedQty, string HeldQty, string ItemId)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:UpdateReplnCommitedHeldInventory]");
            try
            {
                string query = @"update busdta.Inventory set CommittedQuantity= '{0}' , HeldQuantity='{1}',UpdatedBy =" + Managers.UserManager.UserId + ",UpdatedDatetime = getdate() where ItemId='{2}'";
                query = string.Format(query, CommittedQty, HeldQty, ItemId);
                DbEngine.ExecuteNonQuery(query);
                query = string.Empty;
                ResourceManager.QueueManager.QueueProcess("Inventory", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][UpdateReplnCommitedHeldInventory][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:UpdateReplnCommitedHeldInventory]");
        }

        public int UpdateReplnCommitedInventory(string CommittedQty, string ItemId)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:UpdateReplnCommitedInventory]");
            int ResultQty = 0;
            try
            {
                string query = @"update busdta.Inventory set CommittedQuantity= '{0}'  where ItemId='{1}'";
                query = string.Format(query, CommittedQty, ItemId);
                ResultQty = DbEngine.ExecuteNonQuery(query);
                query = string.Empty;
                ResourceManager.QueueManager.QueueProcess("Inventory", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][UpdateReplnCommitedInventory][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:UpdateReplnCommitedInventory]");
            return ResultQty;
        }

        public int UpdateReplnCommittedQtyReduce(int CommittedQty, string ItemId)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:UpdateReplnCommittedQtyReduce]");
            int ResultQty = 0;
            try
            {
                string query = @"update busdta.Inventory set CommittedQuantity= CommittedQuantity-'{0}',UpdatedBy =" + Managers.UserManager.UserId + ",UpdatedDatetime = GETDATE()  where ItemId='{1}'";
                query = string.Format(query, CommittedQty, ItemId);
                ResultQty = DbEngine.ExecuteNonQuery(query);

                ResourceManager.QueueManager.QueueProcess("Inventory", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][UpdateReplnCommittedQtyReduce][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:UpdateReplnCommittedQtyReduce]");
            return ResultQty;
        }

        public int UpdateReplnOnHandQtyReduce(int OnHandQty, string ItemId)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:UpdateReplnOnHandInventory]");
            int ResultQty = 0;
            try
            {
                string query = @"update busdta.Inventory set onhandquantity= onhandquantity-'{0}',UpdatedBy =" + Managers.UserManager.UserId + ",UpdatedDatetime = GETDATE()  where ItemId='{1}'";
                query = string.Format(query, OnHandQty, ItemId);
                ResultQty = DbEngine.ExecuteNonQuery(query);

                ResourceManager.QueueManager.QueueProcess("Inventory", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][UpdateReplnOnHandInventory][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:UpdateReplnOnHandInventory]");
            return ResultQty;
        }

        public int UpdateReplnOnHandInventory(string OnHandQty, string ItemId)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:UpdateReplnCommitedInventory]");
            int ResultQty = 0;
            try
            {
                string query = @"update busdta.Inventory set OnHandQuantity= '{0}'  where ItemId='{1}'";
                query = string.Format(query, OnHandQty, ItemId);
                ResultQty = DbEngine.ExecuteNonQuery(query);

                ResourceManager.QueueManager.QueueProcess("Inventory", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][UpdateReplnCommitedInventory][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:UpdateReplnCommitedInventory]");
            return ResultQty;
        }

        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/
        /* Customer Return Pick View Model - Common Inventory Function */
        /*------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------*/


        //Unused function commented for performance ---Vignesh D
        // Update Exception - Pick
        //public int UpdateReturnInventoryPick(int ReturnPickQty, string ReturnPickItemID, int RouteID)
        //{
        //    Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:UpdateReturnInventoryForSaveOrHold]");
        //    int TransactionQty = 0;
        //    try
        //    {
        //        System.Text.StringBuilder MyQuery = new System.Text.StringBuilder();
        //        MyQuery.Append(" UPDATE BUSDTA.Inventory SET OnHandQuantity = OnHandQuantity - " + ReturnPickQty + ", CommittedQuantity = CommittedQuantity - '" + ReturnPickQty + "' ");
        //        MyQuery.Append(" WHERE ItemId = " + ReturnPickItemID + " AND RouteId = " + RouteID + " ");
        //        TransactionQty = DbEngine.ExecuteNonQuery(MyQuery.ToString());

        //        ResourceManager.QueueManager.QueueProcess("Inventory", false);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][UpdateReturnInventoryForSaveOrHold][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
        //    }

        //    Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:UpdateReturnInventoryForSaveOrHold]");
        //    return TransactionQty;
        //}

        // Update Exception - Pick with Sign
        public int UpdateReturnInventoryPickWithSign(int ReturnPickQty, char PickSign, string ReturnPickItemID, int RouteID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][Start:UpdateReturnInventoryForSaveOrHold]");
            int TransactionQty = 0;
            try
            {
                System.Text.StringBuilder MyQuery = new System.Text.StringBuilder();
                MyQuery.Append(" UPDATE BUSDTA.Inventory SET CommittedQuantity = CommittedQuantity " + PickSign + " " + ReturnPickQty + ", OnHandQuantity = OnHandQuantity " + PickSign + " " + ReturnPickQty + " ");
                MyQuery.Append(" WHERE ItemId = " + ReturnPickItemID + " AND RouteId = " + RouteID + " ");


                //var invr_query = @"update busdta.Inventory set CommittedQuantity = CommittedQuantity " + PickNumericSign + "  " + PickNumericCurrVal + " WHERE ItemId=" + SelectedReturnItem.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                //TransactionQty = DbEngine.ExecuteNonQuery(invr_query);

                //var inv_upd_onHand_query = @"update busdta.Inventory set OnHandQuantity = OnHandQuantity " + PickNumericSign + "  " + PickNumericCurrVal + " WHERE ItemId=" + SelectedReturnItem.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                //TransactionQty = DbEngine.ExecuteNonQuery(inv_upd_onHand_query);


                TransactionQty = DbEngine.ExecuteNonQuery(MyQuery.ToString());

                ResourceManager.QueueManager.QueueProcess("Inventory", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][InventoryManager][UpdateReturnInventoryForSaveOrHold][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][InventoryManager][End:UpdateReturnInventoryForSaveOrHold]");
            return TransactionQty;
        }


        //Disposable Implementation --Vignesh D
        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }

    public class StatusType
    {
        public int ID { get; set; }
        public string StatusCode { get; set; }
        public string StatusDesc { get; set; }
    }

}
