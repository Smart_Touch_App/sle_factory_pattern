﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
namespace SalesLogicExpress.Application.Managers
{
    public class NetworkManager : ViewModelBase
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.NetworkManager");
        public NetworkManager()
        {
            try
            {
                //MSDN : NetworkAddressChanged :-> The NetworkChange class raises NetworkAddressChanged events when the address of a network interface, 
                //also called a network card or adapter, changes.
                NetworkChange.NetworkAddressChanged += new
                    NetworkAddressChangedEventHandler(AddressChangedCallback);

                // MSDN : NetworkAvailabilityChanged:-> The NetworkChange class raises NetworkAvailabilityChanged events when the availability of the network changes. 
                // The network is available when at least one network interface is marked "up" and is not a tunnel or loopback interface.
                NetworkChange.NetworkAvailabilityChanged += NetworkChange_NetworkAvailabilityChanged;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][NetworkManager][NetworkManager][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
        }

        void NetworkChange_NetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            bool result = e.IsAvailable;
            QueryNetworkAdapters();
        }
        void QueryNetworkAdapters()
        {
            // System.Net.NetworkInformation.
            try
            {
                NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
                InternetStateChangedEventArgs args = new InternetStateChangedEventArgs();
                foreach (NetworkInterface n in adapters)
                {
                    //System.Diagnostics.Debug.WriteLine("   {0} is {1}", n.Name, n.OperationalStatus);
                    if (n.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
                    {
                        args.ConnectivityType = ConnectivityType.Wifi;
                        if (!(n.OperationalStatus == OperationalStatus.Down || n.OperationalStatus == OperationalStatus.LowerLayerDown || n.OperationalStatus == OperationalStatus.NotPresent || n.OperationalStatus == OperationalStatus.Unknown))
                        {
                            System.Diagnostics.Debug.WriteLine("Internet Connected and is WIFI");
                            args.State = InternetState.Connected;
                        }
                        else
                        {
                            System.Diagnostics.Debug.WriteLine("Network not connected");
                            args.State = InternetState.Disconnected;

                        }
                    }
                    if (n.NetworkInterfaceType == NetworkInterfaceType.Wman || n.NetworkInterfaceType == NetworkInterfaceType.Wwanpp || n.NetworkInterfaceType == NetworkInterfaceType.Wwanpp2)
                    {
                        args.ConnectivityType = ConnectivityType.Mobile;
                        if (!(n.OperationalStatus == OperationalStatus.Down || n.OperationalStatus == OperationalStatus.LowerLayerDown || n.OperationalStatus == OperationalStatus.NotPresent || n.OperationalStatus == OperationalStatus.Unknown))
                        {
                            System.Diagnostics.Debug.WriteLine("Internet Connected and is Mobile");
                            args.State = InternetState.Connected;
                        }
                        else
                        {
                            System.Diagnostics.Debug.WriteLine("Network not connected");
                            args.State = InternetState.Disconnected;
                        }
                    }
                }
                args.StateChangedTime = DateTime.Now;
                OnInternetStateChanged(args);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][NetworkManager][QueryNetworkAdapters][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
        }
        void AddressChangedCallback(object sender, EventArgs e)
        {
            //QueryNetworkAdapters();
            try
            {
                InternetStateChangedEventArgs args = new InternetStateChangedEventArgs();
                bool isInterNetConnected = IsInterNetConnected();
                if (isInterNetConnected)
                {
                    //System.Diagnostics.Debug.WriteLine("Internet Connected");
                    args.State = InternetState.Connected;
                }
                else
                {
                    //System.Diagnostics.Debug.WriteLine("Internet disconnected");
                    args.State = InternetState.Disconnected;
                }
                args.StateChangedTime = DateTime.Now;
                OnInternetStateChanged(args);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][NetworkManager][QueryNetworkAdapters][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
        }
        protected virtual void OnInternetStateChanged(InternetStateChangedEventArgs e)
        {
            try
            {
                EventHandler<InternetStateChangedEventArgs> handler = InternetStateChanged;
                if (handler != null)
                {
                    Delegate[] eventHandlers = handler.GetInvocationList();
                    foreach (Delegate currentHandler in eventHandlers)
                    {
                        EventHandler<InternetStateChangedEventArgs> currentSubscriber = (EventHandler<InternetStateChangedEventArgs>)currentHandler;
                        currentSubscriber(this, e);
                    }
                    //handler(this, e);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][NetworkManager][OnInternetStateChanged][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
        }
        public bool IsInterNetConnected()
        {
            // MSDN : GetIsNetworkAvailable - >
            // A network connection is considered to be available if any network interface is marked "up" and is not a loopback or tunnel interface.
            return System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable(); ;
        }
        public event EventHandler<InternetStateChangedEventArgs> InternetStateChanged;

        public enum InternetState
        {
            Connected,
            Disconnected
        }
        public enum ConnectivityType
        {
            Wifi,
            Mobile
        }
        public class InternetStateChangedEventArgs : EventArgs
        {
            public InternetState State { get; set; }
            public ConnectivityType ConnectivityType { get; set; }
            public DateTime StateChangedTime { get; set; }
        }

        //************************************************************************************************
        // Comment: Dispose Implementation for Peformance Improvement
        // Created: Nov 03, 2016
        // Author: TechUnison (Velmani Karnan)
        //*************************************************************************************************

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }

    }

    /*
      void ExampleMethod() {
            NetworkManager network = new NetworkManager();
            network.InternetStateChanged += network_InternetStateChanged;
        }

        void network_InternetStateChanged(object sender, InternetStateChangedEventArgs e)
        {
            if (e.State == InternetState.Connected) { 
                // Do Work
            }
        }
     */


}
