﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;

namespace SalesLogicExpress.Application.Managers
{
    public class SettlementConfirmationManager : IDisposable
    {

        private static readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.SettlementConfirmationManager");
        private static ReasonCodeManager objReasonCodeManager = new ReasonCodeManager();
        private static OrderManager objOrderManger = new OrderManager();

        public static bool AddConfirmationSettlement(string statusCD, double settlementAmount,
               ObservableCollection<CashMaster> cashDetailsCollection, ObservableCollection<CheckDetails> CheckDetailsCollection,
            ObservableCollection<MoneyOrderDetails> MoneyOrderDetailsCollection, double cashV, double checkV, double MoneyOrderV, double totalV,
            double expenses, double overShort, Byte[] sign)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:AddConfirmationSettlement]");

            try
            {
                #region Implementation
                string query = string.Empty;

                int NxtNum = GetSettlementID();
                if (NxtNum > 0)
                {
                    string settlementID = NxtNum.ToString();
                    int routeId = CommonNavInfo.RouteID;
                    int userID = Managers.UserManager.UserId;
                    int originator = Managers.UserManager.UserId;
                    int createdBy = Managers.UserManager.UserId;
                    //Get settlement No from DB
                    string settlementNo = "SO" + settlementID.ToString();
                    string statusID = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='" + statusCD + "')").ToString();
                    //string statusID = DbEngine.ExecuteScalar("SELECT statustypeId FROM busdta.Status_Type where StatusTypeCD='" + statusCD + "'").ToString();
                    //insert into Route Settlement
                    query = "insert into busdta.Route_Settlement " +
                                    " (SettlementID,SettlementNo, RouteId,[Status],UserId,Originator,SettlementAmount,ExceptionAmount,[comment],CreatedBy ,OriginatingRoute,partitioningRoute,CreatedDatetime,SettlementDateTime) " +
                                    " values(" + settlementID + ",'" + settlementNo + "'," + routeId + "," + statusID + "," + userID + "," + originator +
                                     " ," + settlementAmount + "," + overShort + ",''," + createdBy + ",'" + CommonNavInfo.OriginatingRouteID + "','" + CommonNavInfo.PartitioningRouteID + "',getdate(),getdate()) ";
                    int i = DbEngine.ExecuteNonQuery(query);
                    query = string.Empty;
                    if (i > 0)
                    {
                        string insertVVDetails = "insert into busdta.Route_Settlement_Detail(SettlementId,CashAmount,CheckAmount,Expenses,MoneyOrderAmount, " +
                                                " OverShortAmount,TotalVerified,Payments,VerificationNum,UserId,VerSignature,CreatedBy,CreatedDatetime,OriginatingRoute,partitioningRoute,RouteId) " +
                                                " values " +
                                                " (" + settlementID + "," + cashV + "," + checkV + "," + expenses + "," + MoneyOrderV + "," + overShort + "," + totalV + "," +
                                                " " + settlementAmount + ",'1','" + createdBy + "',?,'" + createdBy + "',GETDATE(),'" + CommonNavInfo.OriginatingRouteID + "','" + CommonNavInfo.PartitioningRouteID + "','" + routeId + "')";


                        // used SACommand for inserting blob, inserting blobs needs to done using parameters
                        Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(insertVVDetails);
                        Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();
                        parm.SADbType = Sap.Data.SQLAnywhere.SADbType.LongBinary;
                        command.Parameters.Add(parm);
                        command.Parameters[0].Value = sign;
                        i = new DB_CRUD().InsertUpdateData(command);
                        if (i > 0)
                        {
                            //query = "select SettlementDetailId from busdta.Route_Settlement_Detail where SettlementId='" + settlementID + "'";
                            query = "Call BUSDTA.SP_GetConfirmedSettlements(@SettlementID='" + settlementID + "',@Verificationtag='Settlement')";
                            string settlementDetailID = DbEngine.ExecuteScalar(query).ToString();
                            query = string.Empty;


                            if (cashDetailsCollection.Count > 0)
                            {
                                foreach (var item in cashDetailsCollection)
                                {
                                    string insertCashDetails = "insert into busdta.Cash_Verification_Detail " +
                                        " ( SettlementDetailId ,CashTypeId,Quantity ,Amount,CreatedBy ,CreatedDatetime,RouteId )" +
                                        " values " +
                                        " ('" + settlementDetailID + "', '" + item.CashId + "','" + item.Quantity + "','" + item.Amount + "','" + createdBy + "',getdate(),'" + routeId + "')";
                                    DbEngine.ExecuteNonQuery(insertCashDetails);
                                }

                            }

                            if (CheckDetailsCollection.Count > 0)
                            {
                                foreach (var objCheck in CheckDetailsCollection)
                                {
                                    string insertCheckDetails = "insert into busdta.Check_Verification_Detail(SettlementDetailId,   CheckDetailsId,RouteId,CreatedBy,CreatedDatetime)  " +
                                                         " VALUES  (" + settlementDetailID + ",     '" + objCheck.CheckID + "', '" + routeId + "','" + createdBy + "',getdate())";

                                    i = DbEngine.ExecuteNonQuery(insertCheckDetails);
                                }
                            }

                            if (MoneyOrderDetailsCollection.Count > 0)
                            {
                                foreach (var objMO in MoneyOrderDetailsCollection)
                                {
                                    string insertMoDetails = "insert into busdta.MoneyOrder_Verification_Detail (SettlementDetailId,MoneyOrderId,RouteId,CreatedBy,CreatedDatetime) " +
                                                    " VALUES (" + settlementDetailID + ",'" + objMO.MoneyOrderID + "', '" + routeId + "','" + createdBy + "',getdate())";
                                    i = DbEngine.ExecuteNonQuery(insertMoDetails);
                                }
                            }
                            UpdateSettlementIdForTransaction(settlementID);

                        }
                        return true;
                    }
                    else
                        return false;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][AddConfirmationSettlement][ExceptionStackTrace = " + ex.StackTrace + "]");
                //return false;
            }
            ResourceManager.QueueManager.QueueProcess("EndOfDay", false);
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:AddConfirmationSettlement]");
            return true;
        }

        public static void AddVerificationSettlement(string statusCD, double settlementAmount,
             ObservableCollection<CashMaster> cashDetailsCollection, ObservableCollection<CheckDetails> CheckDetailsCollection,
          ObservableCollection<MoneyOrderDetails> MoneyOrderDetailsCollection, double cashV, double checkV, double MoneyOrderV, double totalV,
            double expenses, double overShort, string settlementID, Byte[] sign)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:AddVerificationSettlement]");
            try
            {
                #region Implementation
                string query = string.Empty;
                int routeId = CommonNavInfo.RouteID;
                //int userID = Managers.UserManager.UserId;
                //int originator = Managers.UserManager.UserId;
                //int verifier = 0;
                int createdBy = Managers.UserManager.UserId;
                //string statusID = DbEngine.ExecuteScalar("SELECT statustypeId FROM busdta.Status_Type where StatusTypeCD='" + statusCD + "'").ToString();
                //string statusID = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='" + statusCD + "')").ToString();
                //Get settlement No from DB
                //insert into Route Settlement
                string insertVVDetails = "insert into busdta.Route_Settlement_Detail(SettlementId,CashAmount,CheckAmount,Expenses,MoneyOrderAmount, " +
                                             " OverShortAmount,TotalVerified,Payments,VerificationNum,UserId,VerSignature,CreatedBy,CreatedDatetime,OriginatingRoute,partitioningRoute,RouteId) " +
                                             " values " +
                                             " (" + settlementID + "," + cashV + "," + checkV + "," + expenses + "," + MoneyOrderV + "," + overShort + "," + totalV + "," +
                                             " " + settlementAmount + ",'2','" + createdBy + "',?,'" + createdBy + "',GETDATE(),'" + CommonNavInfo.OriginatingRouteID + "','" + CommonNavInfo.PartitioningRouteID + "','" + routeId + "')";


                // used SACommand for inserting blob, inserting blobs needs to done using parameters
                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(insertVVDetails);
                Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();
                parm.SADbType = Sap.Data.SQLAnywhere.SADbType.LongBinary;
                command.Parameters.Add(parm);
                command.Parameters[0].Value = sign;
                new DB_CRUD().InsertUpdateData(command);

                //query = "select SettlementDetailId from busdta.Route_Settlement_Detail where SettlementId='" + settlementID + "' and VerificationNum ='2'";
                query = "Call BUSDTA.SP_GetConfirmedSettlements(@SettlementID='" + settlementID + "',@Verificationtag='2')";
                string settlementDetailID = DbEngine.ExecuteScalar(query).ToString();
                query = string.Empty;

                if (!string.IsNullOrEmpty(settlementDetailID))
                {
                    if (cashDetailsCollection.Count > 0)
                    {
                        foreach (var item in cashDetailsCollection)
                        {
                            string insertCashDetails = "insert into busdta.Cash_Verification_Detail " +
                                    " ( SettlementDetailId ,CashTypeId,Quantity ,Amount,CreatedBy ,CreatedDatetime,RouteId )" +
                                    " values " +
                                    " ('" + settlementDetailID + "', '" + item.CashId + "','" + item.Quantity + "','" + item.Amount + "','" + createdBy + "',getdate(),'" + routeId + "')";
                            DbEngine.ExecuteNonQuery(insertCashDetails);
                        }

                    }

                    if (CheckDetailsCollection.Count > 0)
                    {
                        foreach (var objCheck in CheckDetailsCollection)
                        {
                            string insertCheckDetails = "insert into busdta.Check_Verification_Detail(SettlementDetailId,   CheckDetailsId,RouteId,CreatedBy,CreatedDatetime)  " +
                                                 " VALUES  (" + settlementDetailID + ",     '" + objCheck.CheckID + "', '" + routeId + "','" + createdBy + "',getdate())";

                            DbEngine.ExecuteNonQuery(insertCheckDetails);
                        }
                    }

                    if (MoneyOrderDetailsCollection.Count > 0)
                    {
                        foreach (var objMO in MoneyOrderDetailsCollection)
                        {
                            string insertMoDetails = "insert into busdta.MoneyOrder_Verification_Detail (SettlementDetailId,MoneyOrderId,RouteId,CreatedBy,CreatedDatetime) " +
                                            " VALUES (" + settlementDetailID + ",'" + objMO.MoneyOrderID + "', '" + routeId + "','" + createdBy + "',getdate())";
                            DbEngine.ExecuteNonQuery(insertMoDetails);
                        }
                    }
                }
                if (statusCD.ToUpper() == "REJCT")
                {
                    VoidSettlement(settlementID, statusCD, "");
                }

                //UpdateSettlementIdForTransaction(settlementID);
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][AddVerificationSettlement][ExceptionStackTrace = " + ex.StackTrace + "]");

            }

            ResourceManager.QueueManager.QueueProcess("EndOfDay", false);
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:AddVerificationSettlement]");
        }


        public static void GetConfirmedSettlements(string settlementID, out List<CashMaster> cashMstr, out  List<CheckDetails> verifiedCheck,
            out List<MoneyOrderDetails> verifiedMo, out List<VerifiedFunds> verifiedFunds)
        {            
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:GetConfirmedSettlements]");
            
            try
            {
                DataSet ds = new DataSet();

                #region Implementation
                //string querySettDetlId = "select SettlementDetailId from busdta.Route_Settlement_Detail where SettlementId ='" + settlementID + "'";
                string querySettDetlId = string.Empty;
                 querySettDetlId = "Call BUSDTA.SP_GetConfirmedSettlements(@SettlementID='" + settlementID + "',@Verificationtag='Settlement')";
                 string settlementDetialID = string.Empty;
                 settlementDetialID = DbEngine.ExecuteScalar(querySettDetlId);
                querySettDetlId = string.Empty;


                #region Cash

                //string query = "select CashTypeId AS 'CashId', Quantity,Amount,SettlementDetailId as 'SettlementDetailID' " +
                //    " from busdta.Cash_Verification_Detail where SettlementDetailId='" + settlementDetialID + "'";
                string query = string.Empty;
                 query = "Call BUSDTA.SP_GetConfirmedSettlements(@SettlementID='" + settlementDetialID + "',@Verificationtag='Cash')";

                ds = DbEngine.ExecuteDataSet(query);
                query = string.Empty;
                cashMstr = new List<CashMaster>();
                if (ds.HasData())
                {
                    cashMstr = ds.GetEntityList<CashMaster>();
                }

                #endregion

                #region Check
                ds = null;
                //ds = new DataSet();

                //string queryVerifiedCheck = "select  ch.CheckDetailsId as 'CheckID', " +
                //    " ch.SettlementDetailId  as 'SettlementID' from busdta.Check_Verification_Detail ch " +
                //    " where SettlementDetailId='" + settlementDetialID + "'";
                string queryVerifiedCheck = "Call BUSDTA.SP_GetConfirmedSettlements(@SettlementID='" + settlementDetialID + "',@Verificationtag='Check')";
                ds = DbEngine.ExecuteDataSet(queryVerifiedCheck);
                queryVerifiedCheck = string.Empty;
                verifiedCheck = new List<CheckDetails>();
                if (ds.HasData())
                {
                    verifiedCheck = ds.GetEntityList<CheckDetails>();
                }
                ds = null;
                //ds = new DataSet();
                #endregion

                #region MoneyOrder
                //string queryVerifiedMO = "select a.MoneyOrderId as 'MoneyOrderID' ,a.SettlementDetailId as 'SettlementID' " +
                //    " from busdta.MoneyOrder_Verification_Detail a " +
                //    " where SettlementDetailId='" + settlementDetialID + "'";

                string queryVerifiedMO = "Call BUSDTA.SP_GetConfirmedSettlements(@SettlementID='" + settlementDetialID + "',@Verificationtag='MoneyOrder')";
                ds = DbEngine.ExecuteDataSet(queryVerifiedMO);
                queryVerifiedMO = string.Empty;
                verifiedMo = new List<MoneyOrderDetails>();
                if (ds.HasData())
                {
                    verifiedMo = ds.GetEntityList<MoneyOrderDetails>();
                }
                ds = null;
                //ds = new DataSet();
                #endregion

                #region Funds

                //string queryVerifiedFunds = "select a.CashAmount as 'Cash',a.CheckAmount as 'Check',a.Expenses  as 'Expenses'" +
                //                            " ,a.MoneyOrderAmount as 'MoneyOrder',a.OverShortAmount as 'OverShort', " +
                //                            " a.SettlementId as 'SettlementID',a.TotalVerified as 'TotalVerified',a.Payments AS 'Payments'" +
                //                            " from busdta.Route_Settlement_Detail a  " +
                //                            " where SettlementId='" + settlementID + "'";

                string queryVerifiedFunds = "Call BUSDTA.SP_GetConfirmedSettlements(@SettlementID='" + settlementID + "',@Verificationtag='Funds')";
                ds = DbEngine.ExecuteDataSet(queryVerifiedFunds);
                queryVerifiedFunds = string.Empty;
                verifiedFunds = new List<VerifiedFunds>();
                if (ds.HasData())
                {
                    verifiedFunds = ds.GetEntityList<VerifiedFunds>();
                }
                #endregion

                #endregion

                ds.Dispose();
                ds = null;
            }
            catch (Exception ex)
            {
                cashMstr = new List<CashMaster>();
                verifiedCheck = new List<CheckDetails>();
                verifiedMo = new List<MoneyOrderDetails>();
                verifiedFunds = new List<VerifiedFunds>();
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][GetConfirmedSettlements][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:GetConfirmedSettlements]");

        }

        public static List<UsersForVerification> GetUserForVerification
        {
            get
            {
                List<UsersForVerification> vUsers = new List<UsersForVerification>();
                Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:GetUserForVerification]");

                try
                {
                    #region Implementation
                    int userID = Managers.UserManager.UserId;
                    //string query = "SELECT App_user_id as 'UserID', NTUser as 'UserName','***' as 'Password' from busdta.userprofilemaster where App_user_id != '" + userID + "' ";
                    //DataSet ds = DbEngine.ExecuteDataSet(query);
                    DataSet ds = DbEngine.ExecuteDataSet("call busdta.SP_GetSettlementUserForVerification('" + userID + "')");

                    if (ds.HasData())
                    {
                        vUsers = ds.GetEntityList<UsersForVerification>();
                    }

                    ds.Dispose();
                    ds = null;
                    #endregion
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][GetUserForVerification][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:GetUserForVerification]");

                return vUsers;
            }
        }
        public static ObservableCollection<ViewModels.ReasonCode> GetReasonListForVoid(string ReasonCodeType)
        {
            ObservableCollection<ViewModels.ReasonCode> reasonCodeList = new ObservableCollection<ViewModels.ReasonCode>();
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:GetReasonListForVoid]");

            try
            {
                #region Implementation
                //DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='" + ReasonCodeType + "'").Tables[0];
                //SettlementConfirmationManager CustManager = new SettlementConfirmationManager();
                DataTable dt = objReasonCodeManager.SettlementConfirmationManager_GetReasonListForVoid(ReasonCodeType);

                ViewModels.ReasonCode ReasonCode = null;
                foreach (DataRow dr in dt.Rows)
                {
                    ReasonCode = new ViewModels.ReasonCode();
                    ReasonCode.Id = Convert.ToInt32(dr["Id"].ToString());
                    ReasonCode.Code = dr["ReasonCodeDescription"].ToString();
                    reasonCodeList.Add(ReasonCode);
                }

                dt.Dispose();
                dt = null;
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][GetReasonListForVoid][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:GetReasonListForVoid]");

            return reasonCodeList;

        }

        public static ObservableCollection<RejectionReason> GetRejectionReasons
        {
            get
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:GetRejectionReasons]");

                ObservableCollection<RejectionReason> reasonCodeList = new ObservableCollection<RejectionReason>();
                #region Implementation
                try
                {
                    //string query = "SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Reject Settlement'";

                    //DataTable dt = DbEngine.ExecuteDataSet(query).Tables[0];
                    //SettlementConfirmationManager CustManager = new SettlementConfirmationManager();
                    DataTable dt = objReasonCodeManager.SettlementConfirmationManager_GetRejectionReasons();

                    RejectionReason ReasonCode = null;
                    foreach (DataRow dr in dt.Rows)
                    {
                        ReasonCode = new RejectionReason();
                        ReasonCode.ReasonID = (dr["Id"].ToString());
                        ReasonCode.ReasonName = dr["ReasonCodeDescription"].ToString();
                        reasonCodeList.Add(ReasonCode);
                    }

                    dt.Dispose();
                    dt = null;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][GetRejectionReasons][ExceptionStackTrace = " + ex.StackTrace + "]");

                }
                #endregion
                Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:GetRejectionReasons]");

                return reasonCodeList;
            }

        }

        public static List<CashMaster> GetCashMaster
        {
            get
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:GetCashMaster]");

                List<CashMaster> vUsers = new List<CashMaster>();

                try
                {
                    //string query = "select CashId,CashType,CashCode,CashDescription,'0' as 'Quantity' from busdta.Cash_Master";
                    string query = string.Empty;
                     query = "Call BUSDTA.SP_GetCashMasterList()";
                    DataSet ds = DbEngine.ExecuteDataSet(query);
                    query = string.Empty;

                    if (ds.HasData())
                    {
                        vUsers = ds.GetEntityList<CashMaster>();
                    }

                    ds.Dispose();
                    ds = null;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][GetCashMaster][ExceptionStackTrace = " + ex.StackTrace + "]");

                }
                Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:GetCashMaster]");

                return vUsers;
            }
        }

        public static void UpdateSettlementIdForTransaction(string settlementID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:UpdateSettlementIdForTransaction]");
            try
            {

                ObservableCollection<string> obj = ViewModelPayload.PayloadManager.RouteSettlementPayload.ActivityIDList;
                for (int i = 0; i < obj.Count; i++)
                {
                    string updateQuery = "update busdta.M50012  set TDSTTLID= '" + settlementID + "' where TDID='" + obj[i].ToString() + "' AND TRIM(ISNULL(TDSTTLID,''))=''";
                    DbEngine.ExecuteNonQuery(updateQuery);
                }

                obj = null;
                //DbEngine.ExecuteNonQuery("Update Busdta.Order_Header Set Busdta.Order_Header.SettlementID=busdta.M50012.TDSTTLID  " +
                //        " from busdta.M50012 inner join Busdta.Order_Header on busdta.M50012.TDREFHDRID = Busdta.Order_Header.OrderID " +
                //        " where TDSTTLID   = " + settlementID + " and TDTYP in('Order','PickOrder','PickItem','Payment','PickComplete','OrderDelivered','CreditMemo','ReturnOrder')");

                //DbEngine.ExecuteNonQuery("Update Busdta.Order_Detail Set Busdta.Order_DETAIL.SettlementID=busdta.M50012.TDSTTLID  " +
                //       " from busdta.M50012 inner join Busdta.Order_Detail on busdta.M50012.TDREFHDRID = Busdta.Order_Detail.OrderID " +
                //       " where TDSTTLID   = " + settlementID + " and TDTYP in('Order','PickOrder','PickItem','Payment','PickComplete','OrderDelivered','CreditMemo','ReturnOrder')");
                //SettlementConfirmationManager objSettlement = new SettlementConfirmationManager();
                objOrderManger.Settlement_UpdateSettlementIdForTransaction(settlementID);

                ViewModelPayload.PayloadManager.RouteSettlementPayload.ActivityIDList.Clear();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][UpdateSettlementIdForTransaction][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:UpdateSettlementIdForTransaction]");


        }

        public static void VoidSettlement(string settlementID, string statusCD, string voidReason)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:VoidSettlement]");

            try
            {
                string updateQuery = "update busdta.M50012  set TDSTTLID= '' where TDSTTLID='" + settlementID + "'";
                DbEngine.ExecuteNonQuery(updateQuery);

                //string querySettDetlId = "select SettlementDetailId from busdta.Route_Settlement_Detail where SettlementId ='" + settlementID + "'";
                //string settlementDetialID = DbEngine.ExecuteScalar(querySettDetlId);

                //string queryDelete = "delete from busdta.Route_Settlement_Detail where SettlementId='" + settlementID + "'";
                //DbEngine.ExecuteNonQuery(queryDelete);
                //string statusID = DbEngine.ExecuteScalar("SELECT statustypeId FROM busdta.Status_Type where StatusTypeCD='" + statusCD + "'").ToString();
                string statusID = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='" + statusCD + "')").ToString();
                if (statusCD.ToLower() == "void")
                {
                    updateQuery = "Update busdta.Route_Settlement set [Status]='" + statusID + "' , [comment]='" + voidReason + "', SettlementDateTime=GETDATE()  where SettlementId='" + settlementID + "'";

                }
                else
                {
                    updateQuery = "Update busdta.Route_Settlement set [Status]='" + statusID + "' , SettlementDateTime=GETDATE()  where SettlementId='" + settlementID + "'";

                }
                DbEngine.ExecuteNonQuery(updateQuery);

                updateQuery = string.Empty;

                //queryDelete = "delete from busdta.MoneyOrder_Verification_Detail where SettlementDetailId='" + settlementDetialID + "'";
                //DbEngine.ExecuteNonQuery(queryDelete);

                //queryDelete = "delete from busdta.Cash_Verification_Detail where SettlementDetailId='" + settlementDetialID + "'";
                //DbEngine.ExecuteNonQuery(queryDelete);

                //queryDelete = "delete from busdta.Check_Verification_Detail where SettlementDetailId='" + settlementDetialID + "'";
                //DbEngine.ExecuteNonQuery(queryDelete);

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][VoidSettlement][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:VoidSettlement]");


        }


        public static void UpdateSettlementVerification(string settlementID, string reason, string verifier, string statusCD)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:UpdateSettlementVerification]");

            try
            {
                //string statusID = DbEngine.ExecuteScalar("SELECT statustypeId FROM busdta.Status_Type where StatusTypeCD='" + statusCD + "'").ToString();
                string statusID = string.Empty;
                 statusID = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='" + statusCD + "')").ToString();

                string query = "update busdta.Route_Settlement  set [Status]='" + statusID + "', [Comment]='" + reason + "', Verifier ='" + verifier + "' , SettlementDateTime=GETDATE()  where SettlementID='" + settlementID + "'";
                DbEngine.ExecuteNonQuery(query);
                statusID = string.Empty;
                query = string.Empty;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][UpdateSettlementVerification][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:UpdateSettlementVerification]");

        }

        public static bool IsPendingVerification()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:IsPendingVerification]");

            bool flag = false;
            try
            {
                //string query = " select count ([status]) from busdta.Route_Settlement where [status]=(SELECT StatusTypeId FROM BUSDTA.Status_Type WHERE StatusTypeCD='NEW')";

                //String handling for Memory Optimization  --Vignesh D
                string query = string.Empty;
                 query = "CALL BUSDTA.SP_GetStatuscountForSettlement()";

                int i = Convert.ToInt16(DbEngine.ExecuteScalar(query));
                query = string.Empty;
                if (i > 0)
                {
                    flag = true;
                }
            }
            catch (Exception ex)
            {

                flag = false;
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][IsPendingVerification][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:IsPendingVerification]");

            return flag;
        }
        //Sathish Removed Unwanted static variables and static class
        /// <summary>
        /// Checks if the settlement is pending for verification 
        /// Unused function
        /// </summary>
        /// <returns></returns>
        //public static bool IsSettlementPendingVerification(string settlementId)
        //{
        //    Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:IsSettlementPendingVerification]");

        //    bool flag = false;
        //    try
        //    {
        //        string query = " select count ([status]) from busdta.Route_Settlement where [status]=(SELECT StatusTypeId FROM BUSDTA.Status_Type WHERE StatusTypeCD='NEW') AND SettlementID=" + settlementId;
        //        int i = Convert.ToInt16(DbEngine.ExecuteScalar(query));
        //        if (i > 0)
        //        {
        //            flag = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        flag = false;
        //        Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][IsSettlementPendingVerification][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:IsSettlementPendingVerification]");

        //    return flag;
        //}
        public static bool IsNonServicedStopPresent(string UserRoute)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:IsNonServicedStopPresent]");

            bool flag = false;
            try
            {
                string baseDate = System.Configuration.ConfigurationManager.AppSettings["BaseDate"].ToString().Replace("-", "");

                // string query1 = "select count(*) from busdta.M56M0004";
                string query1 = string.Empty;
                 query1 = "CALL BUSDTA.SP_GetNonServicedStopPresent()";
                int custCount = Convert.ToInt32(DbEngine.ExecuteScalar(query1));
                query1 = string.Empty;
                if (custCount == 0)
                {
                    flag = true; return flag;
                }
                //string date = DateTime.Now.ToString("yyyy-MM-dd");


                //                string query = @"select COUNT(*) from busdta.M56M0004  where (RPCACT=0 or RPPACT!=0) AND RPSTDT >=  
                //                                (select  CONVERT(VARCHAR(8), isNull(  max( settlementdatetime),'" + baseDate + @"'), 112) from  busdta.Route_Settlement)  
                //                                and RPSTDT < getdate() and RPVTTP !='prospect' and TRIM(RPSTTP) in ('Rescheduled','Planned','Unplanned')";
                string query = string.Empty;
                 query = "CALL BUSDTA.SP_GetNonServicedStopPresent(@BaseDate='" + baseDate + "',@UserRoute='" + UserRoute + "')";
                int i = Convert.ToInt32(DbEngine.ExecuteScalar(query));
                query = string.Empty;
                if (i > 0)
                {
                    flag = true;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][IsNonServicedStopPresent][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:IsNonServicedStopPresent]");

            return flag;
        }

        public static List<RouteSettlementPendingList> GetRouteSettlementPendingList(string UserRoute)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:GetRouteSettlementPendingList]");

            List<RouteSettlementPendingList> lst = new List<RouteSettlementPendingList>();

            try
            {
                //string query = "select a.RPAN8 as CustomerNo, b.abalph AS CustomerName, DATEFORMAT(a.RPSTDT,'MM/DD/YYYY') as StopDate from busdta.M56M0004 a, busdta.f0101 b  where (a.RPCACT=0 or a.RPPACT!=0) AND a.RPSTDT >= (select  CONVERT(VARCHAR(8), isNull(  max( settlementdatetime),'20160228'), 112) from busdta.Route_Settlement) and a.RPSTDT < getdate() and a.RPVTTP !='prospect' and TRIM(a.RPSTTP) in ('Rescheduled','Planned','Unplanned') and a.RPAN8=b.aban8 order by a.RPSTDT desc";
                string query = string.Empty;
                 query = "CALL BUSDTA.SP_GetRouteSettlementPendingList(@UserRoute='" + UserRoute + "')";
                DataSet ds = DbEngine.ExecuteDataSet(query);
                query = string.Empty;

                if (ds.HasData())
                {
                    lst = ds.GetEntityList<RouteSettlementPendingList>();
                }

                ds.Dispose();
                ds = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][GetRouteSettlementPendingList][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:GetRouteSettlementPendingList]");

            return lst;
        }

        public static string InsertCheckDetails(CheckEntryDetails objChk)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:InsertCheckDetails]");

            string checkID = string.Empty;
            try
            {
                string query = string.Empty;
                 query = "insert into BUSDTA.CheckDetails (RouteId,CheckNumber,checkdetailsnumber,CustomerId,CheckAmount, " +
                            " CheckDate,CreatedBy,CreatedDatetime)" +
                            " values " +
                            " ( '" + objChk.RoutId + "','" + objChk.CheckNo + "','" + objChk.CheckNo + "','" + objChk.CustomerId + "','" + objChk.CheckAmount + "','" + objChk.CheckDate + "','" + objChk.CreatedBy + "',getdate())";
                DbEngine.ExecuteNonQuery(query);
                query = string.Empty;

                ResourceManager.QueueManager.QueueProcess("ARPayments", false);
                //string querycheckID = "select CheckDetailsId from  BUSDTA.CheckDetails where CheckNumber='" + objChk.CheckNo.Trim() + "'";
                string querycheckID = string.Empty;
                 querycheckID = "Call BUSDTA.SP_GetCheckDetailsId('" + objChk.CheckNo.Trim() + "')";
                checkID = DbEngine.ExecuteScalar(querycheckID);
                querycheckID = string.Empty;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][InsertCheckDetails][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:InsertCheckDetails]");

            return checkID;
        }

        static int GetSettlementID()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:GetSettlementID]");
            int returnValue = 0;
            try
            {
                Managers.NumberManager numberManager = new NumberManager();
                string value = numberManager.GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.Settlement);
                returnValue = Convert.ToInt32(value);
                value = string.Empty;

                numberManager.Dispose();
                numberManager = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][GetSettlementID][ExceptionStackTrace = " + ex.StackTrace + "]");
                returnValue = 0;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:GetSettlementID]");
            return returnValue;
        }

        public static string GetNewSettlementID
        {
            get
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:GetNewSettlementID]");

                string settlementID = string.Empty;
                try
                {
                    //string statusID = DbEngine.ExecuteScalar("SELECT statustypeId FROM busdta.Status_Type where StatusTypeCD='new'").ToString();
                    string statusID = string.Empty;
                     statusID = DbEngine.ExecuteScalar("Call BUSDTA.SP_GetStatutsTypeIDByStatusTypeCD(@StatusTypeCD='new')").ToString();
                    //select SettlementID from busdta.Route_Settlement  where [Status]=11
                    //settlementID = DbEngine.ExecuteScalar("select SettlementID from busdta.Route_Settlement  where [Status]='" + statusID + "'").ToString();
                    settlementID = DbEngine.ExecuteScalar("CALL BUSDTA.SP_GetNewSettlementID(@StatusID='" + statusID + "')");
                    statusID = string.Empty;


                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][GetNewSettlementID][ExceptionStackTrace = " + ex.StackTrace + "]");

                }
                Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:GetNewSettlementID]");

                return settlementID;
            }
        }

        public static void UpdateReturnOrderRelatedRecordStatus(string settlementId)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:UpdateReturnOrderRelatedRecordStatus]");

                //Get all record bsed TDPNTID=0 AND settlementid and tdrout=fbm105 and tdtyp='ReturnOrder'
                //string queryFormat = "SELECT ISNULL(TDREFHDRID,0) AS TDREFHDRID FROM BUSDTA.M50012 A INNER JOIN  BUSDTA.Route_Master B ON A.TDROUT=B.RouteName WHERE A.TDPNTID=0 AND A.TDSTTLID={0} AND B.RouteMasterID={1} AND A.TDTYP='ReturnOrder'";
                string queryFormat = " CALL BUSDTA.SP_GetReturnOrderRecordStatus(@RouteMasterID={1},@SettlementId='{0}')";
                string queryForReturnOrder = string.Format(queryFormat, settlementId, CommonNavInfo.RouteID.ToString());
                queryFormat = string.Empty;
                DataSet result = DbEngine.ExecuteDataSet(queryForReturnOrder);
                queryForReturnOrder = string.Empty;
                if (result.HasData())
                {
                    List<string> OrderReturnIdList = result.Tables[0].AsEnumerable().Select(dataRow => Convert.ToString(dataRow["TDREFHDRID"])).ToList();

                    if (OrderReturnIdList.Count == 0)
                        return;

                    string strOrderReturn = OrderReturnIdList.Aggregate((oldValue, newValue) => oldValue + "," + newValue);

                    if (!string.IsNullOrEmpty(strOrderReturn))
                    {
                        //Create In query and update the Invoice Status 
                        ARPaymentManager objARpayment = new ARPaymentManager("", "");
                        objARpayment.UpdateReceiptStatus(strOrderReturn, "SETTLED");
                    }

                    OrderReturnIdList = null;
                }

                result.Dispose();
                result = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][UpdateReturnOrderRelatedRecordStatus][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:UpdateReturnOrderRelatedRecordStatus]");


        }

        public static void UpdateSettlmentStatusAfterVerification(string settlementID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:UpdateSettlmentStatusAfterVerification]");
            try
            {
                // Settlement Void Item Status Skip - Related Issue Fix
                //int UpdateTrans = 0;
                //UpdateTrans = DbEngine.ExecuteNonQuery("Update Busdta.Order_Header Set Busdta.Order_Header.UpdatedBy =" + UserManager.UserId + ",Busdta.Order_Header.UpdatedDatetime = getdate(),Busdta.Order_Header.OrderStateID=(SELECT DISTINCT  StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='STTLD')  " +
                //" from busdta.M50012 inner join Busdta.Order_Header on busdta.M50012.TDREFHDRID = Busdta.Order_Header.OrderID and Busdta.Order_Header.OrderStateID NOT IN(SELECT DISTINCT TTID FROM BUSDTA.M5001 WHERE TTKEY IN ('VoidAtCashCollection ','VoidAtOrderEntry','VoidAtDeliverCustomer         ')) " +
                //" where TDSTTLID   = " + settlementID + " and TDTYP in('Order','PickOrder','PickItem','Payment','PickComplete','OrderDelivered','CreditMemo','ReturnOrder')");
                //SettlementConfirmationManager objSettlement = new SettlementConfirmationManager();
                objOrderManger.Settlement_UpdateSettlmentStatusAfterVerification(settlementID);                
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][UpdateSettlmentStatusAfterVerification][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:UpdateSettlmentStatusAfterVerification]");
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();

                if (objReasonCodeManager != null)
                    objReasonCodeManager.Dispose();
                if (objOrderManger != null)
                    objOrderManger.Dispose();
            }
            disposed = true;
        }
    }
}
