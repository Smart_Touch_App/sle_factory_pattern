﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Domain;
using log4net;

namespace SalesLogicExpress.Application.Managers
{
    public class ExpensesManager : IDisposable
    {
        private readonly ILog logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.ExpensesManager");
        private readonly ReasonCodeManager objReasonCodeManger = new ReasonCodeManager();

        private readonly System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        //Disposable Implementation --Vignesh D
        private bool disposed = false;

        public ObservableCollection<Expenses> GetExpensesList()
        {
            var expensesList = new ObservableCollection<Expenses>();
            this.logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][Start:GetExpensesList]");
            try
            {
                string query = string.Empty;
                //string query = "select ED.ExpenseId, ED.RouteId, ED.CategoryId, ED.ExpensesExplanation, ED.ExpenseAmount, ED.StatusId, ED.ExpensesDatetime, ED.VoidReasonId, ED.TransactionId, ED.RouteSettlementId, ";
                //query = query + " ED.UpdatedDatetime, ST.StatusTypeCD as StatusCode, ST.StatusTypeDESC as Status, CT.CategoryTypeDESC as Category, CT.CategoryTypeCD as CategoryCode from BUSDTA.ExpenseDetails ED ";
                //query = query + " join BUSDTA.Status_Type ST on ST.StatusTypeID = ED.StatusId ";
                //query = query + " join BUSDTA.Category_Type CT on ED.CategoryId = CT.CategoryTypeID where ED.RouteId = '" + CommonNavInfo.RouteID + "'";
                //query = query + " ORDER BY ED.UpdatedDatetime DESC";
                //DataSet result = DbEngine.ExecuteDataSet(query);

                DataSet result = DbEngine.ExecuteDataSet(string.Format("call busdta.SP_GetSettlementExpensesList ('{0}')", CommonNavInfo.RouteID));
                if (result.HasData())
                {
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        DataRow row = result.Tables[0].Rows[i];
                        var expense = new Expenses();

                        expense.ExpenseID = row["ExpenseId"].ToString().Trim();
                        expense.RouteID = row["RouteId"].ToString().Trim();
                        expense.CategotyID = row["CategoryId"].ToString().Trim();
                        expense.CategoryCode = string.IsNullOrEmpty(row["CategoryCode"].ToString()) ? string.Empty : row["CategoryCode"].ToString().Trim();
                        if (expense.CategoryCode.ToLower().ToUpper() == "MOCHR")
                        {
                            expense.IsMoneyOrderCharge = true;
                        }
                        expense.ExpenseCategory = string.IsNullOrEmpty(row["Category"].ToString()) ? string.Empty : row["Category"].ToString().Trim();
                        expense.StatusID = row["StatusId"].ToString().Trim();
                        expense.StatusCode = row["StatusCode"].ToString().Trim();
                        expense.Status = row["Status"].ToString().Trim().ToLower().ToUpper();
                        expense.VoidReasonID = string.IsNullOrEmpty(row["VoidReasonId"].ToString()) ? string.Empty : row["VoidReasonId"].ToString().Trim();
                        expense.ExpenseAmount = (Convert.ToDecimal(row["ExpenseAmount"])).ToString("F2").Trim();
                        expense.ExpenseDate = Convert.ToDateTime(row["UpdatedDatetime"]);
                        expense.Explanation = row["ExpensesExplanation"].ToString().Trim();
                        expense.TransactionId = string.IsNullOrEmpty(row["TransactionId"].ToString()) ? string.Empty : row["TransactionId"].ToString();
                        expense.RouteSettlementId = string.IsNullOrEmpty(row["RouteSettlementId"].ToString()) ? string.Empty : row["RouteSettlementId"].ToString();

                        if (!string.IsNullOrEmpty(expense.TransactionId))
                        {
                            // to check whether transaction is MO or other expense.
                            //query = "select count(isnull(MoneyOrderId,0)) from BUSDTA.MoneyOrderDetails where MoneyOrderId = '" + expense.TransactionId + "'";
                            query = string.Format("Call BUSDTA.SP_GetMoneyOrderDetails({0})", expense.TransactionId);
                            expense.IsMoneyOrderCharge = Convert.ToInt32(DbEngine.ExecuteScalar(query)) > 0 ? true : false;
                        }
                        expensesList.Add(expense);

                        expense = null;
                    }
                }
            }
            catch (Exception ex)
            {
                this.logger.Error(string.Format("[SalesLogicExpress.Application.Managers][ExpensesManager][GetExpensesList][ExceptionStackTrace = {0}][Exception Message={1}]", ex.StackTrace, ex.Message));
            }
            this.logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][End:GetExpensesList]");
            return expensesList;
        }

        public Expenses AddExpenses(Expenses expense)
        {
            this.logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][Start:AddExpenses]");           
            try
            {
                string query = "insert into BUSDTA.ExpenseDetails(ExpenseId, RouteId, CategoryId, ExpensesExplanation, ExpenseAmount, StatusId, ExpensesDatetime, VoidReasonId, RouteSettlementId, TransactionId, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)";
                query = string.Format("{0} VALUES((select isnull(max(ExpenseId),0)+1 from BUSDTA.ExpenseDetails), '{1}', '{2}', '{3}', ", query, CommonNavInfo.RouteID, string.IsNullOrEmpty(expense.CategotyID) ? "null" : expense.CategotyID, string.IsNullOrEmpty(expense.Explanation) ? "null" : expense.Explanation);
                query = string.Format("{0} '{1}', (select StatusTypeID from busdta.Status_Type where StatusTypeCD = 'USTLD'), ", query, expense.ExpenseAmount);
                query = string.Format("{0} getdate(), {1}, null, {2}, ", query, string.IsNullOrEmpty(expense.VoidReasonID) ? "null" : expense.VoidReasonID, string.IsNullOrEmpty(expense.TransactionId) ? "null" : expense.TransactionId);
                query = string.Format("{0}'{1}', getdate(), '{2}', getdate())", query, UserManager.UserId, UserManager.UserId);
                int result = DbEngine.ExecuteNonQuery(query);
                if (result == 1)
                {
                    //query = "select top 1 StatusId, ExpenseId from BUSDTA.ExpenseDetails order by UpdatedDatetime DESC";
                    query = "Call BUSDTA.SP_GetTopExpenses()";
                    DataSet obj = DbEngine.ExecuteDataSet(query);
                    if (obj.HasData())
                    {
                        DataRow dr = obj.Tables[0].Rows[0];
                        expense.StatusID = dr["StatusId"].ToString();
                        expense.ExpenseID = dr["ExpenseId"].ToString();
                    }

                    obj.Dispose();
                    obj = null;
                }
                ResourceManager.QueueManager.QueueProcess("EndOfDay", false);
            }
            catch (Exception ex)
            {
                this.logger.Error(string.Format("[SalesLogicExpress.Application.Managers][ExpensesManager][AddExpenses][ExceptionStackTrace = {0}][Exception Message={1}]", ex.StackTrace, ex.Message));
            }
            this.logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][End:AddExpenses]");
            return expense;
        }

        public int UpdateExpenses(Expenses expenses, bool flag)
        {
            this.logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][Start:UpdateExpenses]");
            int statusID = -1;
            try
            {
                string query = string.Empty;
                if (flag)
                {
                    query = string.Format("update BUSDTA.ExpenseDetails set CategoryId = '{0}', ExpensesExplanation = '{1}', ExpenseAmount='{2}', ", expenses.CategotyID, expenses.Explanation, expenses.ExpenseAmount);
                    query = string.Format("{0} UpdatedDatetime = getdate() where RouteID = '{1}' and ExpenseId = '{2}'", query, expenses.RouteID, expenses.ExpenseID);
                }
                else
                {
                    query = string.Format("update BUSDTA.ExpenseDetails set StatusId = (select StatusTypeID from busdta.Status_Type where StatusTypeCD = 'VOID'), VoidReasonId = '{0}', ", expenses.VoidReasonID);
                    query = string.Format("{0} UpdatedDatetime = getdate() where RouteID = '{1}' and ExpenseId = '{2}'", query, expenses.RouteID, expenses.ExpenseID);
                }

                int result = DbEngine.ExecuteNonQuery(query);
                if (result == 1)
                {
                    //query = "select StatusId from BUSDTA.ExpenseDetails where RouteID = '" + expenses.RouteID + "' and ExpenseId = '" + expenses.ExpenseID + "'";
                    query = string.Format("Call BUSDTA.SP_GetExpenseDetails('{0}','{1}')", expenses.RouteID, expenses.ExpenseID);
                    statusID = Convert.ToInt32(DbEngine.ExecuteScalar(query));
                }
                ResourceManager.QueueManager.QueueProcess("EndOfDay", false);
            }
            catch (Exception ex)
            {
                this.logger.Error(string.Format("[SalesLogicExpress.Application.Managers][ExpensesManager][UpdateExpenses][ExceptionStackTrace = {0}][Exception Message={1}]", ex.StackTrace, ex.Message));                
            }
            this.logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][End:UpdateExpenses]");
            return statusID;
        }

        public ObservableCollection<ReasonCode> GetVoidReasonCode()
        {
            var reasonCodeList = new ObservableCollection<ReasonCode>();
            this.logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:GetVoidReasonCode]");
            try
            {
                //DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Void Expenses'").Tables[0];
                DataTable dt = this.objReasonCodeManger.ExpensesManager_GetVoidReasonCode();
                //var reasonCode = new ReasonCode();
                foreach (DataRow dr in dt.Rows)
                {
                    var reasonCode = new ReasonCode();
                    reasonCode.Id = Convert.ToInt32(dr["Id"].ToString().Trim());
                    reasonCode.Code = dr["ReasonCodeDescription"].ToString().Trim();
                    reasonCodeList.Add(reasonCode);
                    reasonCode = null;
                }

                dt.Dispose();
                dt = null;
            }
            catch (Exception ex)
            {
                this.logger.Error(string.Format("[SalesLogicExpress.Application.Managers][ExpensesManager][GetVoidReasonCode {0}]", ex.StackTrace));
            }
            this.logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][End:GetVoidReasonCode]");
            return reasonCodeList;
        }

        public ObservableCollection<ExpenseCategory> GetCategoryForExpenses()
        {
            this.logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:GetCategoryForExpenses]");
            var categoryList = new ObservableCollection<ExpenseCategory>();
            try
            {
                // DataTable dt = DbEngine.ExecuteDataSet("SELECT CategoryTypeID Id, CategoryTypeCD Code,rtrim(ltrim(ISNULL(CategoryTypeDESC,''))) CategoryTypeDESC FROM BUSDTA.Category_Type where CategoryCodeType='Expenses'").Tables[0];
                DataTable dt = DbEngine.ExecuteDataSet("Call BUSDTA.SP_GetCategoryForExpenses()").Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    var category = new ExpenseCategory();
                    category.CategoryID = Convert.ToInt32(dr["Id"].ToString().Trim());
                    category.Code = dr["Code"].ToString().Trim();
                    category.Description = dr["CategoryTypeDESC"].ToString().Trim();
                    categoryList.Add(category);

                    category = null;
                }

                dt.Dispose();
                dt = null;
            }
            catch (Exception ex)
            {
                this.logger.Error(string.Format("[SalesLogicExpress.Application.Managers][ExpensesManager][GetCategoryForExpenses {0}]", ex.StackTrace));
            }
            this.logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][End:GetCategoryForExpenses]");
            return categoryList;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            if (disposing)
            {
                if (this.objReasonCodeManger != null)
                {
                    this.objReasonCodeManger.Dispose();
                }
                this.handle.Dispose();
            }
            this.disposed = true;
        }
    }
}
