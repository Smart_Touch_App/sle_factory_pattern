﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Domain;
using VertexWrapper;
using System.Configuration;
using SalesLogicExpress.Domain.Vertex;
using log4net;
using System.Runtime.Serialization.Json;
using System.IO;
using SalesLogicExpress.Application.Helpers;

namespace SalesLogicExpress.Application.Managers
{
    class TaxManager : IDisposable
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.TaxManager");
        public TaxManager()
        {
            try
            {
                log.Info("[SalesLogicExpress.Application.Managers][TaxManager][Start:TaxManagerConstructor]");
                #region env variables code
                string vertexPath = ConfigurationManager.AppSettings["VERTEX_PATH"].ToString();

                log.Info("Vertex_PATH =" + vertexPath);
                string getUserPath, GetProcessPath;
                getUserPath = Environment.GetEnvironmentVariable("PATH", System.EnvironmentVariableTarget.User);
                GetProcessPath = Environment.GetEnvironmentVariable("PATH", System.EnvironmentVariableTarget.Process);
                string SetUserPath, SetProcessPath;

                if (string.IsNullOrEmpty(getUserPath))
                {

                    Environment.SetEnvironmentVariable("PATH", vertexPath, EnvironmentVariableTarget.User);
                }
                else
                {
                    getUserPath = getUserPath.ToLower();
                    if (!getUserPath.Contains(vertexPath.ToLower()))
                    {
                        SetUserPath = getUserPath + ";" + vertexPath;
                        Environment.SetEnvironmentVariable("PATH", SetUserPath, EnvironmentVariableTarget.User);
                    }
                }

                if (string.IsNullOrEmpty(GetProcessPath))
                {
                    Environment.SetEnvironmentVariable("PATH", vertexPath, EnvironmentVariableTarget.Process);
                }
                else
                {
                    GetProcessPath = GetProcessPath.ToLower();

                    if (!GetProcessPath.Contains(vertexPath.ToLower()))
                    {
                        SetProcessPath = GetProcessPath + ";" + vertexPath;
                        Environment.SetEnvironmentVariable("PATH", SetProcessPath, EnvironmentVariableTarget.Process);
                    }
                }

                log.Info("After Setting Path");
                log.Info("getUserPath =" + getUserPath);
                log.Info("GetProcessPath =" + GetProcessPath);
                #endregion
                log.Info("[SalesLogicExpress.Application.Managers][TaxManager][End:TaxManagerConstructor]");


            }
            catch (Exception e)
            {
                log.Error("[SalesLogicExpress.Application.Managers][TaxManager][TaxManagerConstructor][ExceptionStackTrace = " + e.StackTrace + "]\n\n " +
                    "[InnerException = " + e.InnerException + "]\n\n" +
                    "[Message = " + e.Message + "]\n\n" +
                    "[Data = " + e.Data + "]");
            }

        }
        public bool CalculateTax(ref InvoiceObject objOrderInvoice)
        {

            //Calculates tax for invoice
            try
            {
                log.Info("[SalesLogicExpress.Application.Managers][TaxManager][Start:CalculateTax]");
                VertexApi vertexApiobj = new VertexApi();

                int ItemNumberCount = objOrderInvoice.InvoiceItemList.Count; ;
                string CompCode = String.Empty;
                string Customer = String.Empty;
                string CustomerClassCode = String.Empty;
                double GeoCode = 0;
                string Datasource = String.Empty;
                string InvoiceNumber = String.Empty;
                string ProductSetCode = String.Empty;
                string[] ItemNumber = new string[ItemNumberCount];
                string[] ProductCode = new string[ItemNumberCount];
                string[] ComponentCode = new string[ItemNumberCount];
                int[] ProductQty = new int[ItemNumberCount];
                double[] ExtendedAmount = new double[ItemNumberCount];
                double InvoiceTax = 0;
                double[] ItemTax = new double[ItemNumberCount];
                double GrossAmt = 0;
                bool ReturnStatus;
                bool bStoreTaxLogs;

                CompCode = ConfigurationManager.AppSettings["CompanyCode"].ToString();
                bStoreTaxLogs = Convert.ToBoolean(ConfigurationManager.AppSettings["StoreTaxLogs"].ToString());
                Customer = objOrderInvoice.Customer;
                GeoCode = objOrderInvoice.GeoCode;
                CustomerClassCode = objOrderInvoice.CustomerClassCode;
                InvoiceNumber = objOrderInvoice.InvoiceNumber;
                ProductSetCode = objOrderInvoice.ProductSetCode;
                Datasource = ConfigurationManager.AppSettings["VERTEX_DataSource"].ToString();
                for (int index = 0; index < ItemNumberCount; index++)
                {
                    ItemNumber[index] = objOrderInvoice.InvoiceItemList[index].ItemNumber;
                    ProductCode[index] = objOrderInvoice.InvoiceItemList[index].ProductCode;
                    ComponentCode[index] = "";
                    ProductQty[index] = objOrderInvoice.InvoiceItemList[index].ProductQuantity;
                    ExtendedAmount[index] = Convert.ToDouble(objOrderInvoice.InvoiceItemList[index].ExtendedAmount);

                }
                //Api calls to vertex library
                unsafe
                {
                    //************************************************************************************************
                    // Comment: Commented to enable store logs for tax calculation.
                    // created: jan 13, 2016
                    // Author: Vivensas (Rajesh,Yuvaraj)
                    // Revisions: 
                    //*************************************************************************************************
                    if (bStoreTaxLogs == true) AddTaxLogsInFile(Datasource.ToString(), "Before-Initialize");
                    vertexApiobj.Initialize(&ReturnStatus, Datasource);
                    if (bStoreTaxLogs == true)  AddTaxLogsInFile(Datasource.ToString(), "After-Initialize");
                    vertexApiobj.SetInvoice(CompCode,
                                      Customer,
                                      CustomerClassCode,
                                      GeoCode,
                                      InvoiceNumber,
                                      ProductSetCode,
                                      ItemNumberCount,
                                      ItemNumber,
                                      ProductCode,
                                      ComponentCode,
                                      ProductQty,
                                      ExtendedAmount,
                                      &ReturnStatus);

                    string sItemNumber = "";
                    foreach (var items in ItemNumber)
                    {
                        if (!string.IsNullOrEmpty(sItemNumber))
                        { sItemNumber = sItemNumber + "," + items.ToString(); }
                        else
                        { sItemNumber = items.ToString(); }
                    }

                    string sProductCode = "";
                    foreach (var items in ProductCode)
                    {
                        if (!string.IsNullOrEmpty(sProductCode))
                        { sProductCode = sProductCode + "," + items.ToString(); }
                        else
                        { sProductCode = items.ToString(); }
                    }

                    string sComponentCode = "";
                    foreach (var items in ComponentCode)
                    {
                        if (!string.IsNullOrEmpty(sComponentCode))
                        { sComponentCode = sComponentCode + "," + items.ToString(); }
                        else
                        { sComponentCode = items.ToString(); }
                    }

                    string sProductQty = "";
                    foreach (var items in ProductQty)
                    {
                        if (!string.IsNullOrEmpty(sProductQty))
                        { sProductQty = sProductQty + "," + items.ToString(); }
                        else
                        { sProductQty = items.ToString(); }
                    }

                    string sExtendedAmount = "";
                    foreach (var items in ExtendedAmount)
                    {
                        if (!string.IsNullOrEmpty(sExtendedAmount))
                        { sExtendedAmount = sExtendedAmount + "," + items.ToString(); }
                        else
                        { sExtendedAmount = items.ToString(); }
                        
                    }

                    if (bStoreTaxLogs == true) AddTaxLogsInFile(" vertexApiobj.SetInvoice(CompCode = "+ CompCode + "," +
                    " Customer = "+ Customer+"," +
                    " CustomerClassCode= "+ CustomerClassCode +"," +
                                      "GeoCode= "+  GeoCode +"," + 
                                      "InvoiceNumber = "+InvoiceNumber+"," +
                                      "ProductSetCode = "+ ProductSetCode +"," +
                                      "ItemNumberCount = "+ ItemNumberCount +"," +
                                      "ItemNumber={"+ sItemNumber +"}," +
                                      Environment.NewLine + 
                                      "ProductCode={"+ sProductCode +"}," +
                                      "ComponentCode={"+ sComponentCode +"}," +
                                      "ProductQty={"+ sProductQty +"}," +
                                      "ExtendedAmount={"+ sExtendedAmount +"},"+
                                      "&ReturnStatus =" + ReturnStatus + ");", "SetInvoice");
                    string sItemTax = "";
                    foreach (var items in ItemTax)
                    {
                        if (!string.IsNullOrEmpty(sItemTax))
                        { sItemTax = sItemTax + "," + items.ToString(); }
                        else
                        { sItemTax = items.ToString(); }

                    }

                    if (bStoreTaxLogs == true) AddTaxLogsInFile(" vertexApiobj.GetTax(InvoiceNumber=" + InvoiceNumber + ", &InvoiceTax=" + InvoiceTax + ", ItemTax={" + sItemTax + "}, &GrossAmt=" + GrossAmt + ", &ReturnStatus=" + ReturnStatus + ");", "Before - GetTax");

                   

                    vertexApiobj.GetTax(InvoiceNumber, &InvoiceTax, ItemTax, &GrossAmt, &ReturnStatus);

                    string sItemTax2 = "";
                    foreach (var items in ItemTax)
                    {
                        if (!string.IsNullOrEmpty(sItemTax2))
                        { sItemTax2 = sItemTax2 + "," + items.ToString(); }
                        else
                        { sItemTax2 = items.ToString(); }

                    }

                    if (bStoreTaxLogs == true) AddTaxLogsInFile(" vertexApiobj.GetTax(InvoiceNumber=" + InvoiceNumber + ", &InvoiceTax=" + InvoiceTax + ", ItemTax={" + sItemTax2 + "}, &GrossAmt=" + GrossAmt + ", &ReturnStatus=" + ReturnStatus + ");", "After - GetTax");
                    vertexApiobj.Cleanup(&ReturnStatus);
                    if (bStoreTaxLogs == true) AddTaxLogsInFile(" vertexApiobj.Cleanup(&ReturnStatus=" + ReturnStatus + ");", "Cleanup");

                }

                //*************************************************************************************************
                // Vivensas changes ends over here
                //**************************************************************************************************

                objOrderInvoice.GrossAmt = GrossAmt;
                objOrderInvoice.InvoiceTax = InvoiceTax;
                InvoiceObject orderInvoice = new InvoiceObject();
                orderInvoice = objOrderInvoice;
                for (int index = 0; index < ItemNumberCount; index++)
                {
                    objOrderInvoice.InvoiceItemList.Where(d => d.ItemNumber == orderInvoice.InvoiceItemList[index].ItemNumber).FirstOrDefault().ItemTax = ItemTax[index];
                }
                objOrderInvoice = orderInvoice;
                log.Info("Invoice Tax : = " + objOrderInvoice.InvoiceTax);
                //string BaseDirectory = System.AppDomain.CurrentDomain.BaseDirectory + "VertexInvoices\\";
                string BaseDirectory = ResourceManager.DomainPath + "\\VertexInvoices\\";
                if (!System.IO.Directory.Exists(BaseDirectory))
                {
                    Directory.CreateDirectory(BaseDirectory);
                }
                FileStream fse = new FileStream(BaseDirectory +"\\"+ objOrderInvoice.InvoiceNumber + "_" + objOrderInvoice.Customer + (InvoiceTax == 0 ? "_NO_TAX" : "_TAX") + ".txt", FileMode.Create, FileAccess.Write);
                DataContractJsonSerializer DCJS = new DataContractJsonSerializer(typeof(InvoiceObject));
                DCJS.WriteObject(fse, objOrderInvoice);
                fse.Close();
                log.Info("[SalesLogicExpress.Application.Managers][TaxManager][End:TaxManagerConstructor]");
                return true;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][TaxManager][CalculateTax][ExceptionStackTrace = " + ex.StackTrace + "]\n\n" +
                "[InnerException = " + ex.InnerException + "]\n\n" +
                    "[Message = " + ex.Message + "]\n\n" +
                    "[Data = " + ex.Data + "]");
                return false;
            }


        }

        public void AddTaxLogsInFile(String sLogs, string sfunction)
        {
           
            try
            {
                log.Info("[SalesLogicExpress.Application.Managers][TaxManager][Start:CalculateTax]");
                StringBuilder exStr = new StringBuilder(2000);

                exStr.Append("==============================================================================================");
                exStr.Append(Environment.NewLine);
                exStr.Append("Date/Time: " + DateTime.Now);
                exStr.Append(Environment.NewLine);
                exStr.Append(sfunction.ToString());
                exStr.Append(Environment.NewLine);
                exStr.Append(sLogs);
                exStr.Append(Environment.NewLine);
                exStr.Append("==============================================================================================");

                try
                {
                    string path = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory );

                    File.AppendAllText(path + "\\Logs\\TaxCalcLog"+DateTime.Now.ToString("MMddyyyy")+".txt", exStr.ToString());
                }
                catch (Exception ex)
                {
                    log.Error("[SalesLogicExpress.Application.Managers][TaxManager][AddTaxLogsInFile][ExceptionStackTrace = " + ex.StackTrace + "]\n\n" +
               "[InnerException = " + ex.InnerException + "]\n\n" +
                   "[Message = " + ex.Message + "]\n\n" +
                   "[Data = " + ex.Data + "]");
                }
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][TaxManager][AddTaxLogsInFile][ExceptionStackTrace = " + ex.StackTrace + "]\n\n" +
               "[InnerException = " + ex.InnerException + "]\n\n" +
                   "[Message = " + ex.Message + "]\n\n" +
                   "[Data = " + ex.Data + "]");
            }
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
