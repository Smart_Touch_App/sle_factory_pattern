﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Managers
{
    public class ContactManager:IDisposable
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.ContactManager");

        private TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
        public void SetContactAsDefault(CustomerContact contact)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:SetContactAsDefault][contact=" + contact.SerializeToJson() + "]");
            try
            {
                string query = string.Format("Update busdta.M0111 set CDDFLT=0 where  CDAN8='{0}'", contact.CustomerID);
                DbEngine.ExecuteNonQuery(query);
                int phoneIndex = contact.DefaultPhone != null ? contact.DefaultPhone.Index : 0;
                int emailIndex = contact.DefaultEmail != null ? contact.DefaultEmail.Index : 0;
                foreach (Phone phone in contact.PhoneList)
                {
                    if (phone.Index != phoneIndex)
                    {
                        query = string.Format("Update busdta.M0111 set CDDFLTPH{0}=0 where  CDIDLN='{1}' and CDRCK7='{2}' and CDCNLN='{3}'", phone.Index, contact.LineID, contact.LineNo, contact.RelatedPersonID);
                        DbEngine.ExecuteNonQuery(query);
                    }
                }
                foreach (Email email in contact.EmailList)
                {
                    if (email.Index != emailIndex)
                    {
                        query = string.Format("Update busdta.M0111 set CDDFLTEM{0}=0 where  CDIDLN='{1}' and CDRCK7='{2}' and CDCNLN='{3}'", email.Index, contact.LineID, contact.LineNo, contact.RelatedPersonID);
                        DbEngine.ExecuteNonQuery(query);
                    }
                }
                if (emailIndex > 0)
                {
                    query = string.Format("Update busdta.M0111 set CDDFLTEM{0}=1 where  CDIDLN='{1}' and CDRCK7='{2}' and CDCNLN='{3}'", emailIndex, contact.LineID, contact.LineNo, contact.RelatedPersonID);
                    DbEngine.ExecuteNonQuery(query);
                }
                if (phoneIndex > 0)
                {
                    query = string.Format("Update busdta.M0111 set CDDFLTPH{0}=1 where  CDIDLN='{1}' and CDRCK7='{2}' and CDCNLN='{3}'", phoneIndex, contact.LineID, contact.LineNo, contact.RelatedPersonID);
                    DbEngine.ExecuteNonQuery(query);
                }
                query = string.Format("Update busdta.M0111 set CDDFLT=1 where  CDIDLN='{0}' and CDRCK7='{1}' and CDCNLN='{2}'", contact.LineID, contact.LineNo, contact.RelatedPersonID);
                DbEngine.ExecuteNonQuery(query);

                ResourceManager.QueueManager.QueueProcess("CustomerManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][SetContactAsDefault][contact=" + contact.SerializeToJson() + "][ExceptionStackTrace = " + ex.InnerException == null ? ex.Message : ex.InnerException.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:SetContactAsDefault][contact=" + contact.SerializeToJson() + "]");
        }
        public ObservableCollection<CustomerContact> GetCustomerContacts(string customerID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:GetCustomerContacts][customerID=" + customerID + "]");
            ObservableCollection<CustomerContact> contactList = new ObservableCollection<CustomerContact>();
            try
            {
                //string query = @"select tm.CTCD AS CD1,tm1.CTCD AS CD2 ,tm2.CTCD AS CD3, tm3.CTCD AS CD4, " +
                //                " tm.CTDSC1 AS CDSC1,tm1.CTDSC1 AS CDSC2 ,tm2.CTDSC1 AS CDSC3, tm3.CTDSC1 AS CDSC4, " +
                //                " tm4.CTCD AS CD5, tm5.CTCD AS CD6, tm6.CTCD AS CD7, " +
                //                " tm4.CTDSC1 AS CDSC5, tm5.CTDSC1 AS CDSC6, tm6.CTDSC1 AS CDSC7, " +
                //                " tm7.CTDSC1 AS CDSC8, " +
                //                " ct.* from busdta.M0111  ct " +
                //                " left join busdta.M080111  tm on tm.CTID = ct.CDPHTP1 " +
                //                " left join busdta.M080111  tm1 on tm1.CTID = ct.CDPHTP2 " +
                //                " left join busdta.M080111  tm2 on tm2.CTID = ct.CDPHTP3 " +
                //                " left join busdta.M080111  tm3 on tm3.CTID = ct.CDPHTP4 " +
                //                " LEFT JOIN BUSDTA.M080111 tm4 on tm4.CTID = ct.CDETP1 " +
                //                " LEFT JOIN BUSDTA.M080111 tm5 on tm5.CTID = ct.CDETP2 " +
                //                " LEFT JOIN BUSDTA.M080111 tm6 on tm6.CTID = ct.CDETP3 " +
                //                " LEFT JOIN BUSDTA.M080111 tm7 on tm7.CTID = ct.CDTITL where ct.CDAN8='{0}'";


                //string Handling for memory optimization --Vignesh D
                string query = string.Empty;
                query = "CALL BUSDTA.SP_GetCustomerContacts('{0}')";
                query = string.Format(query, customerID);
                DataSet result = DbEngine.ExecuteDataSet(query);
                query = string.Empty;
                CustomerContact contact;
                if (result.HasData())
                {
                    DataRow row;
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        try
                        {
                            row = result.Tables[0].Rows[i];
                            contact = new CustomerContact();
                            string firstName = row["CDGNNM"].ToString().Trim();
                            contact.FirstName = textInfo.ToTitleCase(firstName.ToLower());
                            string middleName = row["CDMDNM"].ToString().Trim();
                            contact.MiddleName = textInfo.ToTitleCase(middleName.ToLower());
                            string lastName = row["CDSRNM"].ToString().Trim();
                            contact.LastName = textInfo.ToTitleCase(lastName.ToLower());
                            contact.ContactName = contact.FirstName + " " + contact.MiddleName + " " + contact.LastName;
                            //string contactTitle = textInfo.ToTitleCase(row["CDTITL"].ToString().ToLower());
                            //contact.ContactTitle = contactTitle;
                            contact.ContactID = row["CDIDLN"].ToString();
                            contact.CustomerID = row["CDAN8"].ToString();
                            contact.LineID = row["CDIDLN"].ToString();
                            contact.LineNo = row["CDRCK7"].ToString();
                            contact.RelatedPersonID = row["CDCNLN"].ToString();
                            contact.EmailList = new ObservableCollection<Email>();
                            if (!string.IsNullOrEmpty(row["CDEMAL1"].ToString()))
                            {
                                contact.EmailList.Add(new Email { Index = 1, ContactID = row["CDIDLN"].ToString(), TypeID = Convert.ToInt32(row["CDETP1"].ToString().Trim()), Type = row["CDSC5"].ToString().Trim(), Value = row["CDEMAL1"].ToString(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTEM1"].ToString()) ? Convert.ToBoolean(row["CDDFLTEM1"].ToString()) : false });
                            }
                            if (!string.IsNullOrEmpty(row["CDEMAL2"].ToString()))
                            {
                                contact.EmailList.Add(new Email { Index = 2, ContactID = row["CDIDLN"].ToString(), TypeID = Convert.ToInt32(row["CDETP2"].ToString().Trim()), Type = row["CDSC6"].ToString().Trim(), Value = row["CDEMAL2"].ToString(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTEM2"].ToString()) ? Convert.ToBoolean(row["CDDFLTEM2"].ToString()) : false });
                            }
                            if (!string.IsNullOrEmpty(row["CDEMAL3"].ToString()))
                            {
                                contact.EmailList.Add(new Email { Index = 3, ContactID = row["CDIDLN"].ToString(), TypeID = Convert.ToInt32(row["CDETP3"].ToString().Trim()), Type = row["CDSC7"].ToString().Trim(), Value = row["CDEMAL3"].ToString(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTEM3"].ToString()) ? Convert.ToBoolean(row["CDDFLTEM3"].ToString()) : false });
                            }
                            //contact.EmailList.Add(new Email { Type = row["CDETP3"].ToString(), Value = row["CDEMAL4"].ToString(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTEM4"].ToString()) ? Convert.ToBoolean(row["CDDFLTEM4"].ToString()) : false });
                            if (contact.EmailList.Count > 0)
                            {
                                contact.DefaultEmail = contact.EmailList.FirstOrDefault(em => em.IsDefault == true);
                            }
                            contact.PhoneList = new ObservableCollection<Phone>();


                            if (!string.IsNullOrEmpty(row["CDPH1"].ToString()))
                            {
                                contact.PhoneList.Add(new Phone { Index = 1, ContactID = row["CDIDLN"].ToString(), TypeID = Convert.ToInt32(row["CDPHTP1"].ToString().Trim()), Type = row["CDSC1"].ToString().Trim(), Value = row["CDPH1"].ToString().Trim(), Extension = row["CDEXTN1"].ToString().Trim(), AreaCode = row["CDAR1"].ToString().Trim(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTPH1"].ToString()) ? Convert.ToBoolean(row["CDDFLTPH1"].ToString()) : false });
                            }
                            if (!string.IsNullOrEmpty(row["CDPH2"].ToString()))
                            {
                                contact.PhoneList.Add(new Phone { Index = 2, ContactID = row["CDIDLN"].ToString(), TypeID = Convert.ToInt32(row["CDPHTP2"].ToString().Trim()), Type = row["CDSC2"].ToString().Trim(), Value = row["CDPH2"].ToString().Trim(), Extension = row["CDEXTN2"].ToString().Trim(), AreaCode = row["CDAR2"].ToString().Trim(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTPH2"].ToString()) ? Convert.ToBoolean(row["CDDFLTPH2"].ToString()) : false });
                            }
                            if (!string.IsNullOrEmpty(row["CDPH3"].ToString()))
                            {
                                contact.PhoneList.Add(new Phone { Index = 3, ContactID = row["CDIDLN"].ToString(), TypeID = Convert.ToInt32(row["CDPHTP3"].ToString().Trim()), Type = row["CDSC3"].ToString().Trim(), Value = row["CDPH3"].ToString().Trim(), Extension = row["CDEXTN3"].ToString().Trim(), AreaCode = row["CDAR3"].ToString().Trim(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTPH3"].ToString()) ? Convert.ToBoolean(row["CDDFLTPH3"].ToString()) : false });
                            }
                            if (!string.IsNullOrEmpty(row["CDPH4"].ToString()))
                            {
                                contact.PhoneList.Add(new Phone { Index = 4, ContactID = row["CDIDLN"].ToString(), TypeID = Convert.ToInt32(row["CDPHTP4"].ToString().Trim()), Type = row["CDSC4"].ToString().Trim(), Value = row["CDPH4"].ToString().Trim(), Extension = row["CDEXTN4"].ToString().Trim(), AreaCode = row["CDAR4"].ToString().Trim(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTPH4"].ToString()) ? Convert.ToBoolean(row["CDDFLTPH4"].ToString()) : false });
                            }
                            if (contact.PhoneList.Count > 0)
                            {
                                contact.DefaultPhone = contact.PhoneList.FirstOrDefault(em => em.IsDefault == true);
                            }
                            contact.ContactTitle = row["CDSC8"] == null ? "" : row["CDSC8"].ToString().Trim();
                            //contact.TitleList = new ObservableCollection<Title>();
                            //if (!string.IsNullOrEmpty(row["CDTITL"].ToString()))
                            //{
                            //    contact.TitleList.Add(new Title { ContactID = row["CDIDLN"].ToString(), Type = row["CDSC8"].ToString().Trim() });
                            //}

                            contact.IsActive = !string.IsNullOrEmpty(row["CDACTV"].ToString()) ? Convert.ToBoolean(row["CDACTV"].ToString()) : false;
                            contact.ShowOnDashboard = !string.IsNullOrEmpty(row["CDDFLT"].ToString()) ? Convert.ToBoolean(row["CDDFLT"].ToString()) : false;
                            contact.IsExpanded = contact.ShowOnDashboard;
                            contact.IsDefault = row["CDIDLN"].ToString().Trim().Equals("0") ? true : false;

                            //contact.ShowOnDashboard = contact.IsDefault;
                            // if (!string.IsNullOrEmpty(contact.FirstName.Trim()) && !string.IsNullOrEmpty(contact.LastName.Trim()))
                            if (!string.IsNullOrEmpty(contact.FirstName.Trim()))
                            {
                                contactList.Add(contact);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][GetCustomerContacts][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                            continue;
                        }
                    }
                    row = null;
                    Email selectedEmail = null;
                    Phone selectedPhone = null;
                    foreach (CustomerContact contactItem in contactList)
                    {
                        if (contactItem.ShowOnDashboard)
                        {
                            selectedEmail = contactItem.EmailList.FirstOrDefault(email => email.IsDefault == true);
                            if (selectedEmail != null)
                            {
                                selectedEmail.IsSelectedEmail = true;
                            }
                            selectedPhone = contactItem.PhoneList.FirstOrDefault(phone => phone.IsDefault == true);
                            if (selectedPhone != null)
                            {
                                selectedPhone.IsSelectedPhone = true;
                            }
                        }
                    }
                }

                //Dataset Dispose for Memory Optimization --Vignesh D
                result.Dispose();
                result = null;

                
                contact = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][GetCustomerContacts][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:GetCustomerContacts][customerID=" + customerID + "]");
            return contactList;
        }

        public CustomerContact GetCustomerContactDetails(string customerID, string contactID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:GetCustomerContactDetails][customerID=" + customerID + "]");
            CustomerContact contact = new CustomerContact();

            try
            {
                //string query = "select tm.CTCD AS CD1,tm1.CTCD AS CD2 ,tm2.CTCD AS CD3, tm3.CTCD AS CD4, " +
                //                " tm.CTDSC1 AS CDSC1,tm1.CTDSC1 AS CDSC2 ,tm2.CTDSC1 AS CDSC3, tm3.CTDSC1 AS CDSC4, " +
                //                " tm4.CTCD AS CD5, tm5.CTCD AS CD6, tm6.CTCD AS CD7, " +
                //                " tm4.CTDSC1 AS CDSC5, tm5.CTDSC1 AS CDSC6, tm6.CTDSC1 AS CDSC7, " +
                //                " ct.* from busdta.M0111  ct " +
                //                " left join busdta.M080111  tm on tm.CTID = ct.CDPHTP1 " +
                //                " left join busdta.M080111  tm1 on tm1.CTID = ct.CDPHTP2 " +
                //                " left join busdta.M080111  tm2 on tm2.CTID = ct.CDPHTP3 " +
                //                " left join busdta.M080111  tm3 on tm3.CTID = ct.CDPHTP4 " +
                //                " LEFT JOIN BUSDTA.M080111 tm4 on tm4.CTID = ct.CDETP1 " +
                //                " LEFT JOIN BUSDTA.M080111 tm5 on tm5.CTID = ct.CDETP2 " +
                //                " LEFT JOIN BUSDTA.M080111 tm6 on tm6.CTID = ct.CDETP3 where ct.CDAN8='{0}' and ct.CDIDLN='{1}'";
                string query = ("CALL BUSDTA.SP_GetCustomerContactDetails('{0}','{1}')");
                query = string.Format(query, customerID, contactID);
                DataSet result = DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    DataRow row;
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        try
                        {
                            row = result.Tables[0].Rows[i];
                            contact = new CustomerContact();
                            string firstName = row["CDGNNM"].ToString().Trim();
                            contact.FirstName = textInfo.ToTitleCase(firstName.ToLower());
                            string middleName = row["CDMDNM"].ToString().Trim();
                            contact.MiddleName = textInfo.ToTitleCase(middleName.ToLower());
                            string lastName = row["CDSRNM"].ToString().Trim();
                            contact.LastName = textInfo.ToTitleCase(lastName.ToLower());
                            contact.ContactName = contact.FirstName + " " + contact.MiddleName + " " + contact.LastName;
                            string contactTitle = textInfo.ToTitleCase(row["CDTITL"].ToString().ToLower());
                            contact.ContactTitle = contactTitle;
                            contact.ContactID = row["CDIDLN"].ToString();
                            contact.CustomerID = row["CDAN8"].ToString();
                            contact.LineID = row["CDIDLN"].ToString();
                            contact.LineNo = row["CDRCK7"].ToString();
                            contact.RelatedPersonID = row["CDCNLN"].ToString();
                            contact.EmailList = new ObservableCollection<Email>();
                            if (!string.IsNullOrEmpty(row["CDEMAL1"].ToString()))
                            {
                                contact.EmailList.Add(new Email { Index = 1, ContactID = row["CDIDLN"].ToString(), TypeID = Convert.ToInt32(row["CDETP1"].ToString().Trim()), Type = row["CD5"].ToString().Trim() + " - " + row["CDSC5"].ToString().Trim(), Value = row["CDEMAL1"].ToString(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTEM1"].ToString()) ? Convert.ToBoolean(row["CDDFLTEM1"].ToString()) : false });
                            }
                            if (!string.IsNullOrEmpty(row["CDEMAL2"].ToString()))
                            {
                                contact.EmailList.Add(new Email { Index = 2, ContactID = row["CDIDLN"].ToString(), TypeID = Convert.ToInt32(row["CDETP2"].ToString().Trim()), Type = row["CD6"].ToString().Trim() + " - " + row["CDSC6"].ToString().Trim(), Value = row["CDEMAL2"].ToString(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTEM2"].ToString()) ? Convert.ToBoolean(row["CDDFLTEM2"].ToString()) : false });
                            }
                            if (!string.IsNullOrEmpty(row["CDEMAL3"].ToString()))
                            {
                                contact.EmailList.Add(new Email { Index = 3, ContactID = row["CDIDLN"].ToString(), TypeID = Convert.ToInt32(row["CDETP3"].ToString().Trim()), Type = row["CD7"].ToString().Trim() + " - " + row["CDSC7"].ToString().Trim(), Value = row["CDEMAL3"].ToString(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTEM3"].ToString()) ? Convert.ToBoolean(row["CDDFLTEM3"].ToString()) : false });
                            }
                            //contact.EmailList.Add(new Email { Type = row["CDETP3"].ToString(), Value = row["CDEMAL4"].ToString(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTEM4"].ToString()) ? Convert.ToBoolean(row["CDDFLTEM4"].ToString()) : false });
                            if (contact.EmailList.Count > 0)
                            {
                                contact.DefaultEmail = contact.EmailList.FirstOrDefault(em => em.IsDefault == true);
                            }
                            contact.PhoneList = new ObservableCollection<Phone>();


                            if (!string.IsNullOrEmpty(row["CDPH1"].ToString()))
                            {
                                contact.PhoneList.Add(new Phone { Index = 1, ContactID = row["CDIDLN"].ToString(), Type = row["CD1"].ToString().Trim() + " - " + row["CDSC1"].ToString().Trim(), TypeID = Convert.ToInt32(row["CDPHTP1"].ToString().Trim()), Value = row["CDPH1"].ToString().Trim(), Extension = row["CDEXTN1"].ToString().Trim(), AreaCode = row["CDAR1"].ToString().Trim(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTPH1"].ToString()) ? Convert.ToBoolean(row["CDDFLTPH1"].ToString()) : false });
                            }
                            if (!string.IsNullOrEmpty(row["CDPH2"].ToString()))
                            {
                                contact.PhoneList.Add(new Phone { Index = 2, ContactID = row["CDIDLN"].ToString(), Type = row["CD2"].ToString().Trim() + " - " + row["CDSC2"].ToString().Trim(), TypeID = Convert.ToInt32(row["CDPHTP2"].ToString().Trim()), Value = row["CDPH2"].ToString().Trim(), Extension = row["CDEXTN2"].ToString().Trim(), AreaCode = row["CDAR2"].ToString().Trim(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTPH2"].ToString()) ? Convert.ToBoolean(row["CDDFLTPH2"].ToString()) : false });
                            }
                            if (!string.IsNullOrEmpty(row["CDPH3"].ToString()))
                            {
                                contact.PhoneList.Add(new Phone { Index = 3, ContactID = row["CDIDLN"].ToString(), Type = row["CD3"].ToString().Trim() + " - " + row["CDSC3"].ToString().Trim(), TypeID = Convert.ToInt32(row["CDPHTP3"].ToString().Trim()), Value = row["CDPH3"].ToString().Trim(), Extension = row["CDEXTN3"].ToString().Trim(), AreaCode = row["CDAR3"].ToString().Trim(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTPH3"].ToString()) ? Convert.ToBoolean(row["CDDFLTPH3"].ToString()) : false });
                            }
                            if (!string.IsNullOrEmpty(row["CDPH4"].ToString()))
                            {
                                contact.PhoneList.Add(new Phone { Index = 4, ContactID = row["CDIDLN"].ToString(), Type = row["CD4"].ToString().Trim() + " - " + row["CDSC4"].ToString().Trim(), TypeID = Convert.ToInt32(row["CDPHTP4"].ToString().Trim()), Value = row["CDPH4"].ToString().Trim(), Extension = row["CDEXTN4"].ToString().Trim(), AreaCode = row["CDAR4"].ToString().Trim(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTPH4"].ToString()) ? Convert.ToBoolean(row["CDDFLTPH4"].ToString()) : false });
                            }
                            if (contact.PhoneList.Count > 0)
                            {
                                contact.DefaultPhone = contact.PhoneList.FirstOrDefault(em => em.IsDefault == true);
                            }
                            contact.IsActive = !string.IsNullOrEmpty(row["CDACTV"].ToString()) ? Convert.ToBoolean(row["CDACTV"].ToString()) : false;
                            contact.ShowOnDashboard = !string.IsNullOrEmpty(row["CDDFLT"].ToString()) ? Convert.ToBoolean(row["CDDFLT"].ToString()) : false;
                            contact.IsExpanded = contact.ShowOnDashboard;
                            contact.IsDefault = row["CDIDLN"].ToString().Trim().Equals("0") ? true : false;
                            
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][GetCustomerContactDetails][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                            continue;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][GetCustomerContactDetails][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:GetCustomerContactDetails][customerID=" + customerID + "]");
            return contact;
        }
        public ObservableCollection<ContactType> GetContactTypes()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:GetContactTypes]");
            ObservableCollection<ContactType> contactTypeList = new ObservableCollection<ContactType>();
            try
            {
                //string query = "select * from busdta.M080111";
                string query = "CALL BUSDTA.SP_GetContactTypes()";
                DataSet result = DbEngine.ExecuteDataSet(query);
                ContactType contactType;
                if (result.HasData())
                {
                    DataRow row;
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        try
                        {
                            row = result.Tables[0].Rows[i];
                            contactType = new ContactType();
                            contactType.TypeID = row["CTID"].ToString().Trim();
                            contactType.Type = row["CTTYP"].ToString().Trim();
                            contactType.Code = row["CTCD"].ToString().Trim();
                            contactType.Description = row["CTDSC1"].ToString().Trim();
                            contactTypeList.Add(contactType);
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][GetContactTypes][ExceptionStackTrace = " + ex.StackTrace + "]");
                            continue;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][GetContactTypes][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:GetContactTypes]");
            return contactTypeList;
        }
        public int AddCustomerContact(CustomerContact contact)
        {
            int result = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:AddCustomerContact][contact=" + contact + "]");
            ObservableCollection<CustomerContact> contactList = new ObservableCollection<CustomerContact>();
            try
            {
              //  string queryUniqueID = "SELECT COALESCE((MAX(CDIDLN)+1),0) FROM BUSDTA.M0111 WHERE CDAN8 = '" + contact.CustomerID + "';";
                string queryUniqueID = ("CALL BUSDTA.SP_GetAddCustomerContact('" + contact.CustomerID + "')");
                int uniqueID = Convert.ToInt32(DbEngine.ExecuteScalar(queryUniqueID));
                int isDefault = 0;
                if (uniqueID == 0)
                {
                    isDefault = 1;
                }
                string query = "INSERT INTO BUSDTA.M0111 (\"CDIDLN\",\"CDRCK7\",\"CDCNLN\",\"CDAN8\",\"CDAR1\",\"CDPH1\",\"CDEXTN1\",\"CDPHTP1\",\"CDDFLTPH1\",\"CDEMAL1\",\"CDETP1\",\"CDDFLTEM1\",\"CDGNNM\",\"CDMDNM\",\"CDSRNM\",\"CDTITL\",\"CDACTV\",\"CDDFLT\")";
                query = query + " VALUES(";
                query = query + "'" + uniqueID + "',";
                query = query + "'" + "0" + "',";
                query = query + "'" + "0" + "',";
                query = query + "'" + contact.CustomerID + "',";
                query = query + (contact.PhoneList.Count > 0 ? "'" + contact.PhoneList[0].AreaCode + "'" : "NULL") + ",";
                query = query + (contact.PhoneList.Count > 0 ? "'" + contact.PhoneList[0].Value + "'" : "NULL") + ",";
                query = query + (contact.PhoneList.Count > 0 ? "'" + contact.PhoneList[0].Extension + "'" : "NULL") + ",";
                query = query + (contact.PhoneList.Count > 0 ? "'" + contact.PhoneList[0].Type + "'" : "NULL") + ",";
                query = query + isDefault + ",";
                query = query + (contact.EmailList.Count > 0 ? "'" + contact.EmailList[0].Value.ToLower() + "'" : "NULL") + ",";
                query = query + (contact.EmailList.Count > 0 ? "'" + contact.EmailList[0].Type + "'" : "NULL") + ",";
                query = query + isDefault + ",";
                query = query + "'" + contact.FirstName + "',";
                query = query + "'" + contact.MiddleName + "',";
                query = query + "'" + contact.LastName + "',";
                query = query + "'" + contact.ContactTitle + "',";
                query = query + "1,";
                query = query + isDefault + "";
                query = query + ")";
                result = DbEngine.ExecuteNonQuery(query);

                ResourceManager.QueueManager.QueueProcess("CustomerManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][AddCustomerContact][contact=" + contact + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:AddCustomerContact][contact=" + contact + "]");
            return result;
        }
        public int DeleteCustomerContact(CustomerContact contact)
        {
            int result = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:DeleteCustomerContact][contactID=" + contact + "]");
            ObservableCollection<CustomerContact> contactList = new ObservableCollection<CustomerContact>();
            try
            {
                string query = "DELETE FROM BUSDTA.M0111 WHERE CDIDLN ='";
                query = query + contact.ContactID + "' AND CDAN8='";
                query = query + contact.CustomerID + "';";

                result = Helpers.DbEngine.ExecuteNonQuery(query);

                ResourceManager.QueueManager.QueueProcess("CustomerManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][DeleteCustomerContact][contactID=" + contact + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:DeleteCustomerContact][contactID=" + contact + "]");

            return result;
        }
        public int SaveEditedContact(CustomerContact customerContact)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:SaveEditedContact][contact=" + customerContact + "]");
            ObservableCollection<CustomerContact> contact = new ObservableCollection<CustomerContact>();

            int result = -1;
            try
            {
                string query = "UPDATE BUSDTA.M0111 SET CDGNNM = '" + customerContact.FirstName + "',";
                query = query + "CDMDNM = '" + customerContact.MiddleName + "',";
                query = query + "CDSRNM = '" + customerContact.LastName + "',";
                query = query + "CDTITL = '" + customerContact.ContactTitle + "',";
                query = query + "CDAR1 = " + (customerContact.PhoneList.Count > 0 ? "'" + customerContact.PhoneList[0].AreaCode + "'" : "NULL") + ",";
                query = query + "CDPH1 = " + (customerContact.PhoneList.Count > 0 ? "'" + customerContact.PhoneList[0].Value + "'" : "NULL") + ",";
                query = query + "CDEXTN1 =" + (customerContact.PhoneList.Count > 0 ? "'" + customerContact.PhoneList[0].Extension + "'" : "NULL") + ",";
                //query = query + "CDPHTP1 = '" + PhoneTypeID + "',";
                query = query + "CDPHTP1 = " + (customerContact.PhoneList.Count > 0 ? "'" + customerContact.PhoneList[0].TypeID + "'" : "NULL") + ",";
                query = query + "CDEMAL1 = " + (customerContact.EmailList.Count > 0 ? "'" + customerContact.EmailList[0].Value.ToLower() + "'" : "NULL") + ",";
                query = query + "CDETP1 = " + (customerContact.EmailList.Count > 0 ? "'" + customerContact.EmailList[0].TypeID + "'" : "NULL");
                query = query + " WHERE CDIDLN ='" + customerContact.ContactID + "' AND ";
                query = query + "CDAN8 ='";
                query = query + customerContact.CustomerID + "';";

                result = Helpers.DbEngine.ExecuteNonQuery(query);

                ResourceManager.QueueManager.QueueProcess("CustomerManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][SaveEditedContact][contact=" + contact + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:SaveEditedContact][contact=" + contact + "]");
            return result;
        }
        //update exsting emailid of contact
        public int SaveEditedEmail(CustomerContact contactEmail, int index)
        {
            int result = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:SaveEditedEmail][contact=" + contactEmail + "]");
            ObservableCollection<CustomerContact> contact = new ObservableCollection<CustomerContact>();
            int i = index;

            try
            {
                string query = "UPDATE BUSDTA.M0111 SET CDEMAL" + i + "= ";
                query = query + (contactEmail.EmailList.Count > 0 ? "'" + contactEmail.EmailList[0].Value.ToLower() + "'" : "NULL") + ",";
                query = query + " CDETP" + i + "=" + "'" + contactEmail.EmailList[0].TypeID + "'";
                query = query + " WHERE CDIDLN ='" + contactEmail.ContactID + "'";
                query = query + " AND CDAN8 = '" + contactEmail.CustomerID + "';";

                result = Helpers.DbEngine.ExecuteNonQuery(query);

                ResourceManager.QueueManager.QueueProcess("CustomerManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][SaveEditedEmail][contact=" + contactEmail + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:SaveEditedEmail][contact=" + contactEmail + "]");
            return result;
        }
        //update existing phone no. of contact
        public int SaveEditedPhone(CustomerContact contactPhone, int index)
        {
            int result = -1;

            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:SaveEditedPhone][contact=" + contactPhone + "]");
            ObservableCollection<CustomerContact> contact = new ObservableCollection<CustomerContact>();

            int i = index;
            try
            {
                string query = "UPDATE BUSDTA.M0111 SET CDAR" + i + " =";
                query = query + (contactPhone.PhoneList.Count > 0 ? "'" + contactPhone.PhoneList[0].AreaCode + "'" : "NULL") + ",";
                query = query + "CDPH" + i + "=" + (contactPhone.PhoneList.Count > 0 ? "'" + contactPhone.PhoneList[0].Value + "'" : "NULL");
                query = query + ", CDEXTN" + i + "=" + (contactPhone.PhoneList.Count > 0 ? "'" + contactPhone.PhoneList[0].Extension + "'" : "NULL");
                query = query + ", CDPHTP" + i + "=" + "'" + contactPhone.PhoneList[0].TypeID + "'";
                query = query + " WHERE CDIDLN ='" + contactPhone.ContactID + "'";
                query = query + " AND CDAN8 = '" + contactPhone.CustomerID + "';";

                result = Helpers.DbEngine.ExecuteNonQuery(query);

                ResourceManager.QueueManager.QueueProcess("CustomerManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][SaveEditedPhone][contact=" + contactPhone + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:SaveEditedPhone][contact=" + contactPhone + "]");
            return result;
        }
        public int GetTotalPhoneNumber(string contactID, string customerID)
        {
            int result = -1;
            try
            {
                //string query = "SELECT ISNULL(COUNT(c.CDAR1),0) + isnull(COUNT(c.CDAR2),0) + ";
                //query = query + " isnull(COUNT(c.CDAR3),0)+ ISNULL(COUNT(c.CDAR4),0) ";
                //query = query + " FROM BUSDTA.M0111 c WHERE c.CDAN8 = '" + customerID + "' AND ";
                //query = query + " c.CDIDLN = '" + contactID + "';";
                string query = ("CALL BUSDTA.SP_GetTotalPhoneNumber('" + customerID + "','" + contactID + "')");

                result = Convert.ToInt32(Helpers.DbEngine.ExecuteScalar(query));
                result = result + 1;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][GetTotalPhoneNumber][contactID=" + contactID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }
        // Adding new phone no. to contact
        public int SaveContactPhone(Phone contactPhone, string CustomerID)
        {
            string contactID = contactPhone.ContactID;

            int result = -1;
            int i = GetTotalPhoneNumber(contactID, CustomerID);
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:AddCustomerContact][contact=" + contactPhone + "]");
            ObservableCollection<CustomerContact> contact = new ObservableCollection<CustomerContact>();
            try
            {
                string query = " UPDATE BUSDTA.M0111 SET " +
                               " CDAR" + i + " =  '" + contactPhone.AreaCode + "', " +
                               " CDPH" + i + " =  '" + contactPhone.Value + "', " +
                               " CDEXTN" + i + "= '" + contactPhone.Extension + "', " +
                               " CDPHTP" + i + " = '" + Convert.ToInt32(contactPhone.Type) + "'" +
                               " WHERE CDIDLN ='" + contactPhone.ContactID + "' AND CDAN8 = '" + CustomerID + "' ";

                result = Helpers.DbEngine.ExecuteNonQuery(query);

                ResourceManager.QueueManager.QueueProcess("CustomerManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][SaveContactPhone][contact=" + contactPhone + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:SaveContactPhone][contact=" + contactPhone + "]");
            return result;
        }
        public int GetTotalEmailNumber(string contactID, string customerID)
        {
            int result = -1;
            try
            {
                result = 0;
               // string query = "SELECT ISNULL(COUNT(c.CDEMAL1),0) + ISNULL(COUNT(c.CDEMAL2),0) +  ISNULL(COUNT(c.CDEMAL3),0)  FROM BUSDTA.M0111 c WHERE c.CDAN8 = '{0}' AND c.CDIDLN = '{1}'";
                //query = "SELECT *  FROM BUSDTA.M0111 c WHERE c.CDAN8 = '{0}' AND c.CDIDLN = '{1}';";
                string query = ("CALL BUSDTA.SP_GetTotalEmailNumber('{0}','{1}')");
                query = string.Format(query, customerID, contactID);
                result =  Convert.ToInt32(Helpers.DbEngine.ExecuteScalar(query));
               
                result = result + 1;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][GetTotalPhoneNumber][contactID=" + contactID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }
        //adding new emailid to contact
        public int SaveContactEmail(Email contactEmail, string CustomerID)
        {
            string contactID = contactEmail.ContactID;
            //double res;
            //bool flag;
            //flag = Double.TryParse(contactEmail.Type, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture, out res);

            int result = -1;
            int i = GetTotalEmailNumber(contactID, CustomerID);
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:SaveContactEmail][contact=" + contactEmail + "]");
            // ObservableCollection<CustomerContact> contact = new ObservableCollection<CustomerContact>();
            try
            {
                string query = "UPDATE BUSDTA.M0111 SET CDEMAL" + i + "= '" + contactEmail.Value.ToLower() + "', ";
                query = query + "CDETP" + i + " ='" + Convert.ToInt32(contactEmail.Type) + "' ";
                query = query + " WHERE CDIDLN ='" + contactEmail.ContactID + "' AND ";
                query = query + " CDAN8 = '" + CustomerID + "';";

                result = Helpers.DbEngine.ExecuteNonQuery(query);

                ResourceManager.QueueManager.QueueProcess("CustomerManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][SaveContactEmail][contact=" + contactEmail + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:SaveContactEmail][contact=" + contactEmail + "]");
            return result;
        }
        public CustomerContact GetContactNameAndTitle(string contactID, string customerID)
        {
            //CustomerContact> contactList = new ObservableCollection<CustomerContact>();
            CustomerContact contact = new CustomerContact();
            try
            {
            //    string query = "SELECT TOP 1 CDGNNM, CDMDNM, CDSRNM, CDTITL FROM BUSDTA.M0111 WHERE CDIDLN ='" + contactID + "' AND ";
            //    query = query + " CDAN8 ='";
            //    query = query + customerID + "';";
                string query = "CALL BUSDTA.SP_GetContactNameAndTitle('" + contactID + "','"+ customerID + "')";

                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                // CustomerContact contact;
                if (result.HasData())
                {
                    DataRow row;
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        try
                        {
                            row = result.Tables[0].Rows[i];
                            contact = new CustomerContact();    // TODO: redundant memory allocation?
                            string firstName = row["CDGNNM"].ToString().Trim();
                            contact.FirstName = textInfo.ToTitleCase(firstName.ToLower());
                            string middleName = row["CDMDNM"].ToString().Trim();
                            contact.MiddleName = textInfo.ToTitleCase(middleName.ToLower());
                            string lastName = row["CDSRNM"].ToString().Trim();
                            contact.LastName = textInfo.ToTitleCase(lastName.ToLower());
                            contact.ContactName = contact.FirstName + " " + contact.MiddleName + " " + contact.LastName;
                            contact.ContactTitle = row["CDTITL"].ToString();
                            //contactList.Add(contact);
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][GetContactNameAndTitle][customerID=" + customerID + "][contactID=" + contactID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][GetContactNameAndTitle][customerID=" + customerID + "][contactID=" + contactID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return contact;
        }
        public int DeleteContactEmail(CustomerContact contactEmail, int index)
        {
            int result = -1;
            int i = index;
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:DeleteContactEmail][contact=" + contactEmail + "]");
            try
            {
                string query = "UPDATE BUSDTA.M0111 SET CDEMAL" + i + "= NULL, CDETP" + i + "= NULL";
                query = query + " WHERE CDIDLN ='" + contactEmail.ContactID + "'";
                query = query + " AND CDAN8 = '" + contactEmail.CustomerID + "';";

                result = Helpers.DbEngine.ExecuteNonQuery(query);

                ResourceManager.QueueManager.QueueProcess("CustomerManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][DeleteContactEmail][contact=" + contactEmail + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:DeleteContactEmail][contact=" + contactEmail + "]");
            return result;
        }
        public int DeleteContactPhone(CustomerContact contactPhone, int index)
        {
            int result = -1;
            int i = index;
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:DeleteContactPhone][contact=" + contactPhone + "]");
            try
            {
                string query = "UPDATE BUSDTA.M0111 SET CDAR" + i + "= NULL, CDPH" + i + "= NULL, CDEXTN" + i + "= NULL, CDPHTP" + i + "= NULL";
                query = query + " WHERE CDIDLN ='" + contactPhone.ContactID + "'";
                query = query + " AND CDAN8 = '" + contactPhone.CustomerID + "';";

                result = Helpers.DbEngine.ExecuteNonQuery(query);

                ResourceManager.QueueManager.QueueProcess("CustomerManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][DeleteContactPhone][contact=" + contactPhone + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:DeleteContactPhone][contact=" + contactPhone + "]");
            return result;
        }
        public int UpdateEmailIndexAfterDelete(CustomerContact contact, int Index)
        {
            int result = -1;
            int index = Index;
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:UpdateIndexAfterDelete][contact=" + contact + "]");
            try
            {

                //string query = "select isnull(CDEMAL" + (index+1) + ",NULL) from BUSDTA.M0111 where CDIDLN ='" + contact.ContactID + "'";
                //query = query + " AND CDAN8 = '" + contact.CustomerID + "';";
                string query = "CALL BUSDTA.SP_UpdateEmailIndexAfterDelete('" + contact.ContactID + "','" + contact.CustomerID + "')";

                string res = DbEngine.ExecuteScalar(query);
                if (res != null)
                {
                    for (int i = 0; i < contact.EmailList.Count; i++)
                    {
                        string updateQuery = "update BUSDTA.M0111 set cdemal" + index + " = cdemal" + (index + 1) + ", cdemal" + (index + 1) + " = null";
                        updateQuery = updateQuery + " ,cdetp" + index + " = cdetp" + (index + 1) + ", cdetp" + (index + 1) + "= null";
                        updateQuery = updateQuery + " where CDIDLN ='" + contact.ContactID + "'";
                        updateQuery = updateQuery + " AND CDAN8 = '" + contact.CustomerID + "';";
                        result = DbEngine.ExecuteNonQuery(updateQuery);
                        index++;
                    }
                }

                ResourceManager.QueueManager.QueueProcess("CustomerManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][UpdateIndexAfterDelete][contact=" + contact + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:UpdateIndexAfterDelete][contact=" + contact + "]");

            return result;
        }
        public int UpdatePhoneIndexAfterDelete(CustomerContact contact, int Index)
        {
            int result = -1;
            int index = Index;
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:UpdateIndexAfterDelete][contact=" + contact + "]");
            try
            {

                //string query = "select isnull(CDAR" + (index + 1) + ",NULL) from BUSDTA.M0111 where CDIDLN ='" + contact.ContactID + "'";
                //query = query + " AND CDAN8 = '" + contact.CustomerID + "';";
                string query = "CALL BUSDTA.SP_GetUpdatePhoneIndexAfterDelete('" + contact.ContactID + "','" + contact.CustomerID + "'," + (index + 1) + ")";

                string res = DbEngine.ExecuteScalar(query);
                if (res != null)
                {
                    for (int i = 0; i < contact.PhoneList.Count; i++)
                    {
                        string updateQuery = "update BUSDTA.M0111 set CDAR" + index + " = CDAR" + (index + 1) + ", CDAR" + (index + 1) + " = null";
                        updateQuery = updateQuery + " ,CDPH" + index + " = CDPH" + (index + 1) + ", CDPH" + (index + 1) + "= null";
                        updateQuery = updateQuery + " ,CDEXTN" + index + " = CDEXTN" + (index + 1) + ", CDEXTN" + (index + 1) + "= null";
                        updateQuery = updateQuery + " ,CDPHTP" + index + " = CDPHTP" + (index + 1) + ", CDPHTP" + (index + 1) + "= null";
                        updateQuery = updateQuery + " where CDIDLN ='" + contact.ContactID + "'";
                        updateQuery = updateQuery + " AND CDAN8 = '" + contact.CustomerID + "';";
                        result = DbEngine.ExecuteNonQuery(updateQuery);
                        index++;
                    }
                }

                ResourceManager.QueueManager.QueueProcess("CustomerManagement", false);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][UpdateIndexAfterDelete][contact=" + contact + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:UpdateIndexAfterDelete][contact=" + contact + "]");

            return result;
        }

        // Code written by Marimuthu for Performance change
        // Dispose implementation for Performance optimization
        // Date : 11/03/2016
        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
    }
}
