﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesLogicExpress.Application.Helpers
{
    public class ConfirmWindow
    {
        public string Message
        {
            get;
            set;
        }

        public string MessageIcon
        {
            get;
            set;
        }
        public string OkButtonContent
        {
            get;
            set;
        }
        public string CancelButtonContent
        {
            get;
            set;
        }
        public bool Confirmed
        {
            get;
            set;
        }
        public bool Cancelled
        {
            get;
            set;
        }
        public bool Closed { get; set; }
        public string Header
        {
            get;
            set;
        }
        public object Context
        {
            get;
            set;
        }
        public string TemplateKey
        {
            get;
            set;
        }

    }
    public class CloseDialogWindow
    {
        public string DialogKey
        {
            get;
            set;
        }

    }
}
