﻿using Sap.Data.SQLAnywhere;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Helpers
{
    public static class DbEngine
    {
        private static readonly ILog Loggger = LogManager.GetLogger("SalesLogicExpress.Application.Helpers.DbEngine");

        static string ConnectionString = ConfigurationManager.AppSettings.Get("connectionString");
        public static DataSet ExecuteDataSet(string Query)
        {
            try
            {
                return ExecuteDataSet(Query, null, null, null);
            }
            catch (Exception ex)
            {
                Loggger.Error("[SalesLogicExpress.Application.Helpers][DbEngine][ExecuteDataSet][Query=" + Query + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }

        }

        public static DataSet ExecuteDataSet(string query, string sourceTable)
        {
            return ExecuteDataSet(query, null, sourceTable, null);
        }
        public static DataSet ExecuteDataSet(Dictionary<string, string> queryListWithSourceTable)
        {
            DataSet result = new DataSet();
            try
            {
                Loggger.Info("[SalesLogicExpress.Application.Helpers][DbEngine][Start:ExecuteDataSet][QueryListWithSourceTable=" + queryListWithSourceTable.SerializeToJson() + "]");
                string query, sourceTableName;
                SAConnection dbConnection = ResourceManager.GetOpenConnectionInstance();
                using (SACommand command = new SACommand(string.Empty, dbConnection))
                {
                    command.CommandType = CommandType.Text;
                    using (SADataAdapter adapter = new SADataAdapter(command))
                    {
                        foreach (KeyValuePair<string, string> queryItem in queryListWithSourceTable)
                        {
                            sourceTableName = queryItem.Key;
                            query = queryItem.Value;
                            command.CommandText = query;
                            adapter.Fill(result, sourceTableName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Loggger.Error("[SalesLogicExpress.Application.Helpers][DbEngine][ExecuteDataSet][QueryListWithSourceTable=" + queryListWithSourceTable.ToString() + "][ExceptionStackTrace = " + ex.StackTrace + "]");

                throw;
            }
            Loggger.Info("[SalesLogicExpress.Application.Helpers][DbEngine][End:ExecuteDataSet]");
            return result;
        }
        public static DataSet ExecuteDataSet(string query, Dictionary<string, object> parameters, string sourceTable, DataSet referenceDataSet)
        {
            DataSet result = referenceDataSet == null ? new DataSet() : referenceDataSet;
            try
            {
                Loggger.Info("[SalesLogicExpress.Application.Helpers][DbEngine][Start:ExecuteDataSet][Query=" + query + "][Parameters=" + parameters.SerializeToJson() + "][SourceTable=" + sourceTable + "][ReferenceDataSet=" + referenceDataSet.SerializeToJson() + "]");
                SAConnection dbConnection = ResourceManager.GetOpenConnectionInstance();
                if (dbConnection == null) return null;
                using (SACommand command = new SACommand(string.Empty, dbConnection))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = query;
                    AddParameters(parameters, command);
                    using (SADataAdapter adapter = new SADataAdapter(command))
                    {
                        if (string.IsNullOrEmpty(sourceTable))
                        {
                            adapter.Fill(result);
                        }
                        else
                        {
                            adapter.Fill(result, sourceTable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Loggger.Error("[SalesLogicExpress.Application.Helpers][DbEngine][ExecuteDataSet][Query=" + query + "][ExceptionStackTrace = " + ex.Message == null ? ex.InnerException.Message : ex.Message + "]");
                throw;
            }
            Loggger.Info("[SalesLogicExpress.Application.Helpers][DbEngine][End:ExecuteDataSet]");
            return result;
        }
        public static DataSet ExecuteDataSet(string query, Dictionary<string, object> parameters)
        {
            return ExecuteDataSet(query, parameters, null, null);
        }
        public static int ExecuteNonQuery(string query)
        {
            return ExecuteNonQuery(query, null);
        }
        public static int ExecuteNonQuery(string query, Dictionary<string, object> parameters)
        {
            return ExecuteNonQuery(query, parameters, false);
        }
        public static int ExecuteNonQuery(string query, Dictionary<string, object> parameters, bool isStoreProcedure)
        {
            int result = 0;
            try
            {
                Loggger.Info("[SalesLogicExpress.Application.Helpers][DbEngine][Start:ExecuteNonQuery][Query=" + query + "][Parameters=" + parameters.SerializeToJson() + "][isStoreProcedure=" + isStoreProcedure + "]");
                SAConnection dbConnection = ResourceManager.GetOpenConnectionInstance();
                if (dbConnection == null) return -1;
                using (SACommand command = new SACommand(string.Empty, dbConnection))
                {
                    command.CommandType = isStoreProcedure ? CommandType.StoredProcedure : CommandType.Text;
                    command.CommandText = query;
                    AddParameters(parameters, command);
                    result = command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Loggger.Error("[SalesLogicExpress.Application.Helpers][DbEngine][ExecuteNonQuery][Query=" + query + "][ExceptionStackTrace = " + ex.Message == null ? ex.InnerException.Message : ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            Loggger.Info("[SalesLogicExpress.Application.Helpers][DbEngine][End:ExecuteNonQuery]");
            return result;
        }

        private static void AddParameters(Dictionary<string, object> parameters, SACommand command)
        {
            try
            {
                Loggger.Info("[SalesLogicExpress.Application.Helpers][DbEngine][Start:AddParameters][Parameters=" + parameters.SerializeToJson() + "]");
                if (parameters != null)
                {
                    foreach (KeyValuePair<string, object> param in parameters)
                    {
                        command.Parameters.AddWithValue(param.Key, param.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                Loggger.Error("[SalesLogicExpress.Application.Helpers][DbEngine][AddParameters][ExceptionStackTrace = " + ex.Message == null ? ex.InnerException.Message : ex.Message + "]");
                throw;
            }
            Loggger.Info("[SalesLogicExpress.Application.Helpers][DbEngine][End:AddParameters]");
        }
        public static string ExecuteScalar(string query)
        {
            return ExecuteScalar(query, null);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Query"></param>
        /// <param name="Parameters"></param>
        /// <param name="isStoredProcedure">true if the query input is the name of a procedure</param>
        /// <param name="ParameterType">True for in, False for out</param>
        /// <returns></returns>
        public static string ExecuteScalar(string query, Dictionary<string, object> parameters, bool isStoredProcedure, Dictionary<string, bool> parameterType)
        {
            string result = string.Empty;
            try
            {
                Loggger.Info("[SalesLogicExpress.Application.Helpers][DbEngine][Start:ExecuteScalar][Query=" + query + "][Parameters=" + parameters.SerializeToJson() + "][isStoreProcedure=" + isStoredProcedure + "][ParameterType=" + parameterType.SerializeToJson() + "]");
                SAConnection dbConnection = ResourceManager.GetOpenConnectionInstance();
                using (SACommand command = new SACommand(string.Empty, dbConnection))
                {
                    command.CommandType = isStoredProcedure ? CommandType.StoredProcedure : CommandType.Text;
                    command.CommandText = query;
                    SAParameter param;
                    if (parameters != null)
                    {
                        foreach (KeyValuePair<string, object> parameterItem in parameters)
                        {
                            param = command.CreateParameter();
                            if (parameterType.ContainsKey(parameterItem.Key))
                            {
                                param.Direction = parameterType[parameterItem.Key] ? ParameterDirection.Input : ParameterDirection.Output;
                            }
                            param.Value = parameterItem.Value;
                            param.ParameterName = parameterItem.Key;
                            command.Parameters.Add(param);
                        }
                    }
                    result = command.ExecuteScalar().ToString();
                }
            }
            catch (Exception ex)
            {
                Loggger.Error("[SalesLogicExpress.Application.Helpers][DbEngine][ExecuteScalar][Query=" + query + "][ExceptionStackTrace = " + ex.Message == null ? ex.InnerException.Message : ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            Loggger.Info("[SalesLogicExpress.Application.Helpers][DbEngine][End:ExecuteScalar]");
            return result;
        }
        public static string ExecuteScalar(string query, Dictionary<string, object> parameters)
        {
            string result = string.Empty;
            try
            {
                Loggger.Info("[SalesLogicExpress.Application.Helpers][DbEngine][Start:ExecuteScalar][Query=" + query + "][Parameters=" + parameters.SerializeToJson() + "]");
                SAConnection dbConnection = ResourceManager.GetOpenConnectionInstance();
                if (dbConnection == null) return null;
                using (SACommand command = new SACommand(string.Empty, dbConnection))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = query;
                    AddParameters(parameters, command);
                    result = Convert.ToString(command.ExecuteScalar());
                }
            }
            catch (Exception ex)
            {
                Loggger.Error("[SalesLogicExpress.Application.Helpers][DbEngine][ExecuteScalar][Query=" + query + "][ExceptionStackTrace = " + ex.Message == null ? ex.InnerException.Message : ex.Message + "]");
                throw;
            }
            Loggger.Info("[SalesLogicExpress.Application.Helpers][DbEngine][End:ExecuteScalar]");
            return result;
        }



        public async static Task<string> ExecuteScalarAsync(string query)
        {
            string result = string.Empty;
            try
            {
                
                Loggger.Info("[SalesLogicExpress.Application.Helpers][DbEngine][Start:ExecuteScalarAsync][Query=" + query + "]");
                SAConnection dbConnection = ResourceManager.GetOpenConnectionInstance();
                
                if (dbConnection == null) return null;
                using (SACommand command = new SACommand(string.Empty, dbConnection))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = query;
                    result = Convert.ToString(await command.ExecuteScalarAsync());
                }
            }
            catch (Exception ex)
            {
                Loggger.Error("[SalesLogicExpress.Application.Helpers][DbEngine][ExecuteScalarAsync][Query=" + query + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            Loggger.Info("[SalesLogicExpress.Application.Helpers][DbEngine][End:ExecuteScalarAsync]");
            return result;
        }

        public async static Task<int> ExecuteNonQueryAsync(string query, Dictionary<string, object> parameters, bool isStoreProcedure)
        {
            int result = 0;
            try
            {
                Loggger.Info("[SalesLogicExpress.Application.Helpers][DbEngine][Start:ExecuteNonQueryAsync][Query=" + query + "][Parameters=" + parameters.SerializeToJson() + "][isStoreProcedure=" + isStoreProcedure + "]");
                SAConnection dbConnection = ResourceManager.GetOpenConnectionInstance();
                if (dbConnection == null) return -1;
                using (SACommand command = new SACommand(string.Empty, dbConnection))
                {
                    command.CommandType = isStoreProcedure ? CommandType.StoredProcedure : CommandType.Text;
                    command.CommandText = query;
                    AddParameters(parameters, command);
                    result = await command.ExecuteNonQueryAsync();
                }
            }
            catch (Exception ex)
            {
                Loggger.Error("[SalesLogicExpress.Application.Helpers][DbEngine][ExecuteNonQueryAsync][Query=" + query + "][Message = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            Loggger.Info("[SalesLogicExpress.Application.Helpers][DbEngine][End:ExecuteNonQueryAsync]");
            return result;
        }

    }
}
