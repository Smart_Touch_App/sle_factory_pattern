﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Telerik.Windows.Controls;
using System.Windows;
namespace SalesLogicExpress.Application.Helpers
{
    public class AppViewModelBase : Telerik.Windows.Controls.ViewModelBase
    {
        public static List<string> AuthorizationRule = new List<string>();
        public AppViewModelBase()
        {

        }
        public class DelegateCommand : ICommand
        {
            bool _isEnabled = true, _isVisible = true, _isLocked = false;
            private readonly Predicate<object> _canExecute;
            private readonly Action<object> _execute;

            public event EventHandler CanExecuteChanged;

            public DelegateCommand(Action<object> execute)
                : this(execute, null)
            {
            }

            public DelegateCommand(Action<object> execute,
                           Predicate<object> canExecute)
            {
                _execute = execute;
                _canExecute = canExecute;
            }

            public bool CanExecute(object parameter)
            {
                if (_canExecute == null)
                {
                    if (AuthorizationRule.Contains("Save"))
                    {

                    }
                    IsVisible = false;
                    return true;
                }

                return _canExecute(parameter);
            }
            public bool IsLocked { get { return _isLocked; } set { _isLocked = value; } }
            public bool IsEnabled { get { return _isEnabled; } set { _isEnabled = value; } }
            public bool IsVisible { get { return _isVisible; } set { _isVisible = value; } }

            public void Execute(object parameter)
            {
                _execute(parameter);
            }

            public void RaiseCanExecuteChanged()
            {
                if (CanExecuteChanged != null)
                {
                    CanExecuteChanged(this, EventArgs.Empty);
                }
            }
        }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
