﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Helpers
{
    public enum StatusTypesEnum
    {
        OPEN,  // Open 
        CLSD,  // Closed
        STTLD, // Settled
        USTLD, //UnSettled,
        CMTD,  //Committed,
        VOID,  //Void,
        HOLD,  //Hold,
        INPRG, //InProgress,
        VERF,  //Verified
        REJCT,
        NEW,
        DLVRD,
        ACCEPT,
        FAIL,
        PASS,
        PHYSCL,
        WKLY,
        RECNT,
        SUBMIT,
        PNDNG,
        CMPLT,
        RTP,
        HOP,
        INPRS,
        RELS,
        INFO,
        NWARN,
        NOPTN,
        NMAND,
        VDSTLD,
        RETORD,
        SALORD
    }

    public enum ActivityKey
    {
        None,
        AppStart,
        AppExit,
        UserLogin,
        UserLogout,
        PasswordIncorrect,
        PasswordChange,
        AcceptOrderOnPreview,
        CreateOrder,
        HoldAtCashCollection,
        HoldAtDeliverToCustomer,
        HoldAtOrderEntry,
        HoldAtPick,
        OrderDelivered,
        PickComplete,
        PickItem,
        PickOrder,
        SettlementCancelled,
        SettlementSettlement,
        SettlementStarted,
        SettlementSubmitted,
        SettlementVerified,
        VoidAtCashCollection,
        VoidAtDeliverCustomer,
        VoidAtOrderEntry,
        Payment,
        PaymentVoid,
        OrderTemplate,
        NoActivity,
        PreOrder,
        Change,  //Added to record the changes
        DeliverToCustomer,
        AcceptOrderOnDelivery,
        CreateReturnOrder,
        HoldAtROEntry,
        HoldAtROPick,
        HoldAtROPrint,
        VoidAtROEntry,
        VoidAtROPick,
        OrderReturned,
        CreateCreditMemo,
        VoidCreditMemo,
        SettledCreditMemo,
        VOIDEDSETTLED,
        HeldReturn,
        CreateExpense, // expenses activity key
        VoidExpense,    // expenses activity key,
        EditExpense,
        CreateMoneyOrder,
        VoidMoneyOrder,
        EditMoneyOrder,
        Quote,
        ProspectNote,
        InvAdjustment,
        PreOrderChange,
        ItemReturn
    }

    public enum ActivityType
    {
        Transaction,
        NonTransaction,
        CreditMemo,
        UnKnown
    }

    public enum Flow
    {
        OrderLifeCycle,
        OrderReturn,
        Replenishment,
        Credits,
        PreTripInspection,
        UserSetup
    }

    public enum SignType
    {
        OrderSign,
        ChargeOnAccountSign,
        CreditProcessSign,
        ReturnOrderSign,
        PreTripInspection,
        UserSetupSign
    }

    public enum SyncPriority
    {
        High,
        Normal,
        Immediate
    }


    /// <summary>
    /// Enum lists all the available pricing set-up options 
    /// </summary>
    public enum PricingSetup
    {
        Master = 0,
        Override = 1
    }

    public enum AddressInfo
    {
        Parent = 0,
        BillTo = 1
    }

    /// <summary>
    /// Enum containing the value of the 
    /// </summary>
    public enum PricingDetailCategory
    {
        PriceProtection = 24,
        AlliedProductLineOverall = 27,
        AlliedProductLine1 = 15,
        AlliedProductLine2 = 16,
        AlliedProductLine3 = 17,
        AlliedProductLine4 = 18,
        LiquidBracket = 23,
        CoffVolume = 28,
        EquipProg = 29,
        POSUpCharge = 22,
        SpecialCCP = 30,
        TaxGroup = 11
    }
    public enum NavigatedFromTab
    {
        EOD_Transaction,
        EOD_Settlement,
        ProspectHome_Activity,
        ProspectHome_Quote,
        CustomerHome_Activity,
        CustomerHome_Quote,
        CustomerHome_DashBoard,
        ServiceRoute_Dailystop,
        ServiceRoute_Prospect,
        ServiceRoute_Customer

    }
}
