﻿using log4net;
using SalesLogicExpress.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace SalesLogicExpress.Application.Helpers
{
    public static class Utils
    {
        // Using a single instance of Random class minimizes the chances of generating repeating numbersin range
        public static Random RandomGenHelper = new Random();
        private static readonly ILog logger = LogManager.GetLogger("SalesLogicExpress.Application.Helpers.Utils");
        public static void Sort(this ObservableCollection<dynamic> observable, Func<ObservableCollection<dynamic>, List<dynamic>> action)
        {
            List<dynamic> sorted = action(observable);
            int i = 0;
            if (sorted.Count == 0)
            {
                return;
            }
            foreach (var item in sorted)
            {
                var a = observable.FirstOrDefault(x => x.ItemId == item.ItemId && x.TransactionDetailID == item.TransactionDetailID);
                int idx = observable.IndexOf(a);
                observable.Move(idx, i);
                i++;
            }

        }
        //Sathish Removed Unwanted static variables and static class
        //public static void Sort<T>(this ObservableCollection<T> observable) where T : IComparable<T>, IEquatable<T>
        //{
        //    List<T> sorted = observable.OrderBy(x => x).ToList();

        //    int ptr = 0;
        //    while (ptr < sorted.Count)
        //    {
        //        if (!observable[ptr].Equals(sorted[ptr]))
        //        {
        //            T t = observable[ptr];
        //            observable.RemoveAt(ptr);
        //            observable.Insert(sorted.IndexOf(t), t);
        //        }
        //        else
        //        {
        //            ptr++;
        //        }
        //    }
        //}
        //public static void Sort<T>(this ObservableCollection<T> observable, Func<ObservableCollection<T>, List<T>> action) where T : IComparable<T>, IEquatable<T>
        //{
        //    List<T> sorted = action(observable);//.OrderBy(x => x).ToList();

        //    int ptr = 0;
        //    while (ptr < sorted.Count)
        //    {
        //        if (!observable[ptr].Equals(sorted[ptr]))
        //        {
        //            T t = observable[ptr];
        //            observable.RemoveAt(ptr);
        //            observable.Insert(sorted.IndexOf(t), t);
        //        }
        //        else
        //        {
        //            ptr++;
        //        }
        //    }
        //}
        public static T Clone<T>(this T source)
        {
            DataContractSerializer serializer = new DataContractSerializer(typeof(T));
            using (MemoryStream ms = new MemoryStream())
            {
                serializer.WriteObject(ms, source);
                ms.Seek(0, SeekOrigin.Begin);
                return (T)serializer.ReadObject(ms);
            }
        }
        public static string SqlStringEscaping(this string str)
        {
            return str.Replace("'", "''");
        }
        /// <summary>
        /// Extension Synchronize to check whether the DataSet has data or not
        /// </summary>
        /// <param name="DataSetObject"></param>
        /// <returns>true if DataSet has Atleast one table and that table has rows in it.</returns>
        public static bool HasData(this DataSet DataSetObject)
        {
            if (!(DataSetObject == null || DataSetObject.Tables.Count == 0 || DataSetObject.Tables[0].Rows.Count == 0))
            {
                return true;
            }
            return false;

        }
        public static string GetFormattedDateTimeForDb(this DateTime datetime)
        {
            logger.Info("[SalesLogicExpress.Application.Helpers][Utils][GetFormattedDateTimeForDb][datetime=" + datetime.ToString() + "]");
            if (datetime == new DateTime())
            {
                // return " CONVERT(DATETIME,'1900-01-01 00:00:00.000',121) ";
                return " CONVERT(DATETIME,'" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "',121) ";
            }
            else
            {
                return " CONVERT(DATETIME,'" + datetime.ToString("yyyy/MM/dd HH:mm:ss") + "',121) ";
            }
        }
        public static string GetEnumDescription(this Enum enumValue)
        {
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return enumValue.ToString();
            }

        }

        public static int GetStatusIdFromDB(this Enum enumValue)
        {
            logger.Info("[SalesLogicExpress.Apllication.Helpers][Utils][Start:GetStatusTypeIdFromDB][EnumValue:" + enumValue.ToString() + "]");
            int StatusTypeId = 0;
            try
            {
                string data = string.Empty;
                if (enumValue.GetType() == typeof(StatusTypesEnum))
                {
                    data = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) FROM BUSDTA.Status_Type where StatusTypeCD='" + enumValue.ToString() + "'");
                    StatusTypeId = string.IsNullOrEmpty(data) ? 0 : Convert.ToInt32(data);
                }
                else if (enumValue.GetType() == typeof(ActivityKey))
                {
                    data = DbEngine.ExecuteScalar("SELECT ISNULL(TTID,0) FROM BUSDTA.M5001 where TTKEY='" + enumValue.ToString() + "'").Trim();
                    StatusTypeId = string.IsNullOrEmpty(data) ? 0 : Convert.ToInt32(data);
                }

            }
            catch (Exception ex)
            {
                logger.Error("[SalesLogicExpress.Apllication.Helpers][Utils][GetStatusTypeIdFromDB][EnumValue:" + enumValue.ToString() + "][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            logger.Info("[SalesLogicExpress.Apllication.Helpers][Utils][End:GetStatusTypeIdFromDB]");
            return StatusTypeId;
        }

        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> col)
        {
            return new ObservableCollection<T>(col);
        }
        //Sathish Removed Unwanted static variables and static class
        //public static string GetEnumPriority(this Enum enumValue)
        //{
        //    FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());
        //    DefaultPropertyAttribute[] attributes = (DefaultPropertyAttribute[])fi.GetCustomAttributes(typeof(DefaultPropertyAttribute), false);

        //    if (attributes != null && attributes.Length > 0)
        //    {
        //        return attributes[0].ToString();
        //    }
        //    else
        //    {
        //        return enumValue.ToString();
        //    }

        //}
        public static string SerializeToJson<T>(this T t)
        {
            string jsonString = string.Empty;
            try
            {
                MemoryStream stream = new MemoryStream();
                DataContractJsonSerializer ds = new DataContractJsonSerializer(typeof(T));
                DataContractJsonSerializerSettings s = new DataContractJsonSerializerSettings();
                ds.WriteObject(stream, t);
                jsonString = Encoding.UTF8.GetString(stream.ToArray());
                // stream.Close();
            }
            catch (Exception ex)
            {
                // Error Purposely not thrown, as this is a utility method for logging purpose, an error in this method should not cause a block in code continuation.
                logger.Error("[SalesLogicExpress.Application.Helpers][Utils][SerializeToJson][jsonString=" + jsonString + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return jsonString;
        }
        public static T DeserializeJson<T>(this string jsonString)
        {
            T obj = default(T);
            try
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
                MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
                obj = (T)ser.ReadObject(stream);
            }
            catch (Exception ex)
            {
                logger.Error("[SalesLogicExpress.Application.Helpers][Utils][DeserializeJson][jsonString=" + jsonString + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw ex;
            }
            return obj;
        }

        public static List<T> GetEntityList<T>(this DataSet ds) where T : new()
        {
            try
            {
                List<T> entityList = new List<T>();
                var castToEntity = new T();
                var properties = typeof(T).GetProperties();
                PropertyInfo[] propertiess = (castToEntity.GetType()).GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                if (ds.HasData())
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        try
                        {
                            var entity = new T();
                            foreach (System.Data.DataColumn col in row.Table.Columns)
                            {
                                var tru = properties.FirstOrDefault(i => i.Name.ToLower() == col.ColumnName.ToLower());
                                if (tru != null)
                                {
                                    if (tru.PropertyType == typeof(DateTime))
                                    {
                                        if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                            tru.SetValue(entity, Convert.ToDateTime(row[tru.Name]));
                                    }
                                    else if (tru.PropertyType == typeof(Boolean))
                                    {
                                        if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                        {
                                            // Boolean Value can be true/false or 0/1
                                            try
                                            {
                                                if (IsNumeric(row[tru.Name].ToString()) == true)
                                                    tru.SetValue(entity, Convert.ToBoolean(Convert.ToInt32(row[tru.Name].ToString())));
                                                else
                                                    tru.SetValue(entity, Convert.ToBoolean(row[tru.Name].ToString()));
                                            }
                                            catch (Exception ex)
                                            {
                                                //tru.SetValue(entity, Convert.ToBoolean(row[tru.Name].ToString()));
                                            }
                                        }
                                    }
                                    else if (tru.PropertyType == typeof(Int16))
                                    {
                                        if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                            tru.SetValue(entity, Convert.ToInt16(row[tru.Name]));
                                    }
                                    else if (tru.PropertyType == typeof(Int32))
                                    {
                                        if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                            tru.SetValue(entity, Convert.ToInt32(row[tru.Name]));
                                    }
                                    else if (tru.PropertyType == typeof(string))
                                    {
                                        if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                            tru.SetValue(entity, Convert.ToString(row[tru.Name]));
                                    }
                                    else if (tru.PropertyType == typeof(decimal))
                                    {
                                        if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                            tru.SetValue(entity, Convert.ToDecimal(row[tru.Name]));
                                    }
                                    else if (tru.PropertyType == typeof(double))
                                    {
                                        if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                            tru.SetValue(entity, Convert.ToDouble(row[tru.Name].ToString()));
                                    }
                                    else
                                    {
                                        tru.SetValue(entity, row[tru.Name]);
                                    }
                                }
                                else
                                {
                                    //Removed by Dinesh - 10/26/2016
                                    //Unnecessary columns fetching from database but not in C# model... It Increaing log file size too, so removed
                                    //Logger.Error("[SalesLogicExpress.Application.Helpers][Utils][GetEntityList][Unknown Column Property = " + col.ColumnName + "]");
                                }
                            }
                            entityList.Add(entity);
                        }
                        catch (Exception)
                        {
                            continue;
                        }
                    }
                }

                return entityList;
            }
            catch (Exception)
            {
                //Removed by Velmani Karnan - 11/08/2016
                //Unnecessary columns fetching from database but not in C# model... It Increaing log file size too large, so removed
                //Logger.Error("[SalesLogicExpress.Application.Helpers][Utils][GetEntityList][ExceptionStackTrace = " + ex.StackTrace + "]");
                return null;
            }
        }

        // Date - 10/22/2016
        // Added By Velmani - Check If ColName is numeric?
        // It will avoid type Conversion after Error thrown in Catch Block.
        // Performance Improvement - (Avoid unnecessary throw to catch block)

        public static bool IsNumeric(string ColName)
        {
            int result;
            return int.TryParse(ColName, out result);
        }

        public static List<T> GetEntityList<T>(this DataTable datatable) where T : new()
        {
            try
            {
                List<T> entityList = new List<T>();
                var castToEntity = new T();
                var properties = typeof(T).GetProperties();
                PropertyInfo[] propertiess = (castToEntity.GetType()).GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                if (datatable != null)
                {
                    foreach (DataRow row in datatable.Rows)
                    {
                        var entity = new T();
                        foreach (System.Data.DataColumn col in row.Table.Columns)
                        {
                            var tru = properties.FirstOrDefault(i => i.Name.ToLower() == col.ColumnName.ToLower());
                            if (tru != null)
                            {
                                if (tru.PropertyType == typeof(DateTime))
                                {
                                    if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                        tru.SetValue(entity, Convert.ToDateTime(row[tru.Name]));
                                }
                                else if (tru.PropertyType == typeof(Boolean))
                                {
                                    if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                    {
                                        try
                                        {
                                            tru.SetValue(entity, Convert.ToBoolean(row[tru.Name].ToString()));
                                        }
                                        catch (Exception ex)
                                        {
                                            tru.SetValue(entity, Convert.ToBoolean(Convert.ToInt32(row[tru.Name])));
                                        }
                                    }
                                }
                                else if (tru.PropertyType == typeof(Int16))
                                {
                                    if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                        tru.SetValue(entity, Convert.ToInt16(row[tru.Name]));
                                }
                                else if (tru.PropertyType == typeof(Int32))
                                {
                                    if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                        tru.SetValue(entity, Convert.ToInt32(row[tru.Name]));
                                }
                                else if (tru.PropertyType == typeof(string))
                                {
                                    if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                        tru.SetValue(entity, Convert.ToString(row[tru.Name]));
                                }
                                else if (tru.PropertyType == typeof(decimal))
                                {
                                    if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                        tru.SetValue(entity, Convert.ToDecimal(row[tru.Name]));
                                }
                                else if (tru.PropertyType == typeof(double))
                                {
                                    if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                        tru.SetValue(entity, Convert.ToDouble(row[tru.Name].ToString()));
                                }
                                else
                                {
                                    tru.SetValue(entity, row[tru.Name]);
                                }
                            }
                            else
                            {
                                //Removed by Velmani Karnan - 11/08/2016
                                //Unnecessary columns fetching from database but not in C# model... It Increaing log file size too large, so removed
                                //logger.Error("[SalesLogicExpress.Application.Helpers][Utils][GetEntityList][Unknown Column Property = " + col.ColumnName + "]");
                            }
                        }
                        entityList.Add(entity);
                    }
                }

                return entityList;
            }
            catch (Exception ex)
            {
                logger.Error("[SalesLogicExpress.Application.Helpers][Utils][GetEntityList][ExceptionStackTrace = " + ex.StackTrace + "]");
                return null;
            }
        }
        public static T GetEntity<T>(this DataRow row) where T : new()
        {
            var entity = new T();
            try
            {
                var properties = typeof(T).GetProperties();
                PropertyInfo[] propertiess = (entity.GetType()).GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                foreach (System.Data.DataColumn col in row.Table.Columns)
                {
                    try
                    {
                        var tru = properties.FirstOrDefault(i => i.Name == col.ColumnName);
                        if (tru != null)
                        {
                            if (tru.PropertyType == typeof(DateTime))
                            {
                                if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                    tru.SetValue(entity, Convert.ToDateTime(row[tru.Name]));
                            }
                            else if (tru.PropertyType == typeof(Boolean))
                            {
                                tru.SetValue(entity, Convert.ToBoolean(row[tru.Name]));
                            }
                            else if (tru.PropertyType == typeof(Int16))
                            {
                                tru.SetValue(entity, Convert.ToInt16(row[tru.Name]));
                            }
                            else if (tru.PropertyType == typeof(Int32))
                            {
                                tru.SetValue(entity, Convert.ToInt32(row[tru.Name]));
                            }
                            else if (tru.PropertyType == typeof(string))
                            {
                                tru.SetValue(entity, Convert.ToString(row[tru.Name]));
                            }
                            else if (tru.PropertyType == typeof(decimal))
                            {
                                tru.SetValue(entity, Convert.ToDecimal(row[tru.Name]));
                            }
                            else
                            {
                                tru.SetValue(entity, row[tru.Name].ToString());
                            }
                        }
                        else
                        {
                            //Removed by Velmani Karnan - 01/11/2017
                            //Unnecessary columns fetching from database but not in C# model... It Increaing log file size too large, so removed
                            //logger.Error("[SalesLogicExpress.Application.Helpers][Utils][GetEntity][Unknown Column Property = " + col.ColumnName + "]");
                        }
                    }
                    catch (Exception ex)
                    {
                        //Removed by Velmani Karnan - 01/11/2017
                        // Unnecessary columns fetching from database but not in C# model... It Increaing log file size too large, so removed
                        // logger.Error("[SalesLogicExpress.Application.Helpers][Utils][GetEntity][ExceptionStackTrace = " + ex.StackTrace + "]");
                        continue;
                    }
                }
                return entity;
            }
            catch (Exception ex)
            {
                logger.Error("[SalesLogicExpress.Application.Helpers][Utils][GetEntity][ExceptionStackTrace = " + ex.StackTrace + "]");
                return entity;
            }
        }
        public static string ToTitleCase(this string str)
        {
            var cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
            return cultureInfo.TextInfo.ToTitleCase(str.ToLower());
        }
        public static T ParseEnum<T>(this string value, T defaultValue)
        {
            if (!Enum.IsDefined(typeof(T), value))
                return defaultValue;
            try
            {
                return (T)Enum.Parse(typeof(T), value, true);
            }
            catch (Exception ex)
            {
                logger.Error("[SalesLogicExpress.Application.Helpers][Utils][ParseEnum][ExceptionStackTrace = " + ex.StackTrace + "]");
                return defaultValue;
            }
        }
        public static T ParseEnumFromDescription<T>(this string description)
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();
            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field,
                    typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attribute != null)
                {
                    if (attribute.Description == description)
                        return (T)field.GetValue(null);
                }
                else
                {
                    if (field.Name == description)
                        return (T)field.GetValue(null);
                }
            }
            throw new ArgumentException("Not found.", "description");
            // or return default(T);
        }
        public static T ParseEnum<T>(this string value)
        {
            try
            {
                return (T)Enum.Parse(typeof(T), value, true);
            }
            catch (Exception ex)
            {
                logger.Error("[SalesLogicExpress.Application.Helpers][Utils][ParseEnum][ExceptionStackTrace = " + ex.StackTrace + "]");
                return default(T);
            }
        }
        public static DateTime StringToUSDateFormat(this string strdate)
        {
            DateTime dateConverted;
            try
            {
                dateConverted = DateTime.Parse(strdate, CultureInfo.CreateSpecificCulture("en-US"));
            }
            catch (Exception ex)
            {
                logger.Error("[SalesLogicExpress.Application.Helpers][Utils][StringToUSDateFormat][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            return dateConverted;
        }
        public static string GetPrettyDate(this DateTime d)
        {
            // 1.
            // Get time span elapsed since the date.
            TimeSpan s = DateTime.Now.Subtract(d);

            // 2.
            // Get total number of days elapsed.
            int dayDiff = (int)s.TotalDays;

            // 3.
            // Get total number of seconds elapsed.
            int secDiff = (int)s.TotalSeconds;

            // 4.
            // Don't allow out of range values.
            if (dayDiff < 0 || dayDiff >= 31)
            {
                return null;
            }

            // 5.
            // Handle same-day times.
            if (dayDiff == 0)
            {
                // A.
                // Less than one minute ago.
                if (secDiff < 60)
                {
                    return "just now";
                }
                // B.
                // Less than 2 minutes ago.
                if (secDiff < 120)
                {
                    return "1 minute ago";
                }
                // C.
                // Less than one hour ago.
                if (secDiff < 3600)
                {
                    return string.Format("{0} minutes ago",
                        Math.Floor((double)secDiff / 60));
                }
                // D.
                // Less than 2 hours ago.
                if (secDiff < 7200)
                {
                    return "1 hour ago";
                }
                // E.
                // Less than one day ago.
                if (secDiff < 86400)
                {
                    return string.Format("{0} hours ago",
                        Math.Floor((double)secDiff / 3600));
                }
            }
            // 6.
            // Handle previous days.
            if (dayDiff == 1)
            {
                return "yesterday";
            }
            if (dayDiff < 7)
            {
                return string.Format("{0} days ago",
                dayDiff);
            }
            if (dayDiff < 31)
            {
                return string.Format("{0} weeks ago",
                Math.Ceiling((double)dayDiff / 7));
            }
            return null;
        }
        //Sathish Removed Unwanted static variables and static class
        //public static T DeepClone<T>(this T source)
        //{
        //    // Don't serialize a null object, simply return the default for that object
        //    if (Object.ReferenceEquals(source, null))
        //    {
        //        return default(T);
        //    }
        //    return source.SerializeToJson().ToString().DeserializeJson<T>();
        //}

        //public static string GetDescription(this ObservableCollection<PriceDetails> collection, string key)
        //{
        //    try
        //    {
        //        if (collection.Count <= 0)
        //        {
        //            return string.Empty;
        //        }
        //        var priceDetail = collection.Where(o => o.Key.Trim().Equals((string.IsNullOrEmpty(key) ? string.Empty : key.Trim())));
        //        if (priceDetail.Any())
        //        {
        //            string desc = priceDetail.First<PriceDetails>().Description;

        //            return (string.IsNullOrEmpty(desc) ? "" : desc);
        //        }
        //        else
        //        {
        //            return string.Empty;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return string.Empty;
        //    }
        //}
    }

}
