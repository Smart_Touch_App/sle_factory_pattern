﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Sap.Data.SQLAnywhere;
using log4net;


namespace SalesLogicExpress.Application.Helpers
{
    public class DB_CRUD
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Helpers.DB_CRUD");
        SAConnection connection;
        public DB_CRUD()
        {
            try
            {
                connection = ResourceManager.GetOpenConnectionInstance();
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][Constructor][ExceptionStackTrace = " + ex.StackTrace + "][ExceptionMessage = " + ex.Message + "]");
                throw ex;
            }
        }

        ~DB_CRUD()
        {
            try
            {
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][Destructor][ExceptionStackTrace = " + ex.StackTrace + "][ExceptionMessage = " + ex.Message + "]");
                throw ex;
            }
        }

        public Boolean checkValidity(SACommand CommandToExecute)
        {
            bool result = false;
            try
            {
                SADataReader reader = null;
                reader = CommandToExecute.ExecuteReader();
                result = reader.Read();
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][checkValidity][Query = " + CommandToExecute.CommandText + "][ExceptionStackTrace = " + ex.StackTrace + "][ExceptionMessage = " + ex.Message + "]");
            }
            return result;
        }

        public void CloseConnection()
        {
            try
            {
                connection.Close();
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][CloseConnection][ExceptionStackTrace = " + ex.StackTrace + "][ExceptionMessage = " + ex.Message + "]");
            }
        }

        public SADataReader ExecuteReader(SACommand CommandToExecute)
        {
            SADataReader reader = null;
            try
            {
                CommandToExecute.Connection = connection;
                reader = CommandToExecute.ExecuteReader();
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][ExecuteReader][Query = " + CommandToExecute.CommandText + "][ExceptionStackTrace = " + ex.StackTrace + "][ExceptionMessage = " + ex.Message + "]");
            }
            return reader;
        }

        public object ExecuteScalar(SACommand CommandToExecute)
        {
            try
            {
                CommandToExecute.Connection = connection;
                return CommandToExecute.ExecuteScalar();
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][ExecuteScalar][Query = " + CommandToExecute.CommandText + "][ExceptionStackTrace = " + ex.StackTrace + "][ExceptionMessage = " + ex.Message + "]");
            }
            return string.Empty;
        }

        public SAConnection GetActiveConnection()
        {
            return connection;
        }

        public DataTable GetDataIntoDt(SACommand CommandToExecute)
        {
            DataTable resultData = new DataTable();
            try
            {
                CommandToExecute.Connection = connection;
                SADataAdapter da = new SADataAdapter(CommandToExecute);
                da.Fill(resultData);
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][GetDataIntoDt][Query = " + CommandToExecute.CommandText + "][ExceptionStackTrace = " + ex.StackTrace + "][ExceptionMessage = " + ex.Message + "]");
            }
            return resultData;
        }

     
        public int InsertUpdateData(SACommand CommandToExecute)
        {
            try
            {
                CommandToExecute.Connection = connection;
                int inserted = CommandToExecute.ExecuteNonQuery();
                return inserted;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][DB_CRUD][Insertdata][Query = " + CommandToExecute.CommandText + "][ExceptionStackTrace = " + ex.StackTrace + "][ExceptionMessage = " + ex.Message + "]");
            }
            return -1;
        }
    }
}
