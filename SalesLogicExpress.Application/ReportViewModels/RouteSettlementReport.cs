﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Managers = SalesLogicExpress.Application.Managers;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.ViewModels;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using Telerik.Reporting;
using SalesLogicExpress.Reporting;
using SalesLogicExpress.Application.Helpers;
using System.Configuration;
using log4net;
using System.Data;
using SalesLogicExpress.Domain;
using System.Collections.ObjectModel;
using SalesLogicExpress.Application.Managers;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class RouteSettlementReport : ViewModels.BaseViewModel,ReportEngine.IReportSLE
    {
        #region Variables And Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.RouteSettlementReport]");
        InstanceReportSource reportsource;
        public InstanceReportSource ReportSource
        {
            get
            {
                return reportsource;
            }
            set
            {
                reportsource = value;
                OnPropertyChanged("ReportSource");
            }
        }
        public string BranchNo { get; set; }
        public string RouteNo { get; set; }
        public string SettlementFromDate { get; set; }
        public string SettlementToDate { get; set; }
        public string CoffeePoundsSold { get; set; }
        public string CoffeeTotalAmt { get; set; }
        public string TotalPaymentsOnAccount { get; set; }
        public string AlliedTotalAmt { get; set; }
        public string AlliedPoundsSold { get; set; }
        public string NetCashSales { get; set; }
        public string TotalCashReceived { get; set; }
        public string Expenses { get; set; }
        public string MoneyOrder { get; set; }
        public string Checks { get; set; }
        public string Verifier { get; set; }
        public string NSV { get; set; }
        public string BankDeposit { get; set; }
        public string SampleCoffeePoundsSold { get; set; }
        public string SampleAlliedPoundsSold { get; set; }
        public List<Models.RouteSettlementCheckInDetail> Items { get; set; }
        #endregion

        #region Constructor
        public RouteSettlementReport(RouteSettlementSettlementsModel routeSettlementSettlement, bool ShowPreview = false)
        {
            try
            {
                BranchNo = CommonNavInfo.UserBranch.Contains('.') ? CommonNavInfo.UserBranch.Substring(0, CommonNavInfo.UserBranch.LastIndexOf(".")) : CommonNavInfo.UserBranch;
                RouteNo = CommonNavInfo.UserRoute;
                DateTime? SettlementStartDate = RouteSettlementManager.GetTransactionBeginningDateForSettlement(routeSettlementSettlement.SettlementID);
                if (SettlementStartDate != null)
                    SettlementFromDate = Convert.ToDateTime(SettlementStartDate).ToShortDateString();
                SettlementToDate = routeSettlementSettlement.Date.ToShortDateString();
                CoffeePoundsSold = RouteSettlementManager.GetCoffeePoundsSoldForSettlement(routeSettlementSettlement.SettlementID);
                AlliedPoundsSold = RouteSettlementManager.GetAlliedPoundsSoldForSettlement(routeSettlementSettlement.SettlementID);

                NetCashSales = "$" + RouteSettlementManager.GetNetCashSalesForSettlement(routeSettlementSettlement.SettlementID);
                NetCashSales = NetCashSales.Contains('.') ? NetCashSales.Substring(0, NetCashSales.LastIndexOf(".") + 3) : NetCashSales;
                NSV = NetCashSales;// Need to Change = NetCashSales + Charge Sales
                Verifier = routeSettlementSettlement.VerifierName;
                DataSet OrderHeaderWithStatusTypeDS = RouteSettlementManager.GetOrderHeaderWithStatusType(routeSettlementSettlement.SettlementID);
                DataSet OrderHeaderDS = RouteSettlementManager.GetOrderHeaderForSettlement(routeSettlementSettlement.SettlementID);
                if (OrderHeaderDS.HasData())
                {

                    //AlliedTotalAmt = "$" + Convert.ToString(OrderHeaderDS.Tables[0].Compute("Sum(TotalAlliedAmt)", ""));
                    string AlliedTotalAmtval = Math.Round(Convert.ToDecimal(OrderHeaderDS.Tables[0].Compute("Sum(TotalAlliedAmt)", "")), 2, MidpointRounding.AwayFromZero).ToString();
                    AlliedTotalAmt = "$" + AlliedTotalAmtval;
                    AlliedTotalAmt = AlliedTotalAmt.Contains('.') ? AlliedTotalAmt.Substring(0, AlliedTotalAmt.LastIndexOf(".") + 3) : AlliedTotalAmt;
                }
                if (OrderHeaderDS.HasData())
                {
                    //CoffeeTotalAmt = "$" + Convert.ToString(OrderHeaderDS.Tables[0].Compute("Sum(TotalCoffeeAmt)", ""));
                    string CoffeeTotalAmtval = Math.Round(Convert.ToDecimal(OrderHeaderDS.Tables[0].Compute("Sum(TotalCoffeeAmt)", "")), 2, MidpointRounding.AwayFromZero).ToString();
                    CoffeeTotalAmt = "$" + CoffeeTotalAmtval;
                    CoffeeTotalAmt = CoffeeTotalAmt.Contains('.') ? CoffeeTotalAmt.Substring(0, CoffeeTotalAmt.LastIndexOf(".") + 2) : CoffeeTotalAmt;
                }

                string samCoffeeSold = RouteSettlementManager.GetSampleCoffeePoundsSoldForSettlement(routeSettlementSettlement.SettlementID);
                if (samCoffeeSold != "")
                {
                    SampleCoffeePoundsSold = samCoffeeSold;
                    SampleCoffeePoundsSold = SampleCoffeePoundsSold.Contains('.') ? SampleCoffeePoundsSold.Substring(0, SampleCoffeePoundsSold.LastIndexOf(".")) + " lbs" : SampleCoffeePoundsSold;
                }
                string samAlliedSold = RouteSettlementManager.GetSampleAlliedPoundsSoldForSettlement(routeSettlementSettlement.SettlementID);
                if (samAlliedSold != "")
                {
                    SampleAlliedPoundsSold = samAlliedSold;
                    SampleAlliedPoundsSold = SampleAlliedPoundsSold.Contains('.') ? SampleAlliedPoundsSold.Substring(0, SampleAlliedPoundsSold.LastIndexOf(".")) : SampleAlliedPoundsSold;
                }

                DataSet RouteSettlementDS = RouteSettlementManager.GetRouteSettlement(routeSettlementSettlement.SettlementID);
                if (RouteSettlementDS.HasData())
                {
                    TotalPaymentsOnAccount = "$" + Convert.ToString(RouteSettlementDS.Tables[0].Compute("Sum(Payments)", ""));
                    TotalPaymentsOnAccount = TotalPaymentsOnAccount.Contains('.') ? TotalPaymentsOnAccount.Substring(0, TotalPaymentsOnAccount.LastIndexOf(".") + 3) : TotalPaymentsOnAccount;

                    TotalCashReceived = "$" + Convert.ToString(RouteSettlementDS.Tables[0].Compute("Sum(CashAmount)", ""));
                    TotalCashReceived = TotalCashReceived.Contains('.') ? TotalCashReceived.Substring(0, TotalCashReceived.LastIndexOf(".") + 3) : TotalCashReceived;

                    Expenses = "$" + Convert.ToString(RouteSettlementDS.Tables[0].Compute("Sum(Expenses)", ""));
                    Expenses = Expenses.Contains('.') ? Expenses.Substring(0, Expenses.LastIndexOf(".") + 3) : Expenses;

                    MoneyOrder = "$" + Convert.ToString(RouteSettlementDS.Tables[0].Compute("Sum(MoneyOrderAmount)", ""));
                    MoneyOrder = MoneyOrder.Contains('.') ? MoneyOrder.Substring(0, MoneyOrder.LastIndexOf(".") + 3) : MoneyOrder;

                    Checks = "$" + Convert.ToString(RouteSettlementDS.Tables[0].Compute("Sum(CheckAmount)", ""));
                    Checks = Checks.Contains('.') ? Checks.Substring(0, Checks.LastIndexOf(".") + 3) : Checks;

                    double bnkDepositAmount = Convert.ToDouble(RouteSettlementDS.Tables[0].Compute("Sum(CashAmount)", "")) 
                        + Convert.ToDouble(RouteSettlementDS.Tables[0].Compute("Sum(MoneyOrderAmount)", "")) 
                        + Convert.ToDouble(RouteSettlementDS.Tables[0].Compute("Sum(CheckAmount)", ""));
                    BankDeposit = "$" + Convert.ToString(bnkDepositAmount);

                    //BankDeposit = "$" + Convert.ToString(Convert.ToDouble(RouteSettlementDS.Tables[0].Compute("Sum(CashAmount)", "")) + Convert.ToDouble(RouteSettlementDS.Tables[0].Compute("Sum(MoneyOrderAmount)", "")) + Convert.ToDouble(RouteSettlementDS.Tables[0].Compute("Sum(CheckAmount)", "")));                    
                    //BankDeposit = BankDeposit.Contains('.') ? BankDeposit.Substring(0, BankDeposit.LastIndexOf(".") + 3) : BankDeposit;
                }

                Items = new List<RouteSettlementCheckInDetail>();
                if (OrderHeaderWithStatusTypeDS.HasData())
                {
                    RouteSettlementCheckInDetail rscd = new RouteSettlementCheckInDetail();
                    foreach (DataRow OrderHeaderWithStatusTypeDR in OrderHeaderWithStatusTypeDS.Tables[0].Rows)
                    {
                        rscd = new RouteSettlementCheckInDetail();
                        rscd.OrderNumber = Convert.ToString(OrderHeaderWithStatusTypeDR["OrderID"]);
                        rscd.OrderType = Convert.ToString(OrderHeaderWithStatusTypeDR["StatusTypeDESC"]);

                        rscd.OrderTotalAmt = "$" + Convert.ToString(OrderHeaderWithStatusTypeDR["OrderTotalAmt"]);
                        rscd.OrderTotalAmt = rscd.OrderTotalAmt.Contains('.') ? rscd.OrderTotalAmt.Substring(0, rscd.OrderTotalAmt.LastIndexOf(".") + 3) : rscd.OrderTotalAmt;

                        rscd.SalesTaxAmt = "$" + Convert.ToString(OrderHeaderWithStatusTypeDR["SalesTaxAmt"]);
                        rscd.SalesTaxAmt = rscd.SalesTaxAmt.Contains('.') ? rscd.SalesTaxAmt.Substring(0, rscd.SalesTaxAmt.LastIndexOf(".") + 3) : rscd.SalesTaxAmt;

                        rscd.InvoiceTotalAmt = "$" + Convert.ToString(OrderHeaderWithStatusTypeDR["InvoiceTotalAmt"]);
                        rscd.InvoiceTotalAmt = rscd.InvoiceTotalAmt.Contains('.') ? rscd.InvoiceTotalAmt.Substring(0, rscd.InvoiceTotalAmt.LastIndexOf(".") + 3) : rscd.InvoiceTotalAmt;

                        rscd.SurchargeAmt = "$" + Convert.ToString(OrderHeaderWithStatusTypeDR["EnergySurchargeAmt"]);
                        rscd.SurchargeAmt = rscd.SurchargeAmt.Contains('.') ? rscd.SurchargeAmt.Substring(0, rscd.SurchargeAmt.LastIndexOf(".") + 3) : rscd.SurchargeAmt;

                        rscd.DeliverySeq = Convert.ToString(OrderHeaderWithStatusTypeDR["RPSN"]);
                        rscd.PaymentTerm = OrderHeaderWithStatusTypeDR["PNPTD"].ToString();
                        //on ship to column remove the address 
                        //string shipTo = Convert.ToString(OrderHeaderWithStatusTypeDR["abalph"]).Trim() + "\n\n" + Convert.ToString(OrderHeaderWithStatusTypeDR["ALaDD1"]).Trim() + Convert.ToString(OrderHeaderWithStatusTypeDR["ALaDD2"]).Trim() + ",\n" + Convert.ToString(OrderHeaderWithStatusTypeDR["ALCTY1"]).Trim() + ",\n" + Convert.ToString(OrderHeaderWithStatusTypeDR["aladds"]).Trim() + "\n\n" + Convert.ToString(OrderHeaderWithStatusTypeDR["Phone"]);
                        string shipTo = Convert.ToString(OrderHeaderWithStatusTypeDR["abalph"]).Trim();
                        rscd.ShipTo = shipTo;

                        string BillTo = Convert.ToString(OrderHeaderWithStatusTypeDR["abalph2"]).Trim() + "\n\n" + Convert.ToString(OrderHeaderWithStatusTypeDR["ALaDD12"]).Trim() + Convert.ToString(OrderHeaderWithStatusTypeDR["ALaDD22"]).Trim() + ",\n" + Convert.ToString(OrderHeaderWithStatusTypeDR["ALCTY12"]).Trim() + ",\n" + Convert.ToString(OrderHeaderWithStatusTypeDR["aladds2"]).Trim() + "\n\n" + Convert.ToString(OrderHeaderWithStatusTypeDR["Phone2"]);
                        rscd.BillTo = BillTo;

                        rscd.BillToID = OrderHeaderWithStatusTypeDR["CustBillToId"].ToString();
                        rscd.ShipToID = OrderHeaderWithStatusTypeDR["CustShipToId"].ToString();
                        rscd.OrderDate = Convert.ToDateTime(OrderHeaderWithStatusTypeDR["OrderDate"].ToString()).ToString("MM/dd/yyyy");
                        rscd.PaymentTime = OrderHeaderWithStatusTypeDR["CreatedDatetime"].ToString();
                        Items.Add(rscd);
                    }
                }

                ReportSource = new ReportViewModel(this, Reports.RouteSettlementReport, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
                AppUserId = SalesLogicExpress.Application.Managers.UserManager.UserId;
                MachineName = ResourceManager.DeviceID;
                ReportName = "Route Settlement Report";
                ContentType = ReportEngine.ReportCategory.Route;
                RouteInfo = new ReportEngine.RouteInformation() { RouterMasterId = CommonNavInfo.RouteID };
                Parameters = new Dictionary<string, object>();
                Parameters.Add("InvoiceObj", Items);
                Connection = ResourceManager.GetOpenConnectionInstance();
                //if (!ShowPreview)
                //{
                //    System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                //    new Telerik.Reporting.Processing.ReportProcessor().PrintReport(ReportSource, printerSettings);
                //}
            }
            catch (Exception ex)
            {                
                log.Error("RouteSettlementReport RouteSettlementReport( ) Error: " + ex.Message);
            }
        }
        #endregion
        public int AppUserId { get; set; }
        public string MachineName { get; set; }
        public string ReportName { get; set; }

        public ReportEngine.ReportCategory ContentType { get; set; }

        public ReportEngine.CustomerInformation CustomerInfo { get; set; }

        public ReportEngine.RouteInformation RouteInfo { get; set; }

        public Dictionary<string, object> Parameters { get; set; }

        [NonSerialized]
        private Sap.Data.SQLAnywhere.SAConnection _Connection;


        public Sap.Data.SQLAnywhere.SAConnection Connection { get { return _Connection; } set { _Connection = value; } }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
