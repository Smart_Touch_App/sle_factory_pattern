﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Managers = SalesLogicExpress.Application.Managers;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.ViewModels;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using SalesLogicExpress.Reporting;
using Telerik.Reporting;
using System.Configuration;
using System.ComponentModel;
using System.Windows.Data;
using log4net;
using SalesLogicExpress.Application.Helpers;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class PickSlipReport : PickOrder, ReportEngine.IReportSLE
    {
        #region Variables & Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.PickSlipReport]");
        InstanceReportSource reportsource;
        public InstanceReportSource ReportSource
        {
            get
            {
                return reportsource;
            }
            set
            {
                reportsource = value;
                OnPropertyChanged("ReportSource");
            }
        }
        public string Billing { get; set; }
        public string Route { get; set; }
        public string OrderDate { get; set; }
        public string BillToName { get; set; }
        public String CustomerPo { get; set; }

        public List<Models.PickOrderItem> Items { get; set; }
        #endregion

        #region Constructor
        public PickSlipReport()
        {
            try
            {
                log.Info("[Start:PickSlipReport]");
                Customer = CommonNavInfo.Customer;
                Route = Customer.Route.Length > 3 ? Customer.Route.Substring(3) : Customer.Route;
                Billing = new Managers.OrderManager().GetSoldTo(Convert.ToInt32(Customer.CustomerNo));
                CustomerPo = "PO: ";
                if (Billing.Split(',')[0].ToString() != Customer.customerNo)
                {
                    BillToName = CommonNavInfo.Customer.Name + " / " + Billing.Split(',')[1].ToString(); 
                }
                else
                {
                    BillToName = CommonNavInfo.Customer.Name;
                }
                if (Billing == "")
                {
                    Billing = Customer.CustomerNo;
                }
                else
                {
                    if (Billing.Contains(","))
                    {
                        Billing = Billing.Split(',')[0];
                    }
                }

                if (Items != null)
                    Items.Clear();


                Items = PayloadManager.OrderPayload.PickOrderItemsForPrint;


                OrderDate = DateTime.Today.ToString("MM'/'dd'/'yyyy");
                //TotalPickedQty = Items.Sum(s => s.PickedQuantityInPrimaryUOM).ToString();
                ReportSource = new ReportViewModel(this, Reports.PickSlipReport, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
                AppUserId = SalesLogicExpress.Application.Managers.UserManager.UserId;
                MachineName = ResourceManager.DeviceID;
                ReportName = "Pick Slip Report";
                ContentType = ReportEngine.ReportCategory.Route;
                // RERouteInfo = new ReportEngine.RouteInformation() { RouterMasterId = 1 };
                CustomerInfo = new ReportEngine.CustomerInformation() { CustomerShipTo = Convert.ToInt32(CommonNavInfo.Customer.customerNo) };
                Parameters = new Dictionary<string, object>();
                Parameters.Add("PickSlipObj", Items);
                Connection = ResourceManager.GetOpenConnectionInstance();
            }
            catch (Exception ex)
            {
                log.Error("[PickSlipReport][ExceptionStackTrace=" + ex.Message + "]");
            }
            log.Info("[End:PickSlipReport]");
        }
        #endregion
        public int AppUserId { get; set; }
        public string MachineName { get; set; }
        public string ReportName { get; set; }

        public ReportEngine.ReportCategory ContentType { get; set; }

        public ReportEngine.CustomerInformation CustomerInfo { get; set; }

        public ReportEngine.RouteInformation RouteInfo { get; set; }

        public Dictionary<string, object> Parameters { get; set; }

        [NonSerialized]
        private Sap.Data.SQLAnywhere.SAConnection _Connection;


        public Sap.Data.SQLAnywhere.SAConnection Connection { get { return _Connection; } set { _Connection = value; } }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
