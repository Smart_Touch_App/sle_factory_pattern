﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Reporting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.ReportViewModels
{
    class HeldReturnPickReport : CommanInReplenishmentReport, ReportEngine.IReportSLE
    {
        #region Variables & Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.HeldReturnPickReport]");
        #endregion

        public HeldReturnPickReport(IList items)
        {
            try
            {
                log.Info("[Start:HeldReturnPickReport]");
                ReportTitle = "HELD RETURN-PICK FINAL";
                Status = "IN PICKING";
                //UserName = CommonNavInfo.UserName;
                UserName = CommonNavInfo.DisplayUserName;
                RouteNoAndName = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.RouteNameAddLoad;
                ReportDate = DateTime.Today.ToString("MM'/'dd'/'yyyy");
                TransferNo = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.SelectedReplnId;
                TransferFrom = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.FromBranch;
                TransferTo = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.ToBranch.ToString().Trim();
                FromDate = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.FromDate.ToString().Trim();
                ToDate = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.ToDate.ToString().Trim();
                Items = items;
                ReportSource = new ReportViewModel(this, Reports.HeldReturnPickReport, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
                AppUserId = SalesLogicExpress.Application.Managers.UserManager.UserId;
                MachineName = ResourceManager.DeviceID;
                ReportName = "Held Return Pick Report";
                ContentType = ReportEngine.ReportCategory.Route;
                // RERouteInfo = new ReportEngine.RouteInformation() { RouterMasterId = 1 };
                RouteInfo = new ReportEngine.RouteInformation() { RouterMasterId = CommonNavInfo.RouteID };
                Parameters = new Dictionary<string, object>();
                Parameters.Add("HeldPickObj", items);
                Connection = ResourceManager.GetOpenConnectionInstance();
                // System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                // Telerik.Reporting.Processing.ReportProcessor telerikReportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
                //telerikReportProcessor.PrintController.OnEndPrint((PrintDocument)reportsource.ReportDocument, new PrintEventArgs());
                //  telerikReportProcessor.PrintReport(ReportSource, printerSettings);
            }
            catch (Exception ex)
            {
                log.Error("[HeldReturnPickReport][ExceptionStackTrace=" + ex.Message + "]");
                
            }
            log.Info("[End:HeldReturnPickReport]");
        }
        public int AppUserId { get; set; }
        public string MachineName { get; set; }
        public string ReportName { get; set; }

        public ReportEngine.ReportCategory ContentType { get; set; }

        public ReportEngine.CustomerInformation CustomerInfo { get; set; }

        public ReportEngine.RouteInformation RouteInfo { get; set; }

        public Dictionary<string, object> Parameters { get; set; }

        [NonSerialized]
        private Sap.Data.SQLAnywhere.SAConnection _Connection;


        public Sap.Data.SQLAnywhere.SAConnection Connection { get { return _Connection; } set { _Connection = value; } }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
