﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Managers = SalesLogicExpress.Application.Managers;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.ViewModels;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using Telerik.Reporting;
using SalesLogicExpress.Reporting;
using SalesLogicExpress.Application.Helpers;
using System.Configuration;
using log4net;
using System.Data;
using System.Collections.ObjectModel;
using SalesLogicExpress.Application.Managers;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class InvoiceReport : ViewModels.BaseViewModel, ReportEngine.IReportSLE
    {
        #region Variables And Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.InvoiceReport]");
        InstanceReportSource reportsource;
        private ObservableCollection<Models.UserProfile> _UserProfileInfo = null;

        public InstanceReportSource ReportSource
        {
            get
            {
                return reportsource;
            }
            set
            {
                reportsource = value;
                OnPropertyChanged("ReportSource");
            }
        }



        //public Guid MessageToken { get; set; }
        public ObservableCollection<Models.UserProfile> UserProfileInfo
        {
            get
            {
                return _UserProfileInfo;
            }
            set
            {
                _UserProfileInfo = value;
                OnPropertyChanged("UserProfileInfo");
            }
        }

        public Models.Customer Customer { get; set; }
        public string OrderNumber { get; set; }

        public string Billing { get; set; }
        public string BillToName { get; set; }
        public String CustomerPo { get; set; }
        public string Route { get; set; }
        public string VisitUsUrl { get; set; }
        public string RemitToName { get; set; }
        public string RemitToPOBox { get; set; }
        public string RemitToCityZip { get; set; }
        public string TaxAmount { get; set; }
        public string EnergySurcharge { get; set; }
        public string CallForService { get; set; }
        public string OrderDate { get; set; }
        public string TermOfSales { get; set; }
        public string RouteSignImagePath { get; set; }
        public string CustomerSignImagePath { get; set; }
        private string _TotalAllied;
        public string TotalAllied
        {
            get
            {
                return _TotalAllied;
            }
            set
            {
                _TotalAllied = value;
                OnPropertyChanged("TotalAllied");
            }

        }
        public string TotalCoffee { get; set; }
        public string TotalDue { get; set; }
        public string LogoUrl { get; set; }
        public string PriceOverrideFlag { get; set; }
        public string DisplayName { get; set; }

        public List<Models.OrderItem> Items { get; set; }
        SalesLogicExpress.Application.ViewModels.DeliveryScreen viewmodel;
        #endregion
        #region Constructor
        public InvoiceReport(string orderNumber, bool ShowPreview = false)
        {
            try
            {
                viewmodel = new SalesLogicExpress.Application.ViewModels.DeliveryScreen();
                //Issue ID- 104 - Fixed by Zakir - Didnt Provide the Customer PO # and Signature in Sign and Print Screen then Pick the items and navigated to Delivery screen.Provide the Customer Signature and customer PO# in Delivery screen then delivert the SO to customer . Then printed the invoice Customer Signature updated correctly but Customer PO# is not updated in invoice print
                string _customerPO = viewmodel.CustomerPO;
                OrderNumber = orderNumber;

                DataTable orderInfoData = new Managers.OrderManager().GetOrderInfo(OrderNumber);
                this.UserProfileInfo = UserManager.GetUserProfile(CommonNavInfo.RouteID);
                int appUserID = 0;
                if (UserProfileInfo != null && UserProfileInfo.Count > 0)
                {
                    DisplayName = UserProfileInfo.FirstOrDefault().DisplayName;
                    appUserID = UserProfileInfo.FirstOrDefault().AppUserID;
                    CallForService = UserProfileInfo.FirstOrDefault().PhoneNumber;
                }
                RouteSignImagePath = Managers.UserManager.GetUserSignature(appUserID);
                bool IsRouteSignExist = string.IsNullOrEmpty(RouteSignImagePath) ? false : true;
                if (IsRouteSignExist)
                {
                    new CanvasViewModel(Flow.UserSetup, SignType.UserSetupSign, appUserID).UpdateSignatureToDB(false, false, true, "route.jpg");
                }

                //RouteSignImagePath = IsRouteSignExist ? ResourceManager.DomainPath + @"\Docs" + "\\" + "route.jpg" : ResourceManager.DomainPath + @"\Resources\Images\blank.jpg";

                if (IsRouteSignExist)
                {
                    RouteSignImagePath = ResourceManager.DomainPath + @"\Docs" + "\\" + "route.jpg";
                }
                else
                {
                    //if (File.Exists("blank.jpg"))
                    if (File.Exists(ResourceManager.DomainPath + @"\Resources\Images\blank.jpg"))
                    {
                        RouteSignImagePath = ResourceManager.DomainPath + @"\Resources\Images\blank.jpg";
                    }
                }



                Items = orderInfoData.GetEntityList<Models.OrderItem>(); ;
                OrderDate = Convert.ToDateTime(Items.FirstOrDefault().OrderDate).ToString("MM-dd-yyyy");
                EnergySurcharge = Convert.ToDecimal(orderInfoData.Rows[0]["EnergySurchargeAmt"].ToString()).ToString("0.00");
                TaxAmount = Convert.ToDecimal(orderInfoData.Rows[0]["SalesTaxAmt"].ToString()).ToString("0.##");
                //PriceOverrideFlag = Convert.ToString(orderInfoData.Rows[0]["PriceOverrideFlag"]);
                bool IsSignPresent = string.IsNullOrEmpty(orderInfoData.Rows[0]["ordersignature"].ToString()) ? false : true;

                if (IsSignPresent)
                {
                    //Draw sign on Image 
                    new CanvasViewModel(Flow.OrderLifeCycle, SignType.OrderSign, Convert.ToInt32(OrderNumber)).UpdateSignatureToDB(false, false, true, "test.jpg");
                }

                log.Info("[Start:InvoiceReport][IsSignPresent:" + IsSignPresent + "]");
                Customer = CommonNavInfo.Customer;
                Customer.RouteBranch = Customer.RouteBranch.Contains('.') ? Customer.RouteBranch.Substring(0, Customer.RouteBranch.LastIndexOf(".")) : Customer.RouteBranch;
                Route = Customer.Route.Length > 3 ? Customer.Route.Substring(3) : Customer.Route;
                Billing = new Managers.OrderManager().GetSoldTo(Convert.ToInt32(Customer.CustomerNo));

                if (Billing.Split(',')[0].ToString() != Customer.customerNo)
                {
                    BillToName = CommonNavInfo.Customer.Name + " / " + Billing.Split(',')[1].ToString(); //Managers.CustomerManager.GetCustomerBillToName(Billing).ToString();
                }
                else
                {
                    BillToName = CommonNavInfo.Customer.Name;
                }
                if (Billing == "")
                {
                    Billing = Customer.CustomerNo;
                }
                else
                {
                    if (Billing.Contains(","))
                    {
                        Billing = Billing.Split(',')[0];
                    }
                }
                TotalAllied = ((decimal)Items.Where(s => s.SalesCat1 == "ALL").Sum(s => s.ExtendedPrice)).ToString("0.00");
                TotalCoffee = ((decimal)Items.Where(s => s.SalesCat1 == "COF").Sum(s => s.ExtendedPrice)).ToString("0.00");
                // CustomerPo = "PO: " + orderInfoData.Rows[0]["CustomerPo"].ToString();
                CustomerPo = "PO: " + _customerPO;
                //CustomerSignImagePath = IsSignPresent ? ResourceManager.DomainPath + @"\Docs" + "\\" + "test.jpg" : ResourceManager.DomainPath + @"Resources\Images\blank.jpg";

                if (IsSignPresent)
                {
                    CustomerSignImagePath = ResourceManager.DomainPath + @"\Docs" + "\\" + "test.jpg";
                }
                else
                {
                    //if (File.Exists("blank.jpg"))
                    if (File.Exists(ResourceManager.DomainPath + @"\Resources\Images\blank.jpg"))
                    {
                        CustomerSignImagePath = ResourceManager.DomainPath + @"\Resources\Images\blank.jpg";
                    }
                }


                TermOfSales = CommonNavInfo.Customer.PaymentModeDescriptionForList.ToUpper();
                RemitToName = "FARMER BROTHERS";
                RemitToPOBox = "PO BOX 732855";
                RemitToCityZip = "Dallas, TX 75373";
                LogoUrl = @"Resources\Images\FarmerBrothers_Logo.png";
                EnergySurcharge = EnergySurcharge == null ? "0" : EnergySurcharge.Replace("$", "").ToString();
                TaxAmount = TaxAmount == null ? "0.00" : (Convert.ToDecimal(TaxAmount)).ToString("0.00");
                VisitUsUrl = @"Visit us at https://myaccount.farmerbros.com and register your account,
                a convenient way to view your invoices, statement and make payments.";
                TotalDue = (((decimal)Items.Sum(s => s.ExtendedPrice)) + Convert.ToDecimal(EnergySurcharge.Replace("$", "").ToString()) + Convert.ToDecimal(TaxAmount)).ToString("0.00");
                ReportSource = new ReportViewModel(this, Reports.InvoiceReport, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;

                AppUserId = SalesLogicExpress.Application.Managers.UserManager.UserId;
                MachineName = ResourceManager.DeviceID;
                ReportName = "Invoice Report";
                ContentType = ReportEngine.ReportCategory.Customer;
                // RERouteInfo = new ReportEngine.RouteInformation() { RouterMasterId = 1 };
                CustomerInfo = new ReportEngine.CustomerInformation() { CustomerShipTo = Convert.ToInt32(CommonNavInfo.Customer.customerNo) };
                //RouteInfo = new ReportEngine.RouteInformation() { RouterMasterId = CommonNavInfo.RouteID };
                Parameters = new Dictionary<string, object>();
                Parameters.Add("InvoiceObj", Items);
                Connection = ResourceManager.GetOpenConnectionInstance();
                //if (!ShowPreview)
                //{
                //    System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                //    new Telerik.Reporting.Processing.ReportProcessor().PrintReport(ReportSource, printerSettings);
                //}
            }
            catch (Exception ex)
            {
                log.Error("[InvoiceReport][ExceptionStackTrace=" + ex.Message + "]");

            }
            log.Info("[End:InvoiceReport]");
        }

        #endregion
        public int AppUserId { get; set; }
        public string MachineName { get; set; }
        public string ReportName { get; set; }

        public ReportEngine.ReportCategory ContentType { get; set; }

        public ReportEngine.CustomerInformation CustomerInfo { get; set; }

        public ReportEngine.RouteInformation RouteInfo { get; set; }

        public Dictionary<string, object> Parameters { get; set; }

        [NonSerialized]
        private Sap.Data.SQLAnywhere.SAConnection _Connection;


        public Sap.Data.SQLAnywhere.SAConnection Connection { get { return _Connection; } set { _Connection = value; } }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
