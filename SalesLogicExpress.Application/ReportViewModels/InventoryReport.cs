﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Managers = SalesLogicExpress.Application.Managers;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.ViewModels;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using Telerik.Reporting;
using SalesLogicExpress.Reporting;
using SalesLogicExpress.Application.Helpers;
using System.Configuration;
using log4net;
using System.Data;
using System.Collections.ObjectModel;
using SalesLogicExpress.Application.Managers;
using System.Collections;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class InventoryReport : ViewModels.BaseViewModel, ReportEngine.IReportSLE
    {
        #region Variables And Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.InventoryReport]");
        InstanceReportSource reportsource;
        private ObservableCollection<Models.UserProfile> _UserProfileInfo = null;

        public InstanceReportSource ReportSource
        {
            get
            {
                return reportsource;
            }
            set
            {
                reportsource = value;
                OnPropertyChanged("ReportSource");
            }
        }

        //public Guid MessageToken { get; set; }
        public ObservableCollection<Models.UserProfile> UserProfileInfo
        {
            get
            {
                return _UserProfileInfo;
            }
            set
            {
                _UserProfileInfo = value;
                OnPropertyChanged("UserProfileInfo");
            }
        }
        public string UserName { get; set; }
        
        public string RouteNoAndName { get; set; }
        public string ReportTitle { get; set; }
        public string ReportDate { get; set; }
        public string TransferNo { get; set; }
        public string TransferFrom { get; set; }
        public string TransferTo { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public IList Items { get; set; }
        #endregion
        #region Constructor
        public InventoryReport(IList items)
        {
            try
            {

                log.Info("[Start:InventoryReport]");
                ReportTitle = "INVENTORY REPORT";

                //UserName = CommonNavInfo.UserName;
                UserName = CommonNavInfo.DisplayUserName;
                RouteNoAndName = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.RouteNameAddLoad;
                ReportDate = DateTime.Today.ToString("MM'/'dd'/'yyyy");
                TransferNo = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.SelectedReplnId;
                TransferFrom = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.FromBranch;
                TransferTo = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.ToBranch;
                FromDate = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.FromDate.ToString();
                ToDate = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.ToDate.ToString();
                Items = items;

                ReportSource = new ReportViewModel(this, Reports.InventoryReport, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
                // Binding Data as required by interface ReportEngine.IReportSLE
                AppUserId = SalesLogicExpress.Application.Managers.UserManager.UserId;
                MachineName = ResourceManager.DeviceID;
                ReportName = "Inventory Report";
                ContentType = ReportEngine.ReportCategory.Route;
                // RERouteInfo = new ReportEngine.RouteInformation() { RouterMasterId = 1 };
                RouteInfo = new ReportEngine.RouteInformation() { RouterMasterId = CommonNavInfo.RouteID };
                Parameters = new Dictionary<string, object>();
                //Parameters.Add("addLoadPickObj", items);
                Connection = ResourceManager.GetOpenConnectionInstance();
            }
            catch (Exception ex)
            {
                 log.Error("[InventoryReport][ExceptionStackTrace=" + ex.Message + "]");
                throw ex;
                
            }
        }
        #endregion
        public int AppUserId { get; set; }
        public string MachineName { get; set; }
        public string ReportName { get; set; }

        public ReportEngine.ReportCategory ContentType { get; set; }

        public ReportEngine.CustomerInformation CustomerInfo { get; set; }

        public ReportEngine.RouteInformation RouteInfo { get; set; }

        public Dictionary<string, object> Parameters { get; set; }

        [NonSerialized]
        private Sap.Data.SQLAnywhere.SAConnection _Connection;


        public Sap.Data.SQLAnywhere.SAConnection Connection { get { return _Connection; } set { _Connection = value; } }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
