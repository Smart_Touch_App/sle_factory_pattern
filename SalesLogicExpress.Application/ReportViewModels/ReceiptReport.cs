﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Telerik.Reporting;
using Managers = SalesLogicExpress.Application.Managers;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.ViewModels;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using SalesLogicExpress.Reporting;
using System.Configuration;
using SalesLogicExpress.Domain;
using System.Windows.Controls;
using log4net;
using System.IO;
using SalesLogicExpress.Application.Helpers;
using Telerik.Reporting;
using SalesLogicExpress.Application.Managers;
using System.Collections.ObjectModel;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class ReceiptReport : ViewModels.BaseViewModel, ReportEngine.IReportSLE
    {
        #region Variables & Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.ReceiptReport]");
        InstanceReportSource reportsource;
        public string RemitToName { get; set; }
        public string RemitToPOBox { get; set; }
        public string RemitToCityZip { get; set; }
        public string LogoUrl { get; set; }
        public string VisitUsUrl { get; set; }
        public string ReceiptNumber { get; set; }
        PaymentARModel.OpenAR objOpenAR = new PaymentARModel.OpenAR();

        public InstanceReportSource ReportSource
        {
            get
            {
                return reportsource;
            }
            set
            {
                reportsource = value;
                OnPropertyChanged("ReportSource");
            }
        }
        private ObservableCollection<Models.UserProfile> _UserProfileInfo = null;
        public ObservableCollection<Models.UserProfile> UserProfileInfo
        {
            get
            {
                return _UserProfileInfo;
            }
            set
            {
                _UserProfileInfo = value;
                OnPropertyChanged("UserProfileInfo");
            }
        }
        public string RouteSignImagePath { get; set; }
        public string DisplayName { get; set; }
        public string Billing { get; set; }
        public string PaymentMode { get; set; }    
        public string Route { get; set; }
        public string OrderDate { get; set; }
        public Customer Customer { get; set; }
        public PaymentARModel.OpenAR ObjOpenAR
        {
            get
            {
                return this.objOpenAR;
            }
            set
            {
                this.objOpenAR = value;
                OnPropertyChanged("ObjOpenAR");
            }
        }
        public List<Models.PaymentARModel.InvoiceDetails> Items { get; set; }
        #endregion

        #region Constructor
        public ReceiptReport(Models.PaymentARModel.OpenAR openAR)
        {
            try
            {
                log.Info("[Start:ReceiptReport][Parameters:openAR:" + openAR.ToString() + "]");
                ObjOpenAR = openAR;
                Customer = CommonNavInfo.Customer;
                Route = Customer.Route.Length > 3 ? Customer.Route.Substring(3) : Customer.Route;
                PaymentMode = ObjOpenAR.PaymentInfo;
                Billing = new Managers.OrderManager().GetSoldTo(Convert.ToInt32(Customer.CustomerNo));
                this.UserProfileInfo = UserManager.GetUserProfile(CommonNavInfo.RouteID);
                int appUserID = 0;
                if (UserProfileInfo != null && UserProfileInfo.Count > 0)
                {
                    DisplayName = UserProfileInfo.FirstOrDefault().DisplayName;
                    appUserID = UserProfileInfo.FirstOrDefault().AppUserID;
                    RouteSignImagePath = Managers.UserManager.GetUserSignature(appUserID);
                }
                bool IsRouteSignExist = string.IsNullOrEmpty(RouteSignImagePath) ? false : true;
                if (IsRouteSignExist)
                {
                    new CanvasViewModel(Flow.UserSetup, SignType.UserSetupSign, appUserID).UpdateSignatureToDB(false, false, true, "route.jpg");
                }
                
               //RouteSignImagePath = IsRouteSignExist ? ResourceManager.DomainPath + @"\Docs" + "\\" + "route.jpg" : ResourceManager.DomainPath + @"\Resources\Images\blank.jpg";

               if (IsRouteSignExist)
               {
                   RouteSignImagePath = ResourceManager.DomainPath + @"\Docs" + "\\" + "route.jpg";
               }
               else
               {
                   //if (File.Exists("blank.jpg"))
                   if (File.Exists(ResourceManager.DomainPath + @"\Resources\Images\blank.jpg"))
                   {
                       RouteSignImagePath = ResourceManager.DomainPath + @"\Resources\Images\blank.jpg";
                   }
               }

                if (Billing == "")
                {
                    Billing = Customer.CustomerNo;
                }
                else
                {
                    if (Billing.Contains(","))
                    {
                        Billing = Billing.Split(',')[0];
                    }
                }

                if (Items != null)
                    Items.Clear();

                Items = ObjOpenAR.SelectedItems.ToList();


                RemitToName = "FARMER BROTHERS";
                RemitToPOBox = "PO BOX 79705";
                RemitToCityZip = "CITY OF INDUSTRY CA 91716-9705";
                LogoUrl = @"Resources\Images\FarmerBrothers_Logo.png";

                VisitUsUrl = @"Visit us at https://myaccount.farmerbros.com and register your account,
                a convenient way to view your invoices, statement and make payments.";

                OrderDate = DateTime.Today.ToString("MM'/'dd'/'yyyy");
                //TotalPickedQty = Items.Sum(s => s.PickedQuantityInPrimaryUOM).ToString();
                ReportSource = new ReportViewModel(this, Reports.ReceiptReport, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;


                //Binding Data as required by interface ReportEngine.IReportSLE
                AppUserId = SalesLogicExpress.Application.Managers.UserManager.UserId;
                MachineName = ResourceManager.DeviceID;
                ReportName = "ReceiptReport";
                ContentType = ReportEngine.ReportCategory.Customer;
                // RERouteInfo = new ReportEngine.RouteInformation() { RouterMasterId = 1 };
                CustomerInfo = new ReportEngine.CustomerInformation() { CustomerShipTo = Convert.ToInt32(Customer.CustomerNo) };
                Parameters = new Dictionary<string, object>();
                Parameters.Add("PrintLabels", Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]));
                Connection = ResourceManager.GetOpenConnectionInstance();


                //System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                //new //Telerik.Reporting.Processing.ReportProcessor().PrintReport(ReportSource, printerSettings);
            }
            catch (Exception ex)
            {
                log.Error("[ReceiptReport][ExceptionStackTrace=" + ex.Message + "]");
                throw;
            }
            log.Info("[End:ReceiptReport]");
        }
        #endregion

        public int AppUserId { get; set; }
        public string MachineName { get; set; }
        public string ReportName { get; set; }

        public ReportEngine.ReportCategory ContentType { get; set; }

        public ReportEngine.CustomerInformation CustomerInfo { get; set; }

        public ReportEngine.RouteInformation RouteInfo { get; set; }

        public Dictionary<string, object> Parameters { get; set; }

        [NonSerialized]
        private Sap.Data.SQLAnywhere.SAConnection _Connection;


        public Sap.Data.SQLAnywhere.SAConnection Connection { get { return _Connection; } set { _Connection = value; } }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
