﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Managers = SalesLogicExpress.Application.Managers;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.ViewModels;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using Telerik.Reporting;
using SalesLogicExpress.Reporting;
using SalesLogicExpress.Application.Helpers;
using System.Configuration;
using log4net;
using System.Data;
using System.IO;
using System.Collections.ObjectModel;
using SalesLogicExpress.Application.Managers;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class ItemReturnsReport : ViewModels.BaseViewModel, ReportEngine.IReportSLE
    {
        #region Variables And Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.InvoiceReport]");
        InstanceReportSource reportsource;

        public InstanceReportSource ReportSource
        {
            get
            {
                return reportsource;
            }
            set
            {
                reportsource = value;
                OnPropertyChanged("ReportSource");
            }
        }

        public Models.Customer Customer { get; set; }
        public string OrderNumber { get; set; }

        public string Billing { get; set; }
        public string Route { get; set; }
        public string VisitUsUrl { get; set; }
        public string RemitToName { get; set; }
        public string RemitToPOBox { get; set; }
        public string RemitToCityZip { get; set; }
        public string TaxAmount { get; set; }
        public string EnergySurcharge { get; set; }
        public string CustomerPo { get; set; }
        
        public string OrderDate { get; set; }
        public string TermOfSales { get; set; }
        public string CustomerSignImagePath { get; set; }
        private string _TotalAllied;
        public string TotalAllied
        {
            get
            {
                return _TotalAllied;
            }
            set
            {
                _TotalAllied = value;
                OnPropertyChanged("TotalAllied");
            }

        }
        public string TotalCoffee { get; set; }
        
        public string TotalDue { get; set; }
        public string LogoUrl { get; set; }
        private ObservableCollection<Models.UserProfile> _UserProfileInfo = null;
        public ObservableCollection<Models.UserProfile> UserProfileInfo
        {
            get
            {
                return _UserProfileInfo;
            }
            set
            {
                _UserProfileInfo = value;
                OnPropertyChanged("UserProfileInfo");
            }
        }
        public string RouteSignImagePath { get; set; }
        public string DisplayName { get; set; }

        public List<Models.ReturnItem> Items { get; set; }
        #endregion
        #region Constructor
        public ItemReturnsReport(string orderNumber, bool ShowPreview = false)
        {
            try
            {
                OrderNumber = orderNumber;

                Items = new Managers.CustomerReturnsManager().GetOrdersForOrderReturn(OrderNumber).ToList();
                DataTable orderInfoData = new Managers.OrderManager().GetOrderInfo(OrderNumber);

                //Below added for User Issue Id : 5. Order Quantity is binded in Quantity  Column
                //Items.ToList().ForEach(x => { x.OrderQty = x.ReturnQty; });

                //Below added for User Issue Id : 5. Order Quantity is binded in Quantity  Column
                foreach (var item in Items)
                {

                    //foreach (DataRow dr in orderInfoData.Select("ItemNumber = " + item.ItemNumber + " and relatedorderid = " + item.OrderID))                       
                    //        item.OrderQty = Convert.ToInt32(dr["OrderQty"]);
                    if (orderInfoData.Rows.Count > 0)
                    {
                        DataRow[] drr = orderInfoData.Select("ItemNumber = " + item.ItemNumber + " and (relatedorderid = 0 Or relatedorderid = " + item.OrderID + ")");
                        if (drr != null)
                            item.OrderQty = Convert.ToInt32(drr[0]["OrderQty"]);
                    }
                }
                if (Items != null)
                {
                    OrderDate = Items.FirstOrDefault().OrderDate;
                    CustomerPo = Items.FirstOrDefault().CustomerPo;
                }
                EnergySurcharge =  Convert.ToDecimal(orderInfoData.Rows[0]["EnergySurchargeAmt"].ToString()).ToString("0.00");
                TaxAmount =  Convert.ToDecimal(orderInfoData.Rows[0]["SalesTaxAmt"].ToString()).ToString("0.##");
                int appUserID = 0;
                this.UserProfileInfo = UserManager.GetUserProfile(CommonNavInfo.RouteID);
                if (UserProfileInfo != null && UserProfileInfo.Count > 0)
                {
                    DisplayName = UserProfileInfo.FirstOrDefault().DisplayName;
                    appUserID = UserProfileInfo.FirstOrDefault().AppUserID;
                }
                RouteSignImagePath = Managers.UserManager.GetUserSignature(appUserID);
                bool IsRouteSignExist = string.IsNullOrEmpty(RouteSignImagePath) ? false : true;
                if (IsRouteSignExist)
                {
                    new CanvasViewModel(Flow.UserSetup, SignType.UserSetupSign, appUserID).UpdateSignatureToDB(false, false, true, "route.jpg");
                }
                //RouteSignImagePath = IsRouteSignExist ? ResourceManager.DomainPath + @"\Docs" + "\\" + "route.jpg" : ResourceManager.DomainPath + @"\Resources\Images\blank.jpg";

                if (IsRouteSignExist)
                {
                    RouteSignImagePath = ResourceManager.DomainPath + @"\Docs" + "\\" + "route.jpg";
                }
                else
                {
                    //if (File.Exists("blank.jpg"))
                    if (File.Exists(ResourceManager.DomainPath + @"\Resources\Images\blank.jpg"))
                    {
                        RouteSignImagePath = ResourceManager.DomainPath + @"\Resources\Images\blank.jpg";
                    }
                }

                bool IsSignPresent = string.IsNullOrEmpty(orderInfoData.Rows[0]["ordersignature"].ToString().Trim()) ? false : true;

                if (IsSignPresent)
                {
                    //Draw sign on Image 
                    new CanvasViewModel(Flow.OrderReturn, SignType.ReturnOrderSign, Convert.ToInt32(OrderNumber)).UpdateSignatureToDB(false, false, true, "ReturnOrder.jpg");
                }

                log.Info("[Start:InvoiceReport][IsSignPresent:" + IsSignPresent + "]");
                Customer = CommonNavInfo.Customer;
                Customer.RouteBranch = Customer.RouteBranch.Contains('.') ? Customer.RouteBranch.Substring(0, Customer.RouteBranch.LastIndexOf(".")) : Customer.RouteBranch;

                Route = Customer.Route.Length > 3 ? Customer.Route.Substring(3) : Customer.Route;
                Billing = new Managers.OrderManager().GetSoldTo(Convert.ToInt32(Customer.CustomerNo));
                if (Billing == "")
                {
                    Billing = Customer.CustomerNo;
                }
                else
                {
                    if (Billing.Contains(","))
                    {
                        Billing = Billing.Split(',')[0];
                    }
                }
                TotalAllied = (-1 * (decimal)Items.Where(s => s.SalesCat1 == "ALL").Sum(s => s.ExtendedPrice)).ToString("0.00");
                TotalCoffee = (-1 * (decimal)Items.Where(s => s.SalesCat1 == "COF").Sum(s => s.ExtendedPrice)).ToString("0.00");
                       
                //CustomerSignImagePath = IsSignPresent ? ResourceManager.DomainPath + @"\Docs" + "\\" + "ReturnOrder.jpg" : ResourceManager.DomainPath + @"Resources\Images\blank.jpg";

                if (IsSignPresent)
                {
                    
                        CustomerSignImagePath = ResourceManager.DomainPath + @"\Docs" + "\\" + "ReturnOrder.jpg";
                }
                else
                {
                    //if (File.Exists("blank.jpg"))
                    if (File.Exists(ResourceManager.DomainPath + @"\Resources\Images\blank.jpg"))
                    {
                        CustomerSignImagePath = ResourceManager.DomainPath + @"\Resources\Images\blank.jpg";
                    }
                }
               
                TermOfSales = CommonNavInfo.Customer.PaymentModeDescriptionForList.ToUpper();
                RemitToName = "FARMER BROTHERS";
                RemitToPOBox = "PO BOX 79705";
                RemitToCityZip = "CITY OF INDUSTRY CA 91716-9705";
                LogoUrl = @"Resources\Images\FarmerBrothers_Logo.png";
                EnergySurcharge = EnergySurcharge == null ? "0" : EnergySurcharge.Replace("$", "").ToString();
                TaxAmount = TaxAmount == null ? "0.00" : (Convert.ToDecimal(TaxAmount)).ToString("0.00");
                VisitUsUrl = @"Visit us at https://myaccount.farmerbros.com and register your account,
                a convenient way to view your invoices, statement and make payments.";

                TotalDue = (-1 * ((decimal)Items.Sum(s => s.ExtendedPrice) + Convert.ToDecimal(EnergySurcharge.Replace("$", "").ToString()) + Convert.ToDecimal(TaxAmount))).ToString("0.00");
                
                // CO Energy Surcharge Should show Negative
                EnergySurcharge = '-' + Convert.ToDecimal(orderInfoData.Rows[0]["EnergySurchargeAmt"].ToString()).ToString("0.00");
                // CO Tax amount Should show Negative
                TaxAmount = '-' + Convert.ToDecimal(orderInfoData.Rows[0]["SalesTaxAmt"].ToString()).ToString("0.##");

                ReportSource = new ReportViewModel(this, Reports.ItemReturnsReport, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
                //Binding Data as required by interface ReportEngine.IReportSLE
                AppUserId = SalesLogicExpress.Application.Managers.UserManager.UserId;
                MachineName = ResourceManager.DeviceID;
                ReportName = "Items Returns Report";
                ContentType = ReportEngine.ReportCategory.Customer;
                // RouteInfo = new ReportEngine.RouteInformation() { RouterMasterId = 1 };
                //   CustomerInfo = new ReportEngine.CustomerInformation() { CustomerShipTo = Convert.ToInt32(ReturnOrderID) };
                CustomerInfo = new ReportEngine.CustomerInformation() { CustomerShipTo = Convert.ToInt32(CommonNavInfo.Customer.customerNo) };
                Parameters = new Dictionary<string, object>();
                Parameters.Add("itemReturnsObj", Items);
                Connection = ResourceManager.GetOpenConnectionInstance();
                //if (!ShowPreview)
                //{
                //    System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                //    new Telerik.Reporting.Processing.ReportProcessor().PrintReport(ReportSource, printerSettings);
                //}
            }
            catch (Exception ex)
            {
                log.Error("[InvoiceReport][ExceptionStackTrace=" + ex.Message + "]");
            }
            log.Info("[End:InvoiceReport]");
        }
        #endregion
        #region IReportSLE Members

        public int AppUserId { get; set; }
        public string MachineName { get; set; }
        public string ReportName { get; set; }

        public ReportEngine.ReportCategory ContentType { get; set; }

        public ReportEngine.CustomerInformation CustomerInfo { get; set; }

        public ReportEngine.RouteInformation RouteInfo { get; set; }

        public Dictionary<string, object> Parameters { get; set; }

        [NonSerialized]
        private Sap.Data.SQLAnywhere.SAConnection _Connection;


        public Sap.Data.SQLAnywhere.SAConnection Connection { get { return _Connection; } set { _Connection = value; } }


        #endregion


        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
