﻿using log4net;
using SalesLogicExpress.Domain;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class QuoteReport : ViewModels.BaseViewModel
    {
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.CommanInReplenishmentReport]");

        #region Variables & Properties

        #region int
        


        #endregion

        #region Bool
        #endregion

        #region String
        private string quoteNo = "";

        public string QuoteNo
        {
            get { return quoteNo; }
            set { quoteNo = value; }
        }
        public string RouteNoAndName { get; set; }
        public string BranchName { get; set; }
        public string ReportTitle { get; set; }
        public string ReportDate { get; set; }
        public string UserName { get; set; }
        

        private string parent = "";

        public string Parent
        {
            get { return parent; }
            set { parent = value; }
        }

        private string billTo = "";

        public string BillTo
        {
            get { return billTo; }
            set { billTo = value; }
        }

        private string quoteDate = "";

        public string QuoteDate
        {
            get { return quoteDate; }
            set { quoteDate = value; }
        }

        private string priceProtection = "";

        public string PriceProtection
        {
            get { return priceProtection; }
            set { priceProtection = value; }
        }
        #endregion

        #region Class
        private PricingDetails overridePriceSetup = new PricingDetails();

        public PricingDetails OverridePriceSetup
        {
            get { return overridePriceSetup; }
            set { overridePriceSetup = value; }
        }

      

        public InstanceReportSource reportsource;
        public InstanceReportSource ReportSource
        {
            get
            {
                return reportsource;
            }
            set
            {
                reportsource = value;
                OnPropertyChanged("ReportSource");
            }
        }
        #endregion

        #region Collection

        public IList Items { get; set; }
        public IList PricingList { get; set; }

        private List<object> priceList = new List<object>();

        public List<object> PriceList
        {
            get { return priceList; }
            set { priceList = value; }
        }
        #endregion

        #endregion

        public QuoteReport()
        {
            try
            {
                log.Info("[Start:QuoteReport]");
            }
            catch (Exception ex)
            {
                log.Error("[QuoteReport][ExceptionStackTrace=" + ex.Message + "]");
            }
            log.Info("[End:QuoteReport]");
        }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
