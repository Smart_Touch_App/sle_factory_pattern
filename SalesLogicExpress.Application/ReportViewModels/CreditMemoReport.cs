﻿using log4net;
using SalesLogicExpress.Application.ViewModels;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Reporting;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;
using System.Windows.Ink;
using SalesLogicExpress.Application.Helpers;
using System.Collections.ObjectModel;
using SalesLogicExpress.Application.Managers;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class CreditMemoReport : ViewModels.BaseViewModel, ReportEngine.IReportSLE
    {
        #region Variables & Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.CreditMemoReport]");
        public string ReportTitle { get; set; }
        public string Route { get; set; }
        public string Billing { get; set; }
        public string ReceiptID { get; set; }
        public string Status { get; set; }
        public string CreditReason { get; set; }
        public string Note { get; set; }
        public string VoidReason { get; set; }
        public string CreditMemoDate { get; set; }
        public string Amount { get; set; }
        public string CustomerSignaturePath { get; set; }
        public IList Items { get; set; }
        private InstanceReportSource reportsource;
        private Models.Customer _Customer;
        public InstanceReportSource ReportSource
        {
            get
            {
                return reportsource;
            }
            set
            {
                reportsource = value;
                OnPropertyChanged("ReportSource");
            }
        }
        public Models.Customer Customer
        {
            get
            {
                return _Customer;
            }
            set
            {
                _Customer = value;
            }
        }
        private ObservableCollection<Models.UserProfile> _UserProfileInfo = null;
        public ObservableCollection<Models.UserProfile> UserProfileInfo
        {
            get
            {
                return _UserProfileInfo;
            }
            set
            {
                _UserProfileInfo = value;
                OnPropertyChanged("UserProfileInfo");
            }
        }
        public string RouteSignImagePath { get; set; }
        public string DisplayName { get; set; }

        #endregion
        public CreditMemoReport(Models.CreditProcess creditProcess)
        {
            try
            {
                //bool IsSignPresent = new Managers.SignatureManager().GetSignature(Helpers.Flow.Credits, Helpers.SignType.CreditProcessSign, creditProcess.ReceiptID).Count == 0 ? false : true;
                //new CanvasViewModel(Helpers.Flow.Credits, Helpers.SignType.CreditProcessSign, creditProcess.ReceiptID).SaveCanvasAsImage();
                StrokeCollection strkCollection = new Managers.SignatureManager().GetSignature(Helpers.Flow.Credits, Helpers.SignType.CreditProcessSign, creditProcess.ReceiptID);
                new CanvasViewModel(Helpers.Flow.Credits, Helpers.SignType.CreditProcessSign, creditProcess.ReceiptID).GetImageOfCanvas(strkCollection);
                bool IsSignPresent = strkCollection.Count == 0 ? false : true;
                log.Info("[Start:CreditMemoReport]");
                ReportTitle = "CREDIT MEMO REPORT";
                Status = creditProcess.StatusCode;
                ReceiptID = creditProcess.ReceiptID.ToString();
                Customer = CommonNavInfo.Customer;
                Customer.RouteBranch = Customer.RouteBranch.Contains('.') ? Customer.RouteBranch.Substring(0, Customer.RouteBranch.LastIndexOf(".")) : Customer.RouteBranch;
                Route = Customer.Route.Length > 3 ? Customer.Route.Substring(3) : Customer.Route;
                Billing = new Managers.OrderManager().GetSoldTo(Convert.ToInt32(Customer.CustomerNo));
                this.UserProfileInfo = UserManager.GetUserProfile(CommonNavInfo.RouteID);
                int appUserID = 0;
                if (UserProfileInfo != null && UserProfileInfo.Count > 0)
                {
                    DisplayName = UserProfileInfo.FirstOrDefault().DisplayName;
                    appUserID = UserProfileInfo.FirstOrDefault().AppUserID;
                }
                RouteSignImagePath = Managers.UserManager.GetUserSignature(appUserID);
                bool IsRouteSignExist = string.IsNullOrEmpty(RouteSignImagePath) ? false : true;
                if (IsRouteSignExist)
                {
                    new CanvasViewModel(Flow.UserSetup, SignType.UserSetupSign, appUserID).UpdateSignatureToDB(false, false, true, "route.jpg");
                }
                if (IsRouteSignExist)
                {
                        RouteSignImagePath = ResourceManager.DomainPath + @"\Docs" + "\\" + "route.jpg";
                }
                else
                {
                    //if (File.Exists("blank.jpg"))
                    if (File.Exists(ResourceManager.DomainPath + @"\Resources\Images\blank.jpg"))
                    {
                        RouteSignImagePath = ResourceManager.DomainPath + @"\Resources\Images\blank.jpg";
                    }
                }
                //RouteSignImagePath = IsRouteSignExist ? ResourceManager.DomainPath + @"\Docs" + "\\" + "route.jpg" : ResourceManager.DomainPath + @"\Resources\Images\blank.jpg";

                if (Billing == "")
                {
                    Billing = Customer.CustomerNo;
                }
                else
                {
                    if (Billing.Contains(","))
                    {
                        Billing = Billing.Split(',')[0];
                    }
                }
                //CustomerName = creditProcess.CustomerName.Trim();
                Amount = "$" + Convert.ToDecimal(creditProcess.CreditAmount).ToString("F2");
                CreditMemoDate = creditProcess.CreditMemoDate.ToString("MM'/'dd'/'yyyy");
                Note = creditProcess.CreditNote;
                CreditReason = creditProcess.CreditReason;
                VoidReason = creditProcess.VoidReason;

                CustomerSignaturePath = IsSignPresent ? ResourceManager.DomainPath + @"\Docs" + "\\" + "test.jpg" : ResourceManager.DomainPath + @"\Resources\Images\blank.jpg";
                
                          //CustomerSignaturePath = IsSignPresent ? ResourceManager.DomainPath + @"\Docs" + "\\" + "test.jpg" : ResourceManager.DomainPath + @"\Resources\Images\blank.jpg";
               
               if (IsSignPresent)
               {
                   CustomerSignaturePath = ResourceManager.DomainPath + @"\Docs" + "\\" + "test.jpg";
               }
               else
               {
                   //if (File.Exists("blank.jpg"))
                   if (File.Exists(ResourceManager.DomainPath + @"\Resources\Images\blank.jpg"))
                   {
                       CustomerSignaturePath = ResourceManager.DomainPath + @"\Resources\Images\blank.jpg";
                   }
               }

                ReportSource = new ReportViewModel(this, Reports.CreditMemoReport, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
                //System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                //Telerik.Reporting.Processing.ReportProcessor telerikReportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
                //telerikReportProcessor.PrintController.OnEndPrint((PrintDocument)reportsource.ReportDocument, new PrintEventArgs());
                //telerikReportProcessor.PrintReport(ReportSource, printerSettings);

                //Binding Data as required by interface ReportEngine.IReportSLE
                AppUserId = SalesLogicExpress.Application.Managers.UserManager.UserId;
                MachineName = ResourceManager.DeviceID;
                ReportName = "CreditMemo";
                ContentType = ReportEngine.ReportCategory.Customer;
                // RERouteInfo = new ReportEngine.RouteInformation() { RouterMasterId = 1 };
                CustomerInfo = new ReportEngine.CustomerInformation() { CustomerShipTo = Convert.ToInt32(Customer.CustomerNo) };
                Parameters = new Dictionary<string, object>();
                Parameters.Add("PrintLabels", Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]));
                Connection = ResourceManager.GetOpenConnectionInstance();
            }
            catch (Exception ex)
            {
                log.Error("[CreditMemoReport][ExceptionStackTrace=" + ex.Message + "]");
            }
            log.Info("[End:CreditMemoReport]");

        }

        public int AppUserId { get; set; }
        public string MachineName { get; set; }
        public string ReportName { get; set; }

        public ReportEngine.ReportCategory ContentType { get; set; }

        public ReportEngine.CustomerInformation CustomerInfo { get; set; }

        public ReportEngine.RouteInformation RouteInfo { get; set; }

        public Dictionary<string, object> Parameters { get; set; }

        [NonSerialized]
        private Sap.Data.SQLAnywhere.SAConnection _Connection;


        public Sap.Data.SQLAnywhere.SAConnection Connection { get { return _Connection; } set { _Connection = value; } }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
