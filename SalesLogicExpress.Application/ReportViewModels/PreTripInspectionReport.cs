﻿using log4net;
using SalesLogicExpress.Application.ViewModels;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Reporting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Telerik.Reporting;
using System.Collections.ObjectModel;
using System.Windows.Ink;
using SalesLogicExpress.Application.Helpers;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Windows.Media;

namespace SalesLogicExpress.Application.ReportViewModels
{
    [Serializable]
    public class PreTripInspectionQuestionList
    {
        private string questionID = string.Empty;
        public string QuestionID
        {
            get
            { return questionID; }
            set
            {
                questionID = value;

            }
        }

        private string questionTitle = string.Empty;
        public string QuestionTitle
        {
            get
            {
                return questionTitle;
            }
            set
            {
                questionTitle = value;

            }
        }

        private int responseID;
        public int ResponseID
        {
            get
            {
                return responseID;
            }
            set
            {
                responseID = value;

            }
        }

        private string description = string.Empty;
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;

            }
        }
        //public bool IsYesChecked { get; set; }
        // public bool IsNoChecked { get; set; }
        private string notPassedReason = string.Empty;
        public string NotPassedReason
        {
            get
            {
                return notPassedReason;
            }
            set
            {
                notPassedReason = value;

            }
        }

        private bool isYesChecked = false;
        public bool IsYesChecked
        {
            get { return isYesChecked; }
            set
            {
                if (isYesChecked != value)
                {
                    isYesChecked = value;
                    if (isYesChecked)
                    {
                        IsNoChecked = false;
                    }
                    //else isno
                }



            }
        }

        private bool isNoChecked = false;
        public bool IsNoChecked
        {
            get { return isNoChecked; }
            set
            {
                if (isNoChecked != value)
                {
                    isNoChecked = value;
                    if (isNoChecked)
                    {
                        IsYesChecked = false;
                    }
                }

            }
        }

        private bool isCheckboxEnabled = true;
        public bool IsCheckboxEnabled
        {
            get { return isCheckboxEnabled; }
            set
            {
                isCheckboxEnabled = value;
            }
        }

        private bool isNoPassedReasonEnabled = true;

        public bool IsNoPassedReasonEnabled
        {
            get { return isNoPassedReasonEnabled; }
            set
            {
                isNoPassedReasonEnabled = value;
            }
        }
    }
    //ViewModels.BaseViewModel,

    [Serializable]
    public class PreTripInspectionReport : ReportEngine.IReportSLE
    {
        [NonSerialized]
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.PreTripInspectionReport]");

        #region Properties

        public string Route { get; set; }
        public string ReportTitle { get; set; }
        public string Branch { get; set; }
        public string RsrName { get; set; }
        public string VehicleNo { get; set; }
        public string VehicleMake { get; set; }
        public string OdometerReading { get; set; }
        public string AdditionalComments { get; set; }
        public string Date { get; set; }
        public string QuestionID { get; set; }
        public string QuestionTitle { get; set; }
        public string ResponseID { get; set; }
        public string Description { get; set; }
        public bool IsYesChecked { get; set; }
        public bool IsNoChecked { get; set; }
        public string NoPassReason { get; set; }

        // public List<PreTripInspectionQuestionList> QuestionList { get; set; }

        public ObservableCollection<Models.PreTripInspectionFormList> QuestionList { get; set; }

        public IList Items { get; set; }
        public string ConfirmationStatement1 { get; set; }
        public string ConfirmationStatement2 { get; set; }
        public string ConfirmationStatement3 { get; set; }


        public string Signature { get; set; }

        #endregion


        public PreTripInspectionReport()
        {
        }

        public PreTripInspectionReport(Models.PreTripVehicleInspection preTripObj)
        {
            try
            {

                StrokeCollection strkCollection = new Managers.SignatureManager().GetSignature(Helpers.Flow.PreTripInspection, Helpers.SignType.PreTripInspection, Convert.ToInt32(preTripObj.PreTripInspectionID));
                new CanvasViewModel(Helpers.Flow.PreTripInspection, Helpers.SignType.PreTripInspection, Convert.ToInt32(preTripObj.PreTripInspectionID)).GetImageOfCanvas(strkCollection);
                bool IsSignPresent = strkCollection.Count == 0 ? false : true;

                ReportTitle = "PRE-TRIP VEHICLE INSPECTION FORM".Trim();
                Branch = CommonNavInfo.UserBranch.Trim();
                RsrName = CommonNavInfo.RouteUser.Trim();
                Date = preTripObj.PreTripDateTime.ToString("MM'/'dd'/'yyyy");
                VehicleNo = preTripObj.VehicleNumber.ToString().Trim();
                VehicleMake = preTripObj.VehicleModel.Trim();
                VehicleMake = VehicleMake; // preTripObj.VehicleModel.Trim();B
                OdometerReading = preTripObj.OdometerReading.Trim();
                ConfirmationStatement1 = Helpers.Constants.PreTripInspection.ConfirmationStatement_1;
                ConfirmationStatement2 = Helpers.Constants.PreTripInspection.ConfirmationStatement_2;
                ConfirmationStatement3 = Helpers.Constants.PreTripInspection.ConfirmationStatement_3;
                Signature = IsSignPresent ? ResourceManager.DomainPath + @"\Docs" + "\\" + "test.jpg" : "blank.jpg";
                AdditionalComments = preTripObj.AdditionalComments;
                QuestionList = preTripObj.QuestionList;



                //SourceDocumentNo =                 
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.ReportViewModels][PreTripInspectionReport][PreTripInspectionReport][ExceptionStackTrace=" + ex.StackTrace + "]");
            }

            //Binding Data as required by interface ReportEngine.IReportSLE
            AppUserId = SalesLogicExpress.Application.Managers.UserManager.UserId;
            MachineName = ResourceManager.DeviceID;
            ReportName = "PreTripInspectionReport";
            ContentType = ReportEngine.ReportCategory.Route;
            RouteInfo = new ReportEngine.RouteInformation() { RouterMasterId = 1 };
            Parameters = new Dictionary<string, object>();
            Parameters.Add("preTripObj", preTripObj);
            Connection = ResourceManager.GetOpenConnectionInstance();


            //ReportSource = new ReportViewModel(this, Reports.PreTripInspection, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
            //System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
            ////Telerik.Reporting.Processing.ReportProcessor telerikReportProcessor = new //Telerik.Reporting.Processing.ReportProcessor();
            ////////new Telerik.Reporting.Processing.ReportProcessor().PrintController.OnEndPrint((PrintDocument)reportsource.ReportDocument, new PrintEventArgs());
            //////new Telerik.Reporting.Processing.ReportProcessor().PrintReport(ReportSource, printerSettings);
        }


        #region IReportSLE Members

        public int AppUserId { get; set; }
        public string MachineName { get; set; }
        public string ReportName { get; set; }

        public ReportEngine.ReportCategory ContentType { get; set; }

        public ReportEngine.CustomerInformation CustomerInfo { get; set; }

        public ReportEngine.RouteInformation RouteInfo { get; set; }

        public Dictionary<string, object> Parameters { get; set; }

        [NonSerialized]
        private Sap.Data.SQLAnywhere.SAConnection _Connection;


        public Sap.Data.SQLAnywhere.SAConnection Connection { get { return _Connection; } set { _Connection = value; } }


        #endregion


    }
}
