﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Reporting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class LoadSuggetionsReport : CommanInReplenishmentReport, ReportEngine.IReportSLE
    {
        #region Variables & Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.LoadSuggetionsReport]");
        #endregion
        #region Constructor
        public LoadSuggetionsReport(ReplenishmentDetails selectedRplnshmnt, IList items)
        {
            try
            {
                log.Info("[Start:LoadSuggetionsReport]");
                ReportTitle = "ROUTE REPLENISHMENT-LOAD REQUEST";
                Status = selectedRplnshmnt.Status;
                //UserName = CommonNavInfo.UserName;
                UserName = CommonNavInfo.DisplayUserName;
                RouteNoAndName = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.RouteNameAddLoad;
                ReportDate = DateTime.Today.ToString("MM'/'dd'/'yyyy");
                TransferNo = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.SelectedReplnId;
                TransferFrom = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.FromBranch;
                TransferTo = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.ToBranch;
                FromDate = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.FromDate.ToString("MM'/'dd'/'yyyy");
                ToDate = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.ToDate.ToString("MM'/'dd'/'yyyy");
                Items = items;
                if (ViewModelPayload.PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                {
                    ReportSource = new ReportViewModel(this, Reports.LoadSuggetionsReport, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
                }
                else
                {
                    ReportSource = new ReportViewModel(this, Reports.LoadSuggetionsReportDC, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
                }
                //Binding Data as required by interface ReportEngine.IReportSLE
                AppUserId = SalesLogicExpress.Application.Managers.UserManager.UserId;
                MachineName = ResourceManager.DeviceID;
                ReportName = "SuggestionReplenishmentReport";
                ContentType = ReportEngine.ReportCategory.Route;
                // RERouteInfo = new ReportEngine.RouteInformation() { RouterMasterId = 1 };
                RouteInfo = new ReportEngine.RouteInformation() { RouterMasterId = CommonNavInfo.RouteID };
                Parameters = new Dictionary<string, object>();
                Parameters.Add("SuggestionObj", selectedRplnshmnt);
                Connection = ResourceManager.GetOpenConnectionInstance();
                //  System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                //  new Telerik.Reporting.Processing.ReportProcessor().PrintReport(ReportSource, printerSettings);
            }
            catch (Exception ex)
            {
                log.Error("[LoadSuggetionsReport][ExceptionStackTrace=" + ex.Message + "]");
            }
            log.Info("[End:LoadSuggetionsReport]");
        }
        #endregion
        public int AppUserId { get; set; }
        public string MachineName { get; set; }
        public string ReportName { get; set; }

        public ReportEngine.ReportCategory ContentType { get; set; }

        public ReportEngine.CustomerInformation CustomerInfo { get; set; }

        public ReportEngine.RouteInformation RouteInfo { get; set; }

        public Dictionary<string, object> Parameters { get; set; }

        [NonSerialized]
        private Sap.Data.SQLAnywhere.SAConnection _Connection;


        public Sap.Data.SQLAnywhere.SAConnection Connection { get { return _Connection; } set { _Connection = value; } }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
