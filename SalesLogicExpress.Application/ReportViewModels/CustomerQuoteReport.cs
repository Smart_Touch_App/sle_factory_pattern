﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Reporting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SalesLogicExpress.Application.ReportViewModels
{
    public class CustomerQuoteReport : QuoteReport, ReportEngine.IReportSLE
    {
        #region Variables & Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.CustomerQuoteReport]");

        private string customerID = string.Empty;

        public string CustomerID
        {
            get { return customerID; }
            set { customerID = value; }
        }

        private string customerName = string.Empty;

        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }
        public string LogoUrl { get; set; }
        #endregion


        public CustomerQuoteReport(CustomerQuoteHeader CustomerQuote)
        {
            try
            {
                log.Info("[Start:AddLoadRequestReport]");
                LogoUrl = @"Resources\Images\FarmerBrothers_Logo.png";
                ReportTitle = ViewModelPayload.PayloadManager.ProspectPayload.IsProspect ? "PROSPECT PRODUCT QUOTE" : "CUSTOMER PRODUCT QUOTE";
                //UserName = CommonNavInfo.UserName;
                UserName = CommonNavInfo.DisplayUserName;
                RouteNoAndName = CommonNavInfo.UserRoute;
                BranchName = CommonNavInfo.UserBranch;
                ReportDate = DateTime.Today.ToString("MM'/'dd'/'yyyy");
                PriceList.Add(CustomerQuote.OverridePriceSetup);
                PricingList = PriceList;

                foreach (var item in CustomerQuote.Items)
                {
                    item.PricingUOMPrice = Math.Round(item.PricingUOMPrice, 2, MidpointRounding.AwayFromZero); 
                    item.TransactionUOMPrice = Math.Round(item.TransactionUOMPrice, 2, MidpointRounding.AwayFromZero); 
                    item.OtherUOMPrice = Math.Round(item.OtherUOMPrice, 2, MidpointRounding.AwayFromZero); 
                }
                Items = CustomerQuote.Items;
                BillTo = CustomerQuote.BillTo.Trim();
                Parent = CustomerQuote.Parent.Trim();
                CustomerID = ViewModelPayload.PayloadManager.ProspectPayload.IsProspect ? ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID : CustomerQuote.CustomerId.ToString();
                QuoteDate = CustomerQuote.QuoteDate.ToString("MM'/'dd'/'yyyy");
                CustomerName = ViewModelPayload.PayloadManager.ProspectPayload.IsProspect ? ViewModelPayload.PayloadManager.ProspectPayload.Prospect.Name : CommonNavInfo.Customer.Name;
                QuoteNo = ViewModelPayload.PayloadManager.QuotePayload.QuoteId.ToString();
                PriceProtection = CustomerQuote.OverridePriceSetup.PriceProtection;
                ReportSource = ViewModelPayload.PayloadManager.ProspectPayload.IsProspect ?
                    new ReportViewModel(this, Reports.ProspectQuote, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource :
                    new ReportViewModel(this, Reports.CustomerQuote, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;


                //Binding Data as required by interface ReportEngine.IReportSLE
                AppUserId = SalesLogicExpress.Application.Managers.UserManager.UserId;
                MachineName = ResourceManager.DeviceID;
                ReportName = "CustomerQuoteReport";
                ContentType = ReportEngine.ReportCategory.Customer;
                // RERouteInfo = new ReportEngine.RouteInformation() { RouterMasterId = 1 };
                CustomerInfo = new ReportEngine.CustomerInformation() { CustomerShipTo = Convert.ToInt32(CustomerID) };
                Parameters = new Dictionary<string, object>();
                Parameters.Add("preTripObj", CustomerQuote);
                Connection = ResourceManager.GetOpenConnectionInstance();


                System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                //Telerik.Reporting.Processing.ReportProcessor telerikReportProcessor = new //Telerik.Reporting.Processing.ReportProcessor();
                //////new Telerik.Reporting.Processing.ReportProcessor().PrintController.OnEndPrint((PrintDocument)reportsource.ReportDocument, new PrintEventArgs());
                ////new Telerik.Reporting.Processing.ReportProcessor().PrintReport(ReportSource, printerSettings);

            }
            catch (Exception)
            {

                throw;
            }

        }




        #region IReportSLE Members

        public int AppUserId { get; set; }
        public string MachineName { get; set; }
        public string ReportName { get; set; }

        public ReportEngine.ReportCategory ContentType { get; set; }

        public ReportEngine.CustomerInformation CustomerInfo { get; set; }

        public ReportEngine.RouteInformation RouteInfo { get; set; }

        public Dictionary<string, object> Parameters { get; set; }

        [NonSerialized]
        private Sap.Data.SQLAnywhere.SAConnection _Connection;


        public Sap.Data.SQLAnywhere.SAConnection Connection { get { return _Connection; } set { _Connection = value; } }


        //public ReportEngine.DocumentInformation Generate<T>(decimal sourceDocumentNo,bool  reGenerate, ReportEngine.ReportsTypes reportType, ReportEngine.ContentTypes reportFormat = ReportEngine.ContentTypes.PDF) where T : class
        //{

        //    var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());
        //    repEngine.Log = this.log;

        //    //SalesLogicExpress.Reporting.PreTripInspection
        //    var docInfo = repEngine.Generate<T>(sourceDocumentNo, reGenerate, reportType, this, Parameters, ReportEngine.ContentTypes.PDF);
        //    return docInfo;

        //}

        //public byte[] ViewReport(decimal documentId, Sap.Data.SQLAnywhere.SAConnection connection, ReportEngine.ReportCategory category, System.Drawing.Printing.PrinterSettings printerSettings = null)
        //{
        //    var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());
        //    repEngine.Log = this.log;
        //    repEngine.ViewReport(connection, category, documentId, printerSettings);
        //    return null;

        //}
        //public byte[] PrintReport(decimal documentId, Sap.Data.SQLAnywhere.SAConnection connection, ReportEngine.ReportCategory category, System.Drawing.Printing.PrinterSettings printerSettings = null)
        //{
        //    var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());
        //    repEngine.Log = this.log;
        //    return repEngine.PrintReport(connection, category, documentId, printerSettings);

        //}



        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
