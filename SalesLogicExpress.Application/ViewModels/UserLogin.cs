﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using Telerik.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.ObjectModel;
using Managers = SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using log4net;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using System.Windows.Threading;
using System.Configuration;

namespace SalesLogicExpress.Application.ViewModels
{
    public class UserLogin : ViewModelBase
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.UserLogin");

        string progressMessage, loginCaption;
        bool enableLogin;
        public DelegateCommand LoginUser { get; set; }
        public string LoginID { get; set; }
        public bool EnableLogin
        {
            get { return enableLogin; }
            set
            {
                enableLogin = value;
                OnPropertyChanged("enableLogin");
            }
        }
        public string LoginCaption
        {
            get { return loginCaption; }
            set
            {
                loginCaption = value;
                OnPropertyChanged("LoginCaption");
            }
        }
        public string Password
        {
            set;
            get;
        }
        public string DomainUser { get; set; }
        public string DeviceID { get; set; }
        public bool IsValidUser { get; set; }
        public string Route { get; set; }

        bool isNetworkAvailable;
        public bool IsNetworkAvailable
        {
            get { return isNetworkAvailable; }
            set
            {
                isNetworkAvailable = value;
                IsNetworkOffline = !value;
                OnPropertyChanged("IsNetworkAvailable");
            }
        }
        bool isOffline;
        public bool IsNetworkOffline
        {
            get { return isOffline; }
            set
            {
                isOffline = value;
                OnPropertyChanged("IsNetworkOffline");
            }
        }
        public string ProgressMessage
        {
            get { return progressMessage; }
            set
            {
                progressMessage = value;
                OnPropertyChanged("ProgressMessage");
            }
        }
        public Guid MessageToken { get; set; }

        public UserLogin()
        {
            log4net.Config.XmlConfigurator.Configure();
            IsValidUser = true;
            EnableLogin = true;
            LoginCaption = "Login";

            InitializeCommands();

            //   SalesLogicExpress.Application.Helpers.ODBCManager.RemoveDSN("dsn_remote_FBM783");
        }
        void LogActivity()
        {
            PayloadManager.OrderPayload.RouteID = PayloadManager.ApplicationPayload.Route;
            Activity ac = new Activity
            {
                RouteID = PayloadManager.ApplicationPayload.Route,
                ActivityType = ActivityKey.UserLogin.ToString(),
                ActivityStart = DateTime.Now
            };
            Activity a = ResourceManager.Transaction.LogActivity(ac);
        }
        void InitializeCommands()
        {
            IsNetworkAvailable = ResourceManager.NetworkManager.IsInterNetConnected();
            ResourceManager.NetworkManager.InternetStateChanged += NetworkInfo_InternetStateChanged;

            LoginUser = new DelegateCommand((param) =>
            {
                //LoginID = "RSR109"; Route = "FBM173";
                if (LoginID.Trim().Length == 0 || Password.Trim().Length == 0 || Route.Trim().Length == 0)
                {
                    Messenger.Default.Send<AlertWindow>(new AlertWindow() { Message = "All fields are mandatory for login!!" }, MessageToken);
                    return;
                }
                EnableLogin = false;
                LoginCaption = EnableLogin ? "Login" : "Logging in..";
                var progressIndicator = new Progress<string>(ReportProgress);
                //TempLoginUserExecute(progressIndicator);
                Managers.UserManager.RouteEntered = Route;

                LoginUserExecute(progressIndicator);

            });

        }

        private void NetworkInfo_InternetStateChanged(object sender, Managers.NetworkManager.InternetStateChangedEventArgs e)
        {
            if (e.State == Managers.NetworkManager.InternetState.Connected)
            {
                IsNetworkAvailable = true;
            }
            else
            {
                IsNetworkAvailable = false;
            }
        }
        void ReportProgress(string value)
        {
            ProgressMessage = value;
        }

        async void LoginUserExecute(IProgress<string> progress)
        {
            bool isRouteDbPresent = false;
            bool isDeviceRegistered = true;
            bool isOtherUserActiveOnRoute = false;
            Managers.SetupManager setupManager = new Managers.SetupManager();
            await Task.Run(() =>
            {

                bool systemSynched = false, userSynched = false, routeDataSynched = false;
                ResourceManager.CloseConnectionInstance();

                //Commented By Arvind as ConnectionString will be managed by SalesLogicExpress.Extensions.RemoteDB.ConnectionString;
                //ResourceManager.ActiveDatabaseConnectionString = string.Format(setupManager.RouteDbConnectionString, Route);


                //progress.Report("Checking database for route...");
                progress.Report("Launching Sales Logic Express Application...");
                isRouteDbPresent = setupManager.IsRouteDatabasePresent(Route);
                Logger.Info("Route db existence checked " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt"));


                //ToDo - - ToBeRemovedLater
                #region Commented by Arvind as Remoted OnBoarding is handled by Launcher
                /* 
                if (!isRouteDbPresent)
                {
                    if (!IsNetworkAvailable)
                    {
                        return;
                    }
                    progress.Report("Setting up database...");
                    setupManager.CreateRouteDatabase(Route);
                    Logger.Info("Route database created " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt"));
                    progress.Report("Setting up remote");
                    setupManager.SetRemoteID(DeviceID, Route);
                    Logger.Info("Remote ID set in db " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt"));

                    progress.Report("Synchronizing system setup users...");
                    systemSynched = setupManager.SynchronizeSystemDB();
                    progress.Report("Synchronizing users...");
                    userSynched = setupManager.SynchronizeUsers();
                    if (!systemSynched && !userSynched)
                    {
                        progress.Report("Problem syncing Users...");
                        ResourceManager.CloseConnectionInstance();
                        progress.Report("Cleaning installed resources...");
                        setupManager.CleanupInstalledDatabase(Route);
                        EnableLogin = true;
                        LoginCaption = EnableLogin ? "Login" : "Logging in..";
                        ProgressMessage = string.Empty;
                        progress.Report("");
                        return;
                        //ResourceManager.Transaction.AddTransactionInQueueForSync(Managers.Transaction.GetLatestUsers, SalesLogicExpress.Application.Managers.SyncQueueManager.Priority.everything);
                    }
                    Logger.Info("SynchronizeUsers " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt"));
                    progress.Report("Validating user...");

                    if (!setupManager.IsDeviceRegistered(DeviceID))
                    {
                        isDeviceRegistered = false;
                        return;
                    }
                    if (setupManager.isOtherUserActiveOnRoute(Route))
                    {
                        isOtherUserActiveOnRoute = true;
                        return;
                    }
                }
                 * */
                #endregion


                IsValidUser = setupManager.ValidateUser(LoginID, Password, Route, DeviceID);
                IsValidUser = true;
                Logger.Info("Validated user login " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt"));
                if (IsValidUser && !isRouteDbPresent)
                {
                    progress.Report("User validated...");

                    //ToDo  -check if Sync operation is required here as it would bwould have already beendone by Launcher
                    ResourceManager.ActiveRemoteID = Route;
                    //if (!isRouteDbPresent)
                    //{
                    //    progress.Report("Registering device...");
                    //    setupManager.RegisterDeviceAsRemote(setupManager.RemoteID, DeviceID, Route);
                    //    systemSynched = setupManager.SynchronizeSystemDB();
                    //    userSynched = setupManager.SynchronizeUsers();
                    //    Logger.Info("Registering device as remote " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt"));

                    //    progress.Report("Subscribing to publication...");
                    //    setupManager.SubscribeUserToPublication(Route, LoginID);
                    //    Logger.Info("Subscribe User To Publication " + DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt"));
                    //}
                    //progress.Report("Synchronizing data for route...");
                    //routeDataSynched = setupManager.SynchronizeDataForRoute();
                    //if (!routeDataSynched)
                    //{
                    //    //ResourceManager.Transaction.AddTransactionInQueueForSync(Managers.Transaction.GetEverything, Managers.SyncQueueManager.Priority.everything);
                    //}
                    // initialize the navigation header for the first view
                    ViewModels.CommonNavInfo.SetLastSynched(DateTime.Now.AddHours(-6).ToString("M'/'dd'/'yyyy' 'hh:mm") + DateTime.Now.AddHours(-6).ToString(" tt").ToLower());
                    ViewModels.CommonNavInfo.ShowBackNavigation = false;
                    ViewModels.CommonNavInfo.ViewTitle = "Customer Initial";
                }
                if (!isRouteDbPresent && !IsValidUser)
                {

                }
            });
            if (!isRouteDbPresent && !IsValidUser)
            {
                progress.Report("Cleaning installed resources...");
                ResourceManager.CloseConnectionInstance();
                setupManager.CleanupInstalledDatabase(Route);
                EnableLogin = true;
                LoginCaption = EnableLogin ? "Login" : "Logging in..";
                ProgressMessage = string.Empty;
                progress.Report("");
                Messenger.Default.Send<AlertWindow>(new AlertWindow() { Message = Constants.UserLogin.InValidUserRoute }, MessageToken);
                return;
            }
            if (!IsNetworkAvailable && !isRouteDbPresent)
            {
                EnableLogin = true;
                LoginCaption = EnableLogin ? "Login" : "Logging in..";
                ProgressMessage = string.Empty;
                Messenger.Default.Send<AlertWindow>(new AlertWindow() { Message = Constants.UserLogin.InternetConnectionRequired }, MessageToken);
                return;
            }
            if (!isDeviceRegistered)
            {
                EnableLogin = true;
                LoginCaption = EnableLogin ? "Login" : "Logging in..";
                ProgressMessage = string.Empty;
                Messenger.Default.Send<AlertWindow>(new AlertWindow() { Message = string.Format(Constants.UserLogin.DeviceNotRegistered, DeviceID) }, MessageToken);

                return;
            }
            if (isOtherUserActiveOnRoute)
            {
                EnableLogin = true;
                LoginCaption = EnableLogin ? "Login" : "Logging in..";
                ProgressMessage = string.Empty;
                Messenger.Default.Send<AlertWindow>(new AlertWindow() { Message = Constants.UserLogin.RouteInactive }, MessageToken);
                return;
            }
            if (IsValidUser)
            {
                ViewModels.CommonNavInfo.UserName = LoginID;
                Managers.UserManager.UserId = 1929; // Managers.UserManager.GetUserId(LoginID);
                CommonNavInfo.RouteUser = Route;
                PayloadManager.ApplicationPayload.LoggedInUserID = Managers.UserManager.UserId.ToString();
                PayloadManager.ApplicationPayload.Route = Route;
                PayloadManager.ApplicationPayload.BaseDate = Convert.ToDateTime(ConfigurationManager.AppSettings["BaseDate"].ToString());
                LogActivity();
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.UserLogin, CloseCurrentView = true, ShowAsModal = false, SelectTab = ViewModelMappings.TabView.RouteHome.Dashboard };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                EnableLogin = true;
                LoginCaption = EnableLogin ? "Login" : "Logging in..";
                GetItemRestrictionList();
                CommonNavInfo.RouteID = Managers.UserManager.GetRouteId(Route.Trim());

                /**************************************************************************************************/
                // Initialize and Start LListening to Sync Queue
                /**************************************************************************************************/
                SalesLogicExpress.Application.Helpers.ResourceManager.Transaction.Initialize();
                //SalesLogicExpress.Application.Helpers.ResourceManager.Synchronization.Initialize();
                SalesLogicExpress.Application.Helpers.ResourceManager.QueueManager.ListenToSync();
                SalesLogicExpress.Application.Helpers.ResourceManager.Synchronization.ListenToQueue();
                SalesLogicExpress.Application.Helpers.ResourceManager.CommonNavInfo.InitilizeQueue();
                /**************************************************************************************************/


                //Set repnenishBranchID and ReplenishBranch Type
                Managers.UserManager.ReplenishmentToBranch = Managers.UserManager.GetRepnBranchID(CommonNavInfo.RouteID);
                Managers.UserManager.RepnlBranchType = Managers.UserManager.GetRepnBranchType(CommonNavInfo.RouteID);
            }

        }

        // Get the items that could/could not be sold to customer.
        async void GetItemRestrictionList()
        {
            await Task.Run(() =>
            {
                SalesLogicExpress.Application.Managers.ItemManager.getItemRestrictionList();
            });
        }

        async void TempLoginUserExecute(IProgress<string> progress)
        {
            await Task.Run(() =>
            {
                Managers.SetupManager setupManager = new Managers.SetupManager();
                Managers.ServiceRouteManager customerManager = new Managers.ServiceRouteManager();
                progress.Report("Getting data for customers...");
                ObservableCollection<Customer> customers = customerManager.GetCustomersForRoute(Route, DateTime.Now);
                Logger.Info(string.Concat("GetCustomersForRoute(", Route, ")", DateTime.Now.ToString("dd/MMM/yyyy hh:mm:ss tt")));
                Customer randomCustomer = customers.Where(s => s.CustomerNo == "1108911").FirstOrDefault() as Customer;

                // initialize the navigation header for the first view
                ViewModels.CommonNavInfo.SetSelectedCustomer(randomCustomer);
                ViewModels.CommonNavInfo.SetLastSynched(DateTime.Now.AddHours(-6).ToString("M'/'dd'/'yyyy' 'hh:mm") + DateTime.Now.AddHours(-6).ToString(" tt").ToLower());
                ViewModels.CommonNavInfo.ShowBackNavigation = false;
                ViewModels.CommonNavInfo.ViewTitle = "Order Template";
                ViewModels.CommonNavInfo.PreviousViewTitle = "Service Route";
                ViewModels.CommonNavInfo.UserName = LoginID;
            });
            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.UserLogin, Payload = Route, CloseCurrentView = true, ShowAsModal = false };
            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            EnableLogin = true;
            LoginCaption = EnableLogin ? "Login" : "Logging in..";

        }

    }
}
