﻿using System;
using System.Collections.Generic;
using Telerik.Windows.Controls;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Managers;
using log4net;

namespace SalesLogicExpress.Application.ViewModels
{
    public class CreateAndMoveStopViewModel : ViewModelBase
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.ARAgingSummaryViewModel");
        ServiceRouteManager serviceRouteManager = new ServiceRouteManager();

        public event EventHandler<StopActionCompletedEventArgs> StopModified;

        public DelegateCommand MoveStop { get; set; }

        public DelegateCommand CreateStop { get; set; }
        public DelegateCommand CalendarSelectionChanged { get; set; }
        public Visitee _customer = new Visitee();
        public Prospect _prospect = new Prospect();
        public Visitee Visitee
        {
            get
            {
                return _customer;
            }
            set
            {
                _customer = value;
                OnPropertyChanged("customer");
            }
        }

        public Prospect Prospect
        {
            get
            {
                return _prospect;
            }
            set
            {
                _prospect = value;
                OnPropertyChanged("Prospect");
            }
        }

        public List<DateTime> _BlackOutDates = new List<DateTime>();

        public List<DateTime> BlackOutDates
        {
            get
            {
                return _BlackOutDates;
            }
            set
            {
                _BlackOutDates = value;
                OnPropertyChanged("BlackOutDates");
            }
        }

        public bool IsCreateButton = true;

        public DateTime SelectableDateStart { get; set; }
        public DateTime SelectableDateEnd { get; set; }
        public DateTime DailyStopDate { set; get; }
        private bool _IsContinueEnabled = false;
        public bool IsContinueEnabled
        {
            get
            {
                return _IsContinueEnabled;
            }
            set
            {
                _IsContinueEnabled = value;
                OnPropertyChanged("IsContinueEnabled");
            }
        }
        public CreateAndMoveStopViewModel(Visitee VisiteeObj)
        {
            Visitee = VisiteeObj;
            IsContinueEnabled = false;
            InitializeCommands();
        }
        //public CreateAndMoveStopViewModel(Visitee visitee)
        //{
        //    visitee = CustomerObj;
        //    IsContinueEnabled = false;
        //    InitializeCommands();
        //}

        public CreateAndMoveStopViewModel()
        {
            // TODO: Complete member initialization
            InitializeCommands();
        }

        private void InitializeCommands()
        {
            MoveStop = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreateAndMoveStopViewModel][Start:MoveStop]");
                try
                {
                    IsContinueEnabled = false;
                    DateTime moveToDate = Convert.ToDateTime(param);
                    if (param != null)
                    {
                        if (serviceRouteManager.MoveCustomerStop(Visitee, moveToDate, DailyStopDate))
                        {

                            string stopId = Visitee.StopType.ToLower() == "rescheduled" ? Visitee.ReferenceStopId : Visitee.StopID;
                            string orgDate = Visitee.OriginalDate.HasValue ? Visitee.OriginalDate.Value.ToString("yyyy-MM-dd") : Visitee.StopDate.Value.ToString("yyyy-MM-dd");
                            Visitee.StopID = serviceRouteManager.GetUpdatedStopId(Visitee.VisiteeId, orgDate, stopId);
                            StopActionCompletedEventArgs args = new StopActionCompletedEventArgs();
                            args.DateSelected = moveToDate;
                            args.visiteeChanged = Visitee;
                            OnStopMoved(args);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CreateAndMoveStopViewModel][MoveStop][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreateAndMoveStopViewModel][End:MoveStop]");

            });
            CreateStop = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreateAndMoveStopViewModel][Start:CreateStop]");
                try
                {
                    DateTime dateToCreateStop = Convert.ToDateTime(param);
                    IsContinueEnabled = false;
                    if (param != null)
                    {
                        if (serviceRouteManager.CreateUnplannedStop(Visitee, dateToCreateStop))
                        {
                            StopActionCompletedEventArgs args = new StopActionCompletedEventArgs();
                            args.DateSelected = dateToCreateStop;
                            args.visiteeChanged = Visitee;
                            OnStopMoved(args);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CreateAndMoveStopViewModel][CreateStop][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreateAndMoveStopViewModel][End:CreateStop]");


            });
            CalendarSelectionChanged = new DelegateCommand((param) =>
            {
                IsContinueEnabled = true;
            });
        }

        private void OnStopMoved(StopActionCompletedEventArgs e)
        {
            EventHandler<StopActionCompletedEventArgs> handler = StopModified;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
    public class StopActionCompletedEventArgs : EventArgs
    {
        public Visitee visiteeChanged { get; set; }
        public DateTime? DateSelected = null;
        public StopActionCompletedEventArgs()
        {

        }

    }

}
