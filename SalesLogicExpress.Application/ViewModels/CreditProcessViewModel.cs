﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using System.Windows.Ink;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ViewModelPayload;
using System.ComponentModel;
using System.Threading;

namespace SalesLogicExpress.Application.ViewModels
{
    public class CreditProcessViewModel : BaseViewModel, IDataErrorInfo
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CreditProcessViewModel");

        #region Properties
        public bool IsUnplannedCreatedForFuture { get; set; }
        bool _IsSearching = false;
        public bool IsSearching
        {
            get
            {
                return _IsSearching;
            }
            set
            {
                _IsSearching = value;
                OnPropertyChanged("IsSearching");
            }
        }
        //private Guid _MessageToken;
        //public Guid MessageToken
        //{

        //    get { return _MessageToken; }
        //    set
        //    {
        //        _MessageToken = value;
        //        OnPropertyChanged("MessageToken");
        //    }
        //}

        private string customerNo;
        public string CustomerNo
        {
            get { return customerNo; }
            set { customerNo = value; OnPropertyChanged("CustomerNo"); }
        }

        private string routeID;
        public string RouteID
        {
            get { return routeID; }
            set { routeID = value; OnPropertyChanged("RouteID"); }
        }

        private string routeName;
        public string RouteName
        {
            get { return routeName; }
            set { routeName = value; OnPropertyChanged("RouteName"); }
        }

        private string creditNote;
        public string CreditNote
        {
            get { return creditNote; }
            set
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][Start:CreditNote]");
                try
                {
                    creditNote = value;
                    if (!string.IsNullOrEmpty(CreditNote.Trim()))
                    {
                        CreditMemoObject.CreditNote = CreditNote.Trim();
                        if (!string.IsNullOrEmpty(CreditAmount) && IsValidAmount && SelectedReasonCode > 0)
                        {
                            ToggleReasonCodeButton = true;
                        }
                        else
                            ToggleReasonCodeButton = false;
                    }
                    else
                        ToggleReasonCodeButton = false;
                    OnPropertyChanged("CreditNote");
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][CreditNote][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][End:CreditNote]");
            }
        }

        private string creditAmount;
        public string CreditAmount
        {
            get { return creditAmount; }
            set
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][Start:CreditAmount]");
                try
                {
                    creditAmount = value;
                    if (IsAddCreditMemo)
                    {
                        if (!string.IsNullOrEmpty(CreditAmount))
                        {
                            if ((Convert.ToDecimal(CreditAmount) > 0 && Convert.ToDecimal(CreditAmount) <= 100))
                            {
                                IsValidAmount = true;
                                CreditMemoObject.CreditAmount = Convert.ToDecimal(CreditAmount).ToString("F2");

                                if (SelectedReasonCode > 0 && !string.IsNullOrEmpty(CreditNote.Trim()))
                                {
                                    ToggleReasonCodeButton = true;
                                }
                                else
                                {
                                    ToggleReasonCodeButton = false;
                                }
                            }
                            else
                            {
                                IsValidAmount = false;
                                ToggleReasonCodeButton = false;
                            }
                        }
                        else
                        {
                            IsValidAmount = false;
                            ToggleReasonCodeButton = false;
                        }
                    }
                    else
                    {
                        if (SelectedReasonCode > 0)
                        {
                            ToggleReasonCodeButton = true;
                        }
                        else
                            ToggleReasonCodeButton = false;
                    }
                    OnPropertyChanged("CreditAmount");
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][CreditAmount][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][End:CreditAmount]");
            }
        }

        private CreditProcess creditMemoObject = new CreditProcess();
        public CreditProcess CreditMemoObject
        {
            get { return creditMemoObject; }
            set { creditMemoObject = value; OnPropertyChanged("CreditMemoObject"); }
        }

        private ObservableCollection<CreditProcess> creditsMemoList = new ObservableCollection<CreditProcess>();
        private ObservableCollection<CreditProcess> creditsMemoFilteredList = new ObservableCollection<CreditProcess>();
        public ObservableCollection<CreditProcess> CreditsMemoList
        {
            get { return creditsMemoList; }
            set { creditsMemoList = value; OnPropertyChanged("CreditsMemoList"); }
        }

        private ObservableCollection<CreditProcess> creditListForSearch = new ObservableCollection<CreditProcess>();
        public ObservableCollection<CreditProcess> CreditListForSearch
        {
            get { return creditListForSearch; }
            set { creditListForSearch = value; OnPropertyChanged("CreditListForSearch"); }
        }

        private ObservableCollection<CreditProcess> temp = new ObservableCollection<CreditProcess>();
        public ObservableCollection<CreditProcess> Temp
        {
            get { return temp; }
            set { temp = value; OnPropertyChanged("Temp"); }
        }

        private ObservableCollection<CreditProcess> tempCollection = new ObservableCollection<CreditProcess>();
        public ObservableCollection<CreditProcess> TempCollection
        {
            get { return tempCollection; }
            set { tempCollection = value; OnPropertyChanged("TempCollection"); }
        }
        CanvasViewModel _CanvasVM;
        public CanvasViewModel SignatureCanvasVM
        {
            get
            {
                return _CanvasVM;
            }
            set
            {
                _CanvasVM = value;
                OnPropertyChanged("SignatureCanvasVM");
            }
        }
        private ObservableCollection<ReasonCode> reasonCode = null;

        public ObservableCollection<ReasonCode> ReasonCode
        {
            get { return reasonCode; }
            set { reasonCode = value; OnPropertyChanged("ReasonCode"); }
        }

        private int selectedReasonCode;
        public int SelectedReasonCode
        {
            get { return selectedReasonCode; }
            set
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][Start:SelectedReasonCode]");
                try
                {
                    selectedReasonCode = value;
                    ToggleReasonCodeButton = false;
                    if (SelectedReasonCode > 0)
                    {
                        if (IsAddCreditMemo)
                        {
                            CreditMemoObject.CreditReasonID = SelectedReasonCode.ToString();
                            if (!string.IsNullOrEmpty(CreditAmount) && IsValidAmount && !string.IsNullOrEmpty(CreditNote.Trim()))
                            {
                                CreditMemoObject.CreditReasonID = SelectedReasonCode.ToString();
                                if (!string.IsNullOrEmpty(CreditAmount) && IsValidAmount && !string.IsNullOrEmpty(CreditNote.Trim()))
                                {
                                    ToggleReasonCodeButton = true;
                                }
                                else ToggleReasonCodeButton = false;
                            }
                            else ToggleReasonCodeButton = false;
                        }
                        else
                            ToggleReasonCodeButton = true;
                    }
                    OnPropertyChanged("SelectedReasonCode");
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][SelectedReasonCode][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][End:SelectedReasonCode]");
            }
        }

        private string reasonDescription;
        public string ReasonDescription
        {
            get { return reasonDescription; }
            set { reasonDescription = value; OnPropertyChanged("ReasonDescription"); }
        }

        private int reasonCodeID;
        public int ReasonCodeID
        {
            get { return reasonCodeID; }
            set { reasonCodeID = value; OnPropertyChanged("ReasonCodeID"); }
        }

        private bool toggleReasonCodeButton = false;
        public bool ToggleReasonCodeButton
        {
            get { return toggleReasonCodeButton; }
            set { toggleReasonCodeButton = value; OnPropertyChanged("ToggleReasonCodeButton"); }
        }

        private bool toggleSignAndPrintButton;
        public bool ToggleSignAndPrintButton
        {
            get { return toggleSignAndPrintButton; }
            set { toggleSignAndPrintButton = value; OnPropertyChanged("ToggleSignAndPrintButton"); }
        }

        private bool isSwitchDisable = false;
        public bool IsSwitchDisable
        {
            get { return isSwitchDisable; }
            set { isSwitchDisable = value; OnPropertyChanged("IsSwitchDisable"); }
        }

        private bool isPrintButtonEnabled = false;
        public bool IsPrintButtonEnabled
        {
            get { return isPrintButtonEnabled; }
            set { isPrintButtonEnabled = value; OnPropertyChanged("IsPrintButtonEnabled"); }
        }

        private bool isVoidButtonEnabled = true;
        public bool IsVoidButtonEnabled
        {
            get
            {
                return isVoidButtonEnabled;
            }
            set
            {
                isVoidButtonEnabled = value;
                OnPropertyChanged("IsVoidButtonEnabled");
            }
        }

        private bool isAddCreditMemoEnabled = true;
        public bool IsAddCreditMemoEnabled
        {
            get
            {
                return isAddCreditMemoEnabled;
            }
            set
            {
                isAddCreditMemoEnabled = value;
                OnPropertyChanged("IsAddCreditMemoEnabled");
            }
        }

        private bool isVoidDialog = false;
        public bool IsVoidDialog
        {
            get { return isVoidDialog; }
            set { isVoidDialog = value; OnPropertyChanged("IsVoidDialog"); }
        }
        bool IsAddCreditMemo = false;
        bool IsSignPrintRequired = false;
        bool IsValidAmount = true;
        public string Error
        {
            get
            {
                return null;
                //throw new NotImplementedException(); 
            }
        }
        public string this[string columnName]
        {
            get
            {
                string result = string.Empty;
                if (columnName == "CreditAmount" && !IsValidAmount)
                {
                    result = Helpers.Constants.Common.CreditAmountErrorMessage;
                }
                return result;
            }
        }

        public int generatedCreditNo = 0;//Sathish Unwanted Static Keyword code
        #endregion

        #region DelegateCommands
        public DelegateCommand ClearSearchText { get; set; }
        public DelegateCommand OpenSignAndPrintDialog { get; set; }
        public DelegateCommand OpenDialogForAddCredit { get; set; }
        public DelegateCommand OpenDialogForVoidCredit { get; set; }
        public DelegateCommand PrintCreditMemo { get; set; }
        public DelegateCommand FinaliseCreditMemo { get; set; }
        public DelegateCommand ClearSign { get; set; }
        public DelegateCommand SelectionItemInvoke { get; set; }

        public DelegateCommand AddVoidPopUpGridUnloadDlgtCmd { get; set; }
        #endregion

        #region Constructor
        public CreditProcessViewModel(bool flag)
        {
            IsUnplannedCreatedForFuture = flag;
            InitializeCommands();
            LoadData();

        }
        #endregion

        #region Methods

        CreditProcessManager creditManager = new CreditProcessManager();
        public void InitializeCommands()
        {
            #region Grid unloaded events for oppening pop-up

            AddVoidPopUpGridUnloadDlgtCmd = new DelegateCommand((param) =>
            {
                if (this.IsSignPrintRequired)
                {
                    this.IsSignPrintRequired = false;
                    var dialogWindow = new Helpers.DialogWindow { TemplateKey = "SignAndPrint", Title = "Sign And Print" };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialogWindow, MessageToken);
                }
            });

            #endregion

            #region OpenDialogForAddCredit
            ClearSearchText = new DelegateCommand((param) =>
           {
               SearchText = string.Empty;
               RunSearch(SearchText);
           });
            OpenDialogForAddCredit = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][Start:OpenDialogForAddCredit]");
                try
                {
                    IsAddCreditMemo = true;
                    ReasonCode = creditManager.GetCreditReasonCode();
                    CreditMemoObject.ReceiptNumber = CreditMemoObject.ReceiptNumber == -1 || CreditMemoObject.ReceiptNumber == 0 ? creditManager.GetNewCreditMemoNumber() : CreditMemoObject.ReceiptNumber;
                    CreditMemoObject.ReceiptID = CreditMemoObject.ReceiptNumber;
                    if (CreditMemoObject.ReceiptID > 0)
                    {
                        RenderSign();
                        SelectedReasonCode = -1;
                        CreditAmount = "";
                        IsValidAmount = true;
                        ToggleReasonCodeButton = false;
                        CreditNote = string.Empty;
                        var dialogWindow = new Helpers.DialogWindow { TemplateKey = "AddCreditDialog", Title = "Add Credit Memo" };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialogWindow, MessageToken);
                    }
                    else
                        Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, please sync to continue" }, MessageToken);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][OpenDialogForAddCredit][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][End:OpenDialogForAddCredit]");

            });
            #endregion

            PrintCreditMemo = new DelegateCommand((param) =>
            {
                var confirmMessage = new Helpers.ConfirmWindow { Message = "Do you want to print?", MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;
                RenderSign();

                ShowReport(CreditMemoObject);
                //new ReportViewModels.CreditMemoReport(CreditMemoObject);

            });

            #region OpenSignAndPrintDialog

            OpenSignAndPrintDialog = new DelegateCommand((param) =>
            {
                //TODO
                // GENERATE THE RANDOM UNIQUE NO. FROM NO. POOL AND ASSIGN IT TO RECEIPT NO.
                // SHOW ALL INFO INTO sign and print pop-up 
                //if (SignatureCanvasVM.CanvasStrokeCollection.StrokesChanged != null)
                //{
                //}
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][Start:OpenSignAndPrintDialog]");
                try
                {

                    RenderSign();
                    SignatureCanvasVM.CanvasStrokeCollection.Clear();
                    SignatureCanvasVM.IsCanvasEnable = true;
                    SignatureCanvasVM.CanClearCanvas = false;
                    SignatureCanvasVM.CanvasStrokeCollection.StrokesChanged += CanvasStrokeCollection_StrokesChanged;

                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                    ToggleSignAndPrintButton = false;
                    if (IsAddCreditMemo)
                    {
                        IsVoidDialog = false;
                        //CreditMemoObject.ReceiptNumber = 554366;
                        CreditMemoObject.CreditReasonID = SelectedReasonCode.ToString();
                        foreach (ReasonCode item in ReasonCode)
                        {
                            if (item.Id == Convert.ToInt32(CreditMemoObject.CreditReasonID))
                            {
                                CreditMemoObject.CreditReason = item.Code.Trim();
                            }
                        }
                        CreditMemoObject.CreditMemoDate = DateTime.Now;
                    }
                    else
                    {
                        IsVoidDialog = true;
                        CreditMemoObject.VoidReasonID = SelectedReasonCode.ToString();

                        foreach (ReasonCode item in ReasonCode)
                        {
                            if (item.Id == Convert.ToInt32(CreditMemoObject.VoidReasonID))
                            {
                                CreditMemoObject.VoidReason = item.Code.Trim();
                            }
                        }
                    }

                    this.IsSignPrintRequired = true; //Set flag to true to open te Sign print pop-up dialog grd unload event 
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][OpenSignAndPrintDialog][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][End:OpenSignAndPrintDialog]");
            });
            #endregion

            #region FinaliseCreditMemo

            FinaliseCreditMemo = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][Start:FinaliseCreditMemo]");
                try
                {
                    if (IsAddCreditMemo)
                    {
                        CreditMemoObject.StatusID = creditManager.GetStatusId("USTLD");
                        creditManager.AddCreditMemoNew(CreditMemoObject);
                        SignatureCanvasVM.UpdateSignatureToDB(true, true, true, "CreditMemoSign.jpg");
                        decimal ConvertedAmount = -Math.Abs(Convert.ToDecimal(CreditMemoObject.CreditAmount));
                        CreditMemoObject.CreditAmount = Convert.ToString(ConvertedAmount);
                        LogActivityForCreditMemo(ActivityKey.CreateCreditMemo);
                        CreditMemoObject.ReceiptNumber = -1;

                        #region update quantities
                        string tempStopDate = string.Empty;
                        string stopID = string.Empty;
                        if (PayloadManager.OrderPayload.StopDate.Date > DateTime.Now.Date || PayloadManager.OrderPayload.StopDate.Date < DateTime.Now.Date)
                        {
                            tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                            stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.OrderPayload.Customer.CustomerNo, tempStopDate);
                        }
                        else
                        {
                            tempStopDate = PayloadManager.OrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                            stopID = PayloadManager.OrderPayload.Customer.StopID;
                        }

                        new CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                        new CustomerManager().UpdateActivityCount(stopID, false, true);
                        PayloadManager.OrderPayload.Customer.CompletedActivity += 1;
                        PayloadManager.OrderPayload.Customer.SaleStatus = "";
                        new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.OrderPayload.Customer.CustomerNo);
                        #endregion

                        if (!IsUnplannedCreatedForFuture)
                        {
                            DateTime todayDate = Convert.ToDateTime(tempStopDate);
                            ViewModels.CustomerDashboard custDash = new CustomerDashboard();
                            PayloadManager.OrderPayload.Customer = custDash.GetNewCustomer(todayDate, PayloadManager.OrderPayload.Customer);
                        }

                    }
                    else
                    {
                        CreditMemoObject.VoidReasonID = SelectedReasonCode.ToString();
                        foreach (ReasonCode item in reasonCode)
                        {
                            if (item.Id == SelectedReasonCode)
                            {
                                CreditMemoObject.VoidReason = item.Code.Trim();
                                break;
                            }
                        }
                        new ARPaymentManager(CreditMemoObject.CustomerNo, CommonNavInfo.Customer.Company).VoidReceiptNew(CreditMemoObject.ReceiptID.ToString(), CreditMemoObject.VoidReason, CreditMemoObject.RouteID, false);
                        //creditManager.UpdateCreditMemoStatus(CreditMemoObject.ReceiptID, CreditMemoObject.CustomerNo, CreditMemoObject.RouteID);
                        LogActivityForCreditMemo(ActivityKey.VoidCreditMemo);
                        CreditMemoObject.ReceiptNumber = -1;
                    }
                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                    //new ReportViewModels.CreditMemoReport(CreditMemoObject);
                    ShowReport(CreditMemoObject);
                    GetCreditMemoList();
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][FinaliseCreditMemo][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][End:FinaliseCreditMemo]");
            });
            #endregion

            #region OpenDialogForVoidCredit

            OpenDialogForVoidCredit = new DelegateCommand((param) =>
            {
                ReasonCode = creditManager.GetVoidReasonCode();
                SelectedReasonCode = -1;

                IsAddCreditMemo = false;
                var dialogWindow = new Helpers.DialogWindow { TemplateKey = "VoidCreditProcess", Title = "Void Credit" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialogWindow, MessageToken);
            });
            #endregion

            #region SelectionItemInvoke

            SelectionItemInvoke = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][Start:SelectionItemInvoke]");
                try
                {
                    if (param == null)
                    {
                        return;
                    }

                    CreditMemoObject = param as CreditProcess;
                    if (CreditMemoObject.StatusCode.Trim() == StatusTypesEnum.USTLD.ToString())
                    {
                        IsVoidButtonEnabled = IsPrintButtonEnabled = true; IsAddCreditMemoEnabled = false;
                    }
                    else if (CreditMemoObject.StatusCode.Trim() == StatusTypesEnum.VOID.ToString() ||
                        CreditMemoObject.StatusCode.Trim() == StatusTypesEnum.STTLD.ToString() ||
                        CreditMemoObject.StatusCode.Trim() == StatusTypesEnum.VDSTLD.ToString())
                    {
                        IsVoidButtonEnabled = false; IsPrintButtonEnabled = true;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][SelectionItemInvoke][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][End:SelectionItemInvoke]");

            });
            #endregion

        }

        public void ShowReport(CreditProcess creditProcess)
        {
            try
            {
                var DocInfo = new ReportEngine.DocumentInformation();
                var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                repEngine.Notify += repEngine_Notify;

                repEngine.Log = this.Logger;
                ReportEngine.IReportSLE ReportData = null;
                var DocumentId = creditProcess.DocumentId;
                var SourceDocumentNo = Convert.ToDecimal(creditProcess.ReceiptID);
                if (DocumentId <= 0)
                {
                    ReportData = new ReportViewModels.CreditMemoReport(creditProcess);
                    Dictionary<string, object> Parameters;
                    Parameters = new Dictionary<string, object>();
                    Parameters.Add("PrintLabels", Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PrintLabels"]));
                    DocInfo = repEngine.Generate<Reporting.CreditMemo>(SourceDocumentNo, true, ReportEngine.ReportCategory.Customer, ReportEngine.ReportsTypes.CreditMemoReport, ReportData, Parameters, ReportEngine.ContentTypes.PDF);
                }
                else
                {
                    DocInfo.DocumentId = DocumentId;
                }

                if (DocInfo.IsEndOfSequence)
                {
                    Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, Please sync to continue" }, MessageToken);
                    return;
                }

                if (DocInfo.IsDocumentCreated)
                    ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.Order_Header", "OrderID", SourceDocumentNo, DocInfo.DocumentId);
                creditProcess.DocumentId = DocInfo.DocumentId; //Replace Latest

                var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Customer, creditProcess.DocumentId, null);
                if (ReportBytes == null)
                {
                    Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Report not generated, Please try again." }, MessageToken);
                    return;
                }
                SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
                Payload.ReportFile = ReportBytes;
                Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                Payload.PreviousView = ViewModelMappings.View.ReturnsAndCredits;
                Payload.CurrentViewTitle = "Credit Memo Report";
                Payload.PreviousViewTitle = "Returns and Credits";
                Payload.MessageToken = this.MessageToken;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.ReturnsAndCredits, CloseCurrentView = true, Payload = Payload, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][Show Report][ExceptionStackTrace = " + ex.Message + "]");
            }
        }

        void repEngine_Notify(string msg)
        {
            System.Windows.MessageBox.Show(msg);
        }

        private void RenderSign()
        {
            SignatureCanvasVM = new CanvasViewModel(Flow.Credits, SignType.CreditProcessSign, CreditMemoObject.ReceiptID)
            {
                Height = 250,
                Width = 300,
                FileName = "CreditMemo.jpg",
                FooterText = "Customer's Signature"
            };
        }
        private async void LoadData()
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][AsyncStart:LoadData]");
                try
                {
                    this.MessageToken = CommonNavInfo.MessageToken;
                    CustomerNo = PayloadManager.OrderPayload.Customer.CustomerNo;
                    CreditMemoObject.CustomerName = PayloadManager.OrderPayload.Customer.Name.Trim();
                    CreditMemoObject.CustomerNo = CustomerNo;
                    GetCreditMemoList();
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][LoadData][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][AsyncEnd:LoadData]");
            });
        }
        public void GetCreditMemoList()
        {
            #region old code
            //Random rd = new Random();

            //string[] status = new string[] { "USTLD", "STTLD", "VOID", "VDSTLD" };
            //string[] reasons = new string[] {"items defective", "customer unhappy", "bla bla"};
            //string[] note = new string[] {"adshf", "sdfhadfhadgadgadf", "sdfasdfasdf"};
            //for (int i = 0; i < 20; i++)
            //{
            //    ReturnsAndCredits rc = new ReturnsAndCredits();
            //    rc.CreditMemoDate = DateTime.Now;
            //    rc.CreditMemoNumber = rd.Next(10000, 434433);
            //    rc.CreditNote = note[rd.Next(0, note.Length - 1)];
            //    rc.CreditReason = reasons[rd.Next(0, reasons.Length - 1)];
            //    rc.CreditAmount = rd.Next(20, 100);
            //    rc.CreditMemoStatusCode = status[rd.Next(0, status.Length - 1)];

            //    CreditsMemoList.Add(rc);
            //}
            #endregion
            //Todo
            //1. get the observable collection in temp collection
            //2.apply the sorting algorithm
            //3.assign that collection to main.


            Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][Start:GetCreditMemoList]");
            try
            {
                CreditsMemoList = creditManager.GetCreditProcessList(CustomerNo);
                SequenceList(CreditsMemoList);
                foreach (CreditProcess item1 in CreditsMemoList)
                {
                    Temp.Add(item1);
                }
                creditsMemoFilteredList = new ObservableCollection<CreditProcess>(CreditsMemoList.ToList<CreditProcess>());
                EnableDisableButtons();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][GetCreditMemoList][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][End:GetCreditMemoList]");
        }

        public void SequenceList(ObservableCollection<CreditProcess> creditsList)
        {
            CreditsMemoList = new ObservableCollection<CreditProcess>(creditsList.OrderBy(x => x.CreditSeqID).ThenByDescending(x => x.CreditMemoDate).ToList());
            //CreditsMemoList = (ObservableCollection<CreditProcess>)TempCollection.OrderBy(x => x.CreditMemoDate);
            //creditsList.OrderBy(x => x.CreditMemoDate);

        }

        public void EnableDisableButtons()
        {
            //TODO
            //1. CHECK OBSERVABLE COLLECTION.
            // 2. ENABLE DISABLE ADD BUTTON ACC. TO THAT

            Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][Start:EnableDisableButtons]");
            try
            {

                IsVoidButtonEnabled = IsPrintButtonEnabled = false;
                if (PayloadManager.OrderPayload.Customer.IsTodaysStop)
                {
                    CreditProcess obj = CreditsMemoList.FirstOrDefault(x => (x.StatusCode.ToLower() == StatusTypesEnum.USTLD.ToString().ToLower()) && (DateTime.Compare(x.CreditMemoDate.Date, DateTime.Now.Date) == 0));
                    if (obj != null)
                    {
                        IsAddCreditMemoEnabled = true;
                    }
                    else
                        IsAddCreditMemoEnabled = true;
                }
                else
                {
                    IsAddCreditMemoEnabled = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][EnableDisableButtons][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CreditProcessViewModel][End:EnableDisableButtons]");
        }
        void CanvasStrokeCollection_StrokesChanged(object sender, StrokeCollectionChangedEventArgs e)
        {
            try
            {
                if (SignatureCanvasVM.CanvasStrokeCollection.Count > 0)
                {
                    ToggleSignAndPrintButton = true;
                }
                else
                {
                    ToggleSignAndPrintButton = false;
                }
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }

        public void LogActivityForCreditMemo(ActivityKey key)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CreateAndMoveStopViewModel][Start:LogActivityForCreditMemo]");
            try
            {
                Activity ac = new Activity
                {
                    CustomerID = PayloadManager.OrderPayload.Customer.CustomerNo.ToString(),
                    ActivityType = key.ToString(),
                    ActivityStart = DateTime.Now,
                    ActivityEnd = DateTime.Now,
                    IsTxActivity = true,
                    ActivityFlowID = CreditMemoObject.ReceiptNumber,
                    ActivityDetailClass = CreditMemoObject.ToString(),
                    ActivityDetails = CreditMemoObject.SerializeToJson(),
                    RouteID = this.RouteID
                };
                if (key.ToString().ToLower() == "CreateCreditMemo".ToString().ToLower())
                {
                    ac.ActivityStatus = "USTLD";
                }
                else if (key.ToString().ToLower() == "VoidCreditMemo".ToString().ToLower())
                {
                    ac.ActivityStatus = "VOID";
                }
                else if (key.ToString().ToLower() == "SettledCreditMemo".ToString().ToLower())
                {
                    ac.ActivityStatus = "STTLD";
                }
                else if (key.ToString().ToLower() == "VDDSettledCreditMemo".ToString().ToLower())
                {
                    ac.ActivityStatus = "VDSTLD";
                }

                //Get the parent activity id
                ac.ActivityHeaderID = this.GetParentActivity(this.CreditMemoObject.ReceiptID.ToString());
                Activity activity = ResourceManager.Transaction.LogActivity(ac);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CreateAndMoveStopViewModel][LogActivityForCreditMemo][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CreateAndMoveStopViewModel][End:LogActivityForCreditMemo]");
        }

        public void Search(string text)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CreateAndMoveStopViewModel][Start:Search]");
            try
            {
                if (text == null) return;
                string searchText = text.ToString().ToLower();
                if (searchText.Trim().Length > 2 || searchText.Trim().Length == 0)
                {
                    RunSearch(searchText);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CreateAndMoveStopViewModel][Search][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CreateAndMoveStopViewModel][End:Search]");
        }
        public void ResetSearch()
        {
            CreditsMemoList = new ObservableCollection<CreditProcess>(creditsMemoFilteredList.ToList<CreditProcess>());
        }
        CancellationTokenSource tokenForCancelTask = null;

        void RunSearch(string searchText)
        {
            IsSearching = true;
            if (tokenForCancelTask != null)
            {
                tokenForCancelTask.Cancel();
            }
            tokenForCancelTask = new CancellationTokenSource();
            Task.Run(async () => await SearchItem(searchText, tokenForCancelTask.Token), tokenForCancelTask.Token);
        }

        private async Task SearchItem(string searchText, CancellationToken token)
        {
            try
            {
                await Task.Run(() =>
                {
                    CreditsMemoList = new ObservableCollection<CreditProcess>(creditsMemoFilteredList.Where(item => (item.ReceiptID.ToString().ToLower().Contains(searchText) || item.CreditReason.ToLower().Contains(searchText) || item.CreditNote.ToLower().Contains(searchText))).ToList<CreditProcess>());
                    IsSearching = false;
                });
            }
            catch (Exception ex)
            {
            }
        }

        private string GetParentActivity(string creditReceiptId)
        {
            string query = "";

            try
            {
                query = string.Format("SELECT TDID FROM BUSDTA.M50012 WHERE TDAN8='{0}' AND TDTYP='CreditMemo' AND TDDTLS LIKE '%\"ReceiptID\":{1}%' AND TDPNTID=0", customerNo, creditReceiptId);

                return DbEngine.ExecuteScalar(query);

            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        #endregion

        //public enum CreditID
        //{

        //    [Description("USTLD")]
        //    ID = 1,
        //    [Description("VOID")]
        //    ID = 2,
        //    [Description("STTLD")]
        //    ID = 3,
        //    [Description("VDSTLD")]
        //    ID = 4
        //}

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}



