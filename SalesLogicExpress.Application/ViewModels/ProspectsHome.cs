﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ViewModels.ProspectViewModels;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Threading;
using Telerik.Windows.Controls;
using Models = SalesLogicExpress.Domain;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;

namespace SalesLogicExpress.Application.ViewModels
{
    public class ProspectsHome : BaseViewModel
    {

        #region Properties and Commands

        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.RouteHome");
        //public string Route{get;set;}
       // string _NoteID, _NoteDetails, _DateTime;
        string _NoteDetails;
       //Sathish Removed Unwanted static variables
        private string customerNoVM = string.Empty;
       
        ProspectManager prospectManager = new ProspectManager();

        public Guid MessagseToken { get; set; }
        public DelegateCommand AddNewExpensed { get; set; }

        public DelegateCommand AddNewSerialized { get; set; }
        public DelegateCommand OpenSerialisedDialog { get; set; }
        public DelegateCommand OpenCategorySelection { get; set; }
        public DelegateCommand OpenExpensedCatSelection { get; set; }
        public DelegateCommand DeleteProspectCoffeeSelected { get; set; }
        public DelegateCommand DeleteSerialisedSelected { get; set; }
        public DelegateCommand OpenExpensedDialog { get; set; }
        public DelegateCommand DeleteExpensedDialog { get; set; }
        public DelegateCommand EditSerializedItem { get; set; }
        public DelegateCommand EditExpensedItem { get; set; }

        public DelegateCommand OpenCoffeeDialog { get; set; }
        public DelegateCommand AddCoffeeProspect { get; set; }
        public DelegateCommand EditCoffeeProspectSelected { get; set; }
        public DelegateCommand DeleteCoffeeProspectSelected { get; set; }
        public DelegateCommand CoffeeGridItemSelectionChanged { get; set; }

        public DelegateCommand OpenAlliedDialog { get; set; }
        public DelegateCommand AddAlliedProspect { get; set; }
        public DelegateCommand EditAlliedProspectSelected { get; set; }
        public DelegateCommand DeleteAlliedProspectSelected { get; set; }
        public DelegateCommand AlliedCategoryChangedCommand { get; set; }

        public DelegateCommand SearchSerialized { get; set; }
        public DelegateCommand SearchCompetitor { get; set; }

        #region ProspectContact
        public DelegateCommand OpenProspectContact { get; set; }
        public DelegateCommand AddProspectContact { get; set; }
        public DelegateCommand EditProspectContact { get; set; }
        public DelegateCommand OpenProspectPhoneContact { get; set; }
        public DelegateCommand AddProspectPhone { get; set; }
        public DelegateCommand EditProspectPhone { get; set; }
        public DelegateCommand SaveEditedProspectPhone { get; set; }
        public DelegateCommand DeleteProspectPhone { get; set; }
        public DelegateCommand OpenProspectEmailContact { get; set; }
        public DelegateCommand AddProspectEmail { get; set; }
        public DelegateCommand EditProspectEmail { get; set; }
        public DelegateCommand SaveEditedProspectEmail { get; set; }
        public DelegateCommand DeleteProspectEmail { get; set; }
        public DelegateCommand DeleteProspectContactFromList { get; set; }
        public DelegateCommand SelectProspectContact { get; set; }
        public DelegateCommand SelectAllProspectContacts { get; set; }
        public DelegateCommand BeforeProspectPhoneChange { get; set; }
        public DelegateCommand AfterProspectPhoneChange { get; set; }
        public DelegateCommand BeforeProspectEmailChange { get; set; }
        public DelegateCommand AfterProspectEmailChange { get; set; }
        ProspectPhone selectedProspectPhone = null;
        ProspectEmail selectedProspectEmail = null;

        private int _Index;
        public int Index
        {
            get
            {
                return _Index;
            }
            set
            {
                _Index = value;
                OnPropertyChanged("Index");
            }
        }
        #endregion

        public DelegateCommand Searchserialized { get; set; }
        public DelegateCommand AddNewProspectNote { get; set; }
        public DelegateCommand SaveNewProspectNote { get; set; }

        private bool _SelectAllCheck = false;
        public bool SelectAllCheck
        {
            get
            {
                return _SelectAllCheck;
            }
            set
            {
                _SelectAllCheck = value;
                OnPropertyChanged("SelectAllCheck");
            }
        }

        bool _IsProspectSelected = false;
        public bool IsProspectSelected
        {
            get
            {
                return _IsProspectSelected;
            }
            set
            {
                _IsProspectSelected = value;
                OnPropertyChanged("IsProspectSelected");
            }
        }

        string _Route, _CurrentDate, _RouteName;

        public string Route
        {
            get
            {
                return _Route;
            }
            set
            {
                _Route = value;
                OnPropertyChanged("Route");
            }
        }

        #region Pr-Trip Inspection Tab related properties
        PreTripInspectionHistoryViewModel _PreTripInspectionHistoryVM = null;
        public PreTripInspectionHistoryViewModel PreTripInspectionHistoryVM
        {
            get
            {
                return _PreTripInspectionHistoryVM;
            }
            set
            {
                _PreTripInspectionHistoryVM = value;
                OnPropertyChanged("PreTripInspectionHistoryVM");
            }
        }
        private bool _IsPreTripInspectionSelected = false;
        public bool IsPreTripInspectionSelected
        {
            get
            {
                return _IsPreTripInspectionSelected;
            }
            set
            {
                _IsPreTripInspectionSelected = value;
                OnPropertyChanged("IsPreTripInspectionSelected");
            }
        }

        private string _ButtonTitle = "Add";
        public string ButtonTitle
        {
            get
            {
                return _ButtonTitle;
            }
            set
            {
                _ButtonTitle = value;
                OnPropertyChanged("ButtonTitle");
            }
        }

        CycleCountViewModel _CycleCountVM = null;
        public CycleCountViewModel CycleCountVM
        {
            get { return _CycleCountVM; }
            set { _CycleCountVM = value; OnPropertyChanged("CycleCountVM"); }
        }

        bool _IsCycleCountSelected = false;
        public bool IsCycleCountSelected
        {
            get { return _IsCycleCountSelected; }
            set
            {
                _IsCycleCountSelected = value;
                OnPropertyChanged("IsCycleCountSelected");
            }
        }

        #endregion

        #region Inventory Tab related properties
        bool _IsInventorySelected = true;
        public bool IsInventorySelected
        {
            get
            {
                return _IsInventorySelected;
            }
            set
            {
                _IsInventorySelected = value;
                if (value && this.inventoryVM == null)
                {
                    if (!HasPendingCountRequests && !HasPendingCountRequestsForPast)
                    {
                        // IsBusy = true;
                        // LoadInventory();
                    }
                }
                else if (value && this.inventoryVM != null)
                {
                    if (PayloadManager.RouteReplenishmentPayload.IsRefreshInventoryTab)
                    {
                        PayloadManager.RouteReplenishmentPayload.IsRefreshInventoryTab = false;
                        IsBusy = true;
                        // LoadInventory();
                    }
                    this.inventoryVM.MessageToken = MessageToken;
                }
                OnPropertyChanged("IsInventorySelected");
            }
        }
        InventoryTabViewModel inventoryVM = null;
        public InventoryTabViewModel InventoryVM
        {
            get
            {
                return this.inventoryVM;
            }
            set
            {
                this.inventoryVM = value;
                OnPropertyChanged("InventoryVM");
            }
        }
        private bool hasPendingCountRequests = false;

        public bool HasPendingCountRequests
        {
            get { return hasPendingCountRequests; }
            set { hasPendingCountRequests = value; OnPropertyChanged("HasPendingCountRequests"); }
        }

        #endregion

        public string CurrentDate
        {
            get
            {
                return _CurrentDate;
            }
            set
            {
                _CurrentDate = value;
                OnPropertyChanged("CurrentDate");
            }
        }
        private string _EquipmentSelected;
        public string EquipmentSelected
        {
            get
            {
                return _EquipmentSelected;
            }
            set
            {
                _EquipmentSelected = value;
                OnPropertyChanged("EquipmentSelected");
            }
        }

        private string _SubCategorySelected;
        public string SubCategorySelected
        {
            get
            {
                return _SubCategorySelected;
            }
            set
            {
                _SubCategorySelected = value;
                OnPropertyChanged("SubCategorySelected");
            }
        }
        private Int32 _EquipmentSelectedID;
        public Int32 EquipmentSelectedID
        {
            get
            {
                return _EquipmentSelectedID;
            }
            set
            {
                _EquipmentSelectedID = value;
                //OnPropertyChanged("QuantitySelected");
            }
        }
        private decimal? _QuantitySelected;
        public decimal? QuantitySelected
        {
            get
            {
                return _QuantitySelected;
            }
            set
            {
                _QuantitySelected = value;
                OnPropertyChanged("QuantitySelected");
            }
        }
        private string _OwnedSelected;
        public string OwnedSelected
        {
            get
            {
                return _OwnedSelected;
            }
            set
            {
                _OwnedSelected = value;
                OnPropertyChanged("OwnedSelected");
            }
        }
        public string RouteName
        {
            get
            {
                return _RouteName;
            }
            set
            {
                _RouteName = value;
                OnPropertyChanged("RouteName");
            }
        }

        #region Replenishment Tab

        private RouteReplenishmentTabViewModel routeReplenishmentTabVM;
        public RouteReplenishmentTabViewModel RouteReplenishmentTabVM
        {
            get { return routeReplenishmentTabVM; }
            set { routeReplenishmentTabVM = value; OnPropertyChanged("RouteReplenishmentTabVM"); }
        }


        private bool isReplenishmentTabSelected = false;
        public bool IsReplenishmentTabSelected
        {
            get { return isReplenishmentTabSelected; }
            set
            {
                isReplenishmentTabSelected = value;
                if (value)
                {
                    if (RouteReplenishmentTabVM == null)
                    {
                        RouteReplenishmentTabVM = new RouteReplenishmentTabViewModel();
                        RouteReplenishmentTabVM.MessageToken = this.MessageToken;
                    }
                    RouteReplenishmentTabVM.IsVisibleRelease = true; RouteReplenishmentTabVM.IsVisibleVoid = true;
                }
                else
                {
                    if (RouteReplenishmentTabVM != null)
                    {
                        RouteReplenishmentTabVM.IsVisibleRelease = false; RouteReplenishmentTabVM.IsVisibleVoid = false;
                    }
                }
                OnPropertyChanged("IsReplenishmentTabSelected");
            }
        }


        private bool hasPendingCountRequestsForPast;
        public bool HasPendingCountRequestsForPast
        {
            get { return hasPendingCountRequestsForPast; }
            set { hasPendingCountRequestsForPast = value; OnPropertyChanged("HasPendingCountRequestsForPast"); }
        }

        private Brush heldItemReturnBGColor = (Brush)new BrushConverter().ConvertFromString("Green");
        public Brush HeldItemReturnBGColor
        {
            get { return heldItemReturnBGColor; }
            set { heldItemReturnBGColor = value; OnPropertyChanged("HeldItemReturnBGColor"); }
        }

        private bool _IsCompetitorsSelected = true;
        public bool IsCompetitorsSelected
        {
            get
            {
                return _IsCompetitorsSelected;
            }
            set
            {
                _IsCompetitorsSelected = value;
                if (value)
                {
                    IsCoffeeTabActive = true;
                }
                OnPropertyChanged("IsCompetitorsSelected");
            }
        }

        private bool _IsCoffeeTabActive = true;
        public bool IsCoffeeTabActive
        {
            get
            {
                return _IsCoffeeTabActive;
            }
            set
            {
                _IsCoffeeTabActive = value;
                if (value)
                {
                    IsAlliedTabActive = false;
                    GetCoffeeProspectForRoute(CoffeeProspectList.Count != _CoffeeProspectListFilter.Count);
                    SearchEquipmentText = string.Empty;
                    OnPropertyChanged("DefaultEquipmentClearSearchText");

                }
                OnPropertyChanged("IsCoffeeTabActive");
            }
        }

        private bool _IsAlliedTabActive = true;
        public bool IsAlliedTabActive
        {
            get
            {
                return _IsAlliedTabActive;
            }
            set
            {
                _IsAlliedTabActive = value;
                if (value)
                {
                    IsCoffeeTabActive = false;
                    GetAlliedProspectForRoute(AlliedProspectList.Count != _AlliedProspectListFilter.Count);
                    SearchEquipmentText = string.Empty;
                    OnPropertyChanged("DefaultEquipmentClearSearchText");
                }
                OnPropertyChanged("IsAlliedTabActive");
            }
        }

        private bool _IsEquipmentSelected = true;
        public bool IsEquipmentSelected
        {
            get
            {
                return _IsEquipmentSelected;

            }
            set
            {
                _IsEquipmentSelected = value;
                if (value)
                {
                    IsSerializedTabActive = true;

                }
                OnPropertyChanged("IsEquipmentSelected");
            }
        }
        //CategoryDropdown
        private string _CategoryDropdown = "true";
        public string CategoryDropdown
        {
            get
            {
                return _CategoryDropdown;
            }
            set
            {
                _CategoryDropdown = value;
                OnPropertyChanged("CategoryDropdown");
            }
        }
        private string _SubCategoryDropdown = "true";
        public string SubCategoryDropdown
        {
            get
            {
                return _SubCategoryDropdown;
            }
            set
            {
                _SubCategoryDropdown = value;
                OnPropertyChanged("SubCategoryDropdown");
            }
        }
        private bool _IsSerializedTabActive = true;
        public bool IsSerializedTabActive
        {
            get
            {
                return _IsSerializedTabActive;

            }
            set
            {
                _IsSerializedTabActive = value;
                if (value)
                {
                    IsExpensedTabActive = false;
                    GetSerializedList(SerializedList.Count != SerializedListFilter.Count);
                    SearchEquipmentText = string.Empty;
                    OnPropertyChanged("DefaultEquipmentClearSearchText");
                }
                OnPropertyChanged("IsSerializedTabActive");
            }
        }

        private int _SerializedProspectsCount = 0;
        public int SerializedProspectsCount
        {
            get { return SerializedList.Count; }
            set { _SerializedProspectsCount = value; OnPropertyChanged("SerializedProspectsCount"); }
        }

        private bool _IsExpensedTabActive = true;
        public bool IsExpensedTabActive
        {
            get
            {
                return _IsExpensedTabActive;

            }
            set
            {
                _IsExpensedTabActive = value;
                if (value)
                {
                    IsSerializedTabActive = false;
                    GetExpensedList(ExpensedList.Count != ExpensedListFilter.Count);
                    SearchEquipmentText = string.Empty;
                    OnPropertyChanged("DefaultEquipmentClearSearchText");
                }
                OnPropertyChanged("IsSerializedTabActive");
            }
        }
        private int _ExpensedProspectsCount = 0;
        public int ExpensedProspectsCount
        {
            get { return ExpensedList.Count; }
            set { _ExpensedProspectsCount = value; OnPropertyChanged("ExpensedProspectsCount"); }
        }

        #endregion

        #endregion

        #region Serialized
        public Models.Customer Customer
        {
            get;
            set;
        }

        public Prospect Prospect
        {
            get;
            set;
        }
        private bool IsValid = false;
        List<string> _CategoryList;
        public List<string> CategoryList
        {
            get
            {
                return _CategoryList;
            }
            set
            {
                _CategoryList = value;
                IsValid = true;
                OnPropertyChanged("CategoryList");
            }
        }
        List<string> _EQOwned;
        public List<string> EQOwned
        {
            get
            {
                return _EQOwned;
            }
            set
            {
                _EQOwned = value;
                OnPropertyChanged("EQOwned");
            }
        }

        private List<string> _SubCategoryList;
        public List<string> SubCategoryList
        {
            get
            {
                return _SubCategoryList;
            }
            set
            {
                _SubCategoryList = value;
                OnPropertyChanged("SubCategoryList");
            }
        }

        private string CategoryText;

        private Prospect _ProspectToAdd = new Prospect();
        public Prospect ProspectToAdd
        {
            get
            {
                return _ProspectToAdd;
            }
            set
            {
                _ProspectToAdd = value;
                OnPropertyChanged("ProspectToAdd");
            }
        }

        private ProspectInformationViewModel _ProspectInformationVM = new ProspectInformationViewModel();
        public ProspectInformationViewModel ProspectInformationVM
        {
            get
            {
                return _ProspectInformationVM;
            }
            set
            {
                _ProspectInformationVM = value;
                OnPropertyChanged("ProspectInformationVM");
            }
        }

        private CustomerDashboard _CustomerDashboardVM = new CustomerDashboard();
        public CustomerDashboard CustomerDashboardVM
        {
            get
            {
                return _CustomerDashboardVM;
            }
            set
            {
                _CustomerDashboardVM = value;
                OnPropertyChanged("CustomerDashboardVM");
            }
        }


        private bool _IsInformationSelected = false;
        public bool IsInformationSelected
        {
            get
            {
                return _IsInformationSelected;
            }
            set
            {
                _IsInformationSelected = value;
                if (value)
                {
                    IsSerializedTabActive = false;
                }
                this._ProspectInformationVM.MessageToken = MessageToken;
                OnPropertyChanged("IsInformationSelected");
            }
        }
        bool _IsSearching = false;
        public bool IsSearching
        {
            get
            {
                return _IsSearching;
            }
            set
            {
                _IsSearching = value;
                OnPropertyChanged("IsSearching");
            }
        }

        private bool _IsEnableProspectDelete = false;
        public bool IsEnableProspectDelete
        {
            get { return _IsEnableProspectDelete; }
            set
            {
                _IsEnableProspectDelete = value;
                OnPropertyChanged("IsEnableProspectDelete");
            }
        }
        private bool _IsEnableProspectEdit = false;
        public bool IsEnableProspectEdit
        {
            get { return _IsEnableProspectEdit; }
            set { _IsEnableProspectEdit = value; OnPropertyChanged("IsEnableProspectEdit"); }
        }

        void ProspectToAdd_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            IsDirty = true;
        }

        void ContactProspect_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            IsDirty = true;
        }

        #region competitors
        #region Coffee

        private bool _IsEnableCoffeeDelete = false;
        public bool IsEnableCoffeeDelete
        {
            get { return _IsEnableCoffeeDelete; }
            set { _IsEnableCoffeeDelete = value; OnPropertyChanged("IsEnableCoffeeDelete"); }
        }
        private bool _IsEnableCoffeeEdit = false;
        public bool IsEnableCoffeeEdit
        {
            get { return _IsEnableCoffeeEdit; }
            set { _IsEnableCoffeeEdit = value; OnPropertyChanged("IsEnableCoffeeEdit"); }
        }

        private CoffeeProspect _CoffeeProspectToAdd = new Models.CoffeeProspect();
        public CoffeeProspect CoffeeProspectToAdd
        {
            get
            {
                return _CoffeeProspectToAdd;
            }
            set
            {
                _CoffeeProspectToAdd = value;
                OnPropertyChanged("CoffeeProspectToAdd");
            }
        }

        void CoffeeProspectToAdd_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            IsDirty = true;
        }

        public int _CoffeeProspectsCount = 0;
        public int CoffeeProspectsCount
        {
            get { return CoffeeProspectList.Count; }
            set { _CoffeeProspectsCount = value; OnPropertyChanged("CoffeeProspectsCount"); }
        }

        private List<string> _competitorList;
        public List<string> CompetitorList
        {
            get
            {
                return _competitorList;
            }
            set
            {
                _competitorList = value;
                OnPropertyChanged("CompetitorList");
            }
        }

        private List<string> _coffeeBlendsList;
        public List<string> CoffeeBlendsList
        {
            get
            {
                return _coffeeBlendsList;
            }
            set
            {
                _coffeeBlendsList = value;
                OnPropertyChanged("CoffeeBlendsList");
            }
        }

        private List<string> _UOMList;
        public List<string> UOMList
        {
            get
            {
                return _UOMList;
            }
            set
            {
                _UOMList = value;
                OnPropertyChanged("CoffeeBlendsList");
            }
        }

        private List<string> _PackSizeList;
        public List<string> PackSizeList
        {
            get
            {
                return _PackSizeList;
            }
            set
            {
                _PackSizeList = value;
                OnPropertyChanged("PackSizeList");
            }
        }

        private List<string> _CSPKLBList;
        public List<string> CSPKLBList
        {
            get
            {
                return _CSPKLBList;
            }
            set
            {
                _CSPKLBList = value;
                OnPropertyChanged("CSPKLBList");
            }
        }

        private List<string> _coffeeVolumeList;
        public List<string> CoffeeVolumeList
        {
            get
            {
                return _coffeeVolumeList;
            }
            set
            {
                _coffeeVolumeList = value;
                OnPropertyChanged("CoffeeVolumeList");
            }
        }

        private List<string> _liqCoffeeList;
        public List<string> LiqCoffeeList
        {
            get
            {
                return _liqCoffeeList;
            }
            set
            {
                _liqCoffeeList = value;
                OnPropertyChanged("CoffeeBlendsList");
            }
        }
        private ObservableCollection<CoffeeProspectVM> _CoffeeProspectListFilter = new ObservableCollection<CoffeeProspectVM>();
        private ObservableCollection<CoffeeProspectVM> _CoffeeProspectList = new ObservableCollection<CoffeeProspectVM>();
        public ObservableCollection<CoffeeProspectVM> CoffeeProspectList
        {
            get
            {
                return _CoffeeProspectList;
            }
            set
            {
                _CoffeeProspectList = value;
                OnPropertyChanged("CoffeeProspectList");
            }
        }
        bool _IsChecked = false;
        public bool IsChecked
        {
            get
            {
                return _IsChecked;
            }
            set
            {
                _IsChecked = value;
                OnPropertyChanged("IsChecked");
            }
        }

        #endregion

        #region Allied

        private bool _IsEnableAlliedDelete = false;
        public bool IsEnableAlliedDelete
        {
            get { return _IsEnableAlliedDelete; }
            set { _IsEnableAlliedDelete = value; OnPropertyChanged("IsEnableAlliedDelete"); }
        }
        private bool _IsEnableAlliedEdit = false;
        public bool IsEnableAlliedEdit
        {
            get { return _IsEnableAlliedEdit; }
            set { _IsEnableAlliedEdit = value; OnPropertyChanged("IsEnableAlliedEdit"); }
        }

        private AlliedProspect _AlliedProspectToAdd = new Models.AlliedProspect();
        public AlliedProspect AlliedProspectToAdd
        {
            get
            {
                return _AlliedProspectToAdd;
            }
            set
            {
                _AlliedProspectToAdd = value;
                OnPropertyChanged("AlliedProspectToAdd");
            }
        }

        //private AlliedProspectVM _SelectedAlliedItem = new AlliedProspectVM();
        //public AlliedProspectVM SelectedAlliedItem
        //{
        //    get
        //    {
        //        return _SelectedAlliedItem;
        //    }
        //    set
        //    {
        //        _SelectedAlliedItem = value;
        //        if (_SelectedAlliedItem != null)
        //        {
        //            this.SelectedAlliedItem.IsSelected = true;
        //        }
        //        OnPropertyChanged("SelectedAlliedItem");
        //    }
        //}

        void AlliedProspectToAdd_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            IsDirty = true;
        }

        private int _AlliedProspectsCount = 0;
        public int AlliedProspectsCount
        {
            get { return AlliedProspectList.Count; }
            set { _AlliedProspectsCount = value; OnPropertyChanged("AlliedProspectsCount"); }
        }

        private List<string> _AlliedCategoryList;
        public List<string> AlliedCategoryList
        {
            get
            {
                return _AlliedCategoryList;
            }
            set
            {
                _AlliedCategoryList = value;
                OnPropertyChanged("AlliedCategoryList");
            }
        }

        private List<string> _AlliedSubCategoryList;
        public List<string> AlliedSubCategoryList
        {
            get
            {
                return _AlliedSubCategoryList;
            }
            set
            {
                _AlliedSubCategoryList = value;
                OnPropertyChanged("AlliedSubCategoryList");
            }
        }

        private List<string> _BrandList;
        public List<string> BrandList
        {
            get
            {
                return _BrandList;
            }
            set
            {
                _BrandList = value;
                OnPropertyChanged("BrandList");
            }
        }

        private List<string> _AvgVolumeList;
        public List<string> AvgVolumeList
        {
            get
            {
                return _AvgVolumeList;
            }
            set
            {
                _AvgVolumeList = value;
                OnPropertyChanged("AvgVolumeList");
            }
        }
        private ObservableCollection<Models.AlliedProspectVM> _AlliedProspectListFilter = new ObservableCollection<Models.AlliedProspectVM>();
        private ObservableCollection<Models.AlliedProspectVM> _AlliedProspectList = new ObservableCollection<Models.AlliedProspectVM>();
        public ObservableCollection<Models.AlliedProspectVM> AlliedProspectList
        {
            get
            {
                return _AlliedProspectList;
            }
            set
            {
                _AlliedProspectList = value;
                OnPropertyChanged("AlliedProspectList");
            }
        }
        #endregion
        #endregion
        #endregion

        #region ProspectContact


        private bool _IsContactSelected = true;
        public bool IsContactSelected
        {
            get
            {
                return _IsContactSelected;
            }
            set
            {
                _IsContactSelected = value;
                if (value)
                {
                    GetProspectContactList();
                }
                OnPropertyChanged("IsContactSelected");
            }
        }

        bool _IsProspectEmailSelected;
        public bool IsSelectedProspectEmail
        {
            get
            {
                return _IsProspectEmailSelected;
            }
            set
            {
                _IsProspectEmailSelected = value;
                OnPropertyChanged("IsSelectedProspectEmail");
            }
        }
        bool _IsExpanded = false;
        public bool IsExpanded
        {
            get
            {
                return _IsExpanded;
            }
            set
            {
                _IsExpanded = value;
                OnPropertyChanged("IsExpanded");
            }
        }
        private bool _ToggleProspectPhoneImg = true;
        public bool ToggleProspectPhoneImg
        {
            get
            {
                return _ToggleProspectPhoneImg;
            }
            set
            {
                _ToggleProspectPhoneImg = value;
                OnPropertyChanged("ToggleProspectPhoneImg");
            }
        }

        private bool _ToggleProspectEmailImg = true;
        public bool ToggleProspectEmailImg
        {
            get
            {
                return _ToggleProspectEmailImg;
            }
            set
            {
                _ToggleProspectEmailImg = value;
                OnPropertyChanged("ToggleProspectEmailImg");
            }
        }
        bool _IsSelectedProspectPhone;
        public bool IsSelectedProspectPhone
        {
            get
            {
                return _IsSelectedProspectPhone;
            }
            set
            {
                _IsSelectedProspectPhone = value;
                OnPropertyChanged("IsSelectedProspectPhone");
            }
        }
        public string FormattedProspectValue
        {
            get
            {
                return Value.Trim();
            }
        }
        public string FormattedEmailValue
        {
            get
            {
                return Value.Trim();
            }
        }
        public string Value { get; set; }
        public string Type { get; set; }
        private ContactProspect _ContactProspect = new ContactProspect();
        public ContactProspect ContactProspect
        {
            get
            {
                return _ContactProspect;
            }
            set
            {
                _ContactProspect = value;
                OnPropertyChanged("_ContactProspect");
            }
        }

        List<string> _PhoneType;
        public List<string> PhoneType
        {
            get
            {
                return _PhoneType;
            }
            set
            {
                _PhoneType = value;
                //IsValid = true;
                OnPropertyChanged("PhoneType");
            }
        }

        List<string> _EmailType;
        public List<string> EmailType
        {
            get
            {
                return _EmailType;
            }
            set
            {
                _EmailType = value;
                //IsValid = true;
                OnPropertyChanged("EmailType");
            }
        }

        string _ProspectContactName;
        public string ProspectContactName
        {
            get
            {
                return _ProspectContactName;
            }
            set
            {
                _ProspectContactName = value;
                OnPropertyChanged("ProspectContactName");
            }
        }

        string _ProspectContactTitle;
        public string ProspectContactTitle
        {
            get
            {
                return _ProspectContactTitle;
            }
            set
            {
                _ProspectContactTitle = value;
                OnPropertyChanged("ProspectContactTitle");
            }
        }

        string _ProspectContactEmailAddress;
        public string ProspectContactEmailAddress
        {
            get
            {
                return _ProspectContactEmailAddress;
            }
            set
            {
                _ProspectContactEmailAddress = value;
                OnPropertyChanged("ProspectContactEmailAddress");
            }
        }

        private string _ProspectContactEmailType;
        public string SelectedEmailType
        {
            get
            {
                return _ProspectContactEmailType;
            }
            set
            {
                _ProspectContactEmailType = value;
                OnPropertyChanged("SelectedEmailType");
            }
        }

        private List<string> _ProspectContactPhoneType;
        public List<string> SelectedPhoneType
        {
            get
            {
                return _ProspectContactPhoneType;
            }
            set
            {
                _ProspectContactPhoneType = value;
                OnPropertyChanged("SelectedPhoneType");
            }
        }
        string _ProspectContactPhoneNumber;
        public string ProspectContactPhoneNumber
        {
            get
            {
                return _ProspectContactPhoneNumber;
            }
            set
            {
                _ProspectContactPhoneNumber = value;
                OnPropertyChanged("ProspectContactPhoneNumber");
            }
        }

        string _ContactID;
        public string ContactID
        {
            get
            {
                return _ContactID;
            }
            set
            {
                _ContactID = value;
                OnPropertyChanged("ContactID");
            }
        }

        #endregion

        private ObservableCollection<Prospect> SerializedListFilter = new ObservableCollection<Prospect>();
        private ObservableCollection<Prospect> _SerializedList = new ObservableCollection<Prospect>();
        public ObservableCollection<Prospect> SerializedList
        {
            get
            {
                return _SerializedList;
            }
            set
            {
                _SerializedList = value;
                OnPropertyChanged("SerializedList");
            }
        }
        private ObservableCollection<Prospect> ExpensedListFilter = new ObservableCollection<Prospect>();
        private ObservableCollection<Prospect> _ExpensedList = new ObservableCollection<Prospect>();
        public ObservableCollection<Prospect> ExpensedList
        {
            get
            {
                return _ExpensedList;
            }
            set
            {
                _ExpensedList = value;
                OnPropertyChanged("ExpensedList");
            }
        }

        #region Add Notes Tab
        //Add Notes Tab
        bool IsEmpty;
        public string NoteDetails
        {
            get
            {
                return _NoteDetails;
            }
            set
            {
                _NoteDetails = value;
                if (string.IsNullOrEmpty(_NoteDetails.Trim()))
                    IsEmpty = true;
                else
                {
                    //    IsEmpty = false;
                    //    ToggleButton = true;
                }
                OnPropertyChanged("NoteDetails");
            }
        }

        private ObservableCollection<Models.CustomerNote> _CustomerNotesList;
        public ObservableCollection<Models.CustomerNote> CustomerNotesList
        {
            get
            {
                return _CustomerNotesList;
            }
            set
            {
                _CustomerNotesList = value;
                OnPropertyChanged("CustomerNotesList");
            }
        }
        //
        #endregion

        #region Clear/Search functionality

        private string _SearchEquipmentText;     //Variable to hold the search text property value 

        /// <summary>
        /// Get or set the search text 
        /// </summary>
        public string SearchEquipmentText
        {
            get
            {
                return _SearchEquipmentText;
            }
            set
            {
                _SearchEquipmentText = value;
                OnPropertyChanged("SearchEquipmentText");
                OnPropertyChanged("ShowLookupEquipmentSearchButton");
                OnPropertyChanged("DefaultEquipmentClearSearchText");
            }
        }

        private TrulyObservableCollection<Models.ContactProspect> _ProspectContacts;
        public TrulyObservableCollection<Models.ContactProspect> ProspectContactsSelected
        {
            get
            {
                return _ProspectContacts;
            }
            set
            {
                _ProspectContacts = value;
                OnPropertyChanged("ProspectContactsSelected");
            }
        }

        private ObservableCollection<ContactProspect> _ProspectContactList = new ObservableCollection<ContactProspect>();
        public ObservableCollection<ContactProspect> ProspectContactList
        {
            get
            {
                return _ProspectContactList;
            }
            set
            {
                _ProspectContactList = value;
                OnPropertyChanged("ProspectContactList");
            }
        }

        private TrulyObservableCollection<Models.ContactProspect> _ContactProspects;
        public TrulyObservableCollection<Models.ContactProspect> ContactProspects
        {
            get
            {
                return _ContactProspects;
            }
            set
            {
                _ContactProspects = value;
                OnPropertyChanged("ContactProspects");
            }
        }
        private int _ProspectsContactCount = 0;
        public int ProspectsContactCount
        {
            get { return ProspectContactList.Count; }
            set { _ProspectsContactCount = value; OnPropertyChanged("ProspectsContactCount"); }
        }


        private string _ShowLookupEquipmentSearchButton = "Hidden";   //Variable to hold the search icon visibility value 

        /// <summary>
        /// Get or set search button visibility value based on search text 
        /// </summary>
        public string ShowLookupEquipmentSearchButton
        {
            get
            {
                if (string.IsNullOrEmpty(this.SearchEquipmentText))
                {
                    _ShowLookupEquipmentSearchButton = "Visible";
                }
                else
                {
                    _ShowLookupEquipmentSearchButton = "Hidden";
                }

                return _ShowLookupEquipmentSearchButton;
            }
            set
            {
                _ShowLookupEquipmentSearchButton = value;
                OnPropertyChanged("ShowLookupEquipmentSearchButton ");
            }
        }

        private string _DefaultEquipmentClearSearchText = "";//Variable holds the default watermark text 

        /// <summary>
        /// Get or set deault watermark search text based on search text 
        /// </summary>
        public string DefaultEquipmentClearSearchText
        {
            get
            {
                if (string.IsNullOrEmpty(this.SearchEquipmentText))
                    _DefaultEquipmentClearSearchText = "Enter search text ...";
                else
                    _DefaultEquipmentClearSearchText = "";

                return _DefaultEquipmentClearSearchText;
            }
            set
            {
                _DefaultEquipmentClearSearchText = value;
                OnPropertyChanged("DefaultEquipmentClearSearchText");
            }
        }

        #endregion

        #region Properties & Variables
        string selectedTab = string.Empty;

        #endregion

        #region OnPropertyChanged

        #region Bool
        private bool isDashboardTabSelected = false;

        public bool IsDashboardTabSelected
        {
            get { return isDashboardTabSelected; }
            set
            {
                isDashboardTabSelected = value;
                if (IsDashboardTabSelected)
                {
                    //if (DashboardVM == null)
                    {
                        DashboardVM = new DashboardViewModel(MessageToken, this);
                    }
                    selectedTab = "Dashboard";
                    IsBusy = false;
                }
                OnPropertyChanged("IsDashboardTabSelected");
            }
        }
        private bool isNotesTabSelected = false;

        public bool IsNotesTabSelected
        {
            get { return isNotesTabSelected; }
            set
            {
                PayloadManager.ProspectPayload.IsProspect = true;
                isNotesTabSelected = value; OnPropertyChanged("IsNotesTabSelected");
                if (IsNotesTabSelected)
                {
                    if (NotesVM == null)
                    {
                        NotesVM = new NotesViewModel(MessageToken, this);
                    }
                    NotesVM.ViewModelUpdated += NotesVM_ViewModelUpdated;
                    selectedTab = "Notes";
                    IsBusy = false;

                }
            }
        }




        bool isProspectTabSelected;
        public bool IsProspectTabSelected
        {
            get
            {
                return isProspectTabSelected;
            }
            set
            {
                isProspectTabSelected = value;
                if (isProspectTabSelected && ProspectActivityVM == null)
                {
                    ProspectActivityVM = new ProspectActivityViewModel(MessageToken, this);
                    //ProspectActivityVM = new ProspectActivityViewModel();
                    selectedTab = "Activity";
                    IsBusy = false;

                }
                OnPropertyChanged("IsProspectTabSelected");
            }
        }

        private bool isInformationTabSelected = false;

        public bool IsInformationTabSelected
        {
            get { return isInformationTabSelected; }
            set
            {
                isInformationTabSelected = value; OnPropertyChanged("IsInformationTabSelected");
                if (IsInformationTabSelected)
                {
                    if (InformationVM == null)
                    {
                        InformationVM = new InformationViewModel(MessageToken, this);
                    }
                    selectedTab = "Information";
                    IsBusy = false;

                }
            }
        }
        private bool isContactTabSelected = false;

        public bool IsContactTabSelected
        {
            get { return isContactTabSelected; }
            set
            {
                isContactTabSelected = value; OnPropertyChanged("IsContactTabSelected");
                GetProspectContactList();
                if (IsContactTabSelected)
                {
                    if (ContactVM == null)
                    {
                        ContactVM = new ContactViewModel(MessageToken, this);
                    }
                    selectedTab = "Contact";
                    IsBusy = false;

                }
            }
        }
        private bool isQuotesTabSelected = false;

        public bool IsQuotesTabSelected
        {
            get { return isQuotesTabSelected; }
            set
            {
                PayloadManager.ProspectPayload.IsProspect = true;
                isQuotesTabSelected = value; OnPropertyChanged("IsQuotesTabSelected");
                if (IsQuotesTabSelected)
                {
                    if (QuotesVM == null)
                    {
                        QuotesVM = new CustomerQuoteTabViewModel(MessageToken, true, this);
                    }
                    selectedTab = "Quotes";
                    IsBusy = false;

                }

            }
        }
        private bool isPricingTabSelected = false;

        public bool IsPricingTabSelected
        {
            get { return isPricingTabSelected; }
            set
            {
                isPricingTabSelected = value; OnPropertyChanged("IsPricingTabSelected");
                if (IsPricingTabSelected)
                {
                    PayloadManager.ProspectPayload.IsProspect = true;
                    if (PricingVM == null)
                    {
                        PricingVM = new PricingViewModel(MessageToken, this);
                    }
                    selectedTab = "Pricing";
                    IsBusy = false;
                   
                }
                else if (!IsPricingTabSelected && IsDirty && PricingVM != null)
                {
                    //Call Model Popup
                    ModalPopupOnTabChange("Pricing", selectedTab.Clone().ToString());
                }
            }
        }
        private bool isCompetitorInformationTabSelected = false;

        public bool IsCompetitorInformationTabSelected
        {
            get { return isCompetitorInformationTabSelected; }
            set
            {
                isCompetitorInformationTabSelected = value; OnPropertyChanged("IsCompetitorInformationTabSelected");
                if (IsCompetitorInformationTabSelected)
                {
                    if (CompetitorInformationVM == null)
                    {
                        CompetitorInformationVM = new CompetitorInformationViewModel(MessageToken, this);
                    }
                    selectedTab = "CompetitorInformation";
                    IsBusy = false;

                }
            }
        }

        #endregion

        #region Int

        #endregion

        #region string


        #endregion

        #region Date

        #endregion

        #region Collection


        ProspectActivityViewModel prospectActivityVM;
        public ProspectActivityViewModel ProspectActivityVM
        {
            get
            {
                return prospectActivityVM;
            }
            set
            {


                prospectActivityVM = value;
                OnPropertyChanged("ProspectActivityVM");
            }
        }

        private DashboardViewModel dashboardVM = null;

        public DashboardViewModel DashboardVM
        {
            get { return dashboardVM; }
            set { dashboardVM = value; OnPropertyChanged("DashboardVM"); }
        }
        private NotesViewModel notesVM = null;

        public NotesViewModel NotesVM
        {
            get { return notesVM; }
            set { notesVM = value; OnPropertyChanged("NotesVM"); }
        }
        private InformationViewModel informationVM = null;

        public InformationViewModel InformationVM
        {
            get { return informationVM; }
            set { informationVM = value; OnPropertyChanged("InformationVM"); }
        }


        private ContactViewModel contactVM = null;

        public ContactViewModel ContactVM
        {
            get { return contactVM; }
            set { contactVM = value; OnPropertyChanged("ContactVM"); }
        }
        private PricingViewModel pricingVM = null;

        public PricingViewModel PricingVM
        {
            get { return pricingVM; }
            set { pricingVM = value; OnPropertyChanged("PricingVM"); }
        }
        private CustomerQuoteTabViewModel quotesVM = null;

        public CustomerQuoteTabViewModel QuotesVM
        {
            get { return quotesVM; }
            set { quotesVM = value; OnPropertyChanged("QuotesVM"); }
        }
        private CompetitorInformationViewModel competitorInformationVM = null;

        public CompetitorInformationViewModel CompetitorInformationVM
        {
            get { return competitorInformationVM; }
            set { competitorInformationVM = value; OnPropertyChanged("CompetitorInformationVM"); }
        }

        private CustomerQuoteViewModel customerQuoteVM = null;

        public CustomerQuoteViewModel CustomerQuoteVM
        {
            get { return customerQuoteVM; }
            set { customerQuoteVM = value; }
        }


        #endregion

        #endregion

        public ProspectsHome()
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Start:Constructor]");
            try
            {
                PayloadManager.ApplicationPayload.ViewModelLoadComplete = false;

                //Prospect = PayloadManager.OrderPayload.Prospect.Clone();
                Prospect = PayloadManager.ProspectPayload.Prospect;
                Customer = PayloadManager.OrderPayload.Customer.Clone();

                Route = PayloadManager.ApplicationPayload.Route;
                InitializeCommands();
                LoadData();

                ResourceManager.Synchronization.SyncProgressChanged -= Synchronization_SyncProgressChanged;
                ResourceManager.Synchronization.SyncProgressChanged += Synchronization_SyncProgressChanged;
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Constructor][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][End:Constructor]");
        }

        void Synchronization_SyncProgressChanged(object sender, SyncUpdatedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Start:Synchronization_SyncProgressChanged]");

            try
            {
                if (e.State == SyncUpdateType.DownloadComplete)
                    ToggleTabsLock();
                if (!HasPendingCountRequests && !HasPendingCountRequestsForPast && IsInventorySelected)
                {
                    LoadInventory();
                }
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Synchronization_SyncProgressChanged][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome]End:Synchronization_SyncProgressChanged]");

        }
        async void LoadInventory()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Start:LoadInventory]");
            await Task.Run(() =>
            {
                InventoryVM = new InventoryTabViewModel();
                InventoryVM.IsSearchVisible = true;
                InventoryVM.ParentViewModel = this;
                InventoryVM.MessageToken = this.MessageToken;
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][End:LoadInventory]");
            });
        }
        async void GetSerializedList(bool IsRefresh = false)
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Start:GetSerializedList]");
                try
                {
                    if (SerializedList.Count == 0 || IsRefresh)
                    {
                        SerializedList = new ProspectManager().GetEquipmentDetails(Prospect.ProspectID);
                        SerializedListFilter = SerializedList;
                        OnPropertyChanged("SerializedProspectsCount");
                    }
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                    {

                    }));
                    IsBusy = false;
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ProspectsHome][GetSerializedList][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][End:GetSerializedList]");

            });
        }

        async void GetProspectContactList(bool IsRefresh = false)
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Start:GetProspectContactList]");
                try
                {
                    if (ProspectContactList.Count == 0 || IsRefresh)
                    {
                        ProspectContactList = new ProspectManager().GetProspectContactList(Prospect.ProspectID);
                        OnPropertyChanged("ProspectsContactCount");
                    }
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                    {

                    }));
                    IsBusy = false;
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ProspectsHome][GetProspectContactList][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][End:GetProspectContactList]");
            });
        }
        async void GetExpensedList(bool IsRefresh = false)
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Start:GetExpensedList]");
                try
                {
                    if (ExpensedList.Count == 0 || IsRefresh)
                    {
                        ExpensedList = new ProspectManager().GetExpensedDetails(Prospect.ProspectID);
                        ExpensedListFilter = ExpensedList;
                        OnPropertyChanged("ExpensedProspectsCount");
                    }
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                    {

                    }));
                    IsBusy = false;
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ProspectsHome][GetExpensedList][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][End:GetExpensedList]");

            });
        }


        public void NavigateRouteSettlement()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Start:NavigateRouteSettlement]");
            try
            {
                //RouteSettlementViewModel.SelectedCalendarDate = DateTime.Now;
                ViewModelPayload.PayloadManager.RouteSettlementPayload.IsSettlementTabActive = false;
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteSettlement, CurrentViewName = ViewModelMappings.View.RouteHome, CloseCurrentView = false, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][ProspectsHome][NavigateRouteSettlement][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][End:NavigateRouteSettlement]");
        }
        async void LoadData()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Start:LoadData]");
            try
            {
                await Task.Run(() =>
                    {
                        RouteName = Managers.UserManager.RouteName;
                        CurrentDate = DateTime.Now.ToString("dddd, MM/dd/yyyy");
                        ToggleTabsLock();
                        ResourceManager.StatusTypes = ResourceManager.StatusTypes == null || !ResourceManager.StatusTypes.Any() ? new InventoryManager().GetStatusTypes() : ResourceManager.StatusTypes;
                        PayloadManager.ApplicationPayload.ViewModelLoadComplete = true;
                    });
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][ProspectsHome][LoadData][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][End:LoadData]");
        }

        private void ToggleTabsLock()
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Start:ToggleTabsLock]");
                CycleCountManager CCManager = new CycleCountManager();
                var statusDict = CCManager.GetPendingCycleCountStatus();
                if (statusDict != null && statusDict.Count() > 0)
                {
                    HasPendingCountRequests = statusDict["PartialLock"] || statusDict["FullLock"] ? true : false;
                    HasPendingCountRequestsForPast = statusDict["FullLock"] ? true : false;
                }
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][ProspectsHome][LoadData][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][End:LoadData]");
        }

        void InitializeCommands()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Start:InitializeCommands]");
            #region Serialized
            AddNewSerialized = new DelegateCommand((param) =>
            {
                try
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:AddNewProspect][Param =" + param + "]");

                    bool IsSaved = Convert.ToBoolean(new ProspectManager().AddNewSerialized(ProspectToAdd, Route, Prospect.ProspectID));

                    if (IsSaved)
                    {
                        IsDirty = false;
                        Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                        GetSerializedList(true);
                    }
                    else
                    {
                        Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "EquipmentCategory already exists" }, MessageToken);
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:AddNewProspect][Param =" + param + "]");

                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][AddNewProspect][ExceptionStackTrace = " + ex.StackTrace + "]");
                    throw;
                }
            });

            AddNewExpensed = new DelegateCommand((param) =>
            {
                try
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:AddNewProspect][Param =" + param + "]");

                    bool IsSaved = Convert.ToBoolean(new ProspectManager().AddNewExpensed(ProspectToAdd, Route, Prospect.ProspectID));

                    if (IsSaved)
                    {
                        IsDirty = false;
                        Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                        GetExpensedList(true);
                    }
                    else
                    {
                        Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "EquipmentCategory already exists" }, MessageToken);
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:AddNewProspect][Param =" + param + "]");
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][AddNewProspect][ExceptionStackTrace = " + ex.StackTrace + "]");
                    throw;
                }
            });

            #region OpenSerialisedPopup
            OpenSerialisedDialog = new DelegateCommand((param) =>
            {
                CategoryList = prospectManager.GetCategoryList("1");
                EQOwned = prospectManager.GetEQOwned();
                //OnPropertyChanged("CategoryText");
                SubCategoryList = prospectManager.GetSubCategoryList("", "1"); //For Serialized Equipment Type = 1

                ProspectToAdd = new Prospect();
                ProspectToAdd.ButtonTitle = "Add";
                ProspectToAdd.CategoryDropdown = "True";
                ProspectToAdd.SubCategoryDropdown = "True";

                ProspectToAdd.PropertyChanged += ProspectToAdd_PropertyChanged;
                var dialog = new Helpers.DialogWindow { TemplateKey = "AddSerialised", Title = "Create Serialized Item", Payload = this };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });
            OpenCategorySelection = new DelegateCommand((param) =>
            {
                string EquipmentType = "1";  //For Serialized Equipment Type = 1
                SubCategoryList = prospectManager.GetSubCategoryList(ProspectToAdd.EquipmentSelected, EquipmentType);
            });
            OpenExpensedCatSelection = new DelegateCommand((param) =>
            {
                string EquipmentType = "2"; //For Expensed Equipment Type =2
                SubCategoryList = prospectManager.GetSubCategoryList(ProspectToAdd.EquipmentSelected, EquipmentType);
            });
            OpenExpensedDialog = new DelegateCommand((param) =>
            {
                CategoryList = prospectManager.GetCategoryList("2"); //For Expensed Equipment Type =2
                EQOwned = prospectManager.GetEQOwned();
                //OnPropertyChanged("CategoryText");
                SubCategoryList = prospectManager.GetSubCategoryList("", "2");//For Expensed Equipment Type =2
                ProspectToAdd = new Prospect();
                ProspectToAdd.ButtonTitle = "Add";
                ProspectToAdd.CategoryDropdown = "True";
                ProspectToAdd.SubCategoryDropdown = "True";

                ProspectToAdd.PropertyChanged += ProspectToAdd_PropertyChanged;
                var dialog = new Helpers.DialogWindow { TemplateKey = "AddExpensed", Title = "Create Expensed Item", Payload = this };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);

            });
            #endregion
            DeleteSerialisedSelected = new DelegateCommand((Items) =>
            {
                List<Models.Prospect> cofList = SerializedList.Where(cof => cof.IsSelected == true).ToList<Models.Prospect>();

                int selectedSerializedProspect = cofList.Count();

                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Competitor.DeleteSerProspectConfirmationForMany, MessageIcon = "Alert", Confirmed = false };
                switch (selectedSerializedProspect)
                {
                    case 0:
                        var alertMessage = new Helpers.AlertWindow { Message = "Please select a Serialized Item to delete!", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        //return;
                        break;
                    case 1:
                        confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Competitor.DeleteSerProspectConfirmation, MessageIcon = "Alert", Confirmed = false };
                        Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);

                        break;
                    default:
                        confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Competitor.DeleteSerProspectConfirmationForMany, MessageIcon = "Alert", Confirmed = false };
                        Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                        break;
                }

                if (!confirmMessage.Confirmed) return;

                Managers.ProspectManager prospectsManager = new Managers.ProspectManager();
                Models.Prospect prospect = new Models.Prospect();

                for (int index = 0; index < selectedSerializedProspect; index++)
                {
                    prospect.EquipmentID = cofList[index].EquipmentID;
                    prospectsManager.RemoveSerializedProspect(prospect);
                    SerializedList.Remove(SerializedList.ElementAt(SerializedList.IndexOf(SerializedList.First(note => note.EquipmentID == prospect.EquipmentID))));
                }
            });
            DeleteExpensedDialog = new DelegateCommand((Items) =>
            {
                List<Models.Prospect> cofList = ExpensedList.Where(cof => cof.IsSelected == true).ToList<Models.Prospect>();

                int selectedSerializedProspect = cofList.Count();

                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Competitor.DeleteExpProspectConfirmationForMany, MessageIcon = "Alert", Confirmed = false };
                switch (selectedSerializedProspect)
                {
                    case 0:
                        var alertMessage = new Helpers.AlertWindow { Message = "Please select a Expensed Item to delete!", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        //return;
                        break;
                    case 1:
                        confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Competitor.DeleteExpProspectConfirmation, MessageIcon = "Alert", Confirmed = false };
                        Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);

                        break;
                    default:
                        confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Competitor.DeleteExpProspectConfirmationForMany, MessageIcon = "Alert", Confirmed = false };
                        Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                        break;
                }

                if (!confirmMessage.Confirmed) return;

                Managers.ProspectManager prospectManager = new Managers.ProspectManager();
                Models.Prospect prospect = new Models.Prospect();

                for (int index = 0; index < selectedSerializedProspect; index++)
                {
                    prospect.EquipmentID = cofList[index].EquipmentID;
                    prospectManager.RemoveExpensedProspect(prospect);
                    ExpensedList.Remove(ExpensedList.ElementAt(ExpensedList.IndexOf(ExpensedList.First(note => note.EquipmentID == prospect.EquipmentID))));
                }
            });

            EditSerializedItem = new DelegateCommand((Items) =>
            {
                List<Models.Prospect> cofList = SerializedList.Where(cof => cof.IsSelected == true).ToList<Models.Prospect>();
                Prospect Prosplist = cofList.FirstOrDefault();
                ProspectToAdd = new Prospect();
                CategoryList = prospectManager.GetCategoryList("1"); //For Serialized Equipment Type =1
                EQOwned = prospectManager.GetEQOwned();
                SubCategoryList = prospectManager.GetSubCategoryList("", "1"); //For Serialized Equipment Type =1
                //OnPropertyChanged("CategoryText");
                ProspectToAdd.EquipmentSelected = Prosplist.EquipmentCategory.Trim();
                ProspectToAdd.SubCategorySelected = Prosplist.EquipmentSubCategory.Trim();
                ProspectToAdd.QuantitySelected = Prosplist.EquipmentQuantity;
                ProspectToAdd.OwnedSelected = Prosplist.EquipmentOwned;
                ProspectToAdd.EquipmentSelectedID = Prosplist.EquipmentID;
                ProspectToAdd.ButtonTitle = "Save";
                ProspectToAdd.CategoryDropdown = "False";
                ProspectToAdd.SubCategoryDropdown = "False";

                ProspectToAdd.PropertyChanged += ProspectToAdd_PropertyChanged;

                var dialog = new Helpers.DialogWindow { TemplateKey = "AddSerialised", Title = "Edit Serialised Item" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });

            EditExpensedItem = new DelegateCommand((param) =>
            {
                List<Models.Prospect> cofList = ExpensedList.Where(cof => cof.IsSelected == true).ToList<Models.Prospect>();
                Prospect Expensedlist = cofList.FirstOrDefault();
                int selectedSerializedProspect = cofList.Count();

                ProspectToAdd = new Prospect();
                CategoryList = prospectManager.GetCategoryList("2"); //For Expensed Equipment Type =2
                EQOwned = prospectManager.GetEQOwned();
                SubCategoryList = prospectManager.GetSubCategoryList("", "2"); //For Expensed Equipment Type =2
                ProspectToAdd.EquipmentSelected = Expensedlist.EquipmentCategory.Trim();
                ProspectToAdd.SubCategorySelected = Expensedlist.EquipmentSubCategory.Trim();
                ProspectToAdd.QuantitySelected = Expensedlist.EquipmentQuantity;
                ProspectToAdd.OwnedSelected = Expensedlist.EquipmentOwned;
                ProspectToAdd.EquipmentSelectedID = Expensedlist.EquipmentID;
                ProspectToAdd.ButtonTitle = "Save";
                ProspectToAdd.CategoryDropdown = "False";
                ProspectToAdd.SubCategoryDropdown = "False";

                ProspectToAdd.PropertyChanged += ProspectToAdd_PropertyChanged;
                var dialog = new Helpers.DialogWindow { TemplateKey = "AddExpensed", Title = "Edit Expensed Item" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);

            });
            #endregion

            #region SearchSerialized Item
            SearchSerialized = new DelegateCommand((searchTerm) =>
            {
                if (IsSerializedTabActive)
                {
                    SerializedList = SerializedListFilter.Where(x => x.EquipmentCategory.Contains(SearchEquipmentText)).ToObservableCollection();
                }
                else if (IsExpensedTabActive)
                {
                    ExpensedList = ExpensedListFilter.Where(x => x.EquipmentCategory.Contains(SearchEquipmentText)).ToObservableCollection();
                }
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                {

                }));
                IsBusy = false;
                //
            });
            #endregion

            #region Competitors
            ProspectManager objProspectManager = new ProspectManager();
            #region AddCoffeeProspect
            AddCoffeeProspect = new DelegateCommand((param) =>
            {
                try
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:AddNewProspect][Param =" + param + "]");
                    if (CoffeeProspectToAdd.CompetitorSelected != string.Empty)
                    {

                        bool IsSaved = Convert.ToBoolean(new ProspectManager().AddNewCoffeeProspect(CoffeeProspectToAdd, Route, Prospect.ProspectID));

                        if (IsSaved)
                        {
                            IsDirty = false;
                            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                            GetCoffeeProspectForRoute(true);

                        }
                        else
                        {
                            Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Prospect name already exists" }, MessageToken);
                        }
                    }
                    else
                    {
                        Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Please choose a coffee product" }, MessageToken);
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:AddNewProspect][Param =" + param + "]");


                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][AddNewProspect][ExceptionStackTrace = " + ex.StackTrace + "]");
                    throw;
                }
            });
            #endregion
            #region OpenCoffeeDialog
            OpenCoffeeDialog = new DelegateCommand((param) =>
            {
                CoffeeProspectToAdd = new CoffeeProspect();
                _competitorList = objProspectManager.GetCompetitiors();
                _coffeeBlendsList = objProspectManager.GetCoffeeBlends();
                _UOMList = objProspectManager.GetUOMs();
                _PackSizeList = objProspectManager.GetPackSize();
                _CSPKLBList = objProspectManager.GetCsPkLb();
                _coffeeVolumeList = objProspectManager.GetCoffeeVolumes();
                _liqCoffeeList = objProspectManager.GetLiqCoffees();

                CoffeeProspectToAdd.ButtonTitle = "Add";
                CoffeeProspectToAdd.CompetitorDropDown = "True";
                CoffeeProspectToAdd.CoffeeBlendsDropDown = "True";

                CoffeeProspectToAdd.PropertyChanged += CoffeeProspectToAdd_PropertyChanged;
                var dialog = new Helpers.DialogWindow { TemplateKey = "AddCoffee", Title = "Add Coffee Product" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });
            #endregion
            #region EditCoffeeProspectSelected
            EditCoffeeProspectSelected = new DelegateCommand((param) =>
            {
                List<CoffeeProspectVM> cofList = CoffeeProspectList.Where(cof => cof.IsSelected == true).ToList<CoffeeProspectVM>();
                CoffeeProspectVM cofProd = cofList.FirstOrDefault();
                int selectedCoffeeProspect = cofList.Count();

                switch (selectedCoffeeProspect)
                {
                    case 0:
                        var alertMessage = new Helpers.AlertWindow { Message = "Please select a coffee product to edit!", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        break;
                    case 1:
                        break;
                }

                CoffeeProspectToAdd = new CoffeeProspect();
                _competitorList = objProspectManager.GetCompetitiors();
                _coffeeBlendsList = objProspectManager.GetCoffeeBlends();
                _UOMList = objProspectManager.GetUOMs();
                _PackSizeList = objProspectManager.GetPackSize();
                _CSPKLBList = objProspectManager.GetCsPkLb();
                _coffeeVolumeList = objProspectManager.GetCoffeeVolumes();
                _liqCoffeeList = objProspectManager.GetLiqCoffees();

                CoffeeProspectToAdd.CoffeeProspectID = cofProd.CoffeeProspectID;
                CoffeeProspectToAdd.CompetitorSelected = cofProd.Competitor;
                CoffeeProspectToAdd.CoffeeBlendSelected = cofProd.CoffeeBlend;
                CoffeeProspectToAdd.PackSizeSelected = cofProd.PackSizeText;
                CoffeeProspectToAdd.CSPKLBSelected = cofProd.CS_PK_LBText;
                CoffeeProspectToAdd.Price = cofProd.Price;
                CoffeeProspectToAdd.CoffeeVolumeSelected = cofProd.CoffeeVolume;
                CoffeeProspectToAdd.LIQCoffeeSelected = cofProd.LiqCoffeeType;
                CoffeeProspectToAdd.ButtonTitle = "Save";
                CoffeeProspectToAdd.CompetitorDropDown = "False";
                CoffeeProspectToAdd.CoffeeBlendsDropDown = "True";

                CoffeeProspectToAdd.PropertyChanged += CoffeeProspectToAdd_PropertyChanged;
                var dialog = new Helpers.DialogWindow { TemplateKey = "AddCoffee", Title = "Edit Coffee Product" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });
            #endregion
            #region DeleteCoffeeProspectSelected
            DeleteCoffeeProspectSelected = new DelegateCommand((param) =>
            {
                List<Models.CoffeeProspectVM> cofList = CoffeeProspectList.Where(cof => cof.IsSelected == true).ToList<Models.CoffeeProspectVM>();

                int selectedCoffeeProspect = cofList.Count();

                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Competitor.DeleteProspectConfirmationForMany, MessageIcon = "Alert", Confirmed = false };
                switch (selectedCoffeeProspect)
                {
                    case 0:
                        var alertMessage = new Helpers.AlertWindow { Message = "Please select prospects to delete!", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        //return;
                        break;
                    case 1:
                        confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Competitor.DeleteProspectConfirmation, MessageIcon = "Alert", Confirmed = false };
                        Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);

                        break;
                    default:
                        confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Competitor.DeleteProspectConfirmationForMany, MessageIcon = "Alert", Confirmed = false };
                        Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                        break;
                }

                if (!confirmMessage.Confirmed) return;

                Managers.ProspectManager prospectManager = new Managers.ProspectManager();
                Models.CoffeeProspect prospect = new Models.CoffeeProspect();

                for (int index = 0; index < selectedCoffeeProspect; index++)
                {
                    prospect.CoffeeProspectID = cofList[index].CoffeeProspectID;
                    prospectManager.RemoveNewCoffeeProspect(prospect);
                    CoffeeProspectList.Remove(CoffeeProspectList.ElementAt(CoffeeProspectList.IndexOf(CoffeeProspectList.First(note => note.CoffeeProspectID == prospect.CoffeeProspectID))));
                }
            });
            #endregion
            #region CoffeeGridItemSelectionChanged
            CoffeeGridItemSelectionChanged = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    CoffeeProspectVM objCoffeeItem = (CoffeeProspectVM)param;
                    CoffeeProspectVM objCoffee = CoffeeProspectList.Where(cof => cof.CoffeeProspectID == objCoffeeItem.CoffeeProspectID).FirstOrDefault();
                    objCoffee.IsSelected = objCoffee.IsSelected == true ? false : true;
                }
            });
            #endregion

            #region AddAlliedProspect
            AddAlliedProspect = new DelegateCommand((param) =>
            {
                try
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:AddNewProspect][Param =" + param + "]");
                    if (AlliedProspectToAdd.CompetitorSelected != string.Empty)
                    {
                        bool IsSaved = Convert.ToBoolean(new ProspectManager().AddNewAlliedProspect(AlliedProspectToAdd, Route, Prospect.ProspectID));

                        if (IsSaved)
                        {
                            IsDirty = false;
                            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                            GetAlliedProspectForRoute(true);

                        }
                        else
                        {
                            Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Prospect name already exists" }, MessageToken);
                        }
                    }
                    else
                    {
                        Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Please choose an allied product" }, MessageToken);
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:AddNewProspect][Param =" + param + "]");


                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][AddNewProspect][ExceptionStackTrace = " + ex.StackTrace + "]");
                    throw;
                }
            });
            #endregion
            #region OpenAlliedDialog
            OpenAlliedDialog = new DelegateCommand((param) =>
            {
                AlliedProspectToAdd = new AlliedProspect();
                _competitorList = objProspectManager.GetAlliedCompetitiors();
                _AlliedCategoryList = objProspectManager.GetAlliedCategory();
                _AlliedSubCategoryList = objProspectManager.GetAlliedSubCategory("");
                _BrandList = objProspectManager.GetAlliedBrand();
                _PackSizeList = objProspectManager.GetAlliedPackSize();
                _CSPKLBList = objProspectManager.GetAlliedCsPkLb();
                _AvgVolumeList = objProspectManager.GetAlliedVolumes();
                AlliedProspectToAdd.ButtonTitle = "Add";
                AlliedProspectToAdd.CompetitorDropDown = "True";
                AlliedProspectToAdd.CategoryDropDown = "True";
                AlliedProspectToAdd.SubCategoryDropDown = "True";

                AlliedProspectToAdd.PropertyChanged += AlliedProspectToAdd_PropertyChanged;
                var dialog = new Helpers.DialogWindow { TemplateKey = "AddAllied", Title = "Add Allied Product" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });
            #endregion

            #region ProspectContacts
            #region OpenProspectContact
            OpenProspectContact = new DelegateCommand((param) =>
            {
                _ContactProspect = new ContactProspect();
                PhoneType = prospectManager.GetPhoneTypeList();
                EmailType = prospectManager.GetEmailTypeList();
                ContactProspect.IsAddContact = true;
                ContactProspect.IsAddPhone = ContactProspect.IsAddEmail = false;
                ContactProspect.ButtonTitle = "Add";
                var dialog = new Helpers.DialogWindow { TemplateKey = "AddProspectContact", Title = "Add New Contact", Payload = this };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);

            });
            #endregion

            #region OpenProspectPhoneContact
            OpenProspectPhoneContact = new DelegateCommand((param) =>
            {
                ContactProspect ContactToAddphone = (ContactProspect)param;
                ContactProspect.Name = ContactToAddphone.Name;
                ContactProspect.Title = ContactToAddphone.Title;
                PhoneType = prospectManager.GetPhoneTypeList();
                ContactProspect.ContactID = ContactToAddphone.ContactID;
                ContactProspect.PhoneList = ContactToAddphone.PhoneList;
                ContactProspect.Phone1 = "";
                ContactProspect.Phone2 = "";
                ContactProspect.Phone3 = "";
                ContactProspect.PhoneExtention = "";
                ContactProspect.SelectedPhoneType = string.Empty;
                ContactProspect.ToggleAddButton = false;
                ContactProspect.IsAddPhone = true;
                ContactProspect.IsAddContact = ContactProspect.IsAddEmail = false;
                var dialog = new Helpers.DialogWindow { TemplateKey = "AddProspectPhoneContact", Title = "Add Phone Number" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);

            });
            #endregion

            #region OpenProspectEmailContact
            OpenProspectEmailContact = new DelegateCommand((param) =>
            {
                ContactProspect ContactToAddEmail = (ContactProspect)param;
                ContactProspect.Name = ContactToAddEmail.Name;
                ContactProspect.Title = ContactToAddEmail.Title;
                EmailType = prospectManager.GetEmailTypeList();
                ContactProspect.ContactID = ContactToAddEmail.ContactID;
                ContactProspect.EmailList = ContactToAddEmail.EmailList;
                ContactProspect.EmailID = "";
                ContactProspect.SelectedEmailType = string.Empty;
                ContactProspect.ToggleAddButton = false;
                ContactProspect.IsAddEmail = true;
                ContactProspect.IsAddContact = ContactProspect.IsAddPhone = false;
                var dialog = new Helpers.DialogWindow { TemplateKey = "AddProspectEmailContact", Title = "Add Email" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });
            #endregion

            #region EditProspectPhone
            EditProspectPhone = new DelegateCommand((param) =>
            {
                ProspectPhone ContactToEditphone = (ProspectPhone)param;

                ContactProspect ObjContactEditPhone = new ContactProspect();
                ContactProspect.IsAddPhone = true;
                ContactProspect.IsAddContact = ContactProspect.IsAddEmail = false;
                ObjContactEditPhone = prospectManager.GetContactNameAndTitle(ContactToEditphone.ContactID, Prospect.ProspectID);
                ContactProspect.ContactID = ContactToEditphone.ContactID;
                Index = ContactToEditphone.Index;
                ContactProspect.Name = ObjContactEditPhone.Name;
                ContactProspect.Title = ObjContactEditPhone.Title;
                PhoneType = prospectManager.GetPhoneTypeList();
                SelectedPhoneType = prospectManager.GetEditedPhoneType(ContactToEditphone.ProspectID, Convert.ToString(ContactToEditphone.ContactID), Index);
                if (SelectedPhoneType != null && SelectedPhoneType.Count>0)
                    ContactProspect.SelectedPhoneType = SelectedPhoneType[0].ToString().Trim();
                else
                    ContactProspect.SelectedPhoneType = PhoneType[0].ToString().Trim();
                ContactProspect.Phone1 = ContactToEditphone.AreaCode;
                ContactProspect.Phone2 = ContactToEditphone.Value.Substring(0, 3);
                ContactProspect.Phone3 = ContactToEditphone.Value.Substring(4, 4);
                ContactProspect.PhoneExtention = ContactToEditphone.Extension;

                var dialog = new Helpers.DialogWindow { TemplateKey = "EditExistingProspectPhone", Title = "Edit Phone Number" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });
            #endregion

            #region SaveEditedProspectPhone

            SaveEditedProspectPhone = new DelegateCommand((param) =>
            {
                try
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:SaveEditedProspectPhone][Param =" + param + "]");


                    bool IsSaved = Convert.ToBoolean(new ProspectManager().SaveEditedProspectPhone(ContactProspect, Route, Prospect.ProspectID, Index));

                    if (IsSaved)
                    {
                        IsDirty = false;
                        Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                        GetProspectContactExpanded(ContactProspect);
                        //GetProspectContactList(true);
                    }

                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.ProspectsHome.][SaveEditedProspectPhone][customerID =" + Prospect.ProspectID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }

            });
            #endregion

            #region DeleteProspectPhone

            DeleteProspectPhone = new DelegateCommand((param) =>
            {
                ContactID = ContactProspect.ContactID;
                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Contacts.DeletePhoneConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;

                prospectManager.DeleteContactPhone(ContactProspect, Index, Prospect.ProspectID);
                Models.ContactProspect selectedContact = ProspectContactList.First(CT => CT.ContactID == ContactID);
                ObservableCollection<ProspectPhone> ph = selectedContact.PhoneList;
                ProspectPhone selectedPhone = ph.First(p => p.Index == Index);
                selectedContact.PhoneList.Remove(selectedPhone);
                if (selectedPhone.IsSelectedPhone && ph.Count > 0)
                {
                    selectedContact.PhoneList[0].IsSelectedPhone = true;
                    selectedContact.DefaultPhone = selectedContact.PhoneList[0];
                }
                else if (selectedPhone.IsSelectedPhone)
                {
                    selectedContact.DefaultPhone = new ProspectPhone();
                }
                prospectManager.SetContactAsDefault(selectedContact, Prospect.ProspectID);
                //CustomerDashboardVM.SetDefaultContact(selectedContact);
                prospectManager.UpdatePhoneIndexAfterDelete(selectedContact, Index, Prospect.ProspectID);
                Messenger.Default.Send<CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                GetProspectContactExpanded(ContactProspect);
                //GetProspectContactList(true);
            });
            #endregion

            #region DeleteProspectEmail

            DeleteProspectEmail = new DelegateCommand((param) =>
            {
                ProspectEmail emailDetail = new ProspectEmail();
                //emailDetail.Index = Index;
                emailDetail.Value = ContactProspect.EmailID;
                if (string.IsNullOrEmpty(SelectedEmailType))
                {
                    emailDetail.Type = "Email Address";
                }
                else
                {
                    emailDetail.Type = SelectedEmailType;
                }
                if (emailDetail.Value != null && emailDetail.Value.Trim().Length > 1)
                {
                    ContactProspect.EmailList = new ObservableCollection<ProspectEmail>();
                    ContactProspect.EmailList.Add(emailDetail);
                }
                ContactID = ContactProspect.ContactID;
                //LineID = ContactProspect.LineID;
                //ContactProspect.CustomerID = Prospect.ProspectID;
                //Guid MessageToken1 = Guid.NewGuid();
                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Contacts.DeleteEmailConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;

                prospectManager.DeleteProspectContactEmail(ContactProspect, Prospect.ProspectID, Index);

                Models.ContactProspect selectedContact = ProspectContactList.First(CT => CT.ContactID == ContactID);
                ObservableCollection<ProspectEmail> email = selectedContact.EmailList;
                ProspectEmail selectedEmail = email.First(p => p.Index == Index);
                selectedContact.EmailList.Remove(selectedEmail);
                if (selectedEmail.IsSelectedEmail && email.Count > 0)
                {
                    selectedContact.EmailList[0].IsSelectedEmail = true;
                    selectedContact.DefaultEmail = selectedContact.EmailList[0];
                }
                else if (selectedEmail.IsSelectedEmail)
                {
                    selectedContact.DefaultEmail = new ProspectEmail();
                }
                prospectManager.SetContactAsDefault(selectedContact, Prospect.ProspectID);

                prospectManager.UpdateEmailIndexAfterDelete(selectedContact, Index, Prospect.ProspectID);
                //CustomerDashboardVM.SetDefaultContact(selectedContact);

                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                GetProspectContactExpanded(ContactProspect);
                //GetProspectContactList(true);

            });

            #endregion

            #region BeforeProspectPhoneChange

            BeforeProspectPhoneChange = new DelegateCommand((sender) =>
            {
                if (sender != null)
                {
                    selectedProspectPhone = sender as ProspectPhone;
                }
                try
                {
                    List<ContactProspect> otherContacts = ProspectContactList.Where(item => item.ContactID != selectedProspectPhone.ContactID).ToList<ContactProspect>();
                    ContactProspect selectedContact = ProspectContactList.First(item => item.ContactID == selectedProspectPhone.ContactID);
                    foreach (ContactProspect ContactProspect in otherContacts)
                    {
                        foreach (ProspectPhone phone in ProspectContactList.ElementAt(ProspectContactList.IndexOf(ContactProspect)).PhoneList)
                        {
                            phone.IsSelectedPhone = false;
                        }
                        foreach (ProspectEmail email in ProspectContactList.ElementAt(ProspectContactList.IndexOf(ContactProspect)).EmailList)
                        {
                            email.IsSelectedEmail = false;
                        }
                        if (ProspectContactList.ElementAt(ProspectContactList.IndexOf(selectedContact)).EmailList.Count > 0)
                        {
                            int selectedEmailInContact = ProspectContactList.ElementAt(ProspectContactList.IndexOf(selectedContact)).EmailList.Count(EM => EM.IsSelectedEmail == true);
                            if (selectedEmailInContact == 0)
                            {
                                ProspectContactList.ElementAt(ProspectContactList.IndexOf(selectedContact)).EmailList[0].IsSelectedEmail = true;
                                selectedContact.DefaultEmail = ProspectContactList.ElementAt(ProspectContactList.IndexOf(selectedContact)).EmailList[0];
                            }
                        }
                        ProspectContactList.ElementAt(ProspectContactList.IndexOf(ContactProspect)).ShowOnDashboard = false;
                        //contact.ShowOnDashboard = false;
                    }

                    if (sender != null)
                    {
                        selectedContact.DefaultPhone = selectedProspectPhone;
                    }
                    else
                        selectedContact.DefaultPhone = selectedContact.PhoneList[0];

                    selectedContact.ShowOnDashboard = true;
                    new Managers.ProspectManager().SetContactAsDefault(selectedContact, Prospect.ProspectID);
                    //CustomerDashboardVM.SetDefaultContact(selectedContact);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel.][BeforeProspectPhoneChange][customerID =" + Prospect.ProspectID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            #endregion

            #region AfterProspectPhoneChange
            AfterProspectPhoneChange = new DelegateCommand((Selected) =>
            {
                selectedProspectPhone = Selected as ProspectPhone;
            });
            #endregion

            #region AfterProspectEmailChange
            AfterProspectEmailChange = new DelegateCommand((Selected) =>
            {
                selectedProspectEmail = Selected as ProspectEmail;
            });
            #endregion

            #region BeforeProspectEmailChange

            BeforeProspectEmailChange = new DelegateCommand((sender) =>
            {
                if (sender != null)
                {
                    selectedProspectEmail = sender as ProspectEmail;
                }
                try
                {
                    List<ContactProspect> otherContacts = ProspectContactList.Where(item => item.ContactID != selectedProspectEmail.ContactID).ToList<ContactProspect>();
                    ContactProspect selectedContact = ProspectContactList.First(item => item.ContactID == selectedProspectEmail.ContactID);
                    foreach (ContactProspect contact in otherContacts)
                    {
                        foreach (ProspectPhone phone in ProspectContactList.ElementAt(ProspectContactList.IndexOf(contact)).PhoneList)
                        {
                            phone.IsSelectedPhone = false;
                        }
                        foreach (ProspectEmail email in ProspectContactList.ElementAt(ProspectContactList.IndexOf(contact)).EmailList)
                        {
                            email.IsSelectedEmail = false;
                        }
                        contact.ShowOnDashboard = false;
                    }
                    if (ProspectContactList.ElementAt(ProspectContactList.IndexOf(selectedContact)).PhoneList.Count > 0)
                    {
                        int selectedPhoneInContact = ProspectContactList.ElementAt(ProspectContactList.IndexOf(selectedContact)).PhoneList.Count(ph => ph.IsSelectedPhone == true);
                        if (selectedPhoneInContact == 0)
                        {
                            ProspectContactList.ElementAt(ProspectContactList.IndexOf(selectedContact)).PhoneList[0].IsSelectedPhone = true;
                            selectedContact.DefaultPhone = ProspectContactList.ElementAt(ProspectContactList.IndexOf(selectedContact)).PhoneList[0];
                        }
                    }

                    if (sender != null)
                    {
                        selectedContact.DefaultEmail = sender as ProspectEmail;
                    }
                    else
                        selectedContact.DefaultEmail = selectedContact.EmailList[0];

                    selectedContact.ShowOnDashboard = true;
                    new Managers.ProspectManager().SetContactAsDefault(selectedContact, Prospect.ProspectID);
                    // CustomerDashboardVM.SetDefaultContact(selectedContact);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel.][BeforeProspectEmailChange][customerID =" + Prospect.ProspectID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            #endregion

            #region SaveEditedProspectEmail

            SaveEditedProspectEmail = new DelegateCommand((param) =>
            {
                try
                {

                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:SaveEditedProspectEmail][Param =" + param + "]");


                    bool IsSaved = Convert.ToBoolean(new ProspectManager().SaveEditedProspectEmail(ContactProspect, Route, Prospect.ProspectID, Index));

                    if (IsSaved)
                    {
                        IsDirty = false;
                        Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                        GetProspectContactExpanded(ContactProspect);
                        //GetProspectContactList(true);
                    }

                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.ProspectsHome.][SaveEditedProspectEmail][customerID =" + Prospect.ProspectID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            #endregion

            #region EditProspectEmail
            EditProspectEmail = new DelegateCommand((param) =>
            {
                ProspectEmail ContactToEditEmail = (ProspectEmail)param;
                ContactProspect ObjContactEditEmail = new ContactProspect();
                ContactProspect.IsAddEmail = true;
                ContactProspect.IsAddContact = ContactProspect.IsAddPhone = false;
                ObjContactEditEmail = prospectManager.GetContactNameAndTitle(ContactToEditEmail.ContactID, Prospect.ProspectID);
                ContactProspect.ContactID = ContactToEditEmail.ContactID;
                Index = ContactToEditEmail.Index;
                ContactProspect.Name = ObjContactEditEmail.Name;
                ContactProspect.Title = ObjContactEditEmail.Title;
                EmailType = prospectManager.GetEmailTypeList();
                SelectedEmailType = prospectManager.GetEditedEmailType(ContactToEditEmail.ProspectID, ContactToEditEmail.ContactID, Index);
                ContactProspect.SelectedEmailType = SelectedEmailType.Trim();
                ContactProspect.EmailID = ContactToEditEmail.Value;
                var dialog = new Helpers.DialogWindow { TemplateKey = "EditExistingProspectEmail", Title = "Edit Email" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });
            #endregion

            #region AddProspectContact

            AddProspectContact = new DelegateCommand((param) =>
            {
                try
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:AddNewProspect][Param =" + param + "]");

                    bool IsSaved = Convert.ToBoolean(new ProspectManager().AddProspectContact(ContactProspect, Route, Prospect.ProspectID));

                    if (IsSaved)
                    {
                        IsDirty = false;
                        Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                        GetProspectContactExpanded(ContactProspect);
                    }

                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:AddProspectContact][Param =" + param + "]");
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][AddNewProspect][ExceptionStackTrace = " + ex.StackTrace + "]");
                    throw;
                }
            });
            #endregion

            #region AddProspectPhone

            AddProspectPhone = new DelegateCommand((param) =>
            {
                try
                {

                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:AddProspectPhone][Param =" + param + "]");

                    bool IsSaved = Convert.ToBoolean(new ProspectManager().AddProspectPhone(ContactProspect, Route, Prospect.ProspectID));

                    if (IsSaved)
                    {
                        IsDirty = false;
                        Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                        //GetProspectContactList(true);
                        GetProspectContactExpanded(ContactProspect);
                    }

                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:AddProspectPhone][Param =" + param + "]");

                    //}
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][AddProspectPhone][ExceptionStackTrace = " + ex.StackTrace + "]");
                    throw;
                }
            });
            #endregion

            #region AddProspectEmail

            AddProspectEmail = new DelegateCommand((param) =>
            {
                try
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:AddProspectEmail][Param =" + param + "]");

                    bool IsSaved = Convert.ToBoolean(new ProspectManager().AddProspectEmail(ContactProspect, Route, Prospect.ProspectID));

                    if (IsSaved)
                    {
                        IsDirty = false;
                        Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                        //GetProspectContactList(true);
                        GetProspectContactExpanded(ContactProspect);
                    }

                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:AddProspectEmail][Param =" + param + "]");

                    //}
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][AddProspectEmail][ExceptionStackTrace = " + ex.StackTrace + "]");
                    throw;
                }
            });
            #endregion

            #region edit prospect contact
            EditProspectContact = new DelegateCommand((Items) =>
            {
                ContactProspect contactToEdit = (ContactProspect)Items;
                ContactProspect.IsAddContact = true;
                ContactProspect.IsAddPhone = ContactProspect.IsAddEmail = false;
                Index = 1;
                SelectedPhoneType = prospectManager.GetEditedPhoneType(contactToEdit.ProspectID, contactToEdit.ContactID, Index);
                SelectedEmailType = prospectManager.GetEditedEmailType(contactToEdit.ProspectID, contactToEdit.ContactID, Index);
                ContactProspect.Name = contactToEdit.Name.Trim();
                ContactProspect.Title = contactToEdit.Title;
                if (SelectedPhoneType != null && SelectedPhoneType.Count > 0)
                {
                    ContactProspect.SelectedPhoneType = SelectedPhoneType[0].ToString().Trim();
                }
                else
                {
                    ContactProspect.SelectedPhoneType = "";
                }
                ContactProspect.Phone1 = contactToEdit.Phone1.Trim();
                ContactProspect.Phone2 = contactToEdit.Phone2.Trim();
                ContactProspect.Phone3 = contactToEdit.Phone3.Trim();
                ContactProspect.PhoneExtention = contactToEdit.PhoneExtention.Trim();
                ContactProspect.SelectedEmailType = SelectedEmailType.Trim();
                ContactProspect.EmailID = contactToEdit.EmailID;
                ContactProspect.ContactID = contactToEdit.ContactID;
                PhoneType = prospectManager.GetPhoneTypeList();
                EmailType = prospectManager.GetEmailTypeList();

                ContactProspect.ButtonTitle = "Save";
                ContactProspect.PropertyChanged += ContactProspect_PropertyChanged;

                var dialog = new Helpers.DialogWindow { TemplateKey = "AddProspectContact", Title = "Edit Contact" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);

            });
            #endregion

            #region DeleteProspectContactFromList
            DeleteProspectContactFromList = new DelegateCommand((param) =>
            {
                List<ContactProspect> contactList = ProspectContactList.Where(contact => contact.IsSelected == true).ToList<ContactProspect>();
                int selectedContacts = contactList.Count();
                try
                {
                    var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Contacts.DeleteContactConfirmationForMany, MessageIcon = "Alert", Confirmed = false };
                    switch (selectedContacts)
                    {
                        case 0:
                            var alertMessage = new Helpers.AlertWindow { Message = "Please select contact to delete!", MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                            return;
                            break;
                        case 1:
                            confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Contacts.DeleteContactConfirmation, MessageIcon = "Alert", Confirmed = false };
                            Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                            break;
                        default:
                            confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Contacts.DeleteContactConfirmationForMany, MessageIcon = "Alert", Confirmed = false };
                            Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                            break;
                    }

                    if (!confirmMessage.Confirmed) return;

                    for (int index = 0; index < selectedContacts; index++)
                    {
                        if (contactList[index].ShowOnDashboard)
                        {
                            List<ContactProspect> contactListDefault = ProspectContactList.Where(contact => contact.IsDefault == true).ToList<ContactProspect>();
                            foreach (ContactProspect item in contactListDefault)
                            {
                                if (item.IsDefault)
                                {
                                    item.ShowOnDashboard = true;
                                    new Managers.ProspectManager().SetContactAsDefault(item, Prospect.ProspectID);
                                    //CustomerDashboardVM.SetDefaultContact(item);
                                    prospectManager.SetContactAsDefault(item, Prospect.ProspectID);
                                }
                            }
                        }
                        ContactProspect.ContactID = contactList[index].ContactID;
                        prospectManager.DeleteProspectContact(ContactProspect, Prospect.ProspectID);
                    }

                    GetProspectContactList(true);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel.][DeleteContactFromContactList][customerID =" + Prospect.ProspectID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }

            });
            #endregion

            #region EditAlliedProspectSelected
            EditAlliedProspectSelected = new DelegateCommand((param) =>
            {
                List<AlliedProspectVM> alList = AlliedProspectList.Where(cof => cof.IsSelected == true).ToList<AlliedProspectVM>();
                AlliedProspectVM alProd = alList.FirstOrDefault();
                int selectedAlliedProspect = alList.Count();

                switch (selectedAlliedProspect)
                {
                    case 0:
                        var alertMessage = new Helpers.AlertWindow { Message = "Please select a allied product to edit!", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        break;
                    case 1:
                        break;
                }

                AlliedProspectToAdd = new AlliedProspect();
                _competitorList = objProspectManager.GetAlliedCompetitiors();
                _AlliedCategoryList = objProspectManager.GetAlliedCategory();
                _AlliedSubCategoryList = objProspectManager.GetAlliedSubCategory("");
                _BrandList = objProspectManager.GetAlliedBrand();
                _PackSizeList = objProspectManager.GetAlliedPackSize();
                _CSPKLBList = objProspectManager.GetAlliedCsPkLb();
                _AvgVolumeList = objProspectManager.GetAlliedVolumes();

                AlliedProspectToAdd.AlliedProspectID = alProd.AlliedProspectID;
                AlliedProspectToAdd.AlliedCompetitorSelected = alProd.Competitor;
                AlliedProspectToAdd.AlliedCategorySelected = alProd.Category;
                AlliedProspectToAdd.AlliedSubCategorySelected = alProd.SubCategory;
                AlliedProspectToAdd.AlliedBrandSelected = alProd.Brand;
                AlliedProspectToAdd.AlliedPackSizeSelected = alProd.PackSizeText;
                AlliedProspectToAdd.AlliedCSPKLBSelected = alProd.CSPKLBText;
                AlliedProspectToAdd.Price = alProd.Price;
                AlliedProspectToAdd.AlliedVolumeSelected = alProd.AverageVolume;
                AlliedProspectToAdd.ButtonTitle = "Save";
                AlliedProspectToAdd.CompetitorDropDown = "False";
                AlliedProspectToAdd.CategoryDropDown = "True";
                AlliedProspectToAdd.SubCategoryDropDown = "True";

                AlliedProspectToAdd.PropertyChanged += AlliedProspectToAdd_PropertyChanged;
                var dialog = new Helpers.DialogWindow { TemplateKey = "AddAllied", Title = "Edit Allied Product" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });
            #endregion

            #region SelectProspectContact
            SelectProspectContact = new DelegateCommand((Selected) =>
            {
                bool select = (bool)Selected;
                if (!select)
                {
                    SelectAllCheck = select;
                }
                else
                {
                    bool isAllSelected = true;
                    foreach (ContactProspect cust in ProspectContactList)
                    {
                        if (!cust.IsSelected) // && !cust.IsDefault)
                        {
                            isAllSelected = false;
                        }
                    }
                    SelectAllCheck = isAllSelected == true ? true : false;
                }
            });
            #endregion

            #region SelectAllProspectContacts
            SelectAllProspectContacts = new DelegateCommand((SelectAll) =>
            {
                bool select = (bool)SelectAll;
                foreach (ContactProspect ContactProspect in ProspectContactList)
                {
                    if (!ContactProspect.IsDefault)
                    {
                        ContactProspect.IsSelected = select;
                    }
                }
            });
            #endregion

            #endregion

            #region EditAlliedProspectSelected
            EditAlliedProspectSelected = new DelegateCommand((param) =>
            {
                List<AlliedProspectVM> alList = AlliedProspectList.Where(cof => cof.IsSelected == true).ToList<AlliedProspectVM>();
                AlliedProspectVM alProd = alList.FirstOrDefault();
                int selectedAlliedProspect = alList.Count();

                switch (selectedAlliedProspect)
                {
                    case 0:
                        var alertMessage = new Helpers.AlertWindow { Message = "Please select a allied product to edit!", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        break;
                    case 1:
                        break;
                }

                AlliedProspectToAdd = new AlliedProspect();
                _competitorList = objProspectManager.GetAlliedCompetitiors();
                _AlliedCategoryList = objProspectManager.GetAlliedCategory();
                _AlliedSubCategoryList = objProspectManager.GetAlliedSubCategory("");
                _BrandList = objProspectManager.GetAlliedBrand();
                _PackSizeList = objProspectManager.GetAlliedPackSize();
                _CSPKLBList = objProspectManager.GetAlliedCsPkLb();
                _AvgVolumeList = objProspectManager.GetAlliedVolumes();

                AlliedProspectToAdd.AlliedProspectID = alProd.AlliedProspectID;
                AlliedProspectToAdd.AlliedCompetitorSelected = alProd.Competitor;
                AlliedProspectToAdd.AlliedCategorySelected = alProd.Category;
                AlliedProspectToAdd.AlliedSubCategorySelected = alProd.SubCategory;
                AlliedProspectToAdd.AlliedBrandSelected = alProd.Brand;
                AlliedProspectToAdd.AlliedPackSizeSelected = alProd.PackSizeText;
                AlliedProspectToAdd.AlliedCSPKLBSelected = alProd.CSPKLBText;
                AlliedProspectToAdd.Price = alProd.Price;
                AlliedProspectToAdd.AlliedVolumeSelected = alProd.AverageVolume;
                AlliedProspectToAdd.ButtonTitle = "Save";
                AlliedProspectToAdd.CompetitorDropDown = "False";
                AlliedProspectToAdd.CategoryDropDown = "True";
                AlliedProspectToAdd.SubCategoryDropDown = "True";

                AlliedProspectToAdd.PropertyChanged += AlliedProspectToAdd_PropertyChanged;
                var dialog = new Helpers.DialogWindow { TemplateKey = "AddAllied", Title = "Edit Allied Product" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });
            #endregion

            #region AlliedCategoryChangedCommand
            AlliedCategoryChangedCommand = new DelegateCommand((param) =>
            {
                AlliedSubCategoryList = objProspectManager.GetAlliedSubCategory(AlliedProspectToAdd.AlliedCategorySelected);
            });
            #endregion

            #region DeleteAlliedProspectSelected
            DeleteAlliedProspectSelected = new DelegateCommand((param) =>
            {
                List<Models.AlliedProspectVM> cofList = AlliedProspectList.Where(x => x.IsSelected == true).ToList<Models.AlliedProspectVM>();

                int selectedAlliedProspect = cofList.Count();

                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Competitor.DeleteProspectConfirmationForMany, MessageIcon = "Alert", Confirmed = false };
                switch (selectedAlliedProspect)
                {
                    case 0:
                        var alertMessage = new Helpers.AlertWindow { Message = "Please select prospects to delete!", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        //return;
                        break;
                    case 1:
                        confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Competitor.DeleteProspectConfirmation, MessageIcon = "Alert", Confirmed = false };
                        Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);

                        break;
                    default:
                        confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Competitor.DeleteProspectConfirmationForMany, MessageIcon = "Alert", Confirmed = false };
                        Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                        break;
                }

                if (!confirmMessage.Confirmed) return;

                Managers.ProspectManager prospectManager = new Managers.ProspectManager();
                Models.AlliedProspect prospect = new Models.AlliedProspect();

                for (int index = 0; index < selectedAlliedProspect; index++)
                {
                    prospect.AlliedProspectID = cofList[index].AlliedProspectID;
                    prospectManager.RemoveAlliedProspect(prospect);
                    AlliedProspectList.Remove(AlliedProspectList.ElementAt(AlliedProspectList.IndexOf(AlliedProspectList.First(note => note.AlliedProspectID == prospect.AlliedProspectID))));
                }
            });
            #endregion

            #region SearchExpensed Item
            SearchCompetitor = new DelegateCommand((searchTerm) =>
            {
                if (IsCoffeeTabActive)
                {
                    CoffeeProspectList = _CoffeeProspectListFilter.Where(x => x.Competitor.Contains(SearchEquipmentText)).ToObservableCollection();
                }
                else if (IsAlliedTabActive)
                {
                    AlliedProspectList = _AlliedProspectListFilter.Where(x => x.Competitor.Contains(SearchEquipmentText)).ToObservableCollection();
                }
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                {

                }));
                IsBusy = false;
                //
            });
            #endregion

            #endregion
            // For Adding New Notes //
            AddNewProspectNote = new DelegateCommand((param) =>
            {
                var dialog = new Helpers.DialogWindow { TemplateKey = "AddNewNote", Title = "Add New Note", Payload = this };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });
            SaveNewProspectNote = new DelegateCommand((param) =>
            {
                Prospect note = new Prospect();
                try
                {
                    note.NotesDetails = NoteDetails.Trim();
                    string a = Prospect.ProspectID;
                    note.ProspectID = customerNoVM;
                    Managers.ProspectManager noteManager = new Managers.ProspectManager();
                    //noteManager.AddNewCustomerNote(note);
                    //GetCustomerNotes();

                    //ToggleButton = false;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ProspectsHome][SaveNewProspectNote DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][End:InitializeCommands]");
        }
        async void GetCoffeeProspectForRoute(bool IsRefresh = false)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Start:GetCoffeeProspectForRoute]");
            await Task.Run(() =>
            {
                try
                {
                    if (CoffeeProspectList.Count == 0 || IsRefresh)
                    {
                        CoffeeProspectList = new ProspectManager().GetCoffeeProspectForRoute(Prospect.ProspectID);
                        _CoffeeProspectListFilter = CoffeeProspectList;
                        OnPropertyChanged("CoffeeProspectsCount");
                    }
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                    {

                    }));
                    IsBusy = false;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ProspectsHome][GetCoffeeProspectForRoute][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][End:GetCoffeeProspectForRoute]");
            });
        }
        async void GetAlliedProspectForRoute(bool IsRefresh = false)
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Start:GetAlliedProspectForRoute]");
                try
                {
                    if (AlliedProspectList.Count == 0 || IsRefresh)
                    {
                        AlliedProspectList = new ProspectManager().GetAlliedProspectForRoute(Prospect.ProspectID);
                        _AlliedProspectListFilter = AlliedProspectList;
                        OnPropertyChanged("AlliedProspectsCount");
                    }
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                    {

                    }));
                    IsBusy = false;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ProspectsHome][GetAlliedProspectForRoute][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][End:GetAlliedProspectForRoute]");

            });
        }
        public void GetProspectContactExpanded(ContactProspect ContactProspect)
        {
            ObservableCollection<ContactProspect> ProspectContactCollection = new ProspectManager().GetProspectContactList(Prospect.ProspectID);
            try
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Start:GetProspectContactExpanded]");
                if (ProspectContactCollection.Count > 0)
                {
                    if (ProspectContactCollection.Count > ProspectContactList.Count)
                    {
                        //Add new entry
                        ContactProspect contactToBeAdded = new ContactProspect();

                        foreach (ContactProspect item in ProspectContactCollection)
                        {

                            if (!ProspectContactList.Any(d => d.ContactID == item.ContactID))
                            {
                                if (item.EmailList.Count == 3)
                                {
                                    item.ToggleEmailImg = false;
                                }
                                foreach (ProspectEmail em in item.EmailList)
                                {
                                    em.PropertyChanged += EmailSelected;
                                }
                                if (item.PhoneList.Count == 4)
                                {
                                    item.TogglePhoneImg = false;
                                }
                                foreach (ProspectPhone em in item.PhoneList)
                                {
                                    em.PropertyChanged += PhoneSelected;
                                }
                                item.IsExpanded = true;
                                contactToBeAdded = item;
                                break;
                            }
                        }
                        ProspectContactList.Add(contactToBeAdded);
                    }
                    else if (ProspectContactCollection.Count < ProspectContactList.Count)
                    {
                        //Remove deleted entry
                        List<string> contactIdToRemove = new List<string>();
                        foreach (ContactProspect item in ProspectContactList)
                        {
                            if (!ProspectContactCollection.Any(d => d.ContactID == item.ContactID))
                            {
                                contactIdToRemove.Add(item.ContactID);
                                if (item.ShowOnDashboard)
                                {
                                    var defaultCon = ProspectContactList.FirstOrDefault(t => t.IsDefault);
                                    if (defaultCon.PhoneList != null && defaultCon.PhoneList.Count > 0)
                                    {
                                        defaultCon.PhoneList[0].IsSelectedPhone = true;
                                    }
                                    if (defaultCon.EmailList != null && defaultCon.EmailList.Count > 0)
                                    {
                                        defaultCon.EmailList[0].IsSelectedEmail = true;
                                    }
                                }
                            }
                        }
                        for (int i = 0; i < contactIdToRemove.Count; i++)
                        {
                            ProspectContactList.Where(t => t.ContactID == contactIdToRemove[i]).ToList().ForEach(a => ProspectContactList.Remove(a));
                        }
                    }
                    else if (ProspectContactCollection.Count == ProspectContactList.Count)
                    {
                        ContactProspect contactUpdated = ProspectContactCollection.First(t => t.ContactID == ContactProspect.ContactID && t.ProspectID == Convert.ToInt32(Prospect.ProspectID));
                        ContactProspect contactToBeUpdated = ProspectContactList.First(t => t.ContactID == ContactProspect.ContactID && t.ProspectID == Convert.ToInt32(Prospect.ProspectID));
                        int index = ProspectContactList.IndexOf(contactToBeUpdated);
                        ProspectContactList.Where(t => t.ContactID == contactToBeUpdated.ContactID).ToList().ForEach(a => ProspectContactList.Remove(a));

                        if (contactUpdated.EmailList.Count == 3)
                        {
                            contactUpdated.ToggleEmailImg = false;
                        }
                        foreach (ProspectEmail em in contactUpdated.EmailList)
                        {
                            em.PropertyChanged += EmailSelected;
                        }
                        if (contactUpdated.PhoneList.Count == 4)
                        {
                            contactUpdated.TogglePhoneImg = false;
                        }
                        foreach (ProspectPhone em in contactUpdated.PhoneList)
                        {
                            em.PropertyChanged += PhoneSelected;
                        }
                        contactUpdated.IsExpanded = true;
                        //CustomerContactList.Add(contactUpdated);
                        ProspectContactList.Insert(index, contactUpdated);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ProspectsHome][GetProspectContactExpanded][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][End:GetProspectContactExpanded]");
        }
        void EmailSelected(object sender, PropertyChangedEventArgs e)
        {
        }
        void PhoneSelected(object sender, PropertyChangedEventArgs e)
        {
        }
        public class NotesVMArgs : EventArgs
        {
            public CustomerNote Note { get; set; }

        }
        public override bool ConfirmSave()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Start:ConfirmSave]");
            this.IsDirty = false;
            GetActiveTab().ConfirmSave();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][End:ConfirmSave]");
            return base.ConfirmSave();

        }

        public override bool ConfirmCancel()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Start:ConfirmCancel]");
            this.IsDirty = false;
            GetActiveTab().ConfirmCancel();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][End:ConfirmCancel]");
            return base.ConfirmCancel();
        }

        private void ModalPopupOnTabChange(string forTab, string currentSeleced)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Start:ModalPopupOnTabChange]");
            try
            {

                string confirmationMessage = "Do you want to save changes made?";
                ConfirmWindow confirmReadOnlyNavigation = new Helpers.ConfirmWindow { OkButtonContent = "Yes", CancelButtonContent = "No" };
                confirmReadOnlyNavigation.Message = confirmationMessage;
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmReadOnlyNavigation, MessageToken);

                if (confirmReadOnlyNavigation.Confirmed)
                {
                    IsDirty = false;
                    GetChangedTab(forTab).ConfirmSave();
                }
                else if (confirmReadOnlyNavigation.Cancelled || confirmReadOnlyNavigation.Closed)
                {
                    IsDirty = false;
                    GetChangedTab(forTab).ConfirmCancel();
                    switch (forTab)
                    {
                        case "CompetitorInformation":
                            CompetitorInformationVM = null;
                            break;
                        case "Pricing":
                            PricingVM = null;
                            break;
                        case "Quotes":
                            QuotesVM = null;
                            break;
                        case "Contact":
                            ContactVM = null;
                            break;
                        case "Information":
                            InformationVM = null;
                            break;
                        case "Notes":
                            NotesVM = null;
                            break;
                        case "Dashboard":
                            DashboardVM = null;
                            break;
                        default:
                            break;
                    }
                }
                else if (confirmReadOnlyNavigation.Closed)
                {

                    //switch (forTab)
                    //{
                    //    case "CompetitorInformation":
                    //        IsCompetitorInformationTabSelected = true;
                    //        break;
                    //    case "Pricing":
                    //        IsPricingTabSelected = true;

                    //        break;
                    //    case "Quotes":
                    //        IsQuotesTabSelected = true;

                    //        break;
                    //    case "Contact":
                    //        IsContactTabSelected = true;

                    //        break;
                    //    case "Information":
                    //        IsInformationTabSelected = true;

                    //        break;
                    //    case "Notes":
                    //        IsNotesTabSelected = true;

                    //        break;
                    //    case "Dashboard":
                    //        IsDashboardTabSelected = true;
                    //        break;

                    //    default:
                    //        break;
                    //}

                    //switch (currentSeleced)
                    //{
                    //    case "CompetitorInformation":
                    //        IsCompetitorInformationTabSelected = false;
                    //        break;
                    //    case "Pricing":
                    //        IsPricingTabSelected = false;

                    //        break;
                    //    case "Quotes":
                    //        IsQuotesTabSelected = false;

                    //        break;
                    //    case "Contact":
                    //        IsContactTabSelected = false;

                    //        break;
                    //    case "Information":
                    //        IsInformationTabSelected = false;

                    //        break;
                    //    case "Notes":
                    //        IsNotesTabSelected = false;

                    //        break;
                    //    case "Dashboard":
                    //        IsDashboardTabSelected = false;

                    //        break;

                    //    default:
                    //        break;
                    //}
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ProspectsHome][ModalPopupOnTabChange][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][End:ModalPopupOnTabChange]");
        }
        BaseViewModel GetActiveTab()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Start:GetActiveTab]");
            BaseViewModel bmv = null;
            switch (selectedTab)
            {
                case "CompetitorInformation":
                    bmv = CompetitorInformationVM;
                    break;
                case "Pricing":
                    bmv = PricingVM;
                    break;
                case "Quotes":
                    bmv = QuotesVM;
                    break;
                case "Contact":
                    bmv = ContactVM;

                    break;
                case "Information":
                    bmv = InformationVM;

                    break;
                case "Notes":
                    bmv = NotesVM;

                    break;
                case "Dashboard":
                    bmv = DashboardVM;

                    break;

                default:
                    break;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][End:GetActiveTab]");
            return bmv;
        }

        BaseViewModel GetChangedTab(string changedTab)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][Start:GetChangedTab]");
            BaseViewModel bmv = null;
            switch (changedTab)
            {
                case "CompetitorInformation":
                    bmv = CompetitorInformationVM;
                    break;
                case "Pricing":
                    bmv = PricingVM;
                    break;
                case "Quotes":
                    bmv = QuotesVM;
                    break;
                case "Contact":
                    bmv = ContactVM;

                    break;
                case "Information":
                    bmv = InformationVM;

                    break;
                case "Notes":
                    bmv = NotesVM;

                    break;
                case "Dashboard":
                    bmv = DashboardVM;

                    break;

                default:
                    break;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectsHome][End:GetChangedTab]");
            return bmv;
        }
        void NotesVM_ViewModelUpdated(object sender, ProspectNotesVMArgs e)
        {
            DashboardVM.SetDefaultNote(e.Note);
        }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }

    }

}