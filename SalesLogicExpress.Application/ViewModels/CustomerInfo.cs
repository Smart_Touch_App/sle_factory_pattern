﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using SalesLogicExpress.Domain;
using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.Helpers;
using log4net;
using SalesLogicExpress.Application.ViewModelPayload;

namespace SalesLogicExpress.Application.ViewModels
{
    public class CustomerInfo : BaseViewModel, IDataErrorInfo
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CustomerInfo");
        CustomerInformation _customerInformation = null;
        public CustomerInformation customerInformation
        {
            get
            {
                return _customerInformation;
            }
            set
            {
                _customerInformation = value;
                OnPropertyChanged("customerInformation");
            }
        }
        private bool isSaveAllowed = false;
        bool isInfoSaved = false;
        public bool IsInfoSaved
        {
            get { return isInfoSaved; }
            set { isInfoSaved = value; OnPropertyChanged("IsInfoSaved"); }
        }
        public bool IsSaveAllowed
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(customerInformation.Address1) && !string.IsNullOrWhiteSpace(customerInformation.CustInfoCity) && !string.IsNullOrWhiteSpace(customerInformation.CustInfoState) && !string.IsNullOrWhiteSpace(customerInformation.CustInfoZip) && customerInformation.CustInfoZip.Length >= 5)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                isSaveAllowed = value;
                OnPropertyChanged("IsSaveAllowed");
            }
        }
        List<string> _StateCodeList;
        public List<string> StateCodeList
        {
            get
            {
                return _StateCodeList;
            }
            set
            {
                _StateCodeList = value;
                OnPropertyChanged("StateCodeList");
            }
        }
        private bool isSaveButtonUsed = false;
        public bool IsSaveButtonUsed
        {
            get { return isSaveButtonUsed; }
            set { isSaveButtonUsed = false; OnPropertyChanged("IsSaveButtonUsed"); }
        }
        public DelegateCommand SaveCustomerInformation { get; set; }

        Managers.CustomerManager customerManager = new Managers.CustomerManager();
        public CustomerInfo()
        {
            try
            {
                IsDirty = false;
                InitializeCommands();
                LoadDetails();
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerInfo CustomerInfo() error: " + ex.Message);
            }
        }
        async void LoadDetails()
        {
            await Task.Run(() =>
            {
                customerInformation = new CustomerInformation();
                customerInformation = customerManager.GetCustomerInformation(CommonNavInfo.Customer.CustomerNo);
                StateCodeList = customerManager.GetStateCodes();
                IsInfoSaved = false;
                customerInformation.PropertyChanged += customerInformation_PropertyChanged;
            });
        }
        private void InitializeCommands()
        {

            SaveCustomerInformation = new DelegateCommand((param) =>
            {
                SaveInformation();
            });
        }

        public void SaveInformation()
        {
            // customerInformation.IsSaveButtonUsed = true;

            if (customerManager.SaveCustomerInformation(customerInformation) == 0)
            {
                Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Constants.CustomerInformation.UnableToSaveAlert }, MessageToken);
            }
            else
            {
                UpdateTopNavigationWithLatestData();
                IsSaveButtonUsed = true;
                IsInfoSaved = true;
                IsDirty = false;
                IsSaveAllowed = false;
                Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Constants.CustomerInformation.InfoSaveAlert }, MessageToken);
            }

            // throw new NotImplementedException();
        }

        void customerInformation_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            IsSaveAllowed = true;
            if (!IsInfoSaved)
            {
                IsDirty = true;
            }
        }

        public void UpdateTopNavigationWithLatestData()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerHome][Start:UpdateTopNavigationWithLatestData]");
            try
            {
                CommonNavInfo.Customer.Address = customerInformation.Address1 + " " + customerInformation.Address2 + " " + customerInformation.Address3 + " " + customerInformation.Address4;
                CommonNavInfo.Customer.Zip = customerInformation.CustInfoZip;
                CommonNavInfo.Customer.State = customerInformation.CustInfoState;
                CommonNavInfo.Customer.City = customerInformation.CustInfoCity;
                PayloadManager.OrderPayload.Customer = CommonNavInfo.Customer;
                CommonNavInfo.SetSelectedCustomer(PayloadManager.OrderPayload.Customer);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerHome.][UpdateTopNavigationWithLatestData][customerID =" + customerInformation.CustomerNo + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerHome][End:UpdateTopNavigationWithLatestData]");
        }

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get { throw new NotImplementedException(); }
        }

        public void SaveInformationOnTabChange()
        {
            try
            {
                if (!IsSaveButtonUsed)
                {
                    SaveInformation();
                }
            }
            catch (Exception ex)
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerInfo][SaveInformationOnTabChange][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            //throw new NotImplementedException();
        }

        public override bool ConfirmCancel()
        {
            IsDirty = false;
            return base.ConfirmCancel();
        }
        public override bool ConfirmSave()
        {
            IsDirty = false;
            SaveInformation();
            return base.ConfirmSave();
        }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                if (customerManager != null)
                    customerManager.Dispose();
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
