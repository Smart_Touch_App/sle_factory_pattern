﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Managers;
using Telerik.Windows.Controls;
using System.Threading;
using GalaSoft.MvvmLight.Messaging;
namespace SalesLogicExpress.Application.ViewModels
{
    public class CustomerReturnsTabViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CustomerReturnsTabViewModel");

        #region Delgates
        public DelegateCommand ToggleSelectedOrder { get; set; }
        public DelegateCommand PrintOrders { get; set; }
        #endregion
        #region Prop's
        bool _IsPrintButtonEnabled = false;
        public bool IsPrintButtonEnabled
        {
            get
            {
                return _IsPrintButtonEnabled;
            }
            set
            {
                _IsPrintButtonEnabled = value;
                OnPropertyChanged("IsPrintButtonEnabled");
            }
        }
        bool _TogglePrint;
        public bool TogglePrint
        {
            get
            {
                return _TogglePrint;
            }
            set
            {
                _TogglePrint = value;
                OnPropertyChanged("TogglePrint");
            }
        }
        public int SelectedOrderId { get; set; }

        TrulyObservableCollection<ReturnItem> returnItems;

        TrulyObservableCollection<ReturnOrder> returnOrderList = new TrulyObservableCollection<ReturnOrder>();
        public TrulyObservableCollection<ReturnOrder> ReturnOrderList
        {
            get
            {
                return returnOrderList;
            }
            set
            {
                returnOrderList = value;
                OnPropertyChanged("ReturnOrderList");
            }
        }


        public TrulyObservableCollection<ReturnItem> ReturnItems
        {
            get
            {
                return returnItems;
            }
            set
            {
                returnItems = value;
                OnPropertyChanged("ReturnItems");
            }
        }

        TrulyObservableCollection<ReturnOrder> returnOrders = new TrulyObservableCollection<ReturnOrder>();
        public TrulyObservableCollection<ReturnOrder> ReturnOrders
        {
            get
            {
                return returnOrders;
            }
            set
            {

                returnOrders = value;
                OnPropertyChanged("ReturnOrders");
            }
        }
        #endregion

        public CustomerReturnsTabViewModel(BaseViewModel ParentView)
        {
            this.ParentView = ParentView;
            this.ParentView.IsBusy = true;
            this.MessageToken = CommonNavInfo.MessageToken;
            LoadOrders();
            InitializeCommands();
        }

        private void InitializeCommands()
        {
            ToggleSelectedOrder = new DelegateCommand(param =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][Start:ToggleSelectedOrder]");
                try
                {

                    if (param == null) return;
                    List<object> parameters = param as List<object>;
                    //foreach (var item in ReturnItems)
                    //{
                    //    if (item.ReturnOrderID.Equals(Convert.ToInt32(parameters[0])))
                    //    {
                    //        item.IsSelectedItem = Convert.ToBoolean(parameters[1]);
                    //    }
                    //}
                    var selectedOrder = ReturnOrders.Where(ri => ri.ReturnOrderID == Convert.ToInt32(parameters[0])).FirstOrDefault();
                    if (selectedOrder != null)
                    {
                        selectedOrder.IsSelected = true;
                        ReturnOrders.Where(x => x.ReturnOrderID != selectedOrder.ReturnOrderID).ToList().ForEach(x => { x.IsSelected = false; });
                        ReturnOrders.Where(x => x.ReturnOrderID != Convert.ToInt32(parameters[0])).ToList().ForEach(x => { x.ReturnItems.ToList().ForEach(i => { i.IsSelectedItem = false; }); });

                        var orderItems = selectedOrder.ReturnItems;
                        foreach (var item in orderItems)
                        {
                            if (item.ReturnOrderID.Equals(Convert.ToInt32(parameters[0])))
                            {
                                item.IsSelectedItem = Convert.ToBoolean(parameters[1]);
                            }
                        }

                        bool enable = ReturnOrders.Count(i => i.IsSelected == true) > 0 ? true : false;

                        var TEMP = ReturnOrders.FirstOrDefault(i => i.IsSelected == true);
                        IsPrintButtonEnabled = enable;
                    }

                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][ToggleSelectedOrder][ExceptionStackTrace = " + ex.ToString() + "]");
                }
                Logger.Info("SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][End:ToggleSelectedOrder]");
            });
            //PrintOrders = new DelegateCommand(param =>
            //{
            //    Print();
            //});
            PrintOrders = new DelegateCommand(param =>
           {
               //var moveToView1 = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ItemReturnsScreen, CurrentViewName = ViewModelMappings.View.ReturnsAndCredits, CloseCurrentView = true, ShowAsModal = false };
               //Messenger.Default.Send<Helpers.NavigateToView>(moveToView1, CommonNavInfo.MessageToken);

               //PrintAsync();
               Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][Start:PrintOrders]");
               try
               {

                   IsBusy = true;
                   string OrderNumber = "0";
                   decimal DocumentId = 0;
                   OrderNumber = ReturnOrders.Where(s => s.IsSelected).FirstOrDefault().ReturnOrderID.ToString();
                   var temp = ReturnOrders.FirstOrDefault(s => s.IsSelected == true);
                   DocumentId = temp.ReturnItems.Where(s => s.DocumentId >= 0).FirstOrDefault().DocumentId;



                   var DocInfo = new ReportEngine.DocumentInformation();
                   var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                   repEngine.Notify += repEngine_Notify;

                   repEngine.Log = this.Logger;
                   ReportEngine.IReportSLE ReportData = null;
                   //var DocumentId = DocumentId;
                   var SourceDocumentNo = Convert.ToDecimal(OrderNumber);
                   //var DocumentId = ReturnOrders.Where(s => s.IsSelected).FirstOrDefault().DocumentID; ;//addloadpick.DocumentID;
                   //var SourceDocumentNo = ReturnOrders.Where(s => s.IsSelected).FirstOrDefault().OrderId;//Convert.ToInt32(addloadpick.ReplnId);//Convert.ToDecimal(AddLoadPick.ReplnID);

                   if (DocumentId <= 0)
                   {
                       ReportData = new Application.ReportViewModels.ItemReturnsReport(OrderNumber.ToString(), true);
                       Dictionary<string, object> Parameters;
                       Parameters = new Dictionary<string, object>();
                       Parameters.Add("PrintLabels", Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PrintLabels"]));
                       DocInfo = repEngine.Generate<Reporting.ItemReturns>(SourceDocumentNo, true, ReportEngine.ReportCategory.Customer, ReportEngine.ReportsTypes.InvoiceReport, ReportData, Parameters, ReportEngine.ContentTypes.PDF);
                   }
                   else
                   {
                       DocInfo.DocumentId = DocumentId;
                   }
                   if (DocInfo.IsEndOfSequence)
                   {
                       Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, Please sync to continue" }, MessageToken);
                       return;
                   }
                   //var item = ReturnOrders.Where(s => s.OrderId == Convert.ToInt32(OrderNumber)).FirstOrDefault().DocumentID;

                   if (DocInfo.IsDocumentCreated)
                       ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.Order_Header", "OrderID", SourceDocumentNo, DocInfo.DocumentId);
                   DocumentId = DocInfo.DocumentId; //Replace Latest


                   var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Customer, DocumentId, null);
                   if (ReportBytes == null)
                   {
                       Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Report not generated, Please try again." }, MessageToken);
                       return;
                   }
                   SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();

                   Payload.ReportFile = ReportBytes;
                   Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                   Payload.PreviousView = ViewModelMappings.View.ReturnsAndCredits;
                   Payload.CurrentViewTitle = "Returns Report";
                   Payload.PreviousViewTitle = "Returns And Credits";
                   Payload.MessageToken = CommonNavInfo.MessageToken;
                   SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                   SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                   var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.ReturnsAndCredits, CloseCurrentView = true, Payload = Payload, ShowAsModal = false };
                   Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);

                   //var moveToView1 = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.ReturnsAndCredits, CloseCurrentView = true, ShowAsModal = false };
                   //Messenger.Default.Send<Helpers.NavigateToView>(moveToView1, CommonNavInfo.MessageToken);
               }
               catch (Exception ex)
               {
                   Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][PrintOrders][ExceptionStackTrace = " + ex.ToString() + "]");
               }
               Logger.Info("SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][End:PrintOrders]");
           });
        }
        void PrintAsync()
        {
            //await Task.Run(() =>
            //{
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][Start:PrintAsync]");
            try
            {
                IsBusy = true;
                string payload = "0";
                decimal DocumentId = 0;
                try
                {
                    payload = ReturnOrders.Where(s => s.IsSelected).FirstOrDefault().OrderId.ToString();
                    DocumentId = ReturnOrders.Where(s => s.IsSelected).FirstOrDefault().DocumentID;
                }
                catch (Exception) { }
                // new InvoiceReport(payload);
                ShowReport(payload, DocumentId);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][PrintAsync][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][End:PrintAsync]");
            //});
        }

        //   }

        //void Print()
        //{

        //    IsBusy = true;
        //    string payload = "0";
        //    decimal DocumentId = 0;
        //    //var payload = ReturnItems.Where(s => s.IsSelectedItem).FirstOrDefault().ReturnOrderID.ToString();
        //    try
        //    {
        //        payload = ReturnOrders.Where(s => s.IsSelected).FirstOrDefault().ReturnOrderID.ToString();
        //        DocumentId = ReturnOrders.Where(s => s.IsSelected).FirstOrDefault().DocumentID;
        //    }
        //    catch (Exception) { }
        //    //new ItemReturnsReport(payload);
        //    ShowReport(payload, DocumentId);

        //}
        public void ShowReport(string OrderNumber, decimal DocmentId)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][Start:ShowReport]");
            try
            {
                var DocInfo = new ReportEngine.DocumentInformation();
                var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                repEngine.Notify += repEngine_Notify;

                repEngine.Log = this.Logger;
                ReportEngine.IReportSLE ReportData = null;
                var DocumentId = DocmentId;
                var SourceDocumentNo = Convert.ToDecimal(OrderNumber);
                if (DocumentId <= 0)
                {
                    ReportData = new ReportViewModels.ItemReturnsReport(OrderNumber, true);
                    Dictionary<string, object> Parameters;
                    Parameters = new Dictionary<string, object>();
                    Parameters.Add("PrintLabels", Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PrintLabels"]));
                    DocInfo = repEngine.Generate<Reporting.ItemReturns>(SourceDocumentNo, true, ReportEngine.ReportCategory.Customer, ReportEngine.ReportsTypes.ItemReturnsReport, ReportData, Parameters, ReportEngine.ContentTypes.PDF);


                }
                else
                {
                    DocInfo.DocumentId = DocumentId;
                }

                if (DocInfo.IsEndOfSequence)
                {
                    Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, Please sync to continue" }, MessageToken);
                    IsBusy = false;
                    return;
                }

                if (DocInfo.IsDocumentCreated)
                    ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.ORDER_HEADER ", "OrderID", SourceDocumentNo, DocInfo.DocumentId);
                //OrderNumber.DocumentId = DocInfo.DocumentId; //Replace Latest

                var list = ReturnOrders.Where(s => s.IsSelected && s.ReturnOrderID == Convert.ToInt32(OrderNumber)).ToList();
                foreach (var i in list)
                {
                    i.DocumentID = DocInfo.DocumentId;
                }

                var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Customer, DocumentId, null);
                if (ReportBytes == null)
                {
                    Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Report not generated, Please try again." }, MessageToken);
                    return;
                }

                SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
                Payload.ReportFile = ReportBytes;
                Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                Payload.PreviousView = ViewModelMappings.View.ReturnsAndCredits;
                Payload.CurrentViewTitle = "Item Return Report";
                Payload.PreviousViewTitle = "Returns & Credits";
                Payload.MessageToken = this.MessageToken;

                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;

                //var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.ReturnsAndCredits, CloseCurrentView = true, Payload = Payload, ShowAsModal = false };
                //Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.ReturnsAndCredits, CloseCurrentView = true, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);

                //  Messenger.Default.Send<SalesLogicExpress.Application.AppPdfViewer.AppPDfWindow>(new AppPdfViewer.PDfWindow


                //Helpers.ReportViewer viewer = new ReportViewer();
                //viewer.ViewReport(ReportBytes);


            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][Show Report][ExceptionStackTrace = " + ex.Message + "]");
            }
            Logger.Info("SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][End:ShowReport]");
        }

        void repEngine_Notify(string msg)
        {
            System.Windows.MessageBox.Show(msg);
        }
        async void LoadOrders()
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][AsyncStart:LoadOrders]");
                try
                {
                    ReturnItems = new TrulyObservableCollection<ReturnItem>();
                    ReturnItems = new CustomerReturnsManager().GetItemsForReturnsTab();
                    //returnItemsList = new TrulyObservableCollection<ReturnItem>(ReturnItems.ToList<ReturnItem>());
                    //ReturnItems.ItemPropertyChanged += ReturnItems_ItemPropertyChanged;

                    var orderIDCollection = ReturnItems.GroupBy(x => x.ReturnOrderID).Distinct();
                    orderIDCollection = orderIDCollection.OrderByDescending(x => x.Key);
                    var isOR = false; // variable stores OrderStatusId field.

                    foreach (var item in orderIDCollection)
                    {
                        ReturnOrder ro_item = new ReturnOrder();
                        var ro_items = ReturnItems.Where(x => x.ReturnOrderID == Convert.ToInt32(item.Key)).Clone();
                        //var orderDetailCollection = new CustomerReturnsManager().GetOrderDetails(item.Key);

                        foreach (var ro in ro_items)
                        {
                            ro.OrderDateF = ReturnItems.Where(x => x.ReturnOrderID == item.Key && x.OrderID == ro.OrderID && x.ItemId == ro.ItemId).FirstOrDefault().OrderDateF;
                            ro_item.ReturnItems.Add(ro as ReturnItem);
                        }
                        ro_item.ReturnOrderID = item.Key;
                        var dict = new CustomerReturnsManager().GetOrderFlags(item.Key);
                        if (dict != null)
                        {
                            ro_item.IsSettled = dict["IsSettled"];
                        }
                        if (ro_item.ReturnItems.Count() > 0 && ro_item.ReturnItems != null)
                            isOR = ro_item.IsOrderReturned = ro_item.ReturnItems[0].OrderStateID == ActivityKey.OrderReturned.GetStatusIdFromDB();

                        ro_item.IsEnabled = !ro_item.ReturnItems.Any(x => x.OrderQty == x.ReturnQty);
                        // Below line added for User Issue Id : 5. Everytime calc Extended price and Order Total
                        ro_item.ReturnItems.ToList().ForEach(x => { x.ExtendedPrice = (x.ReturnQty * Math.Round(x.UnitPrice, 2, MidpointRounding.AwayFromZero)); });
                        if (ro_item.IsOrderReturned)
                        {
                            ro_item.Surcharge = Convert.ToDecimal(ro_item.ReturnItems[0].EnergySurchargeAmt);
                            ro_item.OrderTotal = Math.Round(ro_item.ReturnItems.Sum(x => x.ExtendedPrice), 2, MidpointRounding.AwayFromZero) + (decimal)ro_item.ReturnItems[0].EnergySurchargeAmt + ro_item.ReturnItems.Sum(x => x.TaxAmount);

                        }
                        else
                        {
                            ro_item.Surcharge = 0;// Energy surcharge is taken at item level as it is common for all items which belongs to same order
                            ro_item.OrderTotal = Math.Round(ro_item.ReturnItems.Sum(x => x.ExtendedPrice), 2, MidpointRounding.AwayFromZero) + ro_item.ReturnItems.Sum(x => x.TaxAmount);

                        }
                        ro_item.OrderDate = ro_item.ReturnItems[0].OrderDateF;
                        ro_item.IsVoid = ro_item.ReturnItems[0].OrderStateID == ActivityKey.VoidAtROEntry.GetStatusIdFromDB() || ro_item.ReturnItems[0].OrderStateID == ActivityKey.VoidAtROPick.GetStatusIdFromDB();
                        ro_item.IsOnHold = ro_item.ReturnItems[0].OrderStateID == ActivityKey.HoldAtROEntry.GetStatusIdFromDB() || ro_item.ReturnItems[0].OrderStateID == ActivityKey.HoldAtROPrint.GetStatusIdFromDB();

                        ro_item.IsHoldAtPick = ro_item.ReturnItems[0].OrderStateID == ActivityKey.HoldAtROPick.GetStatusIdFromDB();

                        ro_item.IsEnabled = true;
                        if (ro_item.IsOrderReturned)
                        {
                            ro_item.ReturnItems.ToList().ForEach(x => { x.IsItemEnabled = false; });
                        }

                        // intentionally set flag to false if order is on hold or void. To hide 'Order Returned' label on returns tab
                        ro_item.IsOrderReturned = ro_item.IsOnHold || ro_item.IsVoid || ro_item.IsHoldAtPick || !isOR ? false : true;

                        ro_item.TotalItems = ro_item.ReturnItems.Count();
                        ReturnOrders.Add(ro_item);
                    }

                    ReturnOrderList = new TrulyObservableCollection<ReturnOrder>(ReturnOrders.ToList<ReturnOrder>()).Clone();
                    foreach (var returnOrder in ReturnOrderList)
                    {
                        foreach (var rItem in returnOrder.ReturnItems)
                        {
                            rItem.OrderDateF = ReturnItems.FirstOrDefault(x => x.ItemId == rItem.ItemId && x.ReturnOrderID == rItem.ReturnOrderID).OrderDateF;

                        }
                    }

                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][LoadOrders][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                finally
                {
                    this.ParentView.IsBusy = false;
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][AsyncStart:LoadOrders]");
            });

        }

        void ReturnItems_ItemPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(""))
            {
            }
        }

        internal void Search(string searchTerm)
        {
            if (searchTerm.Trim().Length > 2)
            {
                SearchItems(searchTerm);
            }
            if (searchTerm.Trim().Length == 0)
            {
                ResetSearch();
            }
        }
        CancellationTokenSource tokenForCancelTask = new CancellationTokenSource();
        public async void SearchItems(string searchTerm)
        {
            await Task.Run(() =>
           {
               Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][AsyncStart:SearchItems]");
               try
               {
                   if (searchTerm.Trim().Length > 2 || searchTerm.Trim().Length == 0)
                   {
                       ReturnOrders = new TrulyObservableCollection<ReturnOrder>(returnOrderList.Where(c => c.OrderId.ToString().ToLower().Contains(searchTerm.ToLower()) || c.ReturnOrderID.ToString().ToLower().Contains(searchTerm.ToLower())).ToList());
                       //ReturnItems = new TrulyObservableCollection<ReturnItem>(returnItemsList.Where(c => c.ItemDescription.ToLower().Contains(searchTerm.ToLower()) || c.ItemNumber.ToLower().Contains(searchTerm.ToLower())).ToList<ReturnItem>());
                       //foreach (var returnOrder in ReturnOrders)
                       //{
                       //    var refROItem = ReturnOrderList.FirstOrDefault(x => x.ReturnOrderID == returnOrder.ReturnOrderID);
                       //    returnOrder.ReturnItems = new ObservableCollection<ReturnItem>(refROItem.ReturnItems.Where(c => c.ItemDescription.ToLower().Contains(searchTerm.ToLower()) || c.ItemNumber.ToLower().Contains(searchTerm.ToLower()) || c.OrderID.ToString().ToLower().Contains(searchTerm.ToLower()) || c.ReturnOrderID.ToString().ToLower().Contains(searchTerm.ToLower())).ToList<ReturnItem>());
                       //}
                   }
               }
               catch (Exception ex)
               {
                   Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][SearchItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
               }
               Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][AsyncStart:SearchItems]");
           }, tokenForCancelTask.Token);
        }

        internal void ResetSearch()
        {
            //ReturnItems = new TrulyObservableCollection<ReturnItem>(returnItemsList.ToList<ReturnItem>());
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][Start:ResetSearch]");
            try
            {
                //foreach (var returnOrder in ReturnOrders)
                //{
                //    returnOrder.ReturnItems.Clear();
                //    returnOrder.ReturnItems = ReturnOrderList.FirstOrDefault(x => x.ReturnOrderID == returnOrder.ReturnOrderID).ReturnItems.Clone();
                //    for (int index = 0; index < returnOrderList.FirstOrDefault(x => x.ReturnOrderID == returnOrder.ReturnOrderID).ReturnItems.Count(); index++)
                //    {
                //        returnOrder.ReturnItems[index].OrderDateF = returnOrderList.FirstOrDefault(x => x.ReturnOrderID == returnOrder.ReturnOrderID).ReturnItems[index].OrderDateF;
                //    }
                //}
                ReturnOrders = returnOrderList;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][ResetSearch][ExceptionStackTrace = " + ex.Message + "]");
            }
            Logger.Info("SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][End:ResetSearch]");
        }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
