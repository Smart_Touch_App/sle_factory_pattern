﻿using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Telerik.Windows.Controls;
using Entities = SalesLogicExpress.Domain;
using System.Globalization;
using log4net;
using SalesLogicExpress.Application.ViewModelPayload;
using System.ComponentModel;
using SalesLogicExpress.Domain;
using System.Data;

namespace SalesLogicExpress.Application.ViewModels
{
    public class CustomerDashboard : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CustomerDashboard");
        StopManager stopManager = new StopManager();
        Managers.ServiceRouteManager serviceRouteM = new ServiceRouteManager();
        Managers.CustomerDashboardManager customerDashboardM = new CustomerDashboardManager();

        #region Properties
        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void NotifyStaticPropertyChanged(string propertyName)
        {
            if (StaticPropertyChanged != null)
                StaticPropertyChanged(null, new PropertyChangedEventArgs(propertyName));
        }

        Entities.CustomerContact _DefaultContact;
        Entities.CustomerNote _DefaultNote;
        public Entities.CustomerContact DefaultContact
        {
            get
            {
                return _DefaultContact;
            }
            set
            {
                _DefaultContact = value;
                OnPropertyChanged("DefaultContact");
            }
        }

        CashDelivery _CashDeliveryContext;
        public CashDelivery CashDeliveryContext
        {
            get { return _CashDeliveryContext; }
            set
            {
                _CashDeliveryContext = value;
                OnPropertyChanged("CashDeliveryContext");
            }
        }
        bool _CanAcceptChargeOnAccount = false;
        public bool CanAcceptChargeOnAccount
        {
            get
            {
                return _CanAcceptChargeOnAccount;
            }
            set
            {
                _CanAcceptChargeOnAccount = value;
                OnPropertyChanged("CanAcceptChargeOnAccount");
            }
        }
        ChargeOnAccount _ChargeOnAccountContext;
        public ChargeOnAccount ChargeOnAccountContext
        {
            get { return _ChargeOnAccountContext; }
            set
            {
                _ChargeOnAccountContext = value;
                OnPropertyChanged("ChargeOnAccountContext");
            }
        }
        ObservableCollection<ReasonCode> _COAReasonList;
        public ObservableCollection<ReasonCode> COAReasonList
        {
            get
            {
                return _COAReasonList;
            }
            set
            {
                _COAReasonList = value;
                OnPropertyChanged("COAReasonList");
            }
        }
        ReasonCode _SelectedReasonCode;
        public ReasonCode SelectedReasonCode
        {
            get
            {
                return _SelectedReasonCode;
            }
            set
            {
                _SelectedReasonCode = value;
                CheckForAcceptForChargeOnAccount();
                OnPropertyChanged("SelectedReasonCode");
            }
        }

        RequestAuthCodeViewModel _RequestAuthCodeVM;
        public RequestAuthCodeViewModel RequestAuthCodeVM
        {
            get
            {
                return _RequestAuthCodeVM;
            }
            set
            {
                _RequestAuthCodeVM = value;
                OnPropertyChanged("RequestAuthCodeVM");
            }
        }
        public Entities.CustomerNote DefaultNote
        {
            get
            {
                return _DefaultNote;
            }
            set
            {
                _DefaultNote = value;
                OnPropertyChanged("DefaultNote");
            }
        }
        private bool isCreateEnabled = true;
        public decimal AR30Days { get; set; }

        public decimal AR60Days { get; set; }

        public decimal AR90Days { get; set; }

        public decimal AR90MoreDays { get; set; }

        public decimal UnappliedCash { get; set; }

        public decimal TotalBalance
        {
            get { return (AR30Days + AR60Days + AR90Days + AR90MoreDays); }
        }
        public bool IsCreateEnabled
        {
            get
            {
                return this.isCreateEnabled;
            }
            set
            {
                this.isCreateEnabled = value;
                OnPropertyChanged("IsCreateEnabled");
            }
        }
        bool isMoveButtonEnabled = true;
        public bool IsMoveButtonEnabled
        {
            get { return isMoveButtonEnabled; }
            set { isMoveButtonEnabled = value; OnPropertyChanged("IsMoveButtonEnabled"); }
        }
        private ObservableCollection<PreOrderStops> _NextStopDates = new ObservableCollection<PreOrderStops>();
        public ObservableCollection<PreOrderStops> NextStopDates
        {
            get
            {
                return _NextStopDates;
            }
            set
            {
                _NextStopDates = value;
                OnPropertyChanged("NextStopDates");
            }
        }

        private bool isNoSaleButtonEnabled = false;
        public bool IsNoSaleButtonEnabled
        {
            get { return isNoSaleButtonEnabled; }

            set
            {
                isNoSaleButtonEnabled = value;

                OnPropertyChanged("IsNoSaleButtonEnabled");
            }
        }
        public Guid MessageToken { get; set; }
        Entities.Customer _Customer;
        public Entities.Customer Customer { get { return _Customer; } set { _Customer = value; OnPropertyChanged("Customer"); } }
        public Entities.ItemRestriction itemRestriction { get; set; }
        private string paymentDetailsCode;
        public string PaymentDetailsCode
        {
            get
            {
                return paymentDetailsCode;
            }
            set
            {
                paymentDetailsCode = value;
                OnPropertyChanged("PaymentDetailsCode");
            }
        }
        private string paymentDetailsDescription;
        public string PaymentDetailsDescription
        {
            get
            {
                return paymentDetailsDescription;
            }
            set
            {
                paymentDetailsDescription = value;
                OnPropertyChanged("PaymentDetailsDescription");
            }
        }

        private CreateAndMoveStopViewModel _MoveStopViewModelProperty;
        public CreateAndMoveStopViewModel MoveStopViewModelProperty
        {
            get
            {
                return _MoveStopViewModelProperty;
            }
            set
            {
                _MoveStopViewModelProperty = value;
                OnPropertyChanged("MoveStopViewModelProperty");
            }
        }
        private CreateAndMoveStopViewModel _CreateStopViewModelProperty;
        public CreateAndMoveStopViewModel CreateStopViewModelProperty
        {
            get
            {
                return _CreateStopViewModelProperty;
            }
            set
            {
                _CreateStopViewModelProperty = value;
                OnPropertyChanged("CreateStopViewModelProperty");
            }
        }
        bool isPreorderEnabled = true;
        public bool IsPreorderEnabled
        {
            get
            {
                return this.isPreorderEnabled;
            }
            set
            {
                this.isPreorderEnabled = value;
                OnPropertyChanged("IsPreorderEnabled");
            }
        }
        string _TodaysDateFormatted;
        public string TodaysDateFormatted
        {
            get { return _TodaysDateFormatted; }
            set { _TodaysDateFormatted = value; }
        }
        private bool isPopUpMoveButtonEnabled = true;
        public bool IsPopUpMoveButtonEnabled
        {
            get { return isPopUpMoveButtonEnabled; }
            set
            {
                isPopUpMoveButtonEnabled = value;
                OnPropertyChanged("IsPopUpMoveButtonEnabled");
            }
        }
        private bool isPopUpCreateButtonEnabled = true;
        public bool IsPopUpCreateButtonEnabled
        {
            get { return isPopUpCreateButtonEnabled; }
            set
            {
                isPopUpCreateButtonEnabled = value;
                OnPropertyChanged("IsPopUpCreateButtonEnabled");
            }
        }
        public bool IsPreTripDoneForDate { get; set; }

        #region Properties of Customer Object
        private DateTime? originalDate;
        public DateTime? OriginalDate
        {
            get { return originalDate; }
            set
            {
                originalDate = value;
                OnPropertyChanged("OriginalDate");
            }
        }
        private DateTime? showDate;
        public DateTime? ShowDate
        {
            get { return showDate; }
            set { showDate = value; OnPropertyChanged("ShowDate"); }
        }

        private bool isShowDate = false;
        public bool IsShowDate
        {
            get { return isShowDate; }
            set { isShowDate = value; OnPropertyChanged("IsShowDate"); }
        }
        private DateTime? rescheduledDate;
        public DateTime? RescheduledDate
        {
            get { return rescheduledDate; }
            set
            {
                rescheduledDate = value;
                OnPropertyChanged("RescheduledDate");
            }
        }
        private DateTime selectedDate;
        public DateTime SelectedDate
        {
            get
            {
                return selectedDate;
            }
            set
            {
                selectedDate = value;
                OnPropertyChanged("SelectedDate");
            }
        }
        //    private DateTime currentSelectedDate;   //Unused --Vignesh D

        private DateTime? nextDate;
        public DateTime? NextDate
        {
            get { return nextDate; }
            set { nextDate = value; OnPropertyChanged("NextDate"); }
        }

        private DateTime? lastDate;
        public DateTime? LastDate
        {
            get { return lastDate; }
            set { lastDate = value; OnPropertyChanged("LastDate"); }
        }
        private int pendingActivity;
        public int PendingActivity
        {
            get { return pendingActivity; }
            set
            {
                pendingActivity = value;
                OnPropertyChanged("PendingActivity");
            }
        }

        private bool isPreOrder = false;
        public bool IsPreOrder
        {
            get { return isPreOrder; }
            set { isPreOrder = value; OnPropertyChanged("IsPreOrder"); }
        }
        private bool isPendingActivity;
        public bool IsPendingActivity
        {
            get { return isPendingActivity; }
            set { isPendingActivity = value; OnPropertyChanged("IsPendingActivity"); }
        }
        private bool isSalesOrderEnabled = true;

        public bool IsSalesOrderEnabled
        {
            get { return isSalesOrderEnabled; }
            set { isSalesOrderEnabled = value; OnPropertyChanged("IsSalesOrderEnabled"); }
        }
        private bool isReturnsCreditsEnabled = true;

        public bool IsReturnsCreditsEnabled
        {
            get { return isReturnsCreditsEnabled; }
            set { isReturnsCreditsEnabled = value; OnPropertyChanged("IsReturnsCreditsEnabled"); }
        }
        private bool isPaymentAREnabled = true;

        public bool IsPaymentAREnabled
        {
            get { return isPaymentAREnabled; }
            set { isPaymentAREnabled = value; OnPropertyChanged("IsPaymentAREnabled"); }
        }

        //private string 

        private DateTime todayDate = DateTime.Today;
        public DateTime TodayDate
        {
            get
            {
                return todayDate;
            }
            set
            {
                todayDate = value;
                OnPropertyChanged("TodayDate");
            }
        }

        private bool activityPerformed;
        public bool ActivityPerformed
        {
            get
            { return activityPerformed; }
            set
            {
                activityPerformed = value;
                OnPropertyChanged("ActivityPerformed");
            }
        }

        private bool isCreditHold;
        public bool IsCreditHold
        {
            get { return isCreditHold; }
            set { isCreditHold = value; OnPropertyChanged("IsCreditHold"); }
        }
        private bool hasStopOccured;
        public bool HasStopOccured
        {
            get
            {
                return hasStopOccured;
            }
            set
            {
                hasStopOccured = value;
                OnPropertyChanged("HasStopOccured");
            }
        }

        private bool isStopRescheduled;
        public bool IsStopRescheduled
        {
            get { return isStopRescheduled; }
            set
            {
                isStopRescheduled = value;
                OnPropertyChanged("IsStopRescheduled");
            }
        }

        private bool isStopMoved;
        public bool IsStopMoved
        {
            get { return isStopMoved; }
            set
            {
                isStopMoved = value;
                OnPropertyChanged("IsStopMoved");
            }
        }

        private bool isStopPlanned;
        public bool IsStopPlanned
        {
            get { return isStopPlanned; }
            set
            {
                isStopPlanned = value;
                OnPropertyChanged("IsStopPlanned");
            }
        }

        private bool isStopUnPlanned;
        public bool IsStopUnPlanned
        {
            get { return isStopUnPlanned; }
            set
            {
                isStopUnPlanned = value;
                OnPropertyChanged("IsStopUnPlanned");
            }
        }

        private int stopCount;
        public int StopCount
        {
            get { return stopCount; }
            set { stopCount = value; OnPropertyChanged("StopCount"); }
        }

        private bool isTodaysStop;
        public bool IsTodaysStop
        {
            get { return isTodaysStop; }
            set
            {
                isTodaysStop = value;
                OnPropertyChanged("IsTodaysStop");
            }
        }
        private int stopSequenceNo;
        public int StopSequenceNo
        {
            get { return stopSequenceNo; }
            set { stopSequenceNo = value; OnPropertyChanged("StopSequenceNo"); }
        }

        public int oriValueOfNoSale = -1;
        private int selectedReason;
        public int SelectedReason
        {
            get { return selectedReason; }
            set
            {
                selectedReason = value;
                if (SelectedReason > 0 && selectedReason != oriValueOfNoSale)
                {
                    IsNoSaleButtonEnabled = true;
                }
                else
                    IsNoSaleButtonEnabled = false;
                OnPropertyChanged("SelectedReason");
            }
        }
        private Dictionary<int, string> noSaleReason;
        public Dictionary<int, string> NoSaleReason
        {
            get { return noSaleReason; }
            set { noSaleReason = value; OnPropertyChanged("NoSaleReason"); }
        }

        private bool isUnplannedStopCreatedForToday;
        public bool IsUnplannedStopCreatedForToday//Sathish Unwanted Static Keyword code
        {
            get { return isUnplannedStopCreatedForToday; }
            set { isUnplannedStopCreatedForToday = value; NotifyStaticPropertyChanged("IsUnplannedStopCreatedForToday"); }
        }

        private bool saleStatus = false;
        public bool SaleStatus
        {
            get { return saleStatus; }
            set { saleStatus = value; OnPropertyChanged("SaleStatus"); }
        }

        private bool isCustomerSeasonal = false;
        public bool IsCustomerSeasonal
        {
            get { return isCustomerSeasonal; }
            set { isCustomerSeasonal = value; OnPropertyChanged("IsCustomerSeasonal"); }
        }

        private bool isAllCustomerTab;
        public bool IsAllCustomerTab
        {
            get { return isAllCustomerTab; }
            set { isAllCustomerTab = value; OnPropertyChanged("IsAllCustomerTab"); }
        }

        private DateTime? customerNextStopFromToday;
        public DateTime? CustomerNextStopFromToday
        {
            get { return customerNextStopFromToday; }
            set { customerNextStopFromToday = value; OnPropertyChanged("CustomerNextStopFromToday"); }
        }

        private DateTime? customerLastStopFromToday;
        public DateTime? CustomerLastStopFromToday
        {
            get { return customerLastStopFromToday; }
            set { customerLastStopFromToday = value; OnPropertyChanged("CustomerLastStopFromToday"); }
        }

        public int previousReason;
        #endregion

        #endregion

        #region Fields

        //Variable used to determine if the create / move stop button is click in the dialog window
        //It helps in returning correct flag for opening the correct feature screens
        public bool IsCreateOrderMoveStopCliked = false;
        public bool IsVoidCreateOrderStopCliked = false;
        private int _VoidOrderReasonId;//Sathish Unwanted Static Keyword code

        ObservableCollection<Customer> todaysStops = new ObservableCollection<Customer>();
        //   string customerNo;   //Unused --Vignesh D
        //   bool IsLastStopServiced;   //Uused --Vignesh D
        bool IsStopMovedToOriginalDate = false;
        public bool IsActionInitiated = false;
        private bool isNextStopDate = false;
        public bool IsNextStopDate
        {
            get { return isNextStopDate; }
            set { isNextStopDate = value; OnPropertyChanged("IsNextStopDate"); }
        }
        private bool isTodayDate = false;
        public bool IsTodayDate
        {
            get { return isTodayDate; }
            set { isTodayDate = value; OnPropertyChanged("IsTodayDate"); }
        }
        private bool isOriginalDate = false;
        public bool IsOriginalDate
        {
            get { return isOriginalDate; }
            set { isOriginalDate = value; OnPropertyChanged("IsOriginalDate"); }
        }
        private bool isRescheduleDate = false;
        public bool IsRescheduleDate
        {
            get { return isRescheduleDate; }
            set { isRescheduleDate = value; OnPropertyChanged("IsRescheduleDate"); }
        }
        private bool isMoveButton = false;
        public bool IsMoveButton
        {
            get { return isMoveButton; }
            set { isMoveButton = value; OnPropertyChanged("IsMoveButton"); }
        }
        private bool isNoSale = false;
        public bool IsNoSale
        {
            get { return isNoSale; }
            set { isNoSale = value; OnPropertyChanged("IsNoSale"); }
        }
        private bool isItemRestricted = false;
        public bool IsItemRestricted
        {
            get { return isItemRestricted; }
            set { isItemRestricted = value; OnPropertyChanged("IsItemRestricted"); }
        }

        private string stopInfoIcon = string.Empty;
        public string StopInfoIcon
        {
            get { return stopInfoIcon; }
            set { stopInfoIcon = value; OnPropertyChanged("StopInfoIcon"); }
        }

        private bool stopInfoSequence = false;
        public bool StopInfoSequence
        {
            get { return stopInfoSequence; }
            set { stopInfoSequence = value; OnPropertyChanged("StopInfoSequence"); }
        }


        public int VoidOrderReasonId//Sathish Unwanted Static Keyword code
        {
            get { return _VoidOrderReasonId; }
            set { _VoidOrderReasonId = value; }
        }
        #endregion

        #region Delegate Commands
        public DelegateCommand ShowOrderTemplate { get; set; }
        public DelegateCommand ShowSalesOrder { get; set; }
        public DelegateCommand OpenPreOrderDialog { get; set; }
        public DelegateCommand ShowCreateStopDialog { get; set; }
        public DelegateCommand ReleaseCreditHold { get; set; }
        public DelegateCommand ShowMoveStopDialog { get; set; }
        public DelegateCommand NoSaleReasonCommand { get; set; }
        public DelegateCommand MoveToPreOrder { get; set; }
        public DelegateCommand ShowPaymentAR { get; set; }
        public DelegateCommand MoveOrCreateDialogLoaded { get; set; }
        public DelegateCommand CreateStopForToday { get; set; }
        public DelegateCommand AcceptChargeOnAccount { get; set; }
        public DelegateCommand DeclineChargeOnAccount { get; set; }
        public DelegateCommand MoveStopOnToday { get; set; }
        public DelegateCommand ShowReturnsAndCredits { get; set; }
        public DelegateCommand VoidOrder { get; set; }
        #endregion

        #region Constructor/Desctructor
        public CustomerDashboard(Entities.Customer Customer, DateTime SelectedDate)
        {
            DateTime timer = DateTime.Now;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:CustomerDashboardViewModel]\t" + DateTime.Now + "");

            try
            {
                this.Customer = Customer;
                this.SelectedDate = SelectedDate;
                InitializeCommands();
                ToggleButtons();
                LoadCustomerDashboard();
                GetNextLastStopFromToday();
                TodaysDateFormatted = (DateTime.Now.ToString("dddd', 'MM'/'dd'/'yyyy "));
                //Reinitialise Order State
                Order.OrderStatus = ActivityKey.None;
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerDashboard CustomerDashboard(" + SelectedDate.ToString() + ") error: " + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:CustomerDashboardViewModel]\t" + (DateTime.Now - timer) + "");

        }

        public CustomerDashboard()
        {

        }
        #endregion

        #region Methods

        public bool GetCreateMoveStopPopUp()
        {
            var dialog = new Helpers.DialogWindow { TemplateKey = "ConfirmMoveorCreateDialog", Title = "Create or Move Stop", Payload = null };
            Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            if (IsCreateOrderMoveStopCliked)
            {
                IsCreateOrderMoveStopCliked = false;
                return true;
            }
            else
            {
                IsCreateOrderMoveStopCliked = false;
                return false;

            }
            //if (dialog.Closed) return false;
            //else return true;
        }

        public bool GetVoidCreateStopPopUp()
        {
            var dialog = new Helpers.DialogWindow { TemplateKey = "ConfirmVoidandCreateDialog", Title = "Void and Create Stop for today", Payload = null };
            Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            if (IsVoidCreateOrderStopCliked)
            {
                IsVoidCreateOrderStopCliked = false;
                return true;
            }
            else
            {
                IsVoidCreateOrderStopCliked = false;
                return false;

            }
        }

        public void GetAlertPopup()
        {
            var alartMessage = new Helpers.AlertWindow { Message = Helpers.Constants.PreTripInspection.AlertMessageForFutureStop, MessageIcon = "Alert" };
            Messenger.Default.Send<Helpers.AlertWindow>(alartMessage, MessageToken);
        }
        public bool GetCreateStopPopup()
        {
            var dialog = new Helpers.DialogWindow { TemplateKey = "ConfirmCreateDialog", Title = "Create Stop", Payload = null };
            Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            if (dialog.Closed)
                return false;
            else
                return true;
        }

        public void GetCreateOrMoveStopDetails()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:GetCreateOrMoveStopDetails]");

            try
            {
                Dictionary<string, string> nextLastStopInfo = null;
                nextLastStopInfo = serviceRouteM.GetVisiteeStopInfo(Customer, TodayDate, false, true);

                var CustomerNextStop = (nextLastStopInfo["NextStop"] == "None") ? new DateTime() : DateTime.Parse(nextLastStopInfo["NextStop"], CultureInfo.CreateSpecificCulture("en-US"));
                //    var CustomerLastStop = (nextLastStopInfo["LastStop"] == "None") ? new DateTime() : DateTime.Parse(nextLastStopInfo["LastStop"], CultureInfo.CreateSpecificCulture("en-US"));   //Unused --Vignesh D
                var todaysStops = ServiceRoute.TodaysStops = serviceRouteM.GetCustomersForRoute(CommonNavInfo.RouteUser, TodayDate.Date);
                if (todaysStops.Any(x => (x.CustomerNo == Customer.CustomerNo) && (x.StopType.ToLower() != "moved")))
                {
                    GetAlertPopup();
                    GetStopInfoFromDailyStop(Customer);
                    //return;
                }
                else
                {
                    if ((DateTime.Compare(Customer.StopDate.Value.Date, TodayDate.Date) > 0))
                    {
                        if (Customer.StopType.ToLower() == "unplanned")
                        {
                            bool flag = GetCreateMoveStopPopUp();
                            if (!flag) return;

                            IsActionInitiated = true;
                        }
                        else if ((DateTime.Compare(Customer.StopDate.Value.Date, CustomerNextStop.Date) > 0))
                        {
                            bool flag = GetCreateStopPopup();
                            if (!flag) return;
                            IsActionInitiated = true;
                            // GetStopInfoFromDailyStop(Customer);
                            //return;
                        }
                        else if ((DateTime.Compare(CustomerNextStop.Date, Customer.StopDate.Value.Date) == 0))
                        {
                            bool flag = GetCreateMoveStopPopUp();
                            if (!flag) return;
                            IsActionInitiated = true;
                            //GetStopInfoFromDailyStop(Customer);
                            //return;
                        }
                    }
                    else if ((DateTime.Compare(Customer.StopDate.Value.Date, TodayDate.Date) < 0))
                    {
                        if (todaysStops.Any(x => (x.CustomerNo == Customer.CustomerNo) && (x.StopType.ToLower() != "moved")))
                        {
                            GetAlertPopup();
                            GetStopInfoFromDailyStop(Customer);
                            //return;
                        }
                        else
                        {
                            if (DateTime.Compare(Customer.StopDate.Value.Date, CustomerLastStopFromToday.Value.Date) < 0)
                            {
                                bool flag = GetCreateStopPopup();
                                if (!flag) return;
                                IsActionInitiated = true;
                                // GetStopInfoFromDailyStop(Customer);
                                //return;
                            }
                            else if ((DateTime.Compare(CustomerLastStopFromToday.Value.Date, Customer.StopDate.Value.Date) == 0))
                            {
                                Domain.Customer cust = GetNewCustomer(CustomerLastStopFromToday.Value);
                                if (cust.HasActivity || cust.SaleStatus.ToLower() == "nosale")
                                {
                                    bool flag = GetCreateStopPopup();
                                    if (!flag) return;
                                    IsActionInitiated = true;
                                    //  GetStopInfoFromDailyStop(Customer);
                                    //return;
                                }
                                else
                                {
                                    bool flag = GetCreateMoveStopPopUp();
                                    if (!flag) return;
                                    IsActionInitiated = true;
                                    //  GetStopInfoFromDailyStop(Customer);
                                    //return;
                                }
                            }
                        }
                    }
                }
               todaysStops = null;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][GetCreateOrMoveStopDetails][customerID =" + Customer.CustomerNo + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:GetCreateOrMoveStopDetails]");

        }
        void InitializeCommands()
        {
            #region ShowOrderTemplate
            ShowOrderTemplate = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:ShowOrderTemplate]");

                try
                {

                    if ((bool)CommonNavInfo.IsEODPendingForVerification)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.EODConfirmedValidation, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);

                        return;
                    }

                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Order Template";
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Customer Home";
                    Customer.Address = CommonNavInfo.Customer.Address;
                    Customer.Zip = CommonNavInfo.Customer.Zip;
                    Customer.State = CommonNavInfo.Customer.State;
                    Customer.City = CommonNavInfo.Customer.City;
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(Customer);
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.NavigationParameter = param;

                    ReinitializeOrderPayload();
                    ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.CustomerHome_DashBoard.ToString();
                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.OrderTemplate, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, ShowAsModal = false };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][ShowOrderTemplate][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:ShowOrderTemplate]");
            });
            #endregion



            AcceptChargeOnAccount = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:AcceptChargeOnAccount]");

                try
                {
                    //!RequestAuthCodeVM.IsAuthCodeValid
                    if (!RequestAuthCodeVM.IsAuthCodeValid && ChargeOnAccountContext.IsApprovalCodeNeeded)
                    {
                        var Alert = new AlertWindow { Message = Constants.CashCollection.MandatoryApprovalCodeAlert };
                        Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                        return;
                    }

                    IsSalesOrderEnabled = true;
                    if (RequestAuthCodeVM.IsAuthCodeValid && ChargeOnAccountContext.IsApprovalCodeNeeded)
                    {
                        RequestAuthCodeVM.LogAuthCode(PayloadManager.ApplicationPayload.LoggedInUserID, RequestAuthCodeVM.RequestCode, RequestAuthCodeVM.Authcode, CommonNavInfo.RouteID.ToString(), PayloadManager.OrderPayload.SerializeToJson(), PayloadManager.OrderPayload.Customer.CustomerNo, CashDeliveryContext.TotalBalanceAmount.ToString(), RequestAuthCodeVM.Params.Feature.ToString());
                    }

                    Messenger.Default.Send<CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][AcceptChargeOnAccount][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:AcceptChargeOnAccount]");
            });
            DeclineChargeOnAccount = new DelegateCommand((param) =>
            {
                Messenger.Default.Send<CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
            });

            #region ShowSalesOrder
            ShowSalesOrder = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:ShowSalesOrder]");

                try
                {
                    //  bool flag;   //UNused --Vignesh D
                    IsPreTripDoneForDate = GetPreTripDetailsForDate();
                    if (IsPreTripDoneForDate)
                    {
                        //Issue ID : 55 Completed the Settlement confirmation and Void it and tried to create a SO but it pops up a message stating that " verification is pending void the settlement"- Fixed by Zakir
                        string query = "CALL BUSDTA.SP_GetStatuscountForSettlement()";
                        int i = Convert.ToInt16(DbEngine.ExecuteScalar(query));
                        query = string.Empty;
                        if (i > 0)
                        {

                            if ((bool)CommonNavInfo.IsEODPendingForVerification)
                            {
                                var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.EODConfirmedValidation, MessageIcon = "Alert" };
                                Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                                return;
                            }
                        }
                        if (!IsAllCustomerTab && DateTime.Compare(Customer.StopDate.Value.Date, TodayDate.Date) != 0)
                        {
                            GetCreateOrMoveStopDetails();
                            if (IsActionInitiated)
                                NavigateForSalesOrder(param);
                            //return;
                        }
                        else if (!Customer.IsTodaysStop)
                        {
                            ShowStopConfirmation();
                            if (IsActionInitiated)
                                NavigateForSalesOrder(param);
                            return;
                        }
                        else if (Customer.IsTodaysStop && Customer.StopType.ToLower() != "moved")
                        {
                            NavigateForSalesOrder(param);
                        }
                    }
                    else
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.Common.PreTripErrorMsg, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        if (alertMessage.Closed) return;
                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.CustomerHome, Refresh = true, CloseCurrentView = true, ShowAsModal = false };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][ShowSalesOrder][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:ShowSalesOrder]");
            });
            #endregion

            #region OpenPreOrderDialog
            OpenPreOrderDialog = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:OpenPreOrderDialog]");
                try
                {
                    IsPreTripDoneForDate = GetPreTripDetailsForDate();
                    if (IsPreTripDoneForDate)
                    {
                        if ((bool)CommonNavInfo.IsEODPendingForVerification)
                        {
                            var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.EODConfirmedValidation, MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);

                            return;
                        }
                        if (!IsAllCustomerTab && DateTime.Compare(Customer.StopDate.Value.Date, TodayDate.Date) != 0)
                        {
                            GetCreateOrMoveStopDetails();
                            if (IsActionInitiated)
                                OpenPreOrderDialogForAction();

                            //return;
                        }
                        else if (!Customer.IsTodaysStop)
                        {
                            ShowStopConfirmation();
                            return;
                        }
                        else if (Customer.IsTodaysStop && Customer.StopType.ToLower() != "moved")
                        {
                            OpenPreOrderDialogForAction();
                        }
                    }
                    else
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.Common.PreTripErrorMsg, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        if (alertMessage.Closed) return;
                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.CustomerHome, Refresh = true, CloseCurrentView = true, ShowAsModal = false };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][OpenPreOrderDialog][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:OpenPreOrderDialog]");
            });
            #endregion

            #region MoveStopDialog

            ReleaseCreditHold = new DelegateCommand((param) =>
            {

                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:ReleaseCreditHold]");

                try
                {
                    Managers.CollectionManager collectionManager = new Managers.CollectionManager();

                    CashDeliveryContext = collectionManager.GetCreditHoldInfo(CommonNavInfo.Customer.CustomerNo);
                    RequestAuthCodeViewModel.RequestAuthCodeParams codeParams = new RequestAuthCodeViewModel.RequestAuthCodeParams
                    {
                        Amount = CashDeliveryContext.TotalBalanceAmount,
                        UserID = UserManager.UserId.ToString(),
                        Feature = AuthCodeManager.Feature.CreditHold
                    };
                    RequestAuthCodeVM = new RequestAuthCodeViewModel(codeParams);

                    RequestAuthCodeVM.AuthCodeChanged += RequestAuthCodeVM_AuthCodeChanged;
                    ChargeOnAccountContext = collectionManager.GetChargeOnAccountInfo(CommonNavInfo.Customer.CustomerNo, Order.OrderId.ToString(), CashDeliveryContext.TotalBalanceAmount, CashDeliveryContext.PreviousBalanceAmount, CashDeliveryContext.CurrentInvoiceAmount);
                    if (!ChargeOnAccountContext.IsApprovalCodeNeeded)
                    {
                        RequestAuthCodeVM.IsAuthCodeValid = true;
                        RequestAuthCodeVM.RequestCode = string.Empty;
                    }
                    COAReasonList = new Managers.OrderManager().GetReasonListFor("Credit Hold");
                    var dialog = new Helpers.DialogWindow { TemplateKey = "AuthorizeCreditHold", Title = "Release Credit Hold", Payload = this };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][ReleaseCreditHold][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:ReleaseCreditHold]");
            });

            ShowMoveStopDialog = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:ShowMoveStopDialog]");

                try
                {
                    IsMoveButtonEnabled = false;
                    MoveStopViewModelProperty = new CreateAndMoveStopViewModel(Customer);

                    MoveStopViewModelProperty.StopModified += MoveStopViewModelProperty_StopMoved;

                    // Enable already moved stop to move again on original date
                    DateTime moveStopBaseDate = new DateTime();
                    if (IsAllCustomerTab)
                    {
                        if (Customer.StopType.ToLower() == "moved")
                        {
                            moveStopBaseDate = Customer.RescheduledDate.Value;
                            Customer = GetNewCustomer(moveStopBaseDate);
                        }
                        else if (Customer.StopType.ToLower() == "rescheduled")
                        {
                            using (CustomerDashboardManager cdm = new CustomerDashboardManager())
                            {
                                Customer.RescheduledDate = cdm.GetRescheduledDateForFuture(Customer.CustomerNo, Customer.OriginalDate.Value);
                                moveStopBaseDate = Customer.RescheduledDate.Value;
                            }
                        }
                        else// if (DateTime.Compare(Customer.StopDate.Value.Date, DateTime.Today.Date) > 0)
                        {
                            if (Customer.IsTodaysStop)
                            {
                                moveStopBaseDate = TodayDate.Date;
                                Customer = GetNewCustomer(TodayDate.Date);
                            }

                            else
                            {
                                if (NextDate.HasValue)
                                {
                                    moveStopBaseDate = NextDate.Value; //Customer.NextStop.StringToUSDateFormat();
                                    Customer = GetNewCustomer(NextDate.Value.Date);
                                }
                            }
                        }
                    }
                    else
                    {
                        moveStopBaseDate = Customer.StopDate.Value;
                    }
                    // Get CustomerInstance for the rescheduled date 
                    //
                    stopManager.MoveStopDialogContext(ref _MoveStopViewModelProperty, Customer, moveStopBaseDate, MessageToken);

                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][ShowMoveStopDialog][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:ShowMoveStopDialog]");
            });
            #endregion

            #region ShowCreateStopDialog
            ShowCreateStopDialog = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:ShowCreateStopDialog]");

                try
                {
                    if ((bool)CommonNavInfo.IsEODPendingForVerification)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.EODConfirmedValidation, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);

                        return;
                    }

                    IsCreateEnabled = false;
                    DateTime createStopDate;
                    CreateStopViewModelProperty = new CreateAndMoveStopViewModel(Customer);
                    CreateStopViewModelProperty.StopModified += CreateStopViewModelProperty_StopCreated;
                    if (Customer.StopDate.HasValue)
                    {
                        if (Customer.StopDate.Value < TodayDate.Date)
                        {
                            createStopDate = TodayDate.Date;
                        }
                        else
                        {
                            createStopDate = Customer.StopDate.Value;
                        }
                    }
                    else
                    {
                        createStopDate = TodayDate.Date;
                    }
                    Customer.IsFromListingTab = IsAllCustomerTab;
                    stopManager.CreateStopDialogContext(ref _CreateStopViewModelProperty, Customer, createStopDate, MessageToken);

                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][ShowCreateStopDialog][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:ShowCreateStopDialog]");
            });
            #endregion

            #region NoSaleReasonCommand
            NoSaleReasonCommand = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:NoSaleReasonCommand]");
                try
                {
                    if (param == null)
                    {
                        return;
                    }
                    // string noReason = param as string;
                    int noSaleReasonID = Convert.ToInt32(param);
                    string reason = NoSaleReason[noSaleReasonID];
                    Managers.CustomerDashboardManager dashboardManager = new CustomerDashboardManager();
                    string tempStopDate = string.Empty;
                    if (SelectedDate.Date >= DateTime.Now.Date)
                    {
                        tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                        string stopID = dashboardManager.GetStopIDForCust(this.Customer.CustomerNo, tempStopDate);
                        dashboardManager.UpdateNoSaleActivity(noSaleReasonID, stopID.ToString(), this.Customer.CustomerNo, CommonNavInfo.RouteUser, reason, tempStopDate);
                    }
                    else
                    {
                        tempStopDate = SelectedDate.Date.ToString("yyyy-MM-dd");
                        dashboardManager.UpdateNoSaleActivity(noSaleReasonID, this.Customer.StopID, this.Customer.CustomerNo, CommonNavInfo.RouteUser, reason, tempStopDate);
                    }
                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, ShowAsModal = false, Refresh = true };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    Customer.SaleStatus = dashboardManager.GetStatusUpdateForNoActivity(this.Customer.StopID, this.Customer.CustomerNo);
                    
                    // NextStop - Date Validation ( Avoid Null String - Date Conversion) - 12/22/2016
                    if (Customer.NextStop != "")
                    {
                        if (!Customer.IsTodaysStop && ((DateTime.Compare(TodayDate.Date, Customer.NextStop.StringToUSDateFormat().Date) == 0)))
                        {
                            Customer.IsTodaysStop = true;
                        }
                    }
                    
                    // PYU Issue ID: 13 No Activity Pop-Up not Showing to Previous Day Customer - 12/22/2016
                    if (Customer.IsTodaysStop)
                    {
                        Customer = GetNewCustomer(TodayDate);
                    }
                    else
                    {
                        Customer = GetNewCustomer(SelectedDate.Date);
                    }

                    PayloadManager.OrderPayload.Customer = Customer;
                    PayloadManager.OrderPayload.Customer.CustName = Customer.Name;

                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][NoSaleReasonCommand][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:NoSaleReasonCommand]");
            });
            #endregion

            #region moveToPreOrderCommand
            MoveToPreOrder = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:MoveToPreOrder]");
                try
                {
                    PreOrderStops POMD = param as PreOrderStops;
                    PayloadManager.OrderPayload.Customer = Customer;
                    PayloadManager.OrderPayload.Customer.CustName = Customer.Name;
                    PayloadManager.OrderPayload.PreOrderStop = POMD;
                    PayloadManager.PreOrderPayload.PreOrderStops = POMD;
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Pre-Order";
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Customer Home";
                    Customer.Address = CommonNavInfo.Customer.Address;
                    Customer.Zip = CommonNavInfo.Customer.Zip;
                    Customer.State = CommonNavInfo.Customer.State;
                    Customer.City = CommonNavInfo.Customer.City;
                    PayloadManager.PreOrderPayload.Customer = Customer;
                    Managers.PreOrderManager preOrderM = new PreOrderManager();

                    PayloadManager.PreOrderPayload.PreOrderCreatedDate = preOrderM.CheckPreOrderForSameDate(Customer.CustomerNo, POMD.Stopdate.StringToUSDateFormat());

                    if (Customer.StopType.ToLower() == "unplanned" && (PayloadManager.PreOrderPayload.PreOrderCreatedDate.Date == DateTime.Now.Date))
                    {
                        PayloadManager.PreOrderPayload.IsPreOrderExistForSameDate = true;
                    }
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(Customer);
                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PreOrder, Payload = IsActionInitiated, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, ShowAsModal = false };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][MoveToPreOrder][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:MoveToPreOrder]");
            });
            #endregion

            #region PaymentAR
            ShowPaymentAR = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:ShowPaymentAR]");
                try
                {
                    IsPreTripDoneForDate = GetPreTripDetailsForDate();
                    if (IsPreTripDoneForDate)
                    {
                        if (!IsAllCustomerTab && DateTime.Compare(Customer.StopDate.Value.Date, TodayDate.Date) != 0)
                        {
                            GetCreateOrMoveStopDetails();
                            //return;
                            if (IsActionInitiated)
                                NavigateToPaymentsAR();
                        }
                        else if (!Customer.IsTodaysStop)
                        {
                            ShowStopConfirmation();
                            if (IsActionInitiated)
                                NavigateToPaymentsAR();
                            return;
                        }
                        else
                        {
                            NavigateToPaymentsAR();
                        }
                    }
                    else
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.Common.PreTripErrorMsg, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        if (alertMessage.Closed) return;

                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.CustomerHome, Refresh = true, CloseCurrentView = true, ShowAsModal = false };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][ShowPaymentAR][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:ShowPaymentAR]");
            });
            #endregion

            # region MoveOrCreateDialogLoaded
            MoveOrCreateDialogLoaded = new DelegateCommand((param) =>
            {
                IsMoveButtonEnabled = true;
                IsCreateEnabled = true;
            });
            #endregion

            # region CreateStopForToday
            CreateStopForToday = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:CreateStopForToday]");
                try
                {
                    IsCreateOrderMoveStopCliked = true;
                    CreateStopViewModelProperty = new CreateAndMoveStopViewModel(Customer);
                    //if (serviceRouteM.CreateUnplannedStop(Customer, TodayDate.Date))
                    //{
                    //    IsUnplannedStopCreatedForToday = true;
                    //    Customer cust = GetNewCustomer(TodayDate.Date);
                    //    PayloadManager.OrderPayload.Customer = cust;
                    //    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, ShowAsModal = false, Refresh = true };
                    //    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    //}
                    CreateStopViewModelProperty.Visitee = Customer;

                    //CreateStopViewModelProperty.DailyStopDate = Customer.StopDate.Value;
                    CreateStopViewModelProperty.StopModified += CreateStopViewModelProperty_StopCreated;
                    CreateStopViewModelProperty.CreateStop.Execute(TodayDate.Date);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][CreateStopForToday][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:CreateStopForToday]");
            });
            #endregion

            #region MoveStopOnToday
            MoveStopOnToday = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:MoveStopOnToday]");
                try
                {
                    IsCreateOrderMoveStopCliked = true;
                    MoveStopViewModelProperty = new CreateAndMoveStopViewModel(Customer);
                    //if (serviceRouteM.MoveCustomerStop(Customer, TodayDate.Date, Customer.StopDate.Value))
                    //{

                    //    //IsStopMovedToOriginalDate = true;
                    //    Customer = GetNewCustomer(TodayDate.Date);
                    //    PayloadManager.OrderPayload.Customer = Customer;
                    //    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, ShowAsModal = false, Refresh = true };
                    //    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    //}

                    MoveStopViewModelProperty.Visitee = Customer;
                    MoveStopViewModelProperty.DailyStopDate = Customer.StopDate.Value;
                    MoveStopViewModelProperty.StopModified += MoveStopViewModelProperty_StopMoved;
                    MoveStopViewModelProperty.MoveStop.Execute(TodayDate.Date);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][MoveStopOnToday][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:MoveStopOnToday]");
            });
            #endregion

            #region ShowReturnsAndCredits
            ShowReturnsAndCredits = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:ShowReturnsAndCredits]");
                try
                {
                    IsPreTripDoneForDate = GetPreTripDetailsForDate();
                    if (IsPreTripDoneForDate)
                    {
                        if (!IsAllCustomerTab && DateTime.Compare(Customer.StopDate.Value.Date, TodayDate.Date) != 0)
                        {
                            GetCreateOrMoveStopDetails();
                            //return;
                            if (IsActionInitiated)
                                NavigateToReturnsAndCredits();
                        }
                        else if (!Customer.IsTodaysStop)
                        {
                            ShowStopConfirmation();
                            if (IsActionInitiated)
                                NavigateToReturnsAndCredits();
                            return;
                        }
                        else if (Customer.IsTodaysStop)
                        {
                            NavigateToReturnsAndCredits();
                        }
                    }
                    else
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.Common.PreTripErrorMsg, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        if (alertMessage.Closed) return;

                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.CustomerHome, Refresh = true, CloseCurrentView = true, ShowAsModal = false };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);

                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][ShowReturnsAndCredits][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:ShowReturnsAndCredits]");
            });
            #endregion
        }

        private void NavigateToReturnsAndCredits()
        {
            PayloadManager.ReturnOrderPayload.Customer = Customer;
            ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.CustomerHome_DashBoard.ToString();
            PayloadManager.ReturnOrderPayload.StopDate = SelectedDate;
            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, Payload = IsActionInitiated, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, ShowAsModal = false, Refresh = true };
            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
        }

        private void NavigateToPaymentsAR()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Customer Home";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Payments & AR";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.CustomerHome;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ARPayment;

            ReinitializeOrderPayload();
            ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.CustomerHome_DashBoard.ToString();
            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ARPayment, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, Payload = IsActionInitiated, ShowAsModal = false };
            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
        }

        private void OpenPreOrderDialogForAction()
        {
            LoadPreOrderStops(selectedDate);
            var dialog = new Helpers.DialogWindow { TemplateKey = "OpenPreOrderDialog", Title = "Create Pre-Order" };
            Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
        }

        private void NavigateForSalesOrder(object param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:NavigateForSalesOrder]");
            try
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Order Template";
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Customer Home";
                Customer.Address = CommonNavInfo.Customer.Address;
                Customer.Zip = CommonNavInfo.Customer.Zip;
                Customer.State = CommonNavInfo.Customer.State;
                Customer.City = CommonNavInfo.Customer.City;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(Customer);
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.NavigationParameter = param;

                ReinitializeOrderPayload();
                ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.CustomerHome_DashBoard.ToString();

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.OrderTemplate, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][NavigateForSalesOrder][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:NavigateForSalesOrder]");
        }
        public void GetNextLastStopFromToday()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:GetNextLastStopFromToday]\t" + DateTime.Now + "");
            try
            {
                Dictionary<string, string> nextLastStopInfo = null;
                nextLastStopInfo = serviceRouteM.GetVisiteeStopInfo(Customer, TodayDate, true, true);

                CustomerNextStopFromToday = (nextLastStopInfo["NextStop"] == "None") ? new DateTime() : DateTime.Parse(nextLastStopInfo["NextStop"], CultureInfo.CreateSpecificCulture("en-US"));
                CustomerLastStopFromToday = (nextLastStopInfo["LastStop"] == "None") ? new DateTime() : DateTime.Parse(nextLastStopInfo["LastStop"], CultureInfo.CreateSpecificCulture("en-US"));
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerDashboard CustomerDashboard(" + SelectedDate.ToString() + ") error: " + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:GetNextLastStopFromToday]\t" + (DateTime.Now) + "");
        }
        private static void ReinitializeOrderPayload()
        {
            Entities.Customer SelectedCustomer = PayloadManager.OrderPayload.Customer;

            DateTime SelectedCalendarDate = PayloadManager.OrderPayload.StopDate;

            PayloadManager.OrderPayload = null;
            PayloadManager.PaymentPayload = null;

            PayloadManager.OrderPayload.Customer = SelectedCustomer;
            PayloadManager.OrderPayload.StopDate = SelectedCalendarDate;
            PayloadManager.OrderPayload.StopID = SelectedCustomer.StopID;
            PayloadManager.OrderPayload.RouteID = PayloadManager.ApplicationPayload.Route;

            PayloadManager.ReturnOrderPayload.StopDate = SelectedCalendarDate;
            PayloadManager.ReturnOrderPayload.StopID = SelectedCustomer.StopID;
            PayloadManager.ReturnOrderPayload.Customer = SelectedCustomer;

            PayloadManager.PaymentPayload.Customer = SelectedCustomer;
            PayloadManager.PaymentPayload.RouteID = PayloadManager.ApplicationPayload.Route;

            Order.OrderId = 0;

        }
        internal void ShowStopConfirmation()
        {
            if (!Customer.StopDate.HasValue)
            {
                IsPopUpMoveButtonEnabled = false;
                bool flag = GetCreateMoveStopPopUp();
                if (flag) IsActionInitiated = true;
                return;
            }
            if (Customer.StopDate.Value.Date > TodayDate.Date)
            {

                int stopCountTemp = 0;
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict = serviceRouteM.GetVisiteeStopInfo(Customer, TodayDate, true, true);

                stopCountTemp = new CustomerDashboardManager().GetStopCountOfCustomer(Customer.DeliveryCode.Trim());

                if ((stopCountTemp == 0 && Customer.NextStop.ToLower() == "none") || string.IsNullOrEmpty(Customer.NextStop))
                {
                    IsPopUpMoveButtonEnabled = false;
                }
                if (!string.IsNullOrEmpty(dict["NextStop"]) || !(dict["NextStop"].ToLower() == "none"))
                    if ((dict["NextStop"].StringToUSDateFormat().Date == Customer.StopDate.Value.Date) && !(Customer.NextStop.ToLower() == "none") && !string.IsNullOrEmpty(Customer.NextStop))
                    {
                        IsPopUpMoveButtonEnabled = true;
                    }
                if (stopCountTemp == 0 && Customer.StopDate.HasValue)
                {
                    IsPopUpMoveButtonEnabled = true;
                }
                var dialog = new Helpers.DialogWindow { TemplateKey = "ConfirmMoveorCreateDialog", Title = "Create or Move Stop", Payload = null };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);

            }
            else if (Customer.StopDate.Value.Date < TodayDate.Date)
            {
                if (todaysStops.Any(x => (x.CustomerNo == Customer.CustomerNo) && (x.StopType.ToLower() != "moved")))
                {
                    var alartMessage = new Helpers.AlertWindow { Message = Helpers.Constants.PreTripInspection.AlertMessageForFutureStop, MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alartMessage, MessageToken);
                }
                else
                {
                    if (DateTime.Compare(Customer.NextStop.StringToUSDateFormat().Date, TodayDate.Date) == 0)
                    {
                        IsPopUpMoveButtonEnabled = false;
                    }
                    var dialog = new Helpers.DialogWindow { TemplateKey = "ConfirmMoveorCreateDialogForPast", Title = "Move Stop or No-Sale", Payload = null };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                }
            }
        }
        private void ToggleButtons()
        {
            CustomerDashboardManager dashBoardM = new CustomerDashboardManager();
            StopCount = dashBoardM.GetStopCountOfCustomer(Customer.DeliveryCode.Trim());
            dashBoardM = null;   //Disposing Objects --Vignesh D

            //Unneccessary if Condition ---Vignesh D
            //if (!(string.IsNullOrEmpty(Customer.StopType)))
            //{
            //    if ((Customer.StopType.ToLower() == "moved"))
            //    {
            //        //IsReturnsCreditsEnabled = false;
            //        //IsPaymentAREnabled = false;
            //        //IsSalesOrderEnabled = false;
            //    }
            //}
            if (StopCount == 0)
            {
                IsPreorderEnabled = false;
            }
            if (Customer.CreditHold)
            {
                IsSalesOrderEnabled = false;
            }

            ToggleButtonForPendingRequest();
        }

        public void ToggleButtonForPendingRequest()
        {
            CycleCountManager CCManager = new CycleCountManager();
            var statusDict = CCManager.GetPendingCycleCountStatus();
            CCManager = null;   //Disposing the Objects --Vignesh D
            if (statusDict != null && statusDict.Count() > 0)
                if (statusDict["FullLock"])
                {
                    IsSalesOrderEnabled = false;
                    IsReturnsCreditsEnabled = false;
                }
        }
        private void MoveStopViewModelProperty_StopMoved(object sender, StopActionCompletedEventArgs e)
        {
            IsUnplannedStopCreatedForToday = false;

            //  DateTime handerNextDate, handlerLastDate;   // handlerLastDate  is Unused --Vignesh D
            DateTime handerNextDate;
            todaysStops = ServiceRoute.TodaysStops;
            Dictionary<string, string> nextLastStopInfo = null;
            nextLastStopInfo = serviceRouteM.GetVisiteeStopInfo(Customer, TodayDate, true, true);

            handerNextDate = (nextLastStopInfo["NextStop"] == "None") ? new DateTime() : DateTime.Parse(nextLastStopInfo["NextStop"], CultureInfo.CreateSpecificCulture("en-US"));
            //     handlerLastDate = (nextLastStopInfo["LastStop"] == "None") ? new DateTime() : DateTime.Parse(nextLastStopInfo["LastStop"], CultureInfo.CreateSpecificCulture("en-US"));   //Unused --Vignesh D

            if (IsAllCustomerTab)
            {
                if (DateTime.Today.Date == e.DateSelected.Value.Date)
                {
                    IsStopMovedToOriginalDate = true;
                    Customer = GetNewCustomer(DateTime.Today.Date);
                    PayloadManager.OrderPayload.Customer = Customer;
                }
                else
                {
                    Customer cust = GetNewCustomer(handerNextDate);
                    cust.IsTodaysStop = false;
                    PayloadManager.OrderPayload.Customer = cust;
                }

                if (e.visiteeChanged.IsTodaysStop || e.DateSelected.Value.Date == TodayDate.Date)
                    ServiceRoute.TodaysStops = serviceRouteM.GetCustomersForRoute(CommonNavInfo.RouteUser, TodayDate.Date);
            }
            else
            {
                Customer cust = GetNewCustomer(e.DateSelected.Value.Date);
                PayloadManager.OrderPayload.Customer = cust;
            }
            PayloadManager.OrderPayload.Customer.IsFromListingTab = IsAllCustomerTab;
            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, ShowAsModal = false, Refresh = true };
            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
        }
        private void CreateStopViewModelProperty_StopCreated(object sender, StopActionCompletedEventArgs e)
        {
            //  DateTime handerNextDate, handlerLastDate;   // handlerLastDate is Unused --Vignesh D
            DateTime handerNextDate;
            todaysStops = ServiceRoute.TodaysStops = serviceRouteM.GetCustomersForRoute(CommonNavInfo.RouteUser, TodayDate.Date);
            Dictionary<string, string> nextLastStopInfo = null;
            nextLastStopInfo = serviceRouteM.GetVisiteeStopInfo(Customer, TodayDate, true, true);

            handerNextDate = (nextLastStopInfo["NextStop"] == "None") ? new DateTime() : DateTime.Parse(nextLastStopInfo["NextStop"], CultureInfo.CreateSpecificCulture("en-US"));
            //     handlerLastDate = (nextLastStopInfo["LastStop"] == "None") ? new DateTime() : DateTime.Parse(nextLastStopInfo["LastStop"], CultureInfo.CreateSpecificCulture("en-US"));   //Unused --Vignesh D

            if (!IsAllCustomerTab)
            {
                //DateTime date = e.DateSelected.Value.Date == TodayDate.Date?TodayDate.Date:Customer.StopDate.Value.Date;
                Domain.Customer customer = GetNewCustomer(e.visiteeChanged.StopDate.Value.Date);
                PayloadManager.OrderPayload.Customer = customer;
                PayloadManager.OrderPayload.Customer.IsFromListingTab = IsAllCustomerTab;
            }
            else
            {
                if (DateTime.Compare(e.DateSelected.Value.Date, DateTime.Today.Date) == 0)
                {
                    IsUnplannedStopCreatedForToday = true;
                    Customer cust = GetNewCustomer(DateTime.Today.Date);
                    PayloadManager.OrderPayload.Customer = cust;
                }
                else if (Customer.IsTodaysStop)
                {
                    IsUnplannedStopCreatedForToday = false;
                    Customer cust = GetNewCustomer(DateTime.Today.Date);
                    PayloadManager.OrderPayload.Customer = cust;
                }
                else
                {
                    IsUnplannedStopCreatedForToday = false;
                    Customer cust = GetNewCustomer(handerNextDate);
                    PayloadManager.OrderPayload.Customer = cust;
                }
                PayloadManager.OrderPayload.Customer.IsFromListingTab = IsAllCustomerTab;
            }
            //PayloadManager.OrderPayload.Customer = "Moved";
            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, ShowAsModal = false, Refresh = true };
            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);

            //if (IsUnplannedStopCreatedForToday)
            //{
            //    Customer.StopDate = TodayDate;
            //    Customer.StopType = "Unplanned";
            //    Customer.IsTodaysStop = true;
            //}
        }
        void LoadCustomerDashboard()
        {
            try
            {
                todaysStops = ServiceRoute.TodaysStops = serviceRouteM.GetCustomersForRoute(CommonNavInfo.RouteUser, TodayDate.Date);
                IsAllCustomerTab = Customer.IsFromListingTab;

                if (!IsAllCustomerTab)
                {
                    IsCreditHold = Customer.CreditHold;
                    //Domain.Customer customer = GetNewCustomer(Customer.StopDate.Value);
                    GetStopInfoFromDailyStop(Customer);
                }
                #region Customer Tab
                else
                {
                    DateTime tempNextDate = new DateTime();
                    IsCreditHold = Customer.CreditHold;
                    Dictionary<string, string> nextLastStopInfo = null;
                    nextLastStopInfo = serviceRouteM.GetVisiteeStopInfo(Customer, TodayDate, true, true);

                    if (!(nextLastStopInfo["LastStop"] == "None"))
                    {
                        NextDate = (nextLastStopInfo["NextStop"] == "None") ? new DateTime() : DateTime.Parse(nextLastStopInfo["NextStop"], CultureInfo.CreateSpecificCulture("en-US"));
                        LastDate = (nextLastStopInfo["LastStop"] == "None") ? new DateTime() : DateTime.Parse(nextLastStopInfo["LastStop"], CultureInfo.CreateSpecificCulture("en-US"));
                    }
                    else
                    {
                        if (nextLastStopInfo["NextStop"] == "None")
                            NextDate = null;
                        else
                            NextDate = DateTime.Parse(nextLastStopInfo["NextStop"], CultureInfo.CreateSpecificCulture("en-US"));
                    }
                    nextLastStopInfo = null;   // Disposing Objects for Memory optimization  --Vignesh D
                    if (todaysStops.Any(item => (item.CustomerNo == Customer.CustomerNo) && item.StopType.ToLower() != "moved"))
                    {
                        //nextDate = todaysStops.FirstOrDefault(item => (item.CustomerNo == Customer.CustomerNo) && Customer.StopType.ToLower() != "moved").StopDate.Value.Date;
                        Customer = GetNewCustomer(TodayDate);
                    }

                    CustomerDashboardManager dashBoardM = new CustomerDashboardManager();

                    StopCount = dashBoardM.GetStopCountOfCustomer(Customer.DeliveryCode.Trim());
                    dashBoardM = null;   // Disposing Objects for Memory optimization  --Vignesh D
                    if (StopCount == 0)
                    {
                        if (nextDate != null && nextDate.Value.Date != Convert.ToDateTime("01/01/0001").Date)  //Vivensas to check the empty default date
                            Customer = GetNewCustomer(NextDate.Value);
                    }
                    if (Customer.NextStop.ToLower() == "none")
                    {
                        if (StopCount == 0 && !IsStopUnPlanned)
                        {
                            IsCustomerSeasonal = true;
                            IsTodayDate = IsNextStopDate = IsOriginalDate = IsRescheduleDate = false;
                            StopInfoSequence = IsMoveButton = IsNoSale = false;
                        }
                    }
                    if (!(Customer.NextStop.ToLower() == "none"))
                    {
                        try
                        {
                            //  if (Customer.NextStop != string.Empty)   //CA1820: Test for empty strings using string length --Vignesh D
                            if (!string.IsNullOrEmpty(Customer.NextStop))    // Added by Vignesh D
                                tempNextDate = Customer.NextStop.StringToUSDateFormat();
                            else
                                tempNextDate = string.IsNullOrEmpty(Customer.NextStop) ? new DateTime() : Convert.ToDateTime(Customer.NextStop);
                        }
                        catch
                        {
                            tempNextDate = string.IsNullOrEmpty(Customer.NextStop) ? new DateTime() : Convert.ToDateTime(Customer.NextStop);
                        }

                        if (NextDate.HasValue)
                            tempNextDate = NextDate.Value;

                        if (Customer.SequenceNo == -1 || tempNextDate > DateTime.Now && !(Customer.SaleStatus.ToLower() == "nosale") && !Customer.IsTodaysStop)
                        {
                            if (!IsStopMovedToOriginalDate)
                            {
                                Domain.Customer cust = GetNewCustomer(tempNextDate);
                                // OBJECT INITIALIZE CONSIDERING THAT SELECTED CUSTOMER IS NOT IN TODAYS AND IN SELECTED DATE LIST
                                if (cust != null)
                                {
                                    Customer = cust;
                                    if (cust.StopType.ToLower() == "rescheduled")
                                    {
                                        Customer.RescheduledDate = cust.StopDate.Value;
                                    }
                                }
                                GetCustomerStopInfo(Customer);
                            }
                        }
                        else
                        {

                            using (ServiceRouteManager serviceRouteManager = new ServiceRouteManager())
                            {
                                Domain.Customer cust = GetNewSequenceOnActivity();
                                if (cust != null)
                                {
                                    Dictionary<string, string> dict = serviceRouteManager.GetVisiteeStopInfo(Customer, todayDate, true, true);
                                    Customer.NextStop = dict["NextStop"];
                                    Customer.PreviousStop = dict["LastStop"];
                                    Customer.TempSequenceNo = cust.TempSequenceNo;
                                }
                            }
                            GetCustomerStopInfo(Customer);
                        }

                        if (StopCount == 0 && !LastDate.HasValue)
                        {
                            if (Customer.IsTodaysStop)
                            {
                                GetStopInfoForTodayStop(Customer);
                            }
                            else
                            {
                                Customer = GetNewCustomer(tempNextDate);
                                GetCustomerNextStopInfo(Customer);
                            }
                        }
                        //else
                        //{
                        //Customer = GetNewCustomer(LastDate.Value.Date);
                        // SelectedDate = Customer.StopDate.Value;
                        // GetStopInfoForPastStop(Customer);
                        //  }
                        SetPendingActivity();

                    }

                }

                if (Customer.StopDate.HasValue)
                {
                    int preOrderCount = customerDashboardM.GetPreOrderInformationForCustomer(Customer.CustomerNo, Customer.StopDate.Value);    
                    if (preOrderCount > 0 && !Customer.HasActivity)
                    {
                        IsPreOrder = true;
                    }
                }
                #endregion

                Managers.ContactManager contactManager = new Managers.ContactManager();
                ObservableCollection<Entities.CustomerContact> contacts = contactManager.GetCustomerContacts(Customer.CustomerNo);
                
                DefaultContact = contacts.FirstOrDefault(contactItem => contactItem.ShowOnDashboard == true);
                contactManager = null;  //Object Dispose for Memory Optimization --Vignesh D
                contacts = null; //Object Dispose for Memory Optimization --Vignesh D

                Managers.CustomerNotesManager notesManager = new Managers.CustomerNotesManager();
                ObservableCollection<Entities.CustomerNote> notes = notesManager.GetCustomerNotes(Customer.CustomerNo);                
                DefaultNote = notes.FirstOrDefault(noteItem => noteItem.IsDefault == true);
                notesManager = null;   //Object Dispose for Memory Optimization --Vignesh D
                notes = null;//Object Dispose for Memory Optimization --Vignesh D

                Managers.CustomerDashboardManager dashboardManager = new Managers.CustomerDashboardManager();
                string payDetails = dashboardManager.GetPaymentDetails(Customer.CustomerNo);

                dashboardManager = null;   //Object Disposing for Memory Optimization --Vignesh D



                PaymentDetailsCode = payDetails.Split('-')[0];
                PaymentDetailsDescription = payDetails.Split('-')[1];
                payDetails = string.Empty;  
                GetNoSaleReason();
                GetARSummaryAgeing();
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerDashboard CustomerDashboard([LoadCustomerDashboard]" + SelectedDate.ToString() + ") error: " + ex.Message);
            }
        }

        private void GetARSummaryAgeing()
        {
            Managers.ARPaymentManager objARPayManager = new Managers.ARPaymentManager(CommonNavInfo.Customer.CustomerNo, CommonNavInfo.Customer.Company);
            List<Dictionary<string, decimal>> AgeingSummary = objARPayManager.GetAgeingSummary(null);

            foreach (Dictionary<string, decimal> item in AgeingSummary)
            {
                if (item.ContainsKey("AR30Days"))
                {
                    AR30Days = item["AR30Days"];
                }
                else if (item.ContainsKey("AR60Days"))
                {
                    AR60Days = item["AR60Days"];
                }
                else if (item.ContainsKey("AR90Days"))
                {
                    AR90Days = item["AR90Days"];
                }
                else if (item.ContainsKey("AR90MoreDays"))
                {
                    AR90MoreDays = item["AR90MoreDays"];
                }

            }

            UnappliedCash = (-1) * objARPayManager.GetUnAppliedAmtForCustomer();

            objARPayManager = null;  //Disposing objects for Memory optimization --Vignesh D
        }

        void RequestAuthCodeVM_AuthCodeChanged(object sender, RequestAuthCodeViewModel.AuthCodeChangedEventArgs e)
        {
            CheckForAcceptForChargeOnAccount();
        }
        private void CheckForAcceptForChargeOnAccount()
        {
            CanAcceptChargeOnAccount = SelectedReasonCode != null && RequestAuthCodeVM.IsAuthCodeValid;
        }

        public Customer GetNewCustomer(DateTime tempNextDate, Domain.Customer Customer)
        {
            Domain.Customer cust = new Domain.Customer();
            try
            {
                ObservableCollection<Domain.Customer> tempCollection = new ObservableCollection<Entities.Customer>();
                ServiceRouteManager serviceRouteManager = new ServiceRouteManager();
                tempCollection = serviceRouteManager.GetCustomersForRoute(CommonNavInfo.RouteUser, tempNextDate);
                cust = tempCollection.FirstOrDefault(c => (c.CustomerNo == Customer.CustomerNo) && c.StopType.ToLower() != "moved");
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerDashboard CustomerDashboard([GetNewCustomer]" + SelectedDate.ToString() + ") error: " + ex.Message);
            }

            return cust;
        }
        public Customer GetNewCustomer(DateTime tempNextDate)
        {
            Domain.Customer cust = new Domain.Customer();
            try
            {
                ObservableCollection<Domain.Customer> tempCollection = new ObservableCollection<Entities.Customer>();
                ServiceRouteManager serviceRouteManager = new ServiceRouteManager();
                tempCollection = serviceRouteManager.GetCustomersForRoute(CommonNavInfo.RouteUser, tempNextDate);
                cust = tempCollection.FirstOrDefault(c => (c.CustomerNo == Customer.CustomerNo) && c.StopType.ToLower() != "moved");


                tempCollection = null;           // Disposing Objects for Memory optimization --Vignesh D
                serviceRouteManager = null;

            }
            catch (Exception ex)
            {
                Logger.Error("CustomerDashboard CustomerDashboard([GetNewCustomer]" + SelectedDate.ToString() + ") error: " + ex.Message);
            }

            return cust;
        }
        public Customer GetNewSequenceOnActivity()
        {
            Domain.Customer cust = new Entities.Customer();
            //Get temp sequence no for customer start
            try
            {
                //Get temp seq no fro stop
                //string queryCustForStopDate = "SELECT RPAN8 AS 'CustomerNo', RPSTTP as 'StopType'  FROM BUSDTA.M56M0004 WHERE RPSTDT='" + SelectedDate.Date.ToString("yyyy-MM-dd") + "' ORDER BY RPSN";
                string queryCustForStopDate = "Call BUSDTA.SP_GetNewSequenceOnActivity(@SelectedDT ='" + SelectedDate.Date.ToString("yyyy-MM-dd") + "')";
                DataSet ds = DbEngine.ExecuteDataSet(queryCustForStopDate);
                DataTable dtCustForStopDate = ds.HasData() ? ds.Tables[0] : new DataTable();
                List<Customer> l = dtCustForStopDate.GetEntityList<Customer>();
                int temp1 = 1;
                foreach (Customer tempCust in l)
                {
                    if (tempCust.StopType.Trim().ToLower() == "planned")
                    {
                        tempCust.TempSequenceNo = temp1;
                        temp1++;
                    }
                    else
                    {
                        tempCust.TempSequenceNo = 0;
                    }
                }
                int tempSeqNo = l.FirstOrDefault(x => x.CustomerNo.Trim() == Customer.CustomerNo.Trim()).TempSequenceNo == null ? 0 : l.FirstOrDefault(x => x.CustomerNo.Trim() == Customer.CustomerNo.Trim()).TempSequenceNo;
                cust.TempSequenceNo = tempSeqNo;
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerDashboard CustomerDashboard([GetNewSequenceOnActivity]" + SelectedDate.ToString() + ") error: " + ex.Message);
            }
            return cust;
        }
        private void LoadPreOrderStops(DateTime date)
        {
            try
            {
                //   DateTime selectedDate = Convert.ToDateTime(date);   //Unused --VIgnesh D
                PreOrderManager manager = new PreOrderManager();
                DateTime baseDate = new DateTime(2014, 12, 29);
                NextStopDates = manager.GetFutureStopDates(Customer.CustomerNo, TodayDate.Date);
                manager.Dispose();
                manager = null;
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerDashboard CustomerDashboard([LoadPreOrderStops]" + SelectedDate.ToString() + ") error: " + ex.Message);
            }
                     
        }
        public void SetDefaultContact(Entities.CustomerContact contact)
        {
            DefaultContact = contact;
            //CommonNavInfo.Customer.Phone = "(" + contact.DefaultPhone.AreaCode.Trim() + ") " + contact.DefaultPhone.Value.Trim() + " " + contact.DefaultPhone.Extension.Trim() ;
        }
        public void SetDefaultNote(Entities.CustomerNote note)
        {
            DefaultNote = note;
        }
        public void GetCustomerStopInfo(Entities.Customer pCustomer)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:GetCustomerStopInfo]");
            try
            {
                // Dictionary<string, string> nextLastStopInfo = null;

                this.Customer = pCustomer;
                TodayDate = DateTime.Now.Date;

                Managers.CustomerDashboardManager customerDashboardM = new CustomerDashboardManager();
                StopSequenceNo = Customer.TempSequenceNo;

                SetPendingActivity();
                try
                {
                    if (Customer.StopDate.HasValue)
                    {
                        if (DateTime.Compare(Customer.StopDate.Value.Date, TodayDate.Date) >= 0)
                        {
                            int preOrderCount = customerDashboardM.GetPreOrderInformationForCustomer(Customer.CustomerNo, Customer.StopDate.Value);
                            customerDashboardM = null;    //Disposing Objects for Memory optimization --Vignesh D
                            if (preOrderCount > 0 && !Customer.HasActivity)
                            {
                                IsPreOrder = true;
                            }
                        }
                    }
                    ActivityPerformed = Customer.HasActivity;
                    if (Customer.SaleStatus == "NOSALE" && !ActivityPerformed)
                        SaleStatus = true;
                    if (!string.IsNullOrEmpty(Customer.OriginalDate.ToString()))
                    {
                        OriginalDate = Customer.OriginalDate;
                    }
                    else OriginalDate = new DateTime();
                    if (!string.IsNullOrEmpty(Customer.RescheduledDate.ToString()))
                    {
                        RescheduledDate = Customer.RescheduledDate;
                    }
                    else RescheduledDate = new DateTime();
                    if (Customer.StopType != null)
                    {
                        IsStopRescheduled = Customer.StopType.ToLower() == "rescheduled" ? true : false;
                        IsStopMoved = Customer.StopType.ToLower() == "moved" ? true : false;
                        IsStopPlanned = Customer.StopType.ToLower() == "planned" ? true : false;
                        IsStopUnPlanned = Customer.StopType.ToLower() == "unplanned" ? true : false;
                    }
                    IsTodaysStop = Customer.IsTodaysStop;

                    // CUSTOMER TAB STOP NOT IN DAILY STOP N SELECTED date LIST
                    if (Customer.SequenceNo == -1 && (DateTime.Compare(SelectedDate.Date, TodayDate.Date) == 0))
                    {
                        StopInfoSequence = IsNextStopDate = IsMoveButton = true;
                        StopInfoIcon = Customer.TempSequenceNo.ToString();
                        NextDate = DateTime.Parse(Customer.NextStop, CultureInfo.CreateSpecificCulture("en-US"));
                        IsTodayDate = IsOriginalDate = IsRescheduleDate = IsNoSale = false;
                    }

                    #region Seasonal customers condition
                    if (StopCount == 0 && IsStopUnPlanned)
                    {
                        if (IsTodaysStop)
                        {
                            Domain.Customer cust = GetNewCustomer(TodayDate);
                            GetStopInfoForTodayStop(cust);
                            //IsTodayDate = IsMoveButton = IsNoSale = true;
                            //IsNextStopDate = false;
                        }
                        else
                        {
                            //   if (Customer.NextStop == null || Customer.NextStop == "" || Customer.NextStop.ToLower().Trim() == "none")   //CA1820: Test for empty strings using string length
                            if (string.IsNullOrEmpty(Customer.NextStop) || Customer.NextStop.ToLower().Trim() == "none")      //Added by Vignesh D
                            {
                                NextDate = Customer.StopDate;
                            }
                            //else
                            //{
                            //    NextDate = Customer.NextStop.StringToUSDateFormat();
                            //}
                            IsMoveButton = IsNextStopDate = true;
                            IsNoSale = IsTodayDate = false;

                        }
                        StopInfoSequence = IsOriginalDate = IsRescheduleDate = false;
                    }
                    #endregion

                    #region stop is today
                    if (IsTodaysStop)
                    {
                        // Domain.Customer cust = GetNewCustomer(TodayDate);
                        GetStopInfoForTodayStop(Customer);
                    }
                    #endregion

                    // RESCHEDULED STOP iS MOVED TO ORIGINAL DATE
                    if (Customer.StopDate.HasValue)
                        if (DateTime.Compare(Customer.StopDate.Value.Date, TodayDate.Date) > 0)
                        {
                            if (IsTodaysStop)
                            {
                                Domain.Customer cust = GetNewCustomer(TodayDate);
                                GetStopInfoForTodayStop(cust);
                            }
                            else if (NextDate.HasValue)
                            {
                                Domain.Customer cust = GetNewCustomer(NextDate.Value);
                                GetCustomerNextStopInfo(cust);
                            }
                        }

                    // stop unplanned 
                    #region Unplanned Stops
                    if (IsStopUnPlanned && StopCount != 0)
                    {
                        if ((DateTime.Compare(SelectedDate.Date, TodayDate.Date) < 0))
                        {
                            if (!SaleStatus)
                            {
                                if (IsUnplannedStopCreatedForToday || IsTodaysStop)
                                {
                                    Domain.Customer cust = GetNewCustomer(TodayDate);
                                    GetStopInfoForTodayStop(cust);
                                }
                                else if (NextDate.HasValue)
                                {
                                    Domain.Customer cust = GetNewCustomer(NextDate.Value);
                                    GetCustomerNextStopInfo(cust);
                                }
                            }
                        }
                        else if ((DateTime.Compare(SelectedDate.Date, TodayDate.Date) == 0))
                        {
                            if (IsUnplannedStopCreatedForToday || IsTodaysStop)
                            {
                                Domain.Customer cust = GetNewCustomer(TodayDate);
                                GetStopInfoForTodayStop(cust);
                            }
                            else if (NextDate.HasValue)
                            {
                                Domain.Customer cust = GetNewCustomer(NextDate.Value);
                                GetCustomerNextStopInfo(cust);
                            }
                        }
                        else if (DateTime.Compare(SelectedDate.Date, TodayDate.Date) > 0)
                        {
                            if (IsUnplannedStopCreatedForToday || IsTodaysStop)
                            {
                                Domain.Customer cust = GetNewCustomer(TodayDate);
                                GetStopInfoForTodayStop(cust);
                            }
                            else if (NextDate.HasValue)
                            {
                                Domain.Customer cust = GetNewCustomer(NextDate.Value);
                                GetCustomerNextStopInfo(cust);
                            }
                        }
                    }
                    #endregion

                }
                catch (Exception ex)
                {
                    Logger.Error("CustomerDashboard CustomerDashboard([GetCustomerStopInfo]" + SelectedDate.ToString() + ") error: " + ex.Message);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerDashboard CustomerDashboard([GetCustomerStopInfo]" + SelectedDate.ToString() + ") error: " + ex.Message);
            }
        }
        private void SetPendingActivity()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:SetPendingActivity]");
            try
            {
                Dictionary<string, int> activityCount = new Dictionary<string, int>();
                activityCount = serviceRouteM.GetActivityCount(Customer.CustomerNo, Customer.StopID, TodayDate);
                PendingActivity = activityCount["PendingActivity"];
                activityCount = null;
                if (PendingActivity > 0)
                {
                    IsPendingActivity = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][SetPendingActivity][customerID =" + Customer.CustomerNo + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:SetPendingActivity]");
        }
        public void GetStopInfoFromDailyStop(Customer customer)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:GetStopInfoFromDailyStop]");
            try
            {
                SetPendingActivity();

                if (customer.RescheduledDate != null)
                    customer = Customer = customer.StopType.ToLower() == "moved" ? GetNewCustomer(customer.RescheduledDate.Value) : customer;


                if (DateTime.Compare(customer.StopDate.Value.Date, TodayDate.Date) < 0)
                {
                    if (customer.HasActivity || customer.SaleStatus.ToLower() == "nosale")
                    {
                        IsMoveButton = IsNoSale = false;
                        if (customer.SaleStatus.ToLower() == "nosale")
                            SaleStatus = true;
                        if (customer.HasActivity)
                        {
                            ActivityPerformed = true;
                        }
                    }
                    else
                    {
                        IsMoveButton = IsNoSale = true;
                    }
                }
                else if (DateTime.Compare(customer.StopDate.Value.Date, TodayDate.Date) == 0)
                {
                    GetStopInfoForTodayStop(customer);
                }
                else if (DateTime.Compare(customer.StopDate.Value.Date, TodayDate.Date) > 0)
                {
                    IsMoveButton = true; IsNoSale = false;
                }
                string type = customer.StopType;
                switch (type.ToLower())
                {
                    case "planned":
                        StopInfoSequence = true;
                        StopInfoIcon = customer.TempSequenceNo.ToString();
                        IsNextStopDate = true;
                        NextDate = customer.StopDate;
                        IsRescheduleDate = IsOriginalDate = IsTodayDate = false;
                        IsStopRescheduled = IsStopUnPlanned = false;
                        break;
                    case "unplanned":
                        IsStopUnPlanned = true;
                        IsNextStopDate = true;
                        NextDate = customer.StopDate;
                        IsRescheduleDate = IsOriginalDate = IsTodayDate = false;
                        IsStopRescheduled = StopInfoSequence = false;
                        break;
                    case "moved":
                        IsStopRescheduled = true;
                        StopInfoSequence = IsStopUnPlanned = false;
                        IsOriginalDate = IsRescheduleDate = true;
                        IsTodayDate = IsNextStopDate = false;
                        OriginalDate = customer.StopDate;
                        RescheduledDate = customer.RescheduledDate;
                        if (DateTime.Compare(RescheduledDate.Value.Date, TodayDate.Date) > 0)
                        {
                            IsNoSale = false;
                        }
                        else if (DateTime.Compare(RescheduledDate.Value.Date, TodayDate.Date) <= 0)
                        {
                            IsNoSale = true;
                        }
                        break;
                    case "rescheduled":
                        IsStopRescheduled = true;
                        IsOriginalDate = IsRescheduleDate = true;
                        IsTodayDate = IsNextStopDate = false;
                        IsStopUnPlanned = StopInfoSequence = false;
                        OriginalDate = customer.OriginalDate;
                        RescheduledDate = customer.StopDate; //SelectedDate; 
                        if (DateTime.Compare(RescheduledDate.Value.Date, TodayDate.Date) > 0)
                        {
                            IsNoSale = false;
                        }
                        else if ((DateTime.Compare(RescheduledDate.Value.Date, TodayDate.Date) <= 0) && !customer.HasActivity && customer.SaleStatus.ToLower() != "nosale")
                        {
                            IsNoSale = true;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][GetStopInfoFromDailyStop][customerID =" + Customer.CustomerNo + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:GetStopInfoFromDailyStop]");
        }
        public void GetCustomerNextStopInfo(Customer customer)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:GetCustomerNextStopInfo]");
            try
            {
                if (customer.IsTodaysStop)
                {
                    GetStopInfoForTodayStop(customer);
                }
                else if (DateTime.Compare(NextDate.Value.Date, TodayDate.Date) > 0)
                {
                    Domain.Customer cust = GetNewCustomer(NextDate.Value);

                    //String Handling for Memory Optimization --Vignesh D
                    string stopType = string.Empty;
                    stopType = cust.StopType;
                    // if (cust.StopType.ToLower() == "unplanned")
                    Customer = cust;
                    SaleStatus = false;
                    IsMoveButton = true; IsNoSale = false;
                    if (stopType.ToLower() == "unplanned")
                    {
                        IsStopUnPlanned = IsNextStopDate = true;
                        StopInfoSequence = IsTodayDate = IsOriginalDate = IsRescheduleDate = false;
                    }
                    else if (stopType.ToLower() == "planned")
                    {
                        StopInfoSequence = IsNextStopDate = true;
                        StopInfoIcon = cust.TempSequenceNo.ToString();
                        IsStopRescheduled = IsStopUnPlanned = false;
                        IsOriginalDate = IsRescheduleDate = IsTodayDate = false;
                    }
                    else if (cust.StopType.ToLower() == "rescheduled")
                    {
                        IsOriginalDate = IsRescheduleDate = true;
                        RescheduledDate = cust.StopDate;
                        OriginalDate = cust.OriginalDate;
                        IsStopRescheduled = true;
                        IsStopUnPlanned = IsTodayDate = StopInfoSequence = IsNextStopDate = false;
                        Customer.RescheduledDate = cust.StopDate;
                    }

                    else if (stopType.ToLower() == "moved")
                    {
                        if (cust.RescheduledDate.HasValue)
                        {
                            if (DateTime.Compare(cust.RescheduledDate.Value.Date, customer.RescheduledDate.Value.Date) < 0)
                            {
                                StopInfoSequence = IsTodayDate = false;
                                IsOriginalDate = IsRescheduleDate = true;
                                OriginalDate = TodayDate;
                                IsStopRescheduled = true;
                                IsNoSale = false;
                            }
                            else
                            {
                                IsNextStopDate = true;
                                IsOriginalDate = IsRescheduleDate = IsTodayDate = false;
                                StopInfoSequence = true;
                                StopInfoIcon = Customer.StopDate.Value.ToString();
                            }
                        }
                    }
                    PayloadManager.OrderPayload.Customer = Customer;

                    cust = null;   //Disposing Objects --Vignesh D
                    stopType = string.Empty;   
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][GetCustomerNextStopInfo][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:GetCustomerNextStopInfo]");
        }
        public void GetStopInfoForTodayStop(Customer customer)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:GetStopInfoForTodayStop]");
            try
            {
               
                if (customer != null)
                {
                    // String Handling - For Memroy Optimization  - By Vignesh D
                    string stopType = string.Empty;
                    stopType = customer.StopType;
                    ActivityPerformed = customer.HasActivity;
                    if (customer.HasActivity)
                    {
                        IsMoveButton = IsNoSale = false;
                    }
                    else if (customer.SaleStatus.ToLower() == "nosale")
                    {
                        Managers.CustomerDashboardManager customerDashboardM = new CustomerDashboardManager();
                        IsMoveButton = false; IsNoSale = true; SaleStatus = true;
                        SelectedReason = customerDashboardM.GetNoSaleReasonForNoActivity(customer.StopID, customer.CustomerNo);
                        customerDashboardM = null;   //Disposing Objects --Vignesh D
                        oriValueOfNoSale = SelectedReason;
                    }
                    else if (!customer.HasActivity && customer.SaleStatus.ToLower() != "nosale")
                    {
                        IsMoveButton = IsNoSale = true;
                    }
                    if (stopType.ToLower() == "unplanned")
                    {
                        IsStopUnPlanned = IsTodayDate = true;
                        StopInfoSequence = IsNextStopDate = IsOriginalDate = IsRescheduleDate = false;
                    }
                    else if (stopType.ToLower() == "planned")
                    {
                        StopInfoSequence = IsTodayDate = true;
                        StopInfoIcon = customer.TempSequenceNo.ToString();
                        IsStopRescheduled = IsStopUnPlanned = false;
                        IsOriginalDate = IsRescheduleDate = IsNextStopDate = false;
                    }
                    else if (customer.StopType.ToLower() == "rescheduled")
                    {
                        IsOriginalDate = IsRescheduleDate = true;
                        OriginalDate = customer.OriginalDate;
                        RescheduledDate = TodayDate;
                        IsStopRescheduled = true;
                        IsTodayDate = StopInfoSequence = false;
                    }

                    else if (stopType.ToLower() == "moved")
                    {
                        if (customer.RescheduledDate.HasValue)
                        {
                            if (DateTime.Compare(customer.RescheduledDate.Value.Date, SelectedDate.Date) < 0)
                            {
                                StopInfoSequence = IsTodayDate = false;
                                IsOriginalDate = IsRescheduleDate = true;
                                OriginalDate = TodayDate;
                                IsStopRescheduled = true;
                                IsNoSale = false;
                            }
                            else
                            {
                                IsNextStopDate = true;
                                IsOriginalDate = IsRescheduleDate = IsTodayDate = false;
                                StopInfoSequence = true;
                                StopInfoIcon = Customer.StopDate.Value.ToString();
                            }
                        }
                    }
                    stopType = string.Empty;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][GetStopInfoForTodayStop][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:GetStopInfoForTodayStop]");
        }
        public void GetStopInfoForPastStop(Customer customer)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][Start:GetStopInfoForPastStop]");
            try
            {
                string stopType = customer.StopType;
                if (stopType.ToLower() == "unplanned")
                {
                    IsMoveButton = IsStopUnPlanned = IsNextStopDate = true;
                    NextDate = customer.StopDate.Value;
                    IsTodayDate = StopInfoSequence = IsOriginalDate = IsRescheduleDate = false;
                    IsNoSale = true;
                }
                else if (stopType.ToLower() == "planned")
                {
                    IsMoveButton = StopInfoSequence = IsNextStopDate = true;
                    NextDate = customer.StopDate.Value;
                    StopInfoIcon = customer.TempSequenceNo.ToString();
                    IsTodayDate = IsStopRescheduled = IsStopUnPlanned = false;
                    IsOriginalDate = IsRescheduleDate = false;
                    IsNoSale = true;
                }
                else if (customer.StopType.ToLower() == "rescheduled")
                {
                    IsMoveButton = IsOriginalDate = IsRescheduleDate = true;
                    OriginalDate = customer.OriginalDate;
                    RescheduledDate = customer.StopDate.Value;
                    IsStopRescheduled = true;
                    IsTodayDate = StopInfoSequence = IsNextStopDate = IsStopUnPlanned = false;
                    IsNoSale = true;
                }
                else if (stopType.ToLower() == "moved")
                {
                    if (customer.RescheduledDate.HasValue)
                    {
                        if (DateTime.Compare(customer.RescheduledDate.Value.Date, Customer.NextStop.StringToUSDateFormat()) < 0)
                        {
                            IsOriginalDate = IsRescheduleDate = IsStopRescheduled = true;
                            RescheduledDate = customer.RescheduledDate;
                            OriginalDate = customer.StopDate.Value;
                            IsTodayDate = IsNextStopDate = stopInfoSequence = IsMoveButton = IsStopUnPlanned = false;
                        }
                        else
                        {
                            Domain.Customer cust = GetNewCustomer(Customer.NextStop.StringToUSDateFormat());
                            GetCustomerNextStopInfo(cust);
                        }
                    }
                }
                if (DateTime.Compare(DateTime.Today.Date, customer.NextStop.StringToUSDateFormat().Date) == 0)
                {
                    IsMoveButton = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerDashboard.][GetStopInfoForPastStop][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerDashboard][End:GetStopInfoForPastStop]");
        }
        public void GetNoSaleReason()
        {
            Managers.CustomerDashboardManager dashBoardManager = new CustomerDashboardManager();
            NoSaleReason = new Dictionary<int, string>();
            try
            {
                NoSaleReason = dashBoardManager.GetNoSaleReason();
                dashBoardManager = null;

            }
            catch (Exception ex)
            {
                Logger.Error("CustomerDashboard CustomerDashboard([GetNoSaleReason]" + SelectedDate.ToString() + ") error: " + ex.Message);
            }
        }

        public bool GetPreTripDetailsForDate()
        {

            bool flag = false;
            Managers.PreTripInspectionManager inspectionManager = new PreTripInspectionManager();
            Dictionary<int, string> dict=null;
            try
            {                
                 dict= inspectionManager.CheckVerificationSignature(CommonNavInfo.RouteID.ToString());
                //int verifySign = 0;
                //string status = string.Empty;
                //   int dayOfWeek = (int)DateTime.Now.DayOfWeek;   //Unused --Vignesh D
                if (dict.ContainsValue("pass") || dict.ContainsValue("fail"))
                    flag = true;
                else
                    flag = false;
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerDashboard CustomerDashboard([GetPreTripDetailsForDate]" + SelectedDate.ToString() + ") error: " + ex.Message);
            }
            finally
            {
                inspectionManager = null;
                dict = null;
            }
            return flag;
        }
        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                if (customerDashboardM != null)
                    customerDashboardM.Dispose();
                if (serviceRouteM != null)
                    serviceRouteM.Dispose();
                if (stopManager != null)
                    stopManager.Dispose();
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
