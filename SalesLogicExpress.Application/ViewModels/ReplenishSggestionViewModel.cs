﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using GalaSoft.MvvmLight.Messaging;
using Telerik.Windows.Controls;
using System.Collections.ObjectModel;
using log4net;


namespace SalesLogicExpress.Application.ViewModels
{
    public class ReplenishSggestionViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.ReplenishSggestionViewModel");

        #region Properties & Variables
        public Guid MessageToken { get; set; }
        
        #endregion

        #region OnPropertyChanged

        
        #endregion

        #region Commands Declaretion
        public DelegateCommand Save { get; set; }
        
        //  UpdateAvailableQty

        #endregion

        #region Command Defination

        //public void InitialiseCommands()
        //{

        //    //Save = new DelegateCommand((param) =>
        //    //{
        //    //    if (param != null)
        //    //    {
        //    //        //SaveExecute();
        //    //    }
        //    //});            
        //}
        
      
        #endregion

        #region Constructor & Methods
        public ReplenishSggestionViewModel()
        {
            //InitialiseCommands();
            //InitialiseVariables();
            //GetQtyUOMCollection();
        }
        //public void InitialiseVariables()
        //{
            
        //}
        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
