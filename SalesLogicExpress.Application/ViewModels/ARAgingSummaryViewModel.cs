﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ViewModelPayload;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Telerik.Windows.Controls;
using Models = SalesLogicExpress.Domain;

namespace SalesLogicExpress.Application.ViewModels
{
    public class ARAgingSummaryViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.ARAgingSummaryViewModel");

        #region Variable and object declaration

        private decimal _TotalAgingAmount = 0;
        public Dictionary<int, string> items = new Dictionary<int, string>();
        private ObservableCollection<Models.ARAgingSummary> _Items = null;
        private Models.ARAgingSummary _SelectedItem = new Models.ARAgingSummary();
        #endregion

        #region Properties
        //public Guid MessageToken { get; set; }

        public decimal TotalAgingAmount
        {
            get
            {
                if (_TotalAgingAmount == 0 && Items.Count > 0)
                {
                    this.TotalAgingAmount = (from o in Items.AsEnumerable()
                                             select o.OpenInvoiceOpenAmt).Sum();
                }
                return _TotalAgingAmount;
            }
            private set
            {
                _TotalAgingAmount = value;
                OnPropertyChanged("TotalAgingAmount");
            }
        }

        /// <summary>
        /// Command to handle customer ledger drill down option 
        /// </summary>
        public DelegateCommand AgingRowSelectedCommand { get; set; }

        public BaseViewModel ParentViewModel { get; set; }

        public ObservableCollection<Models.ARAgingSummary> Items
        {
            get
            {
                return _Items;
            }
            set
            {
                _Items = value;
                OnPropertyChanged("Items");
            }
        }

        /// <summary>
        /// Get or set the current selected item on grid
        /// </summary>
        public Models.ARAgingSummary SelectedItem
        {
            get
            {
                return _SelectedItem;
            }
            set
            {
                _SelectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }
        #endregion

        #region Constructor
        public ARAgingSummaryViewModel()
        {
            this.MessageToken = CommonNavInfo.MessageToken;
            ARAgingManager objARManager = new ARAgingManager();
            this.Items = objARManager.GetAgingDetails();
            objARManager.Dispose();
            objARManager = null;
            this.InitilizeCommand();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the command to handle row click
        /// </summary>
        private void InitilizeCommand()
        {
            this.AgingRowSelectedCommand = new DelegateCommand((param) =>
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ARAgingSummaryViewModel][Start:AgingRowSelectedCommand]");
                    try
                    {
                        Models.ARAgingSummary SelectedCustomer = param as Models.ARAgingSummary;
                        bool ShowAgingScreen = true;
                        //customer id comes null when group row is double-clicked
                        //Hence checked condition to handle the double click
                        if (SelectedCustomer.CustomerId == null)
                        {
                            return;
                        }

                        if (!CommonNavInfo.IsPretripInspectionDone)
                        {
                            ConfirmWindow confirmReadOnlyNavigation = new Helpers.ConfirmWindow { OkButtonContent = "Yes", CancelButtonContent = "No" };
                            confirmReadOnlyNavigation.Message = Application.Helpers.Constants.RouteHome.WithoutPretripAlertNavigationWarningMessage;

                            Messenger.Default.Send<Helpers.ConfirmWindow>(confirmReadOnlyNavigation, MessageToken);

                            if (confirmReadOnlyNavigation.Cancelled)
                            {
                                ShowAgingScreen = false;
                            }
                        }

                        if (ShowAgingScreen)
                        {
                            //Get and set customer object to common info before proceeding to open AR screen. 
                            ARAgingManager _ARAgingManager = new ARAgingManager();
                            Models.Customer customerToForword = _ARAgingManager.GetCustomerByIdWithStopDate(SelectedCustomer.CustomerId);
                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(customerToForword);

                            PayloadManager.PaymentPayload.Customer = customerToForword;
                            PayloadManager.OrderPayload.StopDate = Convert.ToDateTime(customerToForword.StopDate);
                            PayloadManager.OrderPayload.StopID = customerToForword.StopID;
                            PayloadManager.OrderPayload.RouteID = PayloadManager.ApplicationPayload.Route;

                            if (!customerToForword.IsTodaysStop)
                            {
                                var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteHome.NoStopForCustomerNavigationWarningMessage, MessageIcon = "Alert" };
                                Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                            }

                            //Set screen related information and then navigate  

                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Route Home";
                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Payments & AR";
                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteHome;
                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ARPayment;
                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                            SelectedTab = ViewModelMappings.TabView.RouteHome.ARAging;

                            ViewModels.CommonNavInfo objCommonNavInfo = Helpers.ResourceManager.CommonNavInfo;
                            objCommonNavInfo.ParentViewModel = this;

                            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ARPayment, CurrentViewName = ViewModelMappings.View.RouteHome, CloseCurrentView = false, ShowAsModal = false };
                            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][ARAgingSummaryViewModel][AgingRowSelectedCommand][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ARAgingSummaryViewModel][End:AgingRowSelectedCommand]");
                }
                );

        }
        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
