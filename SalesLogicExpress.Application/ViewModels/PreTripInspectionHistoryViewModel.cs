﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using SalesLogicExpress.Application.ReportViewModels;
using SalesLogicExpress.Application.ViewModelPayload;


namespace SalesLogicExpress.Application.ViewModels
{
    public class PreTripInspectionHistoryViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.PreTripInspectionHistoryViewModel");

        #region Properties
        Guid _MessageToken;
        public Guid MessageToken
        {

            get
            {
                return _MessageToken;
            }
            set
            {
                _MessageToken = value;
                OnPropertyChanged("MessageToken");
            }

        }
        ObservableCollection<PreTripVehicleInspection> _PreTripVehicleInspectionList = new ObservableCollection<PreTripVehicleInspection>();
        ObservableCollection<PreTripVehicleInspection> _SelectedInspections = new ObservableCollection<PreTripVehicleInspection>();
        public ObservableCollection<PreTripVehicleInspection> PreTripVehicleInspectionList
        {
            get
            {
                return _PreTripVehicleInspectionList;
            }
            set
            {
                _PreTripVehicleInspectionList = value;
                OnPropertyChanged("PreTripVehicleInspectionList");
            }
        }
        public ObservableCollection<PreTripVehicleInspection> SelectedItems
        {
            get
            {
                return _SelectedInspections;
            }
            set
            {
                _SelectedInspections = value;
                OnPropertyChanged("SelectedItems");
            }
        }

        PrintSettlementViewModel _PrintSettlementVM = null;
        public PrintSettlementViewModel PrintSettlementVM
        {
            get
            {
                return _PrintSettlementVM;
            }
            set
            {
                _PrintSettlementVM = value;
                OnPropertyChanged("PrintSettlementVM");
            }
        }
        public BaseViewModel ParentViewModel { get; set; }

        private int statusCheck;
        public int StatusCheck
        {
            get { return statusCheck; }
            set { statusCheck = value; OnPropertyChanged("StatusCheck"); }
        }

        private string vehicleModel;
        public string VehicleModel
        {
            get { return vehicleModel; }
            set { vehicleModel = value; OnPropertyChanged("VehicleModel"); }
        }

        private string vehicleNo;
        public string VehicleNo
        {
            get { return vehicleNo; }
            set { vehicleNo = value; OnPropertyChanged("VehicleNo"); }
        }

        private DateTime preTripDateTime;
        public DateTime PreTripDateTime
        {
            get { return preTripDateTime; }
            set { preTripDateTime = value; OnPropertyChanged("PreTripDateTime"); }
        }

        private string odometerReading;
        public string OdometerReading
        {
            get { return odometerReading; }
            set { odometerReading = value; }
        }

        private PreTripVehicleInspection pretripInspectionObject = new PreTripVehicleInspection();
        public PreTripVehicleInspection PretripInspectionObject
        {
            get
            { return pretripInspectionObject; }
            set
            {
                pretripInspectionObject = value;
                OnPropertyChanged("PretripInspectionObject");
            }
        }

        private string route;

        public string Route
        {
            get { return route; }
            set { route = value; OnPropertyChanged("Route"); }
        }
        #endregion

        #region Delegate Commands

        public DelegateCommand ShowListDetails { get; set; }
        public DelegateCommand PrintSettlement { get; set; }
        #endregion

        #region Constructor and Methods

        Managers.PreTripInspectionManager preTripM = new Managers.PreTripInspectionManager();
        public PreTripInspectionHistoryViewModel(Guid MessageToken)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:PreTripInspectionHistoryViewModel]\t" + DateTime.Now);
            Route = PayloadManager.ApplicationPayload.Route;
            this.MessageToken = MessageToken;
            InitializeCommands();
            GetInspectionHistory();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:PreTripInspectionHistoryViewModel]\t" + DateTime.Now);
        }
        void InitializeCommands()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:InitializeCommands]");
            #region Print section
            PrintSettlement = new DelegateCommand((param) =>
            {
                try
                {
                    if (SelectedItems != null && SelectedItems.Count == 0)
                    {
                        PrintSettlementVM = new PrintSettlementViewModel
                        {
                            SelectedFromDate = DateTime.Now,
                            SelectedToDate = DateTime.Now,

                            SelectionToDateEnd = DateTime.Now,
                            SelectionFromDateEnd = DateTime.Now,
                            SelectionFromDateStart = SalesLogicExpress.Application.ViewModelPayload.PayloadManager.ApplicationPayload.BaseDate,
                            SelectionToDateStart = SalesLogicExpress.Application.ViewModelPayload.PayloadManager.ApplicationPayload.BaseDate
                        };
                        PreTripVehicleInspection preTripObj = preTripM.GetVehicleDetails(Route, string.Empty);
                        PrintSettlementVM.VehicleMake = new ObservableCollection<VehicleMake>();
                        PrintSettlementVM.VehicleNumber = new ObservableCollection<VehicleNumber>();
                        VehicleMake allMake = new VehicleMake() { MakeID = "0", Make = "All" };
                        PrintSettlementVM.VehicleMake.Add(allMake);
                        VehicleNumber allNo = new VehicleNumber() { VehicleID = "0", VehicleNo = "All" };
                        PrintSettlementVM.VehicleNumber.Add(allNo);
                        var ENUM = preTripObj.VehicleMake.Select(x => x.Make).Distinct().ToObservableCollection();

                        for (int i = 1; i <= ENUM.Count; i++)
                        {
                            VehicleMake obj = new VehicleMake();
                            obj.MakeID = i.ToString();
                            obj.Make = ENUM[i - 1];
                            PrintSettlementVM.VehicleMake.Add(obj);
                        }
                        for (int i = 1; i <= preTripObj.VehicleNo.Count; i++)
                        {
                            VehicleNumber obj = new VehicleNumber();
                            obj.VehicleID = preTripObj.VehicleNo[i - 1].VehicleID;
                            obj.VehicleNo = preTripObj.VehicleNo[i - 1].VehicleNo;
                            PrintSettlementVM.VehicleNumber.Add(obj);
                        }
                        var OpenDialog = new Helpers.ConfirmWindow { TemplateKey = "PrintInspectionPrintSelection", Header = "Print Pre-Trip", OkButtonContent = "Continue", Context = PrintSettlementVM };
                        Messenger.Default.Send<ConfirmWindow>(OpenDialog, MessageToken);
                        if (OpenDialog.Confirmed)
                        {
                            ObservableCollection<PreTripVehicleInspection> newCollection = new ObservableCollection<PreTripVehicleInspection>();

                            newCollection = PrintSettlementVM.GetCollection(PreTripVehicleInspectionList);
                            PrintSelectedItems(newCollection);
                        }
                        //else if (OpenDialog.Cancelled)
                        //{

                        //}
                    }
                    if (SelectedItems != null && SelectedItems.Count > 0)
                    {
                        var OpenDialog = new Helpers.ConfirmWindow { Message = "Do You want to print this inspection form?", Header = "Print" };
                        Messenger.Default.Send<ConfirmWindow>(OpenDialog, MessageToken);
                        if (OpenDialog.Confirmed)
                        {
                            PrintSelectedItems(SelectedItems);
                        }
                        //else if (OpenDialog.Cancelled)
                        //{

                        //}
                    }
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][PrintSettlement DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }

            });

            #endregion

            #region Show all details
            ShowListDetails = new DelegateCommand((param) =>
            {

                if (param != null)
                {
                    pretripInspectionObject.ShowDetailsOfPreTripInspection = true;

                    var moveToView = new Helpers.NavigateToView
                    {
                        NextViewName = ViewModelMappings.View.PreTripVehicleInspection,
                        CurrentViewName = ViewModelMappings.View.RouteHome,
                        CloseCurrentView = true,
                        ShowAsModal = false,
                        Payload = param as PreTripVehicleInspection

                    };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                }
            });
            #endregion

        }


        public async void GetInspectionHistory()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:GetInspectionHistory]");
            await Task.Run(() =>
            {
                try
                {
                    PreTripVehicleInspectionList = new ObservableCollection<PreTripVehicleInspection>();
                    Managers.PreTripInspectionManager inspectionManager = new Managers.PreTripInspectionManager();
                    PreTripVehicleInspectionList = inspectionManager.GetPreTripInsectionHistory();
                    inspectionManager = null;
                    for (int i = 0; i < PreTripVehicleInspectionList.Count; i++)
                    {
                        //string status = Managers.PreTripInspectionManager.GetStatusTypeForInspection(PreTripVehicleInspectionList[i].StatusID);
                        //if (status == "PASS")
                        //{
                        //    StatusCheck = 1;
                        //}
                        //else
                        //    StatusCheck = 0;
                        StatusCheck = PreTripVehicleInspectionList[i].StatusTypeCD == "PASS" ? 1 : 0;
                        PreTripVehicleInspectionList[i].StatusCheckList = StatusCheck;
                       // PreTripVehicleInspectionList[i].StatusCode = status;
                        PreTripVehicleInspectionList[i].StatusCode = PreTripVehicleInspectionList[i].StatusTypeCD;
                        OdometerReading = PreTripVehicleInspectionList[i].OdometerReading;
                        VehicleModel = PreTripVehicleInspectionList[i].VehicleModel;
                        VehicleNo = PreTripVehicleInspectionList[i].VehicleNumber;
                        PreTripDateTime = PreTripVehicleInspectionList[i].PreTripDateTime;
                    }

                    System.Threading.Thread.Sleep(1000);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.PreTripInspectionHistoryViewModel][GetInspectionHistory][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                }
                finally
                {
                    if (this.ParentViewModel != null)
                    {
                        ParentViewModel.IsBusy = false;
                    }
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:GetInspectionHistory]");

            });
        }
        public void PrintSelectedItems(ObservableCollection<PreTripVehicleInspection> SelectedItems)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:PrintSelectedItems]");
            if (SelectedItems.Count > 0)
            {
                if (SelectedItems.Count > 1)
                {
                    Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Helpers.Constants.PreTrip.ReportAlert }, MessageToken);
                    return;
                }
                try
                {
                    List<PreTripInspectionQuestionList> lst = new List<PreTripInspectionQuestionList>();
                    for (int i = 0; i < SelectedItems.Count; i++)
                    {
                        SelectedItems[i].QuestionList = preTripM.GetDetailsForPreTrip(SelectedItems[i]);

                        var itm = SelectedItems[i].QuestionList.ToList();
                        //var m = SelectedItems[i].QuestionList.Select(x => AutoMapper.Mapper.Map<PreTripInspectionQuestionList>(x));
                        //var M = itm.Select(x => AutoMapper.Mapper.Map<PreTripInspectionQuestionList>(x)).ToList <PreTripInspectionQuestionList>();
                        foreach (var q in itm)
                        {
                            lst.Add(new PreTripInspectionQuestionList()
                            {
                                Description = q.Description,
                                IsCheckboxEnabled = q.IsCheckboxEnabled,
                                IsNoChecked = q.IsNoChecked,
                                IsNoPassedReasonEnabled = q.IsNoPassedReasonEnabled,
                                IsYesChecked = q.IsYesChecked,
                                NotPassedReason = q.NotPassedReason,
                                QuestionID = q.QuestionID,
                                QuestionTitle = q.QuestionTitle,
                                ResponseID = q.ResponseID
                            });

                        }
                        ShowReport(SelectedItems[i]);
                    }
                }

                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][PrintSelectedItems][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                ////Managers.UserManager.UserRoute
                //var  Action = new ReportEngine.ReportActions();
                //decimal DocId = ReportEngine.SLEReportEngine.GetExistingDocumentID(ResourceManager.GetOpenConnectionInstance(), Convert.ToInt32(SelectedItems[i].PreTripInspectionID), CommonNavInfo.OriginatingRouteID);
                //if(DocId<=0)
                //        Action= ReportEngine.ReportActions.Generate;
                //else
                //    Action = ReportEngine.ReportActions.Print;

                //Action = ReportEngine.ReportActions.ReGenrate;

                //PreTripInspectionReport Report = new PreTripInspectionReport();
                //if (DocId <= 0)
                //{
                //    Report = new PreTripInspectionReport(SelectedItems[i]);
                //    var DocInfo = Report.Generate<SalesLogicExpress.SLEReporting.PreTripInspection>(Convert.ToInt32(SelectedItems[i].PreTripInspectionID), ReportEngine.ReportActions.Generate, ReportEngine.ReportsTypes.PreTripInspection, ReportEngine.ContentTypes.PDF);
                //    DocId = DocInfo.DocumentId;
                //    ReportEngine.SLEReportEngine.InsertDocumentID(ResourceManager.GetOpenConnectionInstance(), Convert.ToDecimal(SelectedItems[i].PreTripInspectionID), CommonNavInfo.OriginatingRouteID, DocId);
                //}

                //Report.ViewReport(DocId, ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Route, null);
                //Report.REPrintReport(DocId, ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Route, null);
                //SalesLogicExpress.Application.ReportViewModels.GenericReport.FileBytes = ReportBytes;

            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:PrintSelectedItems]");
        }

        // Direct Print Report implementation for Multiple Report Print
        //public void ShowReport(PreTripVehicleInspection SelectedItem)
        //{
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:PrintPreTripInspectionForm]");
        //    try
        //    {
        //        var DocInfo = new ReportEngine.DocumentInformation();
        //        var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

        //        repEngine.Notify += repEngine_Notify;

        //        repEngine.Log = this.Logger;
        //        ReportEngine.IReportSLE ReportData = null;
        //        var DocumentId = SelectedItem.DocumentId;
        //        var SourceDocumentNo = Convert.ToDecimal(SelectedItem.PreTripInspectionID);
        //        if (DocumentId <= 0)
        //        {
        //            ReportData = new ReportViewModels.PreTripInspectionReport(SelectedItem);
        //            DocInfo = repEngine.Generate<Reporting.PreTripInspection>(SourceDocumentNo, true, ReportEngine.ReportCategory.Route, ReportEngine.ReportsTypes.PreTripInspection, ReportData, null, ReportEngine.ContentTypes.PDF);
        //        }
        //        else
        //        {
        //            DocInfo.DocumentId = DocumentId;
        //        }

        //        if (DocInfo.IsEndOfSequence)
        //        {
        //            Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, Please sync to continue" }, MessageToken);
        //            return;
        //        }
        //        if (DocInfo.IsDocumentCreated)
        //            ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.PreTrip_Inspection_Header", "PreTripInspectionHeaderId", SourceDocumentNo, DocInfo.DocumentId);
        //        SelectedItem.DocumentId = DocInfo.DocumentId; //Replace Latest

        //        var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Route, SelectedItem.DocumentId, null);
        //        if (ReportBytes == null)
        //        {
        //            Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Report not generated, Please try again." }, MessageToken);
        //            return;
        //        }


        //        //GenericReport.GenericReportViewModel grv = new GenericReport.GenericReportViewModel();
        //        //grv.ReportSource = ReportBytes;


        //        SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
        //        Payload.ReportFile = ReportBytes;
        //        Payload.CurrentView = ViewModelMappings.View.RouteHome;
        //        Payload.PreviousView = ViewModelMappings.View.RouteHome;
        //        Payload.CurrentViewTitle = "Pre-Inspection Trip Report";
        //        Payload.PreviousViewTitle = "PreInspection Trip";
        //        Payload.MessageToken = this.MessageToken;

        //        SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
        //        SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
        //        //  SalesLogicExpress.Application.ViewModels.CommonNavInfo.SelectTab = ViewModelMappings.TabView.RouteHome.PreTrip;
        //        repEngine.PrintDirectly(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Route, DocInfo.DocumentId, null, true);


        //        //var moveToView = new Helpers.NavigateToView
        //        //{
        //        //    NextViewName = ViewModelMappings.View.AppPDfWindow,
        //        //    CurrentViewName = ViewModelMappings.View.ServiceRoute,
        //        //    CloseCurrentView = false,
        //        //    Payload = Payload,
        //        //    ShowAsModal = false
        //        //};

        //        //Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
        //        var moveToView = new Helpers.NavigateToView
        //        {
        //            NextViewName = ViewModelMappings.View.RouteHome,
        //            Refresh = true,
        //            CurrentViewName = ViewModelMappings.View.PreTripVehicleInspection,

        //            CloseCurrentView = true,
        //            Payload = Payload,
        //            ShowAsModal = false
        //        };

        //        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);

        //        //new ReportViewModels.PreTripInspectionReport(InspectionModel);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("[SalesLogicExpress.Application.ViewModels][PreTripInspectionViewModel][PrintPreTripInspectionForm][ExceptionStackTrace=" + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:PrintPreTripInspectionForm]");
        //}

        public void ShowReport(PreTripVehicleInspection SelectedItem)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:ShowReport]");
            try
            {
                var DocInfo = new ReportEngine.DocumentInformation();
                var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                repEngine.Notify += repEngine_Notify;

                repEngine.Log = this.Logger;
                ReportEngine.IReportSLE ReportData = null;
                var DocumentId = SelectedItem.DocumentId;
                var SourceDocumentNo = Convert.ToDecimal(SelectedItem.PreTripInspectionID);
                if (DocumentId <= 0)
                {
                    ReportData = new ReportViewModels.PreTripInspectionReport(SelectedItem);
                    DocInfo = repEngine.Generate<Reporting.PreTripInspection>(SourceDocumentNo, true, ReportEngine.ReportCategory.Route, ReportEngine.ReportsTypes.PreTripInspection, ReportData, null, ReportEngine.ContentTypes.PDF);

                    //DocInfo = repEngine.SetDocumentView<SLEReporting.PreTripInspection>(documentId: DocumentId,
                    //                                                                      reportCategory: ReportEngine.ReportCategory.Route,
                    //                                                                      SourceDocumentNo: Convert.ToDecimal(SelectedItem.PreTripInspectionID),
                    //                                                                      ReportData: ReportData,
                    //                                                                      connection: ResourceManager.GetOpenConnectionInstance(),
                    //                                                                      reportTypeId: (int)ReportEngine.ReportsTypes.PreTripInspection,
                    //                                                                      reGenerate: false,
                    //                                                                      printerSettings: new System.Drawing.Printing.PrinterSettings()

                    //                                                                      );
                }
                else
                {
                    DocInfo.DocumentId = DocumentId;
                }

                if (DocInfo.IsEndOfSequence)
                {
                    Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, Please sync to continue" }, MessageToken);
                    return;
                }

                if (DocInfo.IsDocumentCreated)
                    ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.PreTrip_Inspection_Header", "PreTripInspectionHeaderId", SourceDocumentNo, DocInfo.DocumentId);
                SelectedItem.DocumentId = DocInfo.DocumentId; //Replace Latest

                var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Route, SelectedItem.DocumentId, null);
                if (ReportBytes == null)
                {
                    Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = Helpers.Constants.PreTripInspection.PriTripHistoryReGeneration }, MessageToken);
                    return;
                }

                //GenericReport.GenericReportViewModel grv = new GenericReport.GenericReportViewModel();
                //grv.ReportSource = ReportBytes;


                SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
                Payload.ReportFile = ReportBytes;
                Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                Payload.PreviousView = ViewModelMappings.View.RouteHome;
                Payload.CurrentViewTitle = "Pre-Inspection Trip Report";
                Payload.PreviousViewTitle = "PreInspection Trip";
                Payload.MessageToken = this.MessageToken;

                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.SelectTab = ViewModelMappings.TabView.RouteHome.PreTrip;

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.RouteHome, CloseCurrentView = true, Payload = Payload, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                ReportData = null;
                ReportBytes = null;
                //  Messenger.Default.Send<SalesLogicExpress.Application.AppPdfViewer.AppPDfWindow>(new AppPdfViewer.PDfWindow


                //Helpers.ReportViewer viewer = new ReportViewer();
                //viewer.ViewReport(ReportBytes);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerQuoteViewModel][Show Report][ExceptionStackTrace = " + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:ShowReport]");

        }

        void repEngine_Notify(string msg)
        {
            System.Windows.MessageBox.Show(msg);
        }

        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                GC.Collect();
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
    /// <summary>
    /// Viewmodel for binding to Print Dialog
    /// </summary>
    public class PrintSettlementViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.PrintSettlementViewModel");

        #region Properties
        private string route;
        public string Route
        {
            get { return route; }
            set { route = value; OnPropertyChanged("Route"); }
        }

        DateTime _SelectedToDate, _SelectedFromDate, _SelectionToDateEnd, _SelectionToDateStart, _SelectionFromDateEnd, _SelectionFromDateStart;
        string _SelectedVehicleMake, _SelectedVehicle;
        public DateTime SelectionFromDateEnd
        {
            get
            {
                return _SelectionFromDateEnd;
            }
            set
            {
                _SelectionFromDateEnd = value;
                OnPropertyChanged("SelectionFromDateEnd");
            }
        }
        public DateTime SelectionFromDateStart
        {
            get
            {
                return _SelectionFromDateStart;
            }
            set
            {
                _SelectionFromDateStart = value;
                OnPropertyChanged("SelectionFromDateStart");
            }
        }
        public DateTime SelectionToDateEnd
        {
            get
            {
                return DateTime.Today;
            }
            set
            {
                _SelectionToDateEnd = value;
                OnPropertyChanged("SelectionToDateEnd");
            }
        }
        public DateTime SelectionToDateStart
        {
            get
            {
                return SelectedFromDate;
            }
            set
            {
                _SelectionToDateStart = value;
                OnPropertyChanged("SelectionToDateStart");
            }
        }
        public DateTime SelectedToDate
        {
            get
            {
                return _SelectedToDate;
            }
            set
            {
                _SelectedToDate = value;
                if (SelectedFromDate > value)
                {
                    SelectedFromDate = value;
                }
                OnPropertyChanged("SelectedToDate");
            }
        }
        public DateTime SelectedFromDate
        {
            get
            {
                return _SelectedFromDate;
            }
            set
            {
                _SelectedFromDate = value;
                if (SelectedToDate < value)
                {
                    SelectedToDate = value;
                }
                if (value != null && value.DayOfYear != DateTime.Now.DayOfYear)
                    SelectionToDateStart = value;
                OnPropertyChanged("SelectedFromDate");
                OnPropertyChanged("SelectionToDateStart");
            }
        }
        public string SelectedVehicleMake
        {
            get
            {
                return _SelectedVehicleMake;
            }
            set
            {
                _SelectedVehicleMake = value;
                OnPropertyChanged("SelectedVehicleMake");
            }
        }
        public string SelectedVehicle
        {
            get
            {
                return _SelectedVehicle;
            }
            set
            {
                _SelectedVehicle = value;
                OnPropertyChanged("SelectedVehicle");
            }
        }

        private PreTripVehicleInspection preTripObj = new PreTripVehicleInspection();
        public PreTripVehicleInspection PreTripObj
        {
            get { return preTripObj; }
            set { preTripObj = value; OnPropertyChanged("PreTripObj"); }
        }
        private ObservableCollection<VehicleNumber> vehicleNumber = new ObservableCollection<VehicleNumber>();
        public ObservableCollection<VehicleNumber> VehicleNumber
        {
            get { return vehicleNumber; }
            set { vehicleNumber = value; OnPropertyChanged("VehicleNumber"); }
        }

        private ObservableCollection<VehicleMake> vehicleMake = new ObservableCollection<VehicleMake>();
        public ObservableCollection<VehicleMake> VehicleMake
        {
            get { return vehicleMake; }
            set { vehicleMake = value; OnPropertyChanged("VehicleMake"); }
        }

        #endregion

        #region DelegtCommand declaration
        public DelegateCommand SelectionOfVehicleNo { get; set; }
        #endregion

        #region Constructor
        public PrintSettlementViewModel()
        {
            Route = PayloadManager.ApplicationPayload.Route;
            InitializeCommands();
        }

        #endregion

        #region Methods
        Managers.PreTripInspectionManager preTripM = new Managers.PreTripInspectionManager();
        private void InitializeCommands()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PrintSettlementViewModel][Start:InitializeCommands]");
            try
            {
                PreTripVehicleInspection obj = new PreTripVehicleInspection();
                SelectionOfVehicleNo = new DelegateCommand((param) =>
                {
                    if (SelectedVehicleMake != "All")
                    {
                        VehicleNumber.Clear();

                        obj = preTripM.GetVehicleDetails(Route, SelectedVehicleMake);
                        VehicleNumber allNo = new VehicleNumber() { VehicleID = "0", VehicleNo = "All" };
                        VehicleNumber.Add(allNo);
                        for (int i = 1; i <= obj.VehicleNo.Count; i++)
                        {
                            VehicleNumber obj1 = new VehicleNumber();
                            obj1.VehicleID = obj.VehicleNo[i - 1].VehicleID;
                            obj1.VehicleNo = obj.VehicleNo[i - 1].VehicleNo;
                            VehicleNumber.Add(obj1);
                        }
                    }
                    else if (SelectedVehicleMake == "All")
                    {

                    }

                });
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels.PreTripInspectionHistoryViewModel][GetInspectionHistory][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PrintSettlementViewModel][End:InitializeCommands]");
        }

        public ObservableCollection<PreTripVehicleInspection> GetCollection(ObservableCollection<PreTripVehicleInspection> preTripList)
        {
            ObservableCollection<PreTripVehicleInspection> newCollectionForPrint = new ObservableCollection<PreTripVehicleInspection>();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PrintSettlementViewModel][Start:GetCollection]");
            try
            {
                if (SelectedVehicle == null)
                {
                    SelectedVehicle = "All";
                }
                if (SelectedVehicleMake == null)
                {
                    SelectedVehicleMake = "All";
                }
                var newcollection = preTripList.Where(x => (x.PreTripDateTime.Date >= SelectedFromDate.Date && x.PreTripDateTime.Date <= SelectedToDate.Date)).ToObservableCollection();

                if (SelectedVehicleMake != "All")
                {
                    newcollection = newcollection.Where(x => (x.VehicleModel == SelectedVehicleMake)).ToObservableCollection();
                }
                if (SelectedVehicle != "All")
                {
                    newcollection = newcollection.Where(x => (x.VehicleNumber == SelectedVehicle)).ToObservableCollection();
                }

                newCollectionForPrint = newcollection;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels.PreTripInspectionHistoryViewModel][GetCollection][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PrintSettlementViewModel][End:GetCollection]");
            return newCollectionForPrint;
        }
        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                GC.Collect();
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
