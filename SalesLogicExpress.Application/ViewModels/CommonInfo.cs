﻿using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using SalesLogicExpress.Domain;
using log4net;

namespace SalesLogicExpress.Application.ViewModels
{
    public class CommonNavInfo : AppViewModelBase
    {

        private readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void NotifyStaticPropertyChanged(string propertyName)
        {
            if (StaticPropertyChanged != null)
                StaticPropertyChanged(null, new PropertyChangedEventArgs(propertyName));
        }

        #region Actions

        bool _SyncEnabled = true;
        public bool SyncEnabled
        {
            get
            {
                return _SyncEnabled = true;
            }
            set
            {
                _SyncEnabled = value;
                SyncTitle = _SyncEnabled ? "Sync Now" : "Synching...";
                OnPropertyChanged("SyncEnabled");
            }
        }
        string _SyncTitle = "Sync Now";
        public string SyncTitle
        {
            get
            {
                return _SyncTitle;
            }
            set
            {
                _SyncTitle = value;
                OnPropertyChanged("SyncTitle");
            }
        }
        public DelegateCommand MoveToView { get; set; }
        public DelegateCommand StartSync { get; set; }
        public DelegateCommand CancelSync { get; set; }
        public DelegateCommand OpenSyncDialog { get; set; }
        public DelegateCommand OpenNotificationDialog { get; set; }

        public object ParentViewModel { get; set; }
        public static object NavigationParameter { get; set; }

        static bool _IsBackwardNavigationEnabled = true;
        public static bool IsBackwardNavigationEnabled
        {
            get { return _IsBackwardNavigationEnabled; }
            set
            {
                _IsBackwardNavigationEnabled = value;
                NotifyStaticPropertyChanged("IsBackwardNavigationEnabled");
            }
        }

        #endregion

        #region Properties

        //private static bool? _IsEODCompleted; 
        ///// <summary>
        ///// Gets the EOD complete status 
        ///// </summary>
        //public static bool? IsEODCompleted
        //{
        //    get
        //    {
        //        if (_IsEODCompleted == null)
        //        {
        //            if (!string.IsNullOrEmpty(ResourceManager.Transaction.GetEODCompleteStatus()))
        //            {
        //                _IsEODCompleted = true;
        //            }
        //            else
        //            {
        //                _IsEODCompleted = false;
        //            }
        //        }

        //        return _IsEODCompleted;
        //    }
        //    set
        //    {
        //        _IsEODCompleted = value; 
        //    }
        //}

        public static string SelectTab { get; set; }

        private static bool? _IsEODPendingForVerification;

        public static bool? IsEODPendingForVerification
        {
            get
            {
                if (_IsEODPendingForVerification == null)
                {
                    if (SettlementConfirmationManager.IsPendingVerification())
                    {
                        _IsEODPendingForVerification = true;
                    }
                    else
                    {
                        _IsEODPendingForVerification = false;
                    }
                }
                return _IsEODPendingForVerification;

            }
            set { _IsEODPendingForVerification = value; }
        }


        public static Guid MessageToken { get; set; }
        public static string UserRoute
        {
            get
            {
                return UserManager.UserRoute;
            }
            set
            {
                UserManager.UserRoute = value;
            }
        }
        public static string RouteUser
        {
            get;
            set;
        }

        public static bool IsManuallySyncAllProfiles { get; set; }

        public static string UserBranch
        {
            get
            {
                return UserManager.UserBranch;
            }
        }
        public static int RouteID { get; set; }

        public static int OriginatingRouteID
        {
            get
            {
                return UserManager.UserId;
            }
        }
        public static int PartitioningRouteID
        {
            get
            {
                return UserManager.UserId;
            }
        }
        public static string DisplayUserName
        {
            get
            {
                return UserManager.UserDisplayName;
            }
        }
        public static string UserName
        {
            get
            {
                return UserManager.UserName.ToUpper();
            }
            set
            {
                UserManager.UserName = value;
            }
        }

        private static string _lastSynced;
        public static string LastSynced
        {
            get { return _lastSynced; }
            set
            {
                _lastSynced = value;
                NotifyStaticPropertyChanged("LastSynced");
            }
        }
        private static string _ViewTitle;
        public static string ViewTitle
        {
            get { return _ViewTitle; }
            set
            {
                _ViewTitle = value;
                NotifyStaticPropertyChanged("ViewTitle");
            }
        }
        private static bool _IsNetworkAvailable;
        public static bool IsNetworkAvailable
        {
            get { return _IsNetworkAvailable; }
            set
            {
                _IsNetworkAvailable = value;
                NotifyStaticPropertyChanged("IsNetworkAvailable");
            }
        }

        private static bool _IsSyncGoing;
        public static bool IsSyncGoing
        {
            get { return _IsSyncGoing; }
            set { _IsSyncGoing = value; IsSyncNowEnabled = !value; NotifyStaticPropertyChanged("IsSyncGoing"); }
        }

        private static bool isUpdateAvailable;
        public static bool IsUpdateAvailable
        {
            get { return isUpdateAvailable; }
            set { isUpdateAvailable = value; NotifyStaticPropertyChanged("IsUpdateAvailable"); }
        }

        private static bool _RefreshPreviousWindow;
        public static bool RefreshPreviousWindow
        {
            get { return _RefreshPreviousWindow; }
            set { _RefreshPreviousWindow = value; NotifyStaticPropertyChanged("RefreshPreviousWindow"); }
        }
        public static ViewModelMappings.View CurrentView { get; set; }
        public static ViewModelMappings.View PreviousView { get; set; }
        public static void SetPreviousViewTitle(string title)
        {
            previousViewTitle = title;
        }
        static string previousViewTitle;
        public static string PreviousViewTitle
        {
            get { return previousViewTitle; }
            set
            {
                previousViewTitle = value;
                NotifyStaticPropertyChanged("PreviousViewTitle");
                NotifyStaticPropertyChanged("ShowBackNavigation");
            }
        }
        public static string LastSynched { get; set; }

        private static bool _ShowBackNavigation = false;
        public static bool ShowBackNavigation
        {
            get
            {
                if (CurrentView == ViewModelMappings.View.AppPDfWindow && PreviousViewTitle != "")
                    return true;
                return _ShowBackNavigation;
            }
            set
            {
                _ShowBackNavigation = value;
                NotifyStaticPropertyChanged("ShowBackNavigation");
            }
        }

        private static SalesLogicExpress.Domain.Customer customer;
        public static Customer Customer
        {
            get
            {
                return customer;
            }
            set
            {
                customer = value;
                NotifyStaticPropertyChanged("Customer");
            }
        }
        //public static SalesLogicExpress.Domain.Customer Customer { get; set; }
        private bool _IsCollectionEmpty;
        public bool IsCollectionEmpty
        {
            get
            {
                return _IsCollectionEmpty;
            }
            set
            {
                _IsCollectionEmpty = value;
                OnPropertyChanged("IsCollectionEmpty");
            }
        }
        bool _networkConnected = false;
        public bool NetWorkConnected
        {
            get { return _networkConnected; }
            set
            {
                _networkConnected = value;
                OnPropertyChanged("NetWorkConnected");
            }
        }
        bool _syncInProgress = false;
        public bool SyncInProgress
        {
            get { return _syncInProgress; }
            set
            {
                _syncInProgress = value;

                OnPropertyChanged("SyncInProgress");
            }
        }
        string _NullSyncedMessage;
        public string NullSyncedMessage
        {
            get
            {
                return _NullSyncedMessage;
            }
            set
            {
                _NullSyncedMessage = value;
                OnPropertyChanged("NullSyncedMessage");
            }
        }

        private static bool isSyncNowEnabled;

        public static bool IsSyncNowEnabled
        {
            get { return isSyncNowEnabled; }
            set { isSyncNowEnabled = value; NotifyStaticPropertyChanged("IsSyncNowEnabled"); }
        }
        public static bool IsMandatorySync { get; set; }
        public static bool IsSynced { get; set; }


        /// <summary>
        /// Get or set the flag to indicate if pretrip inspection has been done or not
        /// </summary>
        public static bool IsPretripInspectionDone { get; set; }
        #endregion

        #region Constructor
        public CommonNavInfo()
        {
            InitializeCommands();
            IsSyncNowEnabled = true;
        }
        ~CommonNavInfo()
        {
            ResourceManager.NetworkManager.InternetStateChanged -= NetworkInfo_InternetStateChanged;
            ResourceManager.Synchronization.SyncProgressChanged -= Synchronization_SyncProgressChanged;
            //ResourceManager.QueueManager.QueueChanged -= queueManager_QueueChanged;
        }
        public void InitilizeQueue()
        {
            ResourceManager.NetworkManager.InternetStateChanged += NetworkInfo_InternetStateChanged;
            ResourceManager.Synchronization.SyncProgressChanged += Synchronization_SyncProgressChanged;

            ResourceManager.QueueManager.BockUIManually += blockUi;
            //ResourceManager.QueueManager.QueueChanged += queueManager_QueueChanged;
            //ResourceManager.Transaction.TransactionLogged += Transaction_TransactionLogged;
        }


        void InitializeCommands()
        {
            //ResourceManager.NetworkManager.InternetStateChanged -= NetworkInfo_InternetStateChanged;
            //ResourceManager.Synchronization.SyncProgressChanged -= Synchronization_SyncProgressChanged;
            //ResourceManager.QueueManager.QueueChanged -= queueManager_QueueChanged;
            //ResourceManager.Transaction.TransactionLogged -= Transaction_TransactionLogged;

            //ResourceManager.NetworkManager.InternetStateChanged += NetworkInfo_InternetStateChanged;
            //ResourceManager.Synchronization.SyncProgressChanged += Synchronization_SyncProgressChanged;
            //ResourceManager.QueueManager.QueueChanged += queueManager_QueueChanged;
            //ResourceManager.Transaction.TransactionLogged += Transaction_TransactionLogged;


            Logger.Info("[SalesLogicExpress.Application.ViewModels][CommonInfo][Start:InitializeCommands]");
            try
            {
                var syncDetails = ResourceManager.Synchronization.GetSyncLastTimes;
                //GetNotifications();
                GetInventoryfromNotification();
                if (syncDetails.Values.Any(x => x == null))
                {
                    if (syncDetails["LastDownloadTime"] == null && syncDetails["LastUploadTime"] != null)
                        LastSynced = syncDetails["LastUploadTime"].Value.ToString("MM'/'dd'/'yyyy' 'hh:mm tt ").ToLower();

                    if (syncDetails["LastDownloadTime"] != null && syncDetails["LastUploadTime"] == null)
                        LastSynced = syncDetails["LastDownloadTime"].Value.ToString("MM'/'dd'/'yyyy' 'hh:mm tt ").ToLower();
                }
                else
                {
                    var lastDownloadSync = syncDetails["LastDownloadTime"];
                    var lastUploadSync = syncDetails["LastUploadTime"];

                    LastSynced = lastDownloadSync > lastUploadSync ? lastDownloadSync.Value.ToString("MM'/'dd'/'yyyy' 'hh:mm tt ").ToLower() : lastUploadSync.Value.ToString("MM'/'dd'/'yyyy' 'hh:mm tt ").ToLower();
                }

                //GetPendingSyncItems();
                IsNetworkAvailable = ResourceManager.NetworkManager.IsInterNetConnected();
                #region MoveToView
                MoveToView = new DelegateCommand((param) =>
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][CommonInfo][Start:MoveToView]");
                    try
                    {

                        bool CanNavigate = true;
                        BaseViewModel model = null;
                        if (IsBackwardNavigationEnabled)
                        {
                            if (ParentViewModel != null)
                            {
                                model = (BaseViewModel)ParentViewModel;
                                if (model.IsDirty || BaseViewModel.IsDirtyStatic)
                                {
                                    string message = string.IsNullOrEmpty(model.Message) ? "You haven't saved your changes to " + ViewTitle.ToTitleCase() + ".\n" : model.Message + ".\n";
                                    string okContent = string.IsNullOrEmpty(model.OkContent) ? "Save & Continue" : model.OkContent;
                                    string cancelContent = string.IsNullOrEmpty(model.CancelContent) ? "Discard & Continue" : model.CancelContent;

                                    var confirmBackNavigation = new Helpers.ConfirmWindow { Message = message, CancelButtonContent = cancelContent, OkButtonContent = okContent };

                                    //** Condition removed as we dont need to have back popup for order return
                                    if (ViewTitle == ViewModelMappings.View.ItemReturnsScreen.GetEnumDescription())//|| ViewTitle == ViewModelMappings.View.OrderReturnsScreen.GetEnumDescription()  
                                    {
                                        confirmBackNavigation = new Helpers.ConfirmWindow { Message = Helpers.Constants.OrderReturns.I_RMessage, CancelButtonContent = "No", OkButtonContent = "Yes" };
                                    }
                                    if (ViewTitle == ViewModelMappings.View.CustomerQuote.GetEnumDescription())
                                    {
                                        if (ViewModelPayload.PayloadManager.QuotePayload.QuoteId.Equals(0) || ViewModelPayload.PayloadManager.QuotePayload.QuoteId.Equals("NEW"))
                                        {
                                            confirmBackNavigation = new Helpers.ConfirmWindow { Message = Helpers.Constants.Quote.CustomerQuoteBackNavigationMessage, CancelButtonContent = "No", OkButtonContent = "Yes" };
                                        }
                                        else
                                        {
                                            if (((CustomerQuoteViewModel)ParentViewModel).CustomerQuote.Items.Count > 0)
                                            {
                                                confirmBackNavigation = new Helpers.ConfirmWindow { Message = Helpers.Constants.Quote.CustomerQuoteBackNavigationMessageInEditMode, CancelButtonContent = "No", OkButtonContent = "Yes" };
                                            }
                                            else
                                            {
                                                confirmBackNavigation = new Helpers.ConfirmWindow { Message = Helpers.Constants.Quote.CustomerQuoteDirtyBackNavigationMessage, CancelButtonContent = "No", OkButtonContent = "Yes" };
                                            }

                                        }
                                    }
                                    if (ViewTitle == ViewModelMappings.View.ProspectQuote.GetEnumDescription())
                                    {
                                        if (ViewModelPayload.PayloadManager.QuotePayload.QuoteId.Equals(0) || ViewModelPayload.PayloadManager.QuotePayload.QuoteId.Equals("NEW"))
                                        {
                                            confirmBackNavigation = new Helpers.ConfirmWindow { Message = Helpers.Constants.Quote.CustomerQuoteBackNavigationMessage, CancelButtonContent = "No", OkButtonContent = "Yes" };
                                        }
                                        else
                                        {
                                            if (ViewModelPayload.PayloadManager.ProspectPayload.IsPartialPicking)
                                            {
                                                CanNavigate = false;
                                                string messagestr = Helpers.Constants.Quote.CustomerQuoteBackNavigationMessagePartialPick;
                                                var alertMessage = new Helpers.AlertWindow { Header = "Alert", Message = messagestr, MessageIcon = "Alert" };
                                                Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                                                return;
                                            }
                                            else
                                            {
                                                if (((CustomerQuoteViewModel)ParentViewModel).CustomerQuote.Items.Count > 0)
                                                {
                                                    if (!ViewModelPayload.PayloadManager.ProspectPayload.ShowEditModeButton)
                                                    {
                                                        confirmBackNavigation = new Helpers.ConfirmWindow { Message = Helpers.Constants.Quote.CustomerQuoteBackNavigatioWithoutComplete, CancelButtonContent = "No", OkButtonContent = "Yes" };
                                                    }
                                                    else
                                                    {
                                                        confirmBackNavigation = new Helpers.ConfirmWindow { Message = Helpers.Constants.Quote.CustomerQuoteBackNavigationMessageInEditMode, CancelButtonContent = "No", OkButtonContent = "Yes" };
                                                    }
                                                }
                                                else
                                                {
                                                    confirmBackNavigation = new Helpers.ConfirmWindow { Message = Helpers.Constants.Quote.CustomerQuoteDirtyBackNavigationMessage, CancelButtonContent = "No", OkButtonContent = "Yes" };
                                                }

                                            }
                                        }
                                    }
                                    if (ViewTitle == ViewModelMappings.View.ReturnsAndCredits.GetEnumDescription())
                                    {
                                        confirmBackNavigation = new Helpers.ConfirmWindow { Message = Helpers.Constants.OrderReturns.ConfirmRO, CancelButtonContent = "Void", OkButtonContent = "Hold" };
                                    }
                                    if (ViewTitle == ViewModelMappings.View.OrderReturnsScreen.GetEnumDescription())
                                    {
                                        confirmBackNavigation = new Helpers.ConfirmWindow { Message = Helpers.Constants.OrderReturns.ConfirmRO, CancelButtonContent = "Void", OkButtonContent = "Hold" };
                                    }
                                    if (ViewTitle == ViewModelMappings.View.CustomerStopSequencingView.GetEnumDescription())
                                    {
                                        confirmBackNavigation = new Helpers.ConfirmWindow { Message = Helpers.Constants.CustStopSequencing.ResetConfirmation, CancelButtonContent = "No", OkButtonContent = "Yes" };
                                    }
                                    else if (ViewTitle == ViewModelMappings.View.SettlementConfirmation.GetEnumDescription())
                                    {
                                        confirmBackNavigation = new Helpers.ConfirmWindow { Message = Helpers.Constants.RouteSettlement.BackNavigation, OkButtonContent = "OK" };
                                    }
                                    else if (ViewTitle == ViewModelMappings.View.SettlementVerification.GetEnumDescription())
                                    {
                                        confirmBackNavigation = new Helpers.ConfirmWindow { Message = Helpers.Constants.RouteSettlement.BackNavigationVerification, OkButtonContent = "OK" };

                                    }
                                    else if (ViewTitle == ViewModelMappings.View.ReplenishAddLoadView.GetEnumDescription()
                                        || ViewTitle == ViewModelMappings.View.ReplenishSggestionView.GetEnumDescription()
                                        || ViewTitle == ViewModelMappings.View.SuggestionReturnView.GetEnumDescription()
                                        || ViewTitle == ViewModelMappings.View.ReplenishUnloadView.GetEnumDescription())
                                    {
                                        if (ViewTitle == ViewModelMappings.View.ReplenishAddLoadView.GetEnumDescription())
                                        {
                                            message = "Do you want to save these changes? \n" + "else you will lost this add load transaction.";
                                        }
                                        else if (ViewTitle == ViewModelMappings.View.ReplenishSggestionView.GetEnumDescription())
                                        {
                                            message = "Do you want to save these changes? \n" + "else you will lost this suggestion repln. transaction.";
                                        }
                                        else if (ViewTitle == ViewModelMappings.View.SuggestionReturnView.GetEnumDescription())
                                        {
                                            message = "Do you want to save these changes? \n" + "else you will lost this suggestion return transaction.";
                                        }
                                        else if (ViewTitle == ViewModelMappings.View.ReplenishUnloadView.GetEnumDescription())
                                        {
                                            message = "Do you want to save these changes? \n" + "else you will lost this unload transaction.";
                                        }
                                        if (ViewModelPayload.PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                                        {
                                            confirmBackNavigation = new Helpers.ConfirmWindow { Message = message, OkButtonContent = "Yes", CancelButtonContent = "No" };
                                            Messenger.Default.Send<Helpers.ConfirmWindow>(confirmBackNavigation, MessageToken);
                                            if (confirmBackNavigation.Confirmed)
                                            {
                                                model.ConfirmSave();

                                                message = "Do you want to continue pick?";
                                                confirmBackNavigation = new Helpers.ConfirmWindow { Message = message, OkButtonContent = "Yes", CancelButtonContent = "No" };
                                                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmBackNavigation, MessageToken);
                                                if (confirmBackNavigation.Confirmed)
                                                {
                                                    model.ConfirmSave();
                                                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReplenishAddLoadPickView, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadView, CloseCurrentView = true, ShowAsModal = false, Refresh = false };
                                                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                                                }
                                                else
                                                {
                                                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadView, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = ViewModelMappings.TabView.RouteHome.Replenishment };
                                                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                                                }
                                                return;
                                            }
                                            if (confirmBackNavigation.Closed)
                                            {
                                                model.CanNavigate = false;
                                                model.ConfirmClosed(); return;
                                            }
                                            else if (confirmBackNavigation.Cancelled)
                                            {
                                                model.ConfirmCancel();
                                                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadView, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = ViewModelMappings.TabView.RouteHome.Replenishment };
                                                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            confirmBackNavigation = new Helpers.ConfirmWindow { Message = message, OkButtonContent = "Yes", CancelButtonContent = "No" };
                                            //Messenger.Default.Send<Helpers.ConfirmWindow>(confirmBackNavigation, MessageToken);
                                        }
                                    }
                                    else if (ViewTitle == ViewModelMappings.View.ReplenishAddLoadPickView.GetEnumDescription()
                                        || ViewTitle == ViewModelMappings.View.ReplenishSuggestionPickView.GetEnumDescription()
                                        || ViewTitle == ViewModelMappings.View.SuggestionReturnPickView.GetEnumDescription()
                                        || ViewTitle == ViewModelMappings.View.HeldReturnPickView.GetEnumDescription()
                                        || ViewTitle == ViewModelMappings.View.ReplenishUnloadPickView.GetEnumDescription())
                                    {
                                        message = "Do you want to hold this transaction?";
                                        confirmBackNavigation = new Helpers.ConfirmWindow { Message = message, OkButtonContent = "Yes", CancelButtonContent = "No" };

                                    }
                                    Messenger.Default.Send<Helpers.ConfirmWindow>(confirmBackNavigation, MessageToken);
                                    if (confirmBackNavigation.Confirmed)
                                    {
                                        model.ConfirmSave();

                                    }
                                    if (confirmBackNavigation.Closed)
                                    {
                                        model.CanNavigate = false;
                                        model.ConfirmClosed();
                                    }
                                    else if (confirmBackNavigation.Cancelled)
                                    {
                                        // CanNavigate is set to false as cancellation doesn't need navigation
                                        model.CanNavigate = false;
                                        model.ConfirmCancel();
                                    }
                                    CanNavigate = model.CanNavigate;
                                }
                                else
                                {
                                    if (ViewTitle == ViewModelMappings.View.ReplenishAddLoadPickView.GetEnumDescription() ||
                                        ViewTitle == ViewModelMappings.View.ReplenishSuggestionPickView.GetEnumDescription() ||
                                        ViewTitle == ViewModelMappings.View.SuggestionReturnPickView.GetEnumDescription() ||
                                        ViewTitle == ViewModelMappings.View.SuggestionReturnPickView.GetEnumDescription() ||
                                        ViewTitle == ViewModelMappings.View.ReplenishUnloadPickView.GetEnumDescription())
                                    {
                                        if (!model.CanNavigate)
                                        {
                                            var alertMessage = new Helpers.AlertWindow { Message = "Complete void process first.", MessageIcon = "Alert" };
                                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                                            return;
                                        }
                                    }

                                    //if (CurrentView == ViewModelMappings.View.ReturnOrderPickScreen)
                                    //{
                                    //    var alertMessage = new Helpers.AlertWindow { Message = "Please complete the pending activity.", MessageIcon = "Alert" };
                                    //    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                                    //    return;
                                    //}

                                    if (CurrentView == ViewModelMappings.View.ReturnOrderPickScreen)
                                    {
                                        IsNavigateFromReturnPickScr = true;
                                    }

                                }
                            }
                            if (CanNavigate)
                            {
                                //if (CurrentView == ViewModelMappings.View.PickOrder)
                                //{
                                //    var alertMessage = new Helpers.AlertWindow { Message = "Please complete the pending activity.", MessageIcon = "Alert" };
                                //    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                                //    return;
                                //}

                                if (PreviousView == ViewModelMappings.View.ServiceRoute && CurrentView == ViewModelMappings.View.ProspectHome)
                                {

                                    model.SelectedTab = "TabItemProspects";
                                    System.Diagnostics.Debug.WriteLine("CommonNavInfo > MoveToView -->RefreshPreviousWindow = " + RefreshPreviousWindow);
                                    var moveToView = new Helpers.NavigateToView { NextViewName = PreviousView, CurrentViewName = CurrentView, CloseCurrentView = true, Refresh = RefreshPreviousWindow, SelectTab = model != null ? model.SelectedTab : "" };
                                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                                    RefreshPreviousWindow = false;
                                }
                                //else if (CurrentView == ViewModelMappings.View.RouteSettlementReport && PreviousView == ViewModelMappings.View.RouteSettlement)
                                //{
                                //    System.Diagnostics.Debug.WriteLine("CommonNavInfo > MoveToView -->RefreshPreviousWindow = " + RefreshPreviousWindow);
                                //    var moveToView = new Helpers.NavigateToView { NextViewName = PreviousView, CurrentViewName = CurrentView, CloseCurrentView = true, Refresh = RefreshPreviousWindow, SelectTab = "TabItem_RouteSettlement_Settlement" != null ? model.SelectedTab : "" };
                                //    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                                //    RefreshPreviousWindow = false;
                                //}
                                else
                                {
                                    System.Diagnostics.Debug.WriteLine("CommonNavInfo > MoveToView -->RefreshPreviousWindow = " + RefreshPreviousWindow);
                                    //var moveToView = new Helpers.NavigateToView { NextViewName = PreviousView, CurrentViewName = CurrentView, CloseCurrentView = true, Refresh = RefreshPreviousWindow, SelectTab = model != null ? model.SelectedTab : "" };
                                    // Added for Back Navigation Selected Tab by Vignesh D
                                    var moveToView = new Helpers.NavigateToView { NextViewName = PreviousView, CurrentViewName = CurrentView, CloseCurrentView = true, Refresh = RefreshPreviousWindow, SelectTab = model != null ? model.SelectedTab : SelectTab != null ? SelectTab : "" };
                                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                                    RefreshPreviousWindow = false;
                                }
                            }
                        }
                        else
                        {
                            // ToDo Remove this condition in future when Pick order from VOID state, back navigation discussed
                            if (CurrentView == ViewModelMappings.View.PickOrder || CurrentView == ViewModelMappings.View.DeliveryScreen || CurrentView == ViewModelMappings.View.AcceptAndPrintReturns || CurrentView == ViewModelMappings.View.ReturnOrderPickScreen)
                            {
                                return;
                            }


                            IsBackwardNavigationEnabled = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][CommonInfo][MoveToView][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][CommonInfo][End:MoveToView]");


                });
                #endregion
                CancelSync = new DelegateCommand((param) =>
                {
                    ResourceManager.Synchronization.CancelSync();
                });
                StartSync = new DelegateCommand((param) =>
                {
                    SyncEnabled = false;
                    SyncNow();
                });
                OpenSyncDialog = new DelegateCommand((param) =>
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][CommonInfo][Start:OpenSyncDialog]");
                    try
                    {
                        //GetPendingSyncItems();
                        Dictionary<string, bool> flagDictionary = ResourceManager.WebServiceManager.GetFlagsForSync();
                        if (flagDictionary != null)
                        {
                            IsMandatorySync = flagDictionary["IsMandatorySync"];
                            IsSynced = flagDictionary["IsSynced"];
                            IsUpdateAvailable = flagDictionary["IsUpdateAvailable"];

                        }
                        var dialog = new Helpers.DialogWindow { TemplateKey = "OpenSyncPopup", Title = "Route Sync", Payload = this };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialog, CommonNavInfo.MessageToken);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][CommonInfo][OpenSyncDialog][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][CommonInfo][End:OpenSyncDialog]");
                });
                OpenNotificationDialog = new DelegateCommand((param) =>
                {
                    if (NotificationModel.NotificationCount > 0)
                    {
                        var dialog = new Helpers.DialogWindow { TemplateKey = "OpenNotificationDialog", Title = "Notification List", Payload = NotificationModel };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialog, CommonNavInfo.MessageToken);
                    }
                });
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CommonInfo][InitializeCommands][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CommonInfo][End:InitializeCommands]");
        }

        string _ProgressSteps = "Completing full synchronization. Please standby.";
        public string ProgressSteps
        {
            get { return _ProgressSteps; }
            set { _ProgressSteps = value; OnPropertyChanged("ProgressSteps"); }
        }
        //void ReportProgress(string value)
        //{
        //    ProgressSteps = ProgressSteps + Environment.NewLine + value;
        //}

        bool _IsCloseVisible;
        public bool IsCloseVisible
        {
            get { return _IsCloseVisible; }
            set { _IsCloseVisible = value; OnPropertyChanged("IsCloseVisible"); }
        }
        //void ReportProgressCompleted(bool value)
        //{
        //    if (value == true)
        //    {
        //        IsCloseVisible = true;
        //    }
        //}

        //void Transaction_TransactionLogged(object sender, EventArgs e)
        //{
        //    GetPendingSyncItems();
        //}

        public async void SyncNow()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CommonInfo][AsyncStart:SyncNow]");

            await Task.Run(() =>
            {
                try
                {
                    IsCloseVisible = false;
                    ProgressSteps = "Completing full synchronization. Please standby.";

                    IsSyncGoing = true;
                    ResourceManager.QueueManager.QueueProcess("All", true);
                    SyncEnabled = true;
                }
                catch (Exception ex)
                {
                    IsCloseVisible = true;
                    ProgressSteps = "Completed full synchronization.";
                    SyncEnabled = true;
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CommonInfo][SyncNow][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CommonInfo][AsyncEnd:SyncNow]");

            //try
            //{
            //    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);

            //    var dialog = new Helpers.DialogWindow { TemplateKey = "OpenSyncProgressPopup", Title = "Synchronizing", Payload = this };
            //    GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<Helpers.DialogWindow>(dialog, CommonNavInfo.MessageToken);
            //}
            //catch (Exception ex)
            //{
            //    Logger.Error("[SalesLogicExpress.Application.ViewModels][CommonInfo][SyncNow][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            //}
        }

        public void blockUi(object sender, bool e)
        {
            if (e)
            {
                if (System.Windows.Application.Current != null)
                    System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)delegate // <--- HERE
                    {
                        try
                        {
                            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);

                            var dialog = new Helpers.DialogWindow { TemplateKey = "OpenSyncProgressPopup", Title = "Synchronizing", Payload = this };
                            GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<Helpers.DialogWindow>(dialog, CommonNavInfo.MessageToken);
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("[SalesLogicExpress.Application.ViewModels][CommonInfo][SyncNow][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                        }
                    });
            }

        }
        //void queueManager_QueueChanged(object sender, QueueManager.SyncQueueChangedEventArgs e)
        //{
        //    GetPendingSyncItems();
        //}

        //void GetPendingSyncItems()
        //{
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][CommonInfo][Start:GetPendingSyncItems]");
        //    try
        //    {
        //        ObservableCollection<SalesLogicExpress.Application.Managers.QueueManager.SyncQueueItem> items = ResourceManager.QueueManager.GetPendingSyncItems();
        //        if (items.Count != 0)
        //        {
        //            if (System.Windows.Application.Current != null)
        //                System.Windows.Application.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
        //                {
        //                    StateCollection.Clear();
        //                    foreach (SalesLogicExpress.Application.Managers.QueueManager.SyncQueueItem item in items)
        //                    {
        //                        StateCollection.Add(item);
        //                    }
        //                    IsCollectionEmpty = false;
        //                });
        //        }
        //        else
        //        {
        //            IsCollectionEmpty = true;
        //            NullSyncedMessage = "No items to be synced.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        SyncEnabled = true;
        //        Logger.Error("[SalesLogicExpress.Application.ViewModels][CommonInfo][GetPendingSyncItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
        //    }
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][CommonInfo][End:GetPendingSyncItems]");
        //}
        void Synchronization_SyncProgressChanged(object sender, SyncUpdatedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CommonInfo][Start:Synchronization_SyncProgressChanged]");
            try
            {
                if (System.Windows.Application.Current != null)
                    System.Windows.Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        if (e.State != SyncUpdateType.ServerNotReachable && e.State != SyncUpdateType.SyncFailed)
                        {
                            IsSyncGoing = ResourceManager.Synchronization.IsSynching;
                            if (e.State == SyncUpdateType.DownloadComplete)
                            {
                                //GetPendingSyncItems();
                                //GetNotifications();
                                GetInventoryfromNotification();
                            }
                        }
                        if (e.State == SyncUpdateType.ServerNotReachable || e.State == SyncUpdateType.SyncFailed)
                        {
                            string message = e.State == SyncUpdateType.SyncFailed ? "Sync Failed" : "Synchronization Server not available for sync.";
                            var alertMessage = new Helpers.AlertWindow { Message = message, MessageIcon = "Alert" };
                            // Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        }
                        SyncEnabled = true;
                        IsCloseVisible = !ResourceManager.Synchronization.IsSynching;

                        //Added by Dinesh - 10/26/2016
                        //Description: Sync Popup Message Change
                        if (ResourceManager.Synchronization.IsSynching)
                            ProgressSteps = "Completing full synchronization. Please standby.";
                        else
                            ProgressSteps = "Completed full synchronization.";
                    });
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CommonInfo][Synchronization_SyncProgressChanged][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CommonInfo][End:Synchronization_SyncProgressChanged]");
        }

        private void GetInventoryfromNotification()
        {
            NotificationModel.GetAdjustmentNotifications();
            NotificationModel.MasterNotifications.ItemPropertyChanged -= Notifications_ItemPropertyChanged;
            NotificationModel.MasterNotifications.ItemPropertyChanged += Notifications_ItemPropertyChanged;

        }
        private void GetNotifications()
        {
            NotificationModel.GetNotifications();
            NotificationModel.Notifications.ItemPropertyChanged -= Notifications_ItemPropertyChanged;
            NotificationModel.Notifications.ItemPropertyChanged += Notifications_ItemPropertyChanged;
        }

        void Notifications_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(""))
            {

            }
        }

        void NetworkInfo_InternetStateChanged(object sender, NetworkManager.InternetStateChangedEventArgs e)
        {
            IsNetworkAvailable = (e.State == NetworkManager.InternetState.Connected) ? true : false;
        }
        public static void SetSelectedCustomer(SalesLogicExpress.Domain.Customer customer)
        {
            Customer = customer;
        }
        public static object GetSelectedCustomer()
        {
            return Customer;
        }
        public static void SetLastSynched(string lastSynched)
        {
            LastSynced = lastSynched;
        }

        #endregion

        //private ObservableCollection<SalesLogicExpress.Application.Managers.QueueManager.SyncQueueItem> _StateCollection = new ObservableCollection<SalesLogicExpress.Application.Managers.QueueManager.SyncQueueItem>();
        //public ObservableCollection<SalesLogicExpress.Application.Managers.QueueManager.SyncQueueItem> StateCollection
        //{
        //    get
        //    {
        //        return _StateCollection;
        //    }
        //    set { _StateCollection = value; OnPropertyChanged("StateCollection"); }
        //}
        NotificationViewModel _Notifications = new NotificationViewModel();
        public NotificationViewModel NotificationModel
        {
            get
            {
                return _Notifications;
            }
            set
            {
                _Notifications = value;
                OnPropertyChanged("Notifications");
            }
        }


        static string _SyncMessage;
        public static string SyncMessage
        {
            get
            {
                return _SyncMessage;
            }
            set
            {
                _SyncMessage = value;
                NotifyStaticPropertyChanged("SyncMessage");
            }
        }

        static bool _IsNavigateFromReturnPickScr = false;

        public static bool IsNavigateFromReturnPickScr
        {
            get { return _IsNavigateFromReturnPickScr; }
            set { _IsNavigateFromReturnPickScr = value; NotifyStaticPropertyChanged("IsNavigateToAcceptAndPrint"); }
        }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
