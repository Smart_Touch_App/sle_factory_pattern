﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using GalaSoft.MvvmLight.Messaging;
using Telerik.Windows.Controls;
using System.Collections.ObjectModel;
using SalesLogicExpress.Application.Managers;
using System.Collections;


namespace SalesLogicExpress.Application.ViewModels
{
    public class ReplenishAddLoadPickViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.ReplenishAddLoadPickViewModel");
        OrderManager objOrderManger = new OrderManager();
        PickItemManager pickMgr;
        #region Properties & Variables
        public Guid MessageToken { get; set; }
        private string uPickID = string.Empty;

        public string UPickID
        {
            get { return uPickID; }
            set { uPickID = value; }
        }

        private string oPickID = string.Empty;

        public string OPickID
        {
            get { return oPickID; }
            set { oPickID = value; }
        }
        private string cPickID = string.Empty;

        public string CPickID
        {
            get { return cPickID; }
            set { cPickID = value; }
        }

        private string aPickID = string.Empty;

        public string APickID
        {
            get { return aPickID; }
            set { aPickID = value; }
        }

        private string sPickID = string.Empty;

        public string SPickID
        {
            get { return sPickID; }
            set { sPickID = value; }
        }

        private ObservableCollection<ReasonCode> manualPickReasonList = new ObservableCollection<ReasonCode>();

        public ObservableCollection<ReasonCode> ManualPickReasonList
        {
            get { return manualPickReasonList; }
            set { manualPickReasonList = value; }
        }



        #endregion

        #region OnPropertyChanged

        #region Bool

        private bool showShippedQty = false;

        /// <summary>
        /// Get or set the flag to show the shipped column
        /// </summary>
        public bool ShowShippedQty
        {
            get
            {
                return showShippedQty;
            }
            set
            {
                showShippedQty = value;
                OnPropertyChanged("ShowShippedQty");
            }
        }

        private bool canRemoveExceptions = false;
        public bool CanRemoveExceptions
        {
            get { return canRemoveExceptions; }
            set { canRemoveExceptions = value; OnPropertyChanged("CanRemoveExceptions"); }
        }

        private int _OldManualPickCount;

        public int OldManualPickCount
        {
            get { return _OldManualPickCount; }
            set { _OldManualPickCount = value; }
        }

        private int _NewManualPickCount;

        public int NewManualPickCount
        {
            get { return _NewManualPickCount; }
            set { _NewManualPickCount = value; }
        }

        private bool isScannerOn = false;
        public bool IsScannerOn
        {
            get { return isScannerOn; }
            set { isScannerOn = value; OnPropertyChanged("IsScannerOn"); }
        }


        private bool isEnableRemoveException = false;
        public bool IsEnableRemoveException
        {
            get { return isEnableRemoveException; }
            set { isEnableRemoveException = value; OnPropertyChanged("IsEnableRemoveException"); }
        }


        private bool isEnableAddException = false;
        public bool IsEnableAddException
        {
            get { return isEnableAddException; }
            set { isEnableAddException = value; OnPropertyChanged("isEnableAddException"); }
        }


        private bool isEnableCompletePick = false;
        public bool IsEnableCompletePick
        {
            get { return isEnableCompletePick; }
            set { isEnableCompletePick = value; OnPropertyChanged("IsEnableCompletePick"); }
        }


        private bool isEnableShort = false;
        public bool IsEnableShort
        {
            get { return isEnableShort; }
            set { isEnableShort = value; OnPropertyChanged("IsEnableShort"); }
        }


        private bool isEnableHold = false;
        public bool IsEnableHold
        {
            get { return isEnableHold; }
            set { isEnableHold = value; OnPropertyChanged("IsEnableHold"); }
        }



        private bool isEnablePrint = false;
        public bool IsEnablePrint
        {
            get { return isEnablePrint; }
            set { isEnablePrint = value; OnPropertyChanged("IsEnablePrint"); }
        }



        private bool isEnableManualPick = false;
        public bool IsEnableManualPick
        {
            get { return isEnableManualPick; }
            set { isEnableManualPick = value; OnPropertyChanged("IsEnableManualPick"); }
        }


        private bool isFocusedManualPick = false;
        public bool IsFocusedManualPick
        {
            get { return isFocusedManualPick; }
            set { isFocusedManualPick = value; OnPropertyChanged("IsFocusedManualPick"); }
        }


        private bool isVisibleHeldQty = false;
        public bool IsVisibleHeldQty
        {
            get { return isVisibleHeldQty; }
            set { isVisibleHeldQty = value; OnPropertyChanged("IsVisibleHeldQty"); }
        }

        private bool isHeldReturn = false;
        public bool IsHeldReturn
        {
            get { return isHeldReturn; }
            set { isHeldReturn = value; OnPropertyChanged("IsHeldReturn"); }
        }

        private bool isVisibleAdjustQty = true;

        public bool IsVisibleAdjustQty
        {
            get { return isVisibleAdjustQty; }
            set { isVisibleAdjustQty = value; OnPropertyChanged("IsVisibleAdjustQty"); }
        }
        #endregion

        #region int
        private int manuallyPickCountOfSelected = 0;
        public int ManuallyPickCountOfSelected
        {
            get { return manuallyPickCountOfSelected; }
            set { manuallyPickCountOfSelected = value; OnPropertyChanged("ManuallyPickCountOfSelected"); }
        }
        #endregion

        #region String
        //used for saving replenishmentID both add load and suggestion
        private string replenishID = string.Empty;
        public string ReplenishID
        {
            get { return replenishID; }
            set { replenishID = value; OnPropertyChanged("ReplenishID"); }
        }


        private string pickedItemNo = string.Empty;
        public string PickedItemNo
        {
            get { return pickedItemNo; }
            set { pickedItemNo = value; OnPropertyChanged("PickedItemNo"); }
        }


        private string primaryUOM = string.Empty;
        public string PrimaryUOM
        {
            get { return primaryUOM; }
            set { primaryUOM = value; OnPropertyChanged("PrimaryUOM"); }
        }


        private string routeNameAddLoad = string.Empty;
        public string RouteNameAddLoad
        {
            get { return routeNameAddLoad; }
            set { routeNameAddLoad = value; OnPropertyChanged("RouteNameAddLoad"); }
        }


        private string replenishIDName = string.Empty;
        public string ReplenishIDName
        {
            get { return replenishIDName; }
            set { replenishIDName = value; OnPropertyChanged("ReplenishIDName"); }
        }


        private string messageLine1 = string.Empty;
        public string MessageLine1
        {
            get { return messageLine1; }
            set { messageLine1 = value; OnPropertyChanged("MessageLine1"); }
        }


        private string messageLine2 = string.Empty;
        public string MessageLine2
        {
            get { return messageLine2; }
            set { messageLine2 = value; OnPropertyChanged("MessageLine2"); }
        }


        private string itemNumberOfSelected = string.Empty;
        public string ItemNumberOfSelected
        {
            get { return itemNumberOfSelected; }
            set { itemNumberOfSelected = value; OnPropertyChanged("ItemNumberOfSelected"); }
        }


        private string primaryUMOfSelected = string.Empty;
        public string PrimaryUMOfSelected
        {
            get { return primaryUMOfSelected; }
            set { primaryUMOfSelected = value; OnPropertyChanged("PrimaryUMOfSelected"); }
        }


        private string primaryQtyHeader = string.Empty;
        public string PrimaryQtyHeader
        {
            get { return primaryQtyHeader; }
            set { primaryQtyHeader = value; OnPropertyChanged("PrimaryQtyHeader"); }
        }
        #endregion

        #region Class

        private AddLoadPick addLoadPickSelectedItem = null;
        public AddLoadPick AddLoadPickSelectedItem
        {
            get { return addLoadPickSelectedItem; }
            set
            {
                addLoadPickSelectedItem = value; OnPropertyChanged("AddLoadPickSelectedItem");
                if (value != null)
                {
                    IsFocusedManualPick = false;
                    AddLoadPickExceptionSelectedItem = null;

                    // Set Manual pick panel
                    PrimaryUMOfSelected = AddLoadPickSelectedItem.TransactionUOM;
                    //ManuallyPickCountOfSelected = 0;
                    ManuallyPickCountOfSelected = Convert.ToInt32(AddLoadPickSelectedItem.TransactionQty);
                    ItemNumberOfSelected = AddLoadPickSelectedItem.ItemNumber;

                    //Enable Disable Short and manual pick
                    if (!PayloadManager.RouteReplenishmentPayload.NavigatedForVoid)
                    {
                        IsEnableShort = AddLoadPickSelectedItem.ExceptionReasonCode == "UnderPicked" ? true : false;
                        IsEnableManualPick = (AddLoadPickSelectedItem.ExceptionReasonCode != "Short") ? true : false;
                        AddLoadPickSelectedItem.IsEnableScanner = (AddLoadPickSelectedItem.ExceptionReasonCode == "Short") ? false : true;
                    }
                    AddLoadPickSelectedItem.IsAllowManualPick = false;
                    if (!AddLoadPickSelectedItem.IsAllowManualPick)
                    {
                        foreach (AddLoadPick item in PickItems)
                        {
                            item.IsAllowManualPick = false;
                        }
                    }
                    foreach (AddLoadPick item in PickItemsException)
                    {
                        item.IsAllowManualPick = false;
                        item.IsEnableScannerInException = false;
                    }
                    if (!PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck)
                    {
                        EnableDisableScannerForSelectedItem();
                    }
                }
                else
                {
                    if (!PayloadManager.RouteReplenishmentPayload.NavigatedForVoid)
                    {
                        IsEnableShort = false;
                    }
                }
            }
        }



        private AddLoadPick addLoadPickExceptionSelectedItem = null;
        public AddLoadPick AddLoadPickExceptionSelectedItem
        {
            get { return addLoadPickExceptionSelectedItem; }
            set
            {
                addLoadPickExceptionSelectedItem = value; OnPropertyChanged("AddLoadPickExceptionSelectedItem");
                if (value != null)
                {
                    IsFocusedManualPick = false;
                    AddLoadPickSelectedItem = null;

                    if (!PayloadManager.RouteReplenishmentPayload.NavigatedForVoid)
                    {
                        IsEnableAddException = true;
                    }
                    IsEnableRemoveException = true;
                    if (PayloadManager.RouteReplenishmentPayload.NavigatedForVoid)
                    {
                        IsEnableAddException = false;
                        IsEnableRemoveException = false;
                    }

                    PrimaryUMOfSelected = AddLoadPickExceptionSelectedItem.TransactionUOM;
                    ItemNumberOfSelected = AddLoadPickExceptionSelectedItem.ItemNumber;
                    //ManuallyPickCountOfSelected = 0;
                    ManuallyPickCountOfSelected = Convert.ToInt32(AddLoadPickExceptionSelectedItem.ExceptionCount);

                    if (AddLoadPickExceptionSelectedItem.IsEnableScannerInException)
                    {
                        AddLoadPickExceptionSelectedItem.IsAllowManualPick = false; IsEnableManualPick = true;
                    }
                    else
                    {
                        foreach (AddLoadPick item in PickItemsException)
                        {
                            item.IsAllowManualPick = false;
                            item.IsEnableScannerInException = false;
                        }
                        IsEnableManualPick = false;
                    }
                    AddLoadPickSelectedItem = null;
                }
                else
                {
                    IsEnableAddException = false; IsEnableRemoveException = false;
                }
            }
        }



        private ReasonCode selectedManualPickReasonCode = new ReasonCode();
        public ReasonCode SelectedManualPickReasonCode
        {
            get { return selectedManualPickReasonCode; }
            set { selectedManualPickReasonCode = value; OnPropertyChanged("SelectedManualPickReasonCode"); }
        }

       private AddLoadPick addLoadPrintPickSelectedItem = null;
       public AddLoadPick AddLoadPrintPickSelectedItem
       {

           get { return addLoadPrintPickSelectedItem; }
           set
           {
               addLoadPrintPickSelectedItem = value; OnPropertyChanged("AddLoadPrintPickSelectedItem");
           }
       }


        #endregion

        #region Collection

        ObservableCollection<dynamic> pickItems = new ObservableCollection<dynamic>();
        public ObservableCollection<dynamic> PickItems
        {
            get { return pickItems; }
            set
            {
                pickItems = value; OnPropertyChanged("PickItems");
            }
        }

        private string _ChkManualPickSameItem;

        public string ChkManualPickSameItem
        {
            get { return _ChkManualPickSameItem; }
            set { _ChkManualPickSameItem = value; }
        }


        private int _ChkManualPickSameOrder;

        public int ChkManualPickSameOrder
        {
            get { return _ChkManualPickSameOrder; }
            set { _ChkManualPickSameOrder = value; }
        }




        private ObservableCollection<dynamic> pickItemsException = new ObservableCollection<dynamic>();
        public ObservableCollection<dynamic> PickItemsException
        {
            get { return pickItemsException; }
            set { pickItemsException = value; OnPropertyChanged("PickItemsException"); }
        }
        #endregion

        #endregion

        #region Commands Declaretion
        public DelegateCommand SaveCommand { get; set; }
        public DelegateCommand SaveGetManualPickReasonListCommand { get; set; }
        public DelegateCommand ScanByDeviceCommand { get; set; }
        public DelegateCommand RemoveCommand { get; set; }
        public DelegateCommand RemoveOKCommand { get; set; }
        public DelegateCommand AddExceptionsCommand { get; set; }
        public DelegateCommand ScanByDeviceExceptionCommand { get; set; }
        public DelegateCommand CompletePickCommand { get; set; }
        public DelegateCommand ShortCommand { get; set; }
        public DelegateCommand SetManualPickReasonCommand { get; set; }
        public DelegateCommand ManualPickCommand { get; set; }

        public DelegateCommand ManualPickContinueCommand { get; set; }
        public DelegateCommand UpdateOnManualCountCommand { get; set; }
        public DelegateCommand HoldCommand { get; set; }
        public DelegateCommand PrintCommand { get; set; }
        public DelegateCommand ShortYesCommand { get; set; }
        public DelegateCommand ShortNoCommand { get; set; }
        public DelegateCommand HoldYesCommand { get; set; }
        public DelegateCommand HoldNoCommand { get; set; }
        public DelegateCommand TwoLineAlertOKCommand { get; set; }
        //public DelegateCommand RemoveExceptionCommand { get; set; }
        //    
        public DelegateCommand SwitchScannerMode { get; set; }

        #endregion

        #region Command Defination

        public void InitialiseCommands()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:InitialiseCommands]");
            SwitchScannerMode = new DelegateCommand((param) =>
            {
                bool InvertMode = !IsScannerOn;
                IsScannerOn = InvertMode;
            });

            //SaveCommand = new DelegateCommand((param) =>
            //{
            //    if (param != null)
            //    {
            //        //SaveExecute();
            //    }
            //});

            SaveGetManualPickReasonListCommand = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    ReasonCode obj = (ReasonCode)param;
                    SaveGetManualPickReasonListExecute(obj);
                }
            });

            ScanByDeviceCommand = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    AddLoadPick obj = (AddLoadPick)param;
                    ScanByDeviceExecute(obj);
                    ClearFlagVarForManualPick();
                }
            });

            ScanByDeviceExceptionCommand = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    AddLoadPick obj = (AddLoadPick)param;
                    ScanByDeviceExceptionExecute(obj);
                    ClearFlagVarForManualPick();
                }
            });

            RemoveCommand = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    AddLoadPick obj = (AddLoadPick)param;
                    RemoveExecute();
                }
            });

            RemoveOKCommand = new DelegateCommand((param) =>
            {
                RemoveOKExecute();

            });

            AddExceptionsCommand = new DelegateCommand((param) =>
            {
                AddExceptionsExecute();

            });

            ShortCommand = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    AddLoadPick obj = (AddLoadPick)param;
                    ShortExecute(obj);
                }
            });
            CompletePickCommand = new DelegateCommand((param) =>
            {
                CompletePickExecute();

            });
            SetManualPickReasonCommand = new DelegateCommand((param) =>
            {
                SetManualPickReasonExecute();

            });
            ManualPickCommand = new DelegateCommand((param) =>
            {
                ManualPickExecute();
            });
            ManualPickContinueCommand = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    ManualPickContinueExecute();
                }
            });
            UpdateOnManualCountCommand = new DelegateCommand((param) =>
            {
                UpdateOnManualCountExecute();
            });
            HoldCommand = new DelegateCommand((param) =>
            {
                HoldExecute();
            });
            PrintCommand = new DelegateCommand((param) =>
            {
                PrintExecute();
            });
            ShortYesCommand = new DelegateCommand((param) =>
            {
                ShortYesExecute();
            });
            ShortNoCommand = new DelegateCommand((param) =>
            {
                ShortNoExecute();
            });
            HoldYesCommand = new DelegateCommand((param) =>
            {
                HoldYesExecute();
            });
            HoldNoCommand = new DelegateCommand((param) =>
            {
                HoldNoExecute();
            });
            TwoLineAlertOKCommand = new DelegateCommand((param) =>
            {
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
            });
            //RemoveExceptionCommand = new DelegateCommand((param) =>
            //{
            //    if (param != null)
            //    {
            //        AddLoadPick obj = (AddLoadPick)param;
            //        RemoveExecute();
            //    }
            //});
            //public DelegateCommand TwoLineAlertOKCommand { get; set; }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:InitialiseCommands]");
        }

        public void ScanByDeviceExecute(AddLoadPick param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:ScanByDeviceExecute]\t" + DateTime.Now + "");

            try
            {
                param.ManuallyPickCount = 0; ManuallyPickCountOfSelected = 0;
                dynamic itm1 = PickItems.FirstOrDefault(i => i.ItemId == param.ItemId && i.TransactionDetailID == param.TransactionDetailID);
                if (itm1.ExceptionReasonCode == "Short")
                {
                    //Show popup 
                    MessageLine1 = "Your last picked item is short adjusted.";
                    MessageLine2 = "Please put it back.";
                    var dialog = new Helpers.DialogWindow { TemplateKey = "TwoLineAlert", Title = "Alert" };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                    return;
                }
                param.PickAdjusted = 0;
                param.IsManuallyPicking = false;
                pickMgr.PickItem(param,null,null,true);
                //if (param.ExceptionReasonCode.ToString() == "Adjusted" || param.ExceptionReasonCode.ToString() == "Complete")
                //{
                //    param.ExceptionReasonCode = "OverPicked";
                //}
                //if (param.PickQtyPrimaryUOM == param.TransactionQtyPrimaryUOM)
                //{
                //    param.ExceptionReasonCode = "Complete";
                //}
                foreach (AddLoadPick item in PickItems.ToList())
                {
                    item.IsAllowManualPick = false;
                }
                foreach (AddLoadPick item in PickItemsException.ToList())
                {
                    item.IsAllowManualPick = false;
                }
                AddLoadPickSelectedItem = param; AddLoadPickExceptionSelectedItem = null;

                if (AddLoadPickSelectedItem != null)
                {
                    AddLoadPickSelectedItem.IsAllowManualPick = true;
                }
                IsFocusedManualPick = true;

            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel ScanByDeviceExecute(" + param.ItemNumber + ") error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:ScanByDeviceExecute]\t");
        }

        public void ScanByDeviceExceptionExecute(AddLoadPick param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:ScanByDeviceExceptionExecute]\t" + DateTime.Now + "");

            try
            {
                param.ManuallyPickCount = 0; ManuallyPickCountOfSelected = 0;
                //You can not unpick item which is not present in exception list
                dynamic itm1 = PickItemsException.FirstOrDefault(i => i.ItemId == param.ItemId && i.TransactionDetailID == param.TransactionDetailID);
                if (itm1 == null)
                {
                    //Show popup 
                    MessageLine1 = "Your last picked item is not in exception list.";
                    MessageLine2 = "Please put it back.";
                    var dialog = new Helpers.DialogWindow { TemplateKey = "TwoLineAlert", Title = "Alert" };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                    return;
                }
                param.IsManuallyPicking = false;
                pickMgr.UnPickItem(param);

                foreach (AddLoadPick item in PickItemsException)
                {
                    item.IsAllowManualPick = false;
                }
                foreach (AddLoadPick item in PickItems)
                {
                    item.IsAllowManualPick = false;
                }
                AddLoadPickExceptionSelectedItem = param; AddLoadPickSelectedItem = null;
                if (AddLoadPickExceptionSelectedItem != null)
                {
                    AddLoadPickExceptionSelectedItem.IsAllowManualPick = true;
                }

                IsFocusedManualPick = true;
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel ScanByDeviceExceptionExecute(" + param.ItemNumber + ") error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:ScanByDeviceExceptionExecute]\t");



        }

        public void RemoveExecute()
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:RemoveExceptionsExecute]\t" + DateTime.Now + "");

            try
            {
                //Show popup for remove Exception
                MessageLine1 = "Are you sure, you want to remove exceptions?";
                MessageLine2 = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck ? "You should now put-away any exception picked by scanning them back onto your branch." : "You should now put-away any exception picked by scanning them back onto your truck.";
                var dialog = new Helpers.DialogWindow { TemplateKey = "RemoveExceptionInAddLoadPicks", Title = "Remove exception" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);

            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel ScanByDeviceExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:RemoveExceptionsExecute]\t");
        }

        public void RemoveOKExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:RemoveExceptionOKExecute]\t" + DateTime.Now + "");

            try
            {
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                foreach (AddLoadPick item in PickItemsException)
                {
                    item.IsEnableScannerInException = false;
                }
                AddLoadPickExceptionSelectedItem.IsEnableScannerInException = true;
                AddLoadPickSelectedItem = null; IsEnableManualPick = true; IsEnableRemoveException = false; IsEnableAddException = false;
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel RemoveExceptionOKExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:RemoveExceptionOKExecute]\t");


        }

        public void AddExceptionsExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:AddExceptionsExecute]\t" + DateTime.Now + "");

            try
            {

                //Add selected item to pick list
                pickMgr.AdjustPick(AddLoadPickExceptionSelectedItem, PickItemManager.PickAdjustmentType.Exception);
                EnableDisableCompletePick(); EnableDisableScanner();
                IsEnableAddException = false; IsEnableRemoveException = false;
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel AddExceptionsExecute() error: " + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:AddExceptionsExecute]\t");
        }

        public void ShortExecute(AddLoadPick param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:ShortExecute]\t" + DateTime.Now + "");

            try
            {
                // Show popup for short
                var dialog = new Helpers.DialogWindow { TemplateKey = "ShortPickItem", Title = "Confirmation" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel ShortExecute(" + param.ItemNumber + ") error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:ShortExecute]\t");


        }
        public void ShortYesExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:ShortYesExecute]\t" + DateTime.Now + "");

            try
            {
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                pickMgr.AdjustPick(AddLoadPickSelectedItem, PickItemManager.PickAdjustmentType.Short);
                IsEnableShort = false; IsEnableManualPick = false;
                EnableDisableCompletePick();
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel ShortYesExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:ShortYesExecute]\t");


        }
        public void ShortNoExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:ShortNoExecute]\t" + DateTime.Now + "");
            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:ShortNoExecute]\t");
        }
        public void CompletePickExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:CompletePickExecute]\t" + DateTime.Now + "");

            try
            {
                List<AddLoadDetails> AddLoadDetailsCollection = new List<AddLoadDetails>();
                List<AddLoadDetails> OrderUpdateCollection = new List<AddLoadDetails>();
                foreach (dynamic item in PickItems)
                {
                    AddLoadDetails o = new AddLoadDetails();
                    o.PickedQty = item.PickedQty;
                    o.AvailableQty = item.AvailableQty;
                    o.ItemId = item.ItemId;
                    o.OnHandQty = item.ActualQtyOnHand;
                    o.ParLevel = item.ParLevel;
                    o.CommittedQty = item.CommittedQty;
                    o.HeldQty = item.HeldQty;
                    o.ReplnDetailId = item.TransactionDetailID;
                    AddLoadDetailsCollection.Add(o);

                }


                if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.HeldReturnPickView.ToString())
                {
                    foreach (dynamic item in PickItems)
                    {
                        AddLoadDetails o = new AddLoadDetails();
                        o.PickedQty = item.PickedQty;
                        o.ItemId = item.ItemId;
                        o.HeldQty = item.HeldQty;
                        o.OrderID = Convert.ToString(item.OrderID);
                        o.ReplnDetailId = item.TransactionDetailID;
                        OrderUpdateCollection.Add(o);
                    }
                }

                //CMPLT
                if (!PayloadManager.RouteReplenishmentPayload.NavigatedForVoid)
                {
                    if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnPickView.ToString()
                        || PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishUnloadPickView.ToString()
                        || PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.HeldReturnPickView.ToString())
                    {


                        if (PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                        {
                            ReplenishAddLoadManager.UpdateAddLoadHeader("CMPLT", PayloadManager.RouteReplenishmentPayload.SelectedReplnId);
                            // Update Committed qty
                            ReplenishAddLoadManager.UpdateCommittedQtyForReplenishment(PayloadManager.RouteReplenishmentPayload.SelectedReplnId);


                            //Update Replenishment detail after completing  replenishment
                            AddLoadDetailsCollection = ReplenishAddLoadManager.GetItemDetailsForReplenishment.ToList();
                            //Update Replenishment detail after completing  replenishment
                            ReplenishAddLoadManager.UpdateAddLoad(AddLoadDetailsCollection, PayloadManager.RouteReplenishmentPayload.SelectedReplnId);

                            if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.HeldReturnPickView.ToString())
                            {
                                new RouteReplenishmentTabViewModel().LogHeldReturnActivity();
                            }
                        }
                        else
                        {
                            ReplenishAddLoadManager.UpdateAddLoadHeader("PIC", PayloadManager.RouteReplenishmentPayload.SelectedReplnId);
                        }

                        if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.HeldReturnPickView.ToString())
                        {
                            foreach (AddLoadDetails item in OrderUpdateCollection)
                            {
                                // pickMgr.UpdateInventory(item.ItemId, item.OnHandQty);
                                //string queryUpdate = "update busdta.Order_Detail  set  ReturnHeldQty = ReturnHeldQty - '" + item.PickedQty + "' where orderid='" + item.OrderID + "' and itemid = '" + item.ItemId + "' and RelatedLineID = '" + item.ReplnDetailId + "'";
                                //int n = DbEngine.ExecuteNonQuery(queryUpdate);

                                objOrderManger.ReplenishAddLoadPickViewModel_CompletePickExecute(item.PickedQty, item.OrderID, item.ItemId, item.ReplnDetailId);
                            }
                        }
                    }
                    else
                    {
                        ReplenishAddLoadManager.UpdateAddLoadHeader("CMPLT", PayloadManager.RouteReplenishmentPayload.SelectedReplnId);

                        //Update inventory
                        foreach (AddLoadDetails item in AddLoadDetailsCollection)
                        {
                            pickMgr.UpdateInventory(item.ItemId, item.OnHandQty);
                        }
                        //Update Replenishment detail after completing  replenishment
                        AddLoadDetailsCollection = ReplenishAddLoadManager.GetItemDetailsForReplenishment.ToList();
                        //Update Replenishment detail after completing  replenishment
                        ReplenishAddLoadManager.UpdateAddLoad(AddLoadDetailsCollection, PayloadManager.RouteReplenishmentPayload.SelectedReplnId);
                    }

                }
                else
                {
                    ReplenishAddLoadManager.UpdateAddLoadHeader("VOID", PayloadManager.RouteReplenishmentPayload.SelectedReplnId);
                    CanNavigate = true; PayloadManager.RouteReplenishmentPayload.NavigatedForVoid = false;
                    //Update inventory
                    foreach (AddLoadDetails item in AddLoadDetailsCollection)
                    {
                        pickMgr.UpdateInventory(item.ItemId, item.OnHandQty);
                    }
                    //Update Replenishment detail after completing  replenishment
                    AddLoadDetailsCollection = ReplenishAddLoadManager.GetItemDetailsForReplenishment.ToList();
                    //Update Replenishment detail after completing  replenishment
                    ReplenishAddLoadManager.UpdateAddLoad(AddLoadDetailsCollection, PayloadManager.RouteReplenishmentPayload.SelectedReplnId);
                }

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadPickView, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = ViewModelMappings.TabView.RouteHome.Replenishment };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel CompletePickExecute() error: " + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:CompletePickExecute]\t");
        }

        public void SaveGetManualPickReasonListExecute(ReasonCode obj)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:SaveGetManualPickReasonListExecute]\t" + DateTime.Now + "");
            var dialog = new Helpers.DialogWindow { TemplateKey = "ManualPickReason", Title = "Reason Code", Payload = null };
            Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:SaveGetManualPickReasonListExecute]\t" + DateTime.Now + "");
        }

        public void SetManualPickReasonExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:SetManualPickReasonExecute]\t" + DateTime.Now + "");
            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:SetManualPickReasonExecute]\t" + DateTime.Now + "");
        }
        public void ManualPickExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:ManualPickExecute]\t" + DateTime.Now + "");

            try
            {
                bool showAlert = false;
                if (AddLoadPickSelectedItem != null)
                {
                    if (!string.IsNullOrEmpty(AddLoadPickSelectedItem.ItemId))
                    {
                        var dialog = new Helpers.DialogWindow { TemplateKey = "ManualPickReason", Title = "Reason Code", Payload = null };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                    }
                    else
                    {
                        showAlert = true;
                    }
                }
                else if (AddLoadPickExceptionSelectedItem != null)
                {

                    if (!string.IsNullOrEmpty(AddLoadPickExceptionSelectedItem.ItemId))
                    {
                        var dialog = new Helpers.DialogWindow { TemplateKey = "ManualPickReason", Title = "Reason Code", Payload = null };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                    }
                    else
                    {
                        showAlert = true;

                    }
                }
                else
                {
                    showAlert = true;

                }
                if (showAlert)
                {
                    var alertMessage = new Helpers.AlertWindow { Message = "Select item for manual pick.", MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    return;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel ManualPickExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:ManualPickExecute]\t");



        }
        public void ManualPickContinueExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:ManualPickContinueExecute]\t" + DateTime.Now + "");

            try
            {
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                if (AddLoadPickSelectedItem != null)
                {
                    AddLoadPickSelectedItem.IsAllowManualPick = true;
                }
                if (AddLoadPickExceptionSelectedItem != null)
                {
                    AddLoadPickExceptionSelectedItem.IsAllowManualPick = true;
                }
                IsFocusedManualPick = true;

            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel ManualPickContinueExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:ManualPickContinueExecute]\t");
        }
        public void UpdateOnManualCountExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:UpdateOnManualCountExecute]\t" + DateTime.Now + "");
            try
            {

                if (AddLoadPickSelectedItem != null)
                {
                    if (IsScannerOn == false)
                    {
                        AddLoadPickSelectedItem.IsAllowManualPick = true;
                    }

                    if (AddLoadPickSelectedItem.IsAllowManualPick)
                    {
                        AddLoadPickSelectedItem.ManuallyPickCount = ManuallyPickCountOfSelected;

                        if (AddLoadPickSelectedItem.ManuallyPickCount == AddLoadPickSelectedItem.PickedQty)
                        {
                            return;
                        }

                        if (AddLoadPickSelectedItem.ManuallyPickCount > AddLoadPickSelectedItem.TransactionQty)
                        {
                            var alertMessage = new Helpers.AlertWindow { Message = "Manual quantity exceeds load quantity", MessageIcon = "Alert" };
                            //Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                            //return;
                        }
                        AddLoadPickSelectedItem.PickAdjusted = 0;
                        AddLoadPickSelectedItem.IsManuallyPicking = true;



                        pickMgr.PickItem(AddLoadPickSelectedItem,null,null,true);
                        //Enable Disable buttons
                        ManuallyPickCountOfSelected = 0;
                        foreach (AddLoadPick item in PickItems)
                        {
                            item.IsAllowManualPick = false;
                        }
                        foreach (AddLoadPick item in PickItemsException)
                        {
                            item.IsAllowManualPick = false;
                        }
                    }
                    else if (!string.IsNullOrEmpty(AddLoadPickSelectedItem.ItemId))
                    {
                        if (AddLoadPickExceptionSelectedItem == null)
                        {
                            var alertMessage = new Helpers.AlertWindow { Message = "Select reason for manual pick.", MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        }
                    }
                }

                if (AddLoadPickExceptionSelectedItem != null)
                {
                    if (!AddLoadPickExceptionSelectedItem.IsEnableScannerInException)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = "Click remove button to enable scanner.", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    if (AddLoadPickExceptionSelectedItem.IsAllowManualPick && AddLoadPickExceptionSelectedItem.IsEnableScannerInException)
                    {
                        AddLoadPickExceptionSelectedItem.ManuallyPickCount = ManuallyPickCountOfSelected;
                        if (AddLoadPickExceptionSelectedItem.ManuallyPickCount == 0)
                        {
                            return;
                        }
                        if (AddLoadPickExceptionSelectedItem.ExceptionCount < AddLoadPickExceptionSelectedItem.ManuallyPickCount)
                        {
                            //AddLoadPickExceptionSelectedItem.ManuallyPickCount = AddLoadPickExceptionSelectedItem.ExceptionCount;
                            //ManuallyPickCountOfSelected = AddLoadPickExceptionSelectedItem.ExceptionCount;
                            var alertMessage = new Helpers.AlertWindow { Message = "Manual quantity exceeds load quantity", MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                            return;
                        }

                        AddLoadPickExceptionSelectedItem.IsManuallyPicking = true;
                        pickMgr.UnPickItem(AddLoadPickExceptionSelectedItem);
                        ManuallyPickCountOfSelected = 0;
                        //Enable Disable buttons
                        EnableDisableCompletePick();
                        foreach (AddLoadPick item in PickItemsException)
                        {
                            item.IsAllowManualPick = false;
                        }
                        foreach (AddLoadPick item in PickItems)
                        {
                            item.IsAllowManualPick = false;
                        }


                    }
                    else if (!string.IsNullOrEmpty(AddLoadPickExceptionSelectedItem.ItemId))
                    {
                        if (AddLoadPickSelectedItem == null)
                        {
                            var alertMessage = new Helpers.AlertWindow { Message = "Select reason for manual pick.", MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken); return;
                        }

                    }
                }
                if (AddLoadPickExceptionSelectedItem == null && AddLoadPickSelectedItem == null)
                {
                    var alertMessage = new Helpers.AlertWindow { Message = "Select Item for manual pick.", MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken); return;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel UpdateOnManualCountExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:UpdateOnManualCountExecute]\t");

        }
        public void HoldExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:HoldExecute]\t" + DateTime.Now + "");
            // Show popup
            MessageLine1 = "Are you sure you want to hold this transaction.";
            MessageLine2 = "Do you want to continue?";
            var dialog = new Helpers.DialogWindow { TemplateKey = "HoldPickItemAddLoad", Title = "Confirmation", Payload = null };
            Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:HoldExecute]\t");
        }
        public void PrintExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:PrintExecute]\t" + DateTime.Now + "");
            var confirmPrintWindow = new ConfirmWindow { OkButtonContent = "Yes", CancelButtonContent = "No", Message = "Do you want to print this transaction?", Header = "Print", MessageIcon = "Alert", Confirmed = false };
            Messenger.Default.Send<ConfirmWindow>(confirmPrintWindow, MessageToken);
            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
            //PrintAsync(confirmPrintWindow.Confirmed);
            ExecutePickPrint(confirmPrintWindow.Confirmed);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:PrintExecute]\t");
        }

        public void ExecutePickPrint(bool isConfirmPrintWindow)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:ExecutePickPrint]\t" + DateTime.Now + "");
            try
            {
                if (isConfirmPrintWindow)
                {
                    if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishAddLoadPickView.ToString())
                    {

                        new ReportViewModels.AddLoadPickReport(PickItems);
                        //if (addLoadPickSelectedItem != null)
                        //    ShowAddLoadPickReport(PickItems, addLoadPickSelectedItem);
                        if (addLoadPickSelectedItem == null)
                        {
                            ShowAddLoadPickReport(PickItems, AddLoadPrintPickSelectedItem);
                        }
                        else { ShowAddLoadPickReport(PickItems, addLoadPickSelectedItem); }
                    }
                    else if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishSuggestionPickView.ToString())
                    {
                        new ReportViewModels.LoadSuggetionsPickReport(PickItems);
                        //if (addLoadPickSelectedItem != null)
                        //    ShowSuggestionPickReport(PickItems, addLoadPickSelectedItem);
                        if (addLoadPickSelectedItem == null)
                        {
                            ShowSuggestionPickReport(PickItems, AddLoadPrintPickSelectedItem);
                        }
                        else { ShowSuggestionPickReport(PickItems, addLoadPickSelectedItem); }
                    }
                    else if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnPickView.ToString())
                    {
                        new ReportViewModels.SuggestionReturnPickReport(PickItems);
                        //if (addLoadPickSelectedItem != null)
                        //    ShowSuggestionReturnPickReport(PickItems, addLoadPickSelectedItem);
                        if (addLoadPickSelectedItem == null)
                        {
                            ShowSuggestionReturnPickReport(PickItems, AddLoadPrintPickSelectedItem);
                        }
                        else { ShowSuggestionReturnPickReport(PickItems, addLoadPickSelectedItem); }
                    }
                    else if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.HeldReturnPickView.ToString())
                    {
                        new ReportViewModels.HeldReturnPickReport(PickItems);
                        //if (addLoadPickSelectedItem != null)
                        //    ShowHeldReturnPickReport(PickItems, addLoadPickSelectedItem);

                        if (addLoadPickSelectedItem == null)
                        {
                            ShowHeldReturnPickReport(PickItems, AddLoadPrintPickSelectedItem);
                        }
                        else { ShowHeldReturnPickReport(PickItems, addLoadPickSelectedItem); }
                    }
                    else if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishUnloadPickView.ToString())
                    {
                      new ReportViewModels.UnloadPickReport(PickItems);
                       //if (addLoadPickSelectedItem != null)
                       //    ShowUnloadPickReport(PickItems, addLoadPickSelectedItem);
                       if (addLoadPickSelectedItem == null)
                       {
                           ShowUnloadPickReport(PickItems, AddLoadPrintPickSelectedItem);
                       }
                       else { ShowUnloadPickReport(PickItems, addLoadPickSelectedItem); }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel PrintAsync() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:ExecutePickPrint]\t");
        }

        //private async void PrintAsync(bool isConfirmPrintWindow)
        //{
        //    Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:PrintAsync]\t" + DateTime.Now + "");
        //    await Task.Run(() =>
        //    {
        //        ExecutePickPrint(isConfirmPrintWindow);
        //        Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:PrintAsync]\t");
        //    });
        //}

        public void ShowAddLoadPickReport(IList LoadPick, AddLoadPick addloadpick)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:ShowAddLoadPickReport]\t" + DateTime.Now + "");
            try
            {
                var DocInfo = new ReportEngine.DocumentInformation();
                var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                repEngine.Notify += repEngine_Notify;

                repEngine.Log = this.Logger;
                ReportEngine.IReportSLE ReportData = null;
                var DocumentId = addloadpick.DocumentID;
                var SourceDocumentNo = Convert.ToInt32(addloadpick.ReplnId);//Convert.ToDecimal(AddLoadPick.ReplnID);

                if (DocumentId <= 0)
                {
                    ReportData = new ReportViewModels.AddLoadPickReport(ReplenishAddLoadManager.GetAddLoadPickCollection(PayloadManager.RouteReplenishmentPayload.SelectedReplnId));
                    DocInfo = repEngine.Generate<Reporting.AddLoadPick>(SourceDocumentNo, true, ReportEngine.ReportCategory.Route, ReportEngine.ReportsTypes.AddLoadPickReport, ReportData, null, ReportEngine.ContentTypes.PDF);
                }
                else
                {
                    DocInfo.DocumentId = DocumentId;
                }

                if (DocInfo.IsDocumentCreated)
                    ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.Route_Replenishment_Header", "ReplenishmentID", SourceDocumentNo, DocInfo.DocumentId);
                addloadpick.DocumentID = DocInfo.DocumentId; //Replace Latest

                var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Route, addloadpick.DocumentID, null);

                if (ReportBytes == null)
                {
                    Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Report not generated, Please try again." }, MessageToken);
                    return;
                }
                //GenericReport.GenericReportViewModel grv = new GenericReport.GenericReportViewModel();
                //grv.ReportSource = ReportBytes;

                SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
                Payload.ReportFile = ReportBytes;
                Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                Payload.PreviousView = ViewModelMappings.View.ReplenishAddLoadPickView;
                Payload.CurrentViewTitle = "Add Load Pick Report";
                Payload.PreviousViewTitle = "Add Load Pick";
                Payload.MessageToken = this.MessageToken;

                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadPickView, CloseCurrentView = true, Payload = Payload, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel ShowReport() Error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:ShowAddLoadPickReport]\t");

        }
        public void ShowSuggestionPickReport(IList LoadPick, AddLoadPick Suggestionpick)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:ShowSuggestionPickReport]\t" + DateTime.Now + "");
            try
            {
                var DocInfo = new ReportEngine.DocumentInformation();
                var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                repEngine.Notify += repEngine_Notify;

                repEngine.Log = this.Logger;
                ReportEngine.IReportSLE ReportData = null;
                var DocumentId = Suggestionpick.DocumentID;
                var SourceDocumentNo = Convert.ToInt32(Suggestionpick.ReplnId);//Convert.ToDecimal(AddLoadPick.ReplnID);

                if (DocumentId <= 0)
                {
                    ReportData = new ReportViewModels.LoadSuggetionsPickReport(ReplenishAddLoadManager.GetAddLoadPickCollection(PayloadManager.RouteReplenishmentPayload.SelectedReplnId));
                    DocInfo = repEngine.Generate<Reporting.LoadSuggetionsPick>(SourceDocumentNo, true, ReportEngine.ReportCategory.Route, ReportEngine.ReportsTypes.LoadSuggetionsPickReport, ReportData, null, ReportEngine.ContentTypes.PDF);
                }
                else
                {
                    DocInfo.DocumentId = DocumentId;
                }

                if (DocInfo.IsDocumentCreated)
                    ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.Route_Replenishment_Header", "ReplenishmentID", SourceDocumentNo, DocInfo.DocumentId);
                Suggestionpick.DocumentID = DocInfo.DocumentId; //Replace Latest

                var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Route, Suggestionpick.DocumentID, null);


                //GenericReport.GenericReportViewModel grv = new GenericReport.GenericReportViewModel();
                //grv.ReportSource = ReportBytes;


                SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
                Payload.ReportFile = ReportBytes;
                Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                Payload.PreviousView = ViewModelMappings.View.ReplenishAddLoadPickView;
                Payload.CurrentViewTitle = "Suggestion Pick Report";
                Payload.PreviousViewTitle = "Suggestion Pick";
                Payload.MessageToken = this.MessageToken;



                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadPickView, CloseCurrentView = true, Payload = Payload, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][ShowSuggestionPickReport][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:ShowSuggestionPickReport]");

        }
        public void ShowSuggestionReturnPickReport(IList LoadPick, AddLoadPick SuggestionReturnpick)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:ShowSuggestionReturnPickReport]\t" + DateTime.Now + "");
            try
            {
                var DocInfo = new ReportEngine.DocumentInformation();
                var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                repEngine.Notify += repEngine_Notify;

                repEngine.Log = this.Logger;
                ReportEngine.IReportSLE ReportData = null;
                var DocumentId = SuggestionReturnpick.DocumentID;
                var SourceDocumentNo = Convert.ToInt32(SuggestionReturnpick.ReplnId);//Convert.ToDecimal(AddLoadPick.ReplnID);

                if (DocumentId <= 0)
                {
                    ReportData = new ReportViewModels.SuggestionReturnPickReport(ReplenishAddLoadManager.GetSuggReturnPickCollection(PayloadManager.RouteReplenishmentPayload.SelectedReplnId));
                    DocInfo = repEngine.Generate<Reporting.SuggestionReturnPick>(SourceDocumentNo, true, ReportEngine.ReportCategory.Route, ReportEngine.ReportsTypes.SuggestionReturnPickReport, ReportData, null, ReportEngine.ContentTypes.PDF);
                }
                else
                {
                    DocInfo.DocumentId = DocumentId;
                }

                if (DocInfo.IsDocumentCreated)
                    ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.Route_Replenishment_Header", "ReplenishmentID", SourceDocumentNo, DocInfo.DocumentId);
                SuggestionReturnpick.DocumentID = DocInfo.DocumentId; //Replace Latest

                var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Route, SuggestionReturnpick.DocumentID, null);


                //GenericReport.GenericReportViewModel grv = new GenericReport.GenericReportViewModel();
                //grv.ReportSource = ReportBytes;


                SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
                Payload.ReportFile = ReportBytes;
                Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                Payload.PreviousView = ViewModelMappings.View.ReplenishAddLoadPickView;
                Payload.CurrentViewTitle = "Suggestion Return Pick Report";
                Payload.PreviousViewTitle = "Suggestion Return Pick";
                Payload.MessageToken = this.MessageToken;



                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadPickView, CloseCurrentView = true, Payload = Payload, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][ShowSuggestionReturnPickReport][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:ShowSuggestionReturnPickReport]");

        }
        public void ShowHeldReturnPickReport(IList LoadPick, AddLoadPick HeldReturnPick)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:ShowHeldReturnPickReport]");
            try
            {
                var DocInfo = new ReportEngine.DocumentInformation();
                var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                repEngine.Notify += repEngine_Notify;

                repEngine.Log = this.Logger;
                ReportEngine.IReportSLE ReportData = null;
                var DocumentId = HeldReturnPick.DocumentID;
                var SourceDocumentNo = Convert.ToInt32(HeldReturnPick.ReplnId);//Convert.ToDecimal(AddLoadPick.ReplnID);

                if (DocumentId <= 0)
                {
                    ReportData = new ReportViewModels.HeldReturnPickReport(ReplenishAddLoadManager.GetHeldReturnPickCollection(PayloadManager.RouteReplenishmentPayload.SelectedReplnId));
                    DocInfo = repEngine.Generate<Reporting.HeldReturnPick>(SourceDocumentNo, true, ReportEngine.ReportCategory.Route, ReportEngine.ReportsTypes.HeldReturnPickReport, ReportData, null, ReportEngine.ContentTypes.PDF);
                }
                else
                {
                    DocInfo.DocumentId = DocumentId;
                }

                if (DocInfo.IsDocumentCreated)
                    ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.Route_Replenishment_Header", "ReplenishmentID", SourceDocumentNo, DocInfo.DocumentId);
                HeldReturnPick.DocumentID = DocInfo.DocumentId; //Replace Latest

                var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Route, HeldReturnPick.DocumentID, null);


                //GenericReport.GenericReportViewModel grv = new GenericReport.GenericReportViewModel();
                //grv.ReportSource = ReportBytes;


                SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
                Payload.ReportFile = ReportBytes;
                Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                Payload.PreviousView = ViewModelMappings.View.ReplenishAddLoadPickView;
                Payload.CurrentViewTitle = "Held Return Pick Report";
                Payload.PreviousViewTitle = "Held Return Pick";
                Payload.MessageToken = this.MessageToken;



                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadPickView, CloseCurrentView = true, Payload = Payload, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][ShowHeldReturnPickReport][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:ShowHeldReturnPickReport]");

        }
        public void ShowUnloadPickReport(IList LoadPick, AddLoadPick UnloadPick)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:ShowUnloadPickReport]");
            try
            {
                var DocInfo = new ReportEngine.DocumentInformation();
                var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                repEngine.Notify += repEngine_Notify;

                repEngine.Log = this.Logger;
                ReportEngine.IReportSLE ReportData = null;
                var DocumentId = UnloadPick.DocumentID;
                var SourceDocumentNo = Convert.ToInt32(UnloadPick.ReplnId);//Convert.ToDecimal(AddLoadPick.ReplnID);

                if (DocumentId <= 0)
                {
                    ReportData = new ReportViewModels.UnloadPickReport(ReplenishAddLoadManager.GetUnloadPickCollection(PayloadManager.RouteReplenishmentPayload.SelectedReplnId));
                    DocInfo = repEngine.Generate<Reporting.UnloadPick>(SourceDocumentNo, true, ReportEngine.ReportCategory.Route, ReportEngine.ReportsTypes.UnloadPickReport, ReportData, null, ReportEngine.ContentTypes.PDF);
                }
                else
                {
                    DocInfo.DocumentId = DocumentId;
                }

                if (DocInfo.IsDocumentCreated)
                    ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.Route_Replenishment_Header", "ReplenishmentID", SourceDocumentNo, DocInfo.DocumentId);
                UnloadPick.DocumentID = DocInfo.DocumentId; //Replace Latest

                var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Route, UnloadPick.DocumentID, null);


                //GenericReport.GenericReportViewModel grv = new GenericReport.GenericReportViewModel();
                //grv.ReportSource = ReportBytes;


                SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
                Payload.ReportFile = ReportBytes;
                Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                Payload.PreviousView = ViewModelMappings.View.ReplenishAddLoadPickView;
                Payload.CurrentViewTitle = "Unload Pick Report";
                Payload.PreviousViewTitle = "Unload Pick";
                Payload.MessageToken = this.MessageToken;



                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadPickView, CloseCurrentView = true, Payload = Payload, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][ShowUnloadPickReport][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:ShowUnloadPickReport]");

        }


        void repEngine_Notify(string msg)
        {
            System.Windows.MessageBox.Show(msg);
        }

        public void HoldYesExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:HoldYesExecute]\t" + DateTime.Now + "");

            try
            {
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                IsDirty = false;
                // Navigate to replenishment tab
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadPickView, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = ViewModelMappings.TabView.RouteHome.Replenishment };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel HoldYesExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:HoldYesExecute]\t");

        }
        public void HoldNoExecute()
        {
            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
        }
        #endregion

        #region Constructor & Methods
        public ReplenishAddLoadPickViewModel()
        {
            InitialiseCommands();
            InitialiseVariables();
        }
        public void InitialiseVariables()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:InitialiseVariables]\t" + DateTime.Now + "");

            try
            {
                RouteNameAddLoad = PayloadManager.RouteReplenishmentPayload.RouteNameAddLoad;
                SelectedTab = ViewModelMappings.TabView.RouteHome.Replenishment;
                ReplenishID = PayloadManager.RouteReplenishmentPayload.SelectedReplnId;
                ShowShippedQty = (!PayloadManager.RouteReplenishmentPayload.IsSalesBranch && PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck && (PayloadManager.RouteReplenishmentPayload.Status.Equals("CMPLT") || PayloadManager.RouteReplenishmentPayload.Status.Equals("HOP") || PayloadManager.RouteReplenishmentPayload.Status.Equals("RTP")));
                SetTypeCodes();

                Managers.PickManager pickManager = new PickManager();
                ManualPickReasonList = pickManager.GetReasonListForManualPick();

                if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishAddLoadPickView.ToString())
                {
                    ReplenishIDName = "Add Load ID. :" + ReplenishID;
                    PrimaryQtyHeader = "Load Quantity";
                    ObservableCollection<AddLoadPick> AddLoadPickCollectionTemp = ReplenishAddLoadManager.GetAddLoadPickCollection(PayloadManager.RouteReplenishmentPayload.SelectedReplnId);
                    GetPickCollection(AddLoadPickCollectionTemp, PickItemManager.PickForTransaction.AddLoad);
                }
                else if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishSuggestionPickView.ToString())
                {
                    ReplenishIDName = "Suggestion Repln. ID. :" + ReplenishID;
                    PrimaryQtyHeader = "Repln. Quantity";
                    ObservableCollection<AddLoadPick> AddLoadPickCollectionTemp = ReplenishAddLoadManager.GetAddLoadPickCollection(PayloadManager.RouteReplenishmentPayload.SelectedReplnId);
                    GetPickCollection(AddLoadPickCollectionTemp, PickItemManager.PickForTransaction.Suggestion);
                }
                else if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnPickView.ToString())
                {
                    ReplenishIDName = "Suggestion Return ID. :" + ReplenishID;
                    PrimaryQtyHeader = "Return Quantity";
                    ObservableCollection<AddLoadPick> AddLoadPickCollectionTemp = ReplenishAddLoadManager.GetSuggReturnPickCollection(PayloadManager.RouteReplenishmentPayload.SelectedReplnId);
                    GetPickCollection(AddLoadPickCollectionTemp, PickItemManager.PickForTransaction.SuggestionReturn, true);
                }
                else if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.HeldReturnPickView.ToString())
                {
                    ReplenishIDName = "Held Return ID. :" + ReplenishID;
                    PrimaryQtyHeader = "Return Quantity"; IsVisibleHeldQty = true; IsHeldReturn = true; IsVisibleAdjustQty = false;
                    ObservableCollection<AddLoadPick> AddLoadPickCollectionTemp = ReplenishAddLoadManager.GetHeldReturnPickCollection(PayloadManager.RouteReplenishmentPayload.SelectedReplnId);
                    GetPickCollection(AddLoadPickCollectionTemp, PickItemManager.PickForTransaction.HeldReturn);
                }
                else if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishUnloadPickView.ToString())
                {
                    ReplenishIDName = "Unload ID. :" + ReplenishID;
                    PrimaryQtyHeader = "Return Quantity";
                    ObservableCollection<AddLoadPick> AddLoadPickCollectionTemp = ReplenishAddLoadManager.GetUnloadPickCollection(PayloadManager.RouteReplenishmentPayload.SelectedReplnId);
                    GetPickCollection(AddLoadPickCollectionTemp, PickItemManager.PickForTransaction.Unload, true);
                }
                EnableDisableCompletePick();
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel InitialiseVariables() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:InitialiseVariables]\t");


        }

        public void GetPickCollection(ObservableCollection<AddLoadPick> PickCollectionTemp, SalesLogicExpress.Application.Managers.PickItemManager.PickForTransaction TransactionTypeID, bool UpdateCommittedQtyInInventoryOnFirstLoad = false)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:GetPickCollection]\t" + DateTime.Now + "");

            try
            {

                List<dynamic> items = new System.Collections.Generic.List<dynamic>();
                foreach (var CC_Item in PickCollectionTemp)
                {
                    items.Add(CC_Item);
                }

                pickMgr = new PickItemManager(CommonNavInfo.RouteID.ToString(), PayloadManager.RouteReplenishmentPayload.SelectedReplnId, TransactionTypeID, !PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck, UpdateCommittedQtyInInventoryOnFirstLoad);
                pickMgr.PickItemUpdate += pickMgr_PickItemUpdate;
                pickMgr.PickUpdate += pickMgr_PickUpdate;

                //below if statement is coded for primay uom based picking qty affect
                if (!PayloadManager.RouteReplenishmentPayload.NavigatedForVoid)
                {
                    foreach (var item in items)
                    {
                        if (item.PrimaryUM != item.TransactionUOM && item.PickQtyPrimaryUOM > 0)
                        {
                            double convFactor = UoMManager.GetUoMFactor(item.UM, item.PrimaryUM, Convert.ToInt32(item.ItemId.Trim()), item.ItemNumber.Trim());
                            item.TransactionQty = convFactor * item.TransactionQty;
                            //item.TransactionQtyDisplay = item.TransactionQty;
                            item.ConversionFactor = 1;
                        }
                    }
                }
                Dictionary<PickItemManager.PickType, ObservableCollection<dynamic>> pickList = pickMgr.CreatePickList(items);
                PickItemsException = pickList[PickItemManager.PickType.Exception];
                PickItemsException.CollectionChanged += PickItemsException_CollectionChanged;


                if (!PayloadManager.RouteReplenishmentPayload.NavigatedForVoid)
                {
                    IsDirty = true;
                    IsEnableHold = true;
                    PickItems = pickList[PickItemManager.PickType.Normal];
                }
                else
                {
                    PickItems = pickList[PickItemManager.PickType.Normal];
                    PickItemsException = pickList[PickItemManager.PickType.Exception];
                    pickMgr.VoidPick(PayloadManager.RouteReplenishmentPayload.VoidReasonID); // TODO : Replace this hardcoded value from the one which was selected during void
                    CanNavigate = false;
                    IsEnableHold = false;
                    IsEnableRemoveException = false;
                    IsEnableManualPick = true;
                    foreach (AddLoadPick item in PickItemsException)
                    {
                        item.IsEnableScannerInException = true;
                    }
                }

                //below if statement is coded for primay uom based picking qty affect
                if (!PayloadManager.RouteReplenishmentPayload.NavigatedForVoid)
                {
                    foreach (var item in PickItems)
                    {
                        if (item.PrimaryUM != item.TransactionUOM && item.PickedQty == 0)
                        {
                            double convFactor = UoMManager.GetUoMFactor(item.UM, item.PrimaryUM, Convert.ToInt32(item.ItemId.Trim()), item.ItemNumber.Trim());
                            item.TransactionQtyPrimaryUOM = convFactor * item.TransactionQty;
                            item.TransactionQtyDisplay = item.TransactionQty;
                            item.TransactionQty = convFactor * item.TransactionQty;
                            //item.TransactionQtyDisplay = convFactor * item.TransactionQty;
                            item.ConversionFactor = 1;
                        }
                    }
                }

                AddLoadPrintPickSelectedItem = (PickCollectionTemp != null) ? PickCollectionTemp.First() : null;
                PickCollectionTemp.Clear();

                if (!PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck)
                {
                    EnableDisableScanner();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel GetPickCollection() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:GetPickCollection]\t");

        }

        private void EnableDisableScanner()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:EnableDisableScanner]\t" + DateTime.Now + "");

            try
            {
                foreach (var item in PickItems)
                {
                    //Disable scanner when available qty is 0 AND item is Short adjusted
                    if (item.PrimaryUM == item.TransactionUOM)
                    {
                        if ((item.AvailableQty == 0 && PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.HeldReturnPickView.ToString())
                            || item.ExceptionReasonCode == "Short")
                        {
                            item.IsAllowManualPick = false; item.IsEnableScanner = false;
                        }
                        else if (item.ExceptionReasonCode != "Short")
                        {
                            item.IsEnableScanner = true;
                        }
                    }
                    else if (item.PrimaryUM != item.TransactionUOM)
                    {
                        if (item.AvailableQty < Convert.ToInt32(item.ConversionFactor)
                            || (item.AvailableQty == 0 && PayloadManager.RouteReplenishmentPayload.CurrentViewName != ViewModelMappings.View.HeldReturnPickView.ToString()) || item.ExceptionReasonCode == "Short")
                        {
                            item.IsAllowManualPick = false; item.IsEnableScanner = false;

                        }
                        else if (item.ExceptionReasonCode != "Short")
                        {
                            item.IsEnableScanner = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel EnableDisableScanner() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:EnableDisableScanner]\t");


        }

        void PickItemsException_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:PickItemsException_CollectionChanged]\t" + DateTime.Now + "");

            try
            {
                if (PayloadManager.RouteReplenishmentPayload.NavigatedForVoid)
                {
                    IsEnableRemoveException = false;
                    return;
                }
                foreach (dynamic item in PickItemsException)
                {
                    item.IsEnableScannerInException = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel PickItemsException_CollectionChanged() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:PickItemsException_CollectionChanged]\t");


        }

        void pickMgr_PickUpdate(object sender, PickItemManager.PickEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:pickMgr_PickUpdate]\t" + DateTime.Now + "");

            try
            {
                EnableDisableCompletePick();
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel pickMgr_PickUpdate() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:pickMgr_PickUpdate]\t");

        }

        void pickMgr_PickItemUpdate(object sender, PickItemManager.PickItemEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:pickMgr_PickItemUpdate]\t" + DateTime.Now + "");

            try
            {
                EnableDisableCompletePick();
                //Update commited quantity for item in case of return item in DC and Unmanned


                string committedQty = Convert.ToString(e.Item.CommittedQty);
                string heldQty = Convert.ToString(e.Item.HeldQty);
                string ItemID = Convert.ToString(e.Item.ItemId);

                if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnPickView.ToString()
                    || PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishUnloadPickView.ToString())
                {
                    //ReplenishAddLoadManager.UpdateAvailableInventory(committedQty,  ItemID);
                }
                else if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.HeldReturnPickView.ToString())
                {
                    ReplenishAddLoadManager.UpdateCommitedInventory(committedQty, heldQty, ItemID);
                }

            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel pickMgr_PickItemUpdate() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:pickMgr_PickItemUpdate]\t");

        }

        //public void CanRemoveException()
        //{
        //    //if (PickItemsException.Count == 0)
        //    //{
        //    //    CanRemoveExceptions = false; return;
        //    //}
        //    //foreach (AddLoadPick item in PickItems)
        //    //{
        //    //    if (item.PickQtyPrimaryUOM < item.TransactionQtyPrimaryUOM)
        //    //    {
        //    //        CanRemoveExceptions = false;
        //    //        break;
        //    //    }
        //    //}
        //}
        public void SetTypeCodes()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:SetTypeCodes]\t" + DateTime.Now + "");

            try
            {
                APickID = RouteReplenishmentTabManagercs.GetStatusTypeCode("APICK").ToString();
                CPickID = RouteReplenishmentTabManagercs.GetStatusTypeCode("CPICK").ToString();
                OPickID = RouteReplenishmentTabManagercs.GetStatusTypeCode("OPICK").ToString();
                SPickID = RouteReplenishmentTabManagercs.GetStatusTypeCode("SPICK").ToString();
                UPickID = RouteReplenishmentTabManagercs.GetStatusTypeCode("UPICK").ToString();
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel SetTypeCodes() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:SetTypeCodes]\t");


        }

        public void EnableDisableCompletePick()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:EnableDisableCompletePick]\t" + DateTime.Now + "");

            try
            {
                IsEnableCompletePick = false;
                IsEnablePrint = false;
                if (PickItemsException.Count == 0)
                {
                    AddLoadPick obj = PickItems.FirstOrDefault(x => x.ExceptionReasonCode.ToString() == "UnderPicked" || x.ExceptionReasonCode.ToString() == "OverPicked");
                    if (obj == null)
                    {
                        IsEnableCompletePick = true;
                        IsEnablePrint = true;
                    }
                    else
                    {
                        IsEnablePrint = false;
                        IsEnableCompletePick = false;
                    }
                }
                if (AddLoadPickSelectedItem != null)
                {
                    IsEnableShort = AddLoadPickSelectedItem.ExceptionReasonCode == "UnderPicked" ? true : false;
                    IsEnableManualPick = (AddLoadPickSelectedItem.ExceptionReasonCode != "Short") ? true : false;
                    if (!PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck)
                    {
                        EnableDisableScannerForSelectedItem();
                    }
                }
                //ManuallyPickCountOfSelected = 0;
                ManuallyPickCountOfSelected = AddLoadPickSelectedItem != null ? Convert.ToInt32(AddLoadPickSelectedItem.TransactionQty) : (AddLoadPickExceptionSelectedItem != null) ? Convert.ToInt32(AddLoadPickExceptionSelectedItem.ExceptionCount) : 0;
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel EnableDisableCompletePick() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:EnableDisableCompletePick]\t");

        }

        private void EnableDisableScannerForSelectedItem()
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:EnableDisableScannerForSelectedItem]\t" + DateTime.Now + "");

            try
            {
                //Disable scanner when available qty is 0 AND item is Short adjusted
                #region Enable disable scanner
                if (AddLoadPickSelectedItem.PrimaryUM == AddLoadPickSelectedItem.TransactionUOM)
                {
                    if (AddLoadPickSelectedItem.AvailableQty == 0 || AddLoadPickSelectedItem.ExceptionReasonCode == "Short")
                    {
                        AddLoadPickSelectedItem.IsAllowManualPick = false;
                        AddLoadPickSelectedItem.IsEnableScanner = false;
                    }
                    else if (AddLoadPickSelectedItem.ExceptionReasonCode != "Short")
                    {
                        AddLoadPickSelectedItem.IsEnableScanner = true;
                        //IsEnableManualPick = true;
                    }
                }
                else if (AddLoadPickSelectedItem.PrimaryUM != AddLoadPickSelectedItem.TransactionUOM)
                {
                    if (AddLoadPickSelectedItem.AvailableQty < Convert.ToInt32(AddLoadPickSelectedItem.ConversionFactor) || (AddLoadPickSelectedItem.AvailableQty == 0) || AddLoadPickSelectedItem.ExceptionReasonCode == "Short")
                    {
                        AddLoadPickSelectedItem.IsAllowManualPick = false;
                        //IsEnableManualPick = false; 
                        AddLoadPickSelectedItem.IsEnableScanner = false;

                    }
                    else if (AddLoadPickSelectedItem.ExceptionReasonCode != "Short")
                    {
                        AddLoadPickSelectedItem.IsEnableScanner = true;
                        //IsEnableManualPick = true;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel EnableDisableScannerForSelectedItem() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:EnableDisableScannerForSelectedItem]\t");

        }

        public override bool ConfirmSave()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:ConfirmSave]\t" + DateTime.Now + "");
            IsDirty = false;
            CanNavigate = true;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:ConfirmSave]\t");
            return base.ConfirmSave();   
        }

        public override bool ConfirmCancel()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:ConfirmCancel]\t" + DateTime.Now + "");
            CanNavigate = false;
            IsDirty = true;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:ConfirmCancel]\t");
            return base.ConfirmCancel();
        }

        public override bool ConfirmClosed()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:ConfirmClosed]\t" + DateTime.Now + "");
            IsDirty = true;
            CanNavigate = false;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:ConfirmClosed]\t");
            return base.ConfirmClosed();
        }

        public void ClearFlagVarForManualPick()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][Start:ClearFlagVarForManualPick]\t" + DateTime.Now + "");
            OldManualPickCount = 0;
            NewManualPickCount = 0;
            ChkManualPickSameOrder = 0;
            ChkManualPickSameItem = "";
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadPickViewModel][End:ClearFlagVarForManualPick]\t");
        }
        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }

    }


}
