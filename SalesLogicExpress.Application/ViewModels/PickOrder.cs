﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Ink;
using System.Windows.Threading;
using Telerik.Windows.Controls;
using Models = SalesLogicExpress.Domain;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;

namespace SalesLogicExpress.Application.ViewModels
{
    public class PickOrder : ViewModelBase, IDataErrorInfo
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.PickOrder");
        private InventoryManager inventoryMgr = new InventoryManager();
        OrderManager objOrderManger = new OrderManager();
        #region DelegateCommand
        public DelegateCommand ScanOneByOneAdd { get; set; }
        public DelegateCommand ScanOneByOneRemoveFromGrid { get; set; }
        public DelegateCommand ScanByDevice { get; set; }
        public DelegateCommand HoldPickList { get; set; }
        public DelegateCommand ManualPick { get; set; }
        public DelegateCommand GetManualPickReasonList { get; set; }
        public DelegateCommand RemoveExceptions { get; set; }
        public DelegateCommand DeliverToCustomer { get; set; }
        public DelegateCommand SwitchScannerMode { get; set; }
        public DelegateCommand SetManualPickReason { get; set; }
        public DelegateCommand VoidOrder { get; set; }
        public DelegateCommand SetPickItemLabels { get; set; }
        public DelegateCommand ChargeOnAccount { get; set; }
        public DelegateCommand AcceptChargeOnAccount { get; set; }
        public DelegateCommand ShowPickSlipReport { get; set; }
        public DelegateCommand ShowInvoiceReport { get; set; }
        public DelegateCommand DeclineChargeOnAccount { get; set; }
        public DelegateCommand ClearSignInChargeOnAccount { get; set; }
        public ReasonCode selectedReason;
        #endregion

        #region Logging
        void LogActivity(ActivityKey key)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:LogActivity]");
            LogActivity(key, false);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:LogActivity]");
        }
        void LogActivity(ActivityKey key, bool logEndTime)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:asyncLogActivity]");
            //await Task.Factory.StartNew(() =>
            //{
            try
            {
                PayloadManager.OrderPayload.OrderID = Order.OrderId.ToString();
                PayloadManager.OrderPayload.InvoiceNo = Order.OrderId.ToString();
                if (key == ActivityKey.HoldAtCashCollection)
                {
                    PayloadManager.OrderPayload.CashDelivery = CashDeliveryContext;
                }
                SalesLogicExpress.Domain.Activity ac = new SalesLogicExpress.Domain.Activity
                {
                    ActivityFlowID = Convert.ToInt32(PayloadManager.OrderPayload.OrderID),
                    ActivityHeaderID = PayloadManager.OrderPayload.TransactionID,
                    CustomerID = PayloadManager.OrderPayload.Customer.CustomerNo,
                    RouteID = PayloadManager.OrderPayload.RouteID,
                    ActivityType = key.ToString(),
                    ActivityStart = DateTime.Now,
                    //ActivityEnd = logEndTime ? DateTime.Now : DateTime.MinValue,
                    IsTxActivity = true,
                    ActivityDetailClass = PayloadManager.OrderPayload.ToString(),
                    ActivityDetails = PayloadManager.OrderPayload.SerializeToJson()
                };
                if (key == ActivityKey.PickComplete)
                {
                    ac.ActivityHeaderID = PayloadManager.OrderPayload.PickOrderTransactionID;
                }
                if (key == ActivityKey.PickOrder)
                {
                    ac.ActivityHeaderID = PayloadManager.OrderPayload.TransactionID;
                }
                if (key == ActivityKey.PickItem)
                {
                    ac.ActivityHeaderID = PayloadManager.OrderPayload.PickOrderTransactionID;
                    ac.ActivityDetailClass = "SalesLogicExpress.Domain.PickOrderItem";
                    ac.ActivityDetails = SelectedPickItem.SerializeToJson();
                }
                if (key == ActivityKey.HoldAtPick || key == ActivityKey.HoldAtCashCollection || key == ActivityKey.VoidAtCashCollection)
                {
                    ac.ActivityEnd = DateTime.Now;
                }
                switch (key)
                {
                    case ActivityKey.HoldAtPick:
                        PayloadManager.OrderPayload.TransactionLastState = ActivityKey.HoldAtPick.ToString();
                        break;
                    case ActivityKey.HoldAtCashCollection:
                        PayloadManager.OrderPayload.TransactionLastState = ActivityKey.HoldAtCashCollection.ToString();
                        break;
                    case ActivityKey.VoidAtCashCollection:
                        PayloadManager.OrderPayload.TransactionLastState = ActivityKey.VoidAtCashCollection.ToString();
                        break;
                }
                //ac.ActivityDetails = PayloadManager.OrderPayload.SerializeToJson();
                SalesLogicExpress.Domain.Activity a = ResourceManager.Transaction.LogActivity(ac);
                if (key == ActivityKey.HoldAtPick || key == ActivityKey.HoldAtCashCollection || key == ActivityKey.VoidAtCashCollection)
                {
                    PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.Order);
                }
                if (a != null && a.ActivityHeaderID != null)
                {
                    if (key == ActivityKey.PickItem)
                    {
                    }
                    if (key == ActivityKey.PickOrder || key == ActivityKey.PickComplete)
                    {
                        PayloadManager.OrderPayload.PickOrderTransactionID = a.ActivityID;
                    }
                    //PayloadManager.OrderPayload.TransactionID = a.ActivityHeaderID;
                }
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][asyncLogActivity][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:asyncLogActivity]");
            //});
        }
        void LogPaymentActivity(ActivityKey key, string receiptId)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:LogPaymentActivity]");

                CashDelivery cd = new CashDelivery();
                cd.ChequeDate = Convert.ToDateTime(CashDeliveryContext.ChequeDate, System.Globalization.CultureInfo.InvariantCulture);
                cd.ChequeNo = CashDeliveryContext.ChequeNo;
                cd.PaymentAmount = CashDeliveryContext.PaymentAmount;
                cd.PaymentMode = string.IsNullOrEmpty(CashDeliveryContext.ChequeNo) ? false : true;
                cd.ChequeNoId = CashDeliveryContext.ChequeNoId;

                PayloadManager.PaymentPayload.InvoiceNo.Add(Order.OrderId.ToString());
                PayloadManager.PaymentPayload.CashDelivery = cd;
                PayloadManager.PaymentPayload.Customer = CommonNavInfo.Customer;
                PayloadManager.PaymentPayload.RouteID = PayloadManager.OrderPayload.RouteID;

                //PayloadManager.PaymentPayload.TransactionID = PayloadManager.OrderPayload.TransactionID; 

                if (!string.IsNullOrEmpty(receiptId))
                    PayloadManager.PaymentPayload.TransactionID = "receipt#" + receiptId;

                SalesLogicExpress.Domain.Activity ac = new SalesLogicExpress.Domain.Activity
                {
                    ActivityFlowID = Convert.ToInt32(PayloadManager.OrderPayload.OrderID),
                    ActivityHeaderID = PayloadManager.OrderPayload.TransactionID,
                    CustomerID = PayloadManager.PaymentPayload.Customer.CustomerNo,
                    RouteID = PayloadManager.PaymentPayload.RouteID,
                    StopInstanceID = PayloadManager.PaymentPayload.Customer.StopID,
                    ActivityType = key.ToString(),
                    ActivityStart = DateTime.Now,
                    ActivityEnd = DateTime.Now,
                    IsTxActivity = true,
                    ActivityDetailClass = PayloadManager.PaymentPayload.ToString(),
                    ActivityDetails = PayloadManager.PaymentPayload.SerializeToJson()
                };
                SalesLogicExpress.Domain.Activity a = ResourceManager.Transaction.LogActivity(ac);
                PayloadManager.PaymentPayload.TransactionID = a.ActivityHeaderID;
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][LogPaymentActivity][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:LogPaymentActivity]");
        }
        #endregion

        #region Contructors

        public PickOrder()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:Contructors]");
            try
            {
                IsBusy = true;
                InitializeCommands();
                PickOrderItems.ItemPropertyChanged -= PickOrderItems_ItemPropertyChanged;
                PickOrderItems.ItemPropertyChanged += PickOrderItems_ItemPropertyChanged;
                selectedReason = new ReasonCode("");
                pickManager = new Managers.PickManager();


                if (PayloadManager.OrderPayload.PickOrderTransactionID != null)
                {
                    //UnHOLD Order if HOLD 
                    pickManager.HoldUnHoldPickList(Order.OrderId.ToString(), false);
                    string tempStopDate = string.Empty;
                    string stopID = string.Empty;
                    if (PayloadManager.OrderPayload.StopDate.Date > DateTime.Now.Date || PayloadManager.OrderPayload.StopDate.Date < DateTime.Now.Date)
                    {
                        tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                        stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.OrderPayload.Customer.CustomerNo, tempStopDate);
                    }
                    else
                    {
                        tempStopDate = PayloadManager.OrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                        stopID = PayloadManager.OrderPayload.Customer.StopID;
                    }
                    new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.OrderPayload.Customer.CustomerNo);
                }

                if (PayloadManager.OrderPayload.PickOrderTransactionID == null)
                {
                    LogActivity(ActivityKey.PickOrder);
                }
                if (PayloadManager.OrderPayload.TransactionLastState == ActivityKey.HoldAtCashCollection.ToString())
                {
                    CanOpenCashCollection = true;
                    CashDeliveryContext = PayloadManager.OrderPayload.CashDelivery;
                }
                if (PayloadManager.OrderPayload.TransactionLastState != null && (PayloadManager.OrderPayload.TransactionLastState == ActivityKey.HoldAtPick.ToString() || PayloadManager.OrderPayload.TransactionLastState == ActivityKey.HoldAtCashCollection.ToString()))
                {
                    PayloadManager.ApplicationPayload.StartActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.Order);
                    PayloadManager.OrderPayload.TransactionLastState = null;
                }

                SetScannerModeByProfile();
                LoadData();
            }
            catch (Exception e)
            {
                Logger.Error("SalesLogicExpress.ViewModels.PickOrder, PickOrder()  Message: " + e.Message);
            }
            IsBusy = false;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:Contructors]");
        }

        async void LoadData()
        {
            await Task.Factory.StartNew(() =>
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.DataBind,
                  new Action(() =>
                  {
                      Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:asyncLoadData]");
                      try
                      {
                          pickManager = new Managers.PickManager();
                          ObservableCollection<Models.OrderItem> orderItemsCollectionFromOrder = new ObservableCollection<Models.OrderItem>(PayloadManager.OrderPayload.Items);
                          ObservableCollection<Models.PickOrderItem> PickorderItemsCollectionFromDB = pickManager.GetPickItemsFromDB(Order.OrderId.ToString());
                          ObservableCollection<Models.PickOrderItem> PickorderItemsCollectionFromOrder = new ObservableCollection<PickOrderItem>();

                          foreach (Models.OrderItem orderItem in orderItemsCollectionFromOrder)
                          {
                              //Commented by dinesh - Junk code

                              //var uomList = UoMManager.ItemUoMFactorList;
                              //if (uomList != null)
                              //{
                              //    var item = orderItem;
                              //    try
                              //    {
                              //        var itemUomConversion = uomList.FirstOrDefault(x => (x.ItemID == item.ItemId.Trim()) && (x.FromUOM == item.UM.ToString()) && (x.ToUOM == item.PrimaryUM));
                              //        if (itemUomConversion == null)
                              //            orderItem.UMConversionFactor = UoMManager.GetUoMFactor(item.UM.ToString(), item.PrimaryUM, Convert.ToInt32(item.ItemId.Trim()), item.ItemNumber.Trim());
                              //        else
                              //            orderItem.UMConversionFactor = itemUomConversion.ConversionFactor;
                              //    }
                              //    catch (Exception e1)
                              //    {
                              //        Logger.Error("SalesLogicExpress.ViewModels.PickOrder, Load UOM Factor From List, Message: " + e1.Message);
                              //    }
                              //}
                              //else
                              //{
                              try
                              {
                                  orderItem.UMConversionFactor = UoMManager.GetUoMFactor(orderItem.UM, orderItem.PrimaryUM, Convert.ToInt32(orderItem.ItemId.Trim()), orderItem.ItemNumber.Trim());
                              }
                              catch (Exception e1)
                              {
                                  Logger.Error("SalesLogicExpress.ViewModels.PickOrder, Load UOM Factor From UOM function, Message: " + e1.Message);
                              }
                              //}
                              PickOrderItem pickOrderItem = new Models.PickOrderItem(orderItem);
                              PickorderItemsCollectionFromOrder.Add(pickOrderItem);
                          }

                          bool IsPickListExists = pickManager.IsPickListCreated(Order.OrderId.ToString());
                          string PickStatus = pickManager.GetPickListStatus(Order.OrderId.ToString());
                          List<string> itemsToDelete = new List<string>();
                          ExceptionItems = pickManager.GetExceptionList(Order.OrderId.ToString());
                          if (IsPickListExists && PickStatus != "Hold")
                          {
                              foreach (Models.PickOrderItem item in PickorderItemsCollectionFromOrder)
                              {
                                  // New item added from Order
                                  if (!PickorderItemsCollectionFromDB.Any(i => i.ItemNumber == item.ItemNumber))
                                  {
                                      if (ExceptionItems.Count > 0)
                                      {
                                          PickOrderItem exItem = ExceptionItems.FirstOrDefault(i => i.ItemNumber.Trim() == item.ItemNumber.Trim());
                                          if (exItem != null)
                                          {
                                              item.ExceptionReason = item.OrderQty < exItem.ExceptionQtyInOrderUOM ? "Over Picked" : "Valid";
                                              if (item.OrderQty < exItem.ExceptionQtyInOrderUOM)
                                              {
                                                  item.PickedQuantityInPrimaryUOM = exItem.PickedQuantityInPrimaryUOM = exItem.ExceptionQtyInPrimaryUOM;
                                                  item.ExceptionQtyInPrimaryUOM = exItem.ExceptionQtyInPrimaryUOM;
                                                  pickManager.SaveException(Order.OrderId.ToString(), item, CommonNavInfo.RouteID.ToString());
                                              }
                                              if (item.OrderQty >= exItem.ExceptionQtyInOrderUOM)
                                              {
                                                  item.PickedQuantityInPrimaryUOM = exItem.PickedQuantityInPrimaryUOM = exItem.ExceptionQtyInPrimaryUOM;
                                                  pickManager.DeleteException(Order.OrderId.ToString(), item);
                                              }
                                          }
                                      }
                                      pickManager.InsertPickItem(Order.OrderId.ToString(), item, CommonNavInfo.RouteID.ToString());
                                  }
                              }

                              foreach (Models.PickOrderItem item in PickorderItemsCollectionFromDB)
                              {
                                  // Existing item deleted from Order
                                  if (!PickorderItemsCollectionFromOrder.Any(i => i.ItemNumber == item.ItemNumber))
                                  {
                                      itemsToDelete.Add(item.ItemNumber);
                                      if (item.PickedQuantityInPrimaryUOM != 0)
                                      {
                                          item.ExceptionReason = "Incorrect Item";
                                          item.ExceptionQtyInPrimaryUOM = item.PickedQuantityInPrimaryUOM;
                                          pickManager.SaveException(Order.OrderId.ToString(), item, CommonNavInfo.RouteID.ToString());
                                      }
                                  }

                                  // Change in Order Qty
                                  if (PickorderItemsCollectionFromOrder.Any(i => i.ItemNumber == item.ItemNumber))
                                  {
                                      Models.PickOrderItem currentItem = null;
                                      currentItem = PickorderItemsCollectionFromOrder.FirstOrDefault(i => i.ItemNumber == item.ItemNumber);
                                      if (item.UM == currentItem.UM && item.OrderQty != currentItem.OrderQty)
                                      {
                                          if (currentItem.OrderQty < item.OrderQty)
                                          {
                                              currentItem.PickedQuantityInPrimaryUOM = item.PickedQuantityInPrimaryUOM;
                                              currentItem.ExceptionQtyInPrimaryUOM = item.PickedQuantityInPrimaryUOM - currentItem.OrderQtyInPrimaryUOM;
                                              currentItem.ExceptionReason = "Over Picked";
                                              pickManager.UpdatePickedItem(Order.OrderId.ToString(), currentItem);

                                              if (currentItem.ExceptionQtyInPrimaryUOM != 0)
                                              {
                                                  pickManager.SaveException(Order.OrderId.ToString(), currentItem, CommonNavInfo.RouteID.ToString());
                                                  UpdateExceptions(currentItem, "Over Picked");
                                              }
                                          }
                                          else
                                          {
                                              currentItem.PickedQuantityInPrimaryUOM = item.PickedQuantityInPrimaryUOM;
                                              pickManager.UpdatePickedItem(Order.OrderId.ToString(), currentItem);
                                              UpdateExceptions(currentItem, IsValidItem(currentItem));
                                          }
                                      }
                                  }

                                  // Change in Order UM
                                  PickOrderItem pickItem = PickorderItemsCollectionFromOrder.FirstOrDefault(i => i.ItemNumber == item.ItemNumber);
                                  if (pickItem != null && !pickItem.UM.Equals(item.UM))
                                  {
                                      if (ExceptionItems.Count > 0)
                                      {
                                          // If the item in db is picked

                                          PickOrderItem exItem = ExceptionItems.FirstOrDefault(i => i.ItemNumber.Trim() == item.ItemNumber.Trim());
                                          if (exItem != null) // and the item was overpicked
                                          {
                                              if (item.PickedQuantityInPrimaryUOM == 0)
                                              {
                                                  if (pickItem.OrderQtyInPrimaryUOM <= exItem.ExceptionQtyInPrimaryUOM)
                                                  {
                                                      pickItem.PickedQuantityInPrimaryUOM = exItem.ExceptionQtyInPrimaryUOM;
                                                      pickManager.UpdatePickedItem(Order.OrderId.ToString(), pickItem, item.UM);
                                                      RemoveItemFromException(exItem);
                                                  }
                                              }
                                              else // Item was underpicke
                                              {
                                                  // Push to exception the current item
                                                  //item.ExceptionQtyInPrimaryUOM = item.PickedQuantityInPrimaryUOM;
                                                  //item.ExceptionReason = "Incorrect Item";
                                                  //item.PickedQuantityInPrimaryUOM = pickItem.PickedQuantityInPrimaryUOM;
                                                  //pickManager.SaveException(Order.OrderId.ToString(), item, CommonNavInfo.RouteID.ToString());
                                                  //UpdateExceptions(item, IsValidItem(item));

                                                  if (exItem.PickedQuantityInPrimaryUOM <= pickItem.OrderQtyInPrimaryUOM)
                                                  {
                                                      pickItem.PickedQuantityInPrimaryUOM = item.PickedQuantityInPrimaryUOM;
                                                      pickManager.UpdatePickedItem(Order.OrderId.ToString(), pickItem, item.UM);
                                                      RemoveItemFromException(exItem);
                                                  }
                                                  else
                                                  {
                                                      //pickItem.PickedQuantityInPrimaryUOM = exItem.ExceptionQtyInPrimaryUOM;
                                                      //pickManager.UpdatePickedItem(Order.OrderId.ToString(), pickItem, item.UM);

                                                      //exItem.ExceptionReason = "Over Picked";
                                                      //exItem.ExceptionQtyInPrimaryUOM = exItem.ExceptionQtyInPrimaryUOM - pickItem.PickedQuantityInPrimaryUOM;
                                                      //UpdateExceptions(exItem, IsValidItem(exItem));

                                                      PickOrderItem temp = new PickOrderItem();
                                                      temp = item;
                                                      temp.ExceptionQtyInPrimaryUOM = item.PickedQuantityInPrimaryUOM - pickItem.OrderQtyInPrimaryUOM;
                                                      temp.UMConversionFactor = pickItem.UMConversionFactor;
                                                      temp.ExceptionReason = "Over Picked";

                                                      if (temp.ExceptionQtyInPrimaryUOM > 0)
                                                      {
                                                          pickManager.SaveException(Order.OrderId.ToString(), temp, CommonNavInfo.RouteID.ToString());
                                                          inventoryMgr.UpdateCommittedQtyInTruckToBranchFlow(item.ItemId, temp.ExceptionQtyInPrimaryUOM, true);
                                                          inventoryMgr.UpdateTransactionQtyInItemLedger(item, PayloadManager.OrderPayload.OrderID, temp.ExceptionQtyInPrimaryUOM);
                                                      }

                                                      pickItem.PickedQuantityInPrimaryUOM = item.PickedQuantityInPrimaryUOM;
                                                      pickManager.UpdatePickedItem(Order.OrderId.ToString(), pickItem, item.UM);
                                                  }

                                              }
                                          }
                                      }
                                      else
                                      {
                                          if (item.PickedQuantityInPrimaryUOM == 0)// Item was not picked
                                          {
                                              pickManager.UpdatePickedItem(Order.OrderId.ToString(), pickItem, item.UM);
                                          }
                                          else // Item was underpicke
                                          {
                                              PickOrderItem temp = new PickOrderItem();
                                              temp = item;
                                              temp.ExceptionQtyInPrimaryUOM = item.PickedQuantityInPrimaryUOM - pickItem.OrderQtyInPrimaryUOM;
                                              temp.UMConversionFactor = pickItem.UMConversionFactor;
                                              temp.ExceptionReason = "Over Picked";

                                              if (temp.ExceptionQtyInPrimaryUOM > 0)
                                              {
                                                  pickManager.SaveException(Order.OrderId.ToString(), temp, CommonNavInfo.RouteID.ToString());
                                                  inventoryMgr.UpdateCommittedQtyInTruckToBranchFlow(item.ItemId, temp.ExceptionQtyInPrimaryUOM, true);
                                                  inventoryMgr.UpdateTransactionQtyInItemLedger(item, PayloadManager.OrderPayload.OrderID, temp.ExceptionQtyInPrimaryUOM);
                                              }

                                              pickItem.PickedQuantityInPrimaryUOM = item.PickedQuantityInPrimaryUOM;
                                              pickManager.UpdatePickedItem(Order.OrderId.ToString(), pickItem, item.UM);
                                              //UpdateExceptions(item, "Over Picked");

                                              //inventoryMgr.UpdateCommittedQtyInTruckToBranchFlow(item.ItemId, temp.ExceptionQtyInPrimaryUOM, true);
                                              //inventoryMgr.UpdateTransactionQtyInItemLedger(item, PayloadManager.OrderPayload.OrderID, item.PickedQuantityInPrimaryUOM);
                                          }
                                      }
                                  }
                              }

                          }

                          if (!IsPickListExists)
                          {
                              pickManager.InsertPickList(Order.OrderId.ToString(), PickorderItemsCollectionFromOrder, CommonNavInfo.RouteID.ToString());
                          }

                          ExceptionItems = pickManager.GetExceptionList(Order.OrderId.ToString());

                          if (itemsToDelete.Count > 0)
                          {
                              foreach (string itemNumber in itemsToDelete)
                              {
                                  pickManager.DeletePickedItem(Order.OrderId.ToString(), itemNumber);
                              }
                          }

                          // get the data from db
                          PickorderItemsCollectionFromDB = pickManager.GetPickItemsFromDB(Order.OrderId.ToString());

                          // Pass existing Order Sub state Is it VOID or HOLD 
                          Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.None, false);

                          ManualPickReasonList = pickManager.GetReasonListForManualPick();

                          foreach (PickOrderItem pickOrderItem in PickorderItemsCollectionFromDB)
                          {
                              PickOrderItem item = ExceptionItems.FirstOrDefault(i => i.ItemNumber == pickOrderItem.ItemNumber && i.UM == pickOrderItem.UM);
                              if (item != null && item.ExceptionReason == "Incorrect Item")
                              {
                                  pickOrderItem.PickedQuantityInPrimaryUOM = item.ExceptionQtyInPrimaryUOM;
                              }
                              pickOrderItem.LastComittedQty = pickOrderItem.PickedQuantityInPrimaryUOM > pickOrderItem.OrderQtyInPrimaryUOM ? pickOrderItem.PickedQuantityInPrimaryUOM : pickOrderItem.OrderQtyInPrimaryUOM;
                              if (pickOrderItem.StkType == "N")
                              {
                                  pickOrderItem.PickedQuantityInPrimaryUOM = pickOrderItem.OrderQtyInPrimaryUOM;
                              }
                              PickOrderItems.Add(pickOrderItem);
                          }

                          SortPickedItems();
                          PayloadManager.OrderPayload.PickOrderItems = PickOrderItems.ToList<PickOrderItem>();

                          OnPropertyChanged("IsPickCompleted");
                          OnPropertyChanged("PickOrderItems");
                          OnPropertyChanged("ExceptionItems");
                          OnPropertyChanged("CanRemoveExceptions");

                          IsBusy = false;
                      }
                      catch (Exception e)
                      {
                          IsBusy = false;
                          Logger.Error("SalesLogicExpress.ViewModels.PickOrder, LoadData()  Message: " + e.Message);
                      }
                  }));
                Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:asyncLoadData]");
            });
        }

        #endregion

        #region Methods
        private void RenderSign()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:RenderSign]");
            try
            {
                SignatureCanvasVM = new CanvasViewModel(Flow.OrderLifeCycle, SignType.ChargeOnAccountSign, Order.OrderId)
                    {
                        Height = 280,
                        Width = 350,
                        FileName = "SmallPic.jpg",
                        FooterText = "Sign here",
                    };
                SignatureCanvasVM.CanvasChanged += SignatureCanvasVM_CanvasChanged;
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][RenderSign][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:RenderSign]");
        }

        void SignatureCanvasVM_CanvasChanged(object sender, CanvasViewModel.CanvasUpdatedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:SignatureCanvasVM_CanvasChanged]");
            CheckForAcceptForChargeOnAccount();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:SignatureCanvasVM_CanvasChanged]");
        }

        private void InitializeCommands()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:InitializeCommands]");
            AcceptChargeOnAccount = new DelegateCommand((param) =>
            {
                //!RequestAuthCodeVM.IsAuthCodeValid
                try
                {
                    if (!RequestAuthCodeVM.IsAuthCodeValid && ChargeOnAccountContext.IsApprovalCodeNeeded)
                    {
                        var Alert = new AlertWindow { Message = Constants.CashCollection.MandatoryApprovalCodeAlert };
                        Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                        return;
                    }

                    SignatureCanvasVM.UpdateSignatureToDB(false, false, false);

                    string result = PayloadManager.OrderPayload.InvoiceNo = invoiceManager.CreateInvoice
                      (
                      "Credit",
                     CommonNavInfo.RouteID.ToString(),
                      Order.OrderId.ToString(),
                      PayloadManager.OrderPayload.Customer.CustomerNo,
                      DateTime.Now,
                      Convert.ToDecimal(PayloadManager.OrderPayload.Amount),
                      Convert.ToDecimal(PayloadManager.OrderPayload.Amount),
                      Order.OrderId.ToString()
                      ).ToString();
                    if (Convert.ToInt32(result) > 0)
                    {
                        CommonNavInfo.Customer.PaymentModeDescriptionForList = "(COA) CHARGE ON ACCOUNT";
                        PayloadManager.OrderPayload.InvoicePaymentType = "Credit";
                        DeliverToCustomerUpdateAndNavigate(true);
                        if (RequestAuthCodeVM.IsAuthCodeValid && ChargeOnAccountContext.IsApprovalCodeNeeded)
                        {
                            RequestAuthCodeVM.LogAuthCode(PayloadManager.ApplicationPayload.LoggedInUserID, RequestAuthCodeVM.RequestCode, RequestAuthCodeVM.Authcode, CommonNavInfo.RouteID.ToString(), PayloadManager.OrderPayload.SerializeToJson(), PayloadManager.OrderPayload.Customer.CustomerNo, PayloadManager.OrderPayload.Amount.ToString(), RequestAuthCodeVM.Params.Feature.ToString());
                        }
                    }
                    else
                    {
                        Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, please sync to continue" }, MessageToken);
                    }
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][AcceptChargeOnAccount DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }

            });
            DeclineChargeOnAccount = new DelegateCommand((param) =>
            {
                Messenger.Default.Send<CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
            });
            ClearSignInChargeOnAccount = new DelegateCommand((param) =>
            {
                SignStroke.Clear();
            });

            ShowPickSlipReport = new DelegateCommand((param) =>
            {
                try
                {
                    //While click print button show loading symbol  fixed by dinesh
                    //Task.Factory.StartNew(() =>
                    //{
                    //    try
                    //    {
                            if (param != null)
                            {
                                IsBusy = true;
                                //var payload = param;//Ununsed Code Vignesh.S
                                if (PickOrderItems != null)
                                    TotalPickedQty = PickOrderItems.Sum(s => s.PickedQuantityInPrimaryUOM).ToString();
                                ShowReport(PickOrderItems, PayloadManager.OrderPayload.OrderID);
                                //var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PrintPickOrder, CurrentViewName = ViewModelMappings.View.PickOrder, CloseCurrentView = true, Payload = payload, ShowAsModal = false };
                                // Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                                IsBusy = false;
                            }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][ShowPickSlipReport DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    //        IsBusy = false;

                    //    }
                    //});
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][ShowPickSlipReport DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    IsBusy = false;
                }
            });
            ShowInvoiceReport = new DelegateCommand((param) =>
            {
                try
                {
                    //While click print button show loading symbol  fixed by dinesh
                    //Task.Factory.StartNew(() =>
                    //{
                    //    try
                    //    {
                            IsBusy = true;
                            //var payload = Order.OrderId;//Unused Code Vignesh.S
                            TotalPickedQty = PickOrderItems.Sum(s => s.PickedQuantityInPrimaryUOM).ToString();
                            ShowPrintInvoiceReport(PickOrderItems, PayloadManager.OrderPayload.OrderID);
                            //var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PrintInvoice, CurrentViewName = ViewModelMappings.View.PickOrder, CloseCurrentView = true, Payload = payload, ShowAsModal = false };
                            //Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                            IsBusy = false;
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][ShowInvoiceReport DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    //        IsBusy = false;
                    //    }
                    //});
                }
                catch (Exception ex)
                {
                    IsBusy = false;
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][ShowInvoiceReport DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });

            VoidOrder = new DelegateCommand((param) =>
            {
                try
                {
                    if (ReasonCodeList != null)
                        Order.VoidOrderReasonId = ReasonCodeList.Id;
                    // Set reason for void Order if not available 
                    if (Order.VoidOrderReasonId == 0)
                    {
                        ReasonCodeList.ParentViewModel = this;
                        ReasonCodeList.MessageToken = MessageToken;
                        ReasonCodeList.ActivityKey = ActivityKey.VoidAtCashCollection;
                        var OpenDialog = new Helpers.DialogWindow { TemplateKey = "VoidOrderReason", Title = "Void Order Reason", Payload = ReasonCodeList };
                        Messenger.Default.Send<DialogWindow>(OpenDialog, MessageToken);
                        if (OpenDialog.Cancelled)
                        {
                            OrderStateChangeArgs arg = new OrderStateChangeArgs();
                            arg.State = OrderState.None;
                            ReasonCodeList.OnStateChanged(arg);
                        }
                        return;
                    }
                    else
                    {
                        // Update void reason in DB and Clear InMemory VoidOrderReasonId = 0

                        Order.VoidOrderReasonId = 0;
                    }
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][VoidOrder DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");

                }
            });

            ChargeOnAccount = new DelegateCommand((param) =>
            {
                try
                {
                    Managers.CollectionManager collectionManager = new Managers.CollectionManager();
                    RequestAuthCodeViewModel.RequestAuthCodeParams codeParams = new RequestAuthCodeViewModel.RequestAuthCodeParams
                    {
                        Amount = CashDeliveryContext.TotalBalanceAmount,
                        UserID = UserManager.UserId.ToString(),
                        Feature = AuthCodeManager.Feature.ChargeOnAccount
                    };
                    RequestAuthCodeVM = new RequestAuthCodeViewModel(codeParams);

                    RequestAuthCodeVM.AuthCodeChanged += RequestAuthCodeVM_AuthCodeChanged;
                    ChargeOnAccountContext = collectionManager.GetChargeOnAccountInfo(CommonNavInfo.Customer.CustomerNo, Order.OrderId.ToString(), CashDeliveryContext.TotalBalanceAmount, CashDeliveryContext.PreviousBalanceAmount, CashDeliveryContext.CurrentInvoiceAmount);
                    if (!ChargeOnAccountContext.IsApprovalCodeNeeded)
                    {
                        RequestAuthCodeVM.IsAuthCodeValid = true;
                        RequestAuthCodeVM.RequestCode = string.Empty;
                    }
                    COAReasonList = new Managers.OrderManager().GetReasonListFor("Charge on Account");
                    _SignStroke = new StrokeCollection();

                    //Messenger.Default.Send<CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                    RenderSign();
                    DialogWindow ChargeOnAccountDialog = new DialogWindow();
                    ChargeOnAccountDialog.Title = "Charge on Account";
                    ChargeOnAccountDialog.TemplateKey = "ChargeOnAccount";
                    Messenger.Default.Send<DialogWindow>(ChargeOnAccountDialog, MessageToken);
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][ChargeOnAccount DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");

                }
            });

            SetPickItemLabels = new DelegateCommand((param) =>
            {
                Models.PickOrderItem orderItem = param as Models.PickOrderItem;
                PickedItemNo = orderItem.ItemNumber.Trim();
                PickedItemUM = orderItem.UM;
                ManualEnteredQty = orderItem.OrderQty;
            });
            ScanByDevice = new DelegateCommand((param) =>
            {
                if (IsScannerOn == true)
                    IsManualPick = false;
                else
                    IsManualPick = true;

                LastScanItem = param as PickOrderItem;
                if (LastScanItem.StkType != "N")
                {
                    ScanOperation(param);
                }
            });

            ScanOneByOneAdd = new DelegateCommand((param) =>
            {
                LastScanItem = param as PickOrderItem;
                if (LastScanItem.StkType != "N")
                {
                    IsAddingToPaletteOneByOne = true;
                    ScanOperationOneByOne(param, false);
                }
            });

            ScanOneByOneRemoveFromGrid = new DelegateCommand((param) =>
            {
                LastScanItem = param as PickOrderItem;
                if (LastScanItem.StkType != "N")
                {
                    IsAddingToPaletteOneByOne = false;
                    ScanOperationOneByOne(param, true);
                }
            });

            ManualPick = new DelegateCommand((param) =>
            {
                if (param == null)
                {
                    string strMessage;
                    if (IsAddingToPalette)
                    {
                        strMessage = Constants.PickOrder.SelectItemToPickAlert;
                    }
                    else
                    {
                        strMessage = Constants.PickOrder.SelectItemToUnPickAlert;
                    }
                    var Alert = new AlertWindow { Message = strMessage };
                    Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                    return;
                }
                Models.PickOrderItem item = param as Models.PickOrderItem;
                if (item.StkType != "N")
                {
                    if (item.LastManualPickReasonCode != 0)
                    {
                        ScanOperation(param);
                    }
                    else
                    {
                        if (IsScannerOn && LastScanItem != item)
                        {
                            var Alert = new AlertWindow { Message = Constants.PickOrder.SelectReasonForManualPickAlert };
                            Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                            return;
                        }
                        else
                        {
                            ScanOperation(param);
                        }
                    }
                }
            });
            GetManualPickReasonList = new DelegateCommand((param) =>
            {
                string strMessage;
                if (IsAddingToPalette)
                {
                    param = SelectedPickItem;
                    strMessage = Constants.PickOrder.SelectItemToPickAlert;
                }
                else
                {
                    param = SelectedExceptionItem;
                    strMessage = Constants.PickOrder.SelectItemToUnPickAlert;
                }


                if (param == null)
                {
                    var Alert = new AlertWindow { Message = strMessage };
                    Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                    return;
                }

                var dialog = new Helpers.DialogWindow { TemplateKey = "ManualPickReason", Title = "Reason Code", Payload = null };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });

            SetManualPickReason = new DelegateCommand((param) =>
            {
                selectedReason = param as ReasonCode;

                if (selectedReason == null)
                {
                    ErrorText = "Please Choose Reason Code";
                }

                if (IsAddingToPalette)
                {
                    SelectedPickItem.LastManualPickReasonCode = selectedReason.Id;
                    //Update in DB
                    pickManager.UpdateManualPickReasonAndCountForPick(SelectedPickItem);

                }
                else
                {
                    SelectedExceptionItem.LastManualPickReasonCode = selectedReason.Id;
                    //Update in DB
                    pickManager.UpdateManualPickReasonAndCountForException(SelectedExceptionItem);

                }

            });

            HoldPickList = new DelegateCommand((param) =>
            {
                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.HoldOrderConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;
                if (confirmMessage.Confirmed)
                {
                    OrderStateChangeArgs arg = new OrderStateChangeArgs();
                    arg.State = OrderState.Hold;
                    selectedReason.OnStateChanged(arg);

                    if (ActivityKey.HoldAtPick.ToString() == param.ToString())
                    {
                        LogActivity(ActivityKey.HoldAtPick, true);
                    }
                    else
                    {
                        LogActivity(ActivityKey.HoldAtCashCollection, true);
                    }

                    System.Threading.Thread.Sleep(50);
                    Messenger.Default.Send<CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                }
                Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.HoldAtPick);
                pickManager.HoldUnHoldPickList(Order.OrderId.ToString(), true);

                #region Update Activity Count
                string tempStopDate = string.Empty;
                string stopID = string.Empty;
                if (PayloadManager.OrderPayload.StopDate.Date > DateTime.Now.Date || PayloadManager.OrderPayload.StopDate.Date < DateTime.Now.Date)
                {
                    tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                    stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.OrderPayload.Customer.CustomerNo, tempStopDate);
                }
                else
                {
                    tempStopDate = PayloadManager.OrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                    stopID = PayloadManager.OrderPayload.Customer.StopID;
                }
                new Managers.CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                new Managers.CustomerManager().UpdateActivityCount(stopID, true, true);
                PayloadManager.OrderPayload.Customer.SaleStatus = "";
                PayloadManager.OrderPayload.Customer.PendingActivity = new Managers.CustomerManager().GetPendingAtivityForStop(stopID);
                new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.OrderPayload.Customer.CustomerNo);
                #endregion

                #region UpdateCommitedQuantity
                // Log commited qty to inventory_ledger  and update inventory
                if (PayloadManager.OrderPayload.Items != null)
                    inventoryMgr.UpdateCommittedQuantity(PayloadManager.OrderPayload.Items.ToList(), StatusTypesEnum.HOLD);
                #endregion

                #region Update temp sequence no.
                //UPDATE CUSTOMER'S TEMPERORY SEQUENCE NO. --- Priya Natu
                DateTime todayDate = Convert.ToDateTime(tempStopDate);
                ViewModels.CustomerDashboard custDash = new CustomerDashboard();
                PayloadManager.OrderPayload.Customer = custDash.GetNewCustomer(todayDate, PayloadManager.OrderPayload.Customer);

                #endregion

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.PickOrder, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });

            RemoveExceptions = new DelegateCommand((s) =>
            {

                var confirmMessage = new Helpers.ConfirmWindow { Message = Constants.PickOrder.RemoveExceptionConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;

                IsAddingToPalette = false;
            });

            DeliverToCustomer = new DelegateCommand(param =>
            {
                //Avoid double click of two buttons
                if (IsBusy == false)
                {
                    Managers.CollectionManager collectionManager = new Managers.CollectionManager();
                    // string newReceiptId = "";
                    try
                    {

                        if (param != null)
                        {
                            IsBusy = true;
                            bool? IsCashDelivery = param as bool?;
                            if (IsCashDelivery.Value)
                            {
                                if (!CashDeliveryContext.IsCashDeliveryAllowed && CashDeliveryContext.PaymentAmount != CashDeliveryContext.CurrentInvoiceAmount.ToString())
                                {
                                    Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Constants.CashCollection.AmountMissMatchAlert }, MessageToken);
                                    return;
                                }
                                else
                                {
                                    //Insert Check Details
                                    //if (CashDeliveryContext.PaymentMode)    // User Issue ID - 15 for Edit and Update the Payment Mode from cash to Check to display the updated check details in Settlement Screen on Web Portal   -- By Vignesh D
                                    //{
                                    CheckEntryDetails o = new CheckEntryDetails();
                                    o.CheckNo = CashDeliveryContext.ChequeNo;
                                    o.CheckAmount = CashDeliveryContext.PaymentAmount;
                                    o.CheckDate = CashDeliveryContext.ChequeDate.ToString("yyyy-MM-dd");
                                    o.CreatedBy = Managers.UserManager.UserId.ToString();
                                    o.RoutId = CommonNavInfo.RouteID.ToString();
                                    o.CustomerId = CommonNavInfo.Customer.CustomerNo;
                                    o.CustomerName = CommonNavInfo.Customer.Name;
                                    string checkID = SettlementConfirmationManager.InsertCheckDetails(o);
                                    CashDeliveryContext.ChequeNoId = checkID;
                                    //}
                                    string result = PayloadManager.OrderPayload.InvoiceNo = invoiceManager.CreateInvoice
                             (
                             "Cash",
                             CommonNavInfo.RouteID.ToString(),
                             Order.OrderId.ToString(),
                             PayloadManager.OrderPayload.Customer.CustomerNo,
                             DateTime.Now,
                             Convert.ToDecimal(PayloadManager.OrderPayload.Amount),
                             Convert.ToDecimal(PayloadManager.OrderPayload.Amount),
                             Order.OrderId.ToString()
                             ).ToString();

                                    if (Convert.ToInt32(result) > 0)
                                    {

                                        PayloadManager.OrderPayload.InvoicePaymentType = "Cash";
                                        string receiptId = "";
                                        collectionManager.SaveCashDeliveryInfo(CashDeliveryContext, CommonNavInfo.Customer.CustomerNo, CommonNavInfo.RouteID.ToString(), PayloadManager.OrderPayload.InvoiceNo, ref receiptId);
                                        //collectionManager.SaveCashSummaryForCustomer(CashDeliveryContext, CommonNavInfo.Customer.CustomerNo, CommonNavInfo.Customer.Company, true);
                                        OrderStateChangeArgs arg = new OrderStateChangeArgs();
                                        arg.State = OrderState.Hold;
                                        selectedReason.OnStateChanged(arg);
                                        System.Threading.Thread.Sleep(50);
                                        DeliverToCustomerUpdateAndNavigate();
                                        LogPaymentActivity(ActivityKey.Payment, receiptId);
                                        return;
                                    }
                                    else
                                    {
                                        Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, please sync to continue" }, MessageToken);
                                    }
                                }
                            }
                        }
                        if (CommonNavInfo.Customer.PaymentModeForList.ToLower() == "cod" || CommonNavInfo.Customer.PaymentModeForList.ToLower() == "csh")
                        {
                            collectionManager = new Managers.CollectionManager();
                            CashDeliveryContext = collectionManager.GetCashDeliveryInfo(CommonNavInfo.Customer.CustomerNo, Order.OrderId.ToString(), CanOpenCashCollection);
                            OpenCashCollectionPopUp();
                        }
                        else
                        {
                            string result = PayloadManager.OrderPayload.InvoiceNo = invoiceManager.CreateInvoice
                               (
                               "Credit",
                               CommonNavInfo.RouteID.ToString(),
                               Order.OrderId.ToString(),
                               PayloadManager.OrderPayload.Customer.CustomerNo,
                               DateTime.Now,
                               Convert.ToDecimal(PayloadManager.OrderPayload.Amount),
                               Convert.ToDecimal(PayloadManager.OrderPayload.Amount),
                               Order.OrderId.ToString()
                               ).ToString();

                            if (Convert.ToInt32(result) > 0)
                            {
                                PayloadManager.OrderPayload.InvoicePaymentType = "Credit";
                                DeliverToCustomerUpdateAndNavigate();
                            }
                            else
                            {
                                Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, please sync to continue" }, MessageToken);
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][DeliverToCustomer DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                    IsBusy = false;
                }
            });

            SwitchScannerMode = new DelegateCommand((param) =>
            {
                bool InvertMode = !IsScannerOn;
                IsScannerOn = InvertMode;
            });
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:InitializeCommands]");
        }


        void RequestAuthCodeVM_AuthCodeChanged(object sender, RequestAuthCodeViewModel.AuthCodeChangedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:RequestAuthCodeVM_AuthCodeChanged]");
            CheckForAcceptForChargeOnAccount();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:RequestAuthCodeVM_AuthCodeChanged]");
        }

        public async void OpenCashCollectionPopUp()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:async OpenCashCollectionPopUp]");
            await Task.Factory.StartNew(() =>
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.DataBind,
                  new Action(() =>
                  {
                      DialogWindow CashCollectionDialog = new DialogWindow();
                      CashCollectionDialog.Title = "Cash Delivery";
                      CashCollectionDialog.TemplateKey = "CashDelivery";
                      Messenger.Default.Send<DialogWindow>(CashCollectionDialog, MessageToken);
                  }));
            });
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:async OpenCashCollectionPopUp]");
        }

        public void ShowReport(TrulyObservableCollection<PickOrderItem> pickorderitem, string orderID)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:ShowReport]");
            try
            {
                var DocInfo = new ReportEngine.DocumentInformation();
                var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                repEngine.Notify += repEngine_Notify;

                repEngine.Log = this.Logger;
                ReportEngine.IReportSLE ReportData = null;
                //var query = string.Format("select DocumentID from BUSDTA.PickOrder where Order_ID = {0}", orderID);
                //var result = Convert.ToInt32(Helpers.DbEngine.ExecuteScalar(query));
                //var DocumentId = result;
                var DocumentId = 0;
                var SourceDocumentNo = Convert.ToDecimal(orderID);
                if (DocumentId <= 0)
                {
                    ReportData = new ReportViewModels.PickSlipReport();
                    Dictionary<string, object> Parameters;
                    Parameters = new Dictionary<string, object>();
                    Parameters.Add("PrintLabels", Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PrintLabels"]));
                    DocInfo = repEngine.Generate<Reporting.rPickOrderSlip>(SourceDocumentNo, true, ReportEngine.ReportCategory.Customer, ReportEngine.ReportsTypes.InvoiceReport, ReportData, Parameters, ReportEngine.ContentTypes.PDF);
                }
                else
                {
                    DocInfo.DocumentId = DocumentId;
                }

                if (DocInfo.IsEndOfSequence)
                {
                    if (System.Windows.Application.Current != null)
                        System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)delegate // <--- HERE
                        {
                            Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, Please sync to continue" }, MessageToken);
                        });
                    IsBusy = false;
                    return;
                }

                if (DocInfo.IsDocumentCreated)
                    ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "PickOrder", "Order_ID", SourceDocumentNo, DocInfo.DocumentId);
                DocumentId = Convert.ToInt32(DocInfo.DocumentId); //Replace Latest

                var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Customer, DocumentId, null);
                if (ReportBytes == null)
                {
                    //if (System.Windows.Application.Current != null)
                    //    System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)delegate // <--- HERE
                    //    {
                            Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Report not generated, Please try again." }, MessageToken);
                        //});
                    IsBusy = false;
                    return;
                }

                SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
                Payload.ReportFile = ReportBytes;
                Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                Payload.PreviousView = ViewModelMappings.View.PickOrder;
                Payload.CurrentViewTitle = "Pick Slip Report";
                Payload.PreviousViewTitle = "Pick Order";
                Payload.MessageToken = this.MessageToken;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.PickOrder, CloseCurrentView = true, Payload = Payload, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ShowReport][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:ShowReport]");
            IsBusy = false;
        }
        public void ShowPrintInvoiceReport(TrulyObservableCollection<PickOrderItem> pickorderitem, string orderID)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:ShowPrintInvoiceReport]");
            try
            {
                var DocInfo = new ReportEngine.DocumentInformation();
                var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                repEngine.Notify += repEngine_Notify;

                repEngine.Log = this.Logger;
                ReportEngine.IReportSLE ReportData = null;
                //var query = string.Format("select DocumentID from BUSDTA.PickOrder where Order_ID = {0}", orderID);
                //var result = Convert.ToInt32(Helpers.DbEngine.ExecuteScalar(query));
                //var DocumentId = result;
                var DocumentId = 0;
                var SourceDocumentNo = Convert.ToDecimal(orderID);
                if (DocumentId <= 0)
                {
                    ReportData = new ReportViewModels.InvoiceReport(orderID, true);
                    Dictionary<string, object> Parameters;
                    Parameters = new Dictionary<string, object>();
                    Parameters.Add("PrintLabels", Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PrintLabels"]));
                    DocInfo = repEngine.Generate<Reporting.Invoice>(SourceDocumentNo, true, ReportEngine.ReportCategory.Customer, ReportEngine.ReportsTypes.InvoiceReport, ReportData, Parameters, ReportEngine.ContentTypes.PDF);
                }
                else
                {
                    DocInfo.DocumentId = DocumentId;
                }

                if (DocInfo.IsEndOfSequence)
                {
                    //if (System.Windows.Application.Current != null)
                    //    System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)delegate // <--- HERE
                    //    {
                            Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, Please sync to continue" }, MessageToken);
                        //});

                    IsBusy = false;
                    return;
                }

                if (DocInfo.IsDocumentCreated)
                    ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.Order_Header", "OrderID", SourceDocumentNo, DocInfo.DocumentId);
                DocumentId = Convert.ToInt32(DocInfo.DocumentId); //Replace Latest

                var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Customer, DocumentId, null);

                if (ReportBytes == null)
                {
                    if (System.Windows.Application.Current != null)
                        System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)delegate // <--- HERE
                        {
                            Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Report not generated, Please try again." }, MessageToken);
                        });
                    IsBusy = false;
                    return;
                }

                SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
                Payload.ReportFile = ReportBytes;
                Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                Payload.PreviousView = ViewModelMappings.View.PickOrder;
                Payload.CurrentViewTitle = "Invoice Report";
                Payload.PreviousViewTitle = "Pick Order";
                Payload.MessageToken = this.MessageToken;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.PickOrder, CloseCurrentView = false, Payload = Payload, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ShowPrintInvoiceReport][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:ShowReport]");
            IsBusy = false;
        }

        void repEngine_Notify(string msg)
        {
            System.Windows.MessageBox.Show(msg);
        }

        private void DeliverToCustomerUpdateAndNavigate(Boolean IsPaymentTermChanged = false)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:DeliverToCustomerUpdateAndNavigate]");
            try
            {
                Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.DeliverToCustomer);

                //Managers.CollectionManager collectionManager = new Managers.CollectionManager();//Unused Code Vignesh.S

                if (IsPaymentTermChanged)
                {
                    CashDeliveryContext.PaymentAmount = "0";
                    //DbEngine.ExecuteNonQuery("Update Busdta.Order_Header Set PaymentTerms='N30' where OrderID='" + Order.OrderId + "'");

                    objOrderManger.PickOrder_DeliverToCustomerUpdateAndNavigate(Order.OrderId);
                    //collectionManager.SaveCashSummaryForCustomer(CashDeliveryContext, CommonNavInfo.Customer.CustomerNo, CommonNavInfo.Customer.Company, false);
                }

                ObservableCollection<OrderItem> ItemsToDeliver = new ObservableCollection<OrderItem>();

                foreach (PickOrderItem pickItem in PickOrderItems)
                {
                    ItemsToDeliver.Add(pickItem);
                }
                Messenger.Default.Send<CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);

                pickManager.HoldUnHoldPickList(Order.OrderId.ToString(), false);

                SalesLogicExpress.Application.ViewModels.CommonNavInfo.NavigationParameter = IsPaymentTermChanged;
                LogActivity(ActivityKey.PickComplete, true);
                PayloadManager.OrderPayload.Items = ItemsToDeliver.ToList();

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.DeliveryScreen, Refresh = true, CloseCurrentView = true, ShowAsModal = false, CurrentViewName = ViewModelMappings.View.PickOrder };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][DeliverToCustomerUpdateAndNavigate][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:DeliverToCustomerUpdateAndNavigate]");
        }


        private void PickOrderItems_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:PickOrderItems_ItemPropertyChanged]");
            PickOrderItem pickedItem = sender as PickOrderItem;
            if (e.PropertyName == "PickedQuantityInPrimaryUOM")
            {
                //Updt qty in DB
                pickManager.UpdatePickQty(Order.OrderId.ToString(), pickedItem.ItemNumber, pickedItem.PickedQuantityInPrimaryUOM.ToString(), pickedItem.UM);

                OnPropertyChanged("IsPickCompleted");
                //ScanOperation(pickedItem);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:PickOrderItems_ItemPropertyChanged]");
        }

        // check for item validity and return Item Status
        public string IsValidItem(Models.PickOrderItem item)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:IsValidItem]" +
                "[Parameters:item.ItemNumber:" + item.ItemNumber + ", item.OrderQtyInPrimaryUOM:" + item.OrderQtyInPrimaryUOM + ", item.PickedQuantityInPrimaryUOM:" + item.PickedQuantityInPrimaryUOM + "]");
            string result = "";
            try
            {
                if (item.OrderQtyInPrimaryUOM < item.PickedQuantityInPrimaryUOM && PickOrderItems.Any(i => i.ItemNumber == item.ItemNumber && i.UM == item.UM))
                {
                    result = "Over Picked";

                }
                else if (!PickOrderItems.Any(i => i.ItemNumber == item.ItemNumber && i.UM == item.UM))
                {
                    result = "Incorrect Item";
                }
                else if (PickOrderItems.Count != 0 && PickOrderItems.Any(i => i.ItemNumber == item.ItemNumber && i.UM == item.UM))
                {
                    result = "Valid";
                }

                //result = "Invalid Item Code";
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][IsValidItem][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:IsValidItem]");

            return result;
        }


        //update picked quantity in memory collection
        public void AddingToPalette(PickOrderItem item)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:AddingToPalette]");
            //Sathish Changed this code.Disposing the objects
            OrderItem itm = null;
            try
            {
                item.PickedQuantityInPrimaryUOM = Convert.ToInt32(item.PickedQuantityInPrimaryUOM + 1);

                if (item.PickedQuantityInPrimaryUOM > item.OrderQtyInPrimaryUOM)
                {
                    item.AvailableQty = item.AvailableQty - 1;
                    inventoryMgr.UpdateCommittedQtyInTruckToBranchFlow(item.ItemId, 1, true);
                    inventoryMgr.UpdateTransactionQtyInItemLedger(item, PayloadManager.OrderPayload.OrderID, 1);
                    itm = PayloadManager.OrderPayload.Items.FirstOrDefault(x => x.ItemNumber == item.ItemNumber);
                    if (itm != null)
                    {
                        itm.LastComittedQty = item.PickedQuantityInPrimaryUOM;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][AddingToPalette][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            finally
            {
                itm = null;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:AddingToPalette]");
        }

        public void AddingToPaletteOneByOne(PickOrderItem item)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:AddingToPaletteOneByOne]");
            if (item.PickedQuantityInPrimaryUOM > item.OrderQtyInPrimaryUOM)
            {
                try
                {
                    item.AvailableQty = item.AvailableQty - 1;
                    inventoryMgr.UpdateCommittedQtyInTruckToBranchFlow(item.ItemId, 1, true);
                    inventoryMgr.UpdateTransactionQtyInItemLedger(item, PayloadManager.OrderPayload.OrderID, 1);
                    OrderItem itm = PayloadManager.OrderPayload.Items.FirstOrDefault(x => x.ItemNumber == item.ItemNumber);
                    if (itm != null)
                    {
                        itm.LastComittedQty = item.PickedQuantityInPrimaryUOM;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][AddingToPaletteOneByOne][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:AddingToPaletteOneByOne]");
        }

        //update picked quantity in memory collection
        public void RemovingFromPalette(PickOrderItem item)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:RemovingFromPalette]");
            if (item.PickedQuantityInPrimaryUOM > 0)
            {
                if (item.PickedQuantityInPrimaryUOM > item.OrderQtyInPrimaryUOM)
                {
                    try
                    {
                        if (PickOrderItems.Count > 0)
                        {
                            var tempItem = PickOrderItems.FirstOrDefault<PickOrderItem>(x => x.ItemNumber == item.ItemNumber);
                            if (tempItem != null)
                            {
                                item.ItemId = tempItem.ItemId;
                                tempItem.AvailableQty = tempItem.AvailableQty + 1;
                                inventoryMgr.UpdateCommittedQtyInTruckToBranchFlow(item.ItemId, 1, false);
                                inventoryMgr.UpdateTransactionQtyInItemLedger(item, PayloadManager.OrderPayload.OrderID, -1);

                                OrderItem itm = PayloadManager.OrderPayload.Items.FirstOrDefault(x => x.ItemNumber == item.ItemNumber);
                                if (itm != null)
                                {
                                    itm.LastComittedQty = Convert.ToInt32(item.PickedQuantityInPrimaryUOM - 1);
                                }
                            }
                        }
                        item.PickedQuantityInPrimaryUOM = Convert.ToInt32(item.PickedQuantityInPrimaryUOM - 1);
                        if (PickOrderItems.Count > 0 && PickOrderItems.Any(s => s.ItemNumber == item.ItemNumber && s.UM == item.UM))
                        {
                            PickOrderItems.FirstOrDefault(s => s.ItemNumber == item.ItemNumber && s.UM == item.UM).PickedQuantityInPrimaryUOM = Convert.ToInt32(PickOrderItems.FirstOrDefault(s => s.ItemNumber == item.ItemNumber && s.UM == item.UM).PickedQuantityInPrimaryUOM - 1);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][RemovingFromPalette][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                }
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:RemovingFromPalette]");
            //else
            //{

            //    //var Alert = new AlertWindow { Message = "Picked item doesn't exist in palette" };
            //    //Messenger.Default.Send<AlertWindow>(Alert, MessageToken);

            //}

        }

        public void RemovingFromPaletteOneByOne(PickOrderItem item)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:RemovingFromPaletteOneByOne]");
            if (IsAddingToPaletteOneByOne == false)
                if (item.PickedQuantityInPrimaryUOM > 0)
                {
                    if (item.PickedQuantityInPrimaryUOM > item.OrderQtyInPrimaryUOM)
                    {
                        try
                        {
                            if (PickOrderItems.Count > 0)
                            {
                                var tempItem = PickOrderItems.FirstOrDefault<PickOrderItem>(x => x.ItemNumber == item.ItemNumber);
                                if (tempItem != null)
                                {
                                    item.ItemId = tempItem.ItemId;
                                    tempItem.AvailableQty = tempItem.AvailableQty + 1;
                                    inventoryMgr.UpdateCommittedQtyInTruckToBranchFlow(item.ItemId, 1, false);
                                    inventoryMgr.UpdateTransactionQtyInItemLedger(item, PayloadManager.OrderPayload.OrderID, -1);

                                    OrderItem itm = PayloadManager.OrderPayload.Items.FirstOrDefault(x => x.ItemNumber == item.ItemNumber);
                                    if (itm != null)
                                    {
                                        itm.LastComittedQty = Convert.ToInt32(item.PickedQuantityInPrimaryUOM - 1);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][RemovingFromPaletteOneByOne][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                        }
                    }
                }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:RemovingFromPaletteOneByOne]");
        }

        // picking/unpicking qty from keypad
        public void ManuallyPicking(PickOrderItem item)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:ManuallyPicking]");
            OrderItem itm = null;
            try
            {
                int intPickedQuantityInPrimaryUOM = Convert.ToInt32(ManualEnteredQty * item.UMConversionFactor);
                //Get extra pick qty to update avilable qty 
                if ((item.ExceptionReason == "Incorrect Item" || item.ExceptionReason == "Over Picked") &&
                    ExceptionItems.Count > 0 && ExceptionItems.Any(s => s.ItemNumber == item.ItemNumber)
                    )
                {
                    //if (intPickedQuantityInPrimaryUOM > item.ExceptionQtyInPrimaryUOM)
                    //{

                    //    var Alert = new AlertWindow { Message = "Item quantity exceeds exception quantity." };
                    //    Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                    //    ManualEnteredQty = 0;
                    //    return;
                    //}

                    if (intPickedQuantityInPrimaryUOM > item.QtyOnHand)
                    {

                        var Alert = new AlertWindow { Message = "Item pick quantity exceeds on hand quantity." };
                        Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                        return;
                    }

                    item.ExceptionQtyInPrimaryUOM = item.ExceptionQtyInPrimaryUOM - intPickedQuantityInPrimaryUOM;
                    item.PickedQuantityInPrimaryUOM = item.ExceptionQtyInPrimaryUOM + item.OrderQtyInPrimaryUOM;
                    if (PickOrderItems.Count > 0 && PickOrderItems.Any(s => s.ItemNumber == item.ItemNumber))
                    {
                        PickOrderItems.FirstOrDefault(s => s.ItemNumber == item.ItemNumber).PickedQuantityInPrimaryUOM = item.PickedQuantityInPrimaryUOM;
                        PickOrderItems.FirstOrDefault(s => s.ItemNumber == item.ItemNumber).AvailableQty += intPickedQuantityInPrimaryUOM;
                        inventoryMgr.UpdateCommittedQtyInTruckToBranchFlow(item.ItemId, intPickedQuantityInPrimaryUOM, false);
                        inventoryMgr.UpdateTransactionQtyInItemLedger(item, PayloadManager.OrderPayload.OrderID, ManualEnteredQty * (-1));
                        itm = PayloadManager.OrderPayload.Items.FirstOrDefault(x => x.ItemNumber == item.ItemNumber);
                        if (itm != null)
                        {
                            itm.LastComittedQty = item.PickedQuantityInPrimaryUOM;
                        }
                    }
                }
                else
                {
                    //if (intPickedQuantityInPrimaryUOM > item.OrderQtyInPrimaryUOM)
                    //{

                    //    var Alert = new AlertWindow { Message = "Picked item quantity exceeds order quantity." };
                    //    Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                    //    return;

                    //}

                    int oldPickQty = item.PickedQuantityInPrimaryUOM;

                    if (PickOrderItems.Count > 0 && PickOrderItems.Any(s => s.ItemNumber == item.ItemNumber))
                    {
                        if ((oldPickQty > intPickedQuantityInPrimaryUOM))
                        {
                            if (intPickedQuantityInPrimaryUOM <= item.OrderQtyInPrimaryUOM)
                            {
                                PickOrderItems.FirstOrDefault(s => s.ItemNumber == item.ItemNumber).AvailableQty += (oldPickQty - item.OrderQtyInPrimaryUOM);
                                inventoryMgr.UpdateCommittedQtyInTruckToBranchFlow(item.ItemId, (oldPickQty - item.OrderQtyInPrimaryUOM), false);

                            }
                            else
                            {
                                PickOrderItems.FirstOrDefault(s => s.ItemNumber == item.ItemNumber).AvailableQty += (oldPickQty - intPickedQuantityInPrimaryUOM);
                                inventoryMgr.UpdateCommittedQtyInTruckToBranchFlow(item.ItemId, (oldPickQty - intPickedQuantityInPrimaryUOM), false);

                            }
                            inventoryMgr.UpdateTransactionQtyInItemLedger(item, PayloadManager.OrderPayload.OrderID, oldPickQty * (-1));
                            inventoryMgr.UpdateTransactionQtyInItemLedger(item, PayloadManager.OrderPayload.OrderID, ManualEnteredQty);
                        }
                        else
                        {
                            item.PickedQuantityInPrimaryUOM = intPickedQuantityInPrimaryUOM;

                            if (item.PickedQuantityInPrimaryUOM > item.OrderQtyInPrimaryUOM)
                            {
                                item.AvailableQty = item.AvailableQty - (intPickedQuantityInPrimaryUOM - oldPickQty);
                                inventoryMgr.UpdateCommittedQtyInTruckToBranchFlow(item.ItemId, (intPickedQuantityInPrimaryUOM - oldPickQty), true);
                                inventoryMgr.UpdateTransactionQtyInItemLedger(item, PayloadManager.OrderPayload.OrderID, (intPickedQuantityInPrimaryUOM - oldPickQty));
                                //OrderItem itm1 = PayloadManager.OrderPayload.Items.FirstOrDefault(x => x.ItemNumber == item.ItemNumber);
                                //if (itm1 != null)
                                //{
                                //    itm1.LastComittedQty = item.PickedQuantityInPrimaryUOM;
                                //}
                            }
                        }

                        PickOrderItems.FirstOrDefault(s => s.ItemNumber == item.ItemNumber).PickedQuantityInPrimaryUOM = intPickedQuantityInPrimaryUOM;
                    }

                    itm = PayloadManager.OrderPayload.Items.FirstOrDefault(x => x.ItemNumber == item.ItemNumber);
                    if (itm != null)
                    {
                        itm.LastComittedQty = (item.OrderQtyInPrimaryUOM > intPickedQuantityInPrimaryUOM ? item.OrderQtyInPrimaryUOM : intPickedQuantityInPrimaryUOM);

                    }
                    item.PickedQuantityInPrimaryUOM = intPickedQuantityInPrimaryUOM;
                }



                ManualEnteredQty = 0;
                IsManualPick = false;
                item.LastManualPickReasonCode = 0;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][ManuallyPicking][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            finally
            {
                itm = null;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:ManuallyPicking]");
        }

        // save exception in Insert/update
        public void SaveException(string OrderId, PickOrderItem item)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:SaveException]");
            pickManager.SaveException(OrderId, item, CommonNavInfo.RouteID.ToString());
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:SaveException]");
        }

        // update exception list and made changes in DB data for transaction
        public void UpdateExceptions(PickOrderItem item, string ItemValidity)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:UpdateExceptions][Parameters:item.SerializeToJson():" + item.SerializeToJson() + "]");
            try
            {
                if (ItemValidity == "Over Picked")
                {
                    if (ExceptionItems.Any(i => i.ItemNumber.Trim() == item.ItemNumber.Trim() && i.UM == item.UM))
                    {
                        if (ExceptionItems.FirstOrDefault(i => i.ItemNumber == item.ItemNumber && i.UM == item.UM).ExceptionQtyInPrimaryUOM > 0)
                        {
                            ExceptionItems.FirstOrDefault(i => i.ItemNumber == item.ItemNumber && i.UM == item.UM).PickedQuantityInPrimaryUOM = item.PickedQuantityInPrimaryUOM;
                            ExceptionItems.FirstOrDefault(i => i.ItemNumber == item.ItemNumber && i.UM == item.UM).ExceptionQtyInPrimaryUOM = item.PickedQuantityInPrimaryUOM - item.OrderQtyInPrimaryUOM;
                        }
                        else
                            RemoveItemFromException(item);
                    }
                    else
                    {
                        ExceptionItems.Add(new Models.PickOrderItem()
                        {
                            OrderQtyInPrimaryUOM = item.OrderQtyInPrimaryUOM,
                            PickedQuantityInPrimaryUOM = item.PickedQuantityInPrimaryUOM,
                            ExceptionQtyInPrimaryUOM = item.PickedQuantityInPrimaryUOM - item.OrderQtyInPrimaryUOM,
                            ExceptionQtyInOrderUOM = Convert.ToInt32(item.ExceptionQtyInPrimaryUOM / item.UMConversionFactor),
                            PrimaryUM = item.PrimaryUM,
                            ItemNumber = item.ItemNumber,
                            ItemDescription = item.ItemDescription,
                            ItemStatus = item.ItemStatus,
                            ExceptionReason = ItemValidity,
                            UMConversionFactor = item.UMConversionFactor,
                            UM = item.UM
                        });
                    }
                    if (ExceptionItems.Count > 0)
                    {
                        pickManager.SaveException(Order.OrderId.ToString(), ExceptionItems.FirstOrDefault(i => i.ItemNumber == item.ItemNumber), CommonNavInfo.RouteID.ToString());
                    }
                }
                else if (ItemValidity == "Valid")
                {
                    RemoveItemFromException(item);
                }
                else if (ItemValidity == "Incorrect Item")
                {
                    if (ExceptionItems.Any(i => i.ItemNumber == item.ItemNumber && i.UM == item.UM))
                    {
                        if (ExceptionItems.FirstOrDefault(i => i.ItemNumber == item.ItemNumber && i.UM == item.UM).ExceptionQtyInPrimaryUOM > 0)
                        {
                            ExceptionItems.FirstOrDefault(i => i.ItemNumber == item.ItemNumber && i.UM == item.UM).PickedQuantityInPrimaryUOM = item.PickedQuantityInPrimaryUOM;
                            ExceptionItems.FirstOrDefault(i => i.ItemNumber == item.ItemNumber && i.UM == item.UM).ExceptionQtyInPrimaryUOM = Convert.ToInt32(item.ExceptionQtyInPrimaryUOM - 1);
                            if (item.ExceptionQtyInPrimaryUOM == 0)
                            {
                                RemoveItemFromException(item);
                                //inventoryMgr.UpdateCommittedQtyInTruckToBranchFlow(item.ItemId, 1, false);
                                //inventoryMgr.UpdateTransactionQtyInItemLedger(item, PayloadManager.OrderPayload.OrderID, -1);
                            }
                        }
                        else
                        {
                            RemoveItemFromException(item);
                        }
                    }
                }

                if (ExceptionItems.Count() == 0)
                {
                    OldExceptionCount = 0;
                    NewExceptionCount = 0;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][UpdateExceptions][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:UpdateExceptions]");
        }

        private void RemoveItemFromException(PickOrderItem item)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:RemoveItemFromException]");
            try
            {
                if (ExceptionItems.Any(i => i.ItemNumber == item.ItemNumber && i.UM == item.UM))
                {
                    foreach (var ExceptionItem in ExceptionItems)
                    {
                        if (ExceptionItem.ItemNumber == item.ItemNumber && ExceptionItem.UM == item.UM)
                        {
                            ExceptionItems.Remove(ExceptionItem);
                            break;
                        }
                    }
                }
                pickManager.DeleteException(Order.OrderId.ToString(), item);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][RemoveItemFromException][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:RemoveItemFromException]");
        }

        // comman scan operation function for every pick/ unpick from scanner or manual
        async void ScanOperation(object param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:async ScanOperation]");
            await Task.Factory.StartNew(() =>
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Render,
                  new Action(() =>
                  {
                      //Sathish Changed this code.Disposing the objects
                      Models.PickOrderItem item = null;
                      try
                      {
                          item = param as Models.PickOrderItem;
                          if (item == null) return;

                          //Get correct item on hand quantity
                          item.QtyOnHand = inventoryMgr.GetItemQuantity(string.IsNullOrEmpty(item.ItemId) ? (inventoryMgr.GetItemIdFromNumber(item.ItemNumber)) : item.ItemId).OnHandQty; ;

                          //Adding mode from scanner (Pick list)
                          if (IsAddingToPalette && IsScannerOn && !IsManualPick)
                          {

                              //Return if picked quantity = on hand qty 
                              if (item != null)
                              {
                                  if (item.QtyOnHand == item.PickedQuantityInPrimaryUOM)
                                  {
                                      return;
                                  }
                              }

                              AddingToPalette(item);
                          }

                          //Removing mode from scanner (Exception list)
                          if (!IsAddingToPalette && IsScannerOn && !IsManualPick)
                          {
                              RemovingFromPalette(item);
                          }

                          //Picking using keypad
                          if (IsManualPick || !IsScannerOn)
                          {
                              ManuallyPicking(item);
                          }

                          UpdateExceptions(item, IsValidItem(item));

                          //Updt qty in DB
                          pickManager.UpdatePickQty(Order.OrderId.ToString(), item.ItemNumber, item.PickedQuantityInPrimaryUOM.ToString(), item.UM);

                          if (IsScannerOn)
                          {
                              // Sort Pick List
                              SortPickedItems();
                          }

                          // Change Scanner mode to adding, If there are no any exception
                          if (ExceptionItems.Count == 0)
                          {
                              IsAddingToPalette = true;
                          }

                          // Update All relevant properties
                          OnPropertyChanged("PickOrderItems");
                          OnPropertyChanged("TotalOrderQtyCount");
                          OnPropertyChanged("DoneItemsQtyCount");
                          OnPropertyChanged("UnderPickItemsQtyCount");
                          OnPropertyChanged("OverPickItemsQtyCount");
                          OnPropertyChanged("IsAddingToPalette");
                          OnPropertyChanged("CanRemoveExceptions");

                          //Set selected Item
                          if (IsAddingToPalette)
                          {
                              SelectedPickItem = item;
                          }
                          else
                          {
                              SelectedExceptionItem = item;
                          }
                          OnPropertyChanged("SelectedPickItem");
                          OnPropertyChanged("SelectedExceptionItem");
                          OnPropertyChanged("IsPickCompleted");
                          PayloadManager.OrderPayload.PickOrderItems = PickOrderItems.ToList<PickOrderItem>();

                          if (IsInVoidState && PickOrderItems.Count == 0 && ExceptionItems.Count == 0)
                          {
                              System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                               new Action(() =>
                               {
                                   Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = "Items unpicked successfully" }, MessageToken);
                                   SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                                   PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.Order);
                                   var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.PickOrder, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
                                   Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                               }
                           ));
                          }
                          else if (IsInVoidState && PickOrderItems.Count == 0 && ExceptionItems.Count > 0)
                          {
                              //Let Activity be in VOID state
                          }
                          else
                          {
                              LogActivity(ActivityKey.PickItem);
                          }
                      }
                      catch (Exception ex)
                      {
                          Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][ScanOperation][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                      }
                      finally
                      {
                          item = null;
                      }
                  }));
                Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:ScanOperation]");
            });
        }

        async void ScanOperationOneByOne(object param, bool flag)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:async ScanOperationOneByOne]");
            await Task.Factory.StartNew(() =>
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Render,
                  new Action(() =>
                  {
                      try
                      {
                          Models.PickOrderItem item = param as Models.PickOrderItem;
                          if (item == null) return;

                          //Get correct item on hand quantity
                          item.QtyOnHand = inventoryMgr.GetItemQuantity(string.IsNullOrEmpty(item.ItemId) ? (inventoryMgr.GetItemIdFromNumber(item.ItemNumber)) : item.ItemId).OnHandQty; ;

                          if (IsAddingToPaletteOneByOne)
                          {

                              //Return if picked quantity = on hand qty 
                              if (item != null)
                              {
                                  if (item.QtyOnHand == item.PickedQuantityInPrimaryUOM)
                                  {
                                      return;
                                  }
                              }

                              AddingToPaletteOneByOne(item);
                          }

                          //Removing mode from scanner (Exception list)
                          if (IsAddingToPaletteOneByOne == false)
                          {
                              if (flag == false)
                                  RemovingFromPaletteOneByOne(item);
                              else
                                  RemovingFromPalette(item);
                          }


                          UpdateExceptions(item, IsValidItem(item));

                          //Updt qty in DB
                          pickManager.UpdatePickQty(Order.OrderId.ToString(), item.ItemNumber, item.PickedQuantityInPrimaryUOM.ToString(), item.UM);

                          // Update All relevant properties
                          OnPropertyChanged("PickOrderItems");
                          OnPropertyChanged("TotalOrderQtyCount");
                          OnPropertyChanged("DoneItemsQtyCount");
                          OnPropertyChanged("UnderPickItemsQtyCount");
                          OnPropertyChanged("OverPickItemsQtyCount");
                          OnPropertyChanged("IsAddingToPalette");
                          OnPropertyChanged("CanRemoveExceptions");
                          OnPropertyChanged("SelectedPickItem");
                          OnPropertyChanged("SelectedExceptionItem");
                          OnPropertyChanged("IsPickCompleted");
                          PayloadManager.OrderPayload.PickOrderItems = PickOrderItems.ToList<PickOrderItem>();

                          if (IsInVoidState && PickOrderItems.Count == 0 && ExceptionItems.Count == 0)
                          {
                              System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                               new Action(() =>
                               {
                                   Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = "Items unpicked successfully" }, MessageToken);
                                   SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                                   PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.Order);
                                   var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.PickOrder, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
                                   Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                               }
                           ));
                          }
                          else if (IsInVoidState && PickOrderItems.Count == 0 && ExceptionItems.Count > 0)
                          {
                              //Let Activity be in VOID state
                          }
                          else
                          {
                              LogActivity(ActivityKey.PickItem);
                          }
                      }
                      catch (Exception ex)
                      {
                          Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][async ScanOperationOneByOne][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                      }
                  }));
                Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:async ScanOperationOneByOne]");
            });
        }

        private void SortPickedItems()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:SortPickedItems]");
            PickOrderItems = new TrulyObservableCollection<PickOrderItem>(PickOrderItems.OrderByDescending(x => x.ItemStatus).ThenBy(x => x.ItemNumber));
            ExceptionItems = new TrulyObservableCollection<PickOrderItem>(ExceptionItems.OrderByDescending(x => x.ItemStatus).ThenBy(x => x.ItemNumber));
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:SortPickedItems]");
        }

        // Set Scanner Mode
        private void SetScannerModeByProfile()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:SetScannerModeByProfile]");
            if (UserManager.GetProfileScannerMode() == 1)
            {
                _IsManualPick = false;
                _IsScannerOn = false;
            }
            else
            {
                _IsManualPick = true;
                _IsScannerOn = true;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:SetScannerModeByProfile]");
        }
        #endregion

        #region Variables
        private TrulyObservableCollection<Models.PickOrderItem> _PickOrderItems = new TrulyObservableCollection<PickOrderItem>();
        private ObservableCollection<Models.PickOrderItem> _ExceptionItems = new ObservableCollection<PickOrderItem>();
        private bool _IsAddingToPalette = true, _IsScannerOn = false, _IsManualPick = false, _CanRemoveExceptions = false, _NonStockBilling = false;
        private bool _IsAddingToPaletteOneByOne = true;
        private int _PackSlipNo = new Random().Next(1000, 10000), _ManualEnteredQty, _PickedItemQty;
        private string _OrderNumber, _PickedItemUM, _PickedItemNo;
        ObservableCollection<ReasonCode> _ManualPickReasonList;
        ObservableCollection<ReasonCode> _COAReasonList;
        private Models.PickOrderItem _SelectedPickItem;
        private Models.PickOrderItem _SelectedExceptionItem;
        private Models.PickOrderItem _LastScanItem;
        private Managers.PickManager pickManager;
        private Managers.InvoiceManager invoiceManager = new InvoiceManager();
        private string _ErrorText;
        private ReasonCode ReasonCodeList = new ReasonCode(string.Empty);
        CashDelivery _CashDeliveryContext;
        ChargeOnAccount _ChargeOnAccountContext;
        StrokeCollection _SignStroke;
        Boolean _IsInVoidState;
        private bool _IsBusy = true;
        private string _TotalPickedQty;
        #endregion

        #region Properties

        bool _CanAcceptChargeOnAccount = false;
        public bool CanAcceptChargeOnAccount
        {
            get
            {
                return _CanAcceptChargeOnAccount;
            }
            set
            {
                _CanAcceptChargeOnAccount = value;
                OnPropertyChanged("CanAcceptChargeOnAccount");
            }
        }
        public bool IsBusy
        {
            get
            {
                return _IsBusy;
            }
            set
            {
                _IsBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        public Boolean IsInVoidState
        {
            get
            {
                return (Order.OrderStatus == ActivityKey.VoidAtOrderEntry
                    || Order.OrderStatus == ActivityKey.VoidAtDeliverCustomer
                    || Order.OrderStatus == ActivityKey.VoidAtCashCollection)
                    ? true : false;
            }
            set
            {
                _IsInVoidState = value; OnPropertyChanged("IsInVoidState");
            }
        }
        public string TotalPickedQty
        {
            get { return _TotalPickedQty; }
            set { _TotalPickedQty = value; OnPropertyChanged("TotalPickedQty"); }
        }
        public CanvasViewModel SignatureCanvasVM { get; set; }

        public string ErrorText
        {
            get { return _ErrorText; }
            set { _ErrorText = value; OnPropertyChanged("ErrorText"); }
        }
        public CashDelivery CashDeliveryContext
        {
            get { return _CashDeliveryContext; }
            set
            {
                _CashDeliveryContext = value;
                OnPropertyChanged("CashDeliveryContext");
            }
        }
        public ChargeOnAccount ChargeOnAccountContext
        {
            get { return _ChargeOnAccountContext; }
            set
            {
                _ChargeOnAccountContext = value;
                OnPropertyChanged("ChargeOnAccountContext");
            }
        }

        RequestAuthCodeViewModel _RequestAuthCodeVM;
        public RequestAuthCodeViewModel RequestAuthCodeVM
        {
            get
            {
                return _RequestAuthCodeVM;
            }
            set
            {
                _RequestAuthCodeVM = value;
                OnPropertyChanged("RequestAuthCodeVM");
            }
        }
        public Models.PickOrderItem LastScanItem
        {
            get
            {
                return _LastScanItem;
            }
            set
            {
                _LastScanItem = value;
                OnPropertyChanged("LastScanItem");
            }
        }

        public Models.PickOrderItem SelectedPickItem
        {
            get
            {
                return _SelectedPickItem;
            }
            set
            {
                _SelectedPickItem = value;
                OnPropertyChanged("SelectedPickItem");
            }
        }
        public Models.PickOrderItem SelectedExceptionItem
        {
            get
            {
                return _SelectedExceptionItem;
            }
            set
            {
                _SelectedExceptionItem = value;
                OnPropertyChanged("SelectedExceptionItem");
            }
        }

        public bool CanOpenCashCollection { get; set; }

        public ObservableCollection<ReasonCode> ManualPickReasonList
        {
            get
            {
                return _ManualPickReasonList;
            }
            set
            {
                _ManualPickReasonList = value;
            }
        }
        public StrokeCollection SignStroke
        {
            get { return _SignStroke; }
            set
            {
                _SignStroke = value;
                OnPropertyChanged("SignStroke");
            }
        }
        public string PickedItemNo
        {
            get
            {
                return _PickedItemNo;
            }
            set
            {
                _PickedItemNo = value;
                OnPropertyChanged("PickedItemNo");
            }
        }

        public int PickedItemQty
        {
            get
            {
                return _PickedItemQty;
            }
            set
            {
                _PickedItemQty = value;
                OnPropertyChanged("PickedItemQty");
            }
        }

        public string PickedItemUM
        {
            get
            {
                return _PickedItemUM;
            }
            set
            {
                _PickedItemUM = value;
                OnPropertyChanged("PickedItemUM");
            }
        }

        public int ManualEnteredQty
        {
            get
            {
                return _ManualEnteredQty;
            }
            set
            {
                _ManualEnteredQty = value;
                OnPropertyChanged("ManualEnteredQty");
            }
        }

        public ObservableCollection<ReasonCode> COAReasonList
        {
            get
            {
                return _COAReasonList;
            }
            set
            {
                _COAReasonList = value;
                OnPropertyChanged("COAReasonList");
            }
        }

        ReasonCode _SelectedReasonCode;
        public ReasonCode SelectedReasonCode
        {
            get
            {
                return _SelectedReasonCode;
            }
            set
            {
                _SelectedReasonCode = value;
                CheckForAcceptForChargeOnAccount();
                OnPropertyChanged("SelectedReasonCode");
            }
        }

        private void CheckForAcceptForChargeOnAccount()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:CheckForAcceptForChargeOnAccount]");
            CanAcceptChargeOnAccount = SelectedReasonCode != null && RequestAuthCodeVM.IsAuthCodeValid && SignatureCanvasVM.SignatureAvailable;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:CheckForAcceptForChargeOnAccount]");
        }

        public bool CanRemoveExceptions
        {
            get
            {
                return ExceptionItems.Count > 0 && !PickOrderItems.Any(i => i.OrderQtyInPrimaryUOM > i.PickedQuantityInPrimaryUOM) ? true : false;
            }
            set
            {
                _CanRemoveExceptions = value;
                OnPropertyChanged("CanRemoveExceptions");
            }
        }

        public bool IsManualPick
        {
            get
            {
                return _IsManualPick;
            }
            set
            {
                _IsManualPick = value;
                OnPropertyChanged("IsManualPick");
            }
        }

        public bool IsPickCompleted
        {
            get
            {
                if (this._PickOrderItems != null)
                    return !this._PickOrderItems.Any(w => w.PickedQuantityInPrimaryUOM != w.OrderQtyInPrimaryUOM || (w.StkType != "N" && w.ActualAvailableQty < 0)) && ExceptionItems.Count == 0 && PickOrderItems.Count != 0;
                return false;
            }
            set
            {
                OnPropertyChanged("IsPickCompleted");
            }
        }
        public Guid MessageToken { get; set; }

        public int UnderPickItemsQtyCount
        {
            get
            {
                if (this._PickOrderItems != null) return this._PickOrderItems.Where(w => w.PickedQuantityInPrimaryUOM < w.OrderQtyInPrimaryUOM).Sum(s => s.OrderQtyInPrimaryUOM - s.PickedQuantityInPrimaryUOM);
                return 0;
            }
            set
            {
                OnPropertyChanged("UnderPickItemsQtyCount");
            }
        }
        public int OverPickItemsQtyCount
        {
            get
            {
                if (this._PickOrderItems != null) return this._PickOrderItems.Where(w => w.PickedQuantityInPrimaryUOM > w.OrderQtyInPrimaryUOM).Sum(s => s.PickedQuantityInPrimaryUOM - s.OrderQtyInPrimaryUOM);
                return 0;
            }
            set
            {
                OnPropertyChanged("OverPickItemsQtyCount");
            }
        }
        public bool IsAddingToPalette
        {
            get
            {
                return _IsAddingToPalette;
            }
            set
            {
                _IsAddingToPalette = value;
                OnPropertyChanged("IsAddingToPalette");

            }
        }

        public bool IsAddingToPaletteOneByOne
        {
            get
            {
                return _IsAddingToPaletteOneByOne;
            }
            set
            {
                _IsAddingToPaletteOneByOne = value;
                OnPropertyChanged("IsAddingToPaletteOneByOne");

            }
        }

        public bool NonStockBilling
        {
            get
            {
                return _NonStockBilling;
            }
            set
            {
                _NonStockBilling = value;
                OnPropertyChanged("NonStockBilling");
            }
        }
        public bool IsScannerOn
        {
            get
            {
                return _IsScannerOn;
            }
            set
            {
                _IsScannerOn = value;
                OnPropertyChanged("IsScannerOn");

            }
        }
        public int PackSlipNo
        {
            get { return _PackSlipNo; }
            set
            {
                _PackSlipNo = value;
                OnPropertyChanged("PackSlipNo");
            }
        }
        public int DoneItemsQtyCount
        {
            get
            {
                if (this._PickOrderItems != null) return this._PickOrderItems.Sum(s => s.PickedQuantityInPrimaryUOM);
                return 0;
            }
            set
            {
                OnPropertyChanged("DoneItemsQtyCount");
            }
        }

        public int TotalOrderQtyCount
        {
            get
            {
                if (this._PickOrderItems != null) return this._PickOrderItems.Sum(s => s.OrderQtyInPrimaryUOM);
                return 0;
            }
            set
            {
                OnPropertyChanged("TotalOrderQtyCount");
            }
        }
        private Models.Customer _Customer;
        public Models.Customer Customer
        {
            get
            {
                if (_Customer == null)
                {
                    _Customer = new Models.Customer();
                    _Customer.Name = "STOP N SHOP";
                    _Customer.CustomerNo = "121343";
                    _Customer.Phone = "9878767655";
                    _Customer.Route = "783";
                    _Customer.City = "Oklahoma City";
                    _Customer.State = "OK";
                    _Customer.Zip = "72132";
                }
                return _Customer;
            }
            set
            {
                _Customer = value;
            }
        }

        public string OrderNumber
        {
            get { return Order.OrderId.ToString(); }
            set
            {
                if (value != _OrderNumber)
                {
                    _OrderNumber = value;
                    OnPropertyChanged("OrderNumber");
                }
            }

        }
        public TrulyObservableCollection<Models.PickOrderItem> PickOrderItems
        {
            get { return _PickOrderItems; }
            set
            {
                _PickOrderItems = value;
                OnPropertyChanged("PickOrderItems");
            }
        }

        public ObservableCollection<Models.PickOrderItem> ExceptionItems
        {
            get { return _ExceptionItems; }
            set
            {
                _ExceptionItems = value;
                OnPropertyChanged("ExceptionItems");
            }
        }
        #endregion

        public string Error
        {
            get { return ErrorText; }

        }

        public string this[string columnName]
        {
            get
            {
                return ErrorText;
            }
        }

        public int OldExceptionCount { get; set; }
        public int NewExceptionCount { get; set; }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {

                if (pickManager != null)
                    pickManager.Dispose();
                if (objOrderManger != null)
                    objOrderManger.Dispose();
                if (invoiceManager != null)
                    invoiceManager.Dispose();
                if (inventoryMgr != null)
                    inventoryMgr.Dispose();
                if (ReasonCodeList != null)
                    ReasonCodeList.Dispose();
                if (selectedReason != null)
                    selectedReason.Dispose();
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
