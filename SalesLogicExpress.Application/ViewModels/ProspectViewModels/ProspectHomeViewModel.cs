﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.ObjectModel;
using Managers = SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System.Media;
using Entities = SalesLogicExpress.Domain;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using Telerik.Windows.Controls;
using System.Globalization;
using SalesLogicExpress.Application.Managers;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using log4net;
using SalesLogicExpress.Domain.Prospect_Models;


namespace SalesLogicExpress.Application.ViewModels.ProspectViewModels
{
    public class ProspectHomeViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.ProspectViewModels.ProspectHomeViewModel");

        #region Properties & Variables
        string selectedTab = string.Empty;
        public Guid MessageToken { get; set; }

        #endregion

        #region OnPropertyChanged

        #region Bool
        private bool isDashboardTabSelected = false;

        public bool IsDashboardTabSelected
        {
            get { return isDashboardTabSelected; }
            set
            {
                isDashboardTabSelected = value; OnPropertyChanged("IsDashboardTabSelected");
                if (IsDashboardTabSelected)
                {
                    if (DashboardVM == null)
                    {
                        DashboardVM = new DashboardViewModel(MessageToken, this);
                    }
                    selectedTab = "Dashboard";
                    IsBusy = false;
                }
            }
        }
        private bool isNotesTabSelected = false;

        public bool IsNotesTabSelected
        {
            get { return isNotesTabSelected; }
            set
            {
                isNotesTabSelected = value; OnPropertyChanged("IsNotesTabSelected");
                if (IsNotesTabSelected)
                {
                    if (NotesVM == null)
                    {
                        NotesVM = new NotesViewModel(MessageToken, this);
                    }
                    NotesVM.ViewModelUpdated += NotesVM_ViewModelUpdated;
                    selectedTab = "Notes";
                    IsBusy = false;

                }
            }
        }

        bool isProspectTabSelected;
        public bool IsProspectTabSelected
        {
            get
            {
                return isProspectTabSelected;
            }
            set
            {
                isProspectTabSelected = value;
                if (isProspectTabSelected && ProspectActivityVM == null)
                {
                    ProspectActivityVM = new ProspectActivityViewModel(MessageToken, this);
                    //ProspectActivityVM = new ProspectActivityViewModel();
                    selectedTab = "Activity";
                    IsBusy = false;

                }
                OnPropertyChanged("IsProspectTabSelected");
            }
        }

        private bool isInformationTabSelected = false;

        public bool IsInformationTabSelected
        {
            get { return isInformationTabSelected; }
            set
            {
                isInformationTabSelected = value; OnPropertyChanged("IsInformationTabSelected");
                if (IsInformationTabSelected)
                {
                    if (InformationVM == null)
                    {
                        InformationVM = new InformationViewModel(MessageToken, this);
                    }
                    selectedTab = "Information";
                    IsBusy = false;
                }
            }
        }
        private bool isContactTabSelected = false;

        public bool IsContactTabSelected
        {
            get { return isContactTabSelected; }
            set
            {
                isContactTabSelected = value; OnPropertyChanged("IsContactTabSelected");
                if (IsContactTabSelected)
                {
                    if (ContactVM == null)
                    {
                        ContactVM = new ContactViewModel(MessageToken, this);
                    }
                    selectedTab = "Contact";
                    IsBusy = false;

                }
            }
        }
        private bool isQuotesTabSelected = false;

        public bool IsQuotesTabSelected
        {
            get { return isQuotesTabSelected; }
            set
            {
                isQuotesTabSelected = value; OnPropertyChanged("IsQuotesTabSelected");
                if (IsQuotesTabSelected)
                {
                    if (QuotesVM == null)
                    {
                        QuotesVM = new CustomerQuoteTabViewModel(MessageToken, true, this);
                    }
                    selectedTab = "Quotes";
                    IsBusy = false;

                }

            }
        }
        private bool isPricingTabSelected = false;

        public bool IsPricingTabSelected
        {
            get { return isPricingTabSelected; }
            set
            {
                isPricingTabSelected = value; OnPropertyChanged("IsPricingTabSelected");
                if (IsPricingTabSelected)
                {
                    if (PricingVM == null)
                    {
                        PricingVM = new PricingViewModel(MessageToken, this);
                    }
                    selectedTab = "Pricing";
                    IsBusy = false;

                }
                else if (!IsPricingTabSelected && IsDirty && PricingVM != null)
                {
                    //Call Model Popup
                    ModalPopupOnTabChange("Pricing", selectedTab.Clone().ToString());
                }
            }
        }
        private bool isCompetitorInformationTabSelected = false;

        public bool IsCompetitorInformationTabSelected
        {
            get { return isCompetitorInformationTabSelected; }
            set
            {
                isCompetitorInformationTabSelected = value; OnPropertyChanged("IsCompetitorInformationTabSelected");
                if (IsCompetitorInformationTabSelected)
                {
                    if (CompetitorInformationVM == null)
                    {
                        CompetitorInformationVM = new CompetitorInformationViewModel(MessageToken, this);
                    }
                    selectedTab = "CompetitorInformation";
                    IsBusy = false;

                }
            }
        }

        #endregion

        #region Int

        #endregion

        #region string


        #endregion

        #region Date

        #endregion

        #region Collection


        ProspectActivityViewModel prospectActivityVM;
        public ProspectActivityViewModel ProspectActivityVM
        {
            get
            {
                return prospectActivityVM;
            }
            set
            {


                prospectActivityVM = value;
                OnPropertyChanged("ProspectActivityVM");
            }
        }
        private ProspectModel prospect = new ProspectModel();
        public ProspectModel Prospect
        {
            get { return prospect; }
            set { prospect = value; OnPropertyChanged("Prospect"); }
        }


        private DashboardViewModel dashboardVM = null;

        public DashboardViewModel DashboardVM
        {
            get { return dashboardVM; }
            set { dashboardVM = value; OnPropertyChanged("DashboardVM"); }
        }
        private NotesViewModel notesVM = null;

        public NotesViewModel NotesVM
        {
            get { return notesVM; }
            set { notesVM = value; OnPropertyChanged("NotesVM"); }
        }
        private InformationViewModel informationVM = null;

        public InformationViewModel InformationVM
        {
            get { return informationVM; }
            set { informationVM = value; OnPropertyChanged("InformationVM"); }
        }


        private ContactViewModel contactVM = null;

        public ContactViewModel ContactVM
        {
            get { return contactVM; }
            set { contactVM = value; OnPropertyChanged("ContactVM"); }
        }
        private PricingViewModel pricingVM = null;

        public PricingViewModel PricingVM
        {
            get { return pricingVM; }
            set { pricingVM = value; OnPropertyChanged("PricingVM"); }
        }
        private CustomerQuoteTabViewModel quotesVM = null;

        public CustomerQuoteTabViewModel QuotesVM
        {
            get { return quotesVM; }
            set { quotesVM = value; OnPropertyChanged("QuotesVM"); }
        }
        private CompetitorInformationViewModel competitorInformationVM = null;

        public CompetitorInformationViewModel CompetitorInformationVM
        {
            get { return competitorInformationVM; }
            set { competitorInformationVM = value; OnPropertyChanged("CompetitorInformationVM"); }
        }

        private CustomerQuoteViewModel customerQuoteVM = null;

        public CustomerQuoteViewModel CustomerQuoteVM
        {
            get { return customerQuoteVM; }
            set { customerQuoteVM = value; }
        }


        #endregion

        #endregion

        #region Commands Declaretion
        public DelegateCommand ClearSearchText { get; set; }


        #endregion

        #region Command Defination

        public void InitialiseCommands()
        {
            ClearSearchText = new DelegateCommand((param) =>
            {

            });

        }

        #endregion

        public ProspectHomeViewModel(Guid token)
        {
            IsBusy = true;
            MessageToken = token;
            Prospect = PayloadManager.ProspectPayload.Prospect;
            ViewModelPayload.PayloadManager.ProspectPayload.IsProspect = true;
        }

        public override bool ConfirmSave()
        {
            this.IsDirty = false;
            GetActiveTab().ConfirmSave();
            return base.ConfirmSave();

        }

        public override bool ConfirmCancel()
        {
            this.IsDirty = false;
            GetActiveTab().ConfirmCancel();
            return base.ConfirmCancel();
        }

        private void ModalPopupOnTabChange(string forTab, string currentSeleced)
        {
            string confirmationMessage = "Do you want to save changes made?";
            ConfirmWindow confirmReadOnlyNavigation = new Helpers.ConfirmWindow { OkButtonContent = "Yes", CancelButtonContent = "No" };
            confirmReadOnlyNavigation.Message = confirmationMessage;
            Messenger.Default.Send<Helpers.ConfirmWindow>(confirmReadOnlyNavigation, MessageToken);

            if (confirmReadOnlyNavigation.Confirmed)
            {
                IsDirty = false;
                GetChangedTab(forTab).ConfirmSave();
            }
            else if (confirmReadOnlyNavigation.Cancelled || confirmReadOnlyNavigation.Closed)
            {
                IsDirty = false;
                GetChangedTab(forTab).ConfirmCancel();
                switch (forTab)
                {
                    case "CompetitorInformation":
                        CompetitorInformationVM = null;
                        break;
                    case "Pricing":
                        PricingVM = null;
                        break;
                    case "Quotes":
                        QuotesVM = null;
                        break;
                    case "Contact":
                        ContactVM = null;
                        break;
                    case "Information":
                        InformationVM = null;
                        break;
                    case "Notes":
                        NotesVM = null;
                        break;
                    case "Dashboard":
                        DashboardVM = null;
                        break;
                    default:
                        break;
                }
            }
            else if (confirmReadOnlyNavigation.Closed)
            {

                //switch (forTab)
                //{
                //    case "CompetitorInformation":
                //        IsCompetitorInformationTabSelected = true;
                //        break;
                //    case "Pricing":
                //        IsPricingTabSelected = true;

                //        break;
                //    case "Quotes":
                //        IsQuotesTabSelected = true;

                //        break;
                //    case "Contact":
                //        IsContactTabSelected = true;

                //        break;
                //    case "Information":
                //        IsInformationTabSelected = true;

                //        break;
                //    case "Notes":
                //        IsNotesTabSelected = true;

                //        break;
                //    case "Dashboard":
                //        IsDashboardTabSelected = true;
                //        break;

                //    default:
                //        break;
                //}

                //switch (currentSeleced)
                //{
                //    case "CompetitorInformation":
                //        IsCompetitorInformationTabSelected = false;
                //        break;
                //    case "Pricing":
                //        IsPricingTabSelected = false;

                //        break;
                //    case "Quotes":
                //        IsQuotesTabSelected = false;

                //        break;
                //    case "Contact":
                //        IsContactTabSelected = false;

                //        break;
                //    case "Information":
                //        IsInformationTabSelected = false;

                //        break;
                //    case "Notes":
                //        IsNotesTabSelected = false;

                //        break;
                //    case "Dashboard":
                //        IsDashboardTabSelected = false;

                //        break;

                //    default:
                //        break;
                //}
            }
        }
        BaseViewModel GetActiveTab()
        {
            BaseViewModel bmv = null;
            switch (selectedTab)
            {
                case "CompetitorInformation":
                    bmv = CompetitorInformationVM;
                    break;
                case "Pricing":
                    bmv = PricingVM;
                    break;
                case "Quotes":
                    bmv = QuotesVM;
                    break;
                case "Contact":
                    bmv = ContactVM;

                    break;
                case "Information":
                    bmv = InformationVM;

                    break;
                case "Notes":
                    bmv = NotesVM;

                    break;
                case "Dashboard":
                    bmv = DashboardVM;

                    break;

                default:
                    break;
            }
            return bmv;
        }

        BaseViewModel GetChangedTab(string changedTab)
        {
            BaseViewModel bmv = null;
            switch (changedTab)
            {
                case "CompetitorInformation":
                    bmv = CompetitorInformationVM;
                    break;
                case "Pricing":
                    bmv = PricingVM;
                    break;
                case "Quotes":
                    bmv = QuotesVM;
                    break;
                case "Contact":
                    bmv = ContactVM;

                    break;
                case "Information":
                    bmv = InformationVM;

                    break;
                case "Notes":
                    bmv = NotesVM;

                    break;
                case "Dashboard":
                    bmv = DashboardVM;

                    break;

                default:
                    break;
            }
            return bmv;
        }
        void NotesVM_ViewModelUpdated(object sender, ProspectNotesVMArgs e)
        {
            DashboardVM.SetDefaultNote(e.Note);
        }
    }
}
