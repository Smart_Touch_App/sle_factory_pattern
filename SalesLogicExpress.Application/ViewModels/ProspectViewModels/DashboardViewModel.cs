﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.ObjectModel;
using Managers = SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System.Media;
using Entities = SalesLogicExpress.Domain;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using Telerik.Windows.Controls;
using System.Globalization;
using SalesLogicExpress.Application.Managers;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using log4net;
using SalesLogicExpress.Domain.Prospect_Models;


namespace SalesLogicExpress.Application.ViewModels.ProspectViewModels
{
    public class DashboardViewModel : BaseViewModel
    {
        #region Properties & Variables
        public Guid MessageToken { get; set; }
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.ProspectViewModels.DashboardViewModel");
        ProspectManager prospectManager = new ProspectManager();
        StopManager stopManager = new StopManager();
        ServiceRouteManager serviceRouteM = new ServiceRouteManager();
        ObservableCollection<Customer> todaysStops = new ObservableCollection<Customer>();


        #endregion

        #region OnPropertyChanged

        #region Bool


        bool isMoveButtonForDialog = true;
        public bool IsMoveButtonForDialog
        {
            get
            {
                return isMoveButtonForDialog;
            }
            set
            {


                isMoveButtonForDialog = value;
                OnPropertyChanged("IsMoveButtonForDialog");
            }
        }


        bool isCreateButtonForDialog = true;
        public bool IsCreateButtonForDialog
        {
            get
            {
                return isCreateButtonForDialog;
            }
            set
            {


                isCreateButtonForDialog = value;
                OnPropertyChanged("IsCreateButtonForDialog");
            }
        }

        private bool isCreateEnabled = true;
        public bool IsCreateEnabled
        {
            get
            {
                return this.isCreateEnabled;
            }
            set
            {
                this.isCreateEnabled = value;
                OnPropertyChanged("IsCreateEnabled");
            }
        }


        bool isMoveButton = true;
        public bool IsMoveButton
        {
            get
            {
                if (Prospect != null && Prospect.HasActivity)
                    isMoveButton = false;
                return isMoveButton;

            }
            set
            {


                isMoveButton = value;
                OnPropertyChanged("IsMoveButton");
            }
        }

        bool noInformationAvailable;
        public bool NoInformationAvailable
        {
            get
            {
                return noInformationAvailable;
            }
            set
            {


                noInformationAvailable = value;
                if (value) // if no information is unavailable then no need to show move button
                    IsMoveButton = false;
                OnPropertyChanged("NoInformationAvailable");
            }
        }

        private bool isProspectTab = false;
        public bool IsProspectTab
        {
            get
            {
                return isProspectTab;
            }
            set
            {
                isProspectTab = value;
                OnPropertyChanged("IsProspectTab");
            }
        }

        private bool isNextStopDate = false;
        public bool IsNextStopDate
        {
            get { return isNextStopDate; }
            set { isNextStopDate = value; OnPropertyChanged("IsNextStopDate"); }
        }
        private bool isTodayDate = false;
        public bool IsTodayDate
        {
            get { return isTodayDate; }
            set { isTodayDate = value; OnPropertyChanged("IsTodayDate"); }
        }

        public bool IsFromListingTab { get; set; }


        bool isMoveDialogOpened;
        public bool IsMoveDialogOpened
        {
            get
            {
                return isMoveDialogOpened;
            }
            set
            {


                isMoveDialogOpened = value;
                OnPropertyChanged("IsMoveDialogOpened");
            }
        }

        public bool IsStopCreatedOrMoved { get; set; }
        public bool StopDialogBypassed { get; set; }



        bool isNoteEmpty = false;
        public bool IsNoteEmpty
        {
            get
            {
                return isNoteEmpty;
            }
            set
            {


                isNoteEmpty = value;
                OnPropertyChanged("IsNoteEmpty");
            }
        }

        bool activityPerformed;
        public bool ActivityPerformed
        {
            get
            {
                if (Prospect != null && Prospect.HasActivity)
                {
                    activityPerformed = true;
                    IsMoveButton = false;
                }
                return activityPerformed;
            }
            set
            {


                activityPerformed = value;
                OnPropertyChanged("ActivityPerformed");
            }
        }
        #endregion

        #region Int

        #endregion

        #region string


        #endregion

        #region Date

        DateTime? _stopDate;
        public DateTime? StopDate
        {
            get
            {
                return _stopDate;
            }
            set
            {


                _stopDate = value;
                OnPropertyChanged("StopDate");
            }
        }

        private DateTime todayDate = DateTime.Today;
        public DateTime TodayDate
        {
            get
            {
                return todayDate;
            }
            set
            {
                todayDate = value;
                OnPropertyChanged("TodayDate");
            }
        }

        private DateTime? nextDate;
        public DateTime? NextDate
        {
            get { return nextDate; }
            set { nextDate = value; OnPropertyChanged("NextDate"); }
        }
        private DateTime? lastDate;
        public DateTime? LastDate
        {
            get { return lastDate; }
            set { lastDate = value; OnPropertyChanged("LastDate"); }
        }
        #endregion

        #region Collection
        private CreateAndMoveStopViewModel _CreateStopViewModelProperty;
        public CreateAndMoveStopViewModel CreateStopViewModelProperty
        {
            get
            {
                return _CreateStopViewModelProperty;
            }
            set
            {
                _CreateStopViewModelProperty = value;
                OnPropertyChanged("CreateStopViewModelProperty");
            }
        }

        private CreateAndMoveStopViewModel _MoveStopViewModelProperty;
        public CreateAndMoveStopViewModel MoveStopViewModelProperty
        {
            get
            {
                return _MoveStopViewModelProperty;
            }
            set
            {
                _MoveStopViewModelProperty = value;
                OnPropertyChanged("MoveStopViewModelProperty");
            }
        }
        #endregion

        #endregion

        #region Prospect

        ProspectContact defaultContact;
        public ProspectContact DefaultContact
        {
            get
            {
                return defaultContact;
            }
            set
            {


                defaultContact = value;
                OnPropertyChanged("DefaultContact");
            }
        }

        ProspectNoteModel _DefaultNoteProspect;

        public ProspectNoteModel DefaultNoteProspect
        {
            get
            {
                return _DefaultNoteProspect;
            }
            set
            {
                _DefaultNoteProspect = value;
                OnPropertyChanged("DefaultNoteProspect");
            }
        }



        Prospect prospect;
        public Prospect Prospect
        {
            get
            {
                return prospect;
            }
            set
            {


                prospect = value;
                OnPropertyChanged("Prospect");
            }
        }
        #endregion

        #region Commands Declaretion
        public DelegateCommand ClearSearchText { get; set; }
        public DelegateCommand ShowCreateStopDialog { get; set; }
        public DelegateCommand ShowMoveStopDialog { get; set; }
        public DelegateCommand MoveStopOnToday { get; set; }
        public DelegateCommand CreateStopForToday { get; set; }
        #endregion

        #region Command Defination

        public void InitialiseCommands()
        {
            ClearSearchText = new DelegateCommand((param) =>
            {

            });

        }

        #endregion

        public DashboardViewModel(Guid token, BaseViewModel Parent)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][Start:DashboardViewModel]");
            try
            {
                MessageToken = token;
                this.ParentView = Parent;
                LoadData();
                InitializeCommands();
                this.ParentView.IsBusy = false;
                if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.ServiceRoute_Dailystop.ToString())
                {
                    this.ParentView.SelectedTab = ViewModelMappings.TabView.ServiceRoute.DailyStop;
                }
                else if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.ServiceRoute_Prospect.ToString())
                {
                    this.ParentView.SelectedTab = ViewModelMappings.TabView.ServiceRoute.ProspectTab;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][DashboardViewModel][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][End:DashboardViewModel]");
        }

        public DashboardViewModel()
        {
            // TODO: Complete member initialization
        }

        #region Methods
        private void InitializeCommands()
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModel.ProspectViewModel][DashboardViewModel][Start:InitializeCommands]");
            try
            {
                ShowCreateStopDialog = new DelegateCommand((param) =>
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][Start:ShowCreateStopDialog]");
                    try
                    {

                        IsCreateEnabled = false;
                        DateTime createStopDate;
                        var visitee = Prospect as Prospect;
                        CreateStopViewModelProperty = new CreateAndMoveStopViewModel(visitee);
                        CreateStopViewModelProperty.StopModified += CreateStopViewModelProperty_StopCreated;
                        if (visitee.StopDate.HasValue)
                        {
                            if (visitee.StopDate.Value < TodayDate.Date)
                            {
                                createStopDate = TodayDate.Date;
                            }
                            else
                            {
                                createStopDate = visitee.StopDate.Value;
                            }
                        }
                        else
                        {
                            createStopDate = TodayDate.Date;
                        }
                        visitee.IsFromListingTab = IsProspectTab;
                        stopManager.CreateStopDialogContext(ref _CreateStopViewModelProperty, visitee, createStopDate, MessageToken);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][ShowCreateStopDialog][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][End:ShowCreateStopDialog]");
                });

                ShowMoveStopDialog = new DelegateCommand(param =>
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][Start:ShowMoveStopDialog]");
                    try
                    {
                        MoveStopViewModelProperty = new CreateAndMoveStopViewModel(Prospect);

                        MoveStopViewModelProperty.StopModified += MoveStopViewModelProperty_StopMoved;

                        // Enable already moved stop to move again on original date
                        DateTime moveStopBaseDate = new DateTime();

                        moveStopBaseDate = Prospect.StopDate.Value;
                        IsFromListingTab = Prospect.IsFromListingTab;
                        IsMoveDialogOpened = true;
                        stopManager.MoveStopDialogContext(ref _MoveStopViewModelProperty, Prospect, moveStopBaseDate, MessageToken);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][ShowMoveStopDialog][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][End:ShowMoveStopDialog]");

                });
                MoveStopOnToday = new DelegateCommand(param =>
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][Start:MoveStopOnToday]");
                    try
                    {
                        if (IsMoveButtonForDialog)
                        {
                            IsMoveButtonForDialog = false;
                            MoveStopViewModelProperty = new CreateAndMoveStopViewModel(Prospect);

                            MoveStopViewModelProperty.Visitee = Prospect;
                            MoveStopViewModelProperty.DailyStopDate = Prospect.StopDate.Value;
                            StopDialogBypassed = true;

                            MoveStopViewModelProperty.StopModified += MoveStopViewModelProperty_StopMoved;
                            MoveStopViewModelProperty.MoveStop.Execute(TodayDate.Date);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][MoveStopOnToday][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][End:MoveStopOnToday]");
                });

                CreateStopForToday = new DelegateCommand(param =>
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][Start:CreateStopForToday]");
                    try
                    {
                        if (IsCreateButtonForDialog)
                        {
                            IsCreateButtonForDialog = false;
                            CreateStopViewModelProperty = new CreateAndMoveStopViewModel(Prospect);

                            CreateStopViewModelProperty.Visitee = Prospect;
                            StopDialogBypassed = true;

                            CreateStopViewModelProperty.StopModified += CreateStopViewModelProperty_StopCreated;
                            CreateStopViewModelProperty.CreateStop.Execute(TodayDate.Date);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][CreateStopForToday][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][End:CreateStopForToday]");
                });
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModel.ProspectViewModel][DashboardViewModel][InitializeCommands][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModel.ProspectViewModel][DashboardViewModel][End:InitializeCommands]");
        }

        private void MoveStopViewModelProperty_StopMoved(object sender, StopActionCompletedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][Start:MoveStopViewModelProperty_StopMoved]");
            try
            {
                Prospect = GetNewProspect(e.DateSelected.Value);
                StopDate = NextDate = Prospect.StopDate;
                PayloadManager.ProspectPayload.Prospect = Prospect;
                if (IsProspectTab)
                {
                    LoadStopInfo();
                    Prospect = GetNewProspect(StopDate.Value);
                }
                if ((ParentView as ProspectsHome).IsQuotesTabSelected || (ParentView as ProspectsHome).IsNotesTabSelected)
                    IsStopCreatedOrMoved = true;
                IsMoveDialogOpened = false;

                ///Get new prospect if it has been created by "MoveStopOnToday" command
                if (StopDialogBypassed)
                    LoadStopInfo();

                IsMoveButtonForDialog = true;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModel.ProspectViewModel][DashboardViewModel][MoveStopViewModelProperty_StopMoved][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModel.ProspectViewModel][DashboardViewModel][End:MoveStopViewModelProperty_StopMoved]");
        }

        private void CreateStopViewModelProperty_StopCreated(object sender, StopActionCompletedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][Start:CreateStopViewModelProperty_StopCreated]");
            try
            {
                NoInformationAvailable = false;
                if (IsProspectTab)
                {
                    LoadStopInfo();

                    Prospect = !StopDate.HasValue ? GetNewProspect(TodayDate) : GetNewProspect(StopDate.Value);
                }

                if (StopDialogBypassed)
                {
                    Prospect = GetNewProspect(TodayDate.Date);
                    StopDate = NextDate = Prospect.StopDate;
                }

                IsMoveButton = true;
                if ((ParentView as ProspectsHome).IsQuotesTabSelected || (ParentView as ProspectsHome).IsNotesTabSelected)
                    IsStopCreatedOrMoved = true;

                IsCreateButtonForDialog = true;

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][CreateStopViewModelProperty_StopCreated][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][End:CreateStopViewModelProperty_StopCreated]");
        }

        private void LoadData()
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][Start:LoadData]");
            try
            {
                Prospect = PayloadManager.ProspectPayload.Prospect;
                DefaultContact = prospectManager.GetDefaultContact(Convert.ToInt32(Prospect.ProspectID));
                DefaultNoteProspect = new SalesLogicExpress.Application.Managers.ProspectManagers.NotesManager().GetProspectNotes(Prospect.ProspectID).FirstOrDefault(x => x.IsDefault);
                if (DefaultNoteProspect != null)
                    DefaultNoteProspect.ProspectNoteDetails = DefaultNoteProspect.ProspectNoteDetails.Replace("\n", "").Replace("\r", " ");
                else
                    IsNoteEmpty = true;
                IsProspectTab = Prospect.IsFromListingTab;

                if (!IsProspectTab)
                {
                    StopDate = Prospect.StopDate;
                    IsNextStopDate = true;
                }
                else
                    LoadStopInfo();

                CheckForActivity();

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][LoadData][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][DashboardViewModel][End:LoadData]");
        }

        private void CheckForActivity()
        {
            if (Prospect.HasActivity)
            {
                ActivityPerformed = true;
                IsMoveButton = false;
            }
        }

        private void LoadStopInfo()
        {

            Logger.Info("[SalesLogicExpress.Application.ProspectViewModel.DashboardViewModel][DashboardViewModel][Start:LoadStopInfo]");
            try
            {
                Dictionary<string, string> nextLastStopInfo = null;
                var visitee = Prospect as Visitee;
                nextLastStopInfo = serviceRouteM.GetVisiteeStopInfo(visitee, TodayDate, true, true);

                if (!(nextLastStopInfo["LastStop"] == "None"))
                {
                    // ternary op is not used as null is not getting assigned to DateTime
                    if ((nextLastStopInfo["NextStop"] == "None"))
                    {
                        StopDate = NextDate = null;
                        IsMoveButton = false;
                        NoInformationAvailable = true;
                    }
                    else
                    {
                        StopDate = DateTime.Parse(nextLastStopInfo["NextStop"], CultureInfo.CreateSpecificCulture("en-US"));
                    }
                    if ((nextLastStopInfo["LastStop"] == "None"))
                    {
                        LastDate = null;
                    }
                    else
                    {
                        LastDate = DateTime.Parse(nextLastStopInfo["LastStop"], CultureInfo.CreateSpecificCulture("en-US"));
                    }
                    Prospect.StopDate = StopDate;
                }
                else
                {
                    if (nextLastStopInfo["NextStop"] == "None")
                        NextDate = null;
                    else
                    {
                        StopDate = NextDate = DateTime.Parse(nextLastStopInfo["NextStop"], CultureInfo.CreateSpecificCulture("en-US"));
                        IsNextStopDate = true;

                    }
                    Prospect.StopDate = StopDate;
                }

                if (nextLastStopInfo["NextStop"] == "None" && nextLastStopInfo["LastStop"] == "None")
                {
                    NoInformationAvailable = true;
                    IsMoveButton = false;
                }
                if (StopDate.HasValue)
                {
                    Prospect = GetNewProspect(StopDate.Value);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ProspectViewModel.DashboardViewModel][DashboardViewModel][LoadStopInfo][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ProspectViewModel.DashboardViewModel][DashboardViewModel][End:LoadStopInfo]");
        }

        public void SetDefaultNote(ProspectNoteModel note)
        {
            if (note != null)
            {
                note.ProspectNoteDetails = note.ProspectNoteDetails.Replace("\n", "").Replace("\r", " ");
                DefaultNoteProspect = note;
            }
        }

        public Prospect GetNewProspect(DateTime tempNextDate, string pProspectId = "")
        {

            Domain.Customer cust = new Domain.Customer();
            Prospect prospect = new Prospect();
            var prospectId = string.IsNullOrEmpty(pProspectId) ? Prospect.ProspectID : pProspectId;
            try
            {
                ObservableCollection<Domain.Customer> tempCollection = new ObservableCollection<Entities.Customer>();
                ServiceRouteManager serviceRouteManager = new ServiceRouteManager();
                tempCollection = serviceRouteManager.GetCustomersForRoute(CommonNavInfo.RouteUser, tempNextDate);
                cust = tempCollection.FirstOrDefault(c => (c.CustomerNo == prospectId));
                prospect = (Prospect)(cust);
                prospect.IsFromListingTab = IsProspectTab;
                PayloadManager.ProspectPayload.Prospect = prospect;
                if (prospect.HasActivity && prospect.StopDate.Value.Date == TodayDate.Date)
                {
                    activityPerformed = true;
                    IsMoveButton = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("DashboardViewModel DashboardViewModel([GetNewProspect]" + StopDate.ToString() + ") error: " + ex.Message);
            }

            return prospect;
        }

        public bool GetCreateMoveStopPopUp()
        {
            var dialog = new Helpers.DialogWindow { TemplateKey = "StopDialogForProspect", Title = "Create or Move Stop", Payload = null };
            Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);

            //if (dialog.Closed || dialog.Cancelled)
            //    return false;
            return IsStopCreatedOrMoved;
        }
        internal bool VerifyStopInfo(Prospect Prospect)
        {
            var result = true;
            Logger.Info("[SalesLogicExpress.Application.ProspectViewModel.DashboardViewModel][DashboardViewModel][Start:VerifyStopInfo]");
            try
            {
                todaysStops = serviceRouteM.GetCustomersForRoute(CommonNavInfo.RouteUser, TodayDate.Date);
                var isStopPresent = todaysStops.Any(x => x.CustomerNo.Trim() == Prospect.ProspectID.Trim());
                Prospect.IsTodaysStop = isStopPresent;
                if (Prospect.IsTodaysStop && Prospect.StopDate.Value.Date != TodayDate.Date)
                {
                    GetAlertPopup();
                    return false;
                }
                if (Prospect.IsTodaysStop)
                    return true;

                if (!Prospect.StopDate.HasValue)
                {
                    IsMoveButtonForDialog = false;
                    return result = GetCreateMoveStopPopUp();
                }
                if ((DateTime.Compare(Prospect.StopDate.Value.Date, TodayDate.Date) > 0))
                {
                    IsMoveButtonForDialog = true;
                    result = GetCreateMoveStopPopUp();

                }
                if (DateTime.Compare(Prospect.StopDate.Value.Date, TodayDate.Date) < 0)
                {
                    //Domain.Prospect_Models.ProspectNoteModel prospect = GetNewProspect(StopDate.Value);
                    Prospect prospect = GetNewProspect(StopDate.Value);
                    if (prospect.HasActivity)
                    {
                        IsMoveButtonForDialog = false;
                    }
                    result = GetCreateMoveStopPopUp();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ProspectViewModel.DashboardViewModel][DashboardViewModel][VerifyStopInfo][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ProspectViewModel.DashboardViewModel][DashboardViewModel][End:VerifyStopInfo]");
            return result;
        }

        public void GetAlertPopup()
        {
            var alartMessage = new Helpers.AlertWindow { Message = Helpers.Constants.PreTripInspection.AlertMessageForFutureStop, MessageIcon = "Alert" };
            Messenger.Default.Send<Helpers.AlertWindow>(alartMessage, MessageToken);
        }
        #endregion



    }
}
