﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.ObjectModel;
using Managers = SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System.Media;
using Entities = SalesLogicExpress.Domain;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using Telerik.Windows.Controls;
using System.Globalization;
using SalesLogicExpress.Application.Managers;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using log4net;


namespace SalesLogicExpress.Application.ViewModels.ProspectViewModels
{
    public class PricingViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.ProspectViewModels");
        #region Properties & Variables
        public Guid MessageToken { get; set; }

        #endregion

        #region OnPropertyChanged

        #region Bool

        private bool isEnableSave = false;

        public bool IsEnableSave
        {
            get { return isEnableSave; }
            set { isEnableSave = value; OnPropertyChanged("IsEnableSave"); }
        }
        #endregion

        #region Int

        #endregion

        #region string


        #endregion

        #region Date

        #endregion

        #region Class
        private PricingSetupDialogViewModel pricingSetupDialogVM = null;

        public PricingSetupDialogViewModel PricingSetupDialogVM
        {
            get { return pricingSetupDialogVM; }
            set { pricingSetupDialogVM = value; OnPropertyChanged("PricingSetupDialogVM"); }
        }


        private CustomerQuoteHeader customerQuote = null;

        public CustomerQuoteHeader CustomerQuote
        {
            get { return customerQuote; }
            set { customerQuote = value; OnPropertyChanged("CustomerQuote"); }
        }
        #endregion
        #region Collection


        #endregion

        #endregion

        #region Commands Declaretion


        #endregion

        #region Command Defination

        public void InitialiseCommands()
        {


        }

        #endregion

        public PricingViewModel(Guid token, BaseViewModel Parent)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingViewModel][Start:PricingViewModel]");
            try
            {
                PayloadManager.ProspectPayload.IsForPricingTab = true;
                ViewModelPayload.PayloadManager.QuotePayload.QuoteId = "NEW";
                MessageToken = token;
                this.ParentView = Parent;
                CustomerQuote = new CustomerQuoteManager().GetCustomerQuote();
                PricingSetupDialogVM = new PricingSetupDialogViewModel(CustomerQuote, token, this.ParentView);
                this.pricingSetupDialogVM.GetDescription();
                PricingSetupDialogVM.IsOverrideView = true;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PricingViewModel][PricingViewModel][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingViewModel][End:PricingViewModel]");
        }

        public override bool ConfirmSave()
        {
            PricingSetupDialogVM.ConfirmSave();
            return base.ConfirmSave();
        }
        public override bool ConfirmCancel()
        {
            PricingSetupDialogVM.ConfirmCancel();

            return base.ConfirmCancel();
        }
    }
}
