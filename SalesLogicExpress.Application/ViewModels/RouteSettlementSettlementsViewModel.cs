﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Threading;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class RouteSettlementSettlementsViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.RouteSettlementSettlementsViewModel");


        //AllSettlements
        #region Properties & Variables
        BaseViewModel _ParentViewModel;
        public BaseViewModel ParentViewModel
        {
            get
            {
                return this._ParentViewModel;
            }
            set
            {
                this._ParentViewModel = value;
                OnPropertyChanged("ParentViewModel");
            }
        }

        private ObservableCollection<RouteSettlementSettlementsModel> allSettlements;

        public ObservableCollection<RouteSettlementSettlementsModel> AllSettlements
        {
            get { return allSettlements; }
            set { allSettlements = value; OnPropertyChanged("AllSettlements"); }
        }
        public ObservableCollection<UsersForVerification> UsersForVerification { get; set; }
        public ObservableCollection<ViewModels.ReasonCode> ReasonCodeList { get; set; }
        private ViewModels.ReasonCode reasonCodeSelected = new ReasonCode();


        public Guid MessageToken { get; set; }
        private static int voidOrderReasonId;

        public static int VoidOrderReasonId
        {
            get { return RouteSettlementSettlementsViewModel.voidOrderReasonId; }
            set { RouteSettlementSettlementsViewModel.voidOrderReasonId = value; }
        }



        #endregion

        #region OnPropertyChanged

        private bool isEnabledPrint = false;

        public bool IsEnabledPrint
        {
            get { return isEnabledPrint; }
            set { isEnabledPrint = value; OnPropertyChanged("IsEnabledPrint"); }
        }

        private bool isEnabledVoid = false;

        public bool IsEnabledVoid
        {
            get { return isEnabledVoid; }
            set { isEnabledVoid = value; OnPropertyChanged("IsEnabledVoid"); }
        }

        private bool isEnabledVerification = false;

        public bool IsEnabledVerification
        {
            get { return isEnabledVerification; }
            set { isEnabledVerification = value; OnPropertyChanged("IsEnabledVerification"); }
        }

        private RouteSettlementSettlementsModel selectedSettlement;

        public RouteSettlementSettlementsModel SelectedSettlement
        {
            get { return selectedSettlement; }
            set
            {
                selectedSettlement = value;
                if (selectedSettlement != null)
                {
                    if (selectedSettlement.Status.ToLower() == "new")
                    {
                        IsEnabledVoid = IsEnabledVerification = IsEnabledPrint = true;
                    }
                    else
                    {
                        IsEnabledVoid = IsEnabledVerification = IsEnabledPrint = false;
                    }

                    if (selectedSettlement.Status.ToLower() == "verified")
                    {
                        IsEnabledPrint = true;
                    }
                }

                OnPropertyChanged("SelectedSettlement");
            }
        }
        UsersForVerification selectedUserForVerification = new UsersForVerification();

        public UsersForVerification SelectedUserForVerification
        {
            get { return selectedUserForVerification; }
            set { selectedUserForVerification = value; OnPropertyChanged("SelectedUserForVerification"); }
        }


        private bool isSelfVerification = false;

        public bool IsSelfVerification
        {
            get { return isSelfVerification; }
            set
            {
                isSelfVerification = value;
                if (value)
                {
                    VerificationPassword = "";
                    IsEnableContinueVeriFication = true;
                }
                else
                {
                    if (String.IsNullOrEmpty(VerificationPassword))
                    {
                        IsEnableContinueVeriFication = false;
                    }

                } OnPropertyChanged("IsSelfVerification");
            }
        }

        private string verificationPassword;

        public string VerificationPassword
        {
            get { return verificationPassword; }
            set
            {
                verificationPassword = value;
                if (!string.IsNullOrEmpty(value))
                {
                    IsEnableContinueVeriFication = true;
                    IsSelfVerification = false;
                }
                else
                {
                    IsEnableContinueVeriFication = false;

                }
                OnPropertyChanged("VerificationPassword");
            }
        }

        private bool isEnableContinueVeriFication = false;

        public bool IsEnableContinueVeriFication
        {
            get { return isEnableContinueVeriFication; }
            set
            {
                isEnableContinueVeriFication = value;
                OnPropertyChanged("IsEnableContinueVeriFication");
            }
        }
        public ViewModels.ReasonCode ReasonCodeSelected
        {
            get { return reasonCodeSelected; }
            set { reasonCodeSelected = value; OnPropertyChanged("ReasonCodeSelected"); }
        }
        private string settlementNoPopup;

        public string SettlementNoPopup
        {
            get { return settlementNoPopup; }
            set { settlementNoPopup = value; OnPropertyChanged("SettlementNoPopup"); }
        }
        private string settlementAmountPopup;


        public string SettlementAmountPopup
        {
            get { return settlementAmountPopup; }
            set { settlementAmountPopup = value; OnPropertyChanged("SettlementAmountPopup"); }
        }

        private string exceptionAmountPopup;

        public string ExceptionAmountPopup
        {
            get { return exceptionAmountPopup; }
            set { exceptionAmountPopup = value; OnPropertyChanged("ExceptionAmountPopup"); }
        }
        private string originatorPopup;

        public string OriginatorPopup
        {
            get { return originatorPopup; }
            set { originatorPopup = value; OnPropertyChanged("OriginatorPopup"); }
        }
        private string datePopup;

        public string DatePopup
        {
            get { return datePopup; }
            set { datePopup = value; OnPropertyChanged("DatePopup"); }
        }
        #endregion

        #region Commands
        public DelegateCommand PrintSettlement { get; set; }
        public DelegateCommand VoidSettlement { get; set; }
        public DelegateCommand VerifySettlement { get; set; }
        public DelegateCommand VoidSettlementExecute { get; set; }

        public DelegateCommand ContinueVerification { get; set; }

        public DelegateCommand VoidOrder { get; set; }

        #endregion

        #region Constructor & Methods
        public RouteSettlementSettlementsViewModel()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:Constructor]\t" + DateTime.Now + "");
            IsBusy = true;
            LoadSettlemntTab();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:Constructor]\t" + DateTime.Now + "");
        }
        public void LoadSettlemntTab()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:LoadSettlemntTab]\t" + DateTime.Now + "");
            InitialiseCommands();
            GetAllSettlements();
            GetUserForVerification();
            IsBusy = false;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:LoadSettlemntTab]\t" + DateTime.Now + "");
        }
        public void InitialiseCommands()
        {
            PrintSettlement = new DelegateCommand((param) =>
            {
                try
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:PrintSettlement]\t" + DateTime.Now + "");
                    PrintSettlementAsync((RouteSettlementSettlementsModel)param);
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:PrintSettlement]\t");
                }
                catch (Exception ex)
                {
                    Logger.Error("RouteSettlementSettlementsViewModel PrintSettlement( ) error: " + ex.Message);
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:PrintSettlement]\t");
               
            });
            VerifySettlement = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:VerifySettlement]\t" + DateTime.Now + "");

                try
                {
                    RouteSettlementSettlementsModel objSelected = (RouteSettlementSettlementsModel)param;
                    ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID = objSelected.SettlementID;
                    ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementNo = objSelected.SettlementNo;
                    ViewModelPayload.PayloadManager.RouteSettlementPayload.OriginatorName = objSelected.OriginatorName;
                    ViewModelPayload.PayloadManager.RouteSettlementPayload.OriginatorID = objSelected.Originator;

                    var dialog = new Helpers.DialogWindow { TemplateKey = "SelectVerificationUser", Title = "Select Verification User" };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                }
                catch (Exception ex)
                {
                    Logger.Error("RouteSettlementSettlementsViewModel VerifySettlement( ) error: " + ex.Message);
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:VerifySettlement]\t");

            });

            ContinueVerification = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:ContinueVerification]\t" + DateTime.Now + "");

                try
                {
                    UsersForVerification objUsr = (UsersForVerification)param;
                    if (IsSelfVerification)
                    {
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.IsForVerification = true;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.GrandTotal = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalCash = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalCheck = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalMoneyOrder = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalExpenses = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.VerifierID = ViewModelPayload.PayloadManager.RouteSettlementPayload.OriginatorID;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.VerifierName = ViewModelPayload.PayloadManager.RouteSettlementPayload.OriginatorName;
                        //ViewModelPayload.PayloadManager.RouteSettlementPayload.IsSettlementTabActive = true;

                        Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                        ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.EOD_Settlement.ToString();

                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.SettlementVerification, CurrentViewName = ViewModelMappings.View.RouteSettlement, CloseCurrentView = false, ShowAsModal = false, Refresh = true };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    }
                    else if (VerificationPassword != objUsr.Password && !IsSelfVerification)
                    {
                        //Password doesnot match
                        var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.PasswordMismatch, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    else if (VerificationPassword == objUsr.Password && !IsSelfVerification)
                    {

                        ViewModelPayload.PayloadManager.RouteSettlementPayload.IsForVerification = true;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.GrandTotal = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalCash = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalCheck = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalMoneyOrder = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalExpenses = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.VerifierID = objUsr.UserID;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.VerifierName = objUsr.UserName;
                        //ViewModelPayload.PayloadManager.RouteSettlementPayload.IsSettlementTabActive = true;

                        Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                        ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.EOD_Settlement.ToString();

                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.SettlementVerification, CurrentViewName = ViewModelMappings.View.RouteSettlement, CloseCurrentView = false, ShowAsModal = false, Refresh = true };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);

                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("RouteSettlementSettlementsViewModel ContinueVerification( ) error: " + ex.Message);
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:ContinueVerification]\t");



            });
            VoidSettlement = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:VoidSettlement]\t" + DateTime.Now + "");

                try
                {
                    RouteSettlementSettlementsModel objSelected = (RouteSettlementSettlementsModel)param;
                    ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID = objSelected.SettlementID;
                    DatePopup = objSelected.Date.ToString();
                    OriginatorPopup = objSelected.OriginatorName;
                    ExceptionAmountPopup = "$" + objSelected.ExceptionAmount.ToString();
                    SettlementAmountPopup = "$" + objSelected.SettlementAmount.ToString();
                    SettlementNoPopup = "Settlement No.: " + objSelected.SettlementNo;


                    var dialog = new Helpers.DialogWindow { TemplateKey = "VoidSettlement", Title = "Void Settlement" };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);


                }
                catch (Exception ex)
                {
                    Logger.Error("RouteSettlementSettlementsViewModel VoidSettlement( ) error: " + ex.Message);
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:VoidSettlement]\t");



            });

            VoidSettlementExecute = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:VoidSettlementExecute]\t" + DateTime.Now + "");

                try
                {
                    if (param != null)
                    {
                        ReasonCode objSe = (ReasonCode)param;
                        SettlementConfirmationManager.VoidSettlement(ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID, "VOID", objSe.Code);
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.IsReinitialiseTransaction = true;
                        GetAllSettlements();
                        Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);

                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("RouteSettlementSettlementsViewModel VoidSettlementExecute( ) error: " + ex.Message);
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:VoidSettlementExecute]\t");



            });

        }

        public void GetAllSettlements()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:GetAllSettlements]\t" + DateTime.Now + "");

            try
            {
                AllSettlements = new ObservableCollection<RouteSettlementSettlementsModel>();//RouteSettlementManager.GetAllSettlements();
                AllSettlements = RouteSettlementManager.GetAllSettlements;
                IsEnabledVoid = false;
                IsEnabledVerification = false;
                IsEnabledPrint = false;
                GetVoidReasons();
            }
            catch (Exception ex)
            {
                Logger.Error("RouteSettlementSettlementsViewModel GetAllSettlements( ) error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:GetAllSettlements]\t");



        }
        public void GetUserForVerification()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:GetUserForVerification]\t" + DateTime.Now + "");

            try
            {
                UsersForVerification = new ObservableCollection<UsersForVerification>();

                List<UsersForVerification> vUsers = new List<UsersForVerification>();

                vUsers = SettlementConfirmationManager.GetUserForVerification;

                UsersForVerification = vUsers.ToObservableCollection();
                //for (int i = 0; i < vUsers.Count; i++)
                //{
                //    UsersForVerification.Add(vUsers[i]);
                //}
            }
            catch (Exception ex)
            {
                Logger.Error("RouteSettlementSettlementsViewModel GetUserForVerification( ) error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:GetUserForVerification]\t");



        }

        public void GetVoidReasons()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:GetVoidReasons]\t" + DateTime.Now + "");

            try
            {
                //ReasonCodeList = new ObservableCollection<ReasonCode>();
                ReasonCodeList = SettlementConfirmationManager.GetReasonListForVoid("Void Settlement");
            }
            catch (Exception ex)
            {
                Logger.Error("RouteSettlementSettlementsViewModel GetVoidReasons( ) error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:GetVoidReasons]\t");
        }

         void PrintSettlementAsync(RouteSettlementSettlementsModel param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:PrintSettlementAsync]\t" + DateTime.Now + "");
            try
            {
                var payload = param;
                ShowRouteSettlementReport(param);
            }
            catch (Exception ex)
            {
                Logger.Error("RouteSettlementSettlementsViewModel PrintSettlementAsync( ) error: " + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:PrintSettlementAsync]\t");
        }

         public void ShowRouteSettlementReport(RouteSettlementSettlementsModel param)
        {
          //Sathish removed this part of code for getting spin
          //await Task.Run(() =>
          //{
            try
            {
                ParentViewModel.IsBusy = true;
                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:ShowRouteSettlementReport]\t" + DateTime.Now + "");
                var DocInfo = new ReportEngine.DocumentInformation();
                var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                repEngine.Notify += repEngine_Notify;

                repEngine.Log = this.Logger;
                ReportEngine.IReportSLE ReportData = null;
                //var query = string.Format("select DocumentID from BUSDTA.PickOrder where Order_ID = {0}", orderID);
                //var result = Convert.ToInt32(Helpers.DbEngine.ExecuteScalar(query));
                //var DocumentId = result;
                var DocumentId = param.DocumentID;
                //var SourceDocumentNo = Convert.ToDecimal(orderID);
                var SourceDocumentNo = Convert.ToDecimal(param.SettlementID);
                if (DocumentId <= 0)
                {
                    ReportData = new ReportViewModels.RouteSettlementReport(param, true);
                    Dictionary<string, object> Parameters;
                    Parameters = new Dictionary<string, object>();
                    Parameters.Add("PrintLabels", Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PrintLabels"]));
                    DocInfo = repEngine.Generate<Reporting.RouteSettlementReport>(SourceDocumentNo, true, ReportEngine.ReportCategory.Route, ReportEngine.ReportsTypes.RouteSettlementReport, ReportData, Parameters, ReportEngine.ContentTypes.PDF);
                }
                else
                {
                    DocInfo.DocumentId = DocumentId;
                }

                if (DocInfo.IsDocumentCreated)
                    ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.Route_Settlement", "SettlementID", SourceDocumentNo, DocInfo.DocumentId);
                DocumentId = Convert.ToInt32(DocInfo.DocumentId); //Replace Latest

                if (DocInfo.IsEndOfSequence)
                {

                    //if (System.Windows.Application.Current != null)
                    //    System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)delegate // <--- HERE
                    //    {
                            Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, Please sync to continue" }, MessageToken);
                      //  });
                    ParentViewModel.IsBusy = false;
                    return;
                }


                var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Route, DocumentId, null);

                if (ReportBytes == null)
                {
                    //if (System.Windows.Application.Current != null)
                    //    System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)delegate // <--- HERE
                    //    {
                            Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Report not generated, Please try again." }, MessageToken);
                      //  });
                    ParentViewModel.IsBusy = false;
                    return;
                }
                SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
                Payload.ReportFile = ReportBytes;
                Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                Payload.PreviousView = ViewModelMappings.View.RouteSettlement;
                Payload.CurrentViewTitle = "Route Settlement Print Report";
                Payload.PreviousViewTitle = "Route Settlement";
                Payload.MessageToken = this.MessageToken;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.RouteSettlement, CloseCurrentView = true, Payload = Payload, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                ParentViewModel.IsBusy = false; 
            }
            catch (Exception ex)
            {
                Logger.Error("RouteSettlementSettlementsViewModel ShowRouteSettlementReport( ) error: " + ex.Message);
                ParentViewModel.IsBusy = false;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:ShowRouteSettlementReport]\t");
            ParentViewModel.IsBusy = false;
          //});
        }


        void repEngine_Notify(string msg)
        {
            System.Windows.MessageBox.Show(msg);
        }

        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
