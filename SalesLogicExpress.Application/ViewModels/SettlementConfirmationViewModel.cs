﻿using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using Telerik.Windows.Controls;
using System.Windows.Ink;
using System.Windows.Media.Imaging;
using log4net;

namespace SalesLogicExpress.Application.ViewModels
{
    public class SettlementConfirmationViewModel : BaseViewModel
    {
        #region Properties & Variables
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.SettlementConfirmationViewModel");
        public static DateTime SelectedCalendarDate = DateTime.Now;
        public Guid MessageToken { get; set; }
        static List<CashMaster> verifiedCash; static List<CheckDetails> verifiedCheck;
        static List<MoneyOrderDetails> verifiedMo; static List<VerifiedFunds> verifiedFunds;
        public ObservableCollection<RejectionReason> RejectionReasons { get; set; }

        #endregion

        #region OnPropertyChanged

        private RejectionReason selectedRejectionReason = new RejectionReason();

        public RejectionReason SelectedRejectionReason
        {
            get { return selectedRejectionReason; }
            set { selectedRejectionReason = value; OnPropertyChanged("SelectedRejectionReason"); }
        }


        private string settlementNo;

        public string SettlementNo
        {
            get { return settlementNo; }
            set { settlementNo = value; OnPropertyChanged("SettlementNo"); }
        }
        private string originator;

        public string Originator
        {
            get { return originator; }
            set { originator = value; }
        }
        private string verifier;

        public string Verifier
        {
            get { return verifier; }
            set { verifier = value; }
        }
        private bool isVerifier;

        public bool IsVerifier
        {
            get { return isVerifier; }
            set { isVerifier = value; }
        }

        private static CashDetais objCashDetails;

        public static CashDetais ObjCashDetails
        {
            get { return objCashDetails; }
            set { objCashDetails = value; NotifyStaticPropertyChanged("ObjCashDetails"); }
        }
        private static CheckDetails objCheckDetails = new CheckDetails();

        public static CheckDetails ObjCheckDetails
        {
            get { return objCheckDetails; }
            set { objCheckDetails = value; NotifyStaticPropertyChanged("ObjCheckDetails"); }
        }
        private static MoneyOrderDetails objMoneyOrderDetails = new MoneyOrderDetails();

        public static MoneyOrderDetails ObjMoneyOrderDetails
        {
            get { return objMoneyOrderDetails; }
            set { objMoneyOrderDetails = value; NotifyStaticPropertyChanged("ObjMoneyOrderDetails"); }
        }

        private static ObservableCollection<CashMaster> cashMasterCollection;

        public static ObservableCollection<CashMaster> CashMasterCollection
        {
            get { return SettlementConfirmationViewModel.cashMasterCollection; }
            set { SettlementConfirmationViewModel.cashMasterCollection = value; NotifyStaticPropertyChanged("CashMasterCollection"); }
        }

        private static ObservableCollection<CheckDetails> checkDetailsCollection;

        public static ObservableCollection<CheckDetails> CheckDetailsCollection
        {
            get { return checkDetailsCollection; }
            set { checkDetailsCollection = value; NotifyStaticPropertyChanged("CheckDetailsCollection"); }
        }
        private static ObservableCollection<MoneyOrderDetails> moneyOrderDetailsCollection;

        public static ObservableCollection<MoneyOrderDetails> MoneyOrderDetailsCollection
        {
            get { return moneyOrderDetailsCollection; }
            set { moneyOrderDetailsCollection = value; NotifyStaticPropertyChanged("MoneyOrderDetailsCollection"); }
        }
        private static double overShort;

        public static double OverShort
        {
            get { return overShort; }
            set
            {
                overShort = value;
                if ((Math.Round(value, 2) == 0.0) && (BrushMO == (Brush)new BrushConverter().ConvertFromString("Green")) && (BrushCheck == (Brush)new BrushConverter().ConvertFromString("Green")))
                {
                    IsEnabledOutOfBalanceSettlement = false;
                    IsEnabledCompleteVerification = true;
                    IsCanvasEnable = true;
                }   //Commented below line as payment overshort condition removed. 
                //else if (value < Payments * 0.2 && value > 0 && (BrushMO == (Brush)new BrushConverter().ConvertFromString("Green")) && (BrushCheck == (Brush)new BrushConverter().ConvertFromString("Green")))
                //else if ((BrushMO == (Brush)new BrushConverter().ConvertFromString("Green")) && (BrushCheck == (Brush)new BrushConverter().ConvertFromString("Green")))
                //{
                //    IsCanvasEnable = true;
                //    IsEnabledCompleteVerification = false;
                //    IsEnabledOutOfBalanceSettlement = true;
                //}
                else
                {
                    IsEnabledCompleteVerification = false;
                    IsCanvasEnable = true;
                    IsEnabledOutOfBalanceSettlement = true;
                }

                NotifyStaticPropertyChanged("OverShort");
            }
        }
        private static double totalVerified;

        public static double TotalVerified
        {
            get { return totalVerified; }
            set
            {
                totalVerified = value;
                OverShort = Payments - value - Expenses;
                IsDirtyStatic = (value > 0) ? true : false;
                NotifyStaticPropertyChanged("TotalVerified");
            }
        }
        private static double expenses;

        public static double Expenses
        {
            get { return expenses; }
            set { expenses = value; NotifyStaticPropertyChanged("TotalVerified"); }
        }

        private static double payments;

        public static double Payments
        {
            get { return payments; }
            set { payments = value; NotifyStaticPropertyChanged("Payments"); }
        }
        private static double moneyOrderVerified;

        public static double MoneyOrderVerified
        {
            get { return moneyOrderVerified; }
            set
            {
                moneyOrderVerified = value;

                NotifyStaticPropertyChanged("MoneyOrderVerified");
            }
        }
        private static double checkVerified;

        public static double CheckVerified
        {
            get { return checkVerified; }
            set
            {
                checkVerified = value;
                NotifyStaticPropertyChanged("CheckVerified");
            }
        }
        private static double cashVerified;

        public static double CashVerified
        {
            get { return cashVerified; }
            set
            {
                TotalVerified = Math.Round(TotalVerified + value - cashVerified, 2);
                cashVerified = value;
                if (ViewModelPayload.PayloadManager.RouteSettlementPayload.IsForVerification)
                {
                    VerificationProcess();
                }
                NotifyStaticPropertyChanged("CashVerified");
            }
        }

        private static Brush brushMO = (Brush)new BrushConverter().ConvertFromString("Red");

        public static Brush BrushMO
        {
            get { return brushMO; }
            set { brushMO = value; NotifyStaticPropertyChanged("BrushMO"); }
        }
        private static Brush brushCheck = (Brush)new BrushConverter().ConvertFromString("Red");

        public static Brush BrushCheck
        {
            get { return brushCheck; }
            set { brushCheck = value; NotifyStaticPropertyChanged("BrushCheck"); }
        }

        private static bool isEnabledCompleteVerification = false;

        public static bool IsEnabledCompleteVerification
        {
            get { return isEnabledCompleteVerification; }
            set { isEnabledCompleteVerification = value; NotifyStaticPropertyChanged("IsEnabledCompleteVerification"); }
        }
        private static bool isEnabledOutOfBalanceSettlement = false;

        public static bool IsEnabledOutOfBalanceSettlement
        {
            get { return isEnabledOutOfBalanceSettlement; }
            set
            {
                if (ViewModelPayload.PayloadManager.RouteSettlementPayload.IsForVerification)
                {
                    value = true;
                }
                isEnabledOutOfBalanceSettlement = value; NotifyStaticPropertyChanged("IsEnabledOutOfBalanceSettlement");
            }
        }

        private string requestCode;

        public string RequestCode
        {
            get { return requestCode; }
            set { requestCode = value; OnPropertyChanged("RequestCode"); }
        }

        RequestAuthCodeViewModel _RequestAuthCodeViewModel;
        public RequestAuthCodeViewModel RequestAuthCodeVM
        {
            get
            {
                return _RequestAuthCodeViewModel;
            }
            set
            {
                _RequestAuthCodeViewModel = value;
                OnPropertyChanged("RequestAuthCodeVM");
            }
        }
        private string approvalCode;

        public string ApprovalCode
        {
            get { return approvalCode; }
            set
            {
                approvalCode = value;
                if (!string.IsNullOrEmpty(value))
                {
                    IsEnabledOutOFBalanceApproval = true;
                }
                else
                {
                    IsEnabledOutOFBalanceApproval = false;

                }
                OnPropertyChanged("ApprovalCode");
            }
        }

        private bool isEnabledOutOFBalanceApproval = false;

        public bool IsEnabledOutOFBalanceApproval
        {
            get { return isEnabledOutOFBalanceApproval; }
            set { isEnabledOutOFBalanceApproval = value; OnPropertyChanged("IsEnabledOutOFBalanceApproval"); }
        }


        private static bool isCanvasEnable;

        public static bool IsCanvasEnable
        {
            get { return SettlementConfirmationViewModel.isCanvasEnable; }
            set
            {
                if (ViewModelPayload.PayloadManager.RouteSettlementPayload.IsForVerification)
                {
                    value = true;
                }
                SettlementConfirmationViewModel.isCanvasEnable = value; NotifyStaticPropertyChanged("IsCanvasEnable");
            }
        }

        private InkCanvas myCanvas;

        public InkCanvas MyCanvas
        {
            get { return myCanvas; }
            set { myCanvas = value;

             // TUI Issue ID 258 fixed on (01/03/2017) for Complete Verification button is not enable after provide signature in Settlement Confirmation screen ---- Vignesh D
            if (ViewModelPayload.PayloadManager.RouteSettlementPayload.IsForVerification)
            {
                VerificationProcess();
            }
                OnPropertyChanged("MyCanvas"); }
        }

        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void NotifyStaticPropertyChanged(string propertyName)
        {
            if (StaticPropertyChanged != null)
                StaticPropertyChanged(null, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region Commands
        public DelegateCommand CompleteConfirmation { get; set; }
        public DelegateCommand OutOFBalanceApproval { get; set; }
        public DelegateCommand ContinueWithOutOFBalanceApproval { get; set; }
        public DelegateCommand RejectSettlement { get; set; }
        public DelegateCommand ContinueWithRejectionReason { get; set; }

        public DelegateCommand CompleteVerification { get; set; }




        #endregion

        #region Constructor & Methods
        public SettlementConfirmationViewModel()
        {
            IsBusy = true;
            SettlementConfirmationViewModelLoad();
        }
        public void SettlementConfirmationViewModelLoad()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][Start:SettlementConfirmationViewModelLoad]\t" + DateTime.Now + "");
            try
            {
                if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.EOD_Transaction.ToString())
                {
                    SelectedTab = ViewModelMappings.TabView.RouteSettlement.Transactions;
                }
                else if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.EOD_Settlement.ToString())
                {
                    SelectedTab = ViewModelMappings.TabView.RouteSettlement.Settlement;
                }
                InitialiseCommands();
                InitialiseVariable();
                string settlementID = ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID;

                if (ViewModelPayload.PayloadManager.RouteSettlementPayload.IsForVerification)
                {

                    GetConfirmedSettlement(settlementID);
                    CashVerified = 0; CheckVerified = 0; TotalVerified = 0;
                    MoneyOrderVerified = 0;
                    //MoneyOrderVerified = Convert.ToDouble(verifiedFunds[0].MoneyOrder);
                    Expenses = Convert.ToDouble(verifiedFunds[0].Expenses);
                    Payments = Convert.ToDouble(verifiedFunds[0].Payments);
                    OverShort = Payments - Expenses;
                    GetDetailsForVerification(settlementID);
                    //GetCheckDetails();
                    //GetMoneyOrderDetails();
                    GetRejectionReasons();
                    Verifier = ViewModelPayload.PayloadManager.RouteSettlementPayload.VerifierName;
                    SettlementNo = ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementNo;

                }
                else
                {
                    GetCheckDetails();
                    GetMoneyOrderDetails();
                    SettlementNo = "New";
                    Expenses = ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalExpenses;
                    Payments = ViewModelPayload.PayloadManager.RouteSettlementPayload.GrandTotal;
                    OverShort = Payments - Expenses;
                }

                ObjCheckDetails.SelectedItems.CollectionChanged += SelectedItems_CollectionChanged;
                ObjMoneyOrderDetails.SelectedItems.CollectionChanged += SelectedItems_CollectionChanged1;
            }
            catch (Exception ex)
            {
                Logger.Error("Error : " + ex.ToString());
                IsBusy = false;
            }
            IsBusy = false;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][End:SettlementConfirmationViewModelLoad]\t" + DateTime.Now + "");

        }
        public void OutOFBalanceApprovalExecute()
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][Start:OutOFBalanceApprovalExecute]\t" + DateTime.Now + "");

                if (MyCanvas.Strokes.Count == 0)
                {
                    //Password doesnot match
                    var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.NoSignature, MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    return;
                }
                RequestAuthCodeViewModel.RequestAuthCodeParams codeParams = new RequestAuthCodeViewModel.RequestAuthCodeParams
                {
                    Amount = (decimal)OverShort,
                    UserID = UserManager.UserId.ToString(),
                    Feature = AuthCodeManager.Feature.OutOfBalanceApproval
                };
                RequestAuthCodeVM = new RequestAuthCodeViewModel(codeParams);
                RequestAuthCodeVM.AuthCodeChanged += RequestAuthCodeVM_AuthCodeChanged;
                var dialog = new Helpers.DialogWindow { TemplateKey = "OutOfBalanceApproval", Title = "Out of Balance Approval" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][UpdateSequenceNo][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][End:OutOFBalanceApprovalExecute]\t" + DateTime.Now + "");


        }

        void RequestAuthCodeVM_AuthCodeChanged(object sender, RequestAuthCodeViewModel.AuthCodeChangedEventArgs e)
        {
            IsEnabledOutOFBalanceApproval = e.IsValid;
        }
        public void CompleteConfirmationExecute()
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][Start:OutOFBalanceApprovalExecute]\t" + DateTime.Now + "");

                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                //Save Out of Balance without approval

                if (MyCanvas.Strokes.Count == 0)
                {
                    //No Signature
                    var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.NoSignature, MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    IsBusy = false;
                    return;
                }
                IsEnabledCompleteVerification = false;
                Byte[] sign = CreateCanvasToImage();

                bool result = SettlementConfirmationManager.AddConfirmationSettlement("NEW", Payments, CashMasterCollection, ObjCheckDetails.SelectedItems, ObjMoneyOrderDetails.SelectedItems,
                    CashVerified, CheckVerified, MoneyOrderVerified, TotalVerified, Expenses, OverShort, sign);
                if (result == true)
                {
                    //Navigate to EOD
                    //ViewModelPayload.PayloadManager.RouteSettlementPayload.IsSettlementTabActive = true;
                    CommonNavInfo.IsEODPendingForVerification = true;

                    IsDirtyStatic = false;
                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteSettlement, CurrentViewName = ViewModelMappings.View.SettlementConfirmation, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = SelectedTab };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                }
                else
                {
                    Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, please sync to continue" }, MessageToken);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][OutOFBalanceApprovalExecute][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][End:OutOFBalanceApprovalExecute]\t" + DateTime.Now + "");

        }
        public void ContinueWithOutOFBalanceApprovalExecute(object param)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][Start:ContinueWithOutOFBalanceApprovalExecute]\t" + DateTime.Now + "");

                if (param != null)
                {
                    string apprcode = param.ToString();
                    if (RequestAuthCodeVM.IsAuthCodeValid)
                    {
                        Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                        //Save Out of Balance without approval
                        IsEnabledOutOFBalanceApproval = false;
                        Byte[] sign = CreateCanvasToImage();
                        bool result = SettlementConfirmationManager.AddConfirmationSettlement("NEW", Payments, CashMasterCollection, ObjCheckDetails.SelectedItems, ObjMoneyOrderDetails.SelectedItems,
                            CashVerified, CheckVerified, MoneyOrderVerified, TotalVerified, Expenses, OverShort, sign);

                        if (result == true)
                        {
                            //Navigate to EOD
                            //ViewModelPayload.PayloadManager.RouteSettlementPayload.IsSettlementTabActive = true;
                            IsDirtyStatic = false;
                            if (RequestAuthCodeVM.IsAuthCodeValid)
                            {
                                RequestAuthCodeVM.LogAuthCode(PayloadManager.ApplicationPayload.LoggedInUserID, RequestAuthCodeVM.RequestCode, RequestAuthCodeVM.Authcode, CommonNavInfo.RouteID.ToString(), PayloadManager.RouteSettlementPayload.SerializeToJson(), null, OverShort.ToString(), RequestAuthCodeVM.Params.Feature.ToString());
                            }
                            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteSettlement, CurrentViewName = ViewModelMappings.View.SettlementConfirmation, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = SelectedTab };
                            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                        }
                        else
                            Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, please sync to continue" }, MessageToken);
                    }
                    else
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.WrongApproval, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        IsBusy = false;

                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][ContinueWithOutOFBalanceApprovalExecute][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][End:ContinueWithOutOFBalanceApprovalExecute]\t" + DateTime.Now + "");

        }
        public void RejectSettlementExecute()
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][Start:RejectSettlementExecute]\t" + DateTime.Now + "");

                if (MyCanvas.Strokes.Count == 0)
                {
                    //No Signature
                    var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.NoSignature, MessageIcon = "Alert" };
                    IsBusy = false;
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    return;
                }
                var dialog = new Helpers.DialogWindow { TemplateKey = "SelectRejectionReason", Title = "Select Rejection Reason" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][RejectSettlementExecute][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][End:RejectSettlementExecute]\t" + DateTime.Now + "");

        }
        public void ContinueWithRejectionReasonExecute(object param)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][Start:ContinueWithRejectionReasonExecute]\t" + DateTime.Now + "");

                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                RejectionReason obj = (RejectionReason)param;
                string settlementID = ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID;
                string verifierID = ViewModelPayload.PayloadManager.RouteSettlementPayload.VerifierID;

                //Update reason in datatbase
                SettlementConfirmationManager.UpdateSettlementVerification(settlementID, obj.ReasonName, verifierID, "REJCT");

                Byte[] sign = CreateCanvasToImage();

                //insert verification entry
                SettlementConfirmationManager.AddVerificationSettlement("REJCT", Payments, CashMasterCollection, ObjCheckDetails.SelectedItems, ObjMoneyOrderDetails.SelectedItems,
                    CashVerified, CheckVerified, MoneyOrderVerified, TotalVerified, Expenses, OverShort, settlementID, sign);
                //Navigate to EOD
                //ViewModelPayload.PayloadManager.RouteSettlementPayload.IsSettlementTabActive = true;
                IsDirtyStatic = false;
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteSettlement, CurrentViewName = ViewModelMappings.View.SettlementVerification, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = SelectedTab };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][ContinueWithRejectionReasonExecute][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][End:ContinueWithRejectionReasonExecute]\t" + DateTime.Now + "");
        }
        public void CompleteVerificationExecute(object param)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][Start:CompleteVerificationExecute]\t" + DateTime.Now + "");

                if (MyCanvas.Strokes.Count == 0)
                {
                    //No Signature
                    var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.NoSignature, MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    IsBusy = false;
                    return;
                }
                RejectionReason obj = (RejectionReason)param;
                string settlementID = ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID;
                string verifierID = ViewModelPayload.PayloadManager.RouteSettlementPayload.VerifierID;

                //Update reason in datatbase
                SettlementConfirmationManager.UpdateSettlementVerification(settlementID, "", verifierID, "VERF");

                //Update settlement status of receipts
                ARPaymentManager objARpayment = new ARPaymentManager("", "");

                if (PayloadManager.RouteSettlementPayload.ReceiptList.Count > 0)
                {
                    objARpayment.UpdateReceiptStatus(PayloadManager.RouteSettlementPayload.ReceiptList.Aggregate<string>((oldData, newData) => oldData + "," + newData), "SETTLED");
                    PayloadManager.RouteSettlementPayload.ReceiptList.Clear();
                }
                if (PayloadManager.RouteSettlementPayload.ExpenseList.Count > 0)
                {
                    objARpayment.UpdateExpenseStatus(PayloadManager.RouteSettlementPayload.ExpenseList.Aggregate<string>((oldData, newData) => oldData + "," + newData), "SETTLED");
                    PayloadManager.RouteSettlementPayload.ExpenseList.Clear();
                }
                if (PayloadManager.RouteSettlementPayload.MoneyOrderList.Count > 0)
                {
                    objARpayment.UpdateMoneyOrderStatus(PayloadManager.RouteSettlementPayload.MoneyOrderList.Aggregate<string>((oldData, newData) => oldData + "," + newData), "SETTLED");
                    PayloadManager.RouteSettlementPayload.MoneyOrderList.Clear();
                }
                objARpayment = null;

                // Update Settlment Status for Orders
                SettlementConfirmationManager.UpdateSettlmentStatusAfterVerification(settlementID);

                //Update receipts generated from return order
                SettlementConfirmationManager.UpdateReturnOrderRelatedRecordStatus(settlementID);

                //Update quote proecss
                if (PayloadManager.QuotePayload.QuoteIdList != null && PayloadManager.QuotePayload.QuoteIdList.Count > 0)
                {
                    (new CustomerQuoteManager()).SettleQuotes(settlementID, PayloadManager.QuotePayload.QuoteIdList.Aggregate<string>((oldData, newData) => oldData + "," + newData), false);
                }

                if (PayloadManager.QuotePayload.ProspectQuoteIdList != null && PayloadManager.QuotePayload.ProspectQuoteIdList.Count > 0)
                {
                    (new CustomerQuoteManager()).SettleQuotes(settlementID, PayloadManager.QuotePayload.ProspectQuoteIdList.Aggregate<string>((oldData, newData) => oldData + "," + newData), true);
                }

                Byte[] sign = CreateCanvasToImage();

                //insert verification entry
                SettlementConfirmationManager.AddVerificationSettlement("VERF", Payments, CashMasterCollection, ObjCheckDetails.SelectedItems, ObjMoneyOrderDetails.SelectedItems,
                    CashVerified, CheckVerified, MoneyOrderVerified, TotalVerified, Expenses, OverShort, settlementID, sign);
                //CommonNavInfo.IsEODCompleted = true;
                CommonNavInfo.IsEODPendingForVerification = false;
                CommonNavInfo.IsManuallySyncAllProfiles = true;
                //Navigate to EOD
                IsDirtyStatic = false;

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteSettlement, CurrentViewName = ViewModelMappings.View.SettlementVerification, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = SelectedTab };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);

                //ResourceManager.CommonNavInfo.StartSync.Execute(null);               
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][CompleteVerificationExecute][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][End:CompleteVerificationExecute]\t" + DateTime.Now + "");

        }

        //async void Empty()
        //{
        //    await Task.Run(() =>
        //    {
        //    });
        //}


        public void InitialiseCommands()
        {
            OutOFBalanceApproval = new DelegateCommand((param) =>
            {
                OutOFBalanceApprovalExecute();
            });

            ContinueWithOutOFBalanceApproval = new DelegateCommand((param) =>
            {
                ContinueWithOutOFBalanceApprovalExecute(param);

            });

            CompleteConfirmation = new DelegateCommand((param) =>
            {

                CompleteConfirmationExecute();

            });
            RejectSettlement = new DelegateCommand((param) =>
            {
                RejectSettlementExecute();

            });
            ContinueWithRejectionReason = new DelegateCommand((param) =>
            {
                ContinueWithRejectionReasonExecute(param);
            });
            CompleteVerification = new DelegateCommand((param) =>
            {
                CompleteVerificationExecute(param);
            });

        }

        public void InitialiseVariable()
        {
            //RouteSettlementViewModel objRSVM = SalesLogicExpress.Application.ViewModelPayload.
            ObjCashDetails = new CashDetais();
            CashVerified = 0; CheckVerified = 0; MoneyOrderVerified = 0; TotalVerified = 0;
            CheckDetailsCollection = new ObservableCollection<CheckDetails>();
            ObjCheckDetails = new CheckDetails();
            ObjMoneyOrderDetails = new MoneyOrderDetails();
            Originator = CommonNavInfo.UserName;
            GetCashMasterCollection();
        }

        void SelectedItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //TotalVerified -= CheckVerified;
            double preCheckVerified = CheckVerified;
            CheckVerified = 0;
            int selectedCount = 0;
            foreach (var item in ObjCheckDetails.SelectedItems)
            {
                CheckVerified += Convert.ToDouble(item.Amount.Replace('$', ' ').Trim());
                selectedCount++;
            }
            if (selectedCount == CheckDetailsCollection.Count)
            {
                BrushCheck = (Brush)new BrushConverter().ConvertFromString("Green");
            }
            else
            {
                BrushCheck = (Brush)new BrushConverter().ConvertFromString("Red");
            }
            TotalVerified += CheckVerified - preCheckVerified;

            if (ViewModelPayload.PayloadManager.RouteSettlementPayload.IsForVerification)
            {
                VerificationProcess();
            }

        }
        void SelectedItems_CollectionChanged1(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][Start:SelectedItems_CollectionChanged1]\t" + DateTime.Now + "");
            //TotalVerified -= MoneyOrderVerified;
            double preMoneyOrderVerified = MoneyOrderVerified;
            MoneyOrderVerified = 0;
            int selectedCount = 0;

            foreach (var item in ObjMoneyOrderDetails.SelectedItems)
            {
                selectedCount++;
                MoneyOrderVerified += Convert.ToDouble(item.Amount.Replace('$', ' ').Trim());
            }
            if (selectedCount == MoneyOrderDetailsCollection.Count)
            {
                BrushMO = (Brush)new BrushConverter().ConvertFromString("Green");
            }
            else
            {
                BrushMO = (Brush)new BrushConverter().ConvertFromString("Red");
            }
            TotalVerified += MoneyOrderVerified - preMoneyOrderVerified;

            if (ViewModelPayload.PayloadManager.RouteSettlementPayload.IsForVerification)
            {
                VerificationProcess();
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][End:SelectedItems_CollectionChanged1]\t" + DateTime.Now + "");
        }
        public void GetCheckDetails()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][Start:GetCheckDetails]\t" + DateTime.Now + "");

            try
            {
                CheckDetailsCollection = ViewModelPayload.PayloadManager.RouteSettlementPayload.CheckDetailsCollection;
                if (CheckDetailsCollection.Count == 0)
                {
                    BrushCheck = (Brush)new BrushConverter().ConvertFromString("Green");
                }
                else
                {
                    BrushCheck = (Brush)new BrushConverter().ConvertFromString("Red");
                }
            }
            catch (Exception ex)
            {
                Logger.Error("SettlementConfirmationViewModel GetCheckDetails( ) error: " + ex.Message);
            }


            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][End:GetCheckDetails]\t" + DateTime.Now + "");
        }

        public void GetMoneyOrderDetails()
        {
            //MoneyOrderDetailsCollection = new ObservableCollection<MoneyOrderDetails>();
            //for (int i = 0; i < 4; i++)
            //{
            //    MoneyOrderDetails objChk = new MoneyOrderDetails();
            //    objChk.Amount = "$" + (100 * (i + 1)).ToString();
            //    objChk.MoneyOrderNo = "1111" + i.ToString();
            //    objChk.FeeAmount = "$" + (5 * (i + 1)).ToString();
            //    objChk.Date = DateTime.Now.ToString("MM/dd/yyyy");
            //    MoneyOrderDetailsCollection.Add(objChk);
            //}
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][Start:GetCheckDetails]\t" + DateTime.Now + "");
            MoneyOrderDetailsCollection = ViewModelPayload.PayloadManager.RouteSettlementPayload.MoneyOrderDetailsCollection;
            if (MoneyOrderDetailsCollection.Count == 0)
            {
                BrushMO = (Brush)new BrushConverter().ConvertFromString("Green");
            }
            else
            {
                BrushMO = (Brush)new BrushConverter().ConvertFromString("Red");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][End:GetCheckDetails]\t" + DateTime.Now + "");
        }

        public void GetDetailsForVerification(string settlementID)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][Start:GetDetailsForVerification]\t" + DateTime.Now + "");
            DataSet ds = new DataSet();
            try
            {
                string activityState = string.Empty;
                ObservableCollection<RouteSettlementModel> transactions = RouteSettlementManager.GetTransactionsSettlementID(settlementID);
                Type detailClassType = null;
                DataContractJsonSerializer serializer = null;
                MemoryStream stream = null;
                CheckDetailsCollection = new ObservableCollection<CheckDetails>();
                MoneyOrderDetailsCollection = new ObservableCollection<MoneyOrderDetails>();
                ViewModelPayload.PayloadManager.RouteSettlementPayload.ActivityIDList = new ObservableCollection<string>();

                foreach (RouteSettlementModel trans in transactions)
                {
                    try
                    {
                        switch (trans.ActivityType.Trim())
                        {

                            case "Payment":
                                if (trans.ActivityDetails.Trim().Length == 0) break;
                                detailClassType = Type.GetType(trans.ActivityDetailClass.Trim());
                                ViewModelPayload.PaymentPayload payloadTypeObj = (ViewModelPayload.PaymentPayload)Activator.CreateInstance(detailClassType);
                                serializer = new DataContractJsonSerializer(detailClassType);
                                stream = new MemoryStream(Encoding.UTF8.GetBytes(trans.ActivityDetails));
                                payloadTypeObj = (ViewModelPayload.PaymentPayload)serializer.ReadObject(stream);
                                PaymentTransaction data1 = new PaymentTransaction();
                                data1.Date = trans.ActivityEnd;
                                data1.Type = "PAY";
                                data1.FilterTypeName = "Payment";
                                data1.InvoiceNo = payloadTypeObj.InvoiceNo;
                                data1.IsInvoiceNoVisible = data1.InvoiceNo.Count == 0 ? false : true;
                                data1.IsUnappliedAmntVisible = !data1.IsInvoiceNoVisible;
                                data1.PaymentType = payloadTypeObj.CashDelivery.PaymentMode ? "Check" : "Cash";
                                data1.BillTo = payloadTypeObj.Customer.CustomerNo + ", " + payloadTypeObj.Customer.CustName.Trim();
                                data1.ShipTo = payloadTypeObj.Customer.CustomerNo + ", " + payloadTypeObj.Customer.CustName.Trim();
                                data1.ChequeNo = payloadTypeObj.CashDelivery.PaymentMode ? payloadTypeObj.CashDelivery.ChequeNo : "";
                                data1.IsCheckVisible = string.IsNullOrEmpty(data1.ChequeNo) ? false : true;
                                data1.Amount = (string.IsNullOrEmpty(payloadTypeObj.CashDelivery.PaymentAmount.ToString()) ? "0.00" : payloadTypeObj.CashDelivery.PaymentAmount.ToString());
                                data1.UnAppliedAmount = "";
                                data1.IsVoid = trans.ActivityStatus.ToLower().Trim() == "paymentvoid" ? true : false;


                                if (data1.IsCheckVisible)
                                {
                                    CheckDetails objChk = new CheckDetails();
                                    objChk.Amount = "$" + data1.Amount;
                                    objChk.CheckNo = data1.ChequeNo;
                                    objChk.CustomerName = data1.BillTo;
                                    objChk.Date = data1.Date.ToString("MM/dd/yyyy");
                                    objChk.CheckID = payloadTypeObj.CashDelivery.ChequeNoId;
                                    if (!data1.IsVoid)
                                    {
                                        CheckDetailsCollection.Add(objChk);
                                    }
                                }

                                break;

                            case "MoneyOrder":
                                if (trans.ActivityDetails.Trim().Length == 0) break;

                                if (trans.ActivityStatus == "CreateMoneyOrder" || trans.ActivityStatus == "EditMoneyOrder")
                                {

                                    serializer = new DataContractJsonSerializer(typeof(MoneyOrder));
                                    stream = new MemoryStream(Encoding.UTF8.GetBytes(trans.ActivityDetails));
                                    MoneyOrder moneyOrder = (MoneyOrder)serializer.ReadObject(stream);
                                    MoneyOrderTransaction data3 = new MoneyOrderTransaction();
                                    data3.Date = trans.ActivityEnd;
                                    data3.Type = "MO";
                                    data3.Amount = moneyOrder.MoneyOrderAmount;
                                    data3.MoneyOrderNo = moneyOrder.MoneyOrderNumber;
                                    data3.MoneyOrerFeeAmnt = moneyOrder.FeeAmount;
                                    data3.FilterTypeName = "Money Order";
                                    data3.MoneyOrderId = moneyOrder.MoneyOrderID;

                                    if (trans.ActivityStatus == "CreateMoneyOrder" || trans.ActivityStatus == "EditMoneyOrder")
                                    {
                                        MoneyOrderDetails objMoney = new MoneyOrderDetails();
                                        objMoney.Amount = "$" + data3.Amount;
                                        objMoney.MoneyOrderNo = data3.MoneyOrderNo;
                                        objMoney.FeeAmount = "$" + data3.MoneyOrerFeeAmnt;
                                        objMoney.Date = data3.Date.ToString("MM/dd/yyyy");
                                        objMoney.MoneyOrderID = moneyOrder.MoneyOrderID;
                                        MoneyOrderDetailsCollection.Add(objMoney);
                                    }
                                }

                                break;
                            default:
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("SettlementConfirmationViewModel switch try : " + ex.Message);
                        continue;
                    }
                }

                if (CheckDetailsCollection.Count == 0)
                {
                    BrushCheck = (Brush)new BrushConverter().ConvertFromString("Green");
                }
                if (MoneyOrderDetailsCollection.Count == 0)
                {
                    BrushMO = (Brush)new BrushConverter().ConvertFromString("Green");
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][GetDetailsForVerification][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][End:GetDetailsForVerification]\t" + DateTime.Now + "");
        }

        public Byte[] CreateCanvasToImage()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][Start:CreateCanvasToImage]\t" + DateTime.Now + "");
            try
            {
                string savedFilePath = "";
                InkCanvas canvas = MyCanvas;
                string settID = ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID;

                string fileName = ResourceManager.DomainPath + @"\Docs\" + "EOD.jpg";
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }
                System.IO.Directory.CreateDirectory(ResourceManager.DomainPath + @"\Docs");

                RenderTargetBitmap rtb = new RenderTargetBitmap((int)canvas.ActualWidth - 10, (int)canvas.ActualHeight, 0, 0, PixelFormats.Pbgra32);
                rtb.Render(canvas);
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(rtb));
                FileStream file = new FileStream(fileName, FileMode.Create);
                encoder.Save(file);
                savedFilePath = file.Name;
                file.Close();
                FileStream fs = new FileStream(savedFilePath, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                br.Close();
                fs.Close();
                File.Delete(savedFilePath);
                Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][End:CreateCanvasToImage]\t" + DateTime.Now + "");
                return bytes;

            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][CreateCanvasToImage][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            

        }
        public void GetConfirmedSettlement(string settlementID)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][Start:GetConfirmedSettlement]\t" + DateTime.Now + "");
            SettlementConfirmationManager.GetConfirmedSettlements(settlementID, out verifiedCash, out verifiedCheck, out verifiedMo, out verifiedFunds);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][End:GetConfirmedSettlement]\t" + DateTime.Now + "");
        }

        public void GetRejectionReasons()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][Start:GetRejectionReasons]\t" + DateTime.Now + "");
            try
            {
                SelectedRejectionReason = new RejectionReason();
                RejectionReasons = new ObservableCollection<RejectionReason>();
                RejectionReasons = SettlementConfirmationManager.GetRejectionReasons;
                Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][End:GetRejectionReasons]\t" + DateTime.Now + "");
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][CreateCanvasToImage][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            //for (int i = 0; i < 3; i++)
            //{
            //    RejectionReason obj = new RejectionReason();
            //    obj.ReasonID = i.ToString();
            //    obj.ReasonName = "Dummy Reason :" + i;
            //    RejectionReasons.Add(obj);
            //}
        }
        public void GetCashMasterCollection()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][Start:GetCashMasterCollection]\t" + DateTime.Now + "");
            CashMasterCollection = new ObservableCollection<CashMaster>();
            List<CashMaster> cashMst = new List<CashMaster>();
            cashMst = SettlementConfirmationManager.GetCashMaster;
            for (int i = 0; i < cashMst.Count; i++)
            {
                CashMasterCollection.Add(cashMst[i]);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][End:GetCashMasterCollection]\t" + DateTime.Now + "");
        }
        public static void VerificationProcess()
        {
            try
            {
                //Verify Cash
                #region Cash
                if (verifiedCash == null || verifiedCheck == null || verifiedMo == null)
                {
                    return;
                }
                if (verifiedCash.Count > 0)
                {
                    if (CashMasterCollection.Count != verifiedCash.Count)
                    {
                        IsEnabledCompleteVerification = false; return;
                    }
                    foreach (var item in verifiedCash)
                    {
                        CashMaster chk = CashMasterCollection.FirstOrDefault(x => x.CashId == item.CashId);
                        if (chk.Quantity != item.Quantity)
                        {
                            IsEnabledCompleteVerification = false; return;
                        }
                        else
                        {
                            IsEnabledCompleteVerification = true;
                        }
                    }
                    //CashDetais o = verifiedCash[0];
                    //if (ObjCashDetails.PENNIES == o.PENNIES
                    //    && ObjCashDetails.DIMES == o.DIMES
                    //    && ObjCashDetails.DOLLERCOIN == o.DOLLERCOIN
                    //    && ObjCashDetails.FIFTYDB == o.FIFTYDB
                    //    && ObjCashDetails.FIVEDB == o.FIVEDB
                    //    && ObjCashDetails.HALFDOLERCOIN == o.HALFDOLERCOIN
                    //    && ObjCashDetails.HUNDREDDB == o.HUNDREDDB
                    //    && ObjCashDetails.NICKELS == o.NICKELS
                    //    && ObjCashDetails.ONEDB == o.ONEDB
                    //    && ObjCashDetails.QUARTERS == o.QUARTERS
                    //    && ObjCashDetails.TENDB == o.TENDB
                    //    && ObjCashDetails.TWENTYDB == o.TWENTYDB
                    //    )
                    //{
                    //    IsEnabledCompleteVerification = true;
                    //}
                    //else
                    //{
                    //    IsEnabledCompleteVerification = false;
                    //    return;
                    //}

                }
                #endregion
                #region Check
                if (verifiedCheck.Count > 0)
                {
                    if (ObjCheckDetails.SelectedItems.Count != verifiedCheck.Count)
                    {
                        IsEnabledCompleteVerification = false; return;
                    }
                    else
                    {
                        IsEnabledCompleteVerification = true;
                        //foreach (var item in verifiedCheck)
                        //{
                        //    CheckDetails chk = ObjCheckDetails.SelectedItems.FirstOrDefault(x => x.CheckID == item.CheckID);
                        //    if (chk == null)
                        //    {
                        //        IsEnabledCompleteVerification = false; return;
                        //    }
                        //    else
                        //    {
                        //        IsEnabledCompleteVerification = true;
                        //    }
                        //}
                    }
                }
                #endregion
                #region Money Order
                if (verifiedMo.Count > 0)
                {
                    if (ObjMoneyOrderDetails.SelectedItems.Count != verifiedMo.Count)
                    {
                        IsEnabledCompleteVerification = false; return;
                    }
                    else
                    {
                        IsEnabledCompleteVerification = true;
                        //foreach (var item in verifiedMo)
                        //{
                        //    MoneyOrderDetails mo = ObjMoneyOrderDetails.SelectedItems.FirstOrDefault(x => x.MoneyOrderID == item.MoneyOrderID);
                        //    if (mo == null)
                        //    {
                        //        IsEnabledCompleteVerification = false; return;
                        //    }
                        //    else
                        //    {
                        //        IsEnabledCompleteVerification = true;
                        //    }
                        //}
                    }
                }
                #endregion
                if (IsEnabledCompleteVerification)
                {
                    IsEnabledOutOfBalanceSettlement = false;
                }
            }
            catch (Exception) 
            {
                
            }
            
        }

        public override bool ConfirmSave()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][Start:ConfirmSave]\t" + DateTime.Now + "");
            IsDirtyStatic = false;
            CanNavigate = true;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][End:ConfirmSave]\t" + DateTime.Now + "");
            return base.ConfirmSave();
        }
        public override bool ConfirmCancel()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][Start:ConfirmCancel]\t" + DateTime.Now + "");
            //IsDirtyStatic = false;
            CanNavigate = false;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][SettlementConfirmationViewModel][End:ConfirmCancel]\t" + DateTime.Now + "");
            return base.ConfirmCancel();
        }


        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }

    public class CashDetais : BaseViewModel
    {
        private int pENNIES = 0;

        public int PENNIES
        {
            get { return pENNIES; }
            set
            {
                SetCashVerified(pENNIES, value, 0.01, "PNSC");
                pENNIES = value; OnPropertyChanged("PENNIES");
            }
        }
        private int nICKELS = 0;

        public int NICKELS
        {
            get { return nICKELS; }
            set
            {
                SetCashVerified(nICKELS, value, 0.05, "NIKC");

                nICKELS = value; OnPropertyChanged("NICKELS");
            }
        }
        private int dIMES = 0;

        public int DIMES
        {
            get { return dIMES; }
            set
            {
                SetCashVerified(dIMES, value, 0.10, "DIMC");

                dIMES = value; OnPropertyChanged("DIMES");
            }
        }
        private int qUARTERS = 0;

        public int QUARTERS
        {
            get { return qUARTERS; }
            set
            {
                SetCashVerified(qUARTERS, value, 0.25, "QTRC");

                qUARTERS = value; OnPropertyChanged("QUARTERS");
            }
        }
        private int hALFDOLERCOIN = 0;

        public int HALFDOLERCOIN
        {
            get { return hALFDOLERCOIN; }
            set
            {
                SetCashVerified(hALFDOLERCOIN, value, 0.50, "HFDC");
                hALFDOLERCOIN = value; OnPropertyChanged("HALFDOLERCOIN");
            }
        }
        private int dOLLERCOIN = 0;

        public int DOLLERCOIN
        {
            get { return dOLLERCOIN; }
            set
            {
                SetCashVerified(dOLLERCOIN, value, 1, "ONDC");
                dOLLERCOIN = value; OnPropertyChanged("DOLLERCOIN");
            }
        }

        private int oNEDB = 0;

        public int ONEDB
        {
            get { return oNEDB; }
            set
            {
                SetCashVerified(oNEDB, value, 1, "ONDB");
                oNEDB = value; OnPropertyChanged("ONEDB");
            }
        }

        private int fIVEDB = 0;

        public int FIVEDB
        {
            get { return fIVEDB; }
            set
            {
                SetCashVerified(fIVEDB, value, 5, "FVDB");
                fIVEDB = value; OnPropertyChanged("FIVEDB");
            }
        }

        private int tENDB = 0;

        public int TENDB
        {
            get { return tENDB; }
            set
            {
                SetCashVerified(tENDB, value, 10, "TNDB");
                tENDB = value; OnPropertyChanged("TENDB");
            }
        }

        private int tWENTYDB = 0;

        public int TWENTYDB
        {
            get { return tWENTYDB; }
            set
            {
                SetCashVerified(tWENTYDB, value, 20, "TWDB");
                tWENTYDB = value; OnPropertyChanged("TWENTYDB");
            }
        }

        private int fIFTYDB = 0;

        public int FIFTYDB
        {
            get { return fIFTYDB; }
            set
            {
                SetCashVerified(fIFTYDB, value, 50, "FTDB");
                fIFTYDB = value; OnPropertyChanged("FIFTYDB");
            }
        }

        private int hUNDREDDB = 0;

        public int HUNDREDDB
        {
            get { return hUNDREDDB; }
            set
            {
                SetCashVerified(hUNDREDDB, value, 100, "HDB");
                hUNDREDDB = value; OnPropertyChanged("HUNDREDDB");
            }
        }
        private string settlementID;

        public string SettlementID
        {
            get { return settlementID; }
            set { settlementID = value; }
        }

        public void SetCashVerified(int previousValue, int newValue, double multiplier, string cashCode)
        {
            double preValue = Math.Round((previousValue * multiplier), 2);
            double postValue = Math.Round((newValue * multiplier), 2);
            SettlementConfirmationViewModel.CashVerified = Math.Round(SettlementConfirmationViewModel.CashVerified + postValue - preValue, 2);
            if (SettlementConfirmationViewModel.CashMasterCollection.Count == 0)
            {
                return;
            }
            SettlementConfirmationViewModel.CashMasterCollection.FirstOrDefault(x => x.CashCode.ToUpper().Trim() == cashCode).Quantity = newValue;
            SettlementConfirmationViewModel.CashMasterCollection.FirstOrDefault(x => x.CashCode.ToUpper().Trim() == cashCode).Amount = postValue.ToString();

        }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }

    public class CheckDetails : BaseViewModel
    {

        private string date;

        public string Date
        {
            get { return date; }
            set { date = value; }
        }
        private string amount;

        public string Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        private string customerName;

        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }

        private string checkNo;

        public string CheckNo
        {
            get { return checkNo; }
            set { checkNo = value; }
        }

        private ObservableCollection<CheckDetails> selectedItems;

        public ObservableCollection<CheckDetails> SelectedItems
        {
            get
            {
                if (selectedItems == null)
                {
                    selectedItems = new ObservableCollection<CheckDetails>();
                }

                return this.selectedItems;
            }
        }
        private CheckDetails selectedItem;

        public CheckDetails SelectedItem
        {
            get { return selectedItem; }
            set { selectedItem = value; OnPropertyChanged("SelectedItem"); }
        }

        private string settlementID;

        public string SettlementID
        {
            get { return settlementID; }
            set { settlementID = value; }
        }
        private string checkID;

        public string CheckID
        {
            get { return checkID; }
            set { checkID = value; }
        }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }

    public class MoneyOrderDetails : BaseViewModel
    {

        private string date;

        public string Date
        {
            get { return date; }
            set { date = value; }
        }
        private string amount;

        public string Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        private string moneyOrderNo;

        public string MoneyOrderNo
        {
            get { return moneyOrderNo; }
            set { moneyOrderNo = value; }
        }
        private string feeAmount;

        public string FeeAmount
        {
            get { return feeAmount; }
            set { feeAmount = value; }
        }

        private ObservableCollection<MoneyOrderDetails> selectedItems;

        public ObservableCollection<MoneyOrderDetails> SelectedItems
        {
            get
            {
                if (selectedItems == null)
                {
                    selectedItems = new ObservableCollection<MoneyOrderDetails>();
                }

                return this.selectedItems;
            }
        }
        private MoneyOrderDetails selectedItem;

        public MoneyOrderDetails SelectedItem
        {
            get { return selectedItem; }
            set { selectedItem = value; OnPropertyChanged("SelectedItem"); }
        }
        private string settlementID;

        public string SettlementID
        {
            get { return settlementID; }
            set { settlementID = value; }
        }
        private string moneyOrderID;

        public string MoneyOrderID
        {
            get { return moneyOrderID; }
            set { moneyOrderID = value; }
        }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }

    public class VerifiedFunds
    {
        private string cash;

        public string Cash
        {
            get { return cash; }
            set { cash = value; }
        }
        private string check;

        public string Check
        {
            get { return check; }
            set { check = value; }
        }
        private string moneyOrder;

        public string MoneyOrder
        {
            get { return moneyOrder; }
            set { moneyOrder = value; }
        }
        private string expenses;

        public string Expenses
        {
            get { return expenses; }
            set { expenses = value; }
        }
        private string totalVerified;

        public string TotalVerified
        {
            get { return totalVerified; }
            set { totalVerified = value; }
        }
        private string overShort;

        public string OverShort
        {
            get { return overShort; }
            set { overShort = value; }
        }
        private string payments;

        public string Payments
        {
            get { return payments; }
            set { payments = value; }
        }

        private string settlementID;

        public string SettlementID
        {
            get { return settlementID; }
            set { settlementID = value; }
        }

    }

    public class UsersForVerification
    {
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class RejectionReason
    {
        public string ReasonID { get; set; }
        public string ReasonName { get; set; }
    }

    public class CashMaster
    {
        public string SettlementDetailID { get; set; }
        public string CashId { get; set; }
        public string CashType { get; set; }
        public string CashCode { get; set; }
        public string CashDescription { get; set; }
        public int Quantity { get; set; }
        public string Amount { get; set; }
    }

    public class CheckEntryDetails
    {
        private string checkNo;

        public string CheckNo
        {
            get { return checkNo; }
            set { checkNo = value; }
        }
        private string customerId;

        public string CustomerId
        {
            get { return customerId; }
            set { customerId = value; }
        }
        private string customerName;

        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }
        private string checkAmount;

        public string CheckAmount
        {
            get { return checkAmount; }
            set { checkAmount = value; }
        }
        private string checkDate;

        public string CheckDate
        {
            get { return checkDate; }
            set { checkDate = value; }
        }
        private string createdBy;

        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        private string routId;

        public string RoutId
        {
            get { return routId; }
            set { routId = value; }
        }
    }

}
