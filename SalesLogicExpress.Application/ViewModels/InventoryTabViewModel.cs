﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using System.Threading;
using System.Data;
using SalesLogicExpress.Domain;
using System.ComponentModel;
using System.Drawing.Imaging;
using log4net;
using System.Windows.Threading;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using SalesLogicExpress.Application.Managers;

namespace SalesLogicExpress.Application.ViewModels
{
    public class InventoryTabViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.InventoryTabViewModel");
        InventoryManager manager = new InventoryManager();
        private string searchText = string.Empty;

        #region Commands
        public DelegateCommand SearchItemOnType { get; set; }
        public DelegateCommand InventoryItemSelectionChanged { get; set; }
        public DelegateCommand UoMSelectionChanged { get; set; }
        public DelegateCommand DeclineAdjustment { get; set; }
        public DelegateCommand ValueChanged { get; set; }
        public DelegateCommand ClearSearchText { get; set; }

        // Inventory Print
        public DelegateCommand Print { get; set; }

        #region Par Level Popup Commands
        public DelegateCommand OpenParLevelPopUp { get; set; }
        public DelegateCommand SaveParLevelAdjustment { get; set; }
        #endregion

        #region Inventory Adjustment Popup
        public DelegateCommand OpenAdjustmentPopup { get; set; }
        public DelegateCommand ConfirmInventoryAdjustment { get; set; }
        public DelegateCommand SaveAdjustment { get; set; }
        #endregion

        #endregion

        #region Properties
        bool isAdjustmentButtonVisible = false;
        public Guid MessageToken { get; set; }
        public bool IsAdjustmentButtonVisible
        {
            get
            {
                return this.isAdjustmentButtonVisible;
            }
            set
            {
                this.isAdjustmentButtonVisible = value;
                OnPropertyChanged("IsAdjustmentButtonVisible");
            }
        }
        bool isAdjustmentButtonEnabled = false;
        public bool IsAdjustmentButtonEnabled
        {
            get
            {
                return this.isAdjustmentButtonEnabled;
            }
            set
            {
                this.isAdjustmentButtonEnabled = value;
                OnPropertyChanged("IsAdjustmentButtonEnabled");
            }
        }
        bool isParLevelButtonEnable = false;
        public bool IsParLevelButtonEnable
        {
            get
            {
                return this.isParLevelButtonEnable;
            }
            set
            {
                this.isParLevelButtonEnable = value;
                OnPropertyChanged("IsParLevelButtonEnable");
            }
        }
        BaseViewModel _ParentViewModel;
        public BaseViewModel ParentViewModel
        {
            get
            {
                return this._ParentViewModel;
            }
            set
            {
                this._ParentViewModel = value;
                OnPropertyChanged("ParentViewModel");
            }
        }
        InventoryItem _SelectedItem;
        public InventoryItem SelectedItem
        {
            get
            {
                return this._SelectedItem;
            }
            set
            {
                this._SelectedItem = value;
                IsParLevelButtonEnable = this._SelectedItem == null ? false : true;
                IsAdjustmentButtonEnabled = this._SelectedItem == null ? false : true;
                OnPropertyChanged("SelectedItem");
            }
        }

        private int _adjustmentSelectedIndex = -1;

        public int AdjustmentSelectedIndex
        {
            get { return _adjustmentSelectedIndex; }
            set
            {
                _adjustmentSelectedIndex = value;
                if (value != -1)
                    IsParLevelSaveEnabled = true;
                OnPropertyChanged("AdjustmentSelectedIndex");
            }
        }
        ParLevelAdjustment _ParLevelAdjustment;
        public ParLevelAdjustment ParAdjustment
        {
            get
            {
                return this._ParLevelAdjustment;
            }
            set
            {
                this._ParLevelAdjustment = value;
                OnPropertyChanged("ParAdjustment");
            }
        }
        InventoryAdjustment _InventoryAdjustment;
        public InventoryAdjustment InventoryItemAdjustment
        {
            get
            {
                return this._InventoryAdjustment;
            }
            set
            {
                this._InventoryAdjustment = value;
                OnPropertyChanged("InventoryItemAdjustment");
            }
        }

        private bool _isAdjustmentSaveEnabled;

        public bool IsAdjustmentSaveEnabled
        {
            get { return _isAdjustmentSaveEnabled; }
            set
            {
                _isAdjustmentSaveEnabled = value;

                OnPropertyChanged("IsAdjustmentSaveEnabled");
            }
        }

        bool _isParLevelSaveEnabled = false;
        public bool IsParLevelSaveEnabled
        {
            get { return _isParLevelSaveEnabled; }
            set { _isParLevelSaveEnabled = value; OnPropertyChanged("IsParLevelSaveEnabled"); }
        }

        private List<string> itemUOMList;
        public List<string> ItemUomList
        {
            get { return itemUOMList; }
            set { itemUOMList = value; OnPropertyChanged("ItemUomList"); }
        }

        bool isSearchVisible = false;
        public bool IsSearchVisible
        {
            get
            {
                return this.isSearchVisible;
            }
            set
            {
                this.isSearchVisible = value;
                OnPropertyChanged("IsSearchVisible");
            }
        }

        bool _IsSearching = false;
        public bool IsSearching
        {
            get
            {
                return _IsSearching;
            }
            set
            {
                _IsSearching = value;
                OnPropertyChanged("IsSearching");
            }
        }
        private ObservableCollection<Models.InventoryItem> _InventoryItems = new ObservableCollection<InventoryItem>();
        private ObservableCollection<Models.InventoryItem> InventoryItemList = new ObservableCollection<InventoryItem>();
        public ObservableCollection<Models.InventoryItem> InventoryItems
        {
            get
            {
                return this._InventoryItems;
            }
            set
            {
                _InventoryItems = value;
                OnPropertyChanged("InventoryItems");
            }
        }

        private int _minumumUoMQty;

        public int MinimumUoMQty
        {
            get { return _minumumUoMQty; }
            set { _minumumUoMQty = value; OnPropertyChanged("MinimumUoMQty"); }
        }
        public PopUpQuantities PopUpQuantities { get; set; }
        public List<ItemUOMConversion> UOMConversionList { get; set; }


        #endregion

        #region Constructor
        public InventoryTabViewModel()
        {
            InitializeCommands();
            LoadData();
        }
        public async void LoadData()
        {
            await Task.Run(() =>
            {
                InventoryItems = new ObservableCollection<InventoryItem>(manager.GetInventoryItems(""));
                InventoryItemList = new ObservableCollection<InventoryItem>(InventoryItems.ToList<InventoryItem>());
                ParentViewModel.IsBusy = false;
            });
        }
        #endregion

        #region Methods

        //SATHISH for TUI Issue ID-214
        //inventory->provided the item no in search field, the item got filtered and clicking on X to remove the item no -> removed the item no from the search field but the inventory is not getting refreshed
        private void InitializeCommands()
        {
            SearchItemOnType = new DelegateCommand((param) =>
            {
                if (param == null) return;
                if (SearchText == string.Empty)
                    param = string.Empty;

                searchText = param.ToString().ToLower();
                if (searchText.Trim().Length >= 0)
                {
                    RunSearch(searchText);
                }
            });
            ClearSearchText = new DelegateCommand((param) =>
            {
                param = string.Empty;
                searchText = string.Empty;
                SearchText = string.Empty;
                RunSearch(SearchText);
            });
            UoMSelectionChanged = new DelegateCommand((param) =>
            {
                // Updates minimum value for selection validation on um change
                Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][Start:UoMSelectionChanged]");
                try
                {
                    if (param == null) return;
                    var itemUomConversionFactor = 0.0;
                    if (UoMManager.ItemUoMFactorList != null)
                    {
                        var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == SelectedItem.ItemId) && (x.FromUOM == SelectedItem.PrimaryUOM) && (x.ToUOM == param.ToString()));
                        itemUomConversionFactor = itemUomConversion == null ? UoMManager.GetUoMFactor(SelectedItem.PrimaryUOM, param.ToString(), Convert.ToInt32(SelectedItem.ItemId.Trim()), SelectedItem.ItemNumber.Trim()) : itemUomConversion.ConversionFactor;
                    }
                    else
                    {
                        itemUomConversionFactor = UoMManager.GetUoMFactor(SelectedItem.PrimaryUOM, param.ToString(), Convert.ToInt32(SelectedItem.ItemId.Trim()), SelectedItem.ItemNumber.Trim());
                    }
                    MinimumUoMQty = -Convert.ToInt32(SelectedItem.AvailableQty * itemUomConversionFactor);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][UoMSelectionChanged][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][End:UoMSelectionChanged]");
            });
            InventoryItemSelectionChanged = new DelegateCommand((param) =>
            {
                if (param == null) return;
                IsParLevelButtonEnable = true;
                IsAdjustmentButtonEnabled = true;
            });

            // Print Button Implement

            Print = new DelegateCommand((param) =>
            {
                // Test - With DB Script Changes
                ShowInventoryReport();
            });

            OpenParLevelPopUp = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][Start:OpenParLevelPopUp]");
                try
                {
                    if (SelectedItem == null) return;
                    ParAdjustment = new ParLevelAdjustment
                    {
                        Item = SelectedItem,
                        ModifiedParlevel = SelectedItem.ParLevel
                    };
                    PopulateUoMList();

                    // Call to generate uom conversion list for current item.
                    //UOMConversionList = manager.GetUOMConversionList(ParAdjustment.Item.ItemId);

                    PopUpQuantities = new PopUpQuantities();

                    if (UoMManager.ItemUoMFactorList != null)
                    {
                        var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == SelectedItem.ItemId) && (x.FromUOM == SelectedItem.PrimaryUOM) && (x.ToUOM == SelectedItem.DefaultUOM)) ?? new ItemUOMConversion
                        {
                            FromUOM = SelectedItem.PrimaryUOM,
                            ToUOM = SelectedItem.DefaultUOM,
                            ItemNumber = SelectedItem.ItemNumber,
                            ItemID = SelectedItem.ItemId,
                            ConversionFactor = UoMManager.GetUoMFactor(SelectedItem.PrimaryUOM, SelectedItem.DefaultUOM, Convert.ToInt32(SelectedItem.ItemId.Trim()), SelectedItem.ItemNumber.Trim())
                        };

                        DoDefaultConversion(itemUomConversion);
                    }
                    else
                    {
                        var itemUomConversion = new ItemUOMConversion
                        {
                            FromUOM = SelectedItem.PrimaryUOM,
                            ToUOM = SelectedItem.DefaultUOM,
                            ItemNumber = SelectedItem.ItemNumber,
                            ItemID = SelectedItem.ItemId,
                            ConversionFactor = UoMManager.GetUoMFactor(SelectedItem.PrimaryUOM, SelectedItem.DefaultUOM, Convert.ToInt32(SelectedItem.ItemId.Trim()), SelectedItem.ItemNumber.Trim())
                        };
                        DoDefaultConversion(itemUomConversion);
                    }
                    var dialog = new Helpers.DialogWindow { TemplateKey = "OpenParLevelDialog", Title = "Edit Par Level", Payload = this };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][OpenParLevelPopUp][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][End:OpenParLevelPopUp]");
            });
            OpenAdjustmentPopup = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][Start:OpenAdjustmentPopup]");
                try
                {
                    InventoryItemAdjustment = new InventoryAdjustment();
                    InventoryItemAdjustment.Item = SelectedItem;
                    InventoryItemAdjustment.AdjustmentQuantity = SelectedItem.ActualQtyOnHand;
                    InventoryItemAdjustment.AdjustmentUOM = SelectedItem.PrimaryUOM;

                    PopulateUoMList();

                    // UOMConversionList = manager.GetUOMConversionList(InventoryItemAdjustment.Item.ItemId);

                    PopUpQuantities = new PopUpQuantities();

                    if (UoMManager.ItemUoMFactorList != null)
                    {
                        var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == SelectedItem.ItemId) && (x.FromUOM == SelectedItem.PrimaryUOM) && (x.ToUOM == SelectedItem.DefaultUOM)) ?? new ItemUOMConversion
                        {
                            FromUOM = SelectedItem.PrimaryUOM,
                            ToUOM = SelectedItem.DefaultUOM,
                            ItemNumber = SelectedItem.ItemNumber,
                            ItemID = SelectedItem.ItemId,
                            ConversionFactor = UoMManager.GetUoMFactor(SelectedItem.PrimaryUOM, SelectedItem.DefaultUOM, Convert.ToInt32(SelectedItem.ItemId.Trim()), SelectedItem.ItemNumber.Trim())
                        };

                        DoDefaultConversion(itemUomConversion);
                        //ItemUOMConversion conversionDetails = UoMManager.ItemUoMFactorList.FirstOrDefault(item => item.ItemNumber == SelectedItem.ItemNumber && item.FromUOM == SelectedItem.PrimaryUOM && item.ToUOM == SelectedItem.DefaultUOM);
                        //InventoryItemAdjustment.ConversionFactor = conversionDetails == null ? 1 : conversionDetails.ConversionFactor;
                        InventoryItemAdjustment.ConversionFactor = itemUomConversion.ConversionFactor;
                    }
                    else
                    {
                        var itemUomConversion = new ItemUOMConversion
                        {
                            FromUOM = SelectedItem.PrimaryUOM,
                            ToUOM = SelectedItem.DefaultUOM,
                            ItemNumber = SelectedItem.ItemNumber,
                            ItemID = SelectedItem.ItemId,
                            ConversionFactor = UoMManager.GetUoMFactor(SelectedItem.PrimaryUOM, SelectedItem.DefaultUOM, Convert.ToInt32(SelectedItem.ItemId.Trim()), SelectedItem.ItemNumber.Trim())
                        };
                        DoDefaultConversion(itemUomConversion);
                        InventoryItemAdjustment.ConversionFactor = itemUomConversion.ConversionFactor;
                    }

                    InventoryItemAdjustment.ReasonCodeList = new InventoryManager().GetReasonCodesForInventoryAdjustment();

                    // Open Inventory Adjustment Level Dialog
                    // On Click of Save, Confirmation Dialog is shown.
                    var dialog = new Helpers.DialogWindow { TemplateKey = "OpenAdjustmentDialog", Title = "Inventory Adjustment", Payload = this };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][OpenAdjustmentPopup][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][End:OpenAdjustmentPopup]");
            });
            ConfirmInventoryAdjustment = new DelegateCommand((param) =>
            {
                var dialog = new Helpers.DialogWindow { TemplateKey = "AdjustmentVerification", Title = "Inventory Adjustment Warning!", Payload = this };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });
            SaveAdjustment = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][Start:SaveAdjustment]");
                try
                {
                    var adjustedItem = InventoryItemAdjustment;
                    if (!string.Equals(InventoryItemAdjustment.AdjustmentUOM, InventoryItemAdjustment.Item.PrimaryUOM))
                    {
                        var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == InventoryItemAdjustment.Item.ItemId) && (x.FromUOM == InventoryItemAdjustment.AdjustmentUOM) && (x.ToUOM == InventoryItemAdjustment.Item.PrimaryUOM));
                        double conversionFactor = 1;
                        conversionFactor = itemUomConversion != null ? itemUomConversion.ConversionFactor : UoMManager.GetUoMFactor(InventoryItemAdjustment.AdjustmentUOM, InventoryItemAdjustment.Item.PrimaryUOM, Convert.ToInt32(InventoryItemAdjustment.Item.ItemId.Trim()), InventoryItemAdjustment.Item.ItemNumber.Trim());
                        InventoryItemAdjustment.AdjustmentQuantity = InventoryItemAdjustment.Item.OnHandQty + Convert.ToInt32(InventoryItemAdjustment.AdjustmentQuantity * conversionFactor);

                    }
                    if (manager.SaveInventoryAdjustment(InventoryItemAdjustment.Clone()))
                    {
                        if (InventoryItems.Any(item => item.ItemNumber == InventoryItemAdjustment.Item.ItemNumber))
                        {
                            //InventoryItems.First(item => item.ItemNumber == InventoryItemAdjustment.Item.ItemNumber).IsApproved = false;
                            InventoryItems.First(item => item.ItemNumber == InventoryItemAdjustment.Item.ItemNumber).IsApproved = 1;
                            InventoryItems.First(item => item.ItemNumber == InventoryItemAdjustment.Item.ItemNumber).IsApplied = false;
                            InventoryItems.First(item => item.ItemNumber == InventoryItemAdjustment.Item.ItemNumber).OnHandQty += InventoryItemAdjustment.AdjustmentQuantity;
                            InventoryItems.First(item => item.ItemNumber == InventoryItemAdjustment.Item.ItemNumber).AvailableQty += InventoryItemAdjustment.AdjustmentQuantity;
                            // IsAdjustmentButtonEnabled = false;
                        }
                        //ParentViewModel.IsBusy = true;
                        //LoadData();
                    }
                    Messenger.Default.Send<CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][SaveAdjustment][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][End:SaveAdjustment]");
            });
            SaveParLevelAdjustment = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][Start:SaveParLevelAdjustment]");
                try
                {
                    var adjustedParLevel = ParAdjustment;
                    if (!string.Equals(ParAdjustment.SelectedParlevelUOM, ParAdjustment.Item.PrimaryUOM))
                    {
                        var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == ParAdjustment.Item.ItemId) && (x.FromUOM == ParAdjustment.SelectedParlevelUOM) && (x.ToUOM == ParAdjustment.Item.PrimaryUOM));
                        double conversionFactor = 1;
                        // gets conversion factor, to convert selected uom qty to primary uom
                        conversionFactor = itemUomConversion != null ? itemUomConversion.ConversionFactor : UoMManager.GetUoMFactor(ParAdjustment.SelectedParlevelUOM, ParAdjustment.Item.PrimaryUOM, Convert.ToInt32(ParAdjustment.Item.ItemId.Trim()), ParAdjustment.Item.ItemNumber.Trim());

                        ParAdjustment.ModifiedParlevel = Convert.ToInt32(ParAdjustment.ModifiedParlevel * conversionFactor);

                    }
                    if (manager.SaveParLevelAdjustment(ParAdjustment) && InventoryItems.Any(item => item.ItemNumber == ParAdjustment.Item.ItemNumber))
                    {
                        InventoryItems.First(item => item.ItemNumber == ParAdjustment.Item.ItemNumber).ParLevel = ParAdjustment.ModifiedParlevel;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][SaveParLevelAdjustment][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][End:SaveParLevelAdjustment]");
            });
            DeclineAdjustment = new DelegateCommand((param) =>
            {
                Messenger.Default.Send<CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
            });
            ValueChanged = new DelegateCommand((param) =>
            {
                switch (param.ToString().ToLower())
                {
                    case "parlevel":
                        IsParLevelSaveEnabled = true; //IsParLevelSaveEnabled = ParAdjustment.ModifiedParlevel != 0;// Check removed as par requirement
                        break;
                    case "adjustment":
                        IsAdjustmentSaveEnabled = !(InventoryItemAdjustment.AdjustmentQuantity < MinimumUoMQty);
                        IsAdjustmentSaveEnabled = AdjustmentSelectedIndex != -1;
                        break;
                }

            });
        }
        CancellationTokenSource tokenForCancelTask = null;
        async void RunSearch(string searchText)
        {
            await Task.Run(() =>
            {
                IsSearching = true;
                if (tokenForCancelTask != null)
                {
                    tokenForCancelTask.Cancel();
                }
                tokenForCancelTask = new CancellationTokenSource();
                Task.Run(async () => await SearchItem(searchText, tokenForCancelTask.Token), tokenForCancelTask.Token);
            });
        }

        private async Task SearchItem(string searchText, CancellationToken token)
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][AsyncStart:SearchItem]");
            try
            {

                await Task.Delay(700);
                InventoryItems = new ObservableCollection<InventoryItem>(InventoryItemList.Where(i => (i.ItemNumber.ToLower().Contains(searchText) || i.ItemDescription.ToLower().Contains(searchText))).ToList<InventoryItem>());
                IsSearching = false;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][SearchItem][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][AsyncEnd:SearchItem]");
        }
        private async void SearchInventory(string searchText)
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][AsyncStart:SearchInventory]");
            try
            {
                Action<int> RunSearch;
                RunSearch = delegate(int a)
                {
                    foreach (InventoryItem i in InventoryItems)
                    {
                        i.IsVisible = (i.ItemNumber.ToLower().Contains(searchText) || i.ItemDescription.ToLower().Contains(searchText)) ? true : false;
                    }
                };

                await Task.Run(() =>
                {

                    foreach (InventoryItem i in InventoryItems)
                    {
                        i.IsVisible = (i.ItemNumber.ToLower().Contains(searchText) || i.ItemDescription.ToLower().Contains(searchText)) ? true : false;
                    }
                });
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][SearchInventory][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][AsyncEnd:SearchInventory]");
        }

        #region ShowReportInvertory
        public async void ShowInventoryReport()
        {
            //var dialog = new Helpers.DialogWindow { TemplateKey = "PrintInvertory", ContentTemplate = "Do you want to print this Invertory?", Title = "Print" };
            //Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][Start:ShowInventoryReport]");
            await Task.Run(() =>
            {
                try
                {
                    ParentViewModel.IsBusy = true;
                    var DocInfo = new ReportEngine.DocumentInformation();
                    var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                    repEngine.Notify += repEngine_Notify;
                    repEngine.Notify -= repEngine_Notify;
                    repEngine.Log = this.Logger;
                    ReportEngine.IReportSLE ReportData = null;

                    var DocumentId = 0;
                    //var DocumentId = AddLoadReplenishment.DocumentID;
                    var SourceDocumentNo = Convert.ToDecimal(InventoryItemList[0].ItemId);
                    if (DocumentId <= 0)
                    {
                        // PYU - User Issue ID: 10 - CR ( Report should generate Based On - Show Zero Quantity Check) 
                        // Added By Velmani Karnan (12/13/2016)
                        if (PayloadManager.RouteReplenishmentPayload.IsCheckedShowZeroQuantity == true)
                        {
                            ReportData = new ReportViewModels.InventoryReport(InventoryItemList);
                        }
                        else
                        {
                            var data = InventoryItemList.Where(x => x.ActualAvailableQty > 0);
                            ObservableCollection<InventoryItem> ZeroFilteredInventoryItemList = new ObservableCollection<InventoryItem>(data.ToList<InventoryItem>());
                            ReportData = new ReportViewModels.InventoryReport(ZeroFilteredInventoryItemList);
                        }
                        DocInfo = repEngine.Generate<Reporting.Inventory>(SourceDocumentNo, true, ReportEngine.ReportCategory.Route,
                                            ReportEngine.ReportsTypes.Inventory, ReportData, null, ReportEngine.ContentTypes.PDF);
                    }
                    else
                    {
                        DocInfo.DocumentId = DocumentId;
                    }

                    if (DocInfo.IsEndOfSequence)
                    {
                        ParentViewModel.IsBusy = false;
                        if (System.Windows.Application.Current != null)
                        {
                            System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)delegate // <--- HERE
                            {
                                Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, Please sync to continue" }, MessageToken);
                            });
                        }
                        return;
                    }

                    //if (DocInfo.IsDocumentCreated)
                    //    ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.Inventory", "ItemId", SourceDocumentNo, DocInfo.DocumentId);
                    //AddLoadReplenishment.DocumentID = DocInfo.DocumentId; //Replace Latest
                    var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Route, DocInfo.DocumentId, null);
                    if (ReportBytes == null)
                    {
                        ParentViewModel.IsBusy = false;
                        if (System.Windows.Application.Current != null)
                        {
                            System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)delegate // <--- HERE
                            {
                                Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Report not generated, Please try again." }, MessageToken);
                            });
                        }
                        return;
                    }
                    SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
                    Payload.ReportFile = ReportBytes;
                    Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                    Payload.PreviousView = ViewModelMappings.View.RouteHome;
                    Payload.CurrentViewTitle = "Inventory Report";
                    Payload.PreviousViewTitle = "Inventory";
                    Payload.MessageToken = this.MessageToken;

                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.SelectTab = "";
                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.RouteHome, CloseCurrentView = true, Payload = Payload, ShowAsModal = false };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);

                    DocInfo = null;
                    ReportBytes = null;
                    Payload.ReportFile = null;
                    ParentViewModel.IsBusy = false;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][ShowInventoryReport][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    ParentViewModel.IsBusy = false;
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][END:ShowInventoryReport]");
            });

        }
        void repEngine_Notify(string msg)
        {
            System.Windows.MessageBox.Show(msg);
        }
        #endregion

        private void PopulateUoMList()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][Start:PopulateUoMList]");
            try
            {
                ItemUomList = new List<string>();
                if (!string.IsNullOrEmpty(SelectedItem.PrimaryUOM)) ItemUomList.Add(SelectedItem.PrimaryUOM);
                ItemUomList.AddRange(SelectedItem.OtherUOM);
                if (!string.IsNullOrEmpty(SelectedItem.DefaultUOM))
                    ItemUomList.Add(SelectedItem.DefaultUOM);
                ItemUomList = ItemUomList.Distinct().ToList();

                if (ParAdjustment != null)
                    ParAdjustment.SelectedParlevelUOM = SelectedItem.PrimaryUOM;

                if (InventoryItemAdjustment != null)
                    InventoryItemAdjustment.AdjustmentUOM = SelectedItem.PrimaryUOM;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][PopulateUoMList][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][End:PopulateUoMList]");
        }

        private void DoDefaultConversion(ItemUOMConversion itemUomConversion)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][Start:DoDefaultConversion]");
            try
            {
                if (itemUomConversion != null)
                {
                    var defaultConvFactor = itemUomConversion.ConversionFactor;
                    // Prepare quantities in default UOM
                    PopUpQuantities.DefaultAvailableQty = Convert.ToInt32(SelectedItem.AvailableQty * defaultConvFactor);
                    PopUpQuantities.DefaultComittedQty = Convert.ToInt32(SelectedItem.CommittedQty * defaultConvFactor);
                    PopUpQuantities.DefaultHeldQty = Convert.ToInt32(SelectedItem.HeldQty * defaultConvFactor);
                    PopUpQuantities.DefaultOnHandQty = Convert.ToInt32(SelectedItem.OnHandQty * defaultConvFactor);
                }
                else
                {
                    PopUpQuantities.DefaultAvailableQty = SelectedItem.AvailableQty;
                    PopUpQuantities.DefaultComittedQty = SelectedItem.CommittedQty;
                    PopUpQuantities.DefaultHeldQty = SelectedItem.HeldQty;
                    PopUpQuantities.DefaultOnHandQty = SelectedItem.OnHandQty;
                }
                MinimumUoMQty = -SelectedItem.AvailableQty;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][DoDefaultConversion][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][InventoryTabViewModel][End:DoDefaultConversion]");
        }

        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }

    public class PopUpQuantities
    {
        public int DefaultOnHandQty { get; set; }
        public int DefaultAvailableQty { get; set; }
        public int DefaultHeldQty { get; set; }
        public int DefaultComittedQty { get; set; }
        //public int PrimaryOnHandQty { get; set; }
        //public int PrimaryAvailableQty { get; set; }
        //public int PrimaryHeldQty { get; set; }
        //public int PrimaryCommittedQty { get; set; }


    }
}
