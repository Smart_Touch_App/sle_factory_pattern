﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Telerik.Windows.Controls;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModelPayload;
using System.Collections.ObjectModel;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using log4net;

namespace SalesLogicExpress.Application.ViewModels
{
    public class ARPaymentViewModel : BaseViewModel
    {
        #region Variable and object declaration

        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.ARPaymentViewModel");

        //public Guid MessageToken { get; set; }//Sathish Unwanted code
        //public static DateTime SelectedCalendarDate = DateTime.Now;}//Sathish Unwanted code
        private bool _IsReceiptHistoryTabActive = false;

        PaymentARModel.OpenAR objOpenAR = new PaymentARModel.OpenAR();
        ARPaymentManager objARPayManager = new ARPaymentManager(CommonNavInfo.Customer.CustomerNo, CommonNavInfo.Customer.Company);

        private List<PaymentARModel.ReceiptHistory> _ReceiptHistoryList = null;
        //private bool ShowCustomerBalanceDialog = false;
        //private bool ShowVoidReceiptApprovalDialog = false;
        private bool IsOriginalPayTypeCheck = false;
        private bool IsOriginalPayTypeCash = false;

        // String Handling - For Memroy Optimization at 12/26/2016 - By Velmani
        private string OriginalCheckNo = string.Empty;
        private string BillTo = string.Empty;
        private string _VoidApprovalCode = string.Empty;
        private string _VoidReasonCode = string.Empty;
        private string WebGeneratedApprovalCode = string.Empty;

        private bool IsContinueEditButtonClicked = false; //Values stores status to hold the actual check number. 
        private bool _IsVoidReceiptApprovalCodeNeeded = false;
       
        private ObservableCollection<string> _VoidReasonList = null;
        private bool _IsVoidReasonButtonEnabled = false;
        private bool _EnableReceiptHistoryUpdateButtons = false;
        private bool _EnableReceiptHistoryPrintButton = false;
        private bool _IsContinueEditReceiptButtonEnabled = false;
        
        public bool IsUnplannedCreatedForFuture { get; set; }
        #endregion

        #region Constructor and destructor
        public ARPaymentViewModel(bool flag)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:ARPaymentViewModel]");
            try
            {
                IsUnplannedCreatedForFuture = flag;
                BillTo = CustomerManager.GetCustomerBillTo(CommonNavInfo.Customer.customerNo);

                objOpenAR.CustomerNo = CommonNavInfo.Customer.CustomerNo;
                InitializeCommands();
                if (!string.IsNullOrEmpty(PayloadManager.PaymentPayload.ReceiptId))
                {
                    _IsReceiptHistoryTabActive = true;
                }

                GetInvoiceDetailsList();
                GetOpenAR();

                //#region Update Activity Count
                string tempStopDate = string.Empty;
                string stopID = string.Empty;



                if (PayloadManager.PaymentPayload.Customer.StopDate.Value.Date > DateTime.Now.Date)
                {
                    tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                    stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.PaymentPayload.Customer.CustomerNo, tempStopDate);
                }
                else
                {
                    tempStopDate = PayloadManager.PaymentPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                    stopID = PayloadManager.PaymentPayload.Customer.StopID;
                }


                //new Managers.CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                //new Managers.CustomerManager().UpdateActivityCount(stopID, false, true);
                //PayloadManager.PaymentPayload.Customer.SaleStatus = "";
                //PayloadManager.PaymentPayload.Customer.CompletedActivity += 1;
                //new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.PaymentPayload.Customer.CustomerNo);
                //#endregion
                IsDirty = false;

                if (!IsUnplannedCreatedForFuture)
                {
                    #region Update tem squncing no.
                    DateTime todayDate = Convert.ToDateTime(tempStopDate);
                    ViewModels.CustomerDashboard custDash = new CustomerDashboard();
                    // Checks for customer alteration if user invoked past activity.
                    if (PayloadManager.PaymentPayload.IsCustomerLoadedForPastActivity)
                    {
                        PayloadManager.PaymentPayload.IsCustomerLoadedForPastActivity = false;
                        PayloadManager.OrderPayload.Customer = new CustomerDashboard().GetNewCustomer(PayloadManager.PaymentPayload.StopDateBeforeInvokingActivity.Value.Date, PayloadManager.PaymentPayload.Customer);
                    }
                    //else
                    //    PayloadManager.OrderPayload.Customer = custDash.GetNewCustomer(todayDate, PayloadManager.PreOrderPayload.Customer);

                    #endregion
                }
                ObjOpenAR.SelectedItems.CollectionChanged += SelectedItems_CollectionChanged;
                if (ObjOpenAR.InvoiceDetailsList != null && SalesLogicExpress.Application.ViewModels.CommonNavInfo.NavigationParameter!=null)
                {
                    if (SalesLogicExpress.Application.ViewModels.CommonNavInfo.NavigationParameter.ToString() == "AutoCheckInvoice")
                    {
                        if (!string.IsNullOrEmpty(PayloadManager.OrderPayload.InvoiceNo) && ObjOpenAR.InvoiceDetailsList.Any(s => s.InvoiceNoInvDtl == PayloadManager.OrderPayload.InvoiceNo))
                        {
                            ObjOpenAR.SelectedItem = ObjOpenAR.InvoiceDetailsList.FirstOrDefault(s => s.InvoiceNoInvDtl == PayloadManager.OrderPayload.InvoiceNo);
                            ObjOpenAR.SelectedItems.Add(ObjOpenAR.InvoiceDetailsList.FirstOrDefault(s => s.InvoiceNoInvDtl == PayloadManager.OrderPayload.InvoiceNo));
                        }
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.NavigationParameter = null;
                    }
                }

                //Check if control is coming from customer activity, if yes then set the tab page details
                if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.CustomerHome_Activity.ToString())
                {
                    SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Activity;
                }
                else if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.CustomerHome_DashBoard.ToString())
                {
                    SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Dashboard;
                }
                //Check if control is comming from AR Aging tab screen, if yes then set the back navigation tab
                if (Helpers.ResourceManager.CommonNavInfo.ParentViewModel != null)
                {
                    if (Helpers.ResourceManager.CommonNavInfo.ParentViewModel.GetType().Equals(typeof(ARAgingSummaryViewModel)))
                    {
                        SelectedTab = ViewModelMappings.TabView.RouteHome.ARAging;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][ARPaymentViewModel][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:ARPaymentViewModel]");


        }

        #endregion

        #region Properties

        public string CustomerPaymentType
        {
            get
            {
                return (string.IsNullOrEmpty(CommonNavInfo.Customer.PaymentModeDescriptionForList) ? "" : CommonNavInfo.Customer.PaymentModeDescriptionForList.Replace(')', ',').Remove(0, 1));
            }
        }


        RequestAuthCodeViewModel _RequestCodeVM;
        public RequestAuthCodeViewModel RequestAuthCodeVM
        {
            get
            {
                return _RequestCodeVM;
            }
            set
            {
                _RequestCodeVM = value;
                OnPropertyChanged("RequestAuthCodeVM");
            }
        }
        string _RequestCode;
        public string RequestCode
        {
            get
            {
                return _RequestCode;
            }
            set
            {
                _RequestCode = value;
                OnPropertyChanged("RequestCode");
            }
        }
        private SalesLogicExpress.Domain.PaymentARModel.ReceiptHistory _SelectedReceipt = null;
        public SalesLogicExpress.Domain.PaymentARModel.ReceiptHistory SelectedReceipt
        {
            get
            {
                if (_SelectedReceipt == null)
                {
                    _SelectedReceipt = new PaymentARModel.ReceiptHistory();
                }

                return _SelectedReceipt;
            }
            set
            {
                _SelectedReceipt = value;
                OnPropertyChanged("SelectedReceipt");
            }
        }

        public PaymentARModel.OpenAR ObjOpenAR
        {
            get
            {
                return this.objOpenAR;
            }
            set
            {
                this.objOpenAR = value;
                OnPropertyChanged("ObjOpenAR");
            }
        }

        public DelegateCommand EditReceiptDlgtCmd { get; set; }  //Handles the click event of Edit button on Receipt History screen 
        public DelegateCommand ContinueEditReceiptDlgtCmd { get; set; } //Handles click event of contiue button present on Edit Receipt model dialog 
        public DelegateCommand VoidReceiptDlgtCmd { get; set; }  //Handles click event of Void button on Receipt History 
        public DelegateCommand ReceiptHistoryDlgtCmd { get; set; } //Handles the ReceiptHistory/Selection event
        public DelegateCommand PrintReceiptDlgtCmd { get; set; } //Handles click event of Void button on Receipt History 
        public DelegateCommand RefreshReceiptHistoryListDlgtCmd { get; set; }
        public DelegateCommand UpdateContinueButtonEnableStatusDlgtCmd { get; set; }

        public DelegateCommand GridUnloadedDlgtCmd { get; set; }

        #endregion

        #region Methods



        void LogPaymentActivity(ActivityKey key, CashDelivery CashDeliveryContext, string receiptId, List<string> InvoiceNos)
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:LogPaymentActivity]");

            try
            {
                PayloadManager.PaymentPayload.InvoiceNo = InvoiceNos;
                PayloadManager.PaymentPayload.CashDelivery = CashDeliveryContext;
                PayloadManager.PaymentPayload.Customer = CommonNavInfo.Customer;
                PayloadManager.PaymentPayload.RouteID = PayloadManager.OrderPayload.RouteID;
                //PayloadManager.PaymentPayload.TransactionID = PayloadManager.OrderPayload.TransactionID;

                if (!string.IsNullOrEmpty(receiptId))
                    PayloadManager.PaymentPayload.TransactionID = "receipt#" + receiptId;

                SalesLogicExpress.Domain.Activity ac = new SalesLogicExpress.Domain.Activity
                {
                    ActivityHeaderID = PayloadManager.OrderPayload.TransactionID,
                    CustomerID = PayloadManager.OrderPayload.Customer.CustomerNo,
                    RouteID = PayloadManager.OrderPayload.RouteID,
                    StopInstanceID = PayloadManager.OrderPayload.StopID,
                    ActivityType = key.ToString(),
                    ActivityStart = DateTime.Now,
                    ActivityEnd = DateTime.Now,
                    IsTxActivity = true,
                    ActivityDetailClass = PayloadManager.PaymentPayload.ToString(),
                    ActivityDetails = PayloadManager.PaymentPayload.SerializeToJson()
                };
                SalesLogicExpress.Domain.Activity a = ResourceManager.Transaction.LogActivity(ac);
                PayloadManager.PaymentPayload.TransactionID = a.ActivityHeaderID;


                //Mark completed activity for stop
                new CustomerManager().UpdateSequenceNo(PayloadManager.OrderPayload.Customer.StopID, PayloadManager.OrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd"));

                new CustomerManager().UpdateActivityCount(PayloadManager.OrderPayload.Customer.StopID, false, true);
                new Managers.OrderManager().UpdateNoSaleReason(PayloadManager.OrderPayload.Customer.StopID, PayloadManager.OrderPayload.Customer.CustomerNo);
                PayloadManager.OrderPayload.Customer.SaleStatus = "";
                PayloadManager.OrderPayload.Customer.CompletedActivity += 1;

                PayloadManager.OrderPayload.Customer.HasActivity = true;

                DateTime todayDate = Convert.ToDateTime(PayloadManager.OrderPayload.Customer.StopDate.Value);
                ViewModels.CustomerDashboard custDash = new CustomerDashboard();
                PayloadManager.OrderPayload.Customer = custDash.GetNewCustomer(todayDate, PayloadManager.OrderPayload.Customer);

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][LogPaymentActivity][ExceptionStackTrace = " + ex.Message + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:LogPaymentActivity]");
        }

        void SelectedItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:SelectedItems_CollectionChanged]");

            try
            {
                decimal total = 0;
                foreach (var item in ObjOpenAR.SelectedItems)
                {
                    total = total + item.OpenAmountInvDtl;
                }
                ObjOpenAR.SelectedInvoiceTotal = total;

                ObjOpenAR.CurrentUnappliedReceipts = ObjOpenAR.UnappliedReceipts;
                ObjOpenAR.NewRemainingInvoceBalance = ObjOpenAR.OpenInvices - ObjOpenAR.SelectedInvoiceTotal;
                //ObjOpenAR.PaymentAmount = ObjOpenAR.SelectedInvoiceTotal.ToString();
                ObjOpenAR.UnapplidAmount = Convert.ToDecimal(string.IsNullOrEmpty(ObjOpenAR.PaymentAmount) ? "0" : ObjOpenAR.PaymentAmount) > ObjOpenAR.SelectedInvoiceTotal ? Convert.ToDecimal(string.IsNullOrEmpty(ObjOpenAR.PaymentAmount) ? "0" : ObjOpenAR.PaymentAmount) - ObjOpenAR.SelectedInvoiceTotal : 0;//(PaymentAmount +CurrentUnappliedReceipts)- SelectedInvoiceTotal;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][SelectedItems_CollectionChanged][ExceptionStackTrace = " + ex.Message + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:SelectedItems_CollectionChanged]");
        }
        private void GetOpenAR()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:GetOpenAR]");

            try
            {
                List<Dictionary<string, decimal>> AgeingSummary = objARPayManager.GetAgeingSummary(objOpenAR.OpenInvoiceDetailsList);

                ObjOpenAR.ARBillTo = CustomerManager.GetCustomerBillTo(CommonNavInfo.Customer.customerNo);
                ObjOpenAR.ARBillToPhone = CustomerManager.GetCustomerPhone(CommonNavInfo.Customer.customerNo);

                foreach (Dictionary<string, decimal> item in AgeingSummary)
                {
                    if (item.ContainsKey("AR30Days"))
                    {
                        ObjOpenAR.AR30Days = item["AR30Days"];
                    }
                    else if (item.ContainsKey("AR60Days"))
                    {
                        ObjOpenAR.AR60Days = item["AR60Days"];
                    }
                    else if (item.ContainsKey("AR90Days"))
                    {
                        ObjOpenAR.AR90Days = item["AR90Days"];
                    }
                    else if (item.ContainsKey("AR90MoreDays"))
                    {
                        ObjOpenAR.AR90MoreDays = item["AR90MoreDays"];
                    }
                }


                if (ObjOpenAR.InvoiceDetailsList != null && ObjOpenAR.InvoiceDetailsList.Count > 0)
                {
                    ObjOpenAR.UnappliedReceipts = Convert.ToDecimal(objARPayManager.GetUnAppliedAmtForCustomer());
                    // ObjOpenAR.OpenInvices = ObjOpenAR.InvoiceDetailsList.Where(x => x.OpenAmountInvDtl > 0).Sum(x => x.OpenAmountInvDtl);
                    ObjOpenAR.OpenInvices = ObjOpenAR.InvoiceDetailsList.Sum(x => x.OpenAmountInvDtl);
                    ObjOpenAR.TotalBalance = ObjOpenAR.OpenInvices + ObjOpenAR.UnappliedReceipts;
                    //ObjOpenAR.TotalBalance = Convert.ToDecimal(objARPayManager.GetTotalBalanceForCustomer());
                    ObjOpenAR.CurrentUnappliedReceipts = ObjOpenAR.UnappliedReceipts;
                    ObjOpenAR.NewRemainingInvoceBalance = ObjOpenAR.OpenInvices - ObjOpenAR.SelectedInvoiceTotal;
                    //ObjOpenAR.PaymentAmount = "0";
                    ObjOpenAR.UnapplidAmount = Convert.ToDecimal(string.IsNullOrEmpty(ObjOpenAR.PaymentAmount) ? "0" : ObjOpenAR.PaymentAmount) > ObjOpenAR.SelectedInvoiceTotal ? Convert.ToDecimal((string.IsNullOrEmpty(ObjOpenAR.PaymentAmount) ? "0" : ObjOpenAR.PaymentAmount)) - ObjOpenAR.SelectedInvoiceTotal : 0;//(PaymentAmount +CurrentUnappliedReceipts)- SelectedInvoiceTotal;

                }
                else
                {
                    ObjOpenAR.UnappliedReceipts = Convert.ToDecimal(objARPayManager.GetUnAppliedAmtForCustomer());
                    ObjOpenAR.OpenInvices = 0;
                    ObjOpenAR.TotalBalance = ObjOpenAR.OpenInvices + ObjOpenAR.UnappliedReceipts;
                    ObjOpenAR.CurrentUnappliedReceipts = ObjOpenAR.UnappliedReceipts;
                    ObjOpenAR.NewRemainingInvoceBalance = ObjOpenAR.OpenInvices - ObjOpenAR.SelectedInvoiceTotal;
                    //ObjOpenAR.PaymentAmount = "0";
                    ObjOpenAR.UnapplidAmount = Convert.ToDecimal(string.IsNullOrEmpty(ObjOpenAR.PaymentAmount) ? "0" : ObjOpenAR.PaymentAmount) > ObjOpenAR.SelectedInvoiceTotal ? Convert.ToDecimal(string.IsNullOrEmpty(ObjOpenAR.PaymentAmount) ? "0" : objOpenAR.PaymentAmount) - ObjOpenAR.SelectedInvoiceTotal : 0;//(PaymentAmount +CurrentUnappliedReceipts)- SelectedInvoiceTotal;
                }


                ObjOpenAR.CheckDate = DateTime.Now.Date.ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][GetOpenAR][ExceptionStackTrace = " + ex.Message + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:GetOpenAR]");
        }
        private void GetInvoiceDetailsList()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:GetInvoiceDetailsList]");

            try
            {
                ObjOpenAR.AllInvoiceDetailsList = new Managers.InvoiceManager().GetInvoices(CommonNavInfo.Customer.CustomerNo);
                if (ObjOpenAR.ShowAllInvoices)
                {
                    if (ObjOpenAR.ByShipToFilter)
                        ObjOpenAR.InvoiceDetailsList = ObjOpenAR.AllInvoiceDetailsList.Where(x => x.InvoiceShipTo == CommonNavInfo.Customer.CustomerNo).ToObservableCollection();
                    else
                        ObjOpenAR.InvoiceDetailsList = ObjOpenAR.AllInvoiceDetailsList;
                }
                else
                {
                    if (ObjOpenAR.ByShipToFilter)
                        ObjOpenAR.InvoiceDetailsList = ObjOpenAR.OpenInvoiceDetailsList.Where(x => x.InvoiceShipTo == CommonNavInfo.Customer.CustomerNo).ToObservableCollection();
                    else
                        ObjOpenAR.InvoiceDetailsList = ObjOpenAR.OpenInvoiceDetailsList;
                }
                //var Creditinvoice = objOpenAR.OpenInvoiceDetailsList.Where(x => x.InvoicePaymentType == "Credit ").ToList();
                var ValueIsZero = ObjOpenAR.OpenInvoiceDetailsList.Sum(t => t.OpenAmountInvDtl);
                if (ValueIsZero == 0)
                {
                    objOpenAR.OpenInvoiceDetailsList.Where(x => x.InvoicePaymentType != "").ToList().ForEach(s => s.IsItemEnabled = false);
                }
                else
                {
                    objOpenAR.OpenInvoiceDetailsList.Where(x => x.InvoicePaymentType == "Credit ").ToList().ForEach(s => s.IsItemEnabled = false);
                    objOpenAR.OpenInvoiceDetailsList.Where(x => x.InvoicePaymentType == "CN").ToList().ForEach(s => s.IsItemEnabled = false);
                }


            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][GetInvoiceDetailsList][ExceptionStackTrace = " + ex.Message + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:GetInvoiceDetailsList]");
        }

        //public bool IsReceiptHistoryTabActive
        //{
        //    get
        //    {
        //        return this._IsReceiptHistoryTabActive;
        //    }
        //    set
        //    {
        //        this._IsReceiptHistoryTabActive = value;
        //        OnPropertyChanged("IsReceiptHistoryTabActive");

        //        if (!value)
        //        {
        //            this.EnableReceiptHistoryPrintButton = false;
        //            this.EnableReceiptHistoryUpdateButtons = false;
        //        }
        //    }
        //}

        /// <summary>
        /// Intialize the delegate commands for using the Edit and Void Dialogs 
        /// </summary>
        private void InitializeCommands()
        {
            #region Receipt History List Commands

            //It will be executed everytime the tabpage is clicked to refresh the screen. 
            //
            RefreshReceiptHistoryListDlgtCmd = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:RefreshReceiptHistoryListDlgtCmd]");

                try
                {
                    if (param != null && (param as System.Windows.Controls.HeaderedContentControl).Header.ToString() == "Receipt History")
                    {
                        this.GetLatestReceiptHistoryList();
                    }
                    if (param != null && (param as System.Windows.Controls.HeaderedContentControl).Header.ToString() == "Customer Ledger")
                    {
                        GetInvoiceDetailsList();
                        GetOpenAR();
                        ObjOpenAR.ShowAllInvoices = false;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][RefreshReceiptHistoryListDlgtCmd][ExceptionStackTrace = " + ex.Message + "]");
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:RefreshReceiptHistoryListDlgtCmd]");
            }
                );
            //Enabled the buttons 
            //Updates the Check / Cash Flags  
            //Updates variable to check for changes on edit screen
            ReceiptHistoryDlgtCmd = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:ReceiptHistoryDlgtCmd]");

                try
                {
                    if (param != null)
                    {
                        _VoidReasonCode = "";
                        //this.SelectedReceipt = (param as SalesLogicExpress.Domain.PaymentARModel.ReceiptHistory);
                        ((Telerik.Windows.Controls.GridView.GridViewDataControl)(((System.Windows.RoutedEventArgs)(param)).OriginalSource)).ScrollIntoView(this.SelectedReceipt);
                        this.EnableReceiptHistoryPrintButton = true;

                        if (param != null)
                        {
                            if (SelectedReceipt.Status == "SETTLED" || SelectedReceipt.Status == "VOID" || SelectedReceipt.Status == "VOIDED / SETTLED" || SelectedReceipt.IsCreditMemo || SelectedReceipt.ReasonCode == "return")
                                this.EnableReceiptHistoryUpdateButtons = false;
                            else
                                this.EnableReceiptHistoryUpdateButtons = true;
                        }

                        this.IsOriginalPayTypeCheck = this.SelectedReceipt.IsCheck;
                        this.IsOriginalPayTypeCash = this.SelectedReceipt.IsCash;
                        this.OriginalCheckNo = this.SelectedReceipt.CheckNo;


                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(PayloadManager.PaymentPayload.ReceiptId))
                        {
                            this.SelectedReceipt = this.ReceiptHistoryList.Where<PaymentARModel.ReceiptHistory>(o => o.ReceiptId == PayloadManager.PaymentPayload.ReceiptId).First();
                            PayloadManager.PaymentPayload.ReceiptId = "";
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][ReceiptHistoryDlgtCmd][ExceptionStackTrace = " + ex.Message + "]");
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:ReceiptHistoryDlgtCmd]");


            });

            #endregion

            #region Edit Receipt Commands

            //This will open the Edit Receipt pop-up screen 
            EditReceiptDlgtCmd = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:EditReceiptDlgtCmd]");
                try
                {
                    this.IsContinueEditReceiptButtonEnabled = false;

                    this.IsOriginalPayTypeCheck = this.SelectedReceipt.IsCheck;
                    this.IsOriginalPayTypeCash = this.SelectedReceipt.IsCash;
                    this.OriginalCheckNo = this.SelectedReceipt.CheckNo.Trim();

                    var dialog = new Helpers.DialogWindow { TemplateKey = "EditReceiptDialog", Title = "Edit Receipt", Payload = this };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, CommonNavInfo.MessageToken);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][EditReceiptDlgtCmd][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:EditReceiptDlgtCmd]");

            });

            //This will monitor the edit receipt screen for change. 
            //If any changes found, update button will be enable to complete the editing
            UpdateContinueButtonEnableStatusDlgtCmd = new DelegateCommand((param) =>
            {
                //User Issue Id : 45 When Iscash is true then continue button should be enable. If the order is in check and we change as cash then check no value should be clear and continue is enable.

                //if (((this.IsOriginalPayTypeCheck == this.SelectedReceipt.IsCheck) &&
                //    (this.IsOriginalPayTypeCash == this.SelectedReceipt.IsCash) &&
                //    ((this.OriginalCheckNo.Trim() == this.SelectedReceipt.CheckNo.Trim()))) ||
                //    (this.SelectedReceipt.IsCheck && this.SelectedReceipt.CheckNo.Trim().Length < 1))  
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:UpdateContinueButtonEnableStatusDlgtCmd]");
                try
                {

                    if (((this.IsOriginalPayTypeCheck == this.SelectedReceipt.IsCheck) &&
                    (this.IsOriginalPayTypeCash == this.SelectedReceipt.IsCash) &&
                    ((this.OriginalCheckNo.Trim() == this.SelectedReceipt.CheckNo.Trim()) && this.SelectedReceipt.CheckNo.Trim().Length < 1)) ||
                    (this.SelectedReceipt.IsCheck && this.SelectedReceipt.CheckNo.Trim().Length < 1))
                    {
                        this.IsContinueEditReceiptButtonEnabled = false;
                    }
                    else
                    {
                        //Below three lines added for issue id : 45
                        if (this.SelectedReceipt.IsCash)
                        {
                            this.SelectedReceipt.CheckNo = "";
                        }
                        this.IsContinueEditReceiptButtonEnabled = true;

                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][UpdateContinueButtonEnableStatusDlgtCmd][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:UpdateContinueButtonEnableStatusDlgtCmd]");

            });

            //This command will revert the chanages by the user if process not completed. 
            GridUnloadedDlgtCmd = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:GridUnloadedDlgtCmd]");
                try
                {

                    if (!IsContinueEditButtonClicked)
                    {
                        this.SelectedReceipt.CheckNo = OriginalCheckNo;
                        this.SelectedReceipt.IsCheck = IsOriginalPayTypeCheck;
                        this.SelectedReceipt.IsCash = IsOriginalPayTypeCash;
                    }

                    //Self assignment assigns the correct value to payment info
                    this.SelectedReceipt.IsCheck = this.SelectedReceipt.IsCheck;
                    this.SelectedReceipt.IsCash = this.SelectedReceipt.IsCash;
                    IsContinueEditButtonClicked = false;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][GridUnloadedDlgtCmd][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:GridUnloadedDlgtCmd]");
            });

            //This will complete the edit receipt process and commit changes in the database 
            ContinueEditReceiptDlgtCmd = new DelegateCommand(
                (param) =>
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:ContinueEditReceiptDlgtCmd]");

                    try
                    {
                        if (this.SelectedReceipt.IsCash)
                            this.SelectedReceipt.CheckNo = "";

                        objARPayManager.UpdateReceiptType(this.SelectedReceipt.IsCash, this.SelectedReceipt.ReceiptNo, this.SelectedReceipt.CheckNo, this.SelectedReceipt.ReceiptId);
                        this.SelectedReceipt = this.SelectedReceipt; //This will refresh the data and update the screen
                        this.UpdateActivity("edit");
                        this.LogReceiptChangeActivity();
                        IsContinueEditButtonClicked = true;
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][ContinueEditReceiptDlgtCmd][ExceptionStackTrace = " + ex.Message + "]");
                    }

                    Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:ContinueEditReceiptDlgtCmd]");
                });

            #endregion

            #region Void Receipt Commands

            //Command used in the Void Receipt flow
            VoidReceiptDlgtCmd = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:VoidReceiptDlgtCmd]" + (string.IsNullOrEmpty(param.ToString()) ? "" : "[Data = " + param.ToString() + "]"));

                try
                {
                    if (param.Equals("VoidReasonSelectionChanged")) //Void Reason combobox selection changed by the user on first Void Receipt screen to continue enable button 
                    {
                        if (!IsVoidReceiptApprovalCodeNeeded)
                            this.IsVoidReasonButtonEnabled = true;
                    }
                    else if (param.Equals("OpenVoidReceiptDialog")) //This block will reset al variables used in the Void Receipt and Open the First Void Receipt Screen
                    {
                        RequestAuthCodeViewModel.RequestAuthCodeParams codeParams = new RequestAuthCodeViewModel.RequestAuthCodeParams
                        {
                            Amount = SelectedReceipt.ReceiptAmount,
                            UserID = UserManager.UserId.ToString(),
                            Feature = AuthCodeManager.Feature.VoidReceipt
                        };
                        RequestAuthCodeVM = new RequestAuthCodeViewModel(codeParams);
                        RequestAuthCodeVM.AuthCodeChanged += RequestAuthCodeVM_AuthCodeChanged;
                        //Start process of getting approval code from web 
                        this.GetWebGeneratedApprovalCode();
                        this.VoidApprovalCode = "";
                        this.IsVoidReceiptApprovalCodeNeeded = false;
                        this.IsVoidReasonButtonEnabled = false;

                        //Get the unapplied amount for customer, if cutomer type is cash and un-applied amount 
                        decimal unappliedAmount = objARPayManager.GetUnAppliedAmtForCustomer();

                        // DV User Issue ID: 11 - CSH Customer Void Payment
                        // Call Method - for Get Previous Balance
                        CollectionManager objColmgr = new CollectionManager();
                        CashDelivery cashDelivery = new CashDelivery();
                        cashDelivery = objColmgr.GetCashDeliveryInfo(CommonNavInfo.Customer.CustomerNo, Order.OrderId.ToString(), false);
                        decimal PrvBalance = Convert.ToDecimal(cashDelivery.PreviousBalanceAmount.ToString());

                        // Commented On - 09/20/2016 - Unapplied Checked instead of Previous Balance
                        // if (CommonNavInfo.Customer.PaymentModeForList.Equals("CSH") && unappliedAmount != 0)
                        if (CommonNavInfo.Customer.PaymentModeForList.Equals("CSH") && PrvBalance != 0)
                        {
                            this.IsVoidReceiptApprovalCodeNeeded = true;
                        }
                        else
                        {
                            RequestAuthCodeVM.IsAuthCodeValid = true;
                            RequestAuthCodeVM.RequestCode = string.Empty;
                        }

                        var dialog = new Helpers.DialogWindow { TemplateKey = "VoidReceiptDialog", Title = "Void Receipt", Payload = this };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialog, CommonNavInfo.MessageToken);
                    }
                    else if (param.Equals("UpdateReceiptVoidDetail"))// && IsUpdateVoidReasonCodeInModelRequired) //This will open the alert message for conitnuing the Voiding process
                    {

                        objARPayManager.VoidReceipt(this.SelectedReceipt.ReceiptNo, this.VoidReasonCode, CommonNavInfo.RouteID.ToString(), true);
                        this.UpdateActivity("PaymentVoid");
                        if (RequestAuthCodeVM.IsAuthCodeValid)
                        {
                            Domain.PaymentARModel.ReceiptHistory rh = new Domain.PaymentARModel.ReceiptHistory();
                            rh.ReceiptId = this.SelectedReceipt.ReceiptId;
                            rh.ReceiptNo = this.SelectedReceipt.ReceiptNo;
                            rh.ReceiptType = this.SelectedReceipt.ReceiptType;
                            rh.ReasonCode = this.SelectedReceipt.ReasonCode;
                            RequestAuthCodeVM.LogAuthCode(PayloadManager.ApplicationPayload.LoggedInUserID, RequestAuthCodeVM.RequestCode, RequestAuthCodeVM.Authcode, CommonNavInfo.RouteID.ToString(), rh.SerializeToJson(), CommonNavInfo.Customer.CustomerNo.ToString(), SelectedReceipt.ReceiptAmount.ToString(), RequestAuthCodeVM.Params.Feature.ToString());
                        }

                        #region Commented code - log activity
                        /*
                        //Log Activity
                        PayloadManager.ChangePayload.CustomerId = CommonNavInfo.Customer.CustomerNo.ToString();
                        PayloadManager.ChangePayload.RouteId = CommonNavInfo.RouteID.ToString();
                        PayloadManager.ChangePayload.DocumentType = "Receipt";
                        PayloadManager.ChangePayload.DocumentNumber = this.SelectedReceipt.ReceiptNo;

                        PayloadManager.ChangePayload.UpdatedProperties.Clear();
                        if (this.SelectedReceipt.IsCash)
                        {
                            PayloadManager.ChangePayload.UpdatedProperties.Add("PaymentMode", "Cash");
                        }
                        else
                        {
                            PayloadManager.ChangePayload.UpdatedProperties.Add("PaymentMode", "Check");
                            PayloadManager.ChangePayload.UpdatedProperties.Add("CheckNo", this.SelectedReceipt.CheckNo);
                        }

                        SalesLogicExpress.Domain.Activity objChangeActivity = new SalesLogicExpress.Domain.Activity();
                        objChangeActivity.ActivityHeaderID = PayloadManager.ChangePayload.TransactionID;
                        objChangeActivity.CustomerID = PayloadManager.ChangePayload.CustomerId;
                        objChangeActivity.RouteID = PayloadManager.PaymentPayload.RouteID;
                        objChangeActivity.StopInstanceID = CommonNavInfo.Customer.StopID;
                        objChangeActivity.ActivityStart = DateTime.Now;
                        objChangeActivity.ActivityEnd = DateTime.Now;
                        objChangeActivity.IsTxActivity = true;
                        objChangeActivity.ActivityType = ActivityKey.Change.ToString();
                        objChangeActivity.ActivityDetailClass = PayloadManager.ChangePayload.ToString();
                        objChangeActivity.ActivityDetails = PayloadManager.ChangePayload.SerializeToJson();

                        objChangeActivity.ActivityID = ResourceManager.Transaction.LogActivity(objChangeActivity).ActivityID;
                        */
                        #endregion

                        this.GetLatestReceiptHistoryList();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][VoidReceiptDlgtCmd][ExceptionStackTrace = " + ex.Message + "]");
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:VoidReceiptDlgtCmd]");
            });

            #endregion

            #region Print Receipt
            PrintReceiptDlgtCmd = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:PrintReceiptDlgtCmd]");

                try
                {
                    PaymentARModel.OpenAR tempOpenAR = new PaymentARModel.OpenAR();

                    tempOpenAR.SelectedItems = SelectedReceipt.PaidInvoices.Where(s => !s.InvoiceNoInvDtl.Contains("Unapplied")).ToObservableCollection();
                    tempOpenAR.CheckDate = SelectedReceipt.CheckDate;
                    tempOpenAR.CheckNo = SelectedReceipt.CheckNo;
                    tempOpenAR.PaymentInfo = SelectedReceipt.PaymentInfo.Contains("Check") ? "Check(" + SelectedReceipt.CheckNo + ")" : "Cash";
                    tempOpenAR.PaymentAmount = SelectedReceipt.ReceiptAmount.ToString("F2");
                    tempOpenAR.ReceiptId = Convert.ToInt32(SelectedReceipt.ReceiptId);
                    tempOpenAR.DocumentId = SelectedReceipt.DocumentId;
                    if (SelectedReceipt.PaidInvoices.Any(s => s.InvoiceNoInvDtl.Contains("Unapplied")))
                    {
                        tempOpenAR.UnapplidAmount = SelectedReceipt.PaidInvoices.Where(s => s.InvoiceNoInvDtl.Contains("Unapplied")).Sum(s => s.OpenAmountInvDtl);
                    }
                    else
                        tempOpenAR.UnapplidAmount = 0;
                    PrintReceipt(tempOpenAR);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][PrintReceiptDlgtCmd][ExceptionStackTrace = " + ex.Message + "]");
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:PrintReceiptDlgtCmd]");

            });

            ObjOpenAR.PrintReceipt = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:ObjOpenAR.PrintReceipt]");

                try
                {
                    if (ObjOpenAR.UnapplidAmount > 0 && ObjOpenAR.SelectedItems.Count > 0)
                    {
                        var confirmWindow = new ConfirmWindow { OkButtonContent = "Yes", CancelButtonContent = "No", Message = "Entered amount greater than invoice amount." + Environment.NewLine + " Do you want to make extra amount as Unapplied?", Header = "Warning", MessageIcon = "Alert", Confirmed = false };
                        Messenger.Default.Send<ConfirmWindow>(confirmWindow, MessageToken);
                        if (!confirmWindow.Confirmed)
                        {
                            return;
                        }
                    }

                    ObjOpenAR.ReceiptId = SaveAndLogReceipt();

                    if (ObjOpenAR.ReceiptId != 0)
                    {

                        ObjOpenAR.PaymentInfo = ObjOpenAR.IsChecked ? "Check(" + ObjOpenAR.CheckNo + ")" : "Cash";

                        PrintReceipt(ObjOpenAR);


                        //Reset Window
                        ObjOpenAR.SelectedItems.Clear();
                        GetInvoiceDetailsList();
                        GetOpenAR();

                        #region Update Activity Count
                        string tempStopDate = string.Empty;
                        string stopID = string.Empty;
                        if (PayloadManager.PaymentPayload.Customer.StopDate.Value.Date > DateTime.Now.Date)
                        {
                            tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                            stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.PaymentPayload.Customer.CustomerNo, tempStopDate);
                        }
                        else
                        {
                            tempStopDate = PayloadManager.PaymentPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                            stopID = PayloadManager.PaymentPayload.Customer.StopID;
                        }
                        new Managers.CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                        new Managers.CustomerManager().UpdateActivityCount(stopID, false, true);
                        PayloadManager.PaymentPayload.Customer.SaleStatus = "";
                        PayloadManager.PaymentPayload.Customer.CompletedActivity += 1;
                        new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.PaymentPayload.Customer.CustomerNo);
                        #endregion
                        IsDirty = false;

                        if (!IsUnplannedCreatedForFuture)
                        {
                            #region Update tem squncing no.
                            DateTime todayDate = Convert.ToDateTime(tempStopDate);
                            ViewModels.CustomerDashboard custDash = new CustomerDashboard();
                            // Checks for customer alteration if user invoked past activity.
                            if (PayloadManager.PaymentPayload.IsCustomerLoadedForPastActivity)
                            {
                                PayloadManager.PaymentPayload.IsCustomerLoadedForPastActivity = false;
                                PayloadManager.OrderPayload.Customer = new CustomerDashboard().GetNewCustomer(PayloadManager.PaymentPayload.StopDateBeforeInvokingActivity.Value.Date, PayloadManager.PaymentPayload.Customer);
                            }
                            //else
                            //    PayloadManager.OrderPayload.Customer = custDash.GetNewCustomer(todayDate, PayloadManager.PreOrderPayload.Customer);

                            #endregion
                        }
                        ObjOpenAR.IsChecked = false;
                        ObjOpenAR.ShowAllInvoices = false; //Uncheck the select all flag 

                        objOpenAR.PaymentAmount = "";//Reset the payment Amount
                    }
                    else
                        Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, please sync to continue" }, MessageToken);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][ObjOpenAR.PrintReceipt][ExceptionStackTrace = " + ex.Message + "]");
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:ObjOpenAR.PrintReceipt]");


            });

            #endregion

        }

        void RequestAuthCodeVM_AuthCodeChanged(object sender, RequestAuthCodeViewModel.AuthCodeChangedEventArgs e)
        {
            this.IsVoidReasonButtonEnabled = e.IsValid;
        }

        private void PrintReceipt(PaymentARModel.OpenAR openAR)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:PrintReceipt]");

            try
            {
                var confirmPrintWindow = new ConfirmWindow { OkButtonContent = "Yes", CancelButtonContent = "No", Message = "Do you want to print this transaction?", Header = "Print", MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<ConfirmWindow>(confirmPrintWindow, MessageToken);
                if (confirmPrintWindow.Confirmed)
                {
                    //Print Receipt
                    //new ReportViewModels.ReceiptReport(openAR);
                    ShowReport(openAR);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][PrintReceipt][ExceptionStackTrace = " + ex.Message + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:PrintReceipt]");


        }

        private void ShowReport(PaymentARModel.OpenAR openAR)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:ShowReport]");
            try
            {
                var DocInfo = new ReportEngine.DocumentInformation();
                var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                repEngine.Notify += repEngine_Notify;

                repEngine.Log = this.Logger;
                ReportEngine.IReportSLE ReportData = null;
                var DocumentId = openAR.DocumentId;
                var SourceDocumentNo = Convert.ToDecimal(openAR.ReceiptId);
                //Do not need to check the condition for documentID as cash will be changed to cheque and vice versa - Fixed by Zakir
                //Issue ID - 11- Updated the Payment type from Cash to Check in Receipt History tab.Then tried to print the receipt but still its showing the Payment type as cash in Receipt Print
                ReportData = new ReportViewModels.ReceiptReport(openAR);
                Dictionary<string, object> Parameters;
                Parameters = new Dictionary<string, object>();
                Parameters.Add("PrintLabels", Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PrintLabels"]));
                DocInfo = repEngine.Generate<Reporting.Receipt>(SourceDocumentNo, true, ReportEngine.ReportCategory.Customer, ReportEngine.ReportsTypes.ReceiptReport, ReportData, Parameters, ReportEngine.ContentTypes.PDF);

                if (DocInfo.IsEndOfSequence)
                {
                    Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, please sync to continue" }, MessageToken);
                    return;
                }

                if (DocInfo.IsDocumentCreated)
                    ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.Receipt_Header", "ReceiptId", SourceDocumentNo, DocInfo.DocumentId);
                openAR.DocumentId = DocInfo.DocumentId; //Replace Latest

                var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Customer, openAR.DocumentId, null);

                if (ReportBytes == null)
                {
                    Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Report not generated, Please try again." }, MessageToken);
                    return;
                }

                SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
                Payload.ReportFile = ReportBytes;
                Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                Payload.PreviousView = ViewModelMappings.View.ARPayment;
                Payload.CurrentViewTitle = "Receipt Report";
                Payload.PreviousViewTitle = "Payment & AR";
                Payload.MessageToken = this.MessageToken;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.ARPayment, CloseCurrentView = true, Payload = Payload, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][ShowReport][ExceptionStackTrace = " + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:ShowReport]");
        }

        void repEngine_Notify(string msg)
        {
            System.Windows.MessageBox.Show(msg);
        }

        private int SaveAndLogReceipt()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:SaveAndLogReceipt]");
            //Receipt Header Insert
            int NewReceiptId = 0;
            try
            {
                Managers.CollectionManager collectionManager = new Managers.CollectionManager();
                CashDelivery cd = new CashDelivery();
                cd.ChequeDate = Convert.ToDateTime(ObjOpenAR.CheckDate, System.Globalization.CultureInfo.InvariantCulture);
                cd.ChequeNo = ObjOpenAR.CheckNo;
                cd.PaymentAmount = ObjOpenAR.PaymentAmount;
                cd.PaymentMode = ObjOpenAR.IsChecked;
                String CustBillTo = CustomerManager.GetCustomerBillTo(CommonNavInfo.Customer.CustomerNo);
                string chequeNo = string.IsNullOrEmpty(cd.ChequeNo) ? "" : cd.ChequeNo;
                string chequeDate = cd.ChequeDate.ToString("yyyy-MM-dd");
                List<string> invoiceNos = new List<string>();

                CheckEntryDetails o = new CheckEntryDetails();
                o.CheckNo = cd.ChequeNo;
                o.CheckAmount = cd.PaymentAmount;
                o.CheckDate = cd.ChequeDate.ToString("yyyy-MM-dd");
                o.CreatedBy = Managers.UserManager.UserId.ToString();
                o.RoutId = CommonNavInfo.RouteID.ToString();
                o.CustomerId = CommonNavInfo.Customer.CustomerNo;
                o.CustomerName = CommonNavInfo.Customer.Name;
                string checkID = SettlementConfirmationManager.InsertCheckDetails(o);
                cd.ChequeNoId = checkID;

                NewReceiptId = collectionManager.InsertReceiptHeader(cd, CommonNavInfo.Customer.CustomerNo, CustBillTo, CommonNavInfo.RouteID.ToString(), chequeNo, chequeDate);

                //Receipt entry in Customer ledger
                //collectionManager.InsertReceiptInCustomerLedger(CommonNavInfo.RouteID.ToString(), NewReceiptId, CommonNavInfo.Customer.CustomerNo, CustBillTo, Convert.ToDecimal(ObjOpenAR.PaymentAmount));

                //Payment Equals Or Greater than Invoice total

                if (NewReceiptId != 0)
                {
                    if (Convert.ToDecimal(ObjOpenAR.SelectedItems.Sum(s => s.OpenAmountInvDtl)) <= Convert.ToDecimal(ObjOpenAR.PaymentAmount))
                    {
                        //If Unapplied Amount
                        if (objOpenAR != null && objOpenAR.SelectedItems != null && objOpenAR.SelectedItems.Count() == 0)
                        {
                            int DetailLineNumber = 1;
                            collectionManager.InsertintoReceiptLedger(
                                NewReceiptId.ToString(), DetailLineNumber.ToString(), CommonNavInfo.RouteID.ToString(),
                                CommonNavInfo.RouteID.ToString(), CustBillTo, chequeNo, chequeDate,
                                cd.PaymentMode.ToString(), DateTime.Now.ToString("yyyy-MM-dd"), cd.PaymentAmount,
                                "0", "0", "0", "RU", "Mobile Receipt", "",
                                "", "", "-" + cd.PaymentAmount, CustBillTo,
                                CommonNavInfo.Customer.CustomerNo, "Mobile Receipt", "", "", "4", ""
                                );
                            DetailLineNumber += 1;
                        }
                        else
                        {
                            int DetailLineNumber = 1;
                            decimal temptotal = 0;
                            //For each Invoice & Receipt Mapping entry In Customer ledger
                            foreach (PaymentARModel.InvoiceDetails item in ObjOpenAR.SelectedItems)
                            {
                                //collectionManager.InsertReceiptInvoiceMappingInCustomerLedger(
                                //    CommonNavInfo.RouteID.ToString(),
                                //    CommonNavInfo.Customer.CustomerNo,
                                //    CustBillTo,
                                //    item.InvoiceIdInvDtl.ToString(),
                                //    item.GrossAmountInvDtl,
                                //    NewReceiptId,
                                //    item.OpenAmountInvDtl
                                //    );
                                //invoiceNos.Add(item.InvoiceNoInvDtl);                            
                                collectionManager.InsertintoReceiptLedger(
                                    NewReceiptId.ToString(), DetailLineNumber.ToString(), CommonNavInfo.RouteID.ToString(),
                                    CommonNavInfo.RouteID.ToString(), CustBillTo, chequeNo, chequeDate,
                                    cd.PaymentMode.ToString(), DateTime.Now.ToString("yyyy-MM-dd"), cd.PaymentAmount,
                                    (Convert.ToDecimal(cd.PaymentAmount) - item.OpenAmountInvDtl).ToString(), "0", "0", "RC", "Mobile Receipt", item.InvoiceIdInvDtl.ToString(),
                                    "", "", "-" + item.OpenAmountInvDtl.ToString(), CustBillTo,
                                    CommonNavInfo.Customer.CustomerNo, "Mobile Receipt", "", "", "4", ""
                                    );
                                DetailLineNumber += 1;

                                temptotal += item.OpenAmountInvDtl;
                            }

                            decimal PaymentAmount = Convert.ToDecimal(string.IsNullOrEmpty(cd.PaymentAmount.Trim()) ? "0" : cd.PaymentAmount.Trim());
                            if (temptotal != PaymentAmount)
                            {
                                collectionManager.InsertintoReceiptLedger(
                                    NewReceiptId.ToString(), DetailLineNumber.ToString(), CommonNavInfo.RouteID.ToString(),
                                    CommonNavInfo.RouteID.ToString(), CustBillTo, chequeNo, chequeDate,
                                    cd.PaymentMode.ToString(), DateTime.Now.ToString("yyyy-MM-dd"), cd.PaymentAmount,
                                    "0", "0", "0", "RU", "Mobile Receipt", (PaymentAmount - temptotal).ToString(),
                                    "", "", "-" + (PaymentAmount - temptotal).ToString(), CustBillTo,
                                    CommonNavInfo.Customer.CustomerNo, "Mobile Receipt", "", "", "4", ""
                                    );
                            }
                        }

                    }
                    LogPaymentActivity(ActivityKey.Payment, cd, NewReceiptId.ToString(), invoiceNos);
                }
                else
                    Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, please sync to continue" }, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][SaveAndLogReceipt][ExceptionStackTrace = " + ex.Message + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:SaveAndLogReceipt]");
            return NewReceiptId;
        }

        #endregion

        #region Receipt History Methods /Properties

        /// <summary>
        /// Get or set receipt history list 
        /// </summary>
        public List<PaymentARModel.ReceiptHistory> ReceiptHistoryList
        {
            get
            {
                if (_ReceiptHistoryList == null)
                    this.GetLatestReceiptHistoryList();

                return _ReceiptHistoryList;
            }
            set
            {
                _ReceiptHistoryList = value;
                OnPropertyChanged("ReceiptHistoryList");
            }
        }

        /// <summary>
        /// Get or set value to enable/disbale Void and Edit button on Receipt History screen. 
        /// </summary>
        public bool EnableReceiptHistoryUpdateButtons
        {
            get { return _EnableReceiptHistoryUpdateButtons; }
            set
            {
                _EnableReceiptHistoryUpdateButtons = value;
                OnPropertyChanged("EnableReceiptHistoryUpdateButtons");
            }
        }

        /// <summary>
        /// Get or set value to enable/disbale Print button on Receipt History screen. 
        /// </summary>
        public bool EnableReceiptHistoryPrintButton
        {
            get { return _EnableReceiptHistoryPrintButton; }
            set
            {
                _EnableReceiptHistoryPrintButton = value;
                OnPropertyChanged("EnableReceiptHistoryPrintButton");
            }
        }

        /// <summary>
        /// Get or set to disable/enable continue button on Edit Receipt screen. 
        /// </summary>
        public bool IsContinueEditReceiptButtonEnabled
        {
            get
            { return _IsContinueEditReceiptButtonEnabled; }
            set
            {
                _IsContinueEditReceiptButtonEnabled = value;
                OnPropertyChanged("IsContinueEditReceiptButtonEnabled");
            }
        }

        /// <summary>
        /// Get or set to determine the final screen in Void Receipt flow
        /// </summary>
        public bool IsVoidReceiptApprovalCodeNeeded
        {
            get
            {
                return _IsVoidReceiptApprovalCodeNeeded;
            }
            set
            {
                _IsVoidReceiptApprovalCodeNeeded = value;
                OnPropertyChanged("IsVoidReceiptApprovalCodeNeeded");
            }
        }

        /// <summary>
        /// Get list of available void reason codes
        /// </summary>

        public ObservableCollection<string> VoidReasonList
        {
            get
            {
                _VoidReasonList = new ObservableCollection<string>(objARPayManager.GetVoidReasonList());
                return _VoidReasonList;
            }
        }

        /// <summary>
        /// Get or set when continue button on Void screen needs to Enabled/Disabled 
        /// </summary>
        public bool IsVoidReasonButtonEnabled
        {
            get
            {
                return _IsVoidReasonButtonEnabled;
            }
            set
            {
                _IsVoidReasonButtonEnabled = value;
                OnPropertyChanged("IsVoidReasonButtonEnabled");
            }
        }

        /// <summary>
        /// Get or set void approval code. 
        /// This will be a numeric value that user will enter/set. 
        /// This will also enable the continue button when correct value is entered. 
        /// </summary>
        public string VoidApprovalCode
        {
            get { return _VoidApprovalCode; }
            set
            {
                _VoidApprovalCode = value;
                OnPropertyChanged("VoidApprovalCode");

                if (string.IsNullOrEmpty(_VoidApprovalCode) || _VoidApprovalCode != this.WebGeneratedApprovalCode)
                    this.IsVoidReasonButtonEnabled = false;
                else
                    this.IsVoidReasonButtonEnabled = true;
            }
        }

        /// <summary>
        /// Get or set reason code for voiding receipt
        /// </summary>
        public string VoidReasonCode
        {
            get { return _VoidReasonCode; }
            set
            {
                _VoidReasonCode = value;
                OnPropertyChanged("VoidReasonCode");
            }
        }

        /// <summary>
        /// Retrieves latest receipt and refreshes the receipt history list 
        /// </summary>
        private void GetLatestReceiptHistoryList()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:GetLatestReceiptHistoryList]");

            try
            {
                if (_ReceiptHistoryList == null)
                    _ReceiptHistoryList = new List<PaymentARModel.ReceiptHistory>();

                this.ReceiptHistoryList = objARPayManager.GetReceiptHistory();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][GetLatestReceiptHistoryList][ExceptionStackTrace = " + ex.Message + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:GetLatestReceiptHistoryList]");
        }

        /// <summary>
        /// Generates codes for validating void reason code 
        /// </summary>
        async private void GetWebGeneratedApprovalCode()
        {
            this.WebGeneratedApprovalCode = await Task.FromResult<string>("12341234");

        }

        /// <summary>
        /// Updates the activity 
        /// </summary>
        /// <param name="receiptUpdateMode">Indicates the update mode for activity i.e. edit/void</param>
        private void UpdateActivity(string receiptUpdateMode)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:UpdateActivity]");
            Activity objActivity = null;
            try
            {
                CustomerActivityManager objCustomerActivityMgr = new CustomerActivityManager();
                ObservableCollection<Activity> objActivities = objCustomerActivityMgr.GetActivities(CommonNavInfo.Customer.CustomerNo);

                //This try catch used to handle object reference issues that comes arbitrarily
                try
                {
                    objActivity = (from activity in objActivities
                                   where activity.ActivityDetails.Contains("receipt#" + this.SelectedReceipt.ReceiptId)
                                   select activity).FirstOrDefault<Activity>();
                }
                catch (Exception) { }

                if (objActivity != null)
                {
                    if (receiptUpdateMode == "edit")
                    {
                        PaymentPayload objPaymentload = objActivity.ActivityDetails.DeserializeJson<PaymentPayload>();
                        objPaymentload.CashDelivery.ChequeDate = Convert.ToDateTime(this.SelectedReceipt.CheckDate, System.Globalization.CultureInfo.InvariantCulture);
                        objPaymentload.CashDelivery.ChequeNo = this.SelectedReceipt.CheckNo;
                        objPaymentload.CashDelivery.PaymentMode = this.SelectedReceipt.IsCheck;
                        objActivity.ActivityDetails = objPaymentload.SerializeToJson();

                        try
                        {
                            string query = "update busdta.CheckDetails set CheckNumber=" + this.SelectedReceipt.CheckNo + ", CheckDetailsNumber=" + this.SelectedReceipt.CheckNo + " where CheckDetailsId=" + objPaymentload.CashDelivery.ChequeNoId;
                            int result = DbEngine.ExecuteNonQuery(query);
                        }
                        catch { }
                    }
                    else if (receiptUpdateMode == "PaymentVoid")
                    {
                        objActivity.ActivityStatus = "PaymentVoid";
                    }

                    objARPayManager.UpdateReceiptActivity(objActivity);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][UpdateActivity][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:UpdateActivity]");
        }

        /// <summary>
        /// Logs a change acivity for the receipt record 
        /// </summary>
        private void LogReceiptChangeActivity()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][Start:LogReceiptChangeActivity]");

            try
            {
                //Log Activity
                PayloadManager.ChangePayload.CustomerId = CommonNavInfo.Customer.CustomerNo.ToString();
                PayloadManager.ChangePayload.RouteId = CommonNavInfo.RouteID.ToString();
                PayloadManager.ChangePayload.DocumentType = "Receipt";
                PayloadManager.ChangePayload.DocumentNumber = this.SelectedReceipt.ReceiptNo;

                PayloadManager.ChangePayload.UpdatedProperties.Clear();
                if (this.SelectedReceipt.IsCash)
                {
                    PayloadManager.ChangePayload.UpdatedProperties.Add("PaymentMode", "Cash");
                }
                else
                {
                    if (this.SelectedReceipt.IsCheck != IsOriginalPayTypeCheck)
                        PayloadManager.ChangePayload.UpdatedProperties.Add("PaymentMode", "Check");

                    if (this.SelectedReceipt.CheckNo != OriginalCheckNo)
                        PayloadManager.ChangePayload.UpdatedProperties.Add("CheckNo", this.SelectedReceipt.CheckNo);
                }

                SalesLogicExpress.Domain.Activity objChangeActivity = new SalesLogicExpress.Domain.Activity();
                objChangeActivity.ActivityHeaderID = PayloadManager.ChangePayload.TransactionID;
                objChangeActivity.CustomerID = PayloadManager.ChangePayload.CustomerId;
                objChangeActivity.RouteID = PayloadManager.PaymentPayload.RouteID;
                objChangeActivity.StopInstanceID = CommonNavInfo.Customer.StopID;
                objChangeActivity.ActivityStart = DateTime.Now;
                objChangeActivity.ActivityEnd = DateTime.Now;
                objChangeActivity.IsTxActivity = true;
                objChangeActivity.ActivityType = ActivityKey.Change.ToString();
                objChangeActivity.ActivityDetailClass = PayloadManager.ChangePayload.ToString();
                objChangeActivity.ActivityDetails = PayloadManager.ChangePayload.SerializeToJson();

                objChangeActivity.ActivityID = ResourceManager.Transaction.LogActivity(objChangeActivity).ActivityID;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][LogReceiptChangeActivity][ExceptionStackTrace = " + ex.Message + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ARPaymentViewModel][End:LogReceiptChangeActivity]");
        }

        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}

