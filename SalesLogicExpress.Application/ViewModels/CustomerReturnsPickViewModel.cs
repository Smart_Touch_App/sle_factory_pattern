﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Threading;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class CustomerReturnsPickViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CustomerReturnsPickViewModel");

        CustomerReturnsManager CR_Manager = new CustomerReturnsManager();

        #region Command's
        public DelegateCommand NavigateToAcceptAndPrint { get; set; }
        public DelegateCommand ScanByDeviceCommand { get; set; }
        public DelegateCommand ScanByDeviceExceptionCommand { get; set; }
        public DelegateCommand StartManualPick { get; set; }
        public DelegateCommand SelectionChanged { get; set; }
        public DelegateCommand UpdateOnManualCountCommand { get; set; }
        public DelegateCommand ManualPickCommand { get; set; }

        public DelegateCommand ManualPickContinueCommand { get; set; }
        public DelegateCommand RemoveExceptions { get; set; }

        public DelegateCommand VoidPickList { get; set; }
        public DelegateCommand HoldPickList { get; set; }

        public DelegateCommand SwitchScannerMode { get; set; }

        #endregion
        #region Prop's
        int pickcounter = 0;
        public Guid MessageToken { get; set; }
        private List<ReturnItem> ItemReturnList = new List<ReturnItem>();


        private int _OldExceptionCount;

        public int OldExceptionCount
        {
            get { return _OldExceptionCount; }
            set { _OldExceptionCount = value; }
        }

        private int _NewExceptionCount;

        public int NewExceptionCount
        {
            get { return _NewExceptionCount; }
            set { _NewExceptionCount = value; }
        }

        private double _InventoryExecuteVal;

        public double InventoryExecuteVal
        {
            get { return _InventoryExecuteVal; }
            set { _InventoryExecuteVal = value; }
        }

        private double _pickNumericOldVal = 0;

        public double PickNumericOldVal
        {
            get { return _pickNumericOldVal; }
            set { _pickNumericOldVal = value; }
        }

        private string _chkSamePickItem = "";

        public string ChkSamePickItem
        {
            get { return _chkSamePickItem; }
            set { _chkSamePickItem = value; }
        }

        private double _chkSamePickOrderId = 0;

        public double ChkSamePickOrderId
        {
            get { return _chkSamePickOrderId; }
            set { _chkSamePickOrderId = value; }
        }


        private double _pickNumericCurrVal;

        public double PickNumericCurrVal
        {
            get { return _pickNumericCurrVal; }
            set { _pickNumericCurrVal = value; }
        }
        private char _pickNumericSign;

        public char PickNumericSign
        {
            get { return _pickNumericSign; }
            set { _pickNumericSign = value; }
        }


        private bool isVoidStarted = false;

        public bool IsVoidStarted
        {
            get { return isVoidStarted; }
            set
            {
                isVoidStarted = value;
                //if (value)
                //{
                //    IsEnableRemoveException = true;
                //}
            }
        }
        bool isExceptionGridEnabled = false;
        public bool IsExceptionGridEnabled
        {
            get
            {
                return isExceptionGridEnabled;
            }
            set
            {


                isExceptionGridEnabled = value;
                OnPropertyChanged("IsExceptionGridEnabled");
            }
        }

        bool isManualCountStarted;
        public bool IsManualCountStarted
        {
            get
            {
                return isManualCountStarted;
            }
            set
            {


                isManualCountStarted = value;
                OnPropertyChanged("IsManualCountStarted");
            }
        }


        TrulyObservableCollection<ReturnItem> returnItems = new TrulyObservableCollection<ReturnItem>();

        public TrulyObservableCollection<ReturnItem> ReturnItems
        {
            get { return returnItems; }
            set { returnItems = value; OnPropertyChanged("ReturnItems"); }
        }

        private ObservableCollection<ReasonCode> manualPickReasonList = null;
        public ObservableCollection<ReasonCode> ManualPickReasonList
        {
            get
            {
                return manualPickReasonList;
            }
            set
            {
                manualPickReasonList = value;
                OnPropertyChanged("ManualPickReasonList");
            }
        }

        string _ReturnOrderNumber;
        public string ReturnOrderNumber
        {
            get
            {
                return _ReturnOrderNumber;
            }
            set
            {
                _ReturnOrderNumber = value;
                OnPropertyChanged("ReturnOrderNumber");
            }
        }
        bool isScannerOn = false;
        public bool IsScannerOn
        {
            get
            {
                return isScannerOn;
            }
            set
            {
                isScannerOn = value;
                OnPropertyChanged("IsScannerOn");
            }
        }

        ReturnItem selectedReturnItem = new ReturnItem();

        public ReturnItem SelectedReturnItem
        {
            get { return selectedReturnItem; }
            set
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:SelectedReturnItem]");
                try
                {
                    selectedReturnItem = value; OnPropertyChanged("SelectedReturnItem");
                    if (SelectedReturnItem != null)
                    {
                        IsTextBoxFocus = true;
                        SelectedExceptionItem = null;
                        IsEnableManualPick = true;
                        SelectedItemNumber = SelectedReturnItem.ItemNumber;
                        SelectedPrimaryUoM = SelectedReturnItem.PrimaryUM;
                        ManuallyPickCountOfSelected = SelectedReturnItem.ReturnQty;
                        IsEnableRemoveException = false;
                        SelectedReturnItem.IsAllowManualPick = false;
                        foreach (ReturnItem item in PickItems)
                        {
                            item.IsAllowManualPick = false;

                        }
                        foreach (ReturnItem item in PickItemsException)
                        {
                            item.IsAllowManualPick = false;
                            //item.IsEnableScannerInException = false;
                            if (IsChkExceptionScannerPickEnable)
                                item.IsEnableScannerInException = IsChkExceptionScannerPickEnable;
                            else
                                item.IsEnableScannerInException = IsChkExceptionScannerPickEnable;
                        }
                        //EnableDisableScannerForSelectedItem();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][SelectedReturnItem][ExceptionStackTrace = " + ex.ToString() + "]");
                }
                Logger.Info("SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:SelectedReturnItem]");
            }
        }

        private ReturnItem selectedExceptionItem = new ReturnItem();

        public ReturnItem SelectedExceptionItem
        {
            get { return selectedExceptionItem; }
            set
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:SelectedExceptionItem]");
                try
                {
                    selectedExceptionItem = value; OnPropertyChanged("SelectedExceptionItem");
                    if (SelectedExceptionItem != null)
                    {
                        IsTextBoxFocus = true;
                        SelectedReturnItem = null;
                        SelectedItemNumber = SelectedExceptionItem.ItemNumber;
                        SelectedPrimaryUoM = SelectedExceptionItem.PrimaryUM;
                        ManuallyPickCountOfSelected = Convert.ToInt32(SelectedExceptionItem.ExceptionCount);
                        if (SelectedExceptionItem.IsEnableScannerInException)
                        {
                            IsEnableManualPick = true;
                            SelectedExceptionItem.IsAllowManualPick = false;
                            IsEnableRemoveException = false;
                        }
                        else
                        {
                            foreach (ReturnItem item in PickItemsException)
                            {
                                item.IsAllowManualPick = false;
                                item.IsEnableScannerInException = false;
                            }
                            IsEnableManualPick = false;
                            IsEnableRemoveException = true;
                        }
                    }
                    else
                    {
                        IsEnableRemoveException = false;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][SelectedExceptionItem][ExceptionStackTrace = " + ex.ToString() + "]");
                }
                Logger.Info("SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:SelectedExceptionItem]");
            }
        }


        string selectedPrimaryUoM;
        public string SelectedPrimaryUoM
        {
            get
            {
                return selectedPrimaryUoM;
            }
            set
            {


                selectedPrimaryUoM = value;
                OnPropertyChanged("SelectedPrimaryUoM");
            }
        }


        string selectedItemNumber;
        public string SelectedItemNumber
        {
            get
            {
                return selectedItemNumber;
            }
            set
            {
                selectedItemNumber = value;
                OnPropertyChanged("SelectedItemNumber");
            }
        }


        bool isAcceptAndPrintEnabled;
        public bool IsAcceptAndPrintEnabled
        {
            get
            {
                return isAcceptAndPrintEnabled;
            }
            set
            {


                isAcceptAndPrintEnabled = value;
                OnPropertyChanged("IsAcceptAndPrintEnabled");
            }
        }


        bool isEnableManualPick = false;
        public bool IsEnableManualPick
        {
            get
            {
                return isEnableManualPick;
            }
            set
            {


                isEnableManualPick = value;
                OnPropertyChanged("IsEnableManualPick");
            }
        }


        bool isEnableRemoveException = false;
        public bool IsEnableRemoveException
        {
            get
            {
                return isEnableRemoveException;
            }
            set
            {
                //If exception list not null, then check item list count and update the value
                if (PickItemsException != null)
                {
                    isEnableRemoveException = (this.PickItemsException.Count > 0 ? true : value);
                }
                else
                {
                    IsEnableRemoveException = value;
                }
                OnPropertyChanged("IsEnableRemoveException");
            }
        }

        int manuallyPickCountOfSelected = 0;
        public int ManuallyPickCountOfSelected
        {
            get
            {
                return manuallyPickCountOfSelected;
            }
            set
            {
                manuallyPickCountOfSelected = value;
                OnPropertyChanged("ManuallyPickCountOfSelected");
            }
        }


        #region PickManager Prop's
        private PickItemManager pickMgr = null;
        ObservableCollection<dynamic> _PickItems = new ObservableCollection<dynamic>();
        public ObservableCollection<dynamic> PickItems
        {
            get { return _PickItems; }
            set
            {
                _PickItems = value;
                OnPropertyChanged("PickItems");
            }
        }
        ObservableCollection<dynamic> pickItemsException = new ObservableCollection<dynamic>();
        public ObservableCollection<dynamic> PickItemsException
        {
            get { return pickItemsException; }
            set
            {
                pickItemsException = value;
                OnPropertyChanged("PickItemsException");
            }
        }



        private void PickItemsException_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.IsEnableRemoveException = ((((System.Collections.ObjectModel.ObservableCollection<object>)(sender))).Count > 0);
        }
        List<dynamic> items = null;

        #endregion

        ReasonCodeVM _ReasonCodes = new ReasonCodeVM();
        public ReasonCodeVM ReasonCodes
        {
            get
            {
                return _ReasonCodes;
            }
            set
            {
                _ReasonCodes = value;
                OnPropertyChanged("ReasonCodes");
            }
        }

        bool _IsInVoidState = false;
        public bool IsInVoidState
        {
            get
            {
                return _IsInVoidState;
            }
            set
            {


                _IsInVoidState = value;
                OnPropertyChanged("IsInVoidState");
            }
        }

        private bool isTextBoxFocus = true;
        public bool IsTextBoxFocus
        {
            get { return isTextBoxFocus; }
            set { isTextBoxFocus = value; OnPropertyChanged("IsTextBoxFocus"); }
        }

        private bool isChkExceptionScannerPickEnable = false;

        public bool IsChkExceptionScannerPickEnable
        {
            get { return isChkExceptionScannerPickEnable; }
            set { isChkExceptionScannerPickEnable = value; OnPropertyChanged("IsChkExceptionScannerPickEnable"); }
        }


        #endregion
        public CustomerReturnsPickViewModel()
        {
            //IsBusy = true;
            // Checks for customer alteration if user invoked past activity.

            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:CustomerReturnsPickViewModel]");
            try
            {
                if (PayloadManager.ReturnOrderPayload.IsCustomerLoadedForPastActivity)
                {
                    PayloadManager.ReturnOrderPayload.IsCustomerLoadedForPastActivity = false;
                    PayloadManager.ReturnOrderPayload.Customer = new CustomerDashboard().GetNewCustomer(PayloadManager.ReturnOrderPayload.StopDateBeforeInvokingActivity.Value.Date, PayloadManager.ReturnOrderPayload.Customer);
                }
                CommonNavInfo.IsNavigateFromReturnPickScr = false;
                InitializeCommands();
                SetScannerModeByProfile();
                ManualPickReasonList = new PickManager().GetReasonListForManualPick();
                UnHoldOrder();
                ReturnItems = ViewModelPayload.PayloadManager.ReturnOrderPayload.ReturnItems;
                //ReturnItems = new CustomerReturnsManager().GetItemsForPick();
                ReturnOrderNumber = ViewModelPayload.PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString();
                PayloadManager.ReturnOrderPayload.ROIsInProgress = true;
                PayloadManager.ReturnOrderPayload.PickIsInProgress = true;

                LoadPickList();
                EnableDisableIsAcceptAndPrint();
                //IsBusy = false;
                PickItemsException.CollectionChanged += PickItemsException_CollectionChanged;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][CustomerReturnsPickViewModel][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:CustomerReturnsPickViewModel]");

        }

        void UnHoldOrder()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:UnHoldOrder]");
            try
            {
                if (PayloadManager.ReturnOrderPayload.IsOnHold)
                {
                    PayloadManager.ApplicationPayload.StartActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.ReturnOrder);
                    #region Update Activity Count
                    string tempStopDate = string.Empty;
                    string stopID = string.Empty;
                    if (PayloadManager.ReturnOrderPayload.StopDate.Date > DateTime.Now.Date)
                    {
                        tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                        stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.ReturnOrderPayload.Customer.CustomerNo, tempStopDate);
                    }
                    else
                    {
                        tempStopDate = PayloadManager.ReturnOrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                        stopID = PayloadManager.ReturnOrderPayload.Customer.StopID;
                    }
                    new CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                    new CustomerManager().UpdateActivityCount(stopID, true, false);
                    PayloadManager.ReturnOrderPayload.Customer.SaleStatus = "";
                    PayloadManager.ReturnOrderPayload.Customer.PendingActivity = new Managers.CustomerManager().GetPendingAtivityForStop(stopID);
                    new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.ReturnOrderPayload.Customer.CustomerNo);
                    #endregion
                    PayloadManager.ReturnOrderPayload.IsOnHold = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][UnHoldOrder][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:UnHoldOrder]");

        }

        #region Routines
        public void InitializeCommands()
        {
            SwitchScannerMode = new DelegateCommand((param) =>
            {
                bool InvertMode = !IsScannerOn;
                IsScannerOn = InvertMode;
            });

            NavigateToAcceptAndPrint = new DelegateCommand(param =>
            {
                ViewModelPayload.PayloadManager.ReturnOrderPayload.ReturnItems = ReturnItems;
                PayloadManager.ReturnOrderPayload.NavigateToAcceptAndPrint = true;

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AcceptAndPrintReturns, CurrentViewName = ViewModelMappings.View.ReturnOrderPickScreen, CloseCurrentView = true, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
            });

            ScanByDeviceCommand = new DelegateCommand(param =>
            {
                if (param == null) return;
                ReturnItem pickedROItem = param as ReturnItem;
                ScanByDeviceExecute(pickedROItem);

            });
            ScanByDeviceExceptionCommand = new DelegateCommand(param =>
            {
                if (param == null) return;


                ReturnItem pickedROItem = param as ReturnItem;
                ScanByDeviceExceptionExecute(pickedROItem);

            });
            ManualPickCommand = new DelegateCommand(param =>
            {
                //if (param == null)
                //    return;
                ManualPickExecute();
                //var pickManager = new PickManager();
                //manualPickReasonList = pickManager.GetReasonListForManualPick();

                //var dialog = new Helpers.DialogWindow { TemplateKey = "ReasonCodeDialog", Title = "Reason Code" };
                //Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);

            });
            SelectionChanged = new DelegateCommand(param =>
            {
                pickcounter = 0;
                if (param == null)
                    return;
                SelectedReturnItem = param as ReturnItem;
                SelectedItemNumber = SelectedReturnItem.ItemNumber;
                SelectedPrimaryUoM = SelectedReturnItem.PrimaryUM;
                ManuallyPickCountOfSelected = SelectedReturnItem.ReturnQty;

            });
            UpdateOnManualCountCommand = new DelegateCommand(param =>
            {
                if (param == null)
                    return;


                UpdateOnManualCountExecute();
              
            });
            StartManualPick = new DelegateCommand(param =>
            {
                SelectedReturnItem.ReasonCodeId = Convert.ToInt32(param);
                IsManualCountStarted = true;

            });
            RemoveExceptions = new DelegateCommand(param =>
            {
                var confirmMessage = new Helpers.ConfirmWindow { Message = Constants.PickOrder.RemoveExceptionConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, CommonNavInfo.MessageToken);
                if (!confirmMessage.Confirmed) return;

                IsExceptionGridEnabled = true;

                foreach (ReturnItem item in PickItemsException)
                {
                    item.IsEnableScannerInException = true;
                    //if (IsScannerOn)
                    //{
                    //    item.IsEnableScannerInExceptionIcon = true;
                    //}
                    //else
                    //{
                    //    item.IsEnableScannerInExceptionIcon = false;
                    //}
                }

                //SelectedExceptionItem.IsEnableScannerInException = true;
                SelectedReturnItem = null;
                IsEnableManualPick = true;
                IsEnableRemoveException = false;
            });

            VoidPickList = new DelegateCommand(param =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:VoidPickList]");
                try
                {

                    ReasonCodes.SelectedReasonCode = null;
                    var ConfirmWindowDialog = new Helpers.ConfirmWindow { TemplateKey = "ReasonCodeListDialog", Context = ReasonCodes, Header = "Void Return Order" };
                    Messenger.Default.Send<ConfirmWindow>(ConfirmWindowDialog, MessageToken);
                    if (ConfirmWindowDialog.Confirmed && ReasonCodes.SelectedReasonCode != null)
                    {
                        IsAcceptAndPrintEnabled = false;
                        // Check added to disable transition after voiding pick screen
                        CommonNavInfo.IsBackwardNavigationEnabled = false;
                        pickMgr.VoidPick(ReasonCodes.SelectedReasonCode.ID.ToString());
                        CR_Manager.UpdateInventory(PayloadManager.ReturnOrderPayload.ReturnItems, PayloadManager.ReturnOrderPayload.ReturnOrderID, ReturnOrderActions.Void);
                        IsInVoidState = true;
                        bool returnValue = new CustomerReturnsManager().VoidOrder(ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString(), ReasonCodes.SelectedReasonCode.ID.ToString(), ActivityKey.VoidAtROPick);
                        if (returnValue)
                        {
                            // INventory updation logic
                            CustomerReturnsManager ItemReturnsManager = new CustomerReturnsManager();
                            //ItemReturnsManager.UpdateInventory(ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID, ActivityKey.VoidAtROPick);

                            IsVoidStarted = true;
                            IsEnableRemoveException = false;
                            IsEnableManualPick = true;
                            foreach (ReturnItem item in PickItemsException)
                            {
                                item.IsEnableScannerInException = true;
                            }
                            IsEnableRemoveException = PickItemsException.Count > 0 ? true : false;
                            //if (PickItemsException.Count() > 0)
                            //    IsEnableRemoveException = true;
                            if (PickItemsException.Count == 0)
                            {
                                PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.ReturnOrder);
                                var SelectedTab = ViewModelMappings.TabView.ReturnsAndCredits.ReturnsProcess;
                                ViewModelPayload.PayloadManager.ReturnOrderPayload.Reset();
                                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.ReturnOrderPickScreen, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = SelectedTab };
                                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);

                            }
                        }
                        else
                        {
                            //iF no item has been picked then just complete the activity and return.
                            //PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.ReturnOrder);
                            //CR_Manager.UpdateInventory(ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID, ActivityKey.VoidAtROPick);
                            ViewModelPayload.PayloadManager.ReturnOrderPayload.Reset();
                            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.ReturnOrderPickScreen, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
                            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][VoidPickList][ExceptionStackTrace = " + ex.ToString() + "]");
                }
                Logger.Info("SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:VoidPickList]");

            });
            HoldPickList = new DelegateCommand(param =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:HoldPickList]");
                try
                {
                    var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.HoldOrderConfirmation, MessageIcon = "Alert", Confirmed = false };
                    Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                    if (!confirmMessage.Confirmed) return;
                    if (confirmMessage.Confirmed)
                    {
                        PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.ReturnOrder);
                        // INventory updation logic
                        //CustomerReturnsManager ItemReturnsManager = new CustomerReturnsManager();
                        //CR_Manager.UpdateInventory(PayloadManager.ReturnOrderPayload.ReturnItems, PayloadManager.ReturnOrderPayload.ReturnOrderID, ReturnOrderActions.Hold);
                        //foreach (ReturnItem item in PayloadManager.ReturnOrderPayload.ReturnItems)
                        //{
                        //    var invr_query = @"update busdta.Inventory set CommittedQuantity = CommittedQuantity " + item.PickNumericSign + "  " + item.PickNumericCurrVal + ",OnHandQuantity = OnHandQuantity " + item.PickNumericSign + "  " + item.PickNumericCurrVal + " WHERE ItemId=" + item.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                        //    DbEngine.ExecuteNonQuery(invr_query);
                        //}
                        //var invr_query = @"update busdta.Inventory set CommittedQuantity = CommittedQuantity " + PickNumericSign + "  " + PickNumericCurrVal + ",OnHandQuantity = OnHandQuantity " + PickNumericSign + "  " + PickNumericCurrVal + " WHERE ItemId=" + SelectedReturnItem.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                        //DbEngine.ExecuteNonQuery(invr_query);

                        PayloadManager.ReturnOrderPayload.IsOnHold = true;
                        bool returnValue = new CustomerReturnsManager().HoldOrder(ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString(), ActivityKey.HoldAtROPick);
                        if (returnValue)
                        {
                            pickMgr.HoldPick(true);
                            #region Update Activity Count
                            string tempStopDate = string.Empty;
                            string stopID = string.Empty;
                            if (PayloadManager.ReturnOrderPayload.StopDate.Date > DateTime.Now.Date)
                            {
                                tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                                stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.ReturnOrderPayload.Customer.CustomerNo, tempStopDate);
                            }
                            else
                            {
                                tempStopDate = PayloadManager.ReturnOrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                                stopID = PayloadManager.ReturnOrderPayload.Customer.StopID;
                            }
                            new CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                            new CustomerManager().UpdateActivityCount(stopID, true, true);
                            PayloadManager.ReturnOrderPayload.Customer.SaleStatus = "";
                            PayloadManager.ReturnOrderPayload.Customer.PendingActivity = new Managers.CustomerManager().GetPendingAtivityForStop(stopID);
                            new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.ReturnOrderPayload.Customer.CustomerNo);

                            ViewModelPayload.PayloadManager.ReturnOrderPayload.Reset();
                            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.ReturnOrderPickScreen, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
                            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
                            #endregion
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][HoldPickList][ExceptionStackTrace = " + ex.ToString() + "]");
                }
                Logger.Info("SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:HoldPickList]");
            });

            ManualPickContinueCommand = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    ManualPickContinueExecute();
                }
            });
        }

        public void ScanByDeviceExecute(ReturnItem param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:ScanByDeviceExecute]\t" + DateTime.Now + "");

            try
            {
                param.ManuallyPickCount = 0; ManuallyPickCountOfSelected = 0;
                dynamic itm1 = PickItems.FirstOrDefault(i => i.ItemId == param.ItemId && i.TransactionDetailID == param.TransactionDetailID);

                param.PickAdjusted = 0;
                param.IsManuallyPicking = false;
                pickMgr.PickItem(param, param, null,false);


                int TransactionQty = 0;
                double lnInventoryExecuteVal = 0;
                if (!IsScannerOn)
                { lnInventoryExecuteVal = InventoryExecuteVal; }
                else { lnInventoryExecuteVal = 1; }
                if (param != null)
                {

                    var invr_query = @"update busdta.Inventory set CommittedQuantity = CommittedQuantity + " + lnInventoryExecuteVal + " WHERE ItemId=" + param.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                    TransactionQty = DbEngine.ExecuteNonQuery(invr_query);

                    var inv_upd_onHand_query = @"update busdta.Inventory set OnHandQuantity = OnHandQuantity + " + lnInventoryExecuteVal + " WHERE ItemId=" + param.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                    TransactionQty = DbEngine.ExecuteNonQuery(inv_upd_onHand_query);
                }

                foreach (ReturnItem item in PickItems)
                {
                    item.IsAllowManualPick = false;
                }
                foreach (ReturnItem item in PickItemsException)
                {
                    item.IsAllowManualPick = false;
                }
                SelectedReturnItem = param; SelectedExceptionItem = null;

                if (SelectedReturnItem != null)
                {
                    SelectedReturnItem.IsAllowManualPick = true;
                }
                IsEnableRemoveException = false;
                // ****
                IsDirty = false;
                pickcounter++;
                PayloadManager.ReturnOrderPayload.ROIsInProgress = true;
                IsTextBoxFocus = true;
                IsChkExceptionScannerPickEnable = false;

            }
            catch (Exception ex)
            {
                Logger.Error("CustomerReturnsPickViewModel ScanByDeviceExecute(" + param.ItemNumber + ") error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:ScanByDeviceExecute]\t");



        }


        public void ScanByDeviceExceptionExecute(ReturnItem param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:ScanByDeviceExceptionExecute]\t" + DateTime.Now + "");

            try
            {
                param.ManuallyPickCount = 0; ManuallyPickCountOfSelected = 0;
                //You can not unpick item which is not present in exception list
                dynamic itm1 = PickItemsException.FirstOrDefault(i => i.ItemId == param.ItemId && i.TransactionDetailID == param.TransactionDetailID);
                if (itm1 == null)
                {
                    return;
                }
                param.IsManuallyPicking = false;
                pickMgr.UnPickItem(param);

                int TransactionQty = 0;
                double lnInventoryExecuteVal = 0;
                if (!IsScannerOn)
                { lnInventoryExecuteVal = InventoryExecuteVal; }
                else { lnInventoryExecuteVal = 1; }
                if (PickItemsException.Count > 0)
                {
                    // Comment Start - 09/14/2016 - Call Common Function
                    var invr_query = @"update busdta.Inventory set CommittedQuantity = CommittedQuantity - " + lnInventoryExecuteVal + " WHERE ItemId=" + param.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                    TransactionQty = DbEngine.ExecuteNonQuery(invr_query);

                    var inv_upd_onHand_query = @"update busdta.Inventory set OnHandQuantity = OnHandQuantity - " + lnInventoryExecuteVal + " WHERE ItemId=" + param.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                    TransactionQty = DbEngine.ExecuteNonQuery(inv_upd_onHand_query);
                    // Comment End

                    // Call Common Function
                    //InventoryManager objInvMgr = new InventoryManager();
                    //TransactionQty = objInvMgr.UpdateReturnInventoryPick(Convert.ToInt32(InventoryExecuteVal), param.ItemId.Trim(), CommonNavInfo.RouteID);
                }

                //foreach (ReturnItem item in PickItemsException)
                //{
                //    item.IsAllowManualPick = false;
                //}
                //foreach (ReturnItem item in PickItems)
                //{
                //    item.IsAllowManualPick = false;
                //}
                SelectedExceptionItem = param; SelectedReturnItem = null;
                SelectedExceptionItem.IsAllowManualPick = true;

                // ****
                IsDirty = false;
                SortPickedItems();
                IsTextBoxFocus = true; IsEnableRemoveException = false;
                IsChkExceptionScannerPickEnable = true;

            }
            catch (Exception ex)
            {
                Logger.Error("CustomerReturnsPickViewModel ScanByDeviceExceptionExecute(" + param.ItemNumber + ") error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:ScanByDeviceExceptionExecute]\t");



        }


        public void UpdateOnManualCountExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:UpdateOnManualCountExecute]\t" + DateTime.Now + "");
            try
            {
                if (SelectedReturnItem != null)
                {
                    if (IsScannerOn == false)
                        SelectedReturnItem.IsAllowManualPick = true;

                    if (SelectedReturnItem.IsAllowManualPick)
                    {
                        bool IsLastManulPick = false;
                        SelectedReturnItem.ManuallyPickCount = ManuallyPickCountOfSelected;

                        if (SelectedReturnItem.ManuallyPickCount == SelectedReturnItem.PickedQty)
                        {
                            return;
                        }

                        if (SelectedReturnItem.ManuallyPickCount == 0 && SelectedReturnItem.PickedQty == 1)
                        {
                            IsLastManulPick = true;
                        }
                        if (SelectedReturnItem.ManuallyPickCount > SelectedReturnItem.TransactionQty)
                        {
                            var alertMessage = new Helpers.AlertWindow { Message = "Manual quantity exceeds load quantity", MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                            return;
                            //ScanByDeviceExecute(SelectedReturnItem);
                            //return;
                        }
                        SelectedReturnItem.PickAdjusted = 0;
                        SelectedReturnItem.IsManuallyPicking = true;

                       pickMgr.PickItem(SelectedReturnItem, SelectedReturnItem, null,true);
                        PayloadManager.ReturnOrderPayload.PickIsInProgress = true;
                        //Enable Disable buttons
                        ManuallyPickCountOfSelected = 0;
                        foreach (ReturnItem item in PickItems)
                        {
                            item.IsAllowManualPick = false;                }
                        foreach (ReturnItem item in PickItemsException)
                        {
                            item.IsAllowManualPick = false;
                        }
                        //while clicking hold affect inventory
                        //PickNumericOldVal = PickNumericCurrVal;


                        // Scanned Qty Update - Current Transaction
                        int TransactionQty = 0;
                        if (PickNumericCurrVal > 0)
                        {

                            // Call Common Function
                            InventoryManager objInvMgr = new InventoryManager();
                            TransactionQty = objInvMgr.UpdateReturnInventoryPickWithSign(Convert.ToInt32(PickNumericCurrVal), PickNumericSign, SelectedReturnItem.ItemId.Trim(), CommonNavInfo.RouteID);
                        }





                    }
                    else if (!string.IsNullOrEmpty(SelectedReturnItem.ItemId))
                    {
                        if (SelectedExceptionItem == null)
                        {
                            var alertMessage = new Helpers.AlertWindow { Message = "Select reason for manual pick.", MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        }
                    }
                }

                if (SelectedExceptionItem != null)
                {
                    if (!SelectedExceptionItem.IsEnableScannerInException)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = "Click Remove Exception button to enable scanner.", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    if (SelectedExceptionItem.IsAllowManualPick && SelectedExceptionItem.IsEnableScannerInException)
                    {
                        SelectedExceptionItem.ManuallyPickCount = ManuallyPickCountOfSelected;
                        if (SelectedExceptionItem.ManuallyPickCount == 0)
                        {
                            return;
                        }
                        if (SelectedExceptionItem.ExceptionCount < SelectedExceptionItem.ManuallyPickCount)
                        {
                            //SelectedExceptionItem.ManuallyPickCount = SelectedExceptionItem.ExceptionCount;
                            //ManuallyPickCountOfSelected = SelectedExceptionItem.ExceptionCount;
                            var alertMessage = new Helpers.AlertWindow { Message = "Manual quantity exceeds load quantity", MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                            return;
                        }

                        SelectedExceptionItem.IsManuallyPicking = true;
                        pickMgr.UnPickItem(SelectedExceptionItem);
                        PayloadManager.ReturnOrderPayload.PickIsInProgress = true;
                        ManuallyPickCountOfSelected = 0;
                        //Enable Disable buttons
                        foreach (ReturnItem item in PickItemsException)
                        {
                            item.IsAllowManualPick = false;
                        }
                        foreach (ReturnItem item in PickItems)
                        {
                            item.IsAllowManualPick = false;
                        }


                    }
                    else if (!string.IsNullOrEmpty(SelectedExceptionItem.ItemId))
                    {
                        if (SelectedReturnItem == null)
                        {
                            var alertMessage = new Helpers.AlertWindow { Message = "Select reason for manual pick.", MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken); return;
                        }

                    }
                }
                if (SelectedExceptionItem == null && SelectedReturnItem == null)
                {
                    var alertMessage = new Helpers.AlertWindow { Message = "Select Item for manual pick.", MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken); return;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerReturnsPickViewModel UpdateOnManualCountExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:UpdateOnManualCountExecute]\t");

        }


        public void ManualPickExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:ManualPickExecute]\t" + DateTime.Now + "");

            try
            {
                IsTextBoxFocus = true;
                bool showAlert = false;
                if (SelectedReturnItem != null)
                {
                    if (!string.IsNullOrEmpty(SelectedReturnItem.ItemId))
                    {
                        var dialog = new Helpers.DialogWindow { TemplateKey = "ManualPickReason", Title = "Reason Code", Payload = null };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                    }
                    else
                    {
                        showAlert = true;
                    }
                }
                else if (SelectedExceptionItem != null)
                {

                    if (!string.IsNullOrEmpty(SelectedExceptionItem.ItemId))
                    {
                        var dialog = new Helpers.DialogWindow { TemplateKey = "ManualPickReason", Title = "Reason Code", Payload = null };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                    }
                    else
                    {
                        showAlert = true;
                    }
                }
                else
                {
                    showAlert = true;
                }
                if (showAlert)
                {
                    var alertMessage = new Helpers.AlertWindow { Message = "Select item for manual pick.", MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    return;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][ManualPickExecute][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:ManualPickExecute]");
        }

        public void ManualPickContinueExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:ManualPickContinueExecute]\t" + DateTime.Now + "");

            try
            {
                IsTextBoxFocus = true;

                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                if (SelectedReturnItem != null)
                {
                    SelectedReturnItem.IsAllowManualPick = true;
                }
                if (SelectedExceptionItem != null)
                {
                    SelectedExceptionItem.IsAllowManualPick = true; SelectedExceptionItem.IsEnableScannerInException = true;
                }

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][ManualPickContinueExecute][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:ManualPickContinueExecute]");

        }
        private void LoadPickList()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:LoadPickList]\t" + DateTime.Now + "");

            try
            {
                items = new System.Collections.Generic.List<dynamic>();
                foreach (var RO_Item in ReturnItems)
                {
                    RO_Item.TransactionID = Convert.ToInt32(ViewModelPayload.PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString());
                    items.Add(RO_Item);
                }

                // Values are passed statically , after implementing next number pool code is to be replaced
                pickMgr = new PickItemManager(CommonNavInfo.RouteID.ToString(), ViewModelPayload.PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString(), PickItemManager.PickForTransaction.ItemReturn, false);
                pickMgr.PickUpdate += pickMgr_PickUpdate;
                pickMgr.PickItemUpdate += pickMgr_PickItemUpdate;

                //below if statement is coded for primay uom based picking qty affect
                if (!PayloadManager.RouteReplenishmentPayload.NavigatedForVoid)
                {
                    foreach (var item in items)
                    {
                        if (item.PrimaryUM != item.TransactionUOM && item.PickQtyPrimaryUOM > 0)
                        {
                            double convFactor = UoMManager.GetUoMFactor(item.UM, item.PrimaryUM, Convert.ToInt32(item.ItemId.Trim()), item.ItemNumber.Trim());
                            //item.TransactionQty = convFactor * item.TransactionQty;
                            item.TransactionQty = item.ReturnQty == 0 ? convFactor * item.TransactionQty : convFactor * item.ReturnQty;
                            //item.TransactionQtyDisplay = item.TransactionQty;
                            item.ConversionFactor = 1;
                        }
                    }
                }

                Dictionary<PickItemManager.PickType, ObservableCollection<dynamic>> pickList = pickMgr.CreatePickList(items);
                PickItems = pickList[PickItemManager.PickType.Normal];

                ////below if statement is coded for primay uom based picking qty affect
                //if (!PayloadManager.RouteReplenishmentPayload.NavigatedForVoid)
                //{
                //    foreach (var item in PickItems)
                //    {
                //        if (item.PrimaryUM != item.TransactionUOM)
                //        {
                //            double convFactor = UoMManager.GetUoMFactor(item.UM, item.PrimaryUM, Convert.ToInt32(item.ItemId.Trim()), item.ItemNumber.Trim());
                //            item.TransactionQty = convFactor * item.TransactionQty;
                //            item.TransactionQtyDisplay = convFactor * item.TransactionQty;
                //            item.ConversionFactor = 1;
                //        }
                //    }
                //}

                //below if statement is coded for primay uom based picking qty affect
                if (!PayloadManager.RouteReplenishmentPayload.NavigatedForVoid)
                {
                    foreach (var item in PickItems)
                    {
                        if (item.PrimaryUM != item.TransactionUOM && item.PickedQty == 0)
                        {
                            double convFactor = UoMManager.GetUoMFactor(item.UM, item.PrimaryUM, Convert.ToInt32(item.ItemId.Trim()), item.ItemNumber.Trim());
                            item.TransactionQtyPrimaryUOM = convFactor * item.TransactionQty;
                            item.TransactionQtyDisplay = item.TransactionQty;
                            item.TransactionQty = item.ReturnQty == 0 ? convFactor * item.TransactionQty : convFactor * item.ReturnQty;
                            //item.TransactionQty = convFactor * item.TransactionQty;
                            //item.TransactionQtyDisplay = convFactor * item.TransactionQty;
                            item.ConversionFactor = 1;
                        }
                    }
                }
                PickItemsException = pickList[PickItemManager.PickType.Exception];
                if (PayloadManager.ReturnOrderPayload.IsNavigatedForVoid)
                {
                    pickMgr.VoidPick(PayloadManager.ReturnOrderPayload.ReasonCodeID.ToString()); // TODO : Replace this hardcoded value from the one which was selected during void
                    IsAcceptAndPrintEnabled = false;
                    IsInVoidState = true;
                    IsVoidStarted = true;
                    PayloadManager.ReturnOrderPayload.IsNavigatedForVoid = false;
                    CommonNavInfo.IsBackwardNavigationEnabled = false;
                }
                LoadReferencedata();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][LoadPickList][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:LoadPickList]");


        }

        void pickMgr_PickItemUpdate(object sender, PickItemManager.PickItemEventArgs e)
        {
            EnableDisableIsAcceptAndPrint();
        }

        void pickMgr_PickUpdate(object sender, PickItemManager.PickEventArgs e)
        {
            if (e.PickComplete)
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:pickMgr_PickUpdate]\t" + DateTime.Now + "");
                try
                {
                    PayloadManager.ReturnOrderPayload.PickCompleted = true;
                    if (PickItemsException != null && PickItemsException.Count() == 0)
                        IsAcceptAndPrintEnabled = true;

                    if (IsVoidStarted)
                    {
                        PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.ReturnOrder);
                        ViewModelPayload.PayloadManager.ReturnOrderPayload.Reset();
                        var SelectedTab = ViewModelMappings.TabView.ReturnsAndCredits.ReturnsProcess;
                        CommonNavInfo.IsBackwardNavigationEnabled = true;

                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.ReturnOrderPickScreen, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = SelectedTab };
                            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);//CommonNavInfo.
                        }));
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][pickMgr_PickUpdate][ExceptionStackTrace = " + ex.ToString() + "]");
                }
                Logger.Info("SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:pickMgr_PickUpdate]");
            }

        }

        void EnableDisableIsAcceptAndPrint()
        {
            //IsEnableRemoveException = PickItemsException.Count() > 0 ? true : false;
            if (PayloadManager.ReturnOrderPayload.IsNavigatedForVoid)
                IsAcceptAndPrintEnabled = false;
            else
                IsAcceptAndPrintEnabled = (PickItems.All(item => item.ExceptionReasonCode.ToString().ToLower() == "complete")) && !IsVoidStarted && PickItemsException.Count() == 0 ? true : false;

        }

        private void SortPickedItems()
        {
            //var sorted_items = new ObservableCollection<ReturnItem>();
            //foreach (var r_item in PickItems.Where(x => x.ExceptionReasonCode.ToString().ToLower() == "underpicked"))
            //{
            //    sorted_items.Add(r_item);
            //}
            //foreach (var r_item in PickItems.Where(x => x.ExceptionReasonCode.ToString().ToLower() == "overpicked"))
            //{
            //    sorted_items.Add(r_item);
            //}
            //foreach (var r_item in PickItems.Where(x => x.ExceptionReasonCode.ToString().ToLower() == "completed"))
            //{
            //    sorted_items.Add(r_item);
            //}
            //PickItems = new ObservableCollection<dynamic>(sorted_items);
            //    PickItems = new ObservableCollection<dynamic>(PickItems.Where(x => x.ExceptionReasonCode.ToString().ToLower() == "underPicked"));
            //    PickItems = new ObservableCollection<dynamic>(PickItems.Where(x => x.ExceptionReasonCode.ToString().ToLower() == "overPicked"));
            //    PickItems = new ObservableCollection<dynamic>(PickItems.Where(x => x.ExceptionReasonCode.ToString().ToLower()=="completed"));
            //PickItems = new ObservableCollection<dynamic>(PickItems.OrderByDescending(x => x.ExceptionReasonCodeID).ThenBy(x => x.ItemNumber));

            //ExceptionItems = new ObservableCollection<dynamic>(ExceptionItems.OrderByDescending(x => x.ExceptionReasonCodeID).ThenBy(x => x.ItemNumber));
        }

        public override bool ConfirmCancel()
        {
            return base.ConfirmCancel();
        }
        public override bool ConfirmSave()
        {
            //var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.ReturnOrderPickScreen, CloseCurrentView = true, ShowAsModal = false };
            //Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);

            IsDirty = false;
            return base.ConfirmSave();
        }
        public override bool ConfirmClosed()
        {

            return base.ConfirmClosed();
        }

        private async void LoadReferencedata()
        {
            await Task.Run(() =>
            {
                foreach (ReturnItem r_Item in PickItems)
                {
                    if (ItemReturnList.All(x => x.ItemNumber.Trim() != r_Item.ItemNumber.Trim()))
                        ItemReturnList.Add(r_Item.Clone());
                }
            });
        }

        private void EnableDisableScannerForSelectedItem()
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:EnableDisableScannerForSelectedItem]\t" + DateTime.Now + "");

            try
            {
                //Disable scanner when available qty is 0 AND item is Short adjusted
                #region Enable disable scanner
                if (SelectedReturnItem.PrimaryUM == SelectedReturnItem.TransactionUOM)
                {
                    if (SelectedReturnItem.AvailableQty == 0)
                    {
                        SelectedReturnItem.IsAllowManualPick = false;
                        //SelectedReturnItem.IsEnableScanner = false;
                    }
                    else if (SelectedReturnItem.ExceptionReasonCode != "Short")
                    {
                        SelectedReturnItem.IsEnableScanner = true;
                        //IsEnableManualPick = true;
                    }
                }
                else if (SelectedReturnItem.PrimaryUM != SelectedReturnItem.TransactionUOM)
                {
                    if (SelectedReturnItem.AvailableQty < Convert.ToInt32(SelectedReturnItem.ConversionFactor) || (SelectedReturnItem.AvailableQty == 0) || SelectedReturnItem.ExceptionReasonCode == "Short")
                    {
                        SelectedReturnItem.IsAllowManualPick = false;
                        //IsEnableManualPick = false; 
                        SelectedReturnItem.IsEnableScanner = false;

                    }
                    else if (SelectedReturnItem.ExceptionReasonCode != "Short")
                    {
                        SelectedReturnItem.IsEnableScanner = true;
                        //IsEnableManualPick = true;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][EnableDisableScannerForSelectedItem][ExceptionStackTrace = " + ex.ToString() + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:EnableDisableScannerForSelectedItem]\t");

        }

        // Set Scanner Mode
        private void SetScannerModeByProfile()
        {
            if (UserManager.GetProfileScannerMode() == 1)
            {
                IsScannerOn = false;
            }
            else
            {
                IsScannerOn = true;
            }
        }


        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
