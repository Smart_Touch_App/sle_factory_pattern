﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using System.Windows.Ink;
using System.Windows.Controls;
using log4net;
using SalesLogicExpress.Domain;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using SalesLogicExpress.Application.Managers;

namespace SalesLogicExpress.Application.ViewModels
{
    public class DeliveryScreen : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.DeliveryScreen");
        OrderManager objOrderManger = new OrderManager();
        public DeliveryScreen()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][Start:DeliveryScreen]");
            try
            {
                IsBusy = true;
                InitializeCommands();

                if (SalesLogicExpress.Application.ViewModels.CommonNavInfo.NavigationParameter != null)
                {
                    IsPaymentTermChanged = Convert.ToBoolean(SalesLogicExpress.Application.ViewModels.CommonNavInfo.NavigationParameter);
                }
                LoadData();
                if (PayloadManager.OrderPayload.TransactionLastState == ActivityKey.OrderDelivered.ToString())
                {
                    IsInDeliverOrderMode = true;
                }
                if (PayloadManager.OrderPayload.TransactionLastState != null && PayloadManager.OrderPayload.TransactionLastState == ActivityKey.HoldAtDeliverToCustomer.ToString())
                {
                    PayloadManager.ApplicationPayload.StartActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.Order);
                }
                RenderSign();

                if ((!string.IsNullOrEmpty(PayloadManager.OrderPayload.TransactionLastState)) && PayloadManager.OrderPayload.TransactionLastState.Equals(ActivityKey.AcceptOrderOnDelivery.ToString()))
                {
                    SignatureCanvasVM.CanClearCanvas = false;
                    PayloadManager.OrderPayload.TransactionLastState = string.Empty;//Reset flag
                }

                //Place this below line, after click of accept
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = false;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][DeliveryScreen][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][End:DeliveryScreen]");
        }
        private void RenderSign()
        {
            SignatureCanvasVM = new CanvasViewModel(Flow.OrderLifeCycle, SignType.OrderSign, Order.OrderId, IsInDeliverOrderMode)
            {
                Height = 245,
                Width = 300,
                FileName = "SmallPic.jpg",
                FooterText = "Customer Signature",
            };
        }

        void LogActivity(ActivityKey key)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][Start:LogActivity]");
            try
            {
                PayloadManager.OrderPayload.RouteID = PayloadManager.ApplicationPayload.Route;
                PayloadManager.OrderPayload.OrderID = Order.OrderId.ToString();
                PayloadManager.OrderPayload.InvoiceNo = Order.OrderId.ToString();

                Activity ac = new Activity
                {
                    ActivityFlowID = Convert.ToInt32(PayloadManager.OrderPayload.OrderID),
                    ActivityHeaderID = PayloadManager.OrderPayload.TransactionID,
                    CustomerID = PayloadManager.OrderPayload.Customer.CustomerNo,
                    RouteID = PayloadManager.OrderPayload.RouteID,
                    ActivityType = key.ToString(),
                    ActivityStart = DateTime.Now,
                    IsTxActivity = true,
                    ActivityDetailClass = PayloadManager.OrderPayload.ToString(),
                    ActivityDetails = PayloadManager.OrderPayload.SerializeToJson()
                };
                if (key == ActivityKey.HoldAtDeliverToCustomer || key == ActivityKey.OrderDelivered)
                {
                    ac.ActivityEnd = DateTime.Now;
                    PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.Order);
                }
                Activity a = ResourceManager.Transaction.LogActivity(ac);
                if (a != null && a.ActivityHeaderID != null)
                {
                    PayloadManager.OrderPayload.TransactionID = a.ActivityHeaderID;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][LogActivity][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][End:LogActivity]");
        }
        async void LoadData()
        {
            //Sathish fixed for Issue 221
            await Task.Run(() =>
            {
                IsBusy = true;
                // Code Commented By Velmani Karnan - DeadLock occur by new code
                //var confirmMessage = new Helpers.ConfirmWindow { Message = "Do you want to print this item", MessageIcon = "Alert", Confirmed = false };
                //Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                //if (confirmMessage.Confirmed == true)
                //{  
                if (PayloadManager.OrderPayload.Items != null)
                {
                    OrderId = Order.OrderId;
                    ObservableCollection<Models.OrderItem> DeliveryScreenCollection = new ObservableCollection<OrderItem>(PayloadManager.OrderPayload.Items);
                    DeliveryScreenItems = DeliveryScreenCollection;
                    //_SignStroke = new StrokeCollection();
                    GetAllTotals();
                }
                //}
                //else
                //{
                //    IsBusy = false;
                //    return;
                //}
                IsBusy = false;
            });
        }
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.OrderManager");

        #region Command Declarations
        public DelegateCommand PickOrder { get; set; }
        public DelegateCommand ValidateAndSaveSign { get; set; }
        public DelegateCommand ClearCanvas { get; set; }
        public DelegateCommand GetReasonCode { get; set; }
        public DelegateCommand OrderDelivered { get; set; }
        public DelegateCommand VoidOrder { get; set; }
        public DelegateCommand BackToCustomerHome { get; set; }
        public DelegateCommand BackToDailyStops { get; set; }
        public DelegateCommand HoldOrder { get; set; }

        #endregion
        string strDateTime = DateTime.Now.ToString("M'/'dd'/'yyyy' - 'hh:mm ") + DateTime.Now.ToString("tt").ToLower();
        private int _OrderId;
        private ObservableCollection<Models.OrderItem> _DeliveryScreen = new ObservableCollection<Models.OrderItem>();
        private bool _IsCanvasEnable = true;
        public bool IsInDeliverOrderMode = false;
        # region Properties
        public ReasonCode ReasonCodeList = new ReasonCode(string.Empty);
        public int OrderId
        {
            get { return Order.OrderId; }
            set { _OrderId = value; }
        }
        public CanvasViewModel SignatureCanvasVM { get; set; }

        public bool IsCanvasEnable
        {
            get { return _IsCanvasEnable; }
            set
            {
                _IsCanvasEnable = value;
                OnPropertyChanged("IsCanvasEnable");

            }
        }
        public Boolean IsPaymentTermChanged { get; set; }
        public bool IsSignPresent { get; set; }

        public StrokeCollection SignStroke
        {
            get { return _SignStroke; }
            set
            {
                _SignStroke = value;
                OnPropertyChanged("SignStroke");

            }
        }

        public Guid MessageToken { get; set; }
        public string CurrentDate
        {
            get { return strDateTime; }
        }

        private bool _ToggleSignPanelVisibility = false;
        public bool ToggleSignPanelVisibility
        {
            get
            {
                return _ToggleSignPanelVisibility;

            }
            set
            {
                _ToggleSignPanelVisibility = value;
                OnPropertyChanged("ToggleSignPanelVisibility");
            }
        }

        // OrderItem Collection for DeliveryScreen Grid
        // public ObservableCollection<Models.TemplateItem> PreviewItems
        public ObservableCollection<Models.OrderItem> DeliveryScreenItems
        {
            get
            {

                ///  this.previewitems = new Managers.TemplateManager().GetTemplateItemsForCustomer("1108911");
                return _DeliveryScreen;
            }
            set
            {
                _DeliveryScreen = value;
                OnPropertyChanged("DeliveryScreenItems");
            }
        }
        private bool _IsBusy;
        public bool IsBusy
        {
            get
            {
                return _IsBusy;
            }
            set
            {
                _IsBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        #endregion

        # region Properties For Textbox Values

        float varTotalCoffeeAmtValue, varTotalAlliedAmtValue, varOrderTotalValue, varEnergySurchargeValue, varSalesTaxValue, varInvoiceTotalValue;
        private string _TotalCoffeeAmtValue = "0.00", _TotalAlliedAmtValue = "0.00",
                       _OrderTotalValue = "0.00", _EnergySurchargeValue = "0.00",
                       _SalesTaxValue = "0.00", _InvoiceTotalValue = "0.00", _CustomerPO = "";

        public string CustomerPO
        {
            get
            {
                return _CustomerPO;
            }
            set
            {
                _CustomerPO = value;
                //Issue ID- 104 - Fixed by Zakir - Didnt Provide the Customer PO # and Signature in Sign and Print Screen then Pick the items and navigated to Delivery screen.Provide the Customer Signature and customer PO# in Delivery screen then delivert the SO to customer . Then printed the invoice Customer Signature updated correctly but Customer PO# is not updated in invoice print
                DbEngine.ExecuteScalar("Update Busdta.Order_Header SET CustomerReference1 = '" + _CustomerPO + "'  where OrderID ='" + OrderId + "'");
                OnPropertyChanged("CustomerPO");
            }
        }

        public string TotalCoffeeAmtValue
        {
            get
            {
                return "$" + _TotalCoffeeAmtValue;
            }
            set
            {
                _TotalCoffeeAmtValue = value;
                OnPropertyChanged("TotalCoffeeAmtValue");
            }
        }

        public string TotalAlliedAmtValue
        {
            get { return "$" + _TotalAlliedAmtValue; }
            set { _TotalAlliedAmtValue = value; OnPropertyChanged("TotalAlliedAmtValue"); }
        }

        public string OrderTotalValue
        {
            get { return "$" + _OrderTotalValue; }
            set
            {
                _OrderTotalValue = value;
                OnPropertyChanged("OrderTotalValue");
            }
        }

        public string EnergySurchargeValue
        {
            get { return "$" + _EnergySurchargeValue; }
            set
            {
                _EnergySurchargeValue = value;
                OnPropertyChanged("EnergySurchargeValue");
            }
        }
        public string SalesTaxValue
        {
            get { return "$" + _SalesTaxValue; }
            set { _SalesTaxValue = value; OnPropertyChanged("SalesTaxValue"); }
        }
        public string InvoiceTotalValue
        {
            get { return "$" + _InvoiceTotalValue; }
            set { _InvoiceTotalValue = value; OnPropertyChanged("InvoiceTotalValue"); }
        }

        #endregion

        #region methods
        // Synchronize to calculate values on right panel
        async void GetAllTotals()
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][AsyncStart:GetAllTotals]");
                try
                {
                    //DataTable dset = new DataTable();

                    //dset = DbEngine.ExecuteDataSet("select * from BUSDTA.ORDER_HEADER where  OrderID =" + "'" + OrderId + "'").Tables[0];

                    DataTable dset = objOrderManger.Delivery_GetAllTotals(OrderId);

                    foreach (DataRow dr in dset.Rows)
                    {
                        varEnergySurchargeValue = (float)Convert.ToDecimal(dr["EnergySurchargeAmt"]);
                        EnergySurchargeValue = varEnergySurchargeValue.ToString("0.00");
                        varInvoiceTotalValue = (float)Convert.ToDecimal(dr["InvoiceTotalAmt"]);
                        InvoiceTotalValue = varInvoiceTotalValue.ToString("0.00");
                        varOrderTotalValue = (float)Convert.ToDecimal(dr["OrderTotalAmt"]);
                        OrderTotalValue = varOrderTotalValue.ToString("0.00");
                        varSalesTaxValue = (float)Convert.ToDecimal(dr["SalesTaxAmt"]);
                        SalesTaxValue = varSalesTaxValue.ToString("0.00");
                        varTotalAlliedAmtValue = (float)Convert.ToDecimal(dr["TotalAlliedAmt"]);
                        TotalAlliedAmtValue = varTotalAlliedAmtValue.ToString("0.00");
                        varTotalCoffeeAmtValue = (float)Convert.ToDecimal(dr["TotalCoffeeAmt"]);
                        TotalCoffeeAmtValue = varTotalCoffeeAmtValue.ToString("0.00");

                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][GetAllTotals][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][AsyncEnd:GetAllTotals]");
            });
        }
        #endregion

        void UpdateActivityStatus(string stopID, bool isPendingAct, bool isIncrement, string stopDate)
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][Start:UpdateActivityStatus]");
            try
            {
                string query1 = "[DeliveryScreen:-> UpdateActivityStatus --> Start : \n Completed Activity :" + PayloadManager.OrderPayload.Customer.CompletedActivity + "\t PendingActivity : " + PayloadManager.OrderPayload.Customer.CompletedActivity + "";
                query1 += "\n stopID :" + stopID + "\t isPendingAct :" + isPendingAct + "isIncrement :" + isIncrement + " stopDate :" + stopDate + "";
                Logger.Info(query1);

                new CustomerManager().UpdateSequenceNo(stopID, stopDate);
                Logger.Info("Before call to UpdateActivityCount");
                new CustomerManager().UpdateActivityCount(stopID, isPendingAct, isIncrement);
                Logger.Info("After call to UpdateActivityCount");

                PayloadManager.OrderPayload.Customer.HasActivity = true;
                if (isPendingAct)
                {
                    PayloadManager.OrderPayload.Customer.PendingActivity = new Managers.CustomerManager().GetPendingAtivityForStop(stopID);
                    //PayloadManager.OrderPayload.Customer.PendingActivity += 1;
                }
                else
                {
                    PayloadManager.OrderPayload.Customer.CompletedActivity += 1;
                    PayloadManager.OrderPayload.Customer.PendingActivity = new Managers.CustomerManager().GetPendingAtivityForStop(stopID);
                }
                PayloadManager.OrderPayload.Customer.SaleStatus = "";
                new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.OrderPayload.Customer.CustomerNo);
                query1 = "[DeliveryScreen:-> UpdateActivityStatus --> End : \n Completed Activity :" + PayloadManager.OrderPayload.Customer.CompletedActivity + "\t PendingActivity : " + PayloadManager.OrderPayload.Customer.CompletedActivity + "";
                query1 += "\n stopID :" + stopID + "\t isPendingAct :" + isPendingAct + "isIncrement :" + isIncrement + " stopDate :" + stopDate + "";
                Logger.Info(query1);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][UpdateActivityStatus][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][End:UpdateActivityStatus]");
        }
        private void InitializeCommands()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][Start:InitializeCommands]");
            try
            {
                OrderManager OrdManager = new OrderManager();
                CustomerPO = OrdManager.GetCustomerPO(PayloadManager.OrderPayload.OrderID);

                VoidOrder = new DelegateCommand((param) =>
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][Start:VoidOrder]");
                    try
                    {
                        if (ReasonCodeList != null)
                            Order.VoidOrderReasonId = ReasonCodeList.Id;
                        // Set reason for void Order if not available 
                        if (Order.VoidOrderReasonId == 0)
                        {
                            ReasonCodeList.ParentViewModel = this;
                            ReasonCodeList.MessageToken = MessageToken;
                            ReasonCodeList.ActivityKey = ActivityKey.VoidAtDeliverCustomer;
                            var OpenDialog = new Helpers.DialogWindow { TemplateKey = "VoidOrderReason", Title = "Void Order Reason", Payload = ReasonCodeList };
                            Messenger.Default.Send<DialogWindow>(OpenDialog, MessageToken);
                            if (OpenDialog.Cancelled)
                            {
                                OrderStateChangeArgs arg = new OrderStateChangeArgs();
                                arg.State = OrderState.None;
                                ReasonCodeList.OnStateChanged(arg);
                            }
                            return;
                        }
                        else
                        {
                            // Update void reason in DB and Clear InMemory VoidOrderReasonId = 0

                            Order.VoidOrderReasonId = 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][VoidOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][End:VoidOrder]");

                });

                BackToCustomerHome = new DelegateCommand((param) =>
                {
                    var moveToView = new Helpers.NavigateToView
                    {
                        NextViewName = ViewModelMappings.View.CustomerHome,
                        CurrentViewName = ViewModelMappings.View.DeliveryScreen,
                        CloseCurrentView = true,
                        ShowAsModal = false,
                        Refresh = true
                    };
                    if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.CustomerHome_Activity.ToString())
                    {
                        SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Activity;
                    }
                    else if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.CustomerHome_DashBoard.ToString())
                    {
                        SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Dashboard;
                    }
                    moveToView.SelectTab = SelectedTab;
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                });

                BackToDailyStops = new DelegateCommand((param) =>
                {
                    var moveToView = new Helpers.NavigateToView
                    {
                        NextViewName = ViewModelMappings.View.ServiceRoute,
                        CurrentViewName = ViewModelMappings.View.DeliveryScreen,
                        CloseCurrentView = true,
                        ShowAsModal = false,
                        Refresh = true
                    };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                });

                HoldOrder = new DelegateCommand((param) =>
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][Start:HoldOrder]");
                    try
                    {
                        var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.HoldOrderConfirmation, MessageIcon = "Alert", Confirmed = false };
                        Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                        if (!confirmMessage.Confirmed) return;
                        SignatureCanvasVM.UpdateSignatureToDB(false, false, true, "test.jpg");
                        LogActivity(ActivityKey.HoldAtDeliverToCustomer);
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                        Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.HoldAtDeliverToCustomer);

                        string tempStopDate = string.Empty;
                        string stopID = string.Empty;
                        if (PayloadManager.OrderPayload.StopDate.Date > DateTime.Now.Date || PayloadManager.OrderPayload.StopDate.Date < DateTime.Now.Date)
                        {
                            tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                            stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.OrderPayload.Customer.CustomerNo, tempStopDate);
                        }
                        else
                        {
                            tempStopDate = PayloadManager.OrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                            stopID = PayloadManager.OrderPayload.Customer.StopID;
                        }
                        new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.OrderPayload.Customer.CustomerNo);
                        Logger.Info("[DeliveryScreen:-> HoldOrder --> Before UpdateActivityStatus : \n Completed Activity :" + PayloadManager.OrderPayload.Customer.CompletedActivity + "\t PendingActivity : " + PayloadManager.OrderPayload.Customer.CompletedActivity + "");
                        UpdateActivityStatus(stopID, true, true, tempStopDate);
                        Logger.Info("[DeliveryScreen:-> HoldOrder --> After UpdateActivityStatus : \n Completed Activity :" + PayloadManager.OrderPayload.Customer.CompletedActivity + "\t PendingActivity : " + PayloadManager.OrderPayload.Customer.CompletedActivity + "");

                        #region Update CommittedQty
                        // Log commited qty to inventory_ledger and update inventory
                        var objInv = new Managers.InventoryManager();
                        if (PayloadManager.OrderPayload.Items != null)
                            objInv.UpdateCommittedQuantity(PayloadManager.OrderPayload.Items.ToList(), StatusTypesEnum.HOLD);
                        #endregion

                        #region Update tem squncing no.
                        //UPDATE CUSTOMER'S TEMPERORY SEQUENCE NO. --- Priya Natu
                        DateTime todayDate = Convert.ToDateTime(tempStopDate);
                        ViewModels.CustomerDashboard custDash = new CustomerDashboard();
                        PayloadManager.OrderPayload.Customer = custDash.GetNewCustomer(todayDate, PayloadManager.OrderPayload.Customer);
                        PayloadManager.OrderPayload.Customer.CustName = PayloadManager.OrderPayload.Customer.Name;
                        #endregion
                        //ResourceManager.Transaction.AddTransactionInQueueForSync(Managers.Transaction.HoldOrder, SalesLogicExpress.Application.Managers.SyncQueueManager.Priority.immediate);
                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.DeliveryScreen, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][HoldOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][End:HoldOrder]");

                });
                ShowInvoiceReport = new DelegateCommand((param) =>
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][Start:ShowInvoiceReport]");
                    try
                    {
                        var payload = Order.OrderId;
                        //var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PrintInvoice, CurrentViewName = ViewModelMappings.View.DeliveryScreen, CloseCurrentView = false, Payload = payload, ShowAsModal = false };
                        //Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                        var DocInfo = new ReportEngine.DocumentInformation();
                        var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                        repEngine.Notify += repEngine_Notify;

                        repEngine.Log = this.Logger;
                        ReportEngine.IReportSLE ReportData = null;
                        //var query = string.Format("select DocumentID from BUSDTA.PickOrder where Order_ID = {0}", orderID);
                        //var result = Convert.ToInt32(Helpers.DbEngine.ExecuteScalar(query));
                        //var DocumentId = result;
                        var DocumentId = 0;
                        var SourceDocumentNo = Convert.ToDecimal(payload.ToString());
                        if (DocumentId <= 0)
                        {
                            ReportData = new ReportViewModels.InvoiceReport(payload.ToString(), true);
                            Dictionary<string, object> Parameters;
                            Parameters = new Dictionary<string, object>();
                            Parameters.Add("PrintLabels", Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PrintLabels"]));
                            DocInfo = repEngine.Generate<Reporting.Invoice>(SourceDocumentNo, true, ReportEngine.ReportCategory.Customer, ReportEngine.ReportsTypes.InvoiceReport, ReportData, Parameters, ReportEngine.ContentTypes.PDF);
                        }
                        else
                        {
                            DocInfo.DocumentId = DocumentId;
                        }

                        if (DocInfo.IsEndOfSequence)
                        {
                            Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, Please sync to continue" }, MessageToken);
                            IsBusy = false;
                            return;
                        }

                        if (DocInfo.IsDocumentCreated)
                            ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.Order_Header", "OrderID", SourceDocumentNo, DocInfo.DocumentId);
                        DocumentId = Convert.ToInt32(DocInfo.DocumentId); //Replace Latest

                        var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Customer, DocumentId, null);
                        SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
                        Payload.ReportFile = ReportBytes;
                        Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                        Payload.PreviousView = ViewModelMappings.View.DeliveryScreen;
                        Payload.CurrentViewTitle = "Invoice Report";
                        Payload.PreviousViewTitle = "Delivery Screen";
                        Payload.MessageToken = this.MessageToken;
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;

                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.DeliveryScreen, CloseCurrentView = false, Payload = Payload, ShowAsModal = false };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][ShowInvoiceReport][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][End:ShowInvoiceReport]");
                });
                ShowARPayment = new DelegateCommand((param) =>
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][Start:ShowARPayment]");
                    try
                    {
                        PayloadManager.OrderPayload.InvoiceNo = Order.OrderId.ToString();
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.NavigationParameter = "AutoCheckInvoice";

                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Customer Home";
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Payments & AR";
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.CustomerHome;
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ARPayment;
                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ARPayment, CurrentViewName = ViewModelMappings.View.DeliveryScreen, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);

                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][ShowARPayment][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][End:ShowARPayment]");
                });
                OrderDelivered = new DelegateCommand((param) =>
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][Start:OrderDelivered]");
                    try
                    {
                        //Sathish moved this code for showing busy cursor.
                        Deliverbuttonclick();
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][OrderDelivered][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][End:OrderDelivered]");
                });

                PickOrder = new DelegateCommand((param) =>
                {
                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PickOrder, CurrentViewName = ViewModelMappings.View.DeliveryScreen, CloseCurrentView = false, ShowAsModal = false };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                });

                ValidateAndSaveSign = new DelegateCommand((param) =>
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][Start:ValidateAndSaveSign]");
                    try
                    {
                        //Sathish moved this code for showing busy cursor.
                        Acceptbuttonclick();
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][ValidateAndSaveSign][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][End:ValidateAndSaveSign]");
                });
                ClearCanvas = new DelegateCommand((param) =>
                {
                    //SignStroke.Clear();
                    IsCanvasEnable = true;
                });
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][InitializeCommands][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][DeliveryScreen][End:InitializeCommands]");
        }

        void repEngine_Notify(string msg)
        {
            System.Windows.MessageBox.Show(msg);
        }

        public string _SignPanelHeaderText;
        private StrokeCollection _SignStroke;

        public string SignPanelHeaderText { get { return _SignPanelHeaderText; } set { _SignPanelHeaderText = value; OnPropertyChanged("SignPanelHeaderText"); } }

        public double canvasWidth { get; set; }

        public double canvasHeight { get; set; }

        public InkCanvas InkCanvas { get; set; }

        public bool LoadSignState { get; set; }

        public DelegateCommand ShowInvoiceReport { get; set; }
        public DelegateCommand ShowARPayment { get; set; }

        async void Deliverbuttonclick()
        {
            await Task.Run(() =>
            {
                //Sathish Added busy cursor in the deliver screen
                IsBusy = true;
                string tempStopDate = string.Empty;
                string stopID = string.Empty;
                if (PayloadManager.OrderPayload.StopDate.Date > DateTime.Now.Date || PayloadManager.OrderPayload.StopDate.Date < DateTime.Now.Date)
                {
                    tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                    stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.OrderPayload.Customer.CustomerNo, tempStopDate);
                }
                else
                {
                    tempStopDate = PayloadManager.OrderPayload.StopDate.Date.ToString("yyyy-MM-dd");
                    stopID = PayloadManager.OrderPayload.StopID;
                }
                new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.OrderPayload.Customer.CustomerNo);
                Logger.Info("[DeliveryScreen:-> OrderDelivered --> Before UpdateActivityStatus : \n Completed Activity :" + PayloadManager.OrderPayload.Customer.CompletedActivity + "\t PendingActivity : " + PayloadManager.OrderPayload.Customer.CompletedActivity + "");
                UpdateActivityStatus(stopID, false, true, tempStopDate);
                Logger.Info("[DeliveryScreen:-> OrderDelivered --> After UpdateActivityStatus : \n Completed Activity :" + PayloadManager.OrderPayload.Customer.CompletedActivity + "\t PendingActivity : " + PayloadManager.OrderPayload.Customer.CompletedActivity + "");

                Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.OrderDelivered);
                if (PayloadManager.OrderPayload.TransactionLastState != "OrderDelivered")
                {
                    LogActivity(ActivityKey.OrderDelivered);
                }
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = false;
                #region Update CommittedQty
                // Log commited qty to inventory_ledger and update inventory
                var objInv = new Managers.InventoryManager();
                if (PayloadManager.OrderPayload.Items != null && PayloadManager.OrderPayload.TransactionLastState != "OrderDelivered")
                    objInv.UpdateCommittedQuantity(PayloadManager.OrderPayload.Items.ToList(), StatusTypesEnum.DLVRD);

                #region Update temp sequencing no.
                //UPDATE CUSTOMER'S TEMPERORY SEQUENCE NO. --- Priya Natu
                DateTime todayDate = Convert.ToDateTime(tempStopDate);
                ViewModels.CustomerDashboard custDash = new CustomerDashboard();
                PayloadManager.OrderPayload.Customer = custDash.GetNewCustomer(todayDate, PayloadManager.OrderPayload.Customer);
                #endregion

                #endregion
                IsBusy = false;
            });
        }


        async void Acceptbuttonclick()
        {
            await Task.Run(() =>
            {
                IsBusy = true;
                SignatureCanvasVM.UpdateSignatureToDB(true, true, true, "test.jpg");
                SignatureCanvasVM.CanClearCanvas = false;
                SignatureCanvasVM.IsCanvasEnable = false;

                //Store the current state 
                //It help in disbling the canvas clear button which get enabled when clicked on print button
                PayloadManager.OrderPayload.TransactionLastState = ActivityKey.AcceptOrderOnDelivery.ToString();
                IsBusy = false;
            });
        }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
