﻿using log4net;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Reporting;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;


namespace SalesLogicExpress.Application.ViewModels
{
    public class PricingViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.PricingViewModel");
        private PricingModel price;
        private ObservableCollection<PricingModel> prices;
        private int customerID;
        private string title;
        private string code;
        private string detail;

        Prospect prospect;
        public Prospect Prospect
        {
            get
            {
                return prospect;
            }
            set
            {
                prospect = value;
                OnPropertyChanged("Prospect");
            }
        }
        public PricingViewModel(string customerID)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingViewModel][Start:PricingViewModel]");
            this.customerID = Convert.ToInt32(customerID);
            LoadPrices();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingViewModel][End:PricingViewModel]");
        }
        async void LoadPrices()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingViewModel][Start:LoadPrices]");
            await Task.Run(() =>
            {
                try
                {
                    Prices = GetPricingDetails(customerID.ToString());
                }
                catch (Exception ex)
                {
                    Logger.Error("PricingViewModel LoadPrices() error: " + ex.Message);
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingViewModel][End:LoadPrices]");
            });

        }
        public string Title
        {
            get
            {
                return this.title;
            }
            set
            {
                this.title = value;
                OnPropertyChanged("Title");
            }
        }
        public string Detail
        {
            get
            {
                return this.detail;
            }
            set
            {
                this.detail = value;
                OnPropertyChanged("Detail");
            }
        }
        public string Code
        {
            get
            {
                return this.code;
            }
            set
            {
                this.code = value;
                OnPropertyChanged("Code");
            }
        }
        public int CustomerID
        {
            get
            {
                return this.customerID;
            }
            set
            {
                this.customerID = value;
                OnPropertyChanged("CustomerID");
            }
        }
        public ObservableCollection<PricingModel> Prices
        {
            get
            {
                return this.prices;
            }
            set
            {
                this.prices = value;
                OnPropertyChanged("Prices");
            }
        }
        public PricingModel Price
        {
            get
            {
                return this.price;
            }
            set
            {
                this.price = value;
                OnPropertyChanged("Price");
            }
        }
        #region Class
        private PricingSetupDialogViewModel pricingSetupDialogVM = null;

        public PricingSetupDialogViewModel PricingSetupDialogVM
        {
            get { return pricingSetupDialogVM; }
            set { pricingSetupDialogVM = value; OnPropertyChanged("PricingSetupDialogVM"); }
        }


        private CustomerQuoteHeader customerQuote = null;

        public CustomerQuoteHeader CustomerQuote
        {
            get { return customerQuote; }
            set { customerQuote = value; OnPropertyChanged("CustomerQuote"); }
        }
        #endregion

        void Prices_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged("Prices");
        }
        public ObservableCollection<PricingModel> GetPricingDetails(string customerNo)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingViewModel][Start:GetPricingDetails]");
            ObservableCollection<PricingModel> objPrices = new ObservableCollection<PricingModel>();
            try
            {
                PricingManager objManager = new PricingManager();
                objPrices = objManager.GetCustomerHomePricing(customerNo);
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][PricingViewModel][GetPricingDetails][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingViewModel][End:GetPricingDetails]");
            return objPrices;
        }
        public PricingViewModel(Guid token, BaseViewModel Parent)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingViewModel][Start:GetPricingDetails]");
            try
            {
                PayloadManager.ProspectPayload.IsForPricingTab = true;
                ViewModelPayload.PayloadManager.QuotePayload.QuoteId = "NEW";
                MessageToken = token;
                this.ParentView = Parent;
                CustomerQuote = new CustomerQuoteManager().GetCustomerQuote();
                PricingSetupDialogVM = new PricingSetupDialogViewModel(CustomerQuote, token, this.ParentView);
                this.pricingSetupDialogVM.GetDescription();
                PricingSetupDialogVM.IsOverrideView = true;
                Prospect = PayloadManager.ProspectPayload.Prospect;
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][PricingViewModel][PricingViewModel][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingViewModel][End:PricingViewModel]");
        }
        public override bool ConfirmSave()
        {
            PricingSetupDialogVM.ConfirmSave();
            return base.ConfirmSave();
        }
        public override bool ConfirmCancel()
        {
            PricingSetupDialogVM.ConfirmCancel();

            return base.ConfirmCancel();
        }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
