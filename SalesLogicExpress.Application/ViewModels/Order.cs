﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using Models = SalesLogicExpress.Domain;
using Telerik.Windows.Controls.GridView;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using System.Windows.Threading;

namespace SalesLogicExpress.Application.ViewModels
{
    public class Order : BaseViewModel, IDataErrorInfo
    {
        // Test For V1 Build
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.Order");

        private decimal OldValue = 0;
        private decimal NewValue = 0;
        private int NewOrderQtyValue = 0;
        private int OldOrderQtyValue = 0;
        //private byte ReasonCode = 0;
        //private decimal NewPrice = 0;
        void LogActivity(ActivityKey key)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels.Order][Order][Enter in LogActivity]");
            try
            {
                PayloadManager.OrderPayload.RouteID = PayloadManager.ApplicationPayload.Route;
                PayloadManager.OrderPayload.OrderID = Order.OrderId.ToString();
                PayloadManager.OrderPayload.InvoiceNo = Order.OrderId.ToString();
                Activity ac = new Activity
                {
                    ActivityFlowID = Convert.ToInt32(PayloadManager.OrderPayload.OrderID),
                    ActivityHeaderID = PayloadManager.OrderPayload.TransactionID,
                    CustomerID = PayloadManager.OrderPayload.Customer.CustomerNo,
                    RouteID = PayloadManager.OrderPayload.RouteID,
                    ActivityType = key.ToString(),
                    ActivityStart = DateTime.Now,
                    StopInstanceID = PayloadManager.OrderPayload.Customer.StopID,
                    IsTxActivity = key == ActivityKey.PickItem ? false : true,
                    ActivityDetailClass = PayloadManager.OrderPayload.ToString(),
                    ActivityDetails = PayloadManager.OrderPayload.SerializeToJson()
                };
                if (key == ActivityKey.HoldAtOrderEntry)
                {
                    ac.ActivityEnd = DateTime.Now;
                }
                Activity a = ResourceManager.Transaction.LogActivity(ac);
                // TransactionID maps to the activity logged in the transactiondetail table primary key, which is the activityID
                if (key == ActivityKey.CreateOrder)
                {
                    PayloadManager.OrderPayload.TransactionID = a.ActivityID;
                }
                if (key == ActivityKey.CreateOrder)
                {
                    PayloadManager.ApplicationPayload.StartActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.Order);
                }
                if (key == ActivityKey.HoldAtOrderEntry)
                {
                    PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.Order);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][LogActivity][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels.Order][Order][Exit from LogActivity]");
        }
        public event EventHandler<ModelChangeArgs> ModelChanged;
        protected virtual void OnModelChanged(ModelChangeArgs e)
        {
            EventHandler<ModelChangeArgs> handler = ModelChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public class ModelChangeArgs : EventArgs
        {
            public ChangeType Change { get; set; }
            public DateTime StateChangedTime { get; set; }
        }
        public enum ChangeType
        {
            Loaded,
            ResetCanvas,
            ItemAdded
        }
        //private object payload;
        public ReasonCode reasonCode;
        Managers.ItemManager itemManager;
        Managers.OrderManager orderManager;
        private DataTable _OrderHistory;
        private DataTable _OrderHistoryHeaders;
        private ObservableCollection<OpenOrder> _AllOpenOrdersForItem;
        public static bool DialogCancelled = false;
        private ObservableCollection<Item> _SuggestedItemList;
        private ObservableCollection<SalesSummary> _SalesSummaryList;

        public DelegateCommand GetOpenOrdersForItem { get; set; }
        public DelegateCommand VoidOrder { get; set; }
        public DelegateCommand HoldOrder { get; set; }
        public DelegateCommand ResetOrder { get; set; }
        public DelegateCommand AddSearchItemsToOrder { get; set; }
        public DelegateCommand ClearSearchText { get; set; }
        public DelegateCommand SetExtendedPrice { get; set; }
        public DelegateCommand SetOrderQtyPrice { get; set; }
        public DelegateCommand AddSuggestedItemsToOrder { get; set; }
        public DelegateCommand SearchResultTextChange { set; get; }

        public DelegateCommand SearchItem { get; set; }

        public DelegateCommand SearchItemOnType { get; set; }
        public DelegateCommand PreviewOrder { get; set; }
        public DelegateCommand DeleteItemFromOrder { get; set; }
        public DelegateCommand OpenExpander { set; get; }
        public DelegateCommand CellEditEndedDelegateCommand { get; set; }
        public DelegateCommand BeginEditDelegateCommand { get; set; }

        public DelegateCommand SelectionChangedCommand { get; set; }

        public DelegateCommand UpdateAvailableQty { get; set; }
        public DelegateCommand OrderUMChange { get; set; }

        public DelegateCommand PriceOverrideChange { get; set; }

        private ObservableCollection<Item> _SearchItems = new ObservableCollection<Item>();

        private TrulyObservableCollection<Models.OrderItem> _OrderItems;
        private List<OrderItem> ItemOrderQtyList = new List<OrderItem>();
        private bool _SearchResultTextVisibility;
        private bool _ToggleExpander = false;
        private OrderItem SelectedItem { get; set; }
        public bool ToggleExpander
        {
            get { return _ToggleExpander; }
            set
            {
                _ToggleExpander = value;
                OnPropertyChanged("ToggleExpander");
            }
        }

        private bool _IsBusy = true;
        private bool _EnablePreview = true;
        private bool IsButtonEnabled = true;
        private static int _OrderId;
        private static int _VoidOrderReasonId;

        public bool IsBusy
        {
            get
            {
                return _IsBusy;
            }
            set
            {
                _IsBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        bool _EnablePreviewOrder = true;
        public bool EnablePreviewOrder
        {
            get
            {
                return _EnablePreviewOrder;
            }
            set
            {
                _EnablePreviewOrder = value;
                OnPropertyChanged("EnablePreviewOrder");
            }
        }
        public bool EnablePreview
        {
            get
            {
                return _EnablePreview;
            }
            set
            {
                _EnablePreview = value;
                OnPropertyChanged("EnablePreview");
            }
        }
        private string _SearchResultText;
        public string SearchResultText
        {
            get
            {
                if (SearchItemsCount == 0)
                {
                    return "No items found";
                }
                else
                {
                    SearchResultTextVisibility = true;
                    OnPropertyChanged("SearchResultTextVisibility");
                    return "Item(s) found " + SearchItemsCount;
                }
            }
            set
            {
                if (value != _SearchResultText)
                {
                    _SearchResultText = value;
                    OnPropertyChanged("SearchResultText");
                }
            }
        }
        public bool SearchResultTextVisibility
        {
            get
            {
                return _SearchResultTextVisibility;
            }
            set
            {
                _SearchResultTextVisibility = value;
            }
        }

        private int _SearchItemsCount;

        private string _ErrorText;

        public string TotalAmount { get; set; }

        public int SearchItemsCount
        {
            get
            {

                return _SearchItemsCount;
            }
            set
            {
                _SearchItemsCount = value;
                OnPropertyChanged("SearchItemsCount");
                OnPropertyChanged("SearchResultText");
            }
        }

        #region Code commented as it is moved to BaseViewModel
        //string _SearchText;
        //public string SearchText
        //{
        //    get { return _SearchText; }
        //    set { _SearchText = value; OnPropertyChanged("SearchText"); }
        //}

        #endregion

        public string ParentCode { get; set; }
        public string HoldCode { get; set; }
        public string SoldTo { get; set; }

        public string ErrorText
        {
            get
            {
                return _ErrorText;
            }
            set
            {
                _ErrorText = value;
                OnPropertyChanged("ErrorText");
            }
        }

        public static int VoidOrderReasonId
        {
            get { return _VoidOrderReasonId; }
            set { _VoidOrderReasonId = value; }
        }

        public static int OrderId
        {
            get { return _OrderId; }
            set { _OrderId = value; }
        }
        public static string StopID { get; set; }

        public static ActivityKey OrderStatus { get; set; }
        public ObservableCollection<OpenOrder> AllOpenOrdersForItem
        {
            get { return _AllOpenOrdersForItem; }
            set
            {
                _AllOpenOrdersForItem = value;
                OnPropertyChanged("AllOpenOrdersForItem");
            }
        }

        public DataTable OrderHistory
        {
            get { return _OrderHistory; }
            set { _OrderHistory = value; OnPropertyChanged("OrderHistory"); }
        }
        public DataTable OrderHistoryHeaders
        {
            get { return _OrderHistoryHeaders; }
            set { _OrderHistoryHeaders = value; OnPropertyChanged("OrderHistoryHeaders"); }
        }

        public TrulyObservableCollection<Models.OrderItem> OrderItems
        {
            get
            {
                return this._OrderItems;
            }
            set
            {
                _OrderItems = value;
                OnPropertyChanged("OrderItems");

            }
        }



        public ObservableCollection<Models.Item> SearchItemList
        {
            get
            {
                return this._SearchItems;
            }
            set
            {
                this._SearchItems = value;
                OnPropertyChanged("SearchItemList");
                this.OnPropertyChanged("SearchItemsCount");
                this.OnPropertyChanged("SearchResultText");
            }
        }

        public ObservableCollection<SalesSummary> SalesSummaryList
        {
            get
            {
                _SalesSummaryList = new ObservableCollection<SalesSummary>();
                _SalesSummaryList.Add(new SalesSummary { Parameter = "Amnt. Collected ($)", Regular = 3000 });
                _SalesSummaryList.Add(new SalesSummary { Parameter = "Sold Coffee (Lbs)", Regular = 110, Promo = 150 });
                _SalesSummaryList.Add(new SalesSummary { Parameter = "Sold Allied ($)", Regular = 150, Promo = 160 });
                return _SalesSummaryList;

            }
            set
            {
                this._SalesSummaryList = value;
                OnPropertyChanged("SalesSummaryList");
            }
        }

        public ObservableCollection<Models.Item> SuggestedItemList
        {
            get
            {
                return this._SuggestedItemList;
            }
            set
            {
                this._SuggestedItemList = value;
                OnPropertyChanged("SuggestedItemList");
            }
        }

        bool _IsSearching = false;
        public bool IsSearching
        {
            get
            {
                return _IsSearching;
            }
            set
            {


                _IsSearching = value;
                OnPropertyChanged("IsSearching");
            }
        }
        public Guid MessageToken { get; set; }

        public Order()
        {
            try
            {

                Logger.Info("[SalesLogicExpress.Application.ViewModels.Order][Order][Enter in Constructor]");
                IsBusy = true;
                SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Dashboard;
                reasonCode = new ReasonCode(string.Empty);
                itemManager = new Managers.ItemManager();
                orderManager = new Managers.OrderManager();
                //this.payload = payload;
                InitializeCommands();
                //PayloadManager.OrderPayload.TransactionID = null;

                LoadData();
                GetSearchItems();
                GetOrderRelatedData();
            }
            catch (Exception ex)
            {
                Logger.Error("Error: Order(object payload)" + ex.StackTrace.ToString());
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels.Order][Order][Exit from Constructor]");
        }
        public override bool ConfirmSave()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][Start:ConfirmSave]");
            try
            {
                VoidOrder.Execute(null);
                //LogActivity(ActivityKey.VoidAtOrderEntry);
                //Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), OrderManager.OrderState.OrderEntry, OrderManager.OrderSubState.Void);
                Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][End:ConfirmSave]");
                return base.ConfirmSave();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][ConfirmSave][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
        }
        public override bool ConfirmCancel()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][Start:ConfirmCancel]");
            LogActivity(ActivityKey.HoldAtOrderEntry);
            Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.HoldAtOrderEntry);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][End:ConfirmCancel]");
            return base.ConfirmCancel();
        }
        public void SetOrderItemsToPayload()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][Start:SetOrderItemsToPayload]");
            try
            {
                if (OrderItems != null && (Order.OrderStatus != ActivityKey.VoidAtCashCollection
                      && Order.OrderStatus != ActivityKey.VoidAtDeliverCustomer
                      && Order.OrderStatus != ActivityKey.VoidAtOrderEntry))
                    PayloadManager.OrderPayload.Items = OrderItems.ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][SetOrderItemsToPayload][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][End:SetOrderItemsToPayload]");
        }
        async void GetSearchItems()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][AsyncStart:GetSearchItems]");
            await Task.Run(() =>
            {
                try
                {
                    IsSearching = true;
                    SearchResultText = string.Empty;
                    SearchItemList = itemManager.SearchItem(string.Empty.ToString(), SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
                    IsSearching = false;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][GetSearchItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][AsyncEnd:GetSearchItems]");
        }

        async void GetOrderRelatedData()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][AsyncStart:GetOrderRelatedData]");
            await Task.Run(() =>
            {
                try
                {
                    GetOrderHistory();
                    GetSuggestedItems();
                    GetSalesHeader();
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][GetOrderRelatedData][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][AsyncEnd:GetOrderRelatedData]");
        }
        List<Models.OrderItem> UpdateInventoryForItems(List<Models.OrderItem> Items)
        {
            List<Models.OrderItem> updatedItems = new List<OrderItem>();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][Start:UpdateInventoryForItems]");
            try
            {
                InventoryManager inventoryManager = new InventoryManager();
                foreach (OrderItem item in Items)
                {
                    InventoryManager.ItemQty itemQty = inventoryManager.GetItemQuantity(item.ItemId);
                    item.QtyOnHand = itemQty.OnHandQty;

                    item.UMConversionFactor = new TemplateManager().SetConversionFactorForItem(item.PrimaryUM, item.UM, item.ItemId, item.ItemNumber);
                    item.AvailableQty = itemQty.OnHandQty - (itemQty.ComittedQty + itemQty.HeldQuantity - item.LastComittedQty + Convert.ToInt32(item.OrderQty * item.UMConversionFactor));
                    // itemQty.OnHandQty - itemQty.ComittedQty - itemQty.HeldQuantity + Convert.ToInt32((item.LastComittedQty - item.OrderQty) * item.UMConversionFactor);
                    item.IsValidForOrder = item.ActualAvailableQty < 0 ? false : true;
                    updatedItems.Add(item);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][UpdateInventoryForItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][End:UpdateInventoryForItems]");
            return updatedItems;
        }
        async void LoadData()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][asyncStart:LoadData]");
            await Task.Run(() =>
            {
                try
                {
                    //int count = Convert.ToInt32(DbEngine.ExecuteScalar(" SELECT Count(*) FROM BUSDTA.F0005 WHERE DRSY='56' AND DRRT='RB' AND DRKY in (SELECT MAPA8 FROM BUSDTA.F0150 WHERE MAAN8= " + SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo + ") "));

                    //**********************************************************************************************************
                    PayloadManager.OrderPayload.Items = UpdateInventoryForItems(PayloadManager.OrderPayload.Items);
                    AdjustOrderQty(PayloadManager.OrderPayload.Items);
                    ObservableCollection<Models.OrderItem> OrderCollection = new ObservableCollection<Models.OrderItem>(PayloadManager.OrderPayload.Items);
                    OrderId = Order.OrderId;
                    PayloadManager.OrderPayload.OrderID = OrderId.ToString();
                    Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.CreateOrder);
                    //**********************************************************************************************************
                    ModelChangeArgs args = new ModelChangeArgs();
                    args.StateChangedTime = DateTime.Now;
                    args.Change = ChangeType.Loaded;
                    OnModelChanged(args);
                    //**********************************************************************************************************
                    if (this._SearchItems != null)
                        _SearchItemsCount = this._SearchItems.Count;
                    OrderItems = new TrulyObservableCollection<OrderItem>(OrderCollection.ToList<OrderItem>());


                    OrderItems.ItemPropertyChanged += OrderItems_ItemPropertyChanged;
                    if (PayloadManager.OrderPayload.TransactionID == null)
                    {
                        LogActivity(ActivityKey.CreateOrder);
                    }
                    else if (PayloadManager.OrderPayload.TransactionLastState != null && PayloadManager.OrderPayload.TransactionLastState.Equals(ActivityKey.HoldAtOrderEntry.ToString()))
                    {
                        PayloadManager.ApplicationPayload.StartActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.Order);
                    }

                    TotalAmount = OrderItems.Sum(item => Math.Round(item.ExtendedPrice, 2, MidpointRounding.AwayFromZero)).ToString();
                    PayloadManager.OrderPayload.Amount = TotalAmount;
                    EnablePreviewOrder = OrderItems.Count != 0 ? true : false;// && (OrderItems.Count(item => item.OrderQty > 0) == 0 ? false : true);
                    IsBusy = false;
                    LoadReferencedata();

                }
                catch (Exception ex)
                {
                    IsBusy = false;
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][LoadData][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][AsyncEnd:LoadData]");
        }

        public void ResetSign()
        {
            new CanvasViewModel(Flow.OrderLifeCycle, SignType.OrderSign, Order.OrderId).ResetSignFromDB();
        }

        private async void LoadReferencedata()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][asyncStart:LoadReferencedata]");
            await Task.Run(() =>
            {
                try
                {
                    foreach (OrderItem orderItem in OrderItems)
                    {
                        if (ItemOrderQtyList.All(x => x.ItemNumber.Trim() != orderItem.ItemNumber.Trim()))
                            ItemOrderQtyList.Add(orderItem.Clone());
                    }
                }
                catch (Exception ex)
                {
                    
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][LoadReferencedata][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][AsyncEnd:LoadReferencedata]");
        }
        void OrderCollection_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            throw new NotImplementedException();
        }

        void OrderCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //  throw new NotImplementedException();
        }
        private void OrderItems_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("OrderQty", StringComparison.CurrentCultureIgnoreCase))
            {
                EnablePreviewOrder = OrderItems.Count != 0 && (OrderItems.Count(item => item.OrderQty > 0) == 0 ? false : true);
            }
        }

        private void OrderItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //  ViewModels.PreviewOrder.GlobalInkCanvas = new System.Windows.Controls.InkCanvas();

        }

        private void ValidateSearch(int TotalItemsToAdd, int ItemsNotAddedCount)
        {
            if (TotalItemsToAdd == ItemsNotAddedCount)
            {
                //ALL/ONE ITEMS PRESNTS in item template
                Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Helpers.Constants.Common.AllOrOneItemsExists }, MessageToken);
            }
            else if (TotalItemsToAdd > ItemsNotAddedCount && ItemsNotAddedCount > 0)
            {
                //PARTIAL ITEM PRESENT in item template
                Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Helpers.Constants.Common.PartialItemsExists }, MessageToken);
            }
        }

        public void GetSalesHeader()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][Start:GetSalesHeader]");
            try
            {

                ParentCode = orderManager.GetParent(Convert.ToInt32(SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo));//get parent
                HoldCode = orderManager.GetHoldCode(SalesLogicExpress.Application.ViewModels.CommonNavInfo.UserBranch);//get HoldCode
                SoldTo = orderManager.GetSoldTo(Convert.ToInt32(SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo));
                //If Parent is null, Show SoldTo
                if (string.IsNullOrEmpty(ParentCode))
                {
                    ParentCode = SoldTo;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][GetSalesHeader][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][End:GetSalesHeader]");
        }

        public class SalesHeader
        {
            public SalesHeader(string orderNumber, string orderDate)
            {
                OrderNumber = orderNumber;
                OrderDate = orderDate;
            }
            public string OrderNumber;
            public string OrderDate;
        }
        public class SalesSummary
        {
            public string Parameter { get; set; }
            public int Regular { get; set; }
            public int Promo { get; set; }
        }
        private void GetOrderHistory()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][Start:GetOrderHistory]");
            try
            {
                Managers.OrderManager orderManager = new Managers.OrderManager();
                DataSet RowsAndHeaders = orderManager.GetOrderHistoryForCustomer(SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
                if (RowsAndHeaders.HasData())
                {
                    OrderHistory = RowsAndHeaders.Tables[0];
                    OrderHistoryHeaders = RowsAndHeaders.Tables[1];
                }

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][GetOrderHistory][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                // throw;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][End:GetOrderHistory]");
        }

        private void GetSuggestedItems()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][Start:GetSuggestedItems]");
            try
            {
                SuggestedItemList = new ObservableCollection<Item>();
                SuggestedItemList = itemManager.GetSuggestedItems(SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][GetSuggestedItems][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][End:GetSuggestedItems]");
        }

        public decimal GetExtentedPrice(decimal Qty, decimal UnitPrice, string PriceUM, string AppliedUM, string ItemId, string ItemNumber)
        {
            decimal ExtendedPrice = -1;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][Start:GetExtentedPrice]");
            try
            {
                if (PriceUM == AppliedUM)
                {
                    ExtendedPrice = Qty * UnitPrice;
                }
                else
                {
                    Managers.PricingManager pricingManager = new Managers.PricingManager();
                    decimal factor = 0.0m;
                    // decimal factor = decimal.Parse(pricingManager.jdeUOMConversion(PriceUM, AppliedUM, Int32.Parse(ItemId)).ToString());

                    //   decimal factor = Convert.ToDecimal(UoMManager.GetUoMFactor(PriceUM, AppliedUM, Convert.ToInt32(ItemId.Trim()), Convert.ToInt32(ItemNumber.Trim())));
                    if (UoMManager.ItemUoMFactorList != null)
                    {
                        var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == ItemId.Trim()) && (x.FromUOM == PriceUM.ToString()) && (x.ToUOM == AppliedUM));
                        factor = itemUomConversion == null ? Convert.ToDecimal(UoMManager.GetUoMFactor(PriceUM, AppliedUM, Convert.ToInt32(ItemId.Trim()), ItemNumber.Trim())) : Convert.ToDecimal(itemUomConversion.ConversionFactor);
                    }
                    else
                    {
                        factor = Convert.ToDecimal(UoMManager.GetUoMFactor(PriceUM, AppliedUM, Convert.ToInt32(ItemId.Trim()), ItemNumber.Trim()));
                    }
                    ExtendedPrice = Qty * UnitPrice * factor;

                }

            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][GetExtentedPrice][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][End:GetExtentedPrice]");
            return ExtendedPrice;
           
        }

        public bool IsValidUM(string AppliedUM, string ItemId, string PriceUM, string ItemNumber)
        {
            bool result = false;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][Start:IsValidUM]");
            try
            {
                Managers.PricingManager pricingManager = new Managers.PricingManager();

                // if (orderManager.ValidateUM(ItemNo).Contains(AppliedUM) && decimal.Parse(pricingManager.jdeUOMConversion(PriceUM, AppliedUM, Int32.Parse(ItemNo)).ToString()) >= 0)
                if (orderManager.ValidateUM(ItemId).Contains(AppliedUM) && Convert.ToDecimal(UoMManager.GetUoMFactor(PriceUM, AppliedUM, Convert.ToInt32(ItemId.Trim()), ItemNumber.Trim())) >= 0)
                    result = true;

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][IsValidUM][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][End:IsValidUM]");
            return result;
        }
        public double AveragerQty(string itemNumber)
        {
            double avgQty = 0;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][Start:AveragerQty]");
            try
            {
                foreach (DataRow dr in OrderHistory.Rows)
                {
                    if (dr["ItemCode"].ToString() == itemNumber)
                    {
                        avgQty = Convert.ToDouble(dr["AverageQty"]);
                        break;
                    }
                    else
                    {
                        avgQty = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][AveragerQty][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][End:AveragerQty]");
            return avgQty;

        }
        public ObservableCollection<OpenOrder> GetAllOpenOrdersForItem(string ItemNumber)
        {
            ObservableCollection<OpenOrder> openOrders = new ObservableCollection<OpenOrder>();
            try
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][Start:GetAllOpenOrdersForItem]");
                DataTable dt = orderManager.GetAllOpenOrdersForItem(ItemNumber);
                OpenOrder openOrder;
                foreach (DataRow item in dt.Rows)
                {
                    openOrder = new OpenOrder();
                    openOrder.CustomerId = item["customer_id"].ToString();
                    openOrder.CustomerName = item["CustomerName"].ToString();
                    openOrder.OrderId = item["Order_id"].ToString();
                    openOrder.OrderQty = Convert.ToInt32(item["Order_Qty_Primary_UOM"].ToString());
                    openOrder.PickQty = Convert.ToInt32(item["Picked_Qty_Primary_UOM"].ToString());
                    openOrder.PrimaryUM = item["Primary_UOM"].ToString();
                    openOrders.Add(openOrder);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][AveragerQty][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][End:GetAllOpenOrdersForItem]");
            return openOrders;

        }
        private void InitializeCommands()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][Start:InitializeCommands]");
            Random qtyOnHand = new Random();

            VoidOrder = new DelegateCommand((param) =>
            {
                //reasonCode = param as ReasonCode;

                try
                {
                    if (reasonCode != null)
                        VoidOrderReasonId = reasonCode.Id;

                    // Set reason for void Order if not available 
                    if (VoidOrderReasonId == 0)
                    {

                        reasonCode.ParentViewModel = this;
                        reasonCode.MessageToken = MessageToken;
                        reasonCode.ActivityKey = ActivityKey.VoidAtOrderEntry;
                        var OpenDialog = new Helpers.DialogWindow { TemplateKey = "VoidOrderReason", Title = "Void Order Reason", Payload = reasonCode };
                        Messenger.Default.Send<DialogWindow>(OpenDialog, MessageToken);
                        DialogCancelled = OpenDialog.Cancelled;

                        return;
                    }
                    else
                    {
                        // Update void reason in DB and Clear InMemory VoidOrderReasonId = 0
                        VoidOrderReasonId = 0;
                    }
                }
                catch (Exception ex)
                {
                    
                   Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][VoidOrder DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }

            });

            HoldOrder = new DelegateCommand((e) =>
            {
                /*Added by Dinesh - TUI 
                 * For Double click of Hold button validation 
                 */

                try
                {
                    if (Order.OrderId == 0)
                        return;

                    DataTable orderInfoData = new Managers.OrderManager().GetOrderInfo(Order.OrderId.ToString());
                    if (orderInfoData != null)
                    {
                        if (orderInfoData.Rows.Count > 0)
                        {
                            if (ActivityKey.HoldAtOrderEntry.GetStatusIdFromDB() == Convert.ToInt32(orderInfoData.Rows[0]["OrderStateId"].ToString()))
                                return;
                        }
                        else
                            return;
                    }
                    else
                        return;

                    var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.HoldOrderConfirmation, MessageIcon = "Alert", Confirmed = false };
                    Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                    if (!confirmMessage.Confirmed || confirmMessage.Cancelled) return;

                    if (confirmMessage.Confirmed)
                    {
                        InventoryManager inventoryManager = new InventoryManager();
                        foreach (OrderItem item in OrderItems)
                        {
                            // item.LastComittedQty = inventoryManager.GetOrderQtyInPrimaryUM(item);
                        }
                        OrderStateChangeArgs arg = new OrderStateChangeArgs();
                        arg.State = OrderState.Hold;
                        PayloadManager.OrderPayload.Items = OrderItems.ToList<Models.OrderItem>();
                        LogActivity(ActivityKey.HoldAtOrderEntry);
                        reasonCode.OnStateChanged(arg);
                    }
                    Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.HoldAtOrderEntry);
                    #region Update Activity Count
                    string tempStopDate = string.Empty;
                    string stopID = string.Empty;
                    if (PayloadManager.OrderPayload.StopDate.Date > DateTime.Now.Date || PayloadManager.OrderPayload.StopDate.Date < DateTime.Now.Date)
                    {
                        tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                        stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.OrderPayload.Customer.CustomerNo, tempStopDate);
                    }
                    else
                    {
                        tempStopDate = PayloadManager.OrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                        stopID = PayloadManager.OrderPayload.Customer.StopID;
                    }
                    new CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                    new CustomerManager().UpdateActivityCount(stopID, true, true);
                    PayloadManager.OrderPayload.Customer.SaleStatus = "";
                    PayloadManager.OrderPayload.Customer.PendingActivity = new Managers.CustomerManager().GetPendingAtivityForStop(stopID);
                    //PayloadManager.OrderPayload.Customer.PendingActivity += 1;
                    new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.OrderPayload.Customer.CustomerNo);
                    //PayloadManager.OrderPayload.Customer.HasActivity = true;
                    #endregion


                    //var invList = new List<InventoryItem>();
                    //foreach (var orderItem in OrderItems)
                    //{
                    //    var inventoryItem = new InventoryItem();
                    //    //orderItem.UM = selected Um
                    //    inventoryItem.ItemId = orderItem.ItemId;
                    //    inventoryItem.ItemNumber = orderItem.ItemNumber;
                    //    inventoryItem.CommittedQty = orderItem.OrderQty;  // needs to be in primary UoM
                    //    inventoryItem.PrimaryUOM = orderItem.PrimaryUM;
                    //    inventoryItem.SelectedUom = orderItem.UM;
                    //    //inventoryItem.
                    //    invList.Add(inventoryItem);
                    //}
                    SaveOrder(Convert.ToInt32(PayloadManager.OrderPayload.Customer.CustomerNo), OrderItems);
                    #region UpdateCommitedQuantity
                    // Log commited qty to inventory_ledger  and update inventory
                    //var objInv = new Managers.InventoryManager();
                    //if (OrderItems != null) objInv.UpdateCommittedQuantity(OrderItems.ToList(), StatusTypesEnum.HOLD);
                    #endregion
                    new OrderManager().UpdateOrderItemQty(OrderItems.ToList(), PayloadManager.OrderPayload.OrderID);
                    //UPDATE CUSTOMER'S TEMPERORY SEQUENCE NO. --- Priya Natu
                    DateTime todayDate = Convert.ToDateTime(tempStopDate);
                    ViewModels.CustomerDashboard custDash = new CustomerDashboard();
                    PayloadManager.OrderPayload.Customer = custDash.GetNewCustomer(todayDate, PayloadManager.OrderPayload.Customer);
                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.Order, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][HoldOrder DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });

            GetOpenOrdersForItem = new DelegateCommand((param) =>
           {

               try
               {
                   Models.OrderItem OrderItem = param as Models.OrderItem;

                   AllOpenOrdersForItem = GetAllOpenOrdersForItem(OrderItem.ItemNumber);

                   if (AllOpenOrdersForItem.Count > 0)
                   {
                       var dialog = new Helpers.DialogWindow { TemplateKey = "AllOpenOrdersForItem", Title = "Open Orders for " + OrderItem.ItemNumber.Trim() + ", " + OrderItem.ItemDescription, Payload = null };
                       Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                   }
                   else
                   {
                       var alertWindow = new Helpers.AlertWindow { Message = "There is no any open Order for this item" };
                       Messenger.Default.Send<Helpers.AlertWindow>(alertWindow, MessageToken);
                   }
               }
               catch (Exception ex)
               {

                   Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][GetOpenOrdersForItem DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
               }

           });


            SetUnitPrice = new DelegateCommand((selectedItem) =>
            {

                try
                {

                    OrderItem orderItem = selectedItem as OrderItem;

                    orderItem.UnitPrice = OrderManager.GetPriceByUomFactor(orderItem.ItemId, orderItem.UMPrice, orderItem.UM, orderItem.UnitPriceByPricingUOM);
                    orderItem.IsUnitPriceDirty = false;
                    if (orderItem.IsReasonCodeProvided == false)
                        orderItem.ReasonCode = 0;
                    else
                        orderItem.ReasonCode = 1;
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][SetUnitPrice DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });

            SetOrderQtyPrice = new DelegateCommand((selectedItem) =>
            {

                //************************************************************************************************
                // Comment: Added Below two lines for Recalculation of the unit price and UnitPriceByPricingUOM.
                // Created: Jan 24, 2016
                // Author: Vivensas (Rajesh,Yuvaraj)
                // Revisions: Update for unit price change.  
                //*************************************************************************************************

                //orderItem.UnitPriceByPricingUOM = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(PayloadManager.OrderPayload.Customer.RouteBranch.ToString().Split('.')[0], PayloadManager.OrderPayload.Customer.CustomerNo, orderItem.ItemNumber, orderItem.OrderQty.ToString()));
                //orderItem.UnitPrice = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(PayloadManager.OrderPayload.Customer.RouteBranch.ToString().Split('.')[0], PayloadManager.OrderPayload.Customer.CustomerNo, orderItem.ItemNumber, orderItem.OrderQty.ToString())); 
                //    Decimal UnitPriceUpdate = 0;

                //************************************************************************************************


                try
                {
                    OrderItem orderItem = selectedItem as OrderItem;

                    PricingManager pricingManager = new PricingManager();
                    Decimal UnitPriceUpdate = 0;

                    if (orderItem.UM != orderItem.UMPrice)
                    {
                        if (!IsValidUM(orderItem.UM, orderItem.ItemId, orderItem.UMPrice, orderItem.ItemNumber))
                        {
                            Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = "This is not a valid UM" }, MessageToken);

                            orderItem.UnitPriceByPricingUOM = orderItem.UnitPriceByPricingUOM;
                            UnitPriceUpdate = orderItem.UnitPrice;
                            orderItem.UM = orderItem.UMPrice;
                            return;
                        }
                        else
                        {
                            //orderItem.ExtendedPrice = GetExtentedPrice(orderItem.OrderQty, orderItem.UnitPrice, orderItem.UM, orderItem.UMPrice, orderItem.ItemId, orderItem.ItemNumber);

                            orderItem.UnitPriceByPricingUOM = orderItem.UnitPriceByPricingUOM;
                            UnitPriceUpdate = orderItem.UnitPrice;

                            //User Issue Id : 42 Instead of passing three decimal values in UnitPrice during the calc values in decimal places showing wrongly. Show that round off the unit price values in two decimal places.
                            //orderItem.ExtendedPrice = orderItem.OrderQty * orderItem.UnitPrice;
                            orderItem.ExtendedPrice = orderItem.OrderQty * Math.Round(orderItem.UnitPrice, 2, MidpointRounding.AwayFromZero);
                            return;
                        }
                    }

                    //Update On hand Qty
                    if ((orderItem.AvailableQty - orderItem.OrderQty) < 0)
                    {
                        //Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = "Order quantity exceeds On hand quantity" }, MessageToken);
                    }
                    //orderItem.AvailableQty = (orderItem.AvailableQty - orderItem.OrderQty) < 0 ? 0 : (orderItem.AvailableQty - orderItem.OrderQty);


                    //update extended price
                    //orderItem.ExtendedPrice = GetExtentedPrice(orderItem.OrderQty, orderItem.UnitPrice, orderItem.UMPrice, orderItem.UM, orderItem.ItemId, orderItem.ItemNumber);

                    orderItem.UnitPriceByPricingUOM = orderItem.UnitPriceByPricingUOM;
                    UnitPriceUpdate = orderItem.UnitPrice;

                    //if (orderItem.UnitPrice != UnitPriceUpdate)
                    //{
                    //    orderItem.UnitPriceByPricingUOM = orderItem.UnitPrice; // Vivensas check and update for the price change.
                    //}
                    // Vivensas round off for extended price from row data change and total
                    orderItem.ExtendedPrice = orderItem.OrderQty * Math.Round(orderItem.UnitPrice, 2, MidpointRounding.AwayFromZero);
                    TotalAmount = OrderItems.Sum(item => Math.Round(item.ExtendedPrice, 2, MidpointRounding.AwayFromZero)).ToString("0.00");
                    PayloadManager.OrderPayload.Amount = TotalAmount;
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][SetOrderQtyPrice DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });

            SetExtendedPrice = new DelegateCommand((selectedItem) =>
            {

                //************************************************************************************************
                // Comment: Added Below two lines for Recalculation of the unit price and UnitPriceByPricingUOM.
                // Created: Jan 24, 2016
                // Author: Vivensas (Rajesh,Yuvaraj)
                // Revisions: Update for unit price change.  
                //*************************************************************************************************

                //orderItem.UnitPriceByPricingUOM = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(PayloadManager.OrderPayload.Customer.RouteBranch.ToString().Split('.')[0], PayloadManager.OrderPayload.Customer.CustomerNo, orderItem.ItemNumber, orderItem.OrderQty.ToString()));
                //orderItem.UnitPrice = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(PayloadManager.OrderPayload.Customer.RouteBranch.ToString().Split('.')[0], PayloadManager.OrderPayload.Customer.CustomerNo, orderItem.ItemNumber, orderItem.OrderQty.ToString())); 
                //    Decimal UnitPriceUpdate = 0;

                //************************************************************************************************


                OrderItem orderItem = selectedItem as OrderItem;
                try
                {

                    PricingManager pricingManager = new PricingManager();
                    Decimal UnitPriceUpdate = 0;

                    if (orderItem.UM != orderItem.UMPrice)
                    {
                        if (!IsValidUM(orderItem.UM, orderItem.ItemId, orderItem.UMPrice, orderItem.ItemNumber))
                        {
                            Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = "This is not a valid UM" }, MessageToken);

                            orderItem.UnitPriceByPricingUOM = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(PayloadManager.OrderPayload.Customer.RouteBranch.ToString().Split('.')[0], PayloadManager.OrderPayload.Customer.CustomerNo, orderItem.ItemNumber, (orderItem.OrderQty * orderItem.UMConversionFactor).ToString()));
                            UnitPriceUpdate = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(PayloadManager.OrderPayload.Customer.RouteBranch.ToString().Split('.')[0], PayloadManager.OrderPayload.Customer.CustomerNo, orderItem.ItemNumber, (orderItem.OrderQty * orderItem.UMConversionFactor).ToString()));
                            orderItem.UM = orderItem.UMPrice;
                            return;
                        }
                        else
                        {
                            //orderItem.ExtendedPrice = GetExtentedPrice(orderItem.OrderQty, orderItem.UnitPrice, orderItem.UM, orderItem.UMPrice, orderItem.ItemId, orderItem.ItemNumber);

                            orderItem.UnitPriceByPricingUOM = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(PayloadManager.OrderPayload.Customer.RouteBranch.ToString().Split('.')[0], PayloadManager.OrderPayload.Customer.CustomerNo, orderItem.ItemNumber, (orderItem.OrderQty * orderItem.UMConversionFactor).ToString()));
                            UnitPriceUpdate = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(PayloadManager.OrderPayload.Customer.RouteBranch.ToString().Split('.')[0], PayloadManager.OrderPayload.Customer.CustomerNo, orderItem.ItemNumber, (orderItem.OrderQty * orderItem.UMConversionFactor).ToString()));
                            orderItem.ExtendedPrice = orderItem.OrderQty * orderItem.UnitPrice;
                            return;
                        }
                    }

                    //Update On hand Qty
                    if ((orderItem.AvailableQty - orderItem.OrderQty) < 0)
                    {
                        //Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = "Order quantity exceeds On hand quantity" }, MessageToken);
                    }
                    //orderItem.AvailableQty = (orderItem.AvailableQty - orderItem.OrderQty) < 0 ? 0 : (orderItem.AvailableQty - orderItem.OrderQty);


                    //update extended price
                    //orderItem.ExtendedPrice = GetExtentedPrice(orderItem.OrderQty, orderItem.UnitPrice, orderItem.UMPrice, orderItem.UM, orderItem.ItemId, orderItem.ItemNumber);

                    orderItem.UnitPriceByPricingUOM = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(PayloadManager.OrderPayload.Customer.RouteBranch.ToString().Split('.')[0], PayloadManager.OrderPayload.Customer.CustomerNo, orderItem.ItemNumber, (orderItem.OrderQty * orderItem.UMConversionFactor).ToString()));
                    UnitPriceUpdate = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(PayloadManager.OrderPayload.Customer.RouteBranch.ToString().Split('.')[0], PayloadManager.OrderPayload.Customer.CustomerNo, orderItem.ItemNumber, (orderItem.OrderQty * orderItem.UMConversionFactor).ToString()));

                    //if (orderItem.UnitPrice != UnitPriceUpdate)
                    //{
                    //    orderItem.UnitPriceByPricingUOM = orderItem.UnitPrice; // Vivensas check and update for the price change.
                    //}
                    // Vivensas round off for extended price from row data change and total



                    orderItem.ExtendedPrice = orderItem.OrderQty * orderItem.UnitPrice;
                    TotalAmount = OrderItems.Sum(item => item.ExtendedPrice).ToString("0.00");
                    PayloadManager.OrderPayload.Amount = TotalAmount;
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][SetExtendedPrice DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });

            SearchResultTextChange = new DelegateCommand((param) =>
            {

                SearchItemsCount = Convert.ToInt32(param);
                OnPropertyChanged("SearchItemsCount");
                OnPropertyChanged("SearchResultText");
            }
            );


            // Add item from search to template list
            AddSearchItemsToOrder = new DelegateCommand((Items) =>
            {
                try
                {
                    ObservableCollection<object> selectedItems = Items as ObservableCollection<object>;
                    if (selectedItems == null || selectedItems.Count == 0)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemToAddAlert, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    int selectionCount = selectedItems.Count;
                    int TotalItemsToAdd = selectedItems.Count;
                    int ItemsNotAddedCount = 0;
                    // Dictionary<string, List<string>> templateUms = ResourceManager.GetItemUMList;
                    Dictionary<string, List<string>> templateUms = UoMManager.GetItemUMList;

                    for (int index = 0; index < selectionCount; index++)
                    {
                        //Check for Existing Item in Order list
                        if (OrderItems.Any(i => i.ItemNumber == (selectedItems[index] as Models.Item).ItemNumber))
                        {
                            ItemsNotAddedCount = ItemsNotAddedCount + 1;
                            continue;
                        }

                        Managers.OrderManager ordermanager = new Managers.OrderManager();
                        Models.OrderItem addedItem = ordermanager.ApplyPricingToItem(selectedItems[index] as Models.Item, SalesLogicExpress.Application.Managers.UserManager.UserBranch, SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
                        // Code commented as available qty is comming from database
                        //addedItem.ActualQtyOnHand = qtyOnHand.Next(5, 50);
                        addedItem.UM = Managers.UoMManager.GetDefaultSaleableUOMForItem(addedItem.ItemNumber);

                        string itemNumber = addedItem.ItemNumber;
                        addedItem.AverageStopQty = AveragerQty(itemNumber);  //returning avg value    
                        // OrderItem order = new OrderItem(new Item());
                        //addedItem.AppliedUMS = itemManager.GetAppliedUMs(addedItem.ItemNumber).Count != 0 ? itemManager.GetAppliedUMs(addedItem.ItemNumber) : new List<string>() { addedItem.UM };
                        addedItem.AppliedUMS = templateUms.ContainsKey(addedItem.ItemNumber.Trim()) ? templateUms[addedItem.ItemNumber.Trim()] : new List<string>() { addedItem.UM };
                        addedItem.InclOnDemand = true;
                        addedItem.OrderQty = 1;
                        var itemtoadd = SearchItemList.FirstOrDefault(x => string.Equals(x.ItemNumber.ToLower().Trim(), addedItem.ItemNumber.ToLower().Trim(), StringComparison.CurrentCultureIgnoreCase));
                        int AvalQty = itemtoadd.AvailableQty;
                        if (itemtoadd != null)
                        {
                            addedItem.AvailableQty = itemtoadd.AvailableQty - 1;
                            addedItem.QtyOnHand = itemtoadd.QtyOnHand;
                        }
                        OrderItems.Insert(OrderItems.Count == 0 ? 0 : OrderItems.Count, addedItem);
                        if (OrderQty.ContainsKey(addedItem.ItemNumber.Trim()))
                            OrderQty.Add(addedItem.ItemNumber.Trim(), addedItem.OrderQty);
                        else
                            OrderQty[addedItem.ItemNumber.Trim()] = addedItem.OrderQty;

                        addedItem.IsValidForOrder = addedItem.AvailableQty == 0 ? false : true;
                        if (AvalQty == 1 && addedItem.AvailableQty == 0)
                        {
                            addedItem.IsValidForOrder = true;
                            AvalQty = 0;
                        }
                        else
                            addedItem.IsValidForOrder = addedItem.AvailableQty == 0 ? false : true;
                        if (ItemOrderQtyList.All(x => x.ItemNumber.Trim() != addedItem.ItemNumber.Trim()))
                            ItemOrderQtyList.Add(addedItem.Clone());

                        SearchItemList.Remove(selectedItems[index] as Models.Item);

                        EnablePreview = OrderItems.Count == 0 ? false : true;
                        EnablePreviewOrder = EnablePreview;
                        index--;
                        selectionCount--;
                        ModelChangeArgs args = new ModelChangeArgs();
                        args.Change = ChangeType.ItemAdded;
                        args.StateChangedTime = DateTime.Now;
                        OnModelChanged(args);
                        SearchItemsCount = SearchItemList.Count;
                        OnPropertyChanged("SearchItemsCount");
                        OnPropertyChanged("SearchResultText");
                    }

                    ValidateSearch(TotalItemsToAdd, ItemsNotAddedCount);

                    //int count = Convert.ToInt32(DbEngine.ExecuteScalar(" SELECT Count(*) FROM BUSDTA.F0005 WHERE DRSY='56' AND DRRT='RB' AND DRKY in (SELECT MAPA8 FROM BUSDTA.F0150 WHERE MAAN8= " + SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo + ") "));
                    //OrderItems.ToList().ForEach(cc => cc.IsUnitPriceEditable = (count > 0 ? true : false));

                    PayloadManager.OrderPayload.Items = OrderItems.ToList();

                    OnPropertyChanged("SearchItemList");
                    OnPropertyChanged("OrderItems");
                    ResetCanvas();
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][AddSearchItemsToOrder DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });

            AddSuggestedItemsToOrder = new DelegateCommand((Items) =>
           {
               try
               {
                   ObservableCollection<object> selectedItems = Items as ObservableCollection<object>;
                   if (selectedItems == null || selectedItems.Count == 0)
                   {
                       var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemToAddAlert, MessageIcon = "Alert" };
                       Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                       return;
                   }
                   int selectionCount = selectedItems.Count;


                   for (int index = 0; index < selectionCount; index++)
                   {

                       //Check for Existing Item in Order list
                       if (OrderItems.Any(i => i.ItemNumber == (selectedItems[index] as Models.Item).ItemNumber))
                       {
                           continue;
                       }

                       Managers.OrderManager ordermanager = new Managers.OrderManager();
                       Models.OrderItem addedItem = ordermanager.ApplyPricingToItem(selectedItems[index] as Models.Item, SalesLogicExpress.Application.Managers.UserManager.UserBranch, SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
                       SuggestedItemList.Remove(selectedItems[index] as Models.Item);
                       OrderItems.Insert(0, addedItem);

                       index--;
                       selectionCount--;
                   }

                   //int count = Convert.ToInt32(DbEngine.ExecuteScalar(" SELECT Count(*) FROM BUSDTA.F0005 WHERE DRSY='56' AND DRRT='RB' AND DRKY in (SELECT MAPA8 FROM BUSDTA.F0150 WHERE MAAN8= " + SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo + ") "));
                   //OrderItems.ToList().ForEach(cc => cc.IsUnitPriceEditable = (count > 0 ? true : false));

                   OnPropertyChanged("SuggestedItemList");
                   OnPropertyChanged("OrderItems");
               }
               catch (Exception ex)
               {

                   Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][AddSuggestedItemsToOrder DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
               }
           });

            ClearSearchText = new DelegateCommand((param) =>
            {
                SearchText = "";
                SearchItems(SearchText.ToString(), true);

            });
            SearchItemOnType = new DelegateCommand((searchTerm) =>
            {
                bool flag = searchTerm.ToString().Length == 0 ? true : false;
                SearchItems(searchTerm.ToString(), flag);
            });
            SearchItem = new DelegateCommand((searchTerm) =>
            {
                SearchItems(searchTerm.ToString(), true);
            });
            PreviewOrder = new DelegateCommand((o) =>
           {
               try
               {
                   //Sathish Added Busy cursor once preview button is clicked
                   IsBusy = true;
                   bool itemChanged = false;
                   var orderItem = OrderItems.Where(s => s.EnablePreviewOrder == false && s.IsUnitPriceDirty == true).ToList();
                   if (orderItem.Count != 0)
                       itemChanged = orderItem.ToList().Any(a => a.ReasonCode <= 1);
                   if (!itemChanged)
                   {
                       ItemManager itemmgr = new ItemManager();
                       bool IsAnyItemUnAvailable = OrderItems.Any(item => !item.IsValidForOrder);
                       bool OtherItemsAreZeroCounted = OrderItems.Count(i => !i.IsValidForOrder && Convert.ToInt16(itemmgr.ValidateNonStockItem(i.ItemNumber)) == 0) != OrderItems.Count(item => !item.IsValidForOrder && item.OrderQty == 0);
                       if (OtherItemsAreZeroCounted)
                       {
                           var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.ItemNotAvailable, MessageIcon = "Alert" };
                           Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                           IsBusy=false;
                           return;
                       }
                       //Sathish Added ths function this takes care to show busy cursor.
                       ExecutePreviewButtonClick();
                       PreviewOrderItems();//On Cash on delivery customer 2nd sales order cant able to navigate so changing out of the async fun.
                   }
                   IsBusy = false;
               }
               catch (Exception ex)
               {

                   Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][PreviewOrder DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                   IsBusy = false;
               }
           });                

            ResetOrder = new DelegateCommand((Items) =>
            {
                try
                {

                    ObservableCollection<object> selectedItems = Items as ObservableCollection<object>;
                    if (selectedItems == null || selectedItems.Count == 0)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemForPriceResetAlert, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.PriceResetItemConfirmation, MessageIcon = "Alert", Confirmed = false };
                    Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                    if (!confirmMessage.Confirmed) return;

                    int selectionCount = selectedItems.Count;
                    for (int index = 0; index < selectionCount; index++)
                    {
                        Models.OrderItem orderItem = selectedItems[index] as Models.OrderItem;
                        //****
                        //OrderItem orderItem = selectedItem as OrderItem;

                        PricingManager pricingManager = new PricingManager();
                        Decimal UnitPriceUpdate = 0;

                        //if (orderItem.UM != orderItem.UMPrice)
                        //{
                        //    if (!IsValidUM(orderItem.UM, orderItem.ItemId, orderItem.UMPrice, orderItem.ItemNumber))
                        //    {
                        //        Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = "This is not a valid UM" }, MessageToken);

                        //        orderItem.UnitPriceByPricingUOM = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(PayloadManager.OrderPayload.Customer.RouteBranch.ToString().Split('.')[0], PayloadManager.OrderPayload.Customer.CustomerNo, orderItem.ItemNumber, orderItem.OrderQty.ToString()));
                        //        UnitPriceUpdate = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(PayloadManager.OrderPayload.Customer.RouteBranch.ToString().Split('.')[0], PayloadManager.OrderPayload.Customer.CustomerNo, orderItem.ItemNumber, orderItem.OrderQty.ToString()));
                        //        orderItem.UM = orderItem.UMPrice;

                        //    }
                        //    else
                        //    {
                        //        //orderItem.ExtendedPrice = GetExtentedPrice(orderItem.OrderQty, orderItem.UnitPrice, orderItem.UM, orderItem.UMPrice, orderItem.ItemId, orderItem.ItemNumber);

                        //        orderItem.UnitPriceByPricingUOM = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(PayloadManager.OrderPayload.Customer.RouteBranch.ToString().Split('.')[0], PayloadManager.OrderPayload.Customer.CustomerNo, orderItem.ItemNumber, orderItem.OrderQty.ToString()));
                        //        UnitPriceUpdate = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(PayloadManager.OrderPayload.Customer.RouteBranch.ToString().Split('.')[0], PayloadManager.OrderPayload.Customer.CustomerNo, orderItem.ItemNumber, orderItem.OrderQty.ToString()));
                        //        orderItem.ExtendedPrice = orderItem.OrderQty * orderItem.UnitPrice;

                        //    }
                        //}

                        orderItem.UnitPriceByPricingUOM = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(PayloadManager.OrderPayload.Customer.RouteBranch.ToString().Split('.')[0], PayloadManager.OrderPayload.Customer.CustomerNo, orderItem.ItemNumber, (orderItem.OrderQty * orderItem.UMConversionFactor).ToString()));
                        //UnitPriceUpdate = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(PayloadManager.OrderPayload.Customer.RouteBranch.ToString().Split('.')[0], PayloadManager.OrderPayload.Customer.CustomerNo, orderItem.ItemNumber, (orderItem.OrderQty * orderItem.UMConversionFactor).ToString()));

                        //if (orderItem.UnitPrice != UnitPriceUpdate)
                        //{
                        //    orderItem.UnitPriceByPricingUOM = UnitPriceUpdate; // Vivensas check and update for the price change.
                        //}
                        // Vivensas round off for extended price from row data change and total
                        //orderItem.ExtendedPrice = orderItem.OrderQty * orderItem.UnitPriceByPricingUOM;
                        TotalAmount = OrderItems.Sum(item => item.ExtendedPrice).ToString("0.00");
                        PayloadManager.OrderPayload.Amount = TotalAmount;


                        //***
                        orderItem.UnitPrice = OrderManager.GetPriceByUomFactor(orderItem.ItemId, orderItem.UMPrice, orderItem.UM, orderItem.UnitPriceByPricingUOM);
                        orderItem.ExtendedPrice = orderItem.OrderQty * orderItem.UnitPrice;
                        orderItem.IsUnitPriceDirty = false;
                        orderItem.ReasonCode = 0;
                        orderItem.ResonCodeDescription = "";
                        orderItem.PriceOverrideFlag = "0";

                    }
                    OnPropertyChanged("TemplateItemsCount");
                    OnPropertyChanged("OrderItems");

                    ResetCanvas();
                    selectedItems.Clear();
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][ResetOrder DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });

            DeleteItemFromOrder = new DelegateCommand((Items) =>
            {
                ObservableCollection<object> selectedItems = Items as ObservableCollection<object>;
                try
                {

                    if (selectedItems == null || selectedItems.Count == 0)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemForDeleteAlert, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.DeleteItemConfirmation, MessageIcon = "Alert", Confirmed = false };
                    Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                    if (!confirmMessage.Confirmed) return;
                    int selectionCount = selectedItems.Count;
                    List<OrderItem> items = new List<OrderItem>();
                    for (int index = 0; index < selectionCount; index++)
                    {
                        Models.OrderItem OrderItemObj = selectedItems[index] as Models.OrderItem;
                        items.Add(OrderItemObj);
                        if (OrderQty.ContainsKey(OrderItemObj.ItemNumber.Trim()))
                            OrderQty.Remove(OrderItemObj.ItemNumber.Trim());
                        OrderItemObj.IsUnitPriceDirty = false;
                        this.EnablePreview = true;
                        System.Windows.Input.CommandManager.InvalidateRequerySuggested();
                        OrderItems.Remove(selectedItems[index] as Models.OrderItem);
                        Models.OrderItem refItem = ItemOrderQtyList.FirstOrDefault(i => i.ItemId.Trim() == OrderItemObj.ItemId.Trim());
                        if (refItem != null)
                        {
                            ItemOrderQtyList.Remove(refItem);
                        }
                        index--;
                        selectionCount--;
                    }
                    //new InventoryManager().UpdateCommittedQuantity(items, true);
                    //EnablePreview = OrderItems.Count == 0 ? false : true;
                    OnPropertyChanged("TemplateItemsCount");
                    OnPropertyChanged("OrderItems");
                    EnablePreviewOrder = OrderItems.Count != 0 ? true : false;
                    //**********************************************************************************************************
                    ResetCanvas();
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][DeleteItemFromOrder DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                //ViewModels.PreviewOrder.GlobalInkCanvas = new System.Windows.Controls.InkCanvas();

            });

            OpenExpander = new DelegateCommand((param) =>
            {
                switch (Convert.ToInt32(param))
                {
                    case 0:
                        if (!ToggleExpander)
                        {
                            ToggleExpander = true;
                        }
                        break;
                    case 1:
                        if (!ToggleExpander)
                        {
                            ToggleExpander = true;
                        }
                        break;
                    case 2:
                        if (!ToggleExpander)
                        {
                            ToggleExpander = true;
                        }
                        break;
                    case 3:
                        if (!ToggleExpander)
                        {
                            ToggleExpander = true;
                        }
                        break;
                    default:
                        ToggleExpander = false;
                        break;

                }

            });
            BeginEditDelegateCommand = new DelegateCommand((item) =>
            {
                string columnName = (((Telerik.Windows.Controls.GridView.GridViewCellBase)(item)).Column).UniqueName;
                switch (columnName)
                {
                    case "UnitPrice":
                        OldValue = Convert.ToDecimal(((Telerik.Windows.Controls.GridView.GridViewCell)(item)).Value);
                        break;
                    case "OrderQty":
                        OldOrderQtyValue = Convert.ToInt32(((Telerik.Windows.Controls.GridView.GridViewCell)(item)).Value);
                        break;
                    default:
                        break;
                }
            });

            CellEditEndedDelegateCommand = new DelegateCommand((item) =>
            {

                //Vivensas passed CalculateAggregates for each column change
                try
                {

                    GridViewRowItem objParentRow = ((Telerik.Windows.Controls.GridView.GridViewCellBase)(item)).ParentRow;
                    OrderItem orderItemObj = ((OrderItem)(objParentRow.DataContext));

                    string columnName = (((Telerik.Windows.Controls.GridView.GridViewCellBase)(item)).Column).UniqueName;
                    switch (columnName)
                    {
                        case "UnitPrice":
                            NewValue = orderItemObj.UnitPrice;
                            if (OldValue != NewValue)
                            {
                                int count = Convert.ToInt32(DbEngine.ExecuteScalar(" SELECT Count(*) FROM BUSDTA.F0005 WHERE DRSY='56' AND DRRT='RB' AND DRKY in (SELECT MAPA8 FROM BUSDTA.F0150 WHERE MAAN8= " + SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo + ") "));
                                if (count > 0)
                                {

                                    orderItemObj.IsUnitPriceDirty = true;
                                    this.IsPriceOverrideColumnEnabled = true;
                                    // If Price Changes
                                    orderItemObj.PriceOverrideFlag = "1";
                                    orderItemObj.ResonCodeDescription = "";
                                    orderItemObj.ReasonCode = 0;

                                    if (NewValue == 0)
                                    {
                                        orderItemObj.OverrideReasonCodeList = orderManager.GetPriceOvrCodes(orderItemObj.UnitPrice == 0 ? "1" : "0");
                                    }
                                    else if (NewValue != 0)
                                    {
                                        orderItemObj.UnitPrice = 0;
                                        orderItemObj.ExtendedPrice = 0;

                                        orderItemObj.OverrideReasonCodeList = orderManager.GetPriceOvrCodes(orderItemObj.UnitPrice == 0 ? "1" : "0");

                                        var alertMessage = new Helpers.AlertWindow { Message = "For Rebate Customer Price Must be a Zero or Unit Price", MessageIcon = "Alert" };
                                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                                    }
                                }
                                else
                                {
                                    orderItemObj.IsUnitPriceDirty = true;
                                    this.IsPriceOverrideColumnEnabled = true;
                                    // If Price Changes
                                    orderItemObj.PriceOverrideFlag = "1";
                                    orderItemObj.ResonCodeDescription = "";
                                    orderItemObj.ReasonCode = 0;
                                    orderItemObj.OverrideReasonCodeList = orderManager.GetPriceOvrCodes(orderItemObj.UnitPrice == 0 ? "1" : "0");
                                }
                            }
                            else
                            {
                                orderItemObj.PriceOverrideFlag = "0";
                            }

                            if (orderItemObj.IsUnitPriceDirty)
                            {
                                this.EnablePreview = false;
                            }
                            else
                            {
                                this.EnablePreview = true;

                                //Reset reason code to blank if reset to blank 
                                if (orderItemObj.ReasonCode > 1)
                                    orderItemObj.ReasonCode = 0;
                            }
                            //Recalculate the extn. price total column group footer
                            objParentRow.GridViewDataControl.CalculateAggregates();
                            break;
                        case "OrderQty":
                            NewOrderQtyValue = orderItemObj.OrderQty;
                            objParentRow.GridViewDataControl.CalculateAggregates();
                            break;
                       // Issue ID - 102 - Override the Price in Create Order Screen Select Reason Code in Create order Screen click to Preview Screen and moving back to Create Order Screen then tried to Change the Order UM but did not changed. Unit price changed Correctly but still reason Code Dispalying in create order screen then forward to preview order screen but reason code is showing as Zero - Fixed by Zakir
                        case "AppliedUMS":
                            orderItemObj.ResonCodeDescription = null;
                            orderItemObj.ItemReasonCode = 0;
                            objParentRow.GridViewDataControl.CalculateAggregates();
                            break;
                        case "PriceOVR":
                            if (orderItemObj.IsUnitPriceDirty && orderItemObj.ReasonCode > 1)
                            {
                                //this.IsPriceOverrideColumnEnabled = false;
                                this.EnablePreview = true;
                                this.IsPriceOverrideColumnEnabled = true;

                                // If Price Changes
                                orderItemObj.PriceOverrideFlag = "1";
                            }
                            else
                            {

                                orderItemObj.PriceOverrideFlag = "0";
                            }
                            objParentRow.GridViewDataControl.CalculateAggregates();
                            break;
                        default:
                            objParentRow.GridViewDataControl.CalculateAggregates();
                            break;
                    }
                    objParentRow.GridViewDataControl.CalculateAggregates();
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][CellEditEndedDelegateCommand DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });

            SelectionChangedCommand = new DelegateCommand((param) =>
                {
                    if (param != null)
                    {
                        OrderItem orderItemObj = ((OrderItem)(((Telerik.Windows.Controls.GridView.GridViewCellBase)(param)).ParentRow.DataContext));

                        if (orderItemObj.IsUnitPriceDirty)
                        {
                            if (orderItemObj.ReasonCode > 1)
                                this.EnablePreview = true;
                            else
                                this.EnablePreview = false;

                            this.IsPriceOverrideColumnEnabled = true;
                        }
                        else
                        {
                            this.EnablePreview = true;
                            this.IsPriceOverrideColumnEnabled = false;
                        }
                    }
                });

            PriceOverrideChange = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    List<object> parameters = param as List<object>;
                    SelectedItem = parameters[1] as OrderItem;
                    var changedUM = parameters[0] as PriceOvrCodes;
                    if (SelectedItem != null && changedUM != null)
                    {
                        SelectedItem.ResonCodeDescription = changedUM.Name;
                    }
                    //clear signature
                    this.ResetCanvas();
                    this.ResetSign();
                }
            });

            OrderUMChange = new DelegateCommand((param) =>
            {

                try
                {
                    if (param != null)
                    {
                        List<object> parameters = param as List<object>;
                        SelectedItem = parameters[1] as OrderItem;
                        string changedUM = parameters[0].ToString();
                        if (SelectedItem != null)
                        {
                            PricingManager pricingManager = new PricingManager();
                            InventoryManager.ItemQty itemQty = new InventoryManager().GetItemQuantity(SelectedItem.ItemId);
                            SelectedItem.QtyOnHand = itemQty.OnHandQty;
                            SelectedItem.UMConversionFactor = new TemplateManager().SetConversionFactorForItem(SelectedItem.PrimaryUM, changedUM, SelectedItem.ItemId, SelectedItem.ItemNumber);
                            SelectedItem.UnitPriceByPricingUOM = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(PayloadManager.OrderPayload.Customer.RouteBranch.ToString().Split('.')[0], PayloadManager.OrderPayload.Customer.CustomerNo, SelectedItem.ItemNumber, (SelectedItem.OrderQty * SelectedItem.UMConversionFactor).ToString()));
                            int availQty = SelectedItem.AvailableQty = itemQty.OnHandQty - itemQty.ComittedQty - itemQty.HeldQuantity + Convert.ToInt32((SelectedItem.LastComittedQty - (SelectedItem.OrderQty * SelectedItem.UMConversionFactor)));
                            SelectedItem.IsValidForOrder = availQty < 0 ? false : true;
                            SelectedItem.AvailableQty = availQty;
                            SelectedItem.UM = changedUM;
                            //Issue ID - 101 - Fixed by Zakir
                            SelectedItem.ResonCodeDescription = null;
                            SelectedItem.UnitPrice = OrderManager.GetPriceByUomFactor(SelectedItem.ItemId, SelectedItem.UMPrice, SelectedItem.UM, SelectedItem.UnitPriceByPricingUOM);
                            //Vivensas math round off for the calculation
                            SelectedItem.ExtendedPrice = decimal.Parse(SelectedItem.OrderQty.ToString()) * Math.Round(SelectedItem.UnitPrice, 2, MidpointRounding.AwayFromZero);

                            if (SelectedItem.IsUnitPriceDirty)
                            {
                                var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.PriceRecalculated, MessageIcon = "Alert" };
                                Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                            }
                        }
                        //clear signature
                        this.ResetCanvas();
                        this.ResetSign();
                    }
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][OrderUMChange DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            UpdateAvailableQty = new DelegateCommand((param) =>
            {
                try
                {

                    if (param != null)
                    {
                        SelectedItem = param as OrderItem;
                        var Deleteorderitem = OrderItems.Where(a => a.ItemId.Trim() == SelectedItem.ItemId.Trim()).ToList();
                        if (Deleteorderitem.Count > 0 && Deleteorderitem != null)
                        {

                            InventoryManager.ItemQty itemQty = new InventoryManager().GetItemQuantity(SelectedItem.ItemId);
                            SelectedItem.QtyOnHand = itemQty.OnHandQty;
                            SelectedItem.UMConversionFactor = new TemplateManager().SetConversionFactorForItem(SelectedItem.PrimaryUM, SelectedItem.UM, SelectedItem.ItemId, SelectedItem.ItemNumber);
                            int availQty = SelectedItem.AvailableQty = itemQty.OnHandQty - itemQty.ComittedQty - itemQty.HeldQuantity + Convert.ToInt32((SelectedItem.LastComittedQty - (SelectedItem.OrderQty * SelectedItem.UMConversionFactor)));
                            SelectedItem.IsValidForOrder = availQty < 0 ? false : true;
                            SelectedItem.AvailableQty = availQty;
                            bool lsDeleteItem = false;
                            if (SelectedItem.OrderQty == 0 || string.IsNullOrEmpty(SelectedItem.OrderQty.ToString()))
                            {
                                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.DeleteItemConfirmation, MessageIcon = "Alert", Confirmed = false };
                                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                                if (confirmMessage.Confirmed)
                                {
                                    lsDeleteItem = true;
                                    foreach (var Itemdelete in Deleteorderitem)
                                    {
                                        OrderItems.Remove(Itemdelete);
                                    }
                                }
                                else
                                {
                                    SelectedItem.OrderQty = 1;
                                }
                            }

                            if (!lsDeleteItem)
                            {
                                // User Issue ID 1: Create Order - When changing the order qty, below added codes for unit price calculation from pricing manager. Based on the changes, it wil affect the unit price, 
                                PricingManager pricingManager = new PricingManager();
                                //Decimal UnitPriceUpdate = 0;
                                if (!SelectedItem.IsUnitPriceDirty)
                                {
                                    SelectedItem.UnitPriceByPricingUOM = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(PayloadManager.OrderPayload.Customer.RouteBranch.ToString().Split('.')[0], PayloadManager.OrderPayload.Customer.CustomerNo, SelectedItem.ItemNumber, (SelectedItem.OrderQty * SelectedItem.UMConversionFactor).ToString()));

                                    //if (SelectedItem.UnitPrice != UnitPriceUpdate)
                                    //{
                                    //    SelectedItem.UnitPriceByPricingUOM = UnitPriceUpdate;
                                    //}
                                }
                                if (!SelectedItem.IsUnitPriceDirty)
                                {
                                    //Vignesh.S Change for TUI Issue :250 (01/02/2017)
                                    SelectedItem.UnitPrice = OrderManager.GetPriceByUomFactor(SelectedItem.ItemId, SelectedItem.UMPrice, SelectedItem.UM, SelectedItem.UnitPriceByPricingUOM);
                                    if (SelectedItem.ReasonCode > 0)
                                    {
                                        SelectedItem.ReasonCode = 0;
                                        SelectedItem.ResonCodeDescription = "";
                                        SelectedItem.PriceOverrideFlag = "0";
                                    }
                                }


                                SelectedItem.ExtendedPrice = decimal.Parse(SelectedItem.OrderQty.ToString()) * Math.Round(SelectedItem.UnitPrice, 2, MidpointRounding.AwayFromZero);
                                TotalAmount = OrderItems.Sum(item => item.ExtendedPrice).ToString("0.00");
                                PayloadManager.OrderPayload.Amount = TotalAmount;
                            }
                        }
                        OnPropertyChanged("TemplateItemsCount");
                        OnPropertyChanged("OrderItems");
                        ResetCanvas();

                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][UpdateAvailableQty DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][End:InitializeCommands]");

        }
        Dictionary<string, int> OrderQty = new Dictionary<string, int>();
        void AdjustOrderQty(List<OrderItem> Items)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][Strat:AdjustOrderQty]");
            foreach (OrderItem item in Items)
            {
                if (OrderQty.ContainsKey(item.ItemNumber.Trim()))
                {
                    OrderQty[item.ItemNumber.Trim()] = item.OrderQty;
                }
                else
                {
                    OrderQty.Add(item.ItemNumber.Trim(), item.OrderQty);
                }
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][End:AdjustOrderQty]");
        }
        private void ResetCanvas()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][Strat:ResetCanvas]");
            ModelChangeArgs args = new ModelChangeArgs();
            args.StateChangedTime = DateTime.Now;
            args.Change = ChangeType.ResetCanvas;
            OnModelChanged(args);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][End:ResetCanvas]");
        }

        
        private void SaveOrder(int CustomerId, ObservableCollection<OrderItem> OrderItems)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][Start:SaveOrder]");        
            orderManager.SaveOrder(Order.OrderId, CustomerId, OrderItems);           
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][End:SaveOrder]");
        }

        private void PreviewOrderItems()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][Start:PreviewOrderItems]");
            try
            {                
                PayloadManager.OrderPayload.Items = OrderItems.Where(item => item.OrderQty != 0).ToList();
                var json = PayloadManager.OrderPayload.SerializeToJson();
                //ResourceManager.Transaction.AddTransactionInQueueForSync(SalesLogicExpress.Application.Managers.Transaction.SaveOrder, SyncQueueManager.Priority.immediate);
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PreviewOrder, CurrentViewName = ViewModelMappings.View.Order, CloseCurrentView = false, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                #region UpdateCommittedQty
                // Log commited qty to inventory_ledger
                //var objInv = new Managers.InventoryManager();
                //if (PayloadManager.OrderPayload.Items != null)
                //    objInv.UpdateCommittedQuantity(PayloadManager.OrderPayload.Items.ToList(), "accept");
                #endregion
                IsBusy = false;
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][PreviewOrderItems][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][End:PreviewOrderItems]");
        }
        CancellationTokenSource tokenForCancelTask = new CancellationTokenSource();

        public async Task SearchItems(string searchTerm, bool skipMinLengthCheck)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][asyncStart:SearchItems]");
            await Task.Run(() =>
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    try
                    {
                        if (!skipMinLengthCheck)
                        {
                            if (searchTerm != null && searchTerm.ToString().Length < 3)
                            {
                                return;
                            }
                            if (searchTerm != null && searchTerm.ToString().Length == 0)
                            {
                                SearchItemList.Clear();
                                this.OnPropertyChanged("SearchItemsCount");
                                return;
                            }
                        }
                        SearchItemList.Clear();
                        SearchItemList = itemManager.SearchItem(searchTerm.ToString(), SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
                        SearchItemsCount = SearchItemList.Count;
                        this.OnPropertyChanged("SearchItemsCount");
                        this.OnPropertyChanged("SearchResultText");
                    }
                    catch (Exception ex)
                    {

                        Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][SearchItems][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                }
                ));
            }, tokenForCancelTask.Token);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][Order][asyncEnd:PreviewOrderItems]");
        }

        //Sathish Added ExecutePreviewEnd Function for showing Busy cursor once preview button is clicked
        async void ExecutePreviewButtonClick()
        {
            await Task.Run(() =>
            {
                try
                {
                    IsBusy = true;
                    SaveOrder(Convert.ToInt32(PayloadManager.OrderPayload.Customer.CustomerNo), OrderItems);                  
                    IsBusy = false;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][SearchItems][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
        }


        private void CalulateTotal(ObservableCollection<OrderItem> OrderCollection)
        {
            // throw new NotImplementedException();
        }

        public string Error
        {
            get
            {
                return null;
                //throw new NotImplementedException();
            }
        }

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "":
                        break;

                    default:
                        break;
                }

                return null;
            }
        }

        private bool _IsPriceOverrideColumnEnabled = false;

        public bool IsPriceOverrideColumnEnabled
        {
            get { return _IsPriceOverrideColumnEnabled; }

            set
            {
                _IsPriceOverrideColumnEnabled = value;
                OnPropertyChanged("IsPriceOverrideColumnEnabled");
            }
        }


        public DelegateCommand SetUnitPrice { get; set; }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }


    public class OpenOrder
    {
        public string OrderId { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }

        public int OrderQty { get; set; }
        public int PickQty { get; set; }

        public string PrimaryUM { get; set; }

    }

}
