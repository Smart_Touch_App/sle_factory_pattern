﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using System.Threading;
using System.Data;
using SalesLogicExpress.Domain;
using System.ComponentModel;
using log4net;
using System.Windows.Threading;
using SalesLogicExpress.Application.Managers;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;

namespace SalesLogicExpress.Application.ViewModels
{
    public class OrderTemplate : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.OrderTemplate");
        CommonNavInfo navInfo = new CommonNavInfo();
        OrderManager objOrderManger = new OrderManager();

        private TrulyObservableCollection<Models.OrderItem> _OrderItems;
        public event EventHandler<ModelChangeArgs> ModelChanged;
        protected virtual void OnModelChanged(ModelChangeArgs e)
        {
            EventHandler<ModelChangeArgs> handler = ModelChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public class ModelChangeArgs : EventArgs
        {
            public ChangeType Change { get; set; }
            public DateTime StateChangedTime { get; set; }
        }
        #region Action Commands exposed to View
        public DelegateCommand AddItemToTemplate { get; set; }
        public DelegateCommand ClearSearchText { get; set; }
        public DelegateCommand OrderItemSelectionChanged { get; set; }
        public DelegateCommand SearchItem { get; set; }
        public DelegateCommand SearchItemOnType { get; set; }
        public DelegateCommand SaveTemplate { get; set; }
        public DelegateCommand SavePreOrder { get; set; }
        public DelegateCommand CreateOrder { get; set; }
        public DelegateCommand DeleteItemFromTemplate { get; set; }
        public DelegateCommand LoadTemplateWithUsualQuantity { get; set; }
        public DelegateCommand LoadTemplateWithPreOrderQuantity { get; set; }
        public DelegateCommand ResetBackTemplateQuantity { get; set; }
        public DelegateCommand OpenExpander { set; get; }
        public DelegateCommand UpdateTemplateItemsFromOrder { set; get; }
        public TrulyObservableCollection<Models.OrderItem> OrderItems
        {
            get
            {
                return this._OrderItems;
            }
            set
            {
                _OrderItems = value;
                OnPropertyChanged("OrderItems");

            }
        }

        private Dictionary<string, int> ItemOrderQtyDictionary = new Dictionary<string, int>();

        public DelegateCommand SearchResultTextChange { set; get; }
        public DelegateCommand GridCellEditEnded { get; set; }
        public DelegateCommand BeginGridCellEdit { get; set; }
        public DelegateCommand UpdateAvailableQty { get; set; }

        public DelegateCommand OrderUMChange { get; set; }

        private Dictionary<string, string> OrderQuantityMap = new Dictionary<string, string>();
        private Dictionary<string, string> OrderPriceMap = new Dictionary<string, string>();
        CancellationTokenSource tokenSource = new CancellationTokenSource();
        //private string OldSeqNo;
        bool CheckIsEditEnded = true;
        bool _SearchResultTextVisibility = false;

        public bool isNumericIncDecAction = false;
        #endregion

        #region Properties exposed to View
        //Timer searchTimer;
        private delegate void Method();
        Managers.TemplateManager templateManager;
        Managers.ItemManager itemManager;
        public Guid MessageToken { get; set; }
        public TemplateItem SelectedItem { get; set; }
        private bool _IsTemplateForOrder = true;
        //private int OldOrderqty = 0;
        //private string previousItemNumber = "";
        public bool IsTemplateForOrder
        {
            get
            {
                return _IsTemplateForOrder;
            }
            set
            {
                _IsTemplateForOrder = value;
                OnPropertyChanged("IsTemplateForOrder");
            }
        }

        #region Code commented as it is moved to BaseViewModel

        //string _SearchText;
        //public string SearchText
        //{
        //    get { return _SearchText; }
        //    set { _SearchText = value; OnPropertyChanged("SearchText"); }
        //}

        #endregion

        private CommonNavInfo _NavigationInfo;
        public CommonNavInfo NavigationInfo
        {
            get
            {
                return _NavigationInfo = new CommonNavInfo();
            }
            set
            {
                _NavigationInfo = value;
            }
        }
        public List<int> PreviousSequenceNumbers
        {
            get { return _previousSequenceNumbers; }
            set { _previousSequenceNumbers = value; }
        }

        private string _OrderNumber;
        private List<int> _previousSequenceNumbers;
        public string OrderNumber
        {
            get { return _OrderNumber; }
            set
            {
                if (value != _OrderNumber)
                {
                    _OrderNumber = value;
                    OnPropertyChanged("OrderNumber");
                }
            }
        }
        private bool _CloseDropdown;
        public bool CloseDropdown
        {
            get { return _CloseDropdown; }
            set { _CloseDropdown = value; OnPropertyChanged("CloseDropdown"); }
        }
        private string _SearchResultText;
        public string SearchResultText
        {
            get
            {
                if (SearchItemsCount == 0)
                {
                    return "No items found";
                }
                else
                {
                    SearchResultTextVisibility = true;
                    OnPropertyChanged("SearchResultTextVisibility");
                    return "Items found " + SearchItemsCount;
                }
            }
            set
            {
                if (value != _SearchResultText)
                {
                    _SearchResultText = value;
                    OnPropertyChanged("SearchResultText");
                }
            }
        }
        public bool SearchResultTextVisibility
        {
            get
            {
                return _SearchResultTextVisibility;
            }
            set
            {
                _SearchResultTextVisibility = value;
            }
        }
        private bool _IsBusy;
        public bool IsBusy
        {
            get
            {
                return _IsBusy;
            }
            set
            {
                _IsBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        private bool _ToggleExpander = false;
        public bool ToggleExpander
        {
            get { return _ToggleExpander; }
            set
            {
                _ToggleExpander = value;
                OnPropertyChanged("ToggleExpander");
            }
        }

        bool _IsSearching = false;
        public bool IsSearching
        {
            get
            {
                return _IsSearching;
            }
            set
            {


                _IsSearching = value;
                OnPropertyChanged("IsSearching");
            }
        }

        private ObservableCollection<Models.Item> _SearchItems = new ObservableCollection<Models.Item>();
        public ObservableCollection<Models.Item> SearchItemList
        {
            get
            {
                return this._SearchItems;
            }
            set
            {
                try
                {
                    this._SearchItems = value;
                    OnPropertyChanged("SearchItemList");
                    OnPropertyChanged("SearchResultText");
                }
                catch (Exception)
                {

                }

            }
        }

        private ObservableCollection<Models.TemplateItem> _TemplateItems = new ObservableCollection<TemplateItem>();
        public ObservableCollection<Models.TemplateItem> TemplateItems
        {
            get
            {
                return this._TemplateItems;
            }
            set
            {
                _TemplateItems = value;
                OnPropertyChanged("TemplateItems");
            }
        }
        public int TemplateItemsCount
        {
            get
            {
                if (this._TemplateItems != null) return this._TemplateItems.Count;
                return 0;
            }
            set
            {
                OnPropertyChanged("TemplateItemsCount");
            }
        }
        private int _SearchItemsCount;

        public int SearchItemsCount
        {
            get
            {

                return _SearchItemsCount;
            }
            set
            {
                _SearchItemsCount = value;
                OnPropertyChanged("SearchItemsCount");
                OnPropertyChanged("SearchResultText");
            }
        }
       // private Models.Customer _Customer;
        public Models.Customer Customer
        {
            get;
            set;
        }
        private DataTable _OrderHistory;
        private DataTable _OrderHistoryHeaders;
        
        public DataTable OrderHistory
        {
            get { return _OrderHistory; }
            set { _OrderHistory = value; OnPropertyChanged("OrderHistory"); }
        }

        public DataTable OrderHistoryHeaders
        {
            get { return _OrderHistoryHeaders; }
            set { _OrderHistoryHeaders = value; OnPropertyChanged("OrderHistoryHeaders"); }
        }
        // Newly added by Hari
        private DataTable _MetricCustHistDemand;
        public DataTable MetricCustHistDemand
        {
            get { return _MetricCustHistDemand; }
            set { _MetricCustHistDemand = value; OnPropertyChanged("MetricCustHistDemand"); }
        }
        //
        #endregion
        public void ResetSign()
        {
            new CanvasViewModel(Flow.OrderLifeCycle, SignType.OrderSign, Order.OrderId).ResetSignFromDB();
        }
        public OrderTemplate()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][Start:SaveOrder]");
            try
            {
                if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.CustomerHome_Activity.ToString())
                {
                    SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Activity;
                }
                else if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.CustomerHome_DashBoard.ToString())
                {
                    SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Dashboard;
                }

                Customer = PayloadManager.OrderPayload.Customer;
                Initialize();
                if (SalesLogicExpress.Application.ViewModels.CommonNavInfo.NavigationParameter != null)
                {
                    IsTemplateForOrder = Convert.ToBoolean(SalesLogicExpress.Application.ViewModels.CommonNavInfo.NavigationParameter);
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.NavigationParameter = null;
                }
                IsBusy = true;
                //TODO:remove this after making any changes in order then only make IsDirty = true
                //if (IsTemplateForOrder)
                //{
                //    IsDirty = true;
                //}

                CheckForOrderCreation();

                HoldVoidMessage();

                GetTemplateItems();
                GetSearchItems();

                // GetOrderHistory();
            }
            catch (Exception ex)
            {
                Logger.Error("Error in OrderTemplate : " + ex.Message.ToString());
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][End:OrderTemplate]");
        }

        async void GetSearchItems()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][AsyncStart:GetSearchItems]");
            await Task.Run(() =>
            {
                try
                {
                    IsSearching = true;
                    SearchResultText = string.Empty;
                    ObservableCollection<Item> items = itemManager.SearchItem(string.Empty, Customer.CustomerNo);
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        SearchItemList = items;
                    }));
                    IsSearching = false;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderTemplate][GetSearchItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][AsyncEnd:GetSearchItems]");
        }
        Dictionary<string, int> OrderQty = new Dictionary<string, int>();
        async void GetTemplateItems()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][AsyncStart:GetTemplateItems]");
            await Task.Run(() =>
            {

                try
                {
                    // Get Order headers
                    //**********************************************************************************************************
                    Managers.PreOrderManager preOrderManager = new Managers.PreOrderManager();

                    // Get Templates
                    //**********************************************************************************************************
                    ObservableCollection<TemplateItem> PreOrderItems = new ObservableCollection<TemplateItem>();

                    TemplateItems = templateManager.GetTemplateItemsForCustomer(Customer.CustomerNo);
                    TemplateItems = TemplateItems.GroupBy(x => x.ItemId).Select(y => y.First()).ToObservableCollection<TemplateItem>();
                    foreach (TemplateItem item in TemplateItems)
                    {
                        if (!OrderQty.ContainsKey(item.ItemNumber.Trim()))
                            OrderQty.Add(item.ItemNumber.Trim(), item.OrderQty);
                    }
                    PreOrderItems = preOrderManager.GetPreOrderItems(Customer.CustomerNo, ServiceRoute.SelectedCalendarDate);
                    preOrderManager.Dispose();
                    preOrderManager = null;
                    foreach (TemplateItem item in TemplateItems)
                    {
                        if (PreOrderItems.Any(x => x.ItemNumber == item.ItemNumber))
                        {
                            Models.TemplateItem i = PreOrderItems.FirstOrDefault(x => x.ItemNumber == item.ItemNumber);
                            item.OrderQty = PreOrderItems.FirstOrDefault(x => x.ItemNumber == item.ItemNumber).PreOrderQty;
                            item.PreOrderQty = PreOrderItems.FirstOrDefault(x => x.ItemNumber == item.ItemNumber).PreOrderQty;
                            item.UM = PreOrderItems.FirstOrDefault(x => x.ItemNumber == item.ItemNumber).UM;
                            PreOrderItems.Remove(i);
                        }
                        if (!ItemOrderQtyDictionary.ContainsKey(item.ItemNumber.Trim()))
                        {
                            ItemOrderQtyDictionary.Add(item.ItemNumber.Trim(), item.AvailableQty);
                        }
                    }

                    foreach (Models.TemplateItem item in PreOrderItems)
                    {
                        item.OrderQty = item.PreOrderQty;
                        item.InclOnTmplt = false;
                        item.SelectedUM = item.UM;
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                          {
                              TemplateItems.Add(item);

                          }));
                    }

                    UpdateOrderTemplateItems();
                    IsBusy = false;

                    Managers.OrderManager orderManager = new Managers.OrderManager();
                    DataSet RowAndHeader = null;
                    RowAndHeader = orderManager.GetOrderHistoryForCustomer(SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
                    if (RowAndHeader != null && RowAndHeader.Tables.Count > 0)
                    {
                        OrderHistory = RowAndHeader.Tables[0];
                        OrderHistoryHeaders = RowAndHeader.Tables[1];
                        MetricCustHistDemand = RowAndHeader.Tables[2];
                    }
                    orderManager.Dispose();
                    orderManager = null;

                    AppendOrderHistory(RowAndHeader);
                    RowAndHeader.Dispose();
                    RowAndHeader = null;
                    foreach (Models.TemplateItem item in this._TemplateItems)
                    {
                        if (!OrderQuantityMap.Any(s => s.Key == item.ItemNumber))
                        {
                            OrderQuantityMap.Add(item.ItemNumber, item.OrderQty.ToString());
                        }
                        if (!OrderPriceMap.Any(s => s.Key == item.ItemNumber))
                        {
                            OrderPriceMap.Add(item.ItemNumber, item.UnitPrice.ToString());
                        }

                        //if (OrderHistory != null)
                        //{
                        //    foreach (DataRow dr in OrderHistory.Rows)
                        //    {
                        //        try
                        //        {
                        //            if (dr["ItemCode"] != null)
                        //            {
                        //                if (dr["ItemCode"].ToString().Trim() == item.ItemNumber)
                        //                {
                        //                    item.AverageStopQty = Convert.ToDouble(dr["AverageQty"].ToString());
                        //                    break;
                        //                }
                        //                else
                        //                {
                        //                    item.AverageStopQty = 0;

                        //                }
                        //            }
                        //        }
                        //        catch (Exception ex)
                        //        {
                        //            Logger.Error("Erro in OrderHistory " + ex.Message.ToString());
                        //            break;
                        //        }

                        //    }
                        //}

                        if (MetricCustHistDemand != null)
                        {
                            foreach (DataRow dr in MetricCustHistDemand.Rows)
                            {
                                try
                                {
                                    if (dr["ItemId"] != null)
                                    {
                                        if (dr["ItemId"].ToString().Trim() == item.ItemId)
                                        {
                                            item.AverageStopQty = Convert.ToDouble(dr["AverageStopQty"].ToString());
                                            break;
                                        }
                                        else
                                        {
                                            item.AverageStopQty = 0;

                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error("Erro in MetricCustHistDemand is used for set value in AverageStopQty Field " + ex.Message.ToString());
                                    break;
                                }

                            }
                        }

                    }
                    //**********************************************************************************************************
                    ModelChangeArgs args = new ModelChangeArgs();
                    args.StateChangedTime = DateTime.Now;
                    args.Change = ChangeType.Loaded;
                    OnModelChanged(args);
                    args = null;
                    OnPropertyChanged("TemplateItemsCount");
                }
                catch (Exception ex)
                {
                    IsBusy = false;
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderTemplate][GetTemplateItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }

            });
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][AsyncEnd:GetTemplateItems]");
        }

        private void AppendOrderHistory(DataSet RowAndHeader)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][Start:AppendOrderHistory]");
            try
            {

                if (RowAndHeader == null)
                {
                    OrderHistory = new DataTable();
                    OrderHistory.Columns.Add("ItemCode");
                    OrderHistory.Columns.Add("ItemDesc");
                    OrderHistory.Columns.Add("AverageQty");
                    OrderHistoryHeaders = new DataTable();
                    OrderHistoryHeaders.Columns.Add("Chronology");
                    OrderHistoryHeaders.Columns.Add("OrderNumber");
                    OrderHistoryHeaders.Columns.Add("OrderDate");
                }
                else
                {
                    OrderHistoryHeaders = RowAndHeader.Tables[1];
                }
                // CustomerID - Changes Done
                //string query = "select 'H'+ convert(varchar,NUMBER(*)) + '_Qty' as Header,OrderDate,OrderID from busdta.ORDER_HEADER where OrderStateId=" + ActivityKey.OrderDelivered.GetStatusIdFromDB() + " AND CustShipToID='" + SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo + "'  ORDER BY ORDERDATE DESC, OrderID DESC";
                //DataSet ds = DbEngine.ExecuteDataSet(query);
                

                DataSet ds = objOrderManger.OrderTemplate_AppendOrderHistory();

                if (ds.HasData())
                {
                    Dictionary<string, string> headerKV = new Dictionary<string, string>();

                    int countInOrderHeader = ds.Tables[0].Rows.Count;
                    int columns = OrderHistory.Columns.Count;
                    for (int i = 0; i < countInOrderHeader; i++)
                    {
                        headerKV.Add(ds.Tables[0].Rows[i]["Header"].ToString(), ds.Tables[0].Rows[i]["OrderID"].ToString());
                    }
                    //for (int i = columns - 2; i >= 2; i--)
                    //{
                    //    OrderHistory.Columns[i].ColumnName = "H" + (i - countInOrderHeader) + "_Qty";
                    //}

                    int orderHeaderCount = OrderHistoryHeaders.Rows.Count;
                    for (int i = 1; i <= orderHeaderCount; i++)
                    {
                        OrderHistoryHeaders.Rows[i - 1]["chronology"] = countInOrderHeader + i;
                    }
                    //OrderHistoryHeaders = new DataTable();
                    DataRow newRow;
                    DataColumn newColumnInHistoryDt;

                    for (int i = 0; i < countInOrderHeader; i++)
                    {
                        if (OrderHistory.Columns.Contains("H" + (i + 1) + "_Qty"))
                        {
                            continue;
                        }

                        newColumnInHistoryDt = new DataColumn();
                        newColumnInHistoryDt.ColumnName = "H" + (i + 1) + "_Qty";
                        OrderHistory.Columns.Add(newColumnInHistoryDt);
                        newColumnInHistoryDt.SetOrdinal(i + 2);
                        newRow = OrderHistoryHeaders.NewRow();
                        newRow["chronology"] = i + 1;
                        newRow["OrderNumber"] = ds.Tables[0].Rows[i]["OrderID"].ToString();
                        newRow["OrderDate"] = Convert.ToDateTime(ds.Tables[0].Rows[i]["OrderDate"]).ToString("MM/dd/yyyy");
                        OrderHistoryHeaders.Rows.Add(newRow);
                    }
                    ds.Dispose();
                    ds = null;
                    OrderHistoryHeaders.DefaultView.Sort = "chronology asc";
                    OrderHistoryHeaders = OrderHistoryHeaders.DefaultView.ToTable();

                    // CustomerID - Changes Done
                   // query = "select IMLITM, IMDSC1, OH.OrderDate,OD.* from busdta.Order_Detail OD inner JOIN BUSDTA.ORDER_HEADER OH ON OD.ORDERID = OH.ORDERID LEFT OUTER JOIN BUSDTA.F4101 ON IMITM = ItemId where OH.CustShipToID='" + SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo + "' ";
                   string query = "CALL BUSDTA.SP_GetOrderHistoryDetails(@CustomerNo = '" + SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo + "')";
                    DataSet dsOrderDetails = DbEngine.ExecuteDataSet(query);
                    query = string.Empty;
                    if (dsOrderDetails.HasData())
                    {
                        if (RowAndHeader == null)
                        {
                            foreach (DataRow dr in dsOrderDetails.Tables[0].Rows)
                            {
                                DataRow drr = OrderHistory.NewRow();
                                drr["ItemCode"] = dr["ItemId"];
                                drr["ItemDesc"] = dr["IMDSC1"];
                                OrderHistory.Rows.Add(drr);
                            }
                        }
                        int orderHistoryCount = OrderHistory.Rows.Count;
                        string headerString = string.Empty;
                        for (int i = 0; i < orderHistoryCount; i++)
                        {
                            string item = OrderHistory.Rows[i]["ItemCode"].ToString();
                            for (int col = 0; col < countInOrderHeader; col++)
                            {
                                headerString = "H" + (col + 1) + "_Qty";
                                string OrderId = headerKV[headerString];
                                string ItemNo = item.Trim();

                                //DataRow dr = dsOrderDetails.Tables[0].Select(string.Format("OrderID = '{0}'  AND ItemID = '{1}'", headerKV[headerString], item.Trim())).FirstOrDefault();
                                DataRow dr = dsOrderDetails.Tables[0].Select("OrderID = '" + OrderId + "'  AND LongItem = '" + ItemNo + "'").FirstOrDefault();

                                OrderHistory.Rows[i][headerString] = (dr == null ? 0 : dr["OrderQty"]);
                                dr = dsOrderDetails.Tables[0].Select("LongItem='" + ItemNo + "'").FirstOrDefault();

                                if (dr != null)
                                {
                                    OrderHistory.Rows[i]["ItemCode"] = Convert.ToString(dr["IMLITM"]);
                                    OrderHistory.Rows[i]["ItemDesc"] = Convert.ToString(dr["IMDSC1"]);
                                }
                            }
                        }
                        headerKV = null;
                        dsOrderDetails.Dispose();
                        dsOrderDetails = null;
                    }
                    bool deleteRow = false;
                    int colCount = 0;
                    List<string> colToRemove = new List<string>();
                    foreach (DataColumn col in OrderHistory.Columns)
                    {
                        if (col.ColumnName.IndexOf("_Qty") > 0)
                            colCount++;
                        if (colCount > 10)
                        {
                            colToRemove.Add(col.ColumnName);
                            colCount--;
                        }
                    }
                    foreach (string col in colToRemove)
                    {
                        OrderHistory.Columns.Remove(col);
                    }
                    colToRemove.Clear();
                    colToRemove = null;

                    columns = OrderHistory.Columns.Count;
                    for (int j = 0; j < OrderHistory.Rows.Count; j++)
                    {
                        // Duplicate Row Remove - True
                        deleteRow = true;
                        for (int i = columns - 2; i >= 2; i--)
                        {
                            if (Convert.ToInt32(OrderHistory.Rows[j][OrderHistory.Columns[i]].ToString().Length > 0 ? OrderHistory.Rows[j][OrderHistory.Columns[i]].ToString():"0") > 0)
                            {
                                deleteRow = false;
                                break;
                            }
                        }
                        if (deleteRow)
                            OrderHistory.Rows[j].Delete();
                    }

                    OrderHistory.AcceptChanges();

                    //Remove duplicate rows from the table 
                    var uniqueRows = OrderHistory.AsEnumerable().Distinct(DataRowComparer.Default);

                    // Validate Done - Issue Addressed
                    if (uniqueRows.Count() > 0)
                        OrderHistory = uniqueRows.CopyToDataTable();
                }
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][Order][AppendOrderHistory][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels]OrderTemplate[End:AppendOrderHistory]");
        }
        //async void GetOrderHistory()
        //{
        //    await Task.Run(() =>
        //    {

        //        try
        //        {


        //        }
        //        catch (Exception)
        //        {
        //            //throw;
        //        }
        //    });
        //}

        private void Initialize()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][Start:Initialize]");
            templateManager = new Managers.TemplateManager();
            itemManager = new Managers.ItemManager();
            if (this._SearchItems != null)
                _SearchItemsCount = this._SearchItems.Count;
            InitializeCommands();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][End:Initialize]");
        }
        void LogOrderTemplateActivity()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][Start:LogOrderTemplateActivity]");
            Activity ac = new Activity
            {
                CustomerID = Customer.CustomerNo,
                RouteID = PayloadManager.ApplicationPayload.Route,
                ActivityType = ActivityKey.OrderTemplate.ToString(),
                ActivityStart = DateTime.Now,
                ActivityEnd = DateTime.Now,
                IsTxActivity = true,
            };
            Activity a = ResourceManager.Transaction.LogActivity(ac);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][End:LogOrderTemplateActivity]");
        }
        private void InitializeCommands()
        {
            Random qtyOnHand = new Random();
            //UpdateTemplateItemsFromOrder = new DelegateCommand((param) =>
            //{
            //    // UpdateOrderTemplateItems();
            //});
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][Start:InitializeCommands]");
            OrderUMChange = new DelegateCommand((param) =>
            {
                try
                {
                    if (param != null)
                    {
                        List<object> parameters = param as List<object>;
                        SelectedItem = parameters[1] as TemplateItem;
                        string changedUM = parameters[0].ToString();
                        if (SelectedItem != null)
                        {
                            SelectedItem.AvailableQty = Convert.ToInt32(ItemOrderQtyDictionary[SelectedItem.ItemNumber.Trim()]);
                            SelectedItem.UMConversionFactor = new TemplateManager().SetConversionFactorForItem(SelectedItem.PrimaryUM, changedUM, SelectedItem.ItemId, SelectedItem.ItemNumber);
                            int availQty = SelectedItem.AvailableQty - Convert.ToInt32(SelectedItem.OrderQty * SelectedItem.UMConversionFactor);
                            SelectedItem.IsValidForOrder = availQty < 0 ? false : true;
                            SelectedItem.AvailableQty = availQty;
                            SelectedItem.UM = changedUM;
                            //Issue ID- 43 and 99 - Fixed by Zakir
                            SelectedItem.UnitPrice = OrderManager.GetPriceByUomFactor(SelectedItem.ItemId, SelectedItem.UMPrice, SelectedItem.UM, SelectedItem.UnitPriceByPricingUOM);
                            SelectedItem.ReasonCodeDescription = null;
                            SelectedItem.ItemReasonCode = 0;
                        }
                        //Clear the signature 
                        this.ResetCanvas();
                        this.ResetSign();
                    }
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderTemplate][OrderUMChange DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            UpdateAvailableQty = new DelegateCommand((param) =>
            {
                try
                {
                    if (param != null)
                    {
                        SelectedItem = param as TemplateItem;
                        if (SelectedItem != null)
                        {
                            InventoryManager.ItemQty itemQty = new InventoryManager().GetItemQuantity(SelectedItem.ItemId);
                            SelectedItem.QtyOnHand = itemQty.OnHandQty;
                            SelectedItem.UMConversionFactor = new TemplateManager().SetConversionFactorForItem(SelectedItem.PrimaryUM, SelectedItem.UM, SelectedItem.ItemId, SelectedItem.ItemNumber);
                            int availQty = SelectedItem.AvailableQty = itemQty.OnHandQty - itemQty.ComittedQty - itemQty.HeldQuantity + Convert.ToInt32((SelectedItem.LastComittedQty - (SelectedItem.OrderQty * SelectedItem.UMConversionFactor)));
                            //SelectedItem.IsValidForOrder = (SelectedItem.AvailableQty == 0) ? true : availQty < 0 ? false : true;
                            if (isNumericIncDecAction)
                                SelectedItem.IsValidForOrder = availQty < 0 ? false : true;
                            SelectedItem.AvailableQty = availQty;
                        }
                    }
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderTemplate][UpdateAvailableQty DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });





            SaveTemplate = new DelegateCommand((param) =>
            {
                try
                {
                    List<TemplateItem> saveTemplateItemList = this._TemplateItems.Where(item => item.InclOnTmplt == true).ToList<TemplateItem>();
                    templateManager.SaveTemplate(saveTemplateItemList, SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
                    var alertMessage = new Helpers.AlertWindow { Message = "Template saved successfully", MessageIcon = "Save Successful", Header = "Success" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    LogOrderTemplateActivity();
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderTemplate][SaveTemplate DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            //SavePreOrder = new DelegateCommand((param) =>
            //{
            //    //  templateManager.SavePreOrder(preOrderItems);
            //});
            LoadTemplateWithUsualQuantity = new DelegateCommand((param) =>
            {
                foreach (Models.TemplateItem item in this._TemplateItems)
                {
                    item.OrderQty = item.UsualQty;
                }
                CloseDropdown = false;
                OnPropertyChanged("CloseDropdown");

            });
            LoadTemplateWithPreOrderQuantity = new DelegateCommand((param) =>
            {
                foreach (Models.TemplateItem item in this._TemplateItems)
                {
                    item.OrderQty = item.PreOrderQty;
                }
                CloseDropdown = false;
                OnPropertyChanged("CloseDropdown");
            });
            ResetBackTemplateQuantity = new DelegateCommand((param) =>
            {
                foreach (Models.TemplateItem item in this._TemplateItems)
                {
                    item.OrderQty = 0;
                }
                CloseDropdown = false;
                OnPropertyChanged("CloseDropdown");
            });
            // Add item from search to template list
            AddItemToTemplate = new DelegateCommand((Items) =>
            {
                try
                {
                    Managers.ItemManager itemManager = new Managers.ItemManager();
                    ObservableCollection<object> selectedItems = Items as ObservableCollection<object>;
                    if (selectedItems == null || selectedItems.Count == 0)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemToAddAlert, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    int selectionCount = selectedItems.Count;
                    int TotalItemsToAdd = selectedItems.Count;
                    int maxSequenceNo = TemplateItems.Count > 0 ? TemplateItems.Max(item => item.SeqNo) : 0;
                    int seqNo = 1;

                    int ItemsNotAddedCount = 0;
                    //Dictionary<string, List<string>> templateUms = ResourceManager.GetItemUMList;
                    Dictionary<string, List<string>> templateUms = UoMManager.GetItemUMList;
                    TemplateItems.ToList().ForEach(s => s.IsValidForOrder = true);
                    for (int index = 0; index < selectionCount; index++)
                    {
                        if (TemplateItems.Any(i => i.ItemNumber == (selectedItems[index] as Models.Item).ItemNumber))
                        {
                            ItemsNotAddedCount = ItemsNotAddedCount + 1;
                            continue;
                        }
                        Models.TemplateItem addedItem = new TemplateItem((selectedItems[index] as Models.Item));
                        addedItem.UM = Managers.UoMManager.GetDefaultSaleableUOMForItem(addedItem.ItemNumber);
                        addedItem.SelectedUM = addedItem.UM;
                        addedItem.EffectiveFrom = DateTime.Now.ToString("mm/dd/yyyy");
                        addedItem.EffectiveThru = DateTime.Now.ToString("mm/dd/yyyy");
                        addedItem.SeqNo = maxSequenceNo + (seqNo * 5);

                        string val = new Managers.NumberManager().GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.TEMPLATEID);
                        if (!string.IsNullOrEmpty(val))
                        {
                            addedItem.TemplateDetailID = val;
                            //Available qty is getting updated from database
                            //addedItem.ActualQtyOnHand = qtyOnHand.Next(5, 50);

                            //string itemNumber = addedItem.ItemNumber;
                            //addedItem.AverageStopQty = AveragerQty(itemNumber);  //returning avg value  

                            string itemId = addedItem.ItemId;
                            addedItem.AverageStopQty = AveragerQty(itemId);  //returning avg value    

                            // OrderItem order = new OrderItem(new Item());
                            //addedItem.AppliedUMS = itemManager.GetAppliedUMs(addedItem.ItemNumber).Count != 0 ? itemManager.GetAppliedUMs(addedItem.ItemNumber) : new List<string>() { addedItem.UM };
                            addedItem.AppliedUMS = templateUms.ContainsKey(addedItem.ItemNumber.Trim()) ? templateUms[addedItem.ItemNumber.Trim()] : new List<string>() { addedItem.UM };
                            addedItem.InclOnTmplt = true;
                            addedItem.OrderQty = 1;
                            var itemtoadd = SearchItemList.FirstOrDefault(x => string.Equals(x.ItemNumber.ToLower().Trim(), addedItem.ItemNumber.ToLower().Trim(), StringComparison.CurrentCultureIgnoreCase));
                            if (itemtoadd != null)
                            {
                                //************************************************************************************************
                                // Comment: Commented not to affect the Available qty
                                // created: jan 13, 2016
                                // Author: Vivensas (Rajesh,Yuvaraj)
                                // Revisions: 
                                //*************************************************************************************************
                                //TUI Issue Id : 35 If the adding new item into template than default order qty is 1 and available qty should be subtract 1 
                                //If the adding new item available qty is 0 then order qty is also 0 it should set default value as 1
                                //addedItem.AvailableQty = itemtoadd.AvailableQty; // -1;
                                addedItem.AvailableQty = (itemtoadd.AvailableQty == 0) ? 0 : (itemtoadd.AvailableQty - 1); // -1;
                                if (itemtoadd.AvailableQty == 0)
                                    addedItem.OrderQty = 0;


                                //*************************************************************************************************
                                // Vivensas changes ends over here
                                //**************************************************************************************************

                                addedItem.QtyOnHand = itemtoadd.QtyOnHand;

                                if (!ItemOrderQtyDictionary.ContainsKey(addedItem.ItemNumber.Trim()))
                                    ItemOrderQtyDictionary.Add(addedItem.ItemNumber.Trim(), itemtoadd.AvailableQty);
                            }
                            TemplateItems.Insert(TemplateItems.Count == 0 ? 0 : TemplateItems.Count, addedItem);
                            if (OrderQty.ContainsKey(addedItem.ItemNumber.Trim()))
                                OrderQty.Add(addedItem.ItemNumber.Trim(), addedItem.OrderQty);
                            else
                                OrderQty[addedItem.ItemNumber.Trim()] = addedItem.OrderQty;


                            addedItem.IsValidForOrder = addedItem.AvailableQty == 0 ? false : true;
                            SearchItemList.Remove(selectedItems[index] as Models.Item);

                            index--;
                            selectionCount--;
                            seqNo++;
                            SearchItemsCount = SearchItemList.Count;
                            OnPropertyChanged("SearchItemsCount");
                            OnPropertyChanged("SearchResultText");
                            ModelChangeArgs args = new ModelChangeArgs();
                            args.StateChangedTime = DateTime.Now;
                            args.Change = ChangeType.ItemAdded;
                            OnModelChanged(args);
                        }
                        else
                        {
                            Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, please sync to continue" }, MessageToken);
                            break;
                        }
                    }

                    ValidateSearch(TotalItemsToAdd, ItemsNotAddedCount);

                    OnPropertyChanged("TemplateItemsCount");
                    ResetCanvas();
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderTemplate][AddItemToTemplate DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });

            ClearSearchText = new DelegateCommand((param) =>
            {
                IsSearching = true;
                SearchText = "";
                SearchResultText = "";
                this.SearchItems("", true);
            });
            SearchItemOnType = new DelegateCommand((searchTerm) =>
            {
                bool flag = this.SearchText.ToString().Length == 0 ? true : false;

                if (!flag)
                {
                    SearchItems(this.SearchText.ToString(), flag);
                }
            });
            SearchItem = new DelegateCommand((searchTerm) =>
            {
                SearchItems(searchTerm.ToString(), true);
            });
            CreateOrder = new DelegateCommand((o) =>
            {
                IsBusy = true;
                try
                {
                    List<TemplateItem> saveTemplateItemList = this._TemplateItems.Where(item => item.InclOnTmplt == true).ToList<TemplateItem>();
                    templateManager.SaveTemplate(saveTemplateItemList, SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
                    CreateOrderItems();
                    //if (ViewModels.PreviewOrder.GlobalInkCanvas != null)
                    //    ViewModels.PreviewOrder.GlobalInkCanvas.Strokes.Clear();
                    Order.OrderStatus = ActivityKey.None;
                    Order.VoidOrderReasonId = 0;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderTemplate][CreateOrder DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    IsBusy = false;
                }
            });
            DeleteItemFromTemplate = new DelegateCommand((Items) =>
            {
                try
                {

                    ObservableCollection<object> selectedItems = Items as ObservableCollection<object>;
                    if (selectedItems == null || selectedItems.Count == 0)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemForDeleteAlert, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.DeleteItemConfirmation, MessageIcon = "Alert", Confirmed = false };
                    Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                    if (!confirmMessage.Confirmed) return;

                    int selectionCount = selectedItems.Count;
                    for (int index = 0; index < selectionCount; index++)
                    {
                        Models.TemplateItem item = selectedItems[index] as Models.TemplateItem;
                        if (OrderQty.ContainsKey(item.ItemNumber.Trim()))
                            OrderQty.Remove(item.ItemNumber.Trim());
                        TemplateItems.Remove(item);
                        index--;
                        selectionCount--;
                    }

                    //List<TemplateItem> IsCheckSelectCount = this._TemplateItems.Where(item => item.IsSelectedTemplateItem == true).ToList<TemplateItem>();
                    //if (IsCheckSelectCount.Count == 0)
                    //{
                    //    var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemForDeleteAlert, MessageIcon = "Alert" };
                    //    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    //    return;
                    //}
                    //var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.DeleteItemConfirmation, MessageIcon = "Alert", Confirmed = false };
                    //Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                    //if (!confirmMessage.Confirmed) return;

                    //foreach(var selectItem in IsCheckSelectCount){
                    //    if (OrderQty.ContainsKey(selectItem.ItemNumber.Trim()))
                    //        OrderQty.Remove(selectItem.ItemNumber.Trim());
                    //    TemplateItems.Remove(selectItem);
                    //}

                    OnPropertyChanged("TemplateItemsCount");
                    ResetCanvas();
                }
                catch (Exception ex)
                {
                    
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderTemplate][DeleteItemFromTemplate DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });

            OpenExpander = new DelegateCommand((param) =>
            {

                switch (Convert.ToInt32(param))
                {
                    case 0:
                        if (!ToggleExpander)
                        {
                            ToggleExpander = true;
                        }
                        break;
                    case 1:
                        if (!ToggleExpander)
                        {
                            ToggleExpander = true;
                        }
                        break;
                    default:
                        ToggleExpander = false;
                        break;

                }
            });

            BeginGridCellEdit = new DelegateCommand((param) =>
            {


                ObservableCollection<Models.TemplateItem> TempItems = new ObservableCollection<TemplateItem>();
                TempItems = (ObservableCollection<Models.TemplateItem>)param;
                PreviousSequenceNumbers = new List<int>();


                for (int index = 0; index < TempItems.Count; index++)
                {
                    PreviousSequenceNumbers.Add(TempItems[index].SeqNo);
                }
                CheckIsEditEnded = true;

            });
            GridCellEditEnded = new DelegateCommand((param) =>
            {

                if (CheckIsEditEnded)
                {
                    TemplateItem TempItem = param as TemplateItem;


                    if (PreviousSequenceNumbers.Contains(TempItem.SeqNo))
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = "Duplicate Sequence number", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        CheckIsEditEnded = false;
                        TempItem.SeqNo = OldSequenceNo;
                    }
                    else
                    {
                        OnPropertyChanged("TemplateItems");

                    }

                }
                else
                {
                    OnPropertyChanged("TemplateItems");
                }

            });
            SearchResultTextChange = new DelegateCommand((param) =>
            {
                //Item itm = param as Item;

                SearchItemsCount = Convert.ToInt32(param);
                OnPropertyChanged("SearchItemsCount");
                OnPropertyChanged("SearchResultText");
            });

            //OrderItemSelectionChanged = new DelegateCommand((param) =>
            //{

            //});
        }

        private void ValidateSearch(int TotalItemsToAdd, int ItemsNotAddedCount)
        {
            if (TotalItemsToAdd == ItemsNotAddedCount)
            {
                //ALL/ONE ITEMS PRESNTS in item template
                Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Helpers.Constants.Common.AllOrOneItemsExists }, MessageToken);
            }
            else if (TotalItemsToAdd > ItemsNotAddedCount && ItemsNotAddedCount > 0)
            {
                //PARTIAL ITEM PRESENT in item template
                Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Helpers.Constants.Common.PartialItemsExists }, MessageToken);
            }
        }
        private void ResetCanvas()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][Start:ResetCanvas]");
            ModelChangeArgs args = new ModelChangeArgs();
            args.StateChangedTime = DateTime.Now;
            args.Change = ChangeType.ResetCanvas;
            OnModelChanged(args);
            Logger.Info("[SalesLogicExpress.Application.ViewModels]OrderTemplate[End:ResetCanvas]");
        }
        public enum ChangeType
        {
            Loaded,
            ResetCanvas,
            ItemAdded
        }
        //public double AveragerQty(string itemNumber)
        //{
        //    double avgQty = 0;
        //    try
        //    {
        //        foreach (DataRow dr in OrderHistory.Rows)
        //        {
        //            if (dr["ItemCode"].ToString() == itemNumber)
        //            {
        //                avgQty = Convert.ToDouble(dr["AverageQty"]);
        //                break;
        //            }
        //            else
        //            {
        //                avgQty = 0;
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        //throw;
        //    }

        //    return avgQty;

        //}

        public double AveragerQty(string itemId)
        {
            double avgQty = 0;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][Start:AveragerQty]");
            try
            {
                foreach (DataRow dr in MetricCustHistDemand.Rows)
                {
                    if (dr["ItemId"].ToString() == itemId)
                    {
                        avgQty = Convert.ToDouble(dr["AverageStopQty"]);
                        break;
                    }
                    else
                    {
                        avgQty = 0;
                    }
                }
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderTemplate][AveragerQty][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][End:AveragerQty]");
            return avgQty;

        }




        //public List<string> GetAppliedUms(int itemNumber)
        //{
        //    List<string> AppliedUms = new List<string>();

        //    DataTable dt = DbEngine.ExecuteDataSet(@"select distinct(UMUM) UMUM from BUSDTA.F41002 where UMITM = (select imitm from BUSDTA.F4101 where IMLITM = '" + itemNumber + "')" +
        //   " union select distinct(UMRUM) UMUM from BUSDTA.F41002 where UMITM = (select imitm from BUSDTA.F4101 where IMLITM = '" + itemNumber + "')").Tables[0];

        //    foreach (DataRow item in dt.Rows)
        //    {
        //        AppliedUms.Add(item["UMUM"].ToString());
        //    }
        //    return AppliedUms;
        //}

        public int OldSequenceNo { get; set; }

        public bool IsDuplicate { get; set; }

        CancellationTokenSource tokenForCancelTask = new CancellationTokenSource();
        public async Task SearchItems(string searchTerm, bool skipMinLengthCheck)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][asyn Start:SearchItems]");
            await Task.Run(() =>
           {
               System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
               {
                   try
                   {
                       if (!skipMinLengthCheck)
                       {
                           if (searchTerm != null && searchTerm.ToString().Length < 3)
                           {
                               return;
                           }
                           if (searchTerm != null && searchTerm.ToString().Length == 0)
                           {
                               SearchItemList.Clear();
                               this.OnPropertyChanged("SearchItemsCount");
                               this.OnPropertyChanged("SearchResultText");
                               return;
                           }
                       }
                       SearchItemList.Clear();
                       SearchItemList = itemManager.SearchItem(searchTerm.ToString(), Customer.CustomerNo);
                       SearchItemsCount = SearchItemList.Count;
                       IsSearching = false;
                       this.OnPropertyChanged("SearchItemsCount");
                       this.OnPropertyChanged("SearchResultText");
                   }
                   catch (Exception ex)
                   {
                       
                      Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderTemplate][SearchItems][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                   }
               }));
           }, tokenForCancelTask.Token);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][asyn End:SearchItems]");
        }



        async void CreateOrderItems()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][asyn Start:CreateOrderItems]");
            await Task.Run(() =>
            {
                try
                {
                    OrderItems = templateManager.ApplyPricingToTemplates(TemplateItems, SalesLogicExpress.Application.Managers.UserManager.UserBranch, SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
                }
                catch (Exception ex)
                {
                    Logger.Error("Error in CreateOrderItems" + ex.Message.ToString());

                }
            });

            try
            {
                SaveOrder(Convert.ToInt32(SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo), OrderItems);
                Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.CreateOrder);
                Managers.OrderManager orderManager = new Managers.OrderManager();
                orderManager.UpdateNoSaleReason(Customer.StopID, Customer.CustomerNo);
                PayloadManager.OrderPayload.Customer.PendingActivity = 1;
            }
            catch (Exception ex)
            {
                Logger.Error("Erro in SaveOrder" + ex.InnerException.Message);
            }
            PayloadManager.OrderPayload.Items = OrderItems.ToList();

            if (Order.OrderId > 0)
            {
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.Order, CurrentViewName = ViewModelMappings.View.OrderTemplate, CloseCurrentView = false, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);

            }
            IsBusy = false;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][asyn End:SearchItems]");
        }
        float varTotalCoffeeAmtValue, varTotalAlliedAmtValue, varOrderTotalValue;// varInvoiceTotalValue;
        private string TotalCoffeeAmtValue = "0.00", TotalAlliedAmtValue = "0.00",
                       OrderTotalValue = "0.00", InvoiceTotalValue = "0.00";
        void CalulateTotal(ObservableCollection<Models.OrderItem> orderitem)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][Start:CalulateTotal]");
            try
            {
                foreach (Models.OrderItem order in orderitem)
                {
                    varOrderTotalValue = varOrderTotalValue + float.Parse(order.ExtendedPrice.ToString());
                    if (order.SalesCat1.Equals("COF"))
                    {
                        varTotalCoffeeAmtValue = varTotalCoffeeAmtValue + float.Parse(order.ExtendedPrice.ToString());

                    }
                    if (order.SalesCat1.Equals("ALL"))
                    {
                        varTotalAlliedAmtValue = varTotalAlliedAmtValue + float.Parse(order.ExtendedPrice.ToString());

                    }
                }
                TotalCoffeeAmtValue = varTotalCoffeeAmtValue.ToString("0.00");
                TotalAlliedAmtValue = varTotalAlliedAmtValue.ToString("0.00");
                OrderTotalValue = varOrderTotalValue.ToString("0.00");
                InvoiceTotalValue = varOrderTotalValue.ToString("0.00");
                PayloadManager.OrderPayload.Amount = InvoiceTotalValue.ToString();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderTemplate][SaveOrder][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][End:SaveOrder]");
        }

        private void SaveOrder(int CustomerId, ObservableCollection<OrderItem> OrderItems)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][Start:SaveOrder]");
            Managers.OrderManager orderManager = new Managers.OrderManager();
            int existingOrderId = Order.OrderId; // orderManager.GetUncomletedLastOrderIdIfAny(CommonNavInfo.Customer.CustomerNo);
            if (existingOrderId != 0)
            {
                Order.OrderId = existingOrderId;
            }
            else
            {
                Order.OrderId = orderManager.GetNewOrderNum();
            }
            //************************************************************************************************
            // Comment: Used for not allowing the order with 0 and less than 0
            // Created: feb 16, 2016
            // Author: Vivensas (Rajesh,Yuvaraj)
            // Revisions: 
            //*************************************************************************************************

            if (Order.OrderId > 0)
            {
                CalulateTotal(OrderItems);
                orderManager.SaveOrder(Order.OrderId, CustomerId, OrderItems, TotalCoffeeAmtValue, TotalAlliedAmtValue);
            }
            else
            {
                var OpenDialog = new Helpers.DialogWindow { TemplateKey = "StatusInforDialog", Title = "End Of Order Sequence" };
                Messenger.Default.Send<DialogWindow>(OpenDialog, MessageToken);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][End:SaveOrder]");
        }
        void AdjustOrderQty(List<OrderItem> Items)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][Start:AdjustOrderQty]");
            foreach (OrderItem item in Items)
            {
                if (OrderQty.ContainsKey(item.ItemNumber.Trim()))
                {
                    OrderQty[item.ItemNumber.Trim()] = item.OrderQty;
                }
                else
                {
                    OrderQty.Add(item.ItemNumber.Trim(), item.OrderQty);
                }
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][End:AdjustOrderQty]");
        }
        public void UpdateOrderTemplateItems()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][Start:UpdateOrderTemplateItems]");
            try
            {
                if (PayloadManager.OrderPayload.Items != null)
                {
                    if (PayloadManager.OrderPayload.Items.Count > 0)
                    {
                        AdjustOrderQty(PayloadManager.OrderPayload.Items);
                        foreach (OrderItem item in PayloadManager.OrderPayload.Items)
                        {
                            if (!TemplateItems.Any(s => s.ItemNumber.Trim() == item.ItemNumber.Trim()))
                            {
                                // If new Item Added to Order
                                Models.TemplateItem addedItem = new TemplateItem(item);
                                addedItem.OrderQty = item.OrderQty;
                                addedItem.EffectiveFrom = DateTime.Now.ToString("mm/dd/yyyy");
                                addedItem.EffectiveThru = DateTime.Now.ToString("mm/dd/yyyy");
                                //addedItem.SeqNo = maxSequenceNo + (seqNo * 5);
                                addedItem.QtyOnHand = item.QtyOnHand;
                                addedItem.AverageStopQty = item.AverageStopQty;  //returning avg value    
                                addedItem.AvailableQty = item.AvailableQty;
                                addedItem.SelectedUM = addedItem.UM;
                                // OrderItem order = new OrderItem(new Item());
                                addedItem.AppliedUMS = item.AppliedUMS;
                                addedItem.IsValidForOrder = item.IsValidForOrder;
                                addedItem.LastComittedQty = item.LastComittedQty;

                                TemplateItems.Insert(TemplateItems.Count == 0 ? 0 : TemplateItems.Count - 1, addedItem);
                            }
                            else
                            {
                                // If any Item Edited from Order
                                TemplateItems.FirstOrDefault(s => s.ItemNumber.Trim() == item.ItemNumber.Trim()).IsValidForOrder = item.IsValidForOrder;
                                TemplateItems.FirstOrDefault(s => s.ItemNumber.Trim() == item.ItemNumber.Trim()).OrderQty = item.OrderQty;
                                TemplateItems.FirstOrDefault(s => s.ItemNumber.Trim() == item.ItemNumber.Trim()).UM = item.UM;
                                TemplateItems.FirstOrDefault(s => s.ItemNumber.Trim() == item.ItemNumber.Trim()).QtyOnHand = item.QtyOnHand;
                                TemplateItems.FirstOrDefault(s => s.ItemNumber.Trim() == item.ItemNumber.Trim()).AvailableQty = item.AvailableQty;
                                TemplateItems.FirstOrDefault(s => s.ItemNumber.Trim() == item.ItemNumber.Trim()).LastComittedQty = item.LastComittedQty;
                                TemplateItems.FirstOrDefault(s => s.ItemNumber.Trim() == item.ItemNumber.Trim()).UnitPrice = item.UnitPrice;
                                TemplateItems.FirstOrDefault(s => s.ItemNumber.Trim() == item.ItemNumber.Trim()).ItemReasonCode = item.ReasonCode;
                                TemplateItems.FirstOrDefault(s => s.ItemNumber.Trim() == item.ItemNumber.Trim()).UnitPriceByPricingUOM = item.UnitPriceByPricingUOM;
                                //Issue ID - 42: Reason code was not provided in Create Order screen but the reason code number is displayed in Preview order Screen - Zakir
                                TemplateItems.FirstOrDefault(s => s.ItemNumber.Trim() == item.ItemNumber.Trim()).ReasonCodeDescription = item.ResonCodeDescription;
                            }

                        }
                    }
                    if (TemplateItems.Count > 0)
                    {
                        int templateItemSequenceNo = 0;
                        foreach (TemplateItem item in TemplateItems)
                        {
                            templateItemSequenceNo = templateItemSequenceNo + 5;
                            item.SeqNo = templateItemSequenceNo;
                            if (item.OrderQty > 0 && !PayloadManager.OrderPayload.Items.Any(s => s.ItemNumber.Trim() == item.ItemNumber.Trim()))
                            {
                                // If any Item deleted from Order
                                item.OrderQty = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderTemplate][UpdateOrderTemplateItems][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][End:UpdateOrderTemplateItems]");
        }

        public Telerik.Windows.Data.SortDescriptor SortGrid()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][Start:SortGrid]");
            Telerik.Windows.Data.SortDescriptor descriptor = new Telerik.Windows.Data.SortDescriptor();
            descriptor.Member = "SeqNo";
            descriptor.SortDirection = ListSortDirection.Ascending;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][End:SortGrid]");
            return descriptor;
        }

        void LogActivity(ActivityKey key)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][Start:LogActivity]");
            try
            {
                PayloadManager.OrderPayload.RouteID = PayloadManager.ApplicationPayload.Route;
                PayloadManager.OrderPayload.OrderID = Order.OrderId.ToString();
                PayloadManager.OrderPayload.InvoiceNo = Order.OrderId.ToString();
                Activity ac = new Activity
                {
                    ActivityHeaderID = PayloadManager.OrderPayload.TransactionID,
                    CustomerID = PayloadManager.OrderPayload.Customer.CustomerNo,
                    RouteID = PayloadManager.OrderPayload.RouteID,
                    ActivityType = key.ToString(),
                    ActivityStart = DateTime.Now,
                    StopInstanceID = PayloadManager.OrderPayload.Customer.StopID,
                    IsTxActivity = key == ActivityKey.PickItem ? false : true,
                    ActivityDetailClass = PayloadManager.OrderPayload.ToString(),
                    ActivityDetails = PayloadManager.OrderPayload.SerializeToJson()
                };
                if (key == ActivityKey.HoldAtOrderEntry || key == ActivityKey.VoidAtOrderEntry)
                {
                    ac.ActivityEnd = DateTime.Now;
                }
                Activity a = ResourceManager.Transaction.LogActivity(ac);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderTemplate][LogActivity][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][End:SortGrid]");
        }
        public override bool OnActivate()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][Start:OnActivate]");
            CheckForOrderCreation();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][End:OnActivate]");
            return base.OnActivate();
        }

        private void CheckForOrderCreation()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][Start:CheckForOrderCreation]");
            IsOrderCreated = false;
            //Managers.OrderManager orderManager = new Managers.OrderManager();//Remove Unused Instance 
            int existingOrderId = Order.OrderId; //orderManager.GetUncomletedLastOrderIdIfAny(CommonNavInfo.Customer.CustomerNo);
            if (existingOrderId != 0)
                IsOrderCreated = true;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][End:CheckForOrderCreation]");
        }
        public override bool ConfirmSave()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][Start:ConfirmSave]");
            try
            {
                ReasonCode reasonCode = new ReasonCode(string.Empty);
                reasonCode.StateChanged += reasonCode_StateChanged;
                reasonCode.ParentViewModel = this;
                reasonCode.MessageToken = MessageToken;
                reasonCode.ActivityKey = ActivityKey.VoidAtOrderEntry;
                var OpenDialog = new Helpers.DialogWindow { TemplateKey = "VoidOrderReason", Title = "Void Order Reason", Payload = reasonCode };
                Messenger.Default.Send<DialogWindow>(OpenDialog, MessageToken);
                CanNavigate = OpenDialog.Confirmed;
                if (!OpenDialog.Cancelled)
                { IsDirty = false; }
                if (CanNavigate)
                {
                    PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.Order);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderTemplate][ConfirmSave][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][End:ConfirmSave]");
            return base.ConfirmSave();
        }

        void reasonCode_StateChanged(object sender, OrderStateChangeArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][Start:reasonCode_StateChanged]");
            if (e.State == OrderState.Void)
            {
                try
                {
                    CanNavigate = true;
                    LogActivity(ActivityKey.VoidAtOrderEntry);
                    Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.VoidAtOrderEntry);
                    string tempStopDate = string.Empty;
                    string stopID = string.Empty;
                    if (PayloadManager.OrderPayload.StopDate.Date > DateTime.Now.Date || PayloadManager.OrderPayload.StopDate.Date < DateTime.Now.Date)
                    {
                        tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                        stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.OrderPayload.Customer.CustomerNo, tempStopDate);
                    }
                    else
                    {
                        tempStopDate = PayloadManager.OrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                        stopID = PayloadManager.OrderPayload.Customer.StopID;
                    }
                    new Managers.CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                    new Managers.CustomerManager().UpdateActivityCount(stopID, false, true);
                    PayloadManager.OrderPayload.Customer.SaleStatus = "";
                    PayloadManager.OrderPayload.Customer.CompletedActivity += 1;
                    new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.OrderPayload.Customer.CustomerNo);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderTemplate][reasonCode_StateChanged][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][End:reasonCode_StateChanged]");
        }
        public override bool ConfirmCancel()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][Start:ConfirmCancel]");
            try
            {
                OrderItems = templateManager.ApplyPricingToTemplates(TemplateItems, SalesLogicExpress.Application.Managers.UserManager.UserBranch, SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
                PayloadManager.OrderPayload.Items = OrderItems.ToList<OrderItem>();
                new OrderManager().SaveOrder(Order.OrderId, Convert.ToInt32(PayloadManager.OrderPayload.Customer.CustomerNo), OrderItems);
                LogActivity(ActivityKey.HoldAtOrderEntry);
                Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.HoldAtOrderEntry);
                #region Update Activity Count
                string tempStopDate = string.Empty;
                string stopID = string.Empty;
                if (PayloadManager.OrderPayload.StopDate.Date > DateTime.Now.Date || PayloadManager.OrderPayload.StopDate.Date < DateTime.Now.Date)
                {
                    tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                    stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.OrderPayload.Customer.CustomerNo, tempStopDate);
                }
                else
                {
                    tempStopDate = PayloadManager.OrderPayload.StopDate.Date.ToString("yyyy-MM-dd");
                    stopID = PayloadManager.OrderPayload.Customer.StopID;
                }
                new Managers.CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                new Managers.CustomerManager().UpdateActivityCount(stopID, true, true);
                PayloadManager.OrderPayload.Customer.SaleStatus = "";
                PayloadManager.OrderPayload.Customer.PendingActivity = new Managers.CustomerManager().GetPendingAtivityForStop(stopID);
                new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.OrderPayload.Customer.CustomerNo);
                #region UpdateCommitedQuantity
                // Log commited qty to inventory_ledger  and update inventory
                //var objInv = new Managers.InventoryManager();
                //if (PayloadManager.OrderPayload.Items != null)
                //    objInv.UpdateCommittedQuantity(PayloadManager.OrderPayload.Items.ToList(), StatusTypesEnum.HOLD);
                #endregion
                #endregion

                #region Update tem squncing no.
                //UPDATE CUSTOMER'S TEMPERORY SEQUENCE NO. --- Priya Natu
                DateTime todayDate = Convert.ToDateTime(tempStopDate);
                ViewModels.CustomerDashboard custDash = new CustomerDashboard();
                PayloadManager.OrderPayload.Customer = custDash.GetNewCustomer(todayDate, PayloadManager.OrderPayload.Customer);

                #endregion
                CanNavigate = true;
                IsDirty = false;
                PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.Order);
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][OrderTemplate][ConfirmCancel][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][OrderTemplate][End:ConfirmCancel]");
            return base.ConfirmCancel();
        }
        bool isordercreated;
        public bool IsOrderCreated
        {
            get { return isordercreated; }
            set
            {
                isordercreated = value;

                if (IsTemplateForOrder)
                {
                    IsDirty = isordercreated;
                }

                HoldVoidMessage();
            }
        }

        private void HoldVoidMessage()
        {
            OkContent = "Void";
            CancelContent = "Hold";
            Message = "Changes have been made to the order.\n Do you want to Hold or Void this Order?";
        }

        //void UpdateAvailableQuantity(TemplateItem item, string direction)
        //{

        //}

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
