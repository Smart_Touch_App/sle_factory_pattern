﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using GalaSoft.MvvmLight.Messaging;
using Telerik.Windows.Controls;
using System.Collections.ObjectModel;
using SalesLogicExpress.Application.Managers;
using System.Threading;
using System.Windows.Threading;

namespace SalesLogicExpress.Application.ViewModels
{
    public class ReplenishAddLoadViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.ReplenishAddLoadViewModel");

        #region Properties & Variables
        public Guid MessageToken { get; set; }

        private string _SearchResultText;
        public string SearchResultText
        {
            get
            {
                if (SearchItemsCount == 0)
                {
                    return "No items found";
                }
                else
                {
                    SearchResultTextVisibility = true;
                    OnPropertyChanged("SearchResultTextVisibility");
                    return "Item(s) found " + SearchItemsCount;
                }
            }
            set
            {
                if (value != _SearchResultText)
                {
                    _SearchResultText = value;
                    OnPropertyChanged("SearchResultText");
                }
            }
        }

        private bool _SearchResultTextVisibility = false;
        public bool SearchResultTextVisibility
        {
            get
            {
                return _SearchResultTextVisibility;
            }
            set
            {
                _SearchResultTextVisibility = value;
            }
        }

        private int _SearchItemsCount;

        public int SearchItemsCount
        {
            get
            {

                return _SearchItemsCount;
            }
            set
            {
                _SearchItemsCount = value;
                OnPropertyChanged("SearchItemsCount");
                OnPropertyChanged("SearchResultText");
            }
        }


        #endregion

        #region OnPropertyChanged

        #region Bool

        private bool showShippedQty = false;
        public bool ShowShippedQty
        {
            get
            {
                return showShippedQty;
            }
            set
            {
                showShippedQty = value;
                OnPropertyChanged("ShowShippedQty");
            }
        }


        private bool isSearching = false;
        public bool IsSearching
        {
            get { return isSearching; }
            set { isSearching = value; OnPropertyChanged("IsSearching"); }
        }
        private bool isEnableSave = false;
        public bool IsEnableSave
        {
            get { return isEnableSave; }
            set
            {
                isEnableSave = value; OnPropertyChanged("IsEnableSave");
                if (!PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment)
                {
                    IsDirty = IsEnableSave;
                }
            }
        }


        private bool isEnableDelete = false;
        public bool IsEnableDelete
        {
            get { return isEnableDelete; }
            set
            {
                isEnableDelete = value; OnPropertyChanged("IsEnableDelete");
            }
        }


        private bool isEnablePrint = true;
        public bool IsEnablePrint
        {
            get { return isEnablePrint; }
            set { isEnablePrint = value; OnPropertyChanged("IsEnablePrint"); }
        }



        private bool isEnablePick = false;
        public bool IsEnablePick
        {
            get { return isEnablePick; }
            set { isEnablePick = value; OnPropertyChanged("IsEnablePick"); }
        }


        private bool isEnableAddItem = false;
        public bool IsEnableAddItem
        {
            get { return isEnableAddItem; }
            set { isEnableAddItem = value; OnPropertyChanged("IsEnableAddItem"); }
        }


        private bool isEnableGridEdit = true;
        public bool IsEnableGridEdit
        {
            get { return isEnableGridEdit; }
            set { isEnableGridEdit = value; OnPropertyChanged("IsEnableGridEdit"); }
        }


        private bool isSuggestionView = false;
        public bool IsSuggestionView
        {
            get { return isSuggestionView; }
            set
            {
                isSuggestionView = value; OnPropertyChanged("IsSuggestionView");
                LoadQtyHeader = IsSuggestionView ? "Repln. Qty." : "Load Qty.";
                TraxnQtyUOMHeader = IsSuggestionView ? "Repln. Qty UOM" : "Load Qty UOM";
            }
        }


        private bool isVisibleRegenerateSuggestion = false;
        public bool IsVisibleRegenerateSuggestion
        {
            get { return isVisibleRegenerateSuggestion; }
            set { isVisibleRegenerateSuggestion = value; OnPropertyChanged("IsVisibleRegenerateSuggestion"); }
        }



        private bool isEnableYesDelete = true;
        public bool IsEnableYesDelete
        {
            get { return isEnableYesDelete; }
            set { isEnableYesDelete = value; OnPropertyChanged("IsEnableYesDelete"); }
        }


        private bool isVisibleContinuePick = false;
        public bool IsVisibleContinuePick
        {
            get { return isVisibleContinuePick; }
            set { isVisibleContinuePick = value; OnPropertyChanged("IsVisibleContinuePick"); }
        }

        private bool isHeldReturn = false;
        public bool IsHeldReturn
        {
            get { return isHeldReturn; }
            set { isHeldReturn = value; OnPropertyChanged("IsHeldReturn"); }
        }


        private bool isEnablePrintYes = true;

        public bool IsEnablePrintYes
        {
            get { return isEnablePrintYes; }
            set { isEnablePrintYes = value; OnPropertyChanged("IsEnablePrintYes"); }
        }

        private bool isShowPrint = false;
        public bool IsShowPrint
        {
            get { return isShowPrint; }
            set { isShowPrint = value; OnPropertyChanged("IsShowPrint"); }
        }
        #endregion

        #region Int
        private int totalLoadQuantity = 0;
        public int TotalLoadQuantity
        {
            get { return totalLoadQuantity; }
            set
            {
                totalLoadQuantity = value;
                if (totalLoadQuantity > 0)
                {
                    IsEnableSave = true;
                    //IsDirty = true;
                }
                else
                {
                    IsDirty = false;
                    IsEnableSave = false;
                }
                OnPropertyChanged("TotalLoadQuantity");
            }
        }
        #endregion

        #region string
        private string fromBranch;

        public string FromBranch
        {
            get { return fromBranch; }
            set { fromBranch = value; OnPropertyChanged("FromBranch"); }
        }
        private string toBranch;

        public string ToBranch
        {
            get { return toBranch; }
            set { toBranch = value; OnPropertyChanged("ToBranch"); }
        }

        private string routeNameAddLoad;

        public string RouteNameAddLoad
        {
            get { return routeNameAddLoad; }
            set { routeNameAddLoad = value; OnPropertyChanged("RouteNameAddLoad"); }
        }
        private string replnID;

        public string ReplnID
        {
            get { return replnID; }
            set { replnID = value; OnPropertyChanged("ReplnID"); }
        }

        private string fromDateToDateTitle = string.Empty;

        public string FromDateToDateTitle
        {
            get { return fromDateToDateTitle; }
            set { fromDateToDateTitle = value; OnPropertyChanged("FromDateToDateTitle"); }
        }

        private string replnIDName = string.Empty;

        public string ReplnIDName
        {
            get { return replnIDName; }
            set { replnIDName = value; OnPropertyChanged("ReplnIDName"); }
        }

        private string messageLine1 = string.Empty;

        public string MessageLine1
        {
            get { return messageLine1; }
            set { messageLine1 = value; OnPropertyChanged("MessageLine1"); }
        }


        private string messageLine2 = string.Empty;
        public string MessageLine2
        {
            get { return messageLine2; }
            set { messageLine2 = value; OnPropertyChanged("MessageLine2"); }
        }


        private string loadQtyHeader = string.Empty;
        public string LoadQtyHeader
        {
            get { return loadQtyHeader; }
            set { loadQtyHeader = value; OnPropertyChanged("LoadQtyHeader"); }
        }



        private string traxnQtyUOMHeader = string.Empty;
        public string TraxnQtyUOMHeader
        {
            get { return traxnQtyUOMHeader; }
            set { traxnQtyUOMHeader = value; OnPropertyChanged("TraxnQtyUOMHeader"); }
        }

        #endregion

        #region Date

        #endregion

        #region Collection

        private ObservableCollection<AddLoadDetails> addLoadDetailsCollection = new ObservableCollection<AddLoadDetails>();
        public ObservableCollection<AddLoadDetails> AddLoadDetailsCollection
        {
            get { return addLoadDetailsCollection; }
            set { addLoadDetailsCollection = value; OnPropertyChanged("AddLoadDetailsCollection"); }
        }



        private AddLoadDetails addLoadDetailsSelectedItem = new AddLoadDetails();
        public AddLoadDetails AddLoadDetailsSelectedItem
        {
            get { return addLoadDetailsSelectedItem; }
            set { addLoadDetailsSelectedItem = value; OnPropertyChanged("AddLoadDetailsSelectedItem"); }
        }




        private ObservableCollection<AddLoadDetails> addLoadDetailsSelectedItems = new ObservableCollection<AddLoadDetails>();
        public ObservableCollection<AddLoadDetails> AddLoadDetailsSelectedItems
        {
            get { return addLoadDetailsSelectedItems; }
            set { addLoadDetailsSelectedItems = value; OnPropertyChanged("AddLoadDetailsSelectedItems"); }
        }




        private ObservableCollection<AddLoadDetails> addLoadSearchItemsDetailsCollection = new ObservableCollection<AddLoadDetails>();
        private ObservableCollection<AddLoadDetails> AddLoadSearchItemsDetailsRefList = new ObservableCollection<AddLoadDetails>();
        public ObservableCollection<AddLoadDetails> AddLoadSearchItemsDetailsCollection
        {
            get { return addLoadSearchItemsDetailsCollection; }
            set { addLoadSearchItemsDetailsCollection = value; OnPropertyChanged("AddLoadSearchItemsDetailsCollection"); }
        }




        private AddLoadDetails addLoadSearchItemsDetailsSelectedItem = new AddLoadDetails();
        public AddLoadDetails AddLoadSearchItemsDetailsSelectedItem
        {
            get { return addLoadSearchItemsDetailsSelectedItem; }
            set { addLoadSearchItemsDetailsSelectedItem = value; OnPropertyChanged("AddLoadSearchItemsDetailsSelectedItem"); }
        }




        private ObservableCollection<AddLoadDetails> addLoadSearchItemsDetailsSelectedItems = new ObservableCollection<AddLoadDetails>();
        public ObservableCollection<AddLoadDetails> AddLoadSearchItemsDetailsSelectedItems
        {
            get { return addLoadSearchItemsDetailsSelectedItems; }
            set { addLoadSearchItemsDetailsSelectedItems = value; OnPropertyChanged("AddLoadSearchItemsDetailsSelectedItems"); }
        }
        #endregion

        #endregion

        #region Commands Declaretion
        public DelegateCommand ClearSearchText { get; set; }
        public DelegateCommand Save { get; set; }
        public DelegateCommand Delete { get; set; }
        public DelegateCommand Print { get; set; }
        public DelegateCommand Pick { get; set; }
        public DelegateCommand SearchItemOnType { get; set; }
        public DelegateCommand AddSearchItems { get; set; }
        public DelegateCommand UpdateAvailableQty { get; set; }
        public DelegateCommand DeleteAddLoadYes { get; set; }
        public DelegateCommand DeleteAddLoadNo { get; set; }
        public DelegateCommand SaveAddLoadYes { get; set; }
        public DelegateCommand SaveAddLoadNo { get; set; }
        public DelegateCommand ContinuePickYes { get; set; }
        public DelegateCommand ContinuePickNo { get; set; }
        public DelegateCommand SaveAddLoadConfirmationYes { get; set; }
        public DelegateCommand SaveAddLoadConfirmationNo { get; set; }
        public DelegateCommand ConfirmationPickYes { get; set; }
        public DelegateCommand ConfirmationPickNo { get; set; }
        public DelegateCommand TwoLineAlertOKCommand { get; set; }
        public DelegateCommand TransactionUMChange { get; set; }
        public DelegateCommand RegenerateSuggestion { get; set; }
        public DelegateCommand ContinueRegenerateYes { get; set; }
        public DelegateCommand ContinueRegenerateNo { get; set; }
        public DelegateCommand PickHeldReturn { get; set; }
        public DelegateCommand PrintHeldReturn { get; set; }

        public DelegateCommand PrintYesCommand { get; set; }
        public DelegateCommand PrintNoCommand { get; set; }
        //          

        #endregion

        #region Command Defination

        public void InitialiseCommands()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:InitialiseCommands]\t" + DateTime.Now + "");
            ClearSearchText = new DelegateCommand((param) =>
           {
               SearchText = "";
               SearchItemOnType.Execute(SearchText);
           });
            Save = new DelegateCommand((param) =>
            {
                //1
                SaveExecute();

            });
            Delete = new DelegateCommand((param) =>
            {
                //1
                DeleteExecute();

                //After changing the UOM when user selects the all items for delete, 
                //the datacontext get cleared but, the row on screen is not getting cleared (data row persisting on screen)
                //Hence, added below screen to refresh the grd screen to avoid the issue
                if (param != null)
                {
                    Telerik.Windows.Controls.RadGridView gridViewAddLoadDetails = param as Telerik.Windows.Controls.RadGridView;

                    if (gridViewAddLoadDetails != null)
                    {
                        gridViewAddLoadDetails.Rebind();
                    }
                }

            });
            //Print = new DelegateCommand((param) =>
            //{
            //    if (param != null)
            //    {
            //        PrintExecute();
            //    }
            //});
            Pick = new DelegateCommand((param) =>
            {
                PickExecute();
            });
            SearchItemOnType = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    SearchItem(param as string);
                }
            });
            AddSearchItems = new DelegateCommand((param) =>
            {
                AddSearchItemsExecute();
            });
            UpdateAvailableQty = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    AddLoadDetails obj = (AddLoadDetails)param;
                    UpdateAvailableQtyExecute(obj);
                }
            });
            DeleteAddLoadYes = new DelegateCommand((param) =>
            {
                DeleteAddLoadYesExecute();
            });
            DeleteAddLoadNo = new DelegateCommand((param) =>
            {
                DeleteAddLoadNoExecute();
            });
            SaveAddLoadYes = new DelegateCommand((param) =>
            {
                SaveAddLoadYesExecute();
            });
            SaveAddLoadNo = new DelegateCommand((param) =>
            {
                SaveAddLoadNoExecute();
            });
            ContinuePickYes = new DelegateCommand((param) =>
            {
                ContinuePickYesExecute();
            });
            ContinuePickNo = new DelegateCommand((param) =>
            {
                ContinuePickNoExecute();
            });
            SaveAddLoadConfirmationYes = new DelegateCommand((param) =>
            {
                SaveAddLoadConfirmationYesExecute();
            });
            SaveAddLoadConfirmationNo = new DelegateCommand((param) =>
            {
                SaveAddLoadConfirmationNoExecute();
            });
            ConfirmationPickYes = new DelegateCommand((param) =>
            {
                ConfirmationPickYesExecute();
            });
            ConfirmationPickNo = new DelegateCommand((param) =>
            {
                ConfirmationPickNoExecute();
            });
            TwoLineAlertOKCommand = new DelegateCommand((param) =>
            {
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                //Navigate to replenishment tab 
                //Set replenishment status to in process
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadView, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = ViewModelMappings.TabView.RouteHome.Replenishment };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });
            TransactionUMChange = new DelegateCommand((param) =>
            {

                //Save button enable disable 
                //AddLoadDetailsSelectedItem.SelectedQtyUOM  QtyUOMClass SelectedQtyUOM
                if (param != null)
                {
                    QtyUOMClass SelectedQtyUOM = (QtyUOMClass)param;
                    if (AddLoadDetailsSelectedItem != null)
                    {
                        if (AddLoadDetailsSelectedItem.SelectedQtyUOM != SelectedQtyUOM)
                        {
                            TotalLoadQuantity = AddLoadDetailsCollection.Sum(x => x.LoadQuantity);
                        }
                    }
                }

            });

            RegenerateSuggestion = new DelegateCommand((param) =>
            {
                //Show popup
                RegenerateSuggestionExecute();
            });

            ContinueRegenerateYes = new DelegateCommand((param) =>
            {
                //Run Suggestion Algo
                ContinueRegenerateYesExecute();
            });

            ContinueRegenerateNo = new DelegateCommand((param) =>
            {

                ContinueRegenerateNoExecute();
            });

            PickHeldReturn = new DelegateCommand((param) =>
            {

                PickHeldReturnExecute();
            });

            PrintHeldReturn = new DelegateCommand((param) =>
            {

                PrintHeldReturnExecute();
            });

            PrintYesCommand = new DelegateCommand((param) =>
            {
                PrintYesExecute();
            });
            PrintNoCommand = new DelegateCommand((param) =>
            {
                PrintNoExecute();
            });
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:InitialiseCommands]\t");
            //   
        }

        public void SaveExecute()
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:SaveExecute]\t" + DateTime.Now + "");

            try
            {
                if (PayloadManager.RouteReplenishmentPayload.SelectedReplnId == "New")
                {
                    string replnType = (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishAddLoadView.ToString()) ? "AddLoad" :
                        (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishSggestionView.ToString()) ? "Suggestion" :
                        (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnView.ToString()) ? "sugg. return" :
                        (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishUnloadView.ToString()) ? "unload" : "held return";

                    string status = PayloadManager.RouteReplenishmentPayload.IsSalesBranch || replnType == "sugg. return" || replnType == "held return" || replnType == "unload" ? "RTP" : "INPRS";

                    PayloadManager.RouteReplenishmentPayload.SelectedReplnId = ReplenishAddLoadManager.InsertReplenishmentHeader(status, FromBranch, ToBranch, replnType);

                    ReplnID = PayloadManager.RouteReplenishmentPayload.SelectedReplnId;

                    ReplnIDName = (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishAddLoadView.ToString()) ? "Add Load ID. : " :
                        (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishSggestionView.ToString()) ? "Suggestion Repln. ID. " :
                        (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnView.ToString()) ? "Suggestion Return ID. : " :
                        (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishUnloadView.ToString()) ? "Unload ID. : " : "Held Return ID. : ";

                    ReplnIDName = ReplnIDName + ReplnID;
                }
                if (ReplnID != "0")
                {
                    //Execute Save Functiionality 
                    bool flag = ReplenishAddLoadManager.SaveAddLoad(AddLoadDetailsCollection, ReplnID);
                    IsDirty = false;
                    IsEnableSave = false;
                    string loadType = PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishAddLoadView.ToString() ? "Add load" :
                        (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishSggestionView.ToString()) ? "Suggestion Repln." :
                        (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnView.ToString()) ? "Suggestion Return" :
                        (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishUnloadView.ToString()) ? "Unload" : "Held Return";
                    MessageLine1 = loadType + " transaction is saved successfully.";

                    if (PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                    {
                        //Popup for continue pick

                        MessageLine2 = "Do you want to continue pick?";
                        var dialog = new Helpers.DialogWindow { TemplateKey = "ContinuePickAddLoad", Title = "Confirmation" };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                    }
                    else
                    {

                        MessageLine2 = "Navigating to Route Home.";

                        var dialog = new Helpers.DialogWindow { TemplateKey = "TwoLineAlert", Title = "Save" };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                    }
                }
                else
                {
                    Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, please sync to continue" }, MessageToken);
                    return;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadViewModel SaveExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:SaveExecute]\t");
        }
        public void DeleteExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:DeleteExecute]\t" + DateTime.Now + "");
            IsEnableYesDelete = true;
            var dialog = new Helpers.DialogWindow { TemplateKey = "DeleteAddLoad", Title = "Delete Item(s)" };
            Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:DeleteExecute]\t");
        }
        //public void PrintExecute()
        //{

        //}
        CancellationTokenSource tokenForCancelTask = null;
        void SearchItem(string searchText)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:SearchItem]\t" + DateTime.Now + "");
            try
            {
                if (tokenForCancelTask != null)
                {
                    tokenForCancelTask.Cancel();
                    tokenForCancelTask = null;
                }
                tokenForCancelTask = new CancellationTokenSource();
                if (searchText.Trim().Length == 0 && searchText == "")
                {
                    Task.Run(async () => await SearchItemAsync(searchText, tokenForCancelTask.Token));
                    return;
                }
                if (searchText.Trim().Length < 3 && searchText != null)
                {
                    return;
                }

                tokenForCancelTask = new CancellationTokenSource();
                Task.Run(async () => await SearchItemAsync(searchText, tokenForCancelTask.Token), tokenForCancelTask.Token);
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadViewModel SaveExecute() error: " + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:SearchItem]\t");
        }
        private async Task SearchItemAsync(string searchText, CancellationToken token)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:SearchItem]\t" + DateTime.Now + "");

            try
            {
                await Task.Run(() =>
                {
                    AddLoadSearchItemsDetailsCollection = new ObservableCollection<AddLoadDetails>(AddLoadSearchItemsDetailsRefList.Where(i => i.ItemNo.ToLower().Contains(searchText.ToLower().Trim()) || i.ItemDescription.ToLower().Contains(searchText.ToLower().Trim())).ToList<AddLoadDetails>());
                    SearchItemsCount = AddLoadSearchItemsDetailsCollection.Count;
                    this.OnPropertyChanged("SearchItemsCount");
                    this.OnPropertyChanged("SearchResultText");
                });
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadViewModel SearchItemAsync() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:SearchItemAsync]\t");



        }
        public void AddSearchItemsExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:AddSearchItemsExecute]\t" + DateTime.Now + "");

            try
            {
                IsEnableAddItem = false;
                List<AddLoadDetails> collectionToRemove = new List<AddLoadDetails>();
                foreach (AddLoadDetails item in AddLoadSearchItemsDetailsSelectedItems)
                {
                    //item.QtyUOM = item.PrimaryUOM;
                    AddLoadDetailsCollection.Add(item);
                    collectionToRemove.Add(item);
                    IsEnableSave = true;
                }
                foreach (AddLoadDetails item in collectionToRemove)
                {
                    AddLoadSearchItemsDetailsCollection.Remove(item);
                    AddLoadSearchItemsDetailsRefList.Remove(item);

                }
                collectionToRemove.Clear();
                collectionToRemove = null;
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadViewModel AddSearchItemsExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:AddSearchItemsExecute]\t");

        }
        public void PickExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:PickExecute]\t" + DateTime.Now + "");

            try
            {
                //Save Add load and navigate to Pick

                bool flag = ReplenishAddLoadManager.SaveAddLoad(AddLoadDetailsCollection, ReplnID);
                if (flag)
                {
                    PayloadManager.RouteReplenishmentPayload.CurrentViewName = (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishAddLoadView.ToString()) ? ViewModelMappings.View.ReplenishAddLoadPickView.ToString() : (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishSggestionView.ToString()) ? ViewModelMappings.View.ReplenishSuggestionPickView.ToString() : (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnView.ToString()) ? ViewModelMappings.View.SuggestionReturnPickView.ToString() : ViewModelMappings.View.HeldReturnPickView.ToString();

                    ReplenishAddLoadManager.UpdateAddLoadHeader("HOP", PayloadManager.RouteReplenishmentPayload.SelectedReplnId);

                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReplenishAddLoadPickView, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadView, CloseCurrentView = true, ShowAsModal = false, Refresh = false };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                }

            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadViewModel PickExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:PickExecute]\t");




        }
        public void DeleteAddLoadYesExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:DeleteAddLoadYesExecute]\t" + DateTime.Now + "");
            try
            {
                if (!IsEnableYesDelete)
                {
                    return;
                }
                IsEnableYesDelete = false;
                DeleteAsync();
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadViewModel DeleteAddLoadYesExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:DeleteAddLoadYesExecute]\t");
        }


        void DeleteAsync()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:deleteAsync]\t" + DateTime.Now + "");

            try
            {
                if (PayloadManager.RouteReplenishmentPayload.SelectedReplnId != "New")
                {
                    ReplenishAddLoadManager.DeleteSavedItem(AddLoadDetailsSelectedItems, PayloadManager.RouteReplenishmentPayload.SelectedReplnId);
                }

                foreach (AddLoadDetails item in AddLoadDetailsSelectedItems)
                {
                    AddLoadDetails itemToRemove = AddLoadDetailsCollection.FirstOrDefault(x => x.ItemId == item.ItemId);
                    AddLoadDetailsCollection.Remove(itemToRemove);
                }

                foreach (AddLoadDetails item in AddLoadDetailsSelectedItems)
                {
                    item.SelectedQtyUOM = item.QtyUOMCollection.FirstOrDefault(x => x.UMCode == item.PrimaryUOM);
                    item.LoadQuantity = 1;
                    item.QtyUOM = item.PrimaryUOM;
                    var a = AddLoadSearchItemsDetailsCollection.FirstOrDefault(x => x.ItemId == item.ItemId);
                    if (a == null)
                    {
                        AddLoadSearchItemsDetailsCollection.Insert(0, item);
                        AddLoadSearchItemsDetailsRefList.Insert(0, item);
                    }

                }
                AddLoadDetailsSelectedItems.Clear();
                //addLoadDetailsSelectedItem = null;
                //Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                IsBusy = false;

            }
            catch (Exception ex)
            {
                Logger.Error("RouteReplenishmentTabViewModel deleteAsync() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:deleteAsync]\t" + DateTime.Now + "");
        }
        public void DeleteAddLoadNoExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:DeleteAddLoadNoExecute]\t" + DateTime.Now + "");
            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:DeleteAddLoadNoExecute]\t" + DateTime.Now + "");
        }

        public void SaveAddLoadYesExecute()
        {
            //Save ADD load and ask for pick load popup
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:SaveAddLoadYesExecute]\t" + DateTime.Now + "");
            IsDirty = false;
            IsEnableSave = false;
            var dialog = new Helpers.DialogWindow { TemplateKey = "ContinuePickAddLoad", Title = "Pick" };
            Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:SaveAddLoadYesExecute]\t" + DateTime.Now + "");
        }
        public void SaveAddLoadNoExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:SaveAddLoadNoExecute]\t" + DateTime.Now + "");            
            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:SaveAddLoadNoExecute]\t" + DateTime.Now + "");

        }
        public void ContinuePickYesExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:ContinuePickYesExecute]\t" + DateTime.Now + "");

            try
            {
                // Navigate to pick screen
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);


                PayloadManager.RouteReplenishmentPayload.CurrentViewName =
                    (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishAddLoadView.ToString()) ?
                    ViewModelMappings.View.ReplenishAddLoadPickView.ToString() :
                    (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishSggestionView.ToString()) ?
                    ViewModelMappings.View.ReplenishSuggestionPickView.ToString() :
                    (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnView.ToString()) ?
                    ViewModelMappings.View.SuggestionReturnPickView.ToString() :
                    (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishUnloadView.ToString()) ?
                    ViewModelMappings.View.ReplenishUnloadPickView.ToString() : ViewModelMappings.View.HeldReturnPickView.ToString();
                if (PayloadManager.RouteReplenishmentPayload.Status != "HOP")
                {
                    ReplenishAddLoadManager.UpdateAddLoadHeader("HOP", PayloadManager.RouteReplenishmentPayload.SelectedReplnId);
                }
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReplenishAddLoadPickView, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadView, CloseCurrentView = true, ShowAsModal = false, Refresh = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);

            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadViewModel ContinuePickYesExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:ContinuePickYesExecute]\t");
        }
        public void ContinuePickNoExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:ContinuePickNoExecute]\t" + DateTime.Now + "");

            try
            {
                //Navigate to Replenishment tab
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadView, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = ViewModelMappings.TabView.RouteHome.Replenishment };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadViewModel ContinuePickNoExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:ContinuePickNoExecute]\t");



        }

        // 
        public void UpdateAvailableQtyExecute(AddLoadDetails param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:UpdateAvailableQtyExecute]\t" + DateTime.Now + "");

            try
            {
                if (!PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment)
                {
                    TotalLoadQuantity = AddLoadDetailsCollection.Sum(x => x.LoadQuantity);
                }
                param.IsFocusableTextBox = false;
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadViewModel UpdateAvailableQtyExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:UpdateAvailableQtyExecute]\t");
        }

        public void SaveAddLoadConfirmationYesExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:SaveAddLoadConfirmationYesExecute]\t" + DateTime.Now + "");
            var dialog = new Helpers.DialogWindow { TemplateKey = "ConfirmationPickAddLoad", Title = "Confirmation" };
            Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:SaveAddLoadConfirmationYesExecute]\t");
        }

        public void SaveAddLoadConfirmationNoExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:SaveAddLoadConfirmationNoExecute]\t" + DateTime.Now + "");
            try
            {
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadView, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = ViewModelMappings.TabView.RouteHome.Replenishment };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadViewModel SaveAddLoadConfirmationNoExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:SaveAddLoadConfirmationNoExecute]\t");
        }
        public void ConfirmationPickYesExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:ConfirmationPickYesExecute]\t" + DateTime.Now + "");
            try
            {
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);

                PayloadManager.RouteReplenishmentPayload.CurrentViewName = (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishAddLoadView.ToString()) ? ViewModelMappings.View.ReplenishAddLoadPickView.ToString() : (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishSggestionView.ToString()) ? ViewModelMappings.View.ReplenishSuggestionPickView.ToString() : (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnView.ToString()) ? ViewModelMappings.View.SuggestionReturnPickView.ToString() : ViewModelMappings.View.HeldReturnPickView.ToString();
                ReplenishAddLoadManager.UpdateAddLoadHeader("HOP", PayloadManager.RouteReplenishmentPayload.SelectedReplnId);

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReplenishAddLoadPickView, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadView, CloseCurrentView = true, ShowAsModal = false, Refresh = false, SelectTab = ViewModelMappings.TabView.RouteHome.Replenishment };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadViewModel ConfirmationPickYesExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:ConfirmationPickYesExecute]\t");
        }
        public void ConfirmationPickNoExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:ConfirmationPickNoExecute]\t" + DateTime.Now + "");
            try
            {
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadView, CloseCurrentView = true, ShowAsModal = false, Refresh = false, SelectTab = ViewModelMappings.TabView.RouteHome.Replenishment };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadViewModel ConfirmationPickNoExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:ConfirmationPickNoExecute]\t");
        }


        public void RegenerateSuggestionExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:RegenerateSuggestionExecute]\t" + DateTime.Now + "");
            //IsBusy = true;
            IsEnableYesDelete = true;
            MessageLine1 = "This will regenarete suggestion.";
            MessageLine2 = "Do you want to continue?";
            var dialog = new Helpers.DialogWindow { TemplateKey = "RegenerateSugg", Title = "Regenerate Suggestion" };
            Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:RegenerateSuggestionExecute]\t");
        }


        public void ContinueRegenerateYesExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:ContinueRegenerateYesExecute]");
            try
            {
                if (!IsEnableYesDelete)
                {
                    return;
                }
                IsEnableYesDelete = false;
                ObservableCollection<AddLoadDetails> SuggesionAlgo = new ObservableCollection<AddLoadDetails>();
                if (PayloadManager.RouteReplenishmentPayload.IsSuggestionReplenishment)
                {
                    SuggesionAlgo = ReplenishAddLoadManager.GetSuggestionsReplnFromAlgo;
                }
                else
                {
                    SuggesionAlgo = ReplenishAddLoadManager.GetSuggestionsReturnFromAlgo;
                }
                foreach (AddLoadDetails item in AddLoadDetailsCollection)
                {

                    item.QtyUOM = item.PrimaryUOM;
                    item.SelectedQtyUOM = item.QtyUOMCollection == null ? new QtyUOMClass() : item.QtyUOMCollection.Count > 0 ? item.QtyUOMCollection[0] : new QtyUOMClass();
                    item.LoadQuantity = 1;
                    AddLoadSearchItemsDetailsCollection.Insert(0, item);
                }
                AddLoadDetailsCollection.Clear();

                foreach (AddLoadDetails obj in SuggesionAlgo)
                {
                    AddLoadDetailsCollection.Add(obj);

                    //Remove item from SearchDetailsCollection
                    AddLoadDetails itemInSearch = AddLoadSearchItemsDetailsCollection.FirstOrDefault(x => x.ItemId.Trim() == obj.ItemId.Trim());
                    if (itemInSearch != null)
                    {
                        AddLoadSearchItemsDetailsCollection.Remove(itemInSearch);
                    }
                }
                if (!PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment)
                {
                    TotalLoadQuantity = AddLoadDetailsCollection.Sum(x => x.LoadQuantity);
                }
                IsBusy = false;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][ContinueRegenerateYesExecute][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:ContinueRegenerateYesExecute]");
            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);

        }

        public void ContinueRegenerateNoExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:ContinueRegenerateNoExecute]");
            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
            IsBusy = false;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:ContinueRegenerateNoExecute]");
        }

        public void PickHeldReturnExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:PickHeldReturnExecute]\t" + DateTime.Now + "");
            try
            {
                if (PayloadManager.RouteReplenishmentPayload.SelectedReplnId == "New")
                {
                    string status = "RTP";
                    string replnType = "held return";
                    PayloadManager.RouteReplenishmentPayload.SelectedReplnId = ReplenishAddLoadManager.InsertReplenishmentHeader(status, FromBranch, ToBranch, replnType);
                    ReplnID = PayloadManager.RouteReplenishmentPayload.SelectedReplnId;
                    bool flag = ReplenishAddLoadManager.SaveHeldReturn(AddLoadDetailsCollection, ReplnID);
                }
                ReplnID = PayloadManager.RouteReplenishmentPayload.SelectedReplnId;
                ReplnIDName = "Held Return ID. : ";
                ReplnIDName = ReplnIDName + ReplnID;
                IsDirty = false;
                IsEnableSave = false;

                //Navigate to pick screen
                PayloadManager.RouteReplenishmentPayload.CurrentViewName = ViewModelMappings.View.HeldReturnPickView.ToString();

                ReplenishAddLoadManager.UpdateAddLoadHeader("HOP", PayloadManager.RouteReplenishmentPayload.SelectedReplnId);
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReplenishAddLoadPickView, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadView, CloseCurrentView = true, ShowAsModal = false, Refresh = false, SelectTab = ViewModelMappings.TabView.RouteHome.Replenishment };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadViewModel PickHeldReturnExecute() error: " + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:PickHeldReturnExecute]\t");

        }

        public void PrintHeldReturnExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:PrintHeldReturnExecute]\t" + DateTime.Now + "");
            try
            {
                IsEnablePrintYes = true;

                MessageLine1 = "Do you want to print this Held Return?";

                var dialog = new Helpers.DialogWindow { TemplateKey = "PrintReplenishment", Title = "Print" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadViewModel PrintHeldReturnExecute() error: " + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:PrintHeldReturnExecute]\t");

        }

        public void PrintYesExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:PrintYesExecute]\t" + DateTime.Now + "");
            IsEnablePrintYes = false;
            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
            PrintAsync();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:PrintYesExecute]\t");

        }

        public async void PrintAsync()
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:PrintAsync]\t" + DateTime.Now + "");

                try
                {
                    PayloadManager.RouteReplenishmentPayload.NavigatedForVoid = false;
                    PayloadManager.RouteReplenishmentPayload.CurrentViewName = ViewModelMappings.View.HeldReturnView.ToString();

                    #region Held Region
                    if (PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                    {
                        if (PayloadManager.RouteReplenishmentPayload.Status.ToUpper() == "RTP" || PayloadManager.RouteReplenishmentPayload.Status.ToUpper() == "HOP")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                        }
                        else if (PayloadManager.RouteReplenishmentPayload.Status.ToUpper() == "CMPLT")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = true;
                        }
                    }
                    else
                    {
                        if (PayloadManager.RouteReplenishmentPayload.Status.ToUpper() == "RTP" || PayloadManager.RouteReplenishmentPayload.Status.ToUpper() == "HOP" || PayloadManager.RouteReplenishmentPayload.Status.ToUpper() == "PIC")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                        }
                        else if (PayloadManager.RouteReplenishmentPayload.Status.ToUpper() == "CMPLT" || PayloadManager.RouteReplenishmentPayload.Status.ToUpper() == "RELS")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = true;
                        }
                    }
                    #endregion
                    ReplenishmentDetails SelectedReplenishment = new ReplenishmentDetails();
                    SelectedReplenishment.Status = PayloadManager.RouteReplenishmentPayload.Status.ToUpper();
                    SelectedReplenishment.ReplnID = PayloadManager.RouteReplenishmentPayload.SelectedReplnId;
                    ReportEngine.IReportSLE ReportData = null;
                    ReportData = new ReportViewModels.HeldReturnLoadReport(SelectedReplenishment, ReplenishAddLoadManager.GetAddLoadDetails(SelectedReplenishment.ReplnID, PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet));
                    var DocInfo = new ReportEngine.DocumentInformation();
                    var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());
                    repEngine.Log = this.Logger;
                    var SourceDocumentNo = 0m;
                    if (PayloadManager.RouteReplenishmentPayload.SelectedReplnId != "New")
                    {
                         SourceDocumentNo = Convert.ToDecimal(SelectedReplenishment.ReplnID);
                    }
                    DocInfo = repEngine.Generate<Reporting.HeldReturnLoad>(SourceDocumentNo, true, ReportEngine.ReportCategory.Route, ReportEngine.ReportsTypes.HeldReturnLoadReport, ReportData, null, ReportEngine.ContentTypes.PDF);

                    if (DocInfo.IsEndOfSequence)
                    {
                        //if (System.Windows.Application.Current != null)
                        //{
                            //System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)delegate // <--- HERE
                            //{
                                Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, Please sync to continue" }, MessageToken);
                        //    });
                        //}
                        return;
                    }
                    
                    if (DocInfo.IsDocumentCreated)
                        ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.Route_Replenishment_Header", "ReplenishmentID", SourceDocumentNo, DocInfo.DocumentId);
                    SelectedReplenishment.DocumentID = DocInfo.DocumentId; //Replace Latest

                    var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Route, SelectedReplenishment.DocumentID, null);
                    if (ReportBytes == null)
                    {
                        //if (System.Windows.Application.Current != null)
                        //{
                        //    System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)delegate // <--- HERE
                        //    {
                                Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Report not generated, Please try again." }, MessageToken);
                        //    });
                        //}
                        return;
                    }



                    SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
                    Payload.ReportFile = ReportBytes;
                    Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                    Payload.PreviousView = ViewModelMappings.View.RouteHome;
                    Payload.CurrentViewTitle = "Held Retrun Replenishment Report";
                    Payload.PreviousViewTitle = "Held Return Replenishment";
                    Payload.MessageToken = this.MessageToken;

                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.SelectTab = ViewModelMappings.TabView.RouteHome.Replenishment;

                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.RouteHome, CloseCurrentView = true, Payload = Payload, ShowAsModal = false };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);

                    //TODO: CAPTURE PRINT ENDED EVENT AND PUT BELOW ALERT IN THAT
                    ////System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    ////{
                    ////    Messenger.Default.Send<AlertWindow>(new AlertWindow() { Message = "Print Completed." }, MessageToken);
                    ////}));
                }
                catch (Exception ex)
                {
                    Logger.Error("RouteReplenishmentTabViewModel PrintAsync() error: " + ex.Message);
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:PrintAsync]\t");

            });

        }
        public void PrintNoExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:PrintNoExecute]\t" + DateTime.Now + "");
            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:PrintNoExecute]\t");
        }
        //
        #endregion

        #region Constructor & Methods
        public ReplenishAddLoadViewModel()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:Constructor]\t" + DateTime.Now + "");
            InitialiseCommands();
            InitialiseVariables();
            //GetQtyUOMCollection();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:ReplenishAddLoadViewModel]\t");
        }
        public void InitialiseVariables()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:InitialiseVariables]\t" + DateTime.Now + "");

            try
            {
                GetReplenishmentDetails();

                SelectedTab = PayloadManager.RouteReplenishmentPayload.IsNavigatedFromRouteHome ? ViewModelMappings.TabView.RouteHome.Dashboard : ViewModelMappings.TabView.RouteHome.Replenishment;

                ReplnID = PayloadManager.RouteReplenishmentPayload.SelectedReplnId;
                FromBranch = PayloadManager.RouteReplenishmentPayload.FromBranch;
                ToBranch = PayloadManager.RouteReplenishmentPayload.ToBranch;
                RouteNameAddLoad = PayloadManager.RouteReplenishmentPayload.RouteNameAddLoad;
                IsEnableGridEdit = PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment ? false : true;

                ShowShippedQty = (!PayloadManager.RouteReplenishmentPayload.IsSalesBranch && PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck && (PayloadManager.RouteReplenishmentPayload.Status.Equals("CMPLT") || PayloadManager.RouteReplenishmentPayload.Status.Equals("HOP") || PayloadManager.RouteReplenishmentPayload.Status.Equals("RTP")));
                AddLoadDetailsSelectedItems.CollectionChanged += AddLoadDetailsSelectedItems_CollectionChanged;


                if (PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment)
                {
                    foreach (AddLoadDetails item in AddLoadDetailsCollection)
                    {
                        item.IsEnableGrid = false;
                    }
                    foreach (AddLoadDetails item in AddLoadSearchItemsDetailsCollection)
                    {
                        item.IsEnableGrid = false;
                    }
                }
                else
                {
                    GetAddLoadSearchItemsDetails();
                    AddLoadSearchItemsDetailsSelectedItems.CollectionChanged += AddLoadSearchItemsDetailsSelectedItems_CollectionChanged;
                }
                //PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = false;
                if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishAddLoadView.ToString())
                {
                    IsSuggestionView = false;
                    FromDateToDateTitle = "";
                    ReplnIDName = "Add Load ID. : " + ReplnID;
                    LoadQtyHeader = "Load Qty.";
                    TraxnQtyUOMHeader = "Load Qty UOM";
                    IsVisibleContinuePick = PayloadManager.RouteReplenishmentPayload.Status == "HOP" ? true : false;

                }
                else if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishSggestionView.ToString())
                {
                    IsVisibleRegenerateSuggestion = PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment ? false : true;
                    IsSuggestionView = true;
                    FromDateToDateTitle = "From - " + PayloadManager.RouteReplenishmentPayload.FromDate.ToString("MM/dd/yyyy") + " | To - " + PayloadManager.RouteReplenishmentPayload.ToDate.ToString("MM/dd/yyyy");
                    ReplnIDName = "Suggestion Repln. ID. : " + ReplnID;
                    LoadQtyHeader = "Repln. Qty.";
                    TraxnQtyUOMHeader = "Repln. Qty UOM";
                    IsVisibleContinuePick = PayloadManager.RouteReplenishmentPayload.Status == "HOP" ? true : false;

                }
                else if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnView.ToString())
                {
                    IsVisibleRegenerateSuggestion = PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment ? false : true;
                    IsSuggestionView = true;
                    FromDateToDateTitle = "From - " + PayloadManager.RouteReplenishmentPayload.FromDate.ToString("MM/dd/yyyy") + " | To - " + PayloadManager.RouteReplenishmentPayload.ToDate.ToString("MM/dd/yyyy");
                    ReplnIDName = "Suggestion Return ID. : " + ReplnID;
                    LoadQtyHeader = "Quantity Return";
                    TraxnQtyUOMHeader = "Return Qty UOM";
                    IsVisibleContinuePick = PayloadManager.RouteReplenishmentPayload.Status == "HOP" ? true : false;
                }
                else if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishUnloadView.ToString())
                {
                    IsVisibleRegenerateSuggestion = false;
                    IsSuggestionView = false;
                    FromDateToDateTitle = "";
                    ReplnIDName = "Unload ID. : " + ReplnID;
                    LoadQtyHeader = "Quantity Return";
                    TraxnQtyUOMHeader = "Return Qty UOM";
                    IsVisibleContinuePick = PayloadManager.RouteReplenishmentPayload.Status == "HOP" ? true : false;
                }
                else if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.HeldReturnView.ToString())
                {
                    IsVisibleRegenerateSuggestion = false;
                    IsSuggestionView = false;
                    IsHeldReturn = PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet ? false : true;
                    IsEnablePick = PayloadManager.RouteReplenishmentPayload.SelectedReplnId == "New" ? true : false;
                    FromDateToDateTitle = "";
                    ReplnIDName = "Held Return ID. : " + ReplnID;
                    LoadQtyHeader = "Quantity Return";
                    TraxnQtyUOMHeader = "Transaction UOM";
                    IsVisibleContinuePick = PayloadManager.RouteReplenishmentPayload.Status == "HOP" && PayloadManager.RouteReplenishmentPayload.SelectedReplnId != "New" ? true : false;
                    IsShowPrint = PayloadManager.RouteReplenishmentPayload.SelectedReplnId != "New" ? true : false; 

                }
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadViewModel InitialiseVariables() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:InitialiseVariables]\t");
        }

        void AddLoadSearchItemsDetailsSelectedItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:AddLoadSearchItemsDetailsSelectedItems_CollectionChanged]\t" + DateTime.Now + "");
            //Enable disable Add button
            IsEnableAddItem = AddLoadSearchItemsDetailsSelectedItems.Count > 0 ? true : false;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:AddLoadSearchItemsDetailsSelectedItems_CollectionChanged]\t");
        }

        void AddLoadDetailsSelectedItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:AddLoadDetailsSelectedItems_CollectionChanged]\t" + DateTime.Now + "");
            //Enable disable delete button
            IsEnableDelete = AddLoadDetailsSelectedItems.Count > 0 ? true : false;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:AddLoadDetailsSelectedItems_CollectionChanged]\t");
        }

        public void GetReplenishmentDetails()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:GetReplenishmentDetails]\t" + DateTime.Now + "");

            try
            {
                AddLoadDetailsCollection = new ObservableCollection<AddLoadDetails>();
                if (PayloadManager.RouteReplenishmentPayload.SelectedReplnId != "New")
                {
                    if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.HeldReturnView.ToString())
                    {
                        AddLoadDetailsCollection = ReplenishAddLoadManager.GetHeldReturnDetails(PayloadManager.RouteReplenishmentPayload.SelectedReplnId, PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet);
                    }
                    else
                    {
                        AddLoadDetailsCollection = ReplenishAddLoadManager.GetAddLoadDetails(PayloadManager.RouteReplenishmentPayload.SelectedReplnId, PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet);

                    }
                }
                else if (PayloadManager.RouteReplenishmentPayload.SelectedReplnId == "New")
                {
                    if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnView.ToString() || PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishSggestionView.ToString())
                    {
                        // Get Suggestions from Algo
                        GetSuggestionsFromAlgo();
                    }
                    else if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.HeldReturnView.ToString())
                    {
                        AddLoadDetailsCollection = ReplenishAddLoadManager.GetNewHeldReturn;
                    }
                }


            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadViewModel GetReplenishmentDetails() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:GetReplenishmentDetails]\t");




        }

       
        async void GetAddLoadSearchItemsDetails()
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:GetAddLoadSearchItemsDetails]\t" + DateTime.Now + "");

                try
                {
                    IsSearching = true;
                    AddLoadSearchItemsDetailsCollection = new ObservableCollection<AddLoadDetails>();
                    List<string> itemsToEscape = new List<string>();
                    foreach (AddLoadDetails item in AddLoadDetailsCollection)
                    {
                        itemsToEscape.Add(item.ItemId.ToString());
                    }
                    if (!PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck)
                    {
                        // Get inventory present in truck
                        AddLoadSearchItemsDetailsCollection = ReplenishAddLoadManager.GetAddLoadSearchItemsDetails(itemsToEscape);
                    }
                    else
                    {
                        //Get inventory present in branch
                        AddLoadSearchItemsDetailsCollection = ReplenishAddLoadManager.GetAddLoadSearchItemsDetailsForBranchToTruck(itemsToEscape);
                        AddLoadSearchItemsDetailsCollection.OrderBy(x => x.ItemNo);
                    }
                    itemsToEscape = null;
                    AddLoadSearchItemsDetailsRefList = new ObservableCollection<AddLoadDetails>(AddLoadSearchItemsDetailsCollection.ToList<AddLoadDetails>());
                    IsSearching = false;
                }
                catch (Exception ex)
                {
                    Logger.Error("ReplenishAddLoadViewModel GetAddLoadSearchItemsDetails() error: " + ex.Message);
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:GetAddLoadSearchItemsDetails]\t");



            });

        }








        public void GetSuggestionsFromAlgo()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][Start:GetSuggestionsFromAlgo]\t" + DateTime.Now + "");

            try
            {
                if (PayloadManager.RouteReplenishmentPayload.IsSuggestionReplenishment)
                {
                    AddLoadDetailsCollection = ReplenishAddLoadManager.GetSuggestionsReplnFromAlgo;

                }
                else
                {
                    AddLoadDetailsCollection = ReplenishAddLoadManager.GetSuggestionsReturnFromAlgo;

                }
                if (!PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment)
                {
                    TotalLoadQuantity = AddLoadDetailsCollection.Sum(x => x.LoadQuantity);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadViewModel GetSuggestionsFromAlgo() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReplenishAddLoadViewModel][End:GetSuggestionsFromAlgo]\t");
        }

        public override bool ConfirmSave()
        {
            if (IsEnableSave)
            {
                if (PayloadManager.RouteReplenishmentPayload.SelectedReplnId == "New")
                {
                    string replnType = (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishAddLoadView.ToString()) ? "AddLoad" :
                        (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishSggestionView.ToString()) ? "Suggestion" :
                        (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnView.ToString()) ? "sugg. return" :
                        (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishUnloadView.ToString()) ? "unload" : "held return";

                    string status = PayloadManager.RouteReplenishmentPayload.IsSalesBranch || replnType == "sugg. return" || replnType == "held return" || replnType == "unload" ? "RTP" : "INPRS";

                    PayloadManager.RouteReplenishmentPayload.SelectedReplnId = ReplenishAddLoadManager.InsertReplenishmentHeader(status, FromBranch, ToBranch, replnType);

                    ReplnID = PayloadManager.RouteReplenishmentPayload.SelectedReplnId;

                    ReplnIDName = (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishAddLoadView.ToString()) ? "Add Load ID. : " :
                        (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishSggestionView.ToString()) ? "Suggestion Repln. ID. " :
                        (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnView.ToString()) ? "Suggestion Return ID. : " :
                        (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishUnloadView.ToString()) ? "Unload ID. : " : "Held Return ID. : ";

                    ReplnIDName = ReplnIDName + ReplnID;
                }

                bool flag = ReplenishAddLoadManager.SaveAddLoad(AddLoadDetailsCollection, ReplnID);
                IsEnableSave = false;
            }
            else
            {
                ReplenishAddLoadManager.UpdateAddLoadHeader("HOP", PayloadManager.RouteReplenishmentPayload.SelectedReplnId);
            }

            IsDirty = false;
            CanNavigate = true;
            return base.ConfirmSave();
        }
        public override bool ConfirmCancel()
        {
            IsDirty = false;
            CanNavigate = true;
            return base.ConfirmCancel();
        }
        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }

    }

}
