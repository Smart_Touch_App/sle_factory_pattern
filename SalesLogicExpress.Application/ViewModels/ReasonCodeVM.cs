﻿using log4net;
using SalesLogicExpress.Application.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class ReasonCodeVM : ViewModelBase
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.ReasonCodeVM");
        ReasonCode _SelectedReasonCode = null;
        public ReasonCode SelectedReasonCode
        {
            get
            {
                return _SelectedReasonCode;
            }
            set
            {
                _SelectedReasonCode = value;
                OnPropertyChanged("SelectedReasonCode");
            }
        }
        List<ReasonCode> _ReasonCodes = new List<ReasonCode>();
        public List<ReasonCode> ReasonCodes
        {
            get
            {
                return _ReasonCodes;
            }
            set
            {
                _ReasonCodes = value;
                OnPropertyChanged("ReasonCodes");
            }
        }
        public ReasonCodeVM()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReasonCodeVM][Start:Constructor]");
            LoadReasonCodes();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ReasonCodeVM][End:Constructor]");
        }

        async void LoadReasonCodes()
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ReasonCodeVM][Start:LoadReasonCodes]");
                ReasonCodes = new ReasonCodeManager().GetReasonCodes();
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ReasonCodeVM][End:LoadReasonCodes]");
            });
        }
        public class ReasonCode
        {
            public int ID { get; set; }
            public string Code { get; set; }
        }
        public enum ReasonCodeType
        {
            Order,
            ReturnOrder
        }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
