﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class RouteSettlementViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.RouteSettlementViewModel");

        #region Properties & Variables
        //public static DateTime SelectedCalendarDate = DateTime.Now;

        public Guid MessageToken { get; set; }

        private string routeName;
        public string RouteName
        {
            get { return routeName; }
            set { routeName = value; OnPropertyChanged("RouteName"); }
        }

        private string userRoute;
        public string UserRoute
        {
            get { return userRoute; }
            set { userRoute = value; OnPropertyChanged("UserRoute"); }
        }

        #endregion

        #region OnPropertyChanged

        private Brush brushEOD = (Brush)new BrushConverter().ConvertFromString("Red");

        public Brush BrushEOD
        {
            get { return brushEOD; }
            set { brushEOD = value; OnPropertyChanged("BrushEOD"); }
        }
        private Brush brushPendingVerif = (Brush)new BrushConverter().ConvertFromString("Red");

        public Brush BrushPendingVerif
        {
            get { return brushPendingVerif; }
            set { brushPendingVerif = value; OnPropertyChanged("BrushPendingVerif"); }
        }

        private double totalCash;
        public double TotalCash
        {
            get { return totalCash; }
            set { totalCash = value; OnPropertyChanged("TotalCash"); }
        }

        private double totalCheck;
        public double TotalCheck
        {
            get { return totalCheck; }
            set { totalCheck = value; OnPropertyChanged("TotalCheck"); }
        }

        private double totalExpenses;
        public double TotalExpenses
        {
            get { return totalExpenses; }
            set { totalExpenses = value; OnPropertyChanged("TotalExpenses"); }
        }

        private double totalMoneyOrder;
        public double TotalMoneyOrder
        {
            get { return totalMoneyOrder; }
            set { totalMoneyOrder = value; OnPropertyChanged("TotalMoneyOrder"); }
        }

        private double grandTotal;
        public double GrandTotal
        {
            get { return grandTotal; }
            set { grandTotal = value; OnPropertyChanged("GrandTotal"); }
        }
        private RouteSettlementSettlementsViewModel routeSettlementSettlementsVM;//= new RouteSettlementSettlementsViewModel();

        public RouteSettlementSettlementsViewModel RouteSettlementSettlementsVM
        {
            get { return routeSettlementSettlementsVM; }
            set { routeSettlementSettlementsVM = value; OnPropertyChanged("RouteSettlementSettlementsVM"); }
        }
        private RouteSettlemenntTransationsViewModel routeSettlemenntTransationsVM;//= new RouteSettlemenntTransationsViewModel();

        public RouteSettlemenntTransationsViewModel RouteSettlemenntTransationsVM
        {
            get { return routeSettlemenntTransationsVM; }
            set
            {
                routeSettlemenntTransationsVM = value; OnPropertyChanged("RouteSettlemenntTransationsVM");

            }
        }

        private bool isTransactionTabSelected;

        public bool IsTransactionTabSelected
        {
            get { return isTransactionTabSelected; }
            set
            {
                isTransactionTabSelected = value; OnPropertyChanged("IsTransactionTabSelected");
                IsBusy = true;
                TransactionTabSelectExe(value);
            }
        }


        private bool isSettlementTabSelected;

        public bool IsSettlementTabSelected
        {
            get { return isSettlementTabSelected; }
            set
            {
                isSettlementTabSelected = value;

                OnPropertyChanged("IsSettlementTabSelected");
                IsBusy = true;
                SettlementTabSelectExe(value);
            }
        }

        private bool isEnabledNewSettlement = false;

        public bool IsEnabledNewSettlement
        {
            get { return isEnabledNewSettlement; }
            set
            {
                isEnabledNewSettlement = value; OnPropertyChanged("IsEnabledNewSettlement");
            }
        }
        #endregion

        #region Commands
        public DelegateCommand ShowExpenses { get; set; }
        public DelegateCommand ShowChecks { get; set; }
        public DelegateCommand ShowMoneyOrder { get; set; }
        public DelegateCommand NewSettlement { get; set; }
        public DelegateCommand Print { get; set; }
        public DelegateCommand OkVoidSettlementOnTransactionTab { get; set; }
        public DelegateCommand CancelVoidSettlementOnTransactionTab { get; set; }
        //OkVoidSettlementOnTransactionTab  CancelVoidSettlementOnTransactionTab
        #endregion

        #region Constructor & Methods
        public RouteSettlementViewModel()
        {
            IsBusy = true;
            ViewModelPayload.PayloadManager.RouteSettlementPayload = new ViewModelPayload.RouteSettlementPayload();
            LoadRouteSettlement();

        }
        async void TransactionTabSelectExe(bool value)
        {
            await Task.Run(() =>
                {

                    Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementViewModel][Start:TransactionTabSelectExe]\t" + DateTime.Now + "");

                    try
                    {
                        if ((value && RouteSettlemenntTransationsVM == null) || ViewModelPayload.PayloadManager.RouteSettlementPayload.IsReinitialiseTransaction)
                        {
                            ViewModelPayload.PayloadManager.RouteSettlementPayload.IsReinitialiseTransaction = false;

                            RouteSettlemenntTransationsVM = new RouteSettlemenntTransationsViewModel();
                            RouteSettlemenntTransationsVM.MessageToken = this.MessageToken;
                            SetHeaderValues();
                        }
                        IsBusy = false;
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("RouteSettlementViewModel TransactionTabSelectExe() error: " + ex.Message);
                    }

                    Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementViewModel][End:TransactionTabSelectExe]\t");


                });
        }
        async void SettlementTabSelectExe(bool value)
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementViewModel][Start:SettlementTabSelectExe]\t" + DateTime.Now + "");

                try
                {
                    IsBusy = true;

                    if (value && RouteSettlementSettlementsVM == null)
                    {
                        if (RouteSettlementSettlementsVM == null)
                            RouteSettlementSettlementsVM = new RouteSettlementSettlementsViewModel();
                        RouteSettlementSettlementsVM.MessageToken = this.MessageToken;
                        RouteSettlementSettlementsVM.ParentViewModel = this;

                    }
                    IsBusy = false;
                }
                catch (Exception ex)
                {
                    Logger.Error("RouteSettlementViewModel SettlementTabSelectExe() error: " + ex.Message);
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementViewModel][End:SettlementTabSelectExe]\t");


            });
        }

        async void LoadRouteSettlement()
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementViewModel][Start:LoadRouteSettlement]\t" + DateTime.Now + "");

                try
                {
                    UserRoute = Managers.UserManager.UserRoute;
                    RouteName = Managers.UserManager.RouteName;
                    InitialiseCommands();
                    //if (ViewModelPayload.PayloadManager.RouteSettlementPayload.IsSettlementTabActive)
                    //{
                    //    IsSettlementTabSelected = true;
                    //    ViewModelPayload.PayloadManager.RouteSettlementPayload.IsSettlementTabActive = false;
                    //}
                    //else
                    //{
                    //    IsTransactionTabSelected = true;
                    //}
                    SetHeaderValues();
                    IsBusy = false;
                }
                catch (Exception ex)
                {
                    Logger.Error("RouteSettlementViewModel LoadRouteSettlement() error: " + ex.Message);
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementViewModel][End:LoadRouteSettlement]\t");

            });
        }
        public void InitialiseCommands()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementViewModel][Start:InitialiseCommands]\t" + DateTime.Now + "");
            ShowExpenses = new DelegateCommand((param) =>
            {

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ExpensesWindow, CurrentViewName = ViewModelMappings.View.RouteSettlement, CloseCurrentView = false, ShowAsModal = false, Refresh = true };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });
            ShowChecks = new DelegateCommand((param) => { });

            ShowMoneyOrder = new DelegateCommand((param) =>
            {

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.MoneyOrderWindow, CurrentViewName = ViewModelMappings.View.RouteSettlement, CloseCurrentView = false, ShowAsModal = false, Refresh = true };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });
            NewSettlement = new DelegateCommand((param) =>
            {
                IsBusy = true;
                //ViewModelPayload.PayloadManager.RouteSettlementPayload = (ViewModelPayload.PayloadManager.RouteSettlementPayload)this;
                NewSettlementeExecute();


            });
            //Print = new DelegateCommand((param) => { });
            OkVoidSettlementOnTransactionTab = new DelegateCommand((param) => { OkVoidSettlementOnTransactionTabExecute(); });
            CancelVoidSettlementOnTransactionTab = new DelegateCommand((param) => { CancelVoidSettlementOnTransactionTabExecute(); });
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementViewModel][End:InitialiseCommands]\t");
        }
        public void SetHeaderValues()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementViewModel][Start:SetHeaderValues]\t" + DateTime.Now + "");

            try
            {
                if (RouteSettlemenntTransationsVM != null)
                {
                    TotalCash = RouteSettlemenntTransationsVM.TotalCash;
                    TotalCheck = RouteSettlemenntTransationsVM.TotalCheck;
                    TotalMoneyOrder = RouteSettlemenntTransationsVM.TotalMoneyOrder;
                    TotalExpenses = RouteSettlemenntTransationsVM.TotalExpenses;
                    GrandTotal = RouteSettlemenntTransationsVM.GrandTotal;
                    RouteSettlemenntTransationsVM.MessageToken = this.MessageToken;
                    IsEnabledNewSettlement = RouteSettlemenntTransationsVM.Transactions.Count > 0 ? true : false;
                }
                bool flag = SettlementConfirmationManager.IsPendingVerification();

                if (flag)
                {
                    BrushPendingVerif = (Brush)new BrushConverter().ConvertFromString("Blue");

                }
                else
                {
                    BrushPendingVerif = (Brush)new BrushConverter().ConvertFromString("Gray");

                }
                flag = SettlementConfirmationManager.IsNonServicedStopPresent(UserRoute);
                if (flag)
                {
                    BrushEOD = (Brush)new BrushConverter().ConvertFromString("Gray");

                }
                else
                {
                    BrushEOD = (Brush)new BrushConverter().ConvertFromString("Green");

                }
            }
            catch (Exception ex)
            {
                Logger.Error("RouteSettlementViewModel SetHeaderValues() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementViewModel][End:SetHeaderValues]\t");


        }

        public void NewSettlementeExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementViewModel][Start:NewSettlementeExecute]\t" + DateTime.Now + "");

            try
            {
                //Check for Pending Settlement 

                bool flag = SettlementConfirmationManager.IsPendingVerification();
                if (flag)
                {

                    var dialog = new Helpers.DialogWindow { TemplateKey = "VoidSettlementOnTransactionTab", Title = "Alert" };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);


                    //Alert message for pending verification
                    //var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.PendimgVerification, MessageIcon = "Alert" };
                    //Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    IsBusy = false;
                    return;
                }
                else
                {
                    flag = SettlementConfirmationManager.IsNonServicedStopPresent(UserRoute);
                    if (flag)
                    {
                        var lst = SettlementConfirmationManager.GetRouteSettlementPendingList(UserRoute);
                        if (lst != null)
                        {
                            if (lst.Count > 0)
                            {
                                RouteSettlementPendingList = lst;
                                PendingCount = lst.Count();
                                var dialog = new Helpers.DialogWindow { TemplateKey = "OpenPendingActivityCustomerPopup", Title = "Please Complete the Pending Activities", Payload = this };
                                GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                            }
                        }
                        IsBusy = false;
                        return;
                        //Alert message for all stops are not serviced
                        //var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.ServiceStop, MessageIcon = "Alert" };
                        //Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken); IsBusy = false;
                        //return;
                    }
                    else
                    {
                        ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.EOD_Transaction.ToString();

                        ShowVerificationWindow();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("RouteSettlementViewModel NewSettlementeExecute() error: " + ex.Message);
            }
            IsBusy = false;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementViewModel][End:NewSettlementeExecute]\t");
        }

        private List<RouteSettlementPendingList> _RouteSettlementPendingList = new List<RouteSettlementPendingList>();
        public List<RouteSettlementPendingList> RouteSettlementPendingList
        {
            get
            {
                return _RouteSettlementPendingList;
            }
            set { _RouteSettlementPendingList = value; OnPropertyChanged("RouteSettlementPendingList"); }
        }

        private int _PendingCount;
        public int PendingCount
        {
            get { return _PendingCount; }
            set { _PendingCount = value; OnPropertyChanged("PendingCount"); }
        }

        public void ShowVerificationWindow()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementViewModel][Start:ShowVerificationWindow]\t" + DateTime.Now + "");

            try
            {
                IsBusy = true;
                ViewModelPayload.PayloadManager.RouteSettlementPayload.IsForVerification = false;
                ViewModelPayload.PayloadManager.RouteSettlementPayload.GrandTotal = GrandTotal;
                ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalCash = TotalCash;
                ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalCheck = TotalCheck;
                ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalMoneyOrder = TotalMoneyOrder;
                ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalExpenses = TotalExpenses;
                ViewModelPayload.PayloadManager.RouteSettlementPayload.MoneyOrderDetailsCollection = RouteSettlemenntTransationsVM.MoneyOrderDetailsCollection;
                ViewModelPayload.PayloadManager.RouteSettlementPayload.CheckDetailsCollection = RouteSettlemenntTransationsVM.CheckDetailsCollection;

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.SettlementConfirmation, CurrentViewName = ViewModelMappings.View.RouteSettlement, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                IsBusy = false;
            }
            catch (Exception ex)
            {
                Logger.Error("RouteSettlementViewModel ShowVerificationWindow() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementViewModel][End:ShowVerificationWindow]\t");



        }

        public void OkVoidSettlementOnTransactionTabExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementViewModel][Start:OkVoidSettlementOnTransactionTabExecute]\t" + DateTime.Now + "");

            try
            {
                // Void And execute
                string settlementID = SettlementConfirmationManager.GetNewSettlementID;
                SettlementConfirmationManager.VoidSettlement(settlementID, "VOID", "Void at new settlement");
                ViewModelPayload.PayloadManager.RouteSettlementPayload.IsReinitialiseTransaction = true;

                if (RouteSettlemenntTransationsVM  == null)
                    RouteSettlemenntTransationsVM = new RouteSettlemenntTransationsViewModel();
                RouteSettlemenntTransationsVM.MessageToken = this.MessageToken;

                if (RouteSettlementSettlementsVM == null)
                    RouteSettlementSettlementsVM = new RouteSettlementSettlementsViewModel();
                RouteSettlementSettlementsVM.MessageToken = this.MessageToken;
                SetHeaderValues();
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.EOD_Transaction.ToString();

                ShowVerificationWindow();
            }
            catch (Exception ex)
            {
                Logger.Error("RouteSettlementViewModel OkVoidSettlementOnTransactionTabExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementViewModel][End:OkVoidSettlementOnTransactionTabExecute]\t");


        }

        public void CancelVoidSettlementOnTransactionTabExecute()
        {
            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
        }
        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();

            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
