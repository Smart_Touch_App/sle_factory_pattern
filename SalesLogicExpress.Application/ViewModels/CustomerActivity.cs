﻿using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System.Runtime.Serialization.Json;
using System.IO;
using SalesLogicExpress.Application.ViewModelPayload;
using log4net;
using System.Globalization;

namespace SalesLogicExpress.Application.ViewModels
{
    public class ActivityListItem : ViewModelBase
    {
        public string Type { get; set; }

        public string Invoice { get; set; }

        public string Amount { get; set; }

        public DateTime Date { get; set; }

        bool _Visible = true;

        public bool Visible
        {
            get
            {
                return _Visible;
            }
            set
            {
                _Visible = value;
                OnPropertyChanged("Visible");
            }
        }

        public string Description { get; set; }

        public string State { get; set; }

        public string ItemType { get; set; }

        public string RouteSettlementId { get; set; }
    }

    public class OrderActivityListItem : ActivityListItem
    {
        public ViewModelPayload.OrderPayload Payload { get; set; }

        public string ActivityState { get; set; }

        public string ActivityTime { get; set; }

        public string Amount { get; set; }

        public string InvoiceNo { get; set; }

        public string ActivityStateKey { get; set; }

        public string CustomerID { get; set; }
    }

    public class PaymentActivityListItem : ActivityListItem
    {
        public ViewModelPayload.PaymentPayload Payload { get; set; }

        public string ActivityState { get; set; }

        public string ActivityTime { get; set; }

        public string Amount { get; set; }

        public string Type { get; set; }

        public string ChequeNo { get; set; }

        public List<string> InvoiceNo { get; set; }

        public string ActivityStateKey { get; set; }

        public string CustomerID { get; set; }

        public decimal UnappliedAmount { get; set; }
        //public string ItemType { get { return "PAY"; } }
    }

    public class ChangeActivityListItem : ActivityListItem
    {
        public ViewModelPayload.ChangePayload Payload { get; set; }

        public string ActivityState { get; set; }

        public string ActivityTime { get; set; }

        public string ActivityStateKey { get; set; }

        public string CustomerID { get; set; }
    }

    public class PreorderActivityListItem : ActivityListItem
    {
        public ViewModelPayload.PreOrderPayload Payload { get; set; }

        public string ActivityState { get; set; }

        public string ActivityTime { get; set; }

        public string Amount { get; set; }

        public string Type { get; set; }

        public string ChequeNo { get; set; }

        public string InvoiceNo { get; set; }

        public DateTime PreOrderDate { get; set; }

        public string ItemOrdered { get; set; }

        public string ActivityStateKey { get; set; }

        public string CustomerID { get; set; }
    }

    public class NoSaleActivityListItem : ActivityListItem
    {
        public string ActivityState { get; set; }

        public string CustomerID { get; set; }

        public string ActivityStateKey { get; set; }

        public string ActivityTime { get; set; }

        public string reason { get; set; }
    }

    public class CreditProcessListItem : ActivityListItem
    {
        //public string Type { get; set; }
        public string ActivityState { get; set; }

        public string RouteID { get; set; }

        //public string Amount { get; set; }
        //public DateTime Date { get; set; }
        public string Reason { get; set; }

        public string ActivityTime { get; set; }

        public string ReceiptID { get; set; }

        public string Note { get; set; }

        public string Status { get; set; }
    }

    public class ReturnOrderListItem : ActivityListItem
    {
        public ViewModelPayload.ReturnOrderPayload Payload { get; set; }

        public string ActivityState { get; set; }

        public string ActivityTime { get; set; }

        public string Amount { get; set; }

        public string InvoiceNo { get; set; }

        public string ActivityStateKey { get; set; }

        public string CustomerID { get; set; }
    }

    public class ProspectNoteListItem : ActivityListItem
    {
        public ViewModelPayload.ProspectPayload Payload { get; set; }

        public string ActivityState { get; set; }

        public string ActivityTime { get; set; }

        public string Note { get; set; }

        public string ProsectNoteEnteredBy { get; set; }

        public string ActivityStateKey { get; set; }

        public string ProspectId { get; set; }
    }

    public class CustomerActivity : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CustomerActivity");
        string _Last30dysTotal, _Last90dysTotal;
        bool _ShowTodaysActivities;

        public bool ShowTodaysActivities
        {
            get
            {
                return _ShowTodaysActivities;
            }
            set
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerActivity][Start:ShowTodaysActivities]");
                try
                {
                    _ShowTodaysActivities = value;
                    foreach (object activity in Activities)
                    {
                        if (_ShowTodaysActivities)
                        {
                            if (activity.GetType() == typeof(OrderActivityListItem))
                            {
                                if (DateTime.Parse(((OrderActivityListItem)activity).ActivityTime, CultureInfo.CreateSpecificCulture("en-US")).Date == DateTime.Today.Date)
                                {
                                    ((OrderActivityListItem)activity).Visible = _ShowTodaysActivities;
                                }
                                else
                                {
                                    ((OrderActivityListItem)activity).Visible = !_ShowTodaysActivities;
                                }
                            }
                            else if (activity.GetType() == typeof(PaymentActivityListItem))
                            {
                                if (DateTime.Parse(((PaymentActivityListItem)activity).ActivityTime, CultureInfo.CreateSpecificCulture("en-US")).Date == DateTime.Today.Date)
                                {
                                    ((PaymentActivityListItem)activity).Visible = _ShowTodaysActivities;
                                }
                                else
                                {
                                    ((PaymentActivityListItem)activity).Visible = !_ShowTodaysActivities;
                                }
                            }
                            else if (activity.GetType() == typeof(NoSaleActivityListItem))
                            {
                                if (DateTime.Parse(((NoSaleActivityListItem)activity).ActivityTime, CultureInfo.CreateSpecificCulture("en-US")).Date == DateTime.Today.Date)
                                {
                                    ((NoSaleActivityListItem)activity).Visible = _ShowTodaysActivities;
                                }
                                else
                                {
                                    ((NoSaleActivityListItem)activity).Visible = !_ShowTodaysActivities;
                                }
                            }
                            else if (activity.GetType() == typeof(PreorderActivityListItem))
                            {
                                if (DateTime.Parse(((PreorderActivityListItem)activity).ActivityTime, CultureInfo.CreateSpecificCulture("en-US")).Date == DateTime.Today.Date)
                                {
                                    ((PreorderActivityListItem)activity).Visible = _ShowTodaysActivities;
                                }
                                else
                                {
                                    ((PreorderActivityListItem)activity).Visible = !_ShowTodaysActivities;
                                }
                            }
                            // Credit Memo Activity - Not filter for Todays Activity - Issue Fixed (11/24/2016)
                            else if (activity.GetType() == typeof(CreditProcessListItem))
                            {
                                if (DateTime.Parse(((CreditProcessListItem)activity).ActivityTime, CultureInfo.CreateSpecificCulture("en-US")).Date == DateTime.Today.Date)
                                {
                                    ((CreditProcessListItem)activity).Visible = _ShowTodaysActivities;
                                }
                                else
                                {
                                    ((CreditProcessListItem)activity).Visible = !_ShowTodaysActivities;
                                }
                            }
                            // Return Order Activity - Not filter for Todays Activity - Issue Fixed (11/26/2016)
                            else if (activity.GetType() == typeof(ReturnOrderListItem))
                            {
                                if (DateTime.Parse(((ReturnOrderListItem)activity).ActivityTime, CultureInfo.CreateSpecificCulture("en-US")).Date == DateTime.Today.Date)
                                {
                                    ((ReturnOrderListItem)activity).Visible = _ShowTodaysActivities;
                                }
                                else
                                {
                                    ((ReturnOrderListItem)activity).Visible = !_ShowTodaysActivities;
                                }
                            }
                            // CHG Activity - Not Filter for Todays Activity --Isssue Fixed (01/02/2017)   -- Vignesh D for TUI Isssue ID - 253
                            else if (activity.GetType() == typeof(ChangeActivityListItem))
                            {
                                if (DateTime.Parse(((ChangeActivityListItem)activity).ActivityTime, CultureInfo.CreateSpecificCulture("en-US")).Date == DateTime.Today.Date)
                                {
                                    ((ChangeActivityListItem)activity).Visible = _ShowTodaysActivities;
                                }
                                else
                                {
                                    ((ChangeActivityListItem)activity).Visible = !_ShowTodaysActivities;
                                }
                            }
                            else if (activity.GetType() == typeof(QuoteActivityListItem))
                            {
                                var quoteObj = ((QuoteActivityListItem)activity);
                                if (quoteObj.Date.Date == DateTime.Today.Date)
                                {
                                    quoteObj.Visible = _ShowTodaysActivities;
                                }
                                else
                                {
                                    quoteObj.Visible = !_ShowTodaysActivities;
                                }
                            }
                            else if (activity.GetType() == typeof(ProspectNoteListItem))
                            {
                                var quoteObj = ((ProspectNoteListItem)activity);
                                if (quoteObj.Date.Date == DateTime.Today.Date)
                                {
                                    quoteObj.Visible = _ShowTodaysActivities;
                                }
                                else
                                {
                                    quoteObj.Visible = !_ShowTodaysActivities;
                                }
                            }
                        }
                        else
                        {
                            if (activity.GetType() == typeof(OrderActivityListItem))
                            {
                                ((OrderActivityListItem)activity).Visible = true;
                            }
                            else if (activity.GetType() == typeof(PaymentActivityListItem))
                            {
                                ((PaymentActivityListItem)activity).Visible = true;
                            }
                            else if (activity.GetType() == typeof(NoSaleActivityListItem))
                            {
                                ((NoSaleActivityListItem)activity).Visible = true;
                            }
                            else if (activity.GetType() == typeof(PreorderActivityListItem))
                            {
                                ((PreorderActivityListItem)activity).Visible = true;
                            }
                            else if (activity.GetType() == typeof(QuoteActivityListItem))
                            {
                                ((QuoteActivityListItem)activity).Visible = true;
                            }
                            else if (activity.GetType() == typeof(ProspectNoteListItem))
                            {
                                ((ProspectNoteListItem)activity).Visible = true;
                            }
                            // Credit Memo Activity - Not filter for Todays Activity - Issue Fixed (11/24/2016)
                            else if (activity.GetType() == typeof(CreditProcessListItem))
                            {
                                ((CreditProcessListItem)activity).Visible = true;
                            }
                            // Return Order Activity - Not filter for Todays Activity - Issue Fixed (11/26/2016)
                            else if (activity.GetType() == typeof(ReturnOrderListItem))
                            {
                                ((ReturnOrderListItem)activity).Visible = true;
                            }
                            // CHG Activity - Not Filter for Todays Activity --Isssue Fixed (01/02/2017)   -- Vignesh D for TUI Isssue ID - 253
                            else if (activity.GetType() == typeof(ChangeActivityListItem))
                            {
                                ((ChangeActivityListItem)activity).Visible = true;
                            }
                        }
                    }
                    OnPropertyChanged("ShowTodaysActivities");
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerActivity][ShowTodaysActivities][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerActivity][End:ShowTodaysActivities]");
            }
        }

        public string Last30dysTotal
        {
            get
            {
                return GetInvoiceAmountTotal(30);
            }
            set
            {
                _Last30dysTotal = value;
                OnPropertyChanged("Last30dysTotal");
            }
        }

        public string Last90dysTotal
        {
            get
            {
                return GetInvoiceAmountTotal(90);                
            }
            set
            {
                _Last90dysTotal = value;
                OnPropertyChanged("Last90dysTotal");
            }
        }

        public string GetInvoiceAmountTotal(int days)
        {
            decimal result = Convert.ToDecimal(new CustomerActivityManager().GetInvoiceAmountTotal(days.ToString(), CommonNavInfo.RouteID.ToString(), CustomerNo));
            return "$" + result.ToString("F");
        }

        public DelegateCommand ShowActivity { get; set; }

        public Guid MessageToken { get; set; }

        public string CustomerNo { get; set; }

        public ObservableCollection<object> Activities { get; set; }

        public CustomerActivity(string customerNo)
        {
            try
            {
                CustomerNo = customerNo;
                Activities = new ObservableCollection<object>();
                LoadActivities(customerNo);
                InitializeCommands();
                IsBusy = false;
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerActivity CustomerActivity(" + customerNo + ") error: " + ex.Message);
            }
        }

        List<string> GetDisplayListItems()
        {
            List<string> items = new List<string>();
            items.Add(ActivityKey.HoldAtOrderEntry.ToString());
            items.Add(ActivityKey.HoldAtCashCollection.ToString());
            items.Add(ActivityKey.HoldAtDeliverToCustomer.ToString());
            items.Add(ActivityKey.HoldAtPick.ToString());
            items.Add(ActivityKey.VoidAtOrderEntry.ToString());
            items.Add(ActivityKey.VoidAtDeliverCustomer.ToString());
            items.Add(ActivityKey.VoidAtCashCollection.ToString());
            items.Add(ActivityKey.OrderDelivered.ToString());
            items.Add(ActivityKey.Payment.ToString());
            items.Add(ActivityKey.PreOrder.ToString());
            items.Add(ActivityKey.NoActivity.ToString());
            items.Add(ActivityKey.PickComplete.ToString());
            items.Add(ActivityKey.PaymentVoid.ToString());
            items.Add(ActivityKey.Change.ToString());
            items.Add(ActivityKey.CreateCreditMemo.ToString());
            items.Add(ActivityKey.VoidCreditMemo.ToString());
            items.Add(ActivityKey.SettledCreditMemo.ToString());
            items.Add(ActivityKey.VOIDEDSETTLED.ToString());
            items.Add(ActivityKey.HoldAtROEntry.ToString());
            items.Add(ActivityKey.HoldAtROPrint.ToString());
            items.Add(ActivityKey.HoldAtROPick.ToString());
            items.Add(ActivityKey.OrderReturned.ToString());
            items.Add(ActivityKey.VoidAtROEntry.ToString());
            items.Add(ActivityKey.VoidAtROPick.ToString());
            items.Add(ActivityKey.Quote.ToString());
            items.Add(ActivityKey.ProspectNote.ToString());
            items.Add(ActivityKey.PreOrderChange.ToString());
            return items;
        }

        async void LoadActivities(string customerNo)
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerActivity][LoadActivities][START]");
                DataSet ds = new DataSet();
                try
                {
                    string activityState = string.Empty;
                    CustomerActivityManager manager = new CustomerActivityManager();
                    ObservableCollection<Activity> activities = manager.GetActivities(customerNo);
                    ObservableCollection<Activity> temp = new ObservableCollection<Activity>();
                    //ObservableCollection<Activity> temp2 = new ObservableCollection<Activity>();

                    List<string> itemsToDisplay = GetDisplayListItems();
                    Type detailClassType = null;
                    DataContractJsonSerializer serializer = null;
                    MemoryStream stream = null;

                    temp = activities.Where(x => x.ActivityType.ToLower() == "preorderchange").ToObservableCollection();
                    foreach (Activity item in temp)
                    {
                        activities.Remove(item);
                    }
                    var trawActivity = (temp.GroupBy(y => new { y.ActivityEnd.Date, y.StopInstanceID }));
                    //sathish assigning to null once the opeation is done
                    //so that memory will be consumed
                    temp = null;

                    foreach (var item in trawActivity)
                    {
                        List<Activity> activityLst = item.ToList();
                        if (activityLst.Count > 0)
                            activities.Add(item.ToList()[0]);
                    }
                    //sathish assigning to null once the opeation is done
                    //so that memory will be consumed
                    trawActivity = null;
                    activities = activities.OrderByDescending(x => x.ActivityEnd.Date).ToList().ToObservableCollection();
                    foreach (Activity activity in activities)
                    {
                        try
                        {
                            // if (!itemsToDisplay.Contains(activity.ActivityStatus.Trim())) continue;
                            switch (activity.ActivityType.Trim())
                            {
                                case "Order":

                                    #region Order

                                    if (activity.ActivityDetails.Trim().Length == 0)
                                        break;
                                    detailClassType = Type.GetType(activity.ActivityDetailClass.Trim());
                                    if (detailClassType.Name == typeof(OrderPayload).Name)
                                    {
                                        ViewModelPayload.OrderPayload obj = (ViewModelPayload.OrderPayload)Activator.CreateInstance(detailClassType);
                                        serializer = new DataContractJsonSerializer(detailClassType);
                                        stream = new MemoryStream(Encoding.UTF8.GetBytes(activity.ActivityDetails));
                                        obj = (ViewModelPayload.OrderPayload)serializer.ReadObject(stream);
                                        OrderActivityListItem data = new OrderActivityListItem();
                                        //data.Date = activity.ActivityEnd.ToString("MM/dd/yyyy hh:mm tt");
                                        data.Date = activity.ActivityEnd;
                                        data.ItemType = "ORD";
                                        data.ActivityTime = activity.ActivityEnd.ToString("MM/dd/yyyy hh:mm tt");
                                        data.InvoiceNo = (string.IsNullOrEmpty(obj.InvoiceNo) ? "" : obj.InvoiceNo + "/SO");
                                        data.Amount = "$" + (string.IsNullOrEmpty(obj.Amount) ? "0.00" : Convert.ToDecimal(obj.Amount).ToString("F2"));
                                        data.Payload = obj;
                                        data.ActivityStateKey = string.IsNullOrEmpty(activity.ActivityStatus) ? "" : activity.ActivityStatus.Trim();
                                        data.Payload.TransactionID = activity.ActivityID;
                                        data.RouteSettlementId = (string.IsNullOrEmpty(activity.SettlementID) ? "" : activity.SettlementID);
                                        if (activity.ActivityStatus == ActivityKey.PickOrder.ToString() ||
                                            activity.ActivityStatus == ActivityKey.PickComplete.ToString() ||
                                            activity.ActivityStatus == ActivityKey.PickItem.ToString() ||
                                            activity.ActivityStatus == ActivityKey.HoldAtCashCollection.ToString() ||
                                            activity.ActivityStatus == ActivityKey.OrderDelivered.ToString()
                                        )
                                        {
                                            data.Payload.PickOrderTransactionID = activity.ActivityID;
                                        }
                                        activityState = string.Empty;
                                        //Logger.Error("activity.ActivityStatus: " + activity.ActivityStatus);

                                        // TUI Issue ID 265 - Void Order Status  After Settlement Is not Displaying in Activity Tab --Vignesh D
                                        if (activity.ActivityStatus == "VOIDED / SETTLED")
                                        {
                                            activity.ActivityStatus = "VOIDEDSETTLED";
                                        }
                                        switch (string.IsNullOrEmpty(activity.ActivityStatus) ? "" : activity.ActivityStatus.Trim().ParseEnum<ActivityKey>(ActivityKey.None).ToString())
                                        {
                                            case "HoldAtDeliverToCustomer":
                                                activityState = "DELIVERY SCREEN";
                                                //data.ActivityTime = activity.ActivityStart.ToString("MM/dd/yyyy hh:mm tt");
                                                break;
                                            case "HoldAtCashCollection":
                                                activityState = "CASH DELIVERY";
                                                //data.ActivityTime = activity.ActivityStart.ToString("MM/dd/yyyy hh:mm tt");
                                                break;
                                            case "HoldAtOrderEntry":
                                                activityState = "CREATE ORDER";
                                                //data.ActivityTime = activity.ActivityStart.ToString("MM/dd/yyyy hh:mm tt");
                                                break;
                                            case "HoldAtPick":
                                                activityState = "PICK ORDER";
                                                //data.ActivityTime = activity.ActivityStart.ToString("MM/dd/yyyy hh:mm tt");
                                                break;
                                            case "VoidAtDeliverCustomer":
                                                activityState = "VOID";
                                                break;
                                            case "PreOrder":
                                                activityState = "PREORDER";
                                                break;
                                            case "VoidAtCashCollection":
                                                activityState = "VOID";
                                                break;
                                            case "VoidAtOrderEntry":
                                                activityState = "VOID";
                                                break;
                                            case "PickComplete":
                                                activityState = "PICK ORDER";
                                                break;
                                            case "OrderDelivered":
                                                activityState = "DELIVERED";
                                                break;
                                            case "PickItem":
                                                activityState = "PICK ORDER";
                                                break;
                                            case "PickOrder":
                                                activityState = "PICK ORDER";
                                                break;
                                            case "SettlementVerified":
                                                activityState = "SETTLED";
                                                break;
                                            case "VOIDEDSETTLED":
                                                activityState = "VOIDED / SETTLED";
                                                break;
                                            default:
                                                break;
                                        }
                                        data.ActivityState = activityState;
                                        //Logger.Error("data.ActivityState: " + data.ActivityState);
                                        Activities.Add(data);
                                    }
                                    #endregion
                                    
                                    break;
                                case "Payment":

                                    #region Payments

                                    if (activity.ActivityDetails.Trim().Length == 0)
                                        break;
                                    detailClassType = Type.GetType(activity.ActivityDetailClass.Trim());
                                    ViewModelPayload.PaymentPayload payloadTypeObj = (ViewModelPayload.PaymentPayload)Activator.CreateInstance(detailClassType);
                                    serializer = new DataContractJsonSerializer(detailClassType);
                                    stream = new MemoryStream(Encoding.UTF8.GetBytes(activity.ActivityDetails));
                                    payloadTypeObj = (ViewModelPayload.PaymentPayload)serializer.ReadObject(stream);
                                    PaymentActivityListItem paymentActivityListItem = new PaymentActivityListItem();
                                    paymentActivityListItem.ActivityTime = activity.ActivityEnd.ToString("MM/dd/yyyy hh:mm tt");

                                    //Append "/SO" in the invoice no 
                                    for (int i = 0; i < payloadTypeObj.InvoiceNo.Count; i++)
                                    {
                                        payloadTypeObj.InvoiceNo[i] = payloadTypeObj.InvoiceNo[i] + "/SO";
                                    }

                                    paymentActivityListItem.InvoiceNo = payloadTypeObj.InvoiceNo;
                                    paymentActivityListItem.Type = payloadTypeObj.CashDelivery.PaymentMode ? "Check" : "Cash";
                                    paymentActivityListItem.ChequeNo = payloadTypeObj.CashDelivery.PaymentMode ? payloadTypeObj.CashDelivery.ChequeNo : "";
                                    paymentActivityListItem.Amount = "$" + (string.IsNullOrEmpty(payloadTypeObj.CashDelivery.PaymentAmount.ToString()) ? "0.00" : Convert.ToDecimal(payloadTypeObj.CashDelivery.PaymentAmount).ToString("F2"));
                                    paymentActivityListItem.Payload = payloadTypeObj;
                                    paymentActivityListItem.ActivityStateKey = activity.ActivityStatus;
                                    string unappliedAmount = (new ARPaymentManager("", "")).GetReceiptUnAppliedAmt(payloadTypeObj.TransactionID.Replace("receipt#", ""));
                                    paymentActivityListItem.UnappliedAmount = Convert.ToDecimal(string.IsNullOrEmpty(unappliedAmount) ? "0" : unappliedAmount);
                                    paymentActivityListItem.Payload.TransactionID = activity.ActivityID;
                                    paymentActivityListItem.RouteSettlementId = (string.IsNullOrEmpty(activity.SettlementID) ? "" : activity.SettlementID);

                                    activityState = string.Empty;
                                    switch (activity.ActivityStatus.Trim().ParseEnum<ActivityKey>(ActivityKey.None).ToString())
                                    {
                                        case "PaymentVoid":
                                            activityState = "VOID";
                                            //data.ActivityTime = activity.ActivityStart.ToString("MM/dd/yyyy hh:mm tt");
                                            break;
                                    }
                                    paymentActivityListItem.ActivityState = activityState;
                                    paymentActivityListItem.Date = activity.ActivityEnd;
                                    paymentActivityListItem.ItemType = "PAY";
                                    Activities.Add(paymentActivityListItem);

                                    #endregion

                                    break;
                                case "NoActivity":

                                    #region NoActivity

                                    if (activity.ActivityDetails.Trim().Length == 0)
                                        break;

                                    NoSaleActivityListItem noActivityListItem = new NoSaleActivityListItem();
                                    noActivityListItem.ActivityTime = activity.ActivityEnd.ToString("MM/dd/yyyy hh:mm tt");
                                    noActivityListItem.Type = "Reason: " + activity.ActivityDetails.Trim();
                                    noActivityListItem.Amount = "";
                                    noActivityListItem.ActivityStateKey = activity.ActivityStatus;
                                    noActivityListItem.Date = activity.ActivityEnd;
                                    noActivityListItem.ItemType = "NO-S";
                                    noActivityListItem.RouteSettlementId = (string.IsNullOrEmpty(activity.SettlementID) ? "" : activity.SettlementID);

                                    Activities.Add(noActivityListItem);

                                    #endregion

                                    break;
                                case "PreOrder":

                                    #region PreOrder

                                    detailClassType = Type.GetType(activity.ActivityDetailClass.Trim());
                                    ViewModelPayload.PreOrderPayload preorderpayload = (ViewModelPayload.PreOrderPayload)Activator.CreateInstance(detailClassType);
                                    serializer = new DataContractJsonSerializer(detailClassType);
                                    stream = new MemoryStream(Encoding.UTF8.GetBytes(activity.ActivityDetails));
                                    preorderpayload = (ViewModelPayload.PreOrderPayload)serializer.ReadObject(stream);
                                    PreorderActivityListItem preorderactivitylistitem = new PreorderActivityListItem();
                                    preorderactivitylistitem.Payload = preorderpayload;
                                    preorderactivitylistitem.Payload.TransactionID = activity.ActivityID;
                                    preorderactivitylistitem.Date = activity.ActivityEnd;//preorderpayload.StopDate;PreOrderDate
                                    preorderactivitylistitem.PreOrderDate = preorderpayload.StopDate;
                                    preorderactivitylistitem.ItemOrdered = preorderpayload.PreOrderItems.Count(item => item.PreOrderQty > 0).ToString();
                                    preorderactivitylistitem.CustomerID = preorderpayload.Customer.CustomerNo;
                                    preorderactivitylistitem.ItemType = "PRE";
                                    preorderactivitylistitem.ActivityStateKey = activity.ActivityStatus.Trim();
                                    preorderactivitylistitem.ActivityState = activity.ActivityStatus.Trim().ParseEnum<ActivityKey>(ActivityKey.None).ToString().ToString().ToUpper();
                                    preorderactivitylistitem.ActivityTime = activity.ActivityEnd.ToString("MM/dd/yyyy hh:mm tt");
                                    Activities.Add(preorderactivitylistitem);

                                    #endregion

                                    break;
                                case "PreOrderChange":

                                    #region PreOrderChange

                                    // TODO: check for latest update on same date. 

                                    detailClassType = Type.GetType(activity.ActivityDetailClass.Trim());
                                    ViewModelPayload.PreOrderPayload preorderpayloadChange = (ViewModelPayload.PreOrderPayload)Activator.CreateInstance(detailClassType);
                                    serializer = new DataContractJsonSerializer(detailClassType);
                                    stream = new MemoryStream(Encoding.UTF8.GetBytes(activity.ActivityDetails));
                                    preorderpayloadChange = (ViewModelPayload.PreOrderPayload)serializer.ReadObject(stream);
                                    PreorderActivityListItem preorderactivitylistitemChange = new PreorderActivityListItem();
                                    preorderactivitylistitemChange.Payload = preorderpayloadChange;
                                    preorderactivitylistitemChange.Payload.TransactionID = activity.ActivityID;
                                    preorderactivitylistitemChange.Date = activity.ActivityEnd;//preorderpayload.StopDate;PreOrderDate
                                    preorderactivitylistitemChange.PreOrderDate = preorderpayloadChange.StopDate;
                                    preorderactivitylistitemChange.ItemOrdered = preorderpayloadChange.PreOrderItems.Count(item => item.PreOrderQty > 0).ToString();
                                    preorderactivitylistitemChange.CustomerID = preorderpayloadChange.Customer.CustomerNo;
                                    preorderactivitylistitemChange.ItemType = "CHG";
                                    preorderactivitylistitemChange.ActivityStateKey = activity.ActivityStatus.Trim();
                                    preorderactivitylistitemChange.ActivityState = activity.ActivityStatus.Trim().ParseEnum<ActivityKey>(ActivityKey.None).ToString().ToString().ToUpper();
                                    preorderactivitylistitemChange.ActivityTime = activity.ActivityEnd.ToString("MM/dd/yyyy hh:mm tt");
                                    Activities.Add(preorderactivitylistitemChange);

                                    #endregion

                                    break;
                                case "Change":

                                    #region Change

                                    detailClassType = Type.GetType(activity.ActivityDetailClass.Trim());
                                    ViewModelPayload.ChangePayload objChangePayload = (ViewModelPayload.ChangePayload)Activator.CreateInstance(detailClassType);
                                    serializer = new DataContractJsonSerializer(detailClassType);
                                    stream = new MemoryStream(Encoding.UTF8.GetBytes(activity.ActivityDetails));
                                    objChangePayload = (ViewModelPayload.ChangePayload)serializer.ReadObject(stream);
                                    ChangeActivityListItem objChangeActivityListItem = new ChangeActivityListItem();
                                    objChangeActivityListItem.Payload = objChangePayload;
                                    objChangeActivityListItem.ActivityTime = activity.ActivityEnd.ToString("MM/dd/yyyy hh:mm tt");
                                    objChangeActivityListItem.Amount = "--";
                                    objChangeActivityListItem.ActivityStateKey = activity.ActivityStatus.Trim();
                                    objChangeActivityListItem.Date = activity.ActivityEnd;
                                    objChangeActivityListItem.ItemType = "CHG";
                                    Activities.Add(objChangeActivityListItem);

                                    #endregion

                                    break;
                                case "CreditMemo":

                                    #region CreditMemo

                                    if (activity.ActivityDetails.Trim().Length == 0)
                                        break;
                                    //detailClassType = Type.GetType(activity.ActivityDetailClass.Trim());
                                    //CreditProcess creditProcess = (CreditProcess)Activator.CreateInstance(detailClassType);
                                    serializer = new DataContractJsonSerializer(typeof(CreditProcess));
                                    stream = new MemoryStream(Encoding.UTF8.GetBytes(activity.ActivityDetails));
                                    CreditProcess creditProcess = (CreditProcess)serializer.ReadObject(stream);
                                    CreditProcessListItem creditProcessListItem = new CreditProcessListItem();
                                    creditProcessListItem.ActivityTime = activity.ActivityEnd.ToString("MM/dd/yyyy hh:mm tt");
                                    creditProcessListItem.ItemType = "CRD";
                                    creditProcessListItem.RouteID = creditProcess.RouteID;
                                    creditProcessListItem.Amount = "($" + Convert.ToDecimal(creditProcess.CreditAmount).ToString("F2") + ")";
                                    creditProcessListItem.Reason = creditProcess.CreditReason.Trim();
                                    creditProcessListItem.Note = creditProcess.CreditNote.Trim();
                                    creditProcessListItem.Date = creditProcess.CreditMemoDate;
                                    creditProcessListItem.ReceiptID = creditProcess.ReceiptID.ToString().Trim() + "/C5";
                                    creditProcessListItem.ActivityState = string.Empty;

                                    switch (activity.ActivityStatus.Trim().ParseEnum<ActivityKey>(ActivityKey.None).ToString())
                                    {
                                        case "CreateCreditMemo":
                                            creditProcessListItem.ActivityState = "UNSETTLED";
                                            break;
                                        case "VoidCreditMemo":
                                            creditProcessListItem.ActivityState = "VOID";
                                            break;
                                        case "SettledCreditMemo":
                                            creditProcessListItem.ActivityState = "SETTLED";
                                            break;
                                        case "VDDSettledCreditMemo":
                                            creditProcessListItem.ActivityState = "VOIDED/SETTLED";
                                            break;
                                    }
                                    Activities.Add(creditProcessListItem);

                                    #endregion

                                    break;
                                case "ReturnOrder":

                                    #region Return Order

                                    if (activity.ActivityDetails.Trim().Length == 0)
                                        break;
                                    detailClassType = Type.GetType(activity.ActivityDetailClass.Trim());
                                    ViewModelPayload.ReturnOrderPayload returnOrderObj = (ViewModelPayload.ReturnOrderPayload)Activator.CreateInstance(detailClassType);
                                    serializer = new DataContractJsonSerializer(detailClassType);
                                    stream = new MemoryStream(Encoding.UTF8.GetBytes(activity.ActivityDetails));
                                    returnOrderObj = (ViewModelPayload.ReturnOrderPayload)serializer.ReadObject(stream);
                                    ReturnOrderListItem returnOrderObjData = new ReturnOrderListItem();
                                    returnOrderObjData.Date = activity.ActivityEnd;
                                    returnOrderObjData.ItemType = "RTN";
                                    returnOrderObjData.ActivityTime = activity.ActivityEnd.ToString("MM/dd/yyyy hh:mm tt");
                                    returnOrderObjData.InvoiceNo = (string.IsNullOrEmpty(returnOrderObj.ReturnOrderID.ToString()) ? "" : returnOrderObj.ReturnOrderID.ToString());
                                    returnOrderObjData.Amount = "$" + (string.IsNullOrEmpty(returnOrderObj.InvoiceTotalAmt.ToString()) ? "0.00" : Convert.ToDecimal(returnOrderObj.InvoiceTotalAmt).ToString("F2"));
                                    returnOrderObjData.Payload = returnOrderObj;
                                    returnOrderObjData.ActivityStateKey = activity.ActivityStatus.Trim();
                                    returnOrderObjData.Payload.TransactionID = activity.ActivityID;

                                    if (activity.ActivityStatus.Trim() == ActivityKey.HoldAtROPrint.ToString() ||
                                        activity.ActivityStatus.Trim() == ActivityKey.HoldAtPick.ToString() ||
                                        activity.ActivityStatus.Trim() == ActivityKey.HoldAtROEntry.ToString() ||
                                        activity.ActivityStatus.Trim() == ActivityKey.HoldAtROPick.ToString() ||
                                        activity.ActivityStatus.Trim() == ActivityKey.VoidAtROPick.ToString() ||
                                        activity.ActivityStatus.Trim() == ActivityKey.VoidAtROEntry.ToString() ||
                                        activity.ActivityStatus.Trim() == ActivityKey.OrderReturned.ToString()
                                    )
                                    {
                                        returnOrderObjData.Payload.TransactionID = activity.ActivityID;
                                    }
                                    activityState = string.Empty;
                                    switch (activity.ActivityStatus.Trim().ParseEnum<ActivityKey>(ActivityKey.None).ToString())
                                    {
                                        //Hold state values are consumed by "ActivityStatusToBackgroundColorConverter" to show the backgrond of the label on the activity list screen
                                        case "HoldAtROPrint":
                                            activityState = "RTN ORD PRINT";
                                            break;
                                        case "HoldAtROEntry":
                                            activityState = "RTN ORD ENTRY";
                                            break;
                                        case "HoldAtROPick":
                                            activityState = "RTN ORD PICK";
                                            break;
                                        case "VoidAtROEntry":
                                            activityState = "VOID";
                                            break;
                                        case "VoidAtROPick":
                                            activityState = "VOID";
                                            break;
                                        case "OrderReturned":
                                            activityState = "DELIVERED";
                                            break;
                                    }
                                    returnOrderObjData.ActivityState = activityState;
                                    Activities.Add(returnOrderObjData);

                                    #endregion

                                    break;
                                case "Quote":

                                    #region Quote

                                    if (activity.ActivityDetails.Trim().Length == 0)
                                        break;
                                    detailClassType = Type.GetType(activity.ActivityDetailClass.Trim());
                                    serializer = new DataContractJsonSerializer(detailClassType);
                                    stream = new MemoryStream(Encoding.UTF8.GetBytes(activity.ActivityDetails));
                                    QuoteActivityListItem quoteActivity = (QuoteActivityListItem)serializer.ReadObject(stream);

                                    quoteActivity.ItemType = "QTE";
                                    if (!string.IsNullOrEmpty(activity.SettlementID))
                                        quoteActivity.SettlementId = DbEngine.ExecuteScalar("SELECT " + activity.SettlementID + " FROM BUSDTA.Route_Settlement WHERE ISNULL(Verifier,'')<>'' AND SettlementId=" + activity.SettlementID);
                                    // TUI Issue ID 256 fixed on (01/03/2017) to display Quote status after settlement in Customer Home Activity tab --Vignesh D
                                    quoteActivity.ActivityState = string.Empty;
                                    switch (activity.ActivityStatus.Trim().ParseEnum<ActivityKey>(ActivityKey.None).ToString())
                                    {
                                        case "SettlementVerified":
                                            quoteActivity.ActivityState = "SETTLED";
                                            break;
                                    }
                                    Activities.Add(quoteActivity);

                                    #endregion

                                    break;
                                case "ProspectNote":

                                    #region ProspectNote

                                    if (activity.ActivityDetails.Trim().Length == 0)
                                        break;
                                    detailClassType = Type.GetType(activity.ActivityDetailClass.Trim());
                                    serializer = new DataContractJsonSerializer(detailClassType);
                                    stream = new MemoryStream(Encoding.UTF8.GetBytes(activity.ActivityDetails));
                                    ProspectNoteListItem listItem = (ProspectNoteListItem)serializer.ReadObject(stream);

                                    listItem.ItemType = "NTS";
                                    listItem.Note = listItem.Note.Replace("\n", "").Replace("\r", " ");
                                    Activities.Add(listItem);

                                    #endregion

                                    break;
                                default:
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerActivity][ExceptionStackTrace = " + ex.Message == null ? ex.InnerException.Message : ex.Message + "]");
                            continue;
                        }
                    }
                    //sathish assigning to null once the opeation is done
                    //so that memory will be consumed
                    activities = null;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][CustomerActivity][ExceptionStackTrace = " + ex.Message == null ? ex.InnerException.Message : ex.Message + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerActivity][LoadActivities][END]");
            });
        }

        void InitializeCommands()
        {
            ShowActivity = new DelegateCommand((param) =>
            {
                try
                {
                    CustomerDashboard custDash = new CustomerDashboard();
                    bool IsPreTripDoneForDate = false;
                    //   object p = param;    //Unused code --Vignesh D
                    if (param == null)
                        return;
                    IsPreTripDoneForDate = custDash.GetPreTripDetailsForDate();
                    if (IsPreTripDoneForDate)
                    {
                        if (param.GetType() == typeof(ReturnOrderListItem))
                        {
                            #region Implementation Logic

                            ReturnOrderListItem data = (ReturnOrderListItem)param;

                            PayloadManager.ReturnOrderPayload = data.Payload;
                            // Check is only for today as activity can be performed only today, if visitee has physical stop today
                            if (DateTime.Parse(data.ActivityTime, CultureInfo.CreateSpecificCulture("en-US")) < DateTime.Today.Date)
                            {
                                PayloadManager.ReturnOrderPayload.StopDateBeforeInvokingActivity = PayloadManager.OrderPayload.Customer.StopDate;
                                PayloadManager.ReturnOrderPayload.Customer = new CustomerDashboard().GetNewCustomer(DateTime.Parse(data.ActivityTime, CultureInfo.CreateSpecificCulture("en-US")), data.Payload.Customer);
                                PayloadManager.ReturnOrderPayload.IsCustomerLoadedForPastActivity = true;

                                string tempStopDate = string.Empty;
                                string stopID = string.Empty;
                                if (PayloadManager.OrderPayload.StopDate.Date != DateTime.Now.Date)
                                {
                                    CustomerManager cus = new CustomerManager();
                                    cus.CloseActivity(data.Payload.StopID);

                                    tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                                    stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.OrderPayload.Customer.CustomerNo, tempStopDate);
                                }
                            }
                            else
                            {
                                PayloadManager.ReturnOrderPayload.Customer = PayloadManager.OrderPayload.Customer;
                            }
                            GetWarnPopupForPastActivity(DateTime.Parse(data.ActivityTime, CultureInfo.CreateSpecificCulture("en-US")), false, data.CustomerID);

                            PayloadManager.ReturnOrderPayload.IsFromActivityTab = true;
                            ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.CustomerHome_Activity.ToString();

                            if (data.ActivityStateKey == ActivityKey.HoldAtROEntry.ToString())
                            {
                                PayloadManager.ReturnOrderPayload.CurrentViewName = PayloadManager.ReturnOrderPayload.IsReturnOrder ? ViewModelMappings.View.OrderReturnsScreen.ToString() : ViewModelMappings.View.ItemReturnsScreen.ToString();
                                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ItemReturnsScreen, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, ShowAsModal = false };
                                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
                            }
                            else if (data.ActivityStateKey == ActivityKey.HoldAtROPick.ToString())
                            {
                                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnOrderPickScreen, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, Payload = null, ShowAsModal = false };
                                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                            }
                            else if (data.ActivityStateKey == ActivityKey.HoldAtROPrint.ToString())
                            {
                                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AcceptAndPrintReturns, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, ShowAsModal = false };
                                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
                            }
                            else
                            {
                                PayloadManager.ReturnOrderPayload.ROIsInProgress = false;
                                PayloadManager.ReturnOrderPayload.PickIsInProgress = false;
                                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, ShowAsModal = false };
                                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
                            }

                            #endregion
                        }
                        if (param.GetType() == typeof(OrderActivityListItem))
                        {
                            #region Implementation Logic

                            OrderActivityListItem data = (OrderActivityListItem)param;

                            Order.OrderId = Convert.ToInt32(data.Payload.OrderID);
                            // Check is only for today as activity can be performed only today, if visitee has physical stop today
                            if (DateTime.Parse(data.ActivityTime, CultureInfo.CreateSpecificCulture("en-US")) < DateTime.Today.Date)
                            {
                                PayloadManager.OrderPayload.StopDateBeforeInvokingActivity = PayloadManager.OrderPayload.StopDate;
                                //PayloadManager.OrderPayload.Customer = new CustomerDashboard().GetNewCustomer(DateTime.Parse(data.ActivityTime, CultureInfo.CreateSpecificCulture("en-US")), data.Payload.Customer);
                                PayloadManager.OrderPayload.Customer = data.Payload.Customer;
                                PayloadManager.OrderPayload.Customer.CustName = data.Payload.Customer.CustName; // PayloadManager.OrderPayload.Customer.Name;
                                PayloadManager.OrderPayload.IsCustomerLoadedForPastActivity = true;
                            }
                            //GetWarnPopupForPastActivity(DateTime.Parse(data.ActivityTime, CultureInfo.CreateSpecificCulture("en-US")), false, data.CustomerID);
                            PayloadManager.OrderPayload.OrderID = data.Payload.OrderID;
                            PayloadManager.OrderPayload.TransactionID = data.Payload.TransactionID;
                            PayloadManager.OrderPayload.Amount = data.Payload.Amount;
                            PayloadManager.OrderPayload.Surcharge = data.Payload.Surcharge;

                            if (data.ActivityStateKey.Trim() == ActivityKey.PickOrder.ToString() ||
                                data.ActivityStateKey.Trim() == ActivityKey.PickComplete.ToString() ||
                                data.ActivityStateKey.Trim() == ActivityKey.PickItem.ToString() ||
                                data.ActivityStateKey.Trim() == ActivityKey.HoldAtCashCollection.ToString() ||
                                data.ActivityStateKey.Trim() == ActivityKey.OrderDelivered.ToString()
                            )
                            {
                                PayloadManager.OrderPayload.PickOrderTransactionID = data.Payload.TransactionID;
                            }

                            PayloadManager.OrderPayload.TransactionLastState = data.ActivityStateKey.Trim();

                            // Redirect only on Picked and Hold
                            // SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = false;
                            var moveToView = new Helpers.NavigateToView { CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, ShowAsModal = false, Refresh = false };

                            if (data.ActivityStateKey.Trim() == ActivityKey.HoldAtOrderEntry.ToString())
                            {
                                //************************************************************************************************
                                // Comment: Added to Create a unplanned stop for today
                                // Created: feb 03, 2016
                                // Author: Vivensas (Rajesh,Yuvaraj)
                                // Revisions: 
                                //*************************************************************************************************
                                DateTime dateToCreateStop = Convert.ToDateTime(DateTime.Now);

                                if (dateToCreateStop.Date != data.Payload.StopDate.Date)
                                {
                                    Visitee curcustomer = new Visitee();
                                    curcustomer = data.Payload.Customer;
                                    curcustomer.StopType = "unplanned";
                                    curcustomer.ReferenceStopId = data.Payload.StopID;
                                    curcustomer.StopID = data.Payload.StopID;
                                    ServiceRouteManager mngr = new ServiceRouteManager();
                                    //if (mngr.MoveCustomerStop(curcustomer, dateToCreateStop, data.Payload.StopDate))
                                    //{
                                    //    StopActionCompletedEventArgs args = new StopActionCompletedEventArgs();
                                    //    args.DateSelected = dateToCreateStop;
                                    //    args.visiteeChanged = curcustomer;
                                    //}

                                    List<DateTime> datecheck = mngr.GetPresentStops(data.Payload.Customer.customerNo, dateToCreateStop, dateToCreateStop, false);
                                    bool valcheck = false;
                                    foreach (var ch in datecheck)
                                    {
                                        if (ch.Date == dateToCreateStop.Date)
                                        {
                                            valcheck = true;
                                        }
                                    }
                                    //sathish assigning to null once the opeation is done
                                    //so that memory will be consumed
                                    datecheck = null;
                                    if (!valcheck)
                                    {
                                        var dialog = new Helpers.DialogWindow { TemplateKey = "ConfirmVoidandCreateDialog", Title = "Stop Update Info", Payload = null };
                                        Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                                    }

                                    OrderManager ord = new OrderManager();
                                    //   int chkint = ord.UpdateOrderHeader(Convert.ToInt32(data.Payload.OrderID));   //Unused Local --Vignesh D
                                    ord.UpdateOrderHeader(Convert.ToInt32(data.Payload.OrderID));                  //Added by Vignesh D
                                }

                                //*************************************************************************************************
                                // Vivensas changes ends over here
                                //**************************************************************************************************
                                ResourceManager.Transaction.ResetActivity(data.Payload.TransactionID);
                                PayloadManager.OrderPayload.Items = data.Payload.Items;
                                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                                moveToView.NextViewName = ViewModelMappings.View.Order;
                                //if (PayloadManager.OrderPayload.Items != null && PayloadManager.OrderPayload.Items.Count > 0)
                                //{
                                //    new InventoryManager().UpdateCommittedQuantity(PayloadManager.OrderPayload.Items, Helpers.StatusTypesEnum.INPRG);
                                //}
                            }
                            if (data.ActivityStateKey.Trim() == ActivityKey.HoldAtCashCollection.ToString())
                            {
                                //************************************************************************************************
                                // Comment: Added to Create a unplanned stop for today
                                // Created: feb 03, 2016
                                // Author: Vivensas (Rajesh,Yuvaraj)
                                // Revisions: 
                                //*************************************************************************************************
                                DateTime dateToCreateStop = Convert.ToDateTime(DateTime.Now);

                                if (dateToCreateStop.Date != data.Payload.StopDate.Date)
                                {
                                    Visitee curcustomer = new Visitee();
                                    curcustomer = data.Payload.Customer;
                                    curcustomer.StopType = "unplanned";
                                    curcustomer.ReferenceStopId = data.Payload.StopID;
                                    curcustomer.StopID = data.Payload.StopID;
                                    ServiceRouteManager mngr = new ServiceRouteManager();
                                    //if (mngr.MoveCustomerStop(curcustomer, dateToCreateStop, data.Payload.StopDate))
                                    //{
                                    //    StopActionCompletedEventArgs args = new StopActionCompletedEventArgs();
                                    //    args.DateSelected = dateToCreateStop;
                                    //    args.visiteeChanged = curcustomer;
                                    //}

                                    //var dialog = new Helpers.DialogWindow { TemplateKey = "ConfirmVoidandCreateDialog", Title = "Stop Update Info", Payload = null };
                                    //Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                                    List<DateTime> datecheck = mngr.GetPresentStops(data.Payload.Customer.customerNo, dateToCreateStop, dateToCreateStop, false);

                                    bool valcheck = false;
                                    foreach (var ch in datecheck)
                                    {
                                        if (ch.Date == dateToCreateStop.Date)
                                        {
                                            valcheck = true;
                                        }
                                    }
                                    //sathish assigning to null once the opeation is done
                                    //so that memory will be consumed
                                    datecheck = null;
                                    if (!valcheck)
                                    {
                                        var dialog = new Helpers.DialogWindow { TemplateKey = "ConfirmVoidandCreateDialog", Title = "Stop Update Info", Payload = null };
                                        Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                                    }

                                    OrderManager ord = new OrderManager();
                                    //  int chkint = ord.UpdateOrderHeader(Convert.ToInt32(data.Payload.OrderID));     //Unused Local --Vignesh D
                                    ord.UpdateOrderHeader(Convert.ToInt32(data.Payload.OrderID));                    //Added by Vignesh D
                                }

                                //*************************************************************************************************
                                // Vivensas changes ends over here
                                //**************************************************************************************************
                                ResourceManager.Transaction.ResetActivity(data.Payload.TransactionID);
                                PayloadManager.OrderPayload.Items = data.Payload.Items;
                                PayloadManager.OrderPayload.CashDelivery = data.Payload.CashDelivery;
                                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                                moveToView.NextViewName = ViewModelMappings.View.PickOrder;
                            }
                            if (data.ActivityStateKey.Trim() == ActivityKey.HoldAtDeliverToCustomer.ToString() || data.ActivityStateKey.Trim() == ActivityKey.OrderDelivered.ToString())
                            {
                                //************************************************************************************************
                                // Comment: Added to Create a unplanned stop for today
                                // Created: feb 03, 2016
                                // Author: Vivensas (Rajesh,Yuvaraj)
                                // Revisions: 
                                //*************************************************************************************************
                                DateTime dateToCreateStop = Convert.ToDateTime(DateTime.Now);

                                if (dateToCreateStop.Date != data.Payload.StopDate.Date && data.ActivityStateKey.Trim() != ActivityKey.OrderDelivered.ToString())
                                {
                                    Visitee curcustomer = new Visitee();
                                    curcustomer = data.Payload.Customer;
                                    curcustomer.StopType = "unplanned";
                                    curcustomer.ReferenceStopId = data.Payload.StopID;
                                    curcustomer.StopID = data.Payload.StopID;
                                    ServiceRouteManager mngr = new ServiceRouteManager();
                                    //if (mngr.MoveCustomerStop(curcustomer, dateToCreateStop, data.Payload.StopDate))
                                    //{
                                    //    StopActionCompletedEventArgs args = new StopActionCompletedEventArgs();
                                    //    args.DateSelected = dateToCreateStop;
                                    //    args.visiteeChanged = curcustomer;
                                    //}

                                    //var dialog = new Helpers.DialogWindow { TemplateKey = "ConfirmVoidandCreateDialog", Title = "Stop Update Info", Payload = null };
                                    //Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                                    List<DateTime> datecheck = mngr.GetPresentStops(data.Payload.Customer.customerNo, dateToCreateStop, dateToCreateStop, false);

                                    bool valcheck = false;
                                    foreach (var ch in datecheck)
                                    {
                                        if (ch.Date == dateToCreateStop.Date)
                                        {
                                            valcheck = true;
                                        }
                                    }
                                    //sathish assigning to null once the opeation is done
                                    //so that memory will be consumed
                                    datecheck = null;
                                    if (!valcheck)
                                    {
                                        var dialog = new Helpers.DialogWindow { TemplateKey = "ConfirmVoidandCreateDialog", Title = "Stop Update Info", Payload = null };
                                        Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                                    }
                                    OrderManager ord = new OrderManager();
                                    //   int chkint = ord.UpdateOrderHeader(Convert.ToInt32(data.Payload.OrderID));    //Unused Local --Vignesh D
                                    ord.UpdateOrderHeader(Convert.ToInt32(data.Payload.OrderID));                      //Added by Vignesh D
                                }

                                //*************************************************************************************************
                                // Vivensas changes ends over here
                                //**************************************************************************************************
                                if (data.ActivityStateKey.Trim() == ActivityKey.HoldAtDeliverToCustomer.ToString())
                                {
                                    ResourceManager.Transaction.ResetActivity(data.Payload.TransactionID);
                                    //Get payment transaction ID
                                    PayloadManager.PaymentPayload.TransactionID = TransactionManager.GetPaymentTransactionID(data.Payload.TransactionID);
                                }
                                PayloadManager.OrderPayload.Items = data.Payload.Items;
                                moveToView.NextViewName = ViewModelMappings.View.DeliveryScreen;
                            }
                            if (data.ActivityStateKey.Trim() == ActivityKey.HoldAtPick.ToString() || data.ActivityStateKey.Trim() == ActivityKey.PickComplete.ToString())
                            {
                                //************************************************************************************************
                                // Comment: Added to Create a unplanned stop for today
                                // Created: feb 03, 2016
                                // Author: Vivensas (Rajesh,Yuvaraj)
                                // Revisions: 
                                //*************************************************************************************************
                                DateTime dateToCreateStop = Convert.ToDateTime(DateTime.Now);

                                if (dateToCreateStop.Date != data.Payload.StopDate.Date)
                                {
                                    Visitee curcustomer = new Visitee();
                                    curcustomer = data.Payload.Customer;
                                    curcustomer.StopType = "unplanned";
                                    curcustomer.ReferenceStopId = data.Payload.StopID;
                                    curcustomer.StopID = data.Payload.StopID;
                                    ServiceRouteManager mngr = new ServiceRouteManager();

                                    List<DateTime> datecheck = mngr.GetPresentStops(data.Payload.Customer.customerNo, dateToCreateStop, dateToCreateStop, false);

                                    bool valcheck = false;
                                    foreach (var ch in datecheck)
                                    {
                                        if (ch.Date == dateToCreateStop.Date)
                                        {
                                            valcheck = true;
                                        }
                                    }
                                    //sathish assigning to null once the opeation is done
                                    //so that memory will be consumed
                                    datecheck = null;
                                    if (!valcheck)
                                    {
                                        var dialog = new Helpers.DialogWindow { TemplateKey = "ConfirmVoidandCreateDialog", Title = "Stop Update Info", Payload = null };
                                        Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                                    }

                                    OrderManager ord = new OrderManager();
                                    //   int chkint = ord.UpdateOrderHeader(Convert.ToInt32(data.Payload.OrderID));    //Unused Local --Vignesh D
                                    ord.UpdateOrderHeader(Convert.ToInt32(data.Payload.OrderID));
                                }

                                //*************************************************************************************************
                                // Vivensas changes ends over here
                                //**************************************************************************************************
                                ResourceManager.Transaction.ResetActivity(data.Payload.TransactionID);
                                PayloadManager.OrderPayload.Items = data.Payload.Items;
                                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                                moveToView.NextViewName = ViewModelMappings.View.PickOrder;
                            }
                            string[] arr = { ActivityKey.HoldAtPick.ToString(), ActivityKey.HoldAtDeliverToCustomer.ToString(), ActivityKey.HoldAtCashCollection.ToString(), ActivityKey.HoldAtOrderEntry.ToString() };
                            if (arr.Contains(data.ActivityStateKey.Trim()))
                            {
                                #region Update Activity Count

                                string tempStopDate = string.Empty;
                                string stopID = string.Empty;
                                if (PayloadManager.OrderPayload.StopDate.Date != DateTime.Now.Date)
                                {
                                    CustomerManager cus = new CustomerManager();
                                    cus.CloseActivity(data.Payload.StopID);

                                    tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                                    stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.OrderPayload.Customer.CustomerNo, tempStopDate);
                                }
                                else
                                {
                                    tempStopDate = PayloadManager.OrderPayload.StopDate.Date.ToString("yyyy-MM-dd");
                                    stopID = PayloadManager.OrderPayload.StopID;
                                }
                                new CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                                new CustomerManager().UpdateActivityCount(stopID, true, false);
                                PayloadManager.OrderPayload.Customer.PendingActivity = new Managers.CustomerManager().GetPendingAtivityForStop(stopID);
                                //PayloadManager.OrderPayload.Customer.PendingActivity += 1;
                                PayloadManager.OrderPayload.Customer.SaleStatus = "";
                                new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.OrderPayload.Customer.CustomerNo);

                                #endregion
                            }
                            ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.CustomerHome_Activity.ToString();

                            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                            //IsBusy = false;

                            #endregion
                        }
                        if (param.GetType() == typeof(PreorderActivityListItem))
                        {
                            #region Implentation Logic

                            PreorderActivityListItem data = (PreorderActivityListItem)param;

                            if (data.ActivityStateKey.Trim() == ActivityKey.PreOrder.ToString() || data.ActivityStateKey.Trim() == ActivityKey.PreOrderChange.ToString())
                            {
                                // Check is only for today as activity can be performed only today, if visitee has physical stop today
                                PayloadManager.PreOrderPayload.Customer = new CustomerDashboard().GetNewCustomer(DateTime.Today.Date, data.Payload.Customer);

                                if (DateTime.Today.Date < data.Payload.StopDate && data.ActivityTime.StringToUSDateFormat().Date != DateTime.Today.Date)
                                {
                                    PayloadManager.PreOrderPayload.IsFromActivityTab = true;
                                }
                                if (DateTime.Today.Date == data.ActivityTime.StringToUSDateFormat().Date)
                                {
                                    PayloadManager.PreOrderPayload.IsPreOrderCreatedFromUnplanned = true;
                                }
                                else
                                    PayloadManager.PreOrderPayload.IsPreOrderCreatedFromUnplanned = false;

                                if (DateTime.Today.Date == data.Payload.StopDate)
                                    PayloadManager.PreOrderPayload.IsReadOnly = true;

                                GetWarnPopupForPastActivity(DateTime.Parse(data.ActivityTime, CultureInfo.CreateSpecificCulture("en-US")), false, data.CustomerID);

                                PayloadManager.PreOrderPayload.PreOrderStops = data.Payload.PreOrderStops;
                                PayloadManager.PreOrderPayload.PreOrderItems = data.Payload.PreOrderItems;
                                ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.CustomerHome_Activity.ToString();
                                var moveToView = new Helpers.NavigateToView { CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, ShowAsModal = false, Refresh = true };
                                moveToView.NextViewName = ViewModelMappings.View.PreOrder;
                                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                            }

                            #endregion
                        }
                        if (param.GetType() == typeof(PaymentActivityListItem))
                        {
                            #region implemetation logic

                            PaymentActivityListItem data = (PaymentActivityListItem)param;
                            PayloadManager.PaymentPayload.TransactionID = data.Payload.TransactionID;
                            PayloadManager.PaymentPayload.Customer = data.Payload.Customer;// **

                            //************************************************************************************************
                            // Comment: Passed the values to payment Stopdate data from the payload manager 
                            // Created: Jan 28, 2016
                            // Author: Vivensas (Rajesh,Yuvaraj)
                            // Revisions: 
                            //*************************************************************************************************
                            PayloadManager.PaymentPayload.Customer.StopDate = PayloadManager.OrderPayload.Customer.StopDate;

                            //*************************************************************************************************
                            // Vivensas changes ends over here
                            //**************************************************************************************************

                            var activities = (new CustomerActivityManager()).GetActivities(CommonNavInfo.Customer.CustomerNo);

                            if (activities.Count == 0)
                                return;

                            Activity objActivity = activities.Where(o => o.ActivityID == data.Payload.TransactionID).FirstOrDefault<Activity>();

                            if (objActivity == null)
                                return;

                            PaymentPayload objPaymentload = objActivity.ActivityDetails.DeserializeJson<PaymentPayload>();
                            PayloadManager.PaymentPayload.ReceiptId = objPaymentload.TransactionID.Replace("receipt#", "");
                            ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.CustomerHome_Activity.ToString();

                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Customer Home";
                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Payments & AR";
                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.CustomerHome;
                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ARPayment;
                            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ARPayment, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, Payload = null, ShowAsModal = false, Refresh = true };
                            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);

                            #endregion
                        }

                        if (param.GetType() == typeof(CreditProcessListItem))
                        {
                            ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.CustomerHome_Activity.ToString();
                            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, Payload = null, ShowAsModal = false };
                            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                        }
                        if (param.GetType() == typeof(QuoteActivityListItem))
                        {
                            #region Implemettion logic

                            QuoteActivityListItem data = (QuoteActivityListItem)param;

                            //PayloadManager.ProspectPayload = data.ProspectPayload;
                            //PayloadManager.QuotePayload = data.QuotePayload;
                            ViewModelPayload.PayloadManager.QuotePayload.QuoteId = data.QuoteId;
                            PayloadManager.ProspectPayload.IsProspect = data.IsProspect;
                            if (data.IsProspect)
                            {
                                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = ViewModelMappings.View.ProspectQuote.GetEnumDescription();
                                SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = ViewModelMappings.View.ProspectHome.GetEnumDescription();
                                ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.ProspectHome_Activity.ToString();
                                ViewModelPayload.PayloadManager.ProspectPayload.ShowEditModeButton = true;
                                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ProspectQuote, CurrentViewName = ViewModelMappings.View.ProspectHome, CloseCurrentView = false, ShowAsModal = false };
                                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                            }
                            else
                            {
                                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = ViewModelMappings.View.CustomerQuote.GetEnumDescription();
                                SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = ViewModelMappings.View.CustomerHome.GetEnumDescription();
                                ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.CustomerHome_Activity.ToString();
                                ViewModelPayload.PayloadManager.ProspectPayload.ShowEditModeButton = true;
                                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerQuote, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, ShowAsModal = false };
                                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                            }

                            #endregion
                        }
                    }
                    else
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.Common.PreTripErrorMsg, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        if (alertMessage.Closed)
                            return;

                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.CustomerHome, Refresh = true, CloseCurrentView = true, ShowAsModal = false };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][CustomerActivity][ShowActivity][ExceptionStackTrace = " + ex.Message == null ? ex.InnerException.Message : ex.Message + "]");
                }
            });
        }

        void GetWarnPopupForPastActivity(DateTime activityTime, bool isProspect, string customerNo)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerActivity][Start:GetWarnPopupForPastActivity]");
            try
            {
                if (activityTime.Date == DateTime.Today.Date)
                    return;

                if (activityTime.Date < DateTime.Today.Date)
                {
                    var alartMessage = new Helpers.AlertWindow { Message = Helpers.Constants.CustomerDashboard.AlertStatement, MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alartMessage, CommonNavInfo.MessageToken);
                    return;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerActivity][GetWarnPopupForPastActivity][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerActivity][End:GetWarnPopupForPastActivity]");
        }

        private bool disposed = false;
        private readonly System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);

        protected override void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            disposed = true;
        }
    }

    public class QuoteActivityListItem : ActivityListItem
    {
        //public ViewModelPayload.QuotePayload QuotePayload { get; set; }
        //public ViewModelPayload.ProspectPayload ProspectPayload { get; set; }
        public string ActivityState { get; set; }

        public string ActivityTime { get; set; }

        public string ActivityStateKey { get; set; }

        public bool IsProspect { get; set; }

        public string QuoteId { get; set; }

        public bool IsPrinted { get; set; }

        public int ItemsCount { get; set; }

        public string PriceSetup { get; set; }

        public string SettlementId { get; set; }

        public string ProspectOrCustId { get; set; }
    }
}