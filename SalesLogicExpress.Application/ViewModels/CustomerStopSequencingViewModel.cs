﻿using System;
using System.Linq;
using System.Threading.Tasks;
//using WpfApplicationForStudy.Model;
//using WpfApplicationForStudy.Behavior;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls;
using SalesLogicExpress.Domain;
using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.Managers;
using System.Data;
using SalesLogicExpress.Application.Helpers;
using log4net;

namespace SalesLogicExpress.Application.ViewModels
{
    public class CustomerStopSequencingViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CustomerStopSequencingViewModel");
        private static CustomerStopSequencingViewModel self;
        private bool isList1Selected;
        private bool isList2Selected;
        private bool isList3Selected;
        private bool isList4Selected;
        private bool isList1AllowDrop = false;
        private bool isList2AllowDrop = false;
        private bool isList3AllowDrop = false;
        private bool isList4AllowDrop = false;
        private bool isChangedList1 = false;
        private bool isChangedList2 = false;
        private bool isChangedList3 = false;
        private bool isChangedList4 = false;
        private bool isSaveResetEnable = false;

        private int previousIndex = -1;
        private int newIndex = -1;
        #region Properties
        public Guid MessageToken { get; set; }
        private ObservableCollection<CustomerStopSequencingModel> customerStopSequencingW1List;
        private ObservableCollection<CustomerStopSequencingModel> customerStopSequencingW2List;
        private ObservableCollection<CustomerStopSequencingModel> customerStopSequencingW3List;
        private ObservableCollection<CustomerStopSequencingModel> customerStopSequencingW4List;
        private ObservableCollection<DayList> dayListForCombo;
        private CustomerStopSequencingModel customerStopSequencing;
        private CustomerStopSequencingModel customerStopSequencing1;
        private CustomerStopSequencingModel customerStopSequencing2;
        private CustomerStopSequencingModel customerStopSequencing3;
        private CustomerStopSequencingModel customerStopSequencing4;
        public CustomerStopSequencingModel CustomerStopSequencing4
        {
            get
            {
                return this.customerStopSequencing4;
            }
            set
            {
                if (value != null)
                {
                    CustomerStopSequencing1 = null;
                    CustomerStopSequencing3 = null;
                    CustomerStopSequencing2 = null;
                }
                this.customerStopSequencing4 = value;

                OnPropertyChanged("CustomerStopSequencing4");

            }
        }
        public CustomerStopSequencingModel CustomerStopSequencing3
        {
            get
            {
                return this.customerStopSequencing3;
            }
            set
            {
                if (value != null)
                {
                    CustomerStopSequencing1 = null;
                    CustomerStopSequencing2 = null;
                    CustomerStopSequencing4 = null;
                }
                else
                {

                }
                this.customerStopSequencing3 = value;

                OnPropertyChanged("CustomerStopSequencing3");


            }
        }
        public CustomerStopSequencingModel CustomerStopSequencing2
        {
            get
            {
                return this.customerStopSequencing2;
            }
            set
            {
                if (value != null)
                {

                    CustomerStopSequencing1 = null;
                    CustomerStopSequencing3 = null;
                    CustomerStopSequencing4 = null;
                }
                this.customerStopSequencing2 = value;
                OnPropertyChanged("CustomerStopSequencing2");

            }
        }
        public CustomerStopSequencingModel CustomerStopSequencing1
        {
            get
            {
                return this.customerStopSequencing1;
            }
            set
            {
                if (value != null)
                {
                    CustomerStopSequencing2 = null;
                    CustomerStopSequencing3 = null;
                    CustomerStopSequencing4 = null;
                }
                this.customerStopSequencing1 = value;
                OnPropertyChanged("CustomerStopSequencing1");

            }
        }
        private DayList dayListItem;
        public bool IsSaveResetEnable
        {
            get
            {
                return this.isSaveResetEnable;
            }
            set
            {
                this.isSaveResetEnable = value;
                OnPropertyChanged("IsSaveResetEnable");
            }
        }
        public bool IsChangedList4
        {
            get
            {
                return this.isChangedList4;
            }
            set
            {
                this.isChangedList4 = value;
                OnPropertyChanged("IsChangedList4");
            }
        }
        public bool IsChangedList3
        {
            get
            {
                return this.isChangedList3;
            }
            set
            {
                this.isChangedList3 = value;
                OnPropertyChanged("IsChangedList3");
            }
        }
        public bool IsChangedList2
        {
            get
            {
                return this.isChangedList2;
            }
            set
            {
                this.isChangedList2 = value;
                OnPropertyChanged("IsChangedList2");
            }
        }
        public bool IsChangedList1
        {
            get
            {
                return this.isChangedList1;
            }
            set
            {
                this.isChangedList1 = value;
                OnPropertyChanged("IsChangedList1");
            }
        }
        public bool IsList4AllowDrop
        {
            get
            {
                return this.isList4AllowDrop;
            }
            set
            {
                this.isList4AllowDrop = value;
                OnPropertyChanged("IsList4AllowDrop");
            }
        }
        public bool IsList3AllowDrop
        {
            get
            {
                return this.isList3AllowDrop;
            }
            set
            {
                this.isList3AllowDrop = value;
                OnPropertyChanged("IsList3AllowDrop");
            }
        }
        public bool IsList2AllowDrop
        {
            get
            {
                return this.isList2AllowDrop;
            }
            set
            {
                this.isList2AllowDrop = value;
                OnPropertyChanged("IsList2AllowDrop");
            }
        }
        public bool IsList1AllowDrop
        {
            get
            {
                return this.isList1AllowDrop;
            }
            set
            {
                this.isList1AllowDrop = value;
                OnPropertyChanged("IsList1AllowDrop");
            }
        }
        public bool IsList4Selected
        {
            get
            {
                return this.isList4Selected;
            }
            set
            {
                IsList1AllowDrop = false;
                IsList2AllowDrop = false;
                IsList3AllowDrop = false;
                IsList4AllowDrop = true;
                this.isList4Selected = value;
                OnPropertyChanged("IsList4Selected");
            }
        }
        public bool IsList3Selected
        {
            get
            {
                return this.isList3Selected;
            }
            set
            {
                IsList1AllowDrop = false;
                IsList2AllowDrop = false;
                IsList4AllowDrop = false;
                IsList3AllowDrop = true;
                this.isList3Selected = value;
                OnPropertyChanged("IsList3Selected");
            }
        }
        public bool IsList2Selected
        {
            get
            {
                return this.isList2Selected;
            }
            set
            {
                IsList1AllowDrop = false;
                IsList4AllowDrop = false;
                IsList3AllowDrop = false;
                IsList2AllowDrop = true;
                this.isList2Selected = value;
                OnPropertyChanged("IsList2Selected");
            }
        }
        public bool IsList1Selected
        {
            get
            {
                return this.isList1Selected;
            }
            set
            {
                IsList4AllowDrop = false;
                IsList2AllowDrop = false;
                IsList3AllowDrop = false;
                IsList1AllowDrop = true;
                this.isList1Selected = value;
                OnPropertyChanged("IsList1Selected");
            }
        }
        public DayList DayListItem
        {
            get
            {
                return this.dayListItem;
            }
            set
            {
                this.dayListItem = value;
                OnPropertyChanged("DayListItem");
            }
        }
        public CustomerStopSequencingModel CustomerStopSequencing
        {
            get
            {
                return this.customerStopSequencing;
            }
            set
            {
                this.customerStopSequencing = value;
                OnPropertyChanged("CustomerStopSequencing");
            }
        }
        public ObservableCollection<CustomerStopSequencingModel> CustomerStopSequencingW4List
        {
            get
            {
                return this.customerStopSequencingW4List;
            }
            set
            {
                this.customerStopSequencingW4List = value;
                OnPropertyChanged("CustomerStopSequencingW4List");
            }
        }
        public ObservableCollection<CustomerStopSequencingModel> CustomerStopSequencingW3List
        {
            get
            {
                return this.customerStopSequencingW3List;
            }
            set
            {
                this.customerStopSequencingW3List = value;
                OnPropertyChanged("customerStopSequencingW3List");
            }
        }
        public ObservableCollection<CustomerStopSequencingModel> CustomerStopSequencingW2List
        {
            get
            {
                return this.customerStopSequencingW2List;
            }
            set
            {
                this.customerStopSequencingW2List = value;
                OnPropertyChanged("CustomerStopSequencingW2List");
            }
        }
        public ObservableCollection<CustomerStopSequencingModel> CustomerStopSequencingW1List
        {
            get
            {
                return this.customerStopSequencingW1List;
            }
            set
            {
                this.customerStopSequencingW1List = value;
                OnPropertyChanged("CustomerStopSequencingW1List");
            }
        }
        public ObservableCollection<DayList> DayListForCombo
        {
            get
            {
                return this.dayListForCombo;
            }
            set
            {
                this.dayListForCombo = value;
                OnPropertyChanged("DayListForCombo");
            }
        }
        #endregion

        #region Methods, Constructor & Handlers
        async void GetCustomerStopSequencing(DayList days)
        {

            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][AsyncStart:GetCustomerStopSequencing]");
                try
                {
                    //System.Threading.Thread.Sleep(5000);
                    CustomerStopSequencing = new CustomerStopSequencingModel();
                    CustomerStopSequencingW1List = new ObservableCollection<CustomerStopSequencingModel>();
                    CustomerStopSequencingW2List = new ObservableCollection<CustomerStopSequencingModel>();
                    CustomerStopSequencingW3List = new ObservableCollection<CustomerStopSequencingModel>();
                    CustomerStopSequencingW4List = new ObservableCollection<CustomerStopSequencingModel>();

                    ObservableCollection<CustomerStopSequencingModel> customerStopSequencingList = new ObservableCollection<CustomerStopSequencingModel>();
                    customerStopSequencingList = Managers.CustomerStopSequencingManager.GetCustomerStopSequencingForWeek(days.Index, days.Day);
                    CustomerStopSequencing.DayOfWeek = days.Day;
                    CustomerStopSequencingW1List = new ObservableCollection<CustomerStopSequencingModel>(customerStopSequencingList.Where(a => a.ListID == 1.ToString()).ToList());
                    CustomerStopSequencingW2List = new ObservableCollection<CustomerStopSequencingModel>(customerStopSequencingList.Where(a => a.ListID == 2.ToString()).ToList());
                    CustomerStopSequencingW3List = new ObservableCollection<CustomerStopSequencingModel>(customerStopSequencingList.Where(a => a.ListID == 3.ToString()).ToList());
                    CustomerStopSequencingW4List = new ObservableCollection<CustomerStopSequencingModel>(customerStopSequencingList.Where(a => a.ListID == 4.ToString()).ToList());

                    CustomerStopSequencingW4List.CollectionChanged += CustomerStopSequencingW4List_CollectionChanged;
                    CustomerStopSequencingW3List.CollectionChanged += CustomerStopSequencingW3List_CollectionChanged;
                    CustomerStopSequencingW2List.CollectionChanged += CustomerStopSequencingW2List_CollectionChanged;
                    CustomerStopSequencingW1List.CollectionChanged += CustomerStopSequencingW1List_CollectionChanged;
                    IsBusy = false;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][GetCustomerStopSequencing][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][AsyncEnd:GetCustomerStopSequencing]");
            });
        }
        public void UpdateCustomerStopSequence()
        {
            //To check first load, Get dates present in M56M0004 for day along with customer counts and week no

            System.Data.DataTable dtDates = CustomerStopSequencingManager.GetDatesForDayOfWeek(DayListItem.Index + 1);
            bool isM56M0004toBeUpdate = false;

            #region List1
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][Start:UpdateCustomerStopSequence]" + "" + "List1");
            try
            {
                if (IsChangedList1)
                {
                    //dtSort is sorted datable from dtDates for respected week no.
                    DataTable dtSort = dtDates.Clone();
                    for (int i = 0; i < dtDates.Rows.Count; i++)
                    {
                        if (dtDates.Rows[i]["WeekNo"].ToString() == "1")
                        {
                            DataRow dr = dtSort.NewRow();
                            dr["Date"] = dtDates.Rows[i]["Date"].ToString();
                            dr["WeekNo"] = dtDates.Rows[i]["WeekNo"].ToString();
                            dr["CustCount"] = dtDates.Rows[i]["CustCount"].ToString();
                            dtSort.Rows.Add(dr);
                            isM56M0004toBeUpdate = true;
                        }
                    }

                    for (int i = 0; i < CustomerStopSequencingW1List.Count; i++)
                    {
                        string customerID = CustomerStopSequencingW1List[i].CustomerCode.ToString().Trim();
                        string dayNo = CustomerStopSequencingW1List[i].DayNo.ToString().Trim();
                        string weekNo = CustomerStopSequencingW1List[i].ListID.ToString().Trim();
                        string descCode = CustomerStopSequencingW1List[i].IdCode.ToString().Trim();
                        string sequenceNO = i.ToString().Trim();
                        Managers.CustomerStopSequencingManager.UpadateSequenceNo(dayNo, customerID, weekNo, descCode, sequenceNO);
                        if (isM56M0004toBeUpdate)
                        {
                            for (int j = 0; j < dtSort.Rows.Count; j++)
                            {
                                string date = dtSort.Rows[j]["Date"].ToString();
                                int custCount = Convert.ToInt32(dtSort.Rows[j]["CustCount"].ToString());
                                //CustomerStopSequencingW1List.Count is no of customers in template. It includes only planned cust.
                                //custCount is no. of cust for date. It may include Reschedule and Unplanned cust.
                                //if both counts are same means no R and U
                                if (custCount == CustomerStopSequencingW1List.Count)
                                {
                                    //Sequence no starts from 1. So here passing (i+1)
                                    Managers.CustomerStopSequencingManager.UpadateSequenceNoForM56M0004(date, customerID, (i + 1).ToString());
                                }
                                else
                                {
                                    //Add count of R and U for sequence no. of planned
                                    Managers.CustomerStopSequencingManager.UpadateSequenceNoForM56M0004(date, customerID, (i + 1 + (custCount - CustomerStopSequencingW1List.Count)).ToString());
                                    if (i == 0)//Update sequence no of rescheduled and unplanned customers
                                    {
                                        //Arrange Rescheduled
                                        Managers.CustomerStopSequencingManager.UpadateReschduledAndUnplanned(date);
                                    }
                                }

                            }
                        }
                    }
                    isM56M0004toBeUpdate = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][UpdateCustomerStopSequence][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][End:UpdateCustomerStopSequence]" + "" + "List1");

            #endregion

            #region List2
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][Start:UpdateCustomerStopSequence]" + "" + "List2");
            try
            {
                if (IsChangedList2)
                {
                    DataTable dtSort = dtDates.Clone();
                    for (int i = 0; i < dtDates.Rows.Count; i++)
                    {
                        if (dtDates.Rows[i]["WeekNo"].ToString() == "2")
                        {
                            DataRow dr = dtSort.NewRow();
                            dr["Date"] = dtDates.Rows[i]["Date"].ToString();
                            dr["WeekNo"] = dtDates.Rows[i]["WeekNo"].ToString();
                            dr["CustCount"] = dtDates.Rows[i]["CustCount"].ToString();
                            dtSort.Rows.Add(dr);
                            isM56M0004toBeUpdate = true;
                        }
                    }

                    for (int i = 0; i < CustomerStopSequencingW2List.Count; i++)
                    {
                        string customerID = CustomerStopSequencingW2List[i].CustomerCode.ToString().Trim();
                        string dayNo = CustomerStopSequencingW2List[i].DayNo.ToString().Trim();
                        string weekNo = CustomerStopSequencingW2List[i].ListID.ToString().Trim();
                        string descCode = CustomerStopSequencingW2List[i].IdCode.ToString().Trim();
                        string sequenceNO = i.ToString().Trim();
                        Managers.CustomerStopSequencingManager.UpadateSequenceNo(dayNo, customerID, weekNo, descCode, sequenceNO);


                        if (isM56M0004toBeUpdate)
                        {
                            for (int j = 0; j < dtSort.Rows.Count; j++)
                            {
                                string date = dtSort.Rows[j]["Date"].ToString();
                                int custCount = Convert.ToInt32(dtSort.Rows[j]["CustCount"].ToString());
                                if (custCount == CustomerStopSequencingW2List.Count)
                                {
                                    Managers.CustomerStopSequencingManager.UpadateSequenceNoForM56M0004(date, customerID, (i + 1).ToString());
                                }
                                else
                                {
                                    Managers.CustomerStopSequencingManager.UpadateSequenceNoForM56M0004(date, customerID, (i + 1 + (custCount - CustomerStopSequencingW2List.Count)).ToString());
                                    if (i == 0)//Update sequence no of rescheduled and unplanned customers
                                    {
                                        //Arrange Rescheduled
                                        Managers.CustomerStopSequencingManager.UpadateReschduledAndUnplanned(date);
                                    }
                                }

                            }
                        }
                    }
                    isM56M0004toBeUpdate = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][UpdateCustomerStopSequence][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][End:UpdateCustomerStopSequence]" + "" + "List2");

            #endregion

            #region List3
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][Start:UpdateCustomerStopSequence]" + "" + "List3");
            try
            {
                if (IsChangedList3)
                {
                    DataTable dtSort = dtDates.Clone();
                    for (int i = 0; i < dtDates.Rows.Count; i++)
                    {
                        if (dtDates.Rows[i]["WeekNo"].ToString() == "3")
                        {
                            DataRow dr = dtSort.NewRow();
                            dr["Date"] = dtDates.Rows[i]["Date"].ToString();
                            dr["WeekNo"] = dtDates.Rows[i]["WeekNo"].ToString();
                            dr["CustCount"] = dtDates.Rows[i]["CustCount"].ToString();
                            dtSort.Rows.Add(dr);
                            isM56M0004toBeUpdate = true;
                        }
                    }
                    for (int i = 0; i < CustomerStopSequencingW3List.Count; i++)
                    {
                        string customerID = CustomerStopSequencingW3List[i].CustomerCode.ToString().Trim();
                        string dayNo = CustomerStopSequencingW3List[i].DayNo.ToString().Trim();
                        string weekNo = CustomerStopSequencingW3List[i].ListID.ToString().Trim();
                        string descCode = CustomerStopSequencingW3List[i].IdCode.ToString().Trim();
                        string sequenceNO = i.ToString().Trim();
                        Managers.CustomerStopSequencingManager.UpadateSequenceNo(dayNo, customerID, weekNo, descCode, sequenceNO);
                        if (isM56M0004toBeUpdate)
                        {
                            for (int j = 0; j < dtSort.Rows.Count; j++)
                            {
                                string date = dtSort.Rows[j]["Date"].ToString();
                                int custCount = Convert.ToInt32(dtSort.Rows[j]["CustCount"].ToString());
                                if (custCount == CustomerStopSequencingW3List.Count)
                                {
                                    Managers.CustomerStopSequencingManager.UpadateSequenceNoForM56M0004(date, customerID, (i + 1).ToString());
                                }
                                else
                                {
                                    Managers.CustomerStopSequencingManager.UpadateSequenceNoForM56M0004(date, customerID, (i + 1 + (custCount - CustomerStopSequencingW3List.Count)).ToString());
                                    if (i == 0)//Update sequence no of rescheduled and unplanned customers
                                    {
                                        //Arrange Rescheduled
                                        Managers.CustomerStopSequencingManager.UpadateReschduledAndUnplanned(date);
                                    }
                                }

                            }
                        }
                    }
                    isM56M0004toBeUpdate = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][UpdateCustomerStopSequence][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][End:UpdateCustomerStopSequence]" + "" + "List3");

            #endregion

            #region List4
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][Start:UpdateCustomerStopSequence]" + "" + "List4");
            try
            {
                if (IsChangedList4)
                {
                    DataTable dtSort = dtDates.Clone();
                    for (int i = 0; i < dtDates.Rows.Count; i++)
                    {
                        if (dtDates.Rows[i]["WeekNo"].ToString() == "4")
                        {
                            DataRow dr = dtSort.NewRow();
                            dr["Date"] = dtDates.Rows[i]["Date"].ToString();
                            dr["WeekNo"] = dtDates.Rows[i]["WeekNo"].ToString();
                            dr["CustCount"] = dtDates.Rows[i]["CustCount"].ToString();
                            dtSort.Rows.Add(dr);
                            isM56M0004toBeUpdate = true;
                        }
                    }
                    for (int i = 0; i < CustomerStopSequencingW4List.Count; i++)
                    {
                        string customerID = CustomerStopSequencingW4List[i].CustomerCode.ToString().Trim();
                        string dayNo = CustomerStopSequencingW4List[i].DayNo.ToString().Trim();
                        string weekNo = CustomerStopSequencingW4List[i].ListID.ToString().Trim();
                        string descCode = CustomerStopSequencingW4List[i].IdCode.ToString().Trim();
                        string sequenceNO = i.ToString().Trim();
                        Managers.CustomerStopSequencingManager.UpadateSequenceNo(dayNo, customerID, weekNo, descCode, sequenceNO);
                        if (isM56M0004toBeUpdate)
                        {
                            for (int j = 0; j < dtSort.Rows.Count; j++)
                            {
                                string date = dtSort.Rows[j]["Date"].ToString();
                                int custCount = Convert.ToInt32(dtSort.Rows[j]["CustCount"].ToString());
                                if (custCount == CustomerStopSequencingW4List.Count)
                                {
                                    Managers.CustomerStopSequencingManager.UpadateSequenceNoForM56M0004(date, customerID, (i + 1).ToString());
                                }
                                else
                                {
                                    Managers.CustomerStopSequencingManager.UpadateSequenceNoForM56M0004(date, customerID, (i + 1 + (custCount - CustomerStopSequencingW4List.Count)).ToString());
                                    if (i == 0)//Update sequence no of rescheduled and unplanned customers
                                    {
                                        //Arrange Rescheduled
                                        Managers.CustomerStopSequencingManager.UpadateReschduledAndUnplanned(date);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][UpdateCustomerStopSequence][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][End:UpdateCustomerStopSequence]" + "" + "List4");

            #endregion

        }
        void CustomerStopSequencingW1List_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][Start:CustomerStopSequencingW1List_CollectionChanged]");
            try
            {
                if (!IsChangedList1)
                {
                    if (e.OldStartingIndex == -1)
                    {
                        newIndex = e.NewStartingIndex;
                    }
                    if (e.NewStartingIndex == -1)
                    {
                        previousIndex = e.OldStartingIndex;
                    }
                    if (previousIndex != -1 && newIndex != -1)
                    {
                        if (previousIndex != newIndex)
                        {
                            IsChangedList1 = true;
                            IsSaveResetEnable = true;
                            IsDirty = true;
                        }
                        newIndex = previousIndex = -1;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][CustomerStopSequencingW1List_CollectionChanged][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][End:CustomerStopSequencingW1List_CollectionChanged]");

        }
        void CustomerStopSequencingW2List_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][Start:CustomerStopSequencingW2List_CollectionChanged]");
            try
            {
                if (!IsChangedList2)
                {
                    if (e.OldStartingIndex == -1)
                    {
                        newIndex = e.NewStartingIndex;
                    }
                    if (e.NewStartingIndex == -1)
                    {
                        previousIndex = e.OldStartingIndex;
                    }
                    if (previousIndex != -1 && newIndex != -1)
                    {
                        if (previousIndex != newIndex)
                        {
                            IsChangedList2 = true;
                            IsSaveResetEnable = true;
                            IsDirty = true;
                        }
                        newIndex = previousIndex = -1;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][CustomerStopSequencingW2List_CollectionChanged][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][End:CustomerStopSequencingW2List_CollectionChanged]");

        }
        void CustomerStopSequencingW3List_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][Start:CustomerStopSequencingW3List_CollectionChanged]");
            try
            {
                if (!IsChangedList3)
                {
                    if (e.OldStartingIndex == -1)
                    {
                        newIndex = e.NewStartingIndex;
                    }
                    if (e.NewStartingIndex == -1)
                    {
                        previousIndex = e.OldStartingIndex;
                    }
                    if (previousIndex != -1 && newIndex != -1)
                    {
                        if (previousIndex != newIndex)
                        {
                            IsChangedList3 = true;
                            IsSaveResetEnable = true;
                            IsDirty = true;
                        }
                        newIndex = previousIndex = -1;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][CustomerStopSequencingW3List_CollectionChanged][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][End:CustomerStopSequencingW3List_CollectionChanged]");

        }
        void CustomerStopSequencingW4List_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][Start:CustomerStopSequencingW4List_CollectionChanged]");
            try
            {
                if (!IsChangedList4)
                {
                    if (e.OldStartingIndex == -1)
                    {
                        newIndex = e.NewStartingIndex;
                    }
                    if (e.NewStartingIndex == -1)
                    {
                        previousIndex = e.OldStartingIndex;
                    }
                    if (previousIndex != -1 && newIndex != -1)
                    {
                        if (previousIndex != newIndex)
                        {
                            IsChangedList4 = true;
                            IsSaveResetEnable = true;
                            IsDirty = true;
                        }
                        newIndex = previousIndex = -1;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][CustomerStopSequencingW4List_CollectionChanged][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][End:CustomerStopSequencingW4List_CollectionChanged]");

        }
        public void GetDaysList()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][Start:GetDaysList]");
            try
            {
                DayListForCombo = new ObservableCollection<DayList>();
                DayList objdl = new DayList();
                objdl.Day = "Monday";
                objdl.Index = 1;
                DayListForCombo.Add(objdl);

                objdl = new DayList();
                objdl.Day = "Tuesday";
                objdl.Index = 2;
                DayListForCombo.Add(objdl);

                objdl = new DayList();
                objdl.Day = "Wednesday";
                objdl.Index = 3;
                DayListForCombo.Add(objdl);

                objdl = new DayList();
                objdl.Day = "Thursday";
                objdl.Index = 4;
                DayListForCombo.Add(objdl);

                objdl = new DayList();
                objdl.Day = "Friday";
                objdl.Index = 5;
                DayListForCombo.Add(objdl);

                DayListItem = new DayList();
                DayListItem.Day = DayListForCombo[0].Day;
                DayListItem.Index = DayListForCombo[0].Index;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][GetDaysList][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerStopSequencingViewModel][End:GetDaysList]");

        }
        public override bool ConfirmSave()
        {
            IsDirty = false;
            UpdateCustomerStopSequence();
            CanNavigate = true;
            return base.ConfirmSave();
        }
        public override bool ConfirmCancel()
        {
            IsDirty = false;
            CanNavigate = true;
            return base.ConfirmCancel();
        }
        public void ResetFlags()
        {
            IsChangedList4 = IsChangedList3 = IsChangedList2 = IsChangedList1 = IsSaveResetEnable = IsDirty = false;
            newIndex = -1; previousIndex = -1;
        }
        public CustomerStopSequencingViewModel()
        {
            InitializeCommands();
            GetDaysList();
            SelectedTab = ViewModelMappings.TabView.ServiceRoute.Customers;
            IsBusy = true;
            GetCustomerStopSequencing(DayListItem);

        }
        public DelegateCommand Reset { get; set; }
        public DelegateCommand Save { get; set; }
        public DelegateCommand DragOverItem { get; set; }
        public DelegateCommand SelectionChangedCommand { get; set; }
        void InitializeCommands()
        {
            Reset = new DelegateCommand((param) =>
            {
                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.CustStopSequencing.Reset, MessageIcon = "Alert", Confirmed = false, OkButtonContent = "Yes", CancelButtonContent = "No" };
                if (IsChangedList4 || IsChangedList3 || IsChangedList2 || IsChangedList1)
                {
                    confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.CustStopSequencing.Reset, MessageIcon = "Alert", Confirmed = false, OkButtonContent = "Yes", CancelButtonContent = "No" };
                    Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                    if (confirmMessage.Confirmed)
                    {
                        // Reset changes 
                        IsBusy = true;
                        GetCustomerStopSequencing(DayListItem);
                        ResetFlags();
                    }
                }
                else
                {
                    var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.CustStopSequencing.ResetAlter, MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                }
            });

            Save = new DelegateCommand((param) =>
            {
                if (IsChangedList4 || IsChangedList3 || IsChangedList2 || IsChangedList1)
                {
                    //Saves Changes 
                    UpdateCustomerStopSequence();
                    //var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.CustStopSequencing.SavedChanges, MessageIcon = "Alert" };
                    //Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                }
                //else
                //{
                //    ////Show message , no changes made
                //    //var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.CustStopSequencing.ResetAlter, MessageIcon = "Alert" };
                //    //Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                //}
                ResetFlags();
            });

            SelectionChangedCommand = new DelegateCommand((param) =>
            {
                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.CustStopSequencing.ResetConfirmation, MessageIcon = "Alert", Confirmed = false, OkButtonContent = "Yes", CancelButtonContent = "No" };
                if (IsChangedList4 || IsChangedList3 || IsChangedList2 || IsChangedList1)
                {
                    confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.CustStopSequencing.DayChangeConfirmation, MessageIcon = "Alert", Confirmed = false, OkButtonContent = "Yes", CancelButtonContent = "No" };
                    Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                    if (confirmMessage.Confirmed)
                    {
                        // Save changes 
                        UpdateCustomerStopSequence();
                        //var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.CustStopSequencing.SavedChanges, MessageIcon = "Alert" };
                        //Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    }
                    //GetCustomerStopSequencing(DayListItem);

                }
                IsBusy = true;
                GetCustomerStopSequencing(DayListItem);
                ResetFlags();
            });
            DragOverItem = new DelegateCommand((param) =>
            {
                //DayList objdl = new DayList();
                //CustomerStopSequencingModel obj = (CustomerStopSequencingModel)param;
                int idx = Convert.ToInt16(param);
                switch (idx)
                {
                    case 1:
                        IsList1Selected = true;
                        break;
                    case 2:
                        IsList2Selected = true;
                        break;
                    case 3:
                        IsList3Selected = true;
                        break;
                    case 4:
                        IsList4Selected = true;
                        break;
                    default:
                        break;
                }

            });
        }

        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }

    public class DayList
    {
        int index;
        string day;
        public string Day
        {
            get
            {
                return this.day;
            }
            set
            {
                this.day = value;
            }
        }
        public int Index
        {
            get
            {
                return this.index;
            }
            set
            {
                this.index = value;
            }
        }
    }
}
