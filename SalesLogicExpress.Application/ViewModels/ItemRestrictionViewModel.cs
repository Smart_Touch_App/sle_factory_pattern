﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using Telerik.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.ObjectModel;
using Managers = SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System.Media;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Collections.Specialized;
using log4net;

namespace SalesLogicExpress.Application.ViewModels
{
    public class ItemRestrictionViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.ItemRestrictionViewModel");

        #region Properties
        private string itemNo;
        public string ItemNo
        {
            get
            {
                return itemNo;
            }
            set
            {
                itemNo = value;
                OnPropertyChanged("ItemNo");
            }
        }

        private string description;
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
                OnPropertyChanged("Description");
            }
        }

        private string flag;
        public string Flag
        {
            get
            {
                return flag;
            }
            set
            {
                flag = value;
                OnPropertyChanged("Flag");
            }
        }

        private bool isRestricted;
        public bool IsRestricted
        {
            get
            {
                return isRestricted;
            }
            set
            {
                isRestricted = value;
                OnPropertyChanged("IsRestricted");
            }
        }

        private bool isCollapsed = true;
        public bool IsCollapsed
        {
            get { return isCollapsed; }
            set
            {
                isCollapsed = value;
                OnPropertyChanged("IsCollapsed");
            }
        }

        private string itemRestrictionHeader;
        public string ItemRestrictionHeader
        {
            get
            {
                return itemRestrictionHeader;
            }
            set
            {
                itemRestrictionHeader = value;
                OnPropertyChanged("ItemRestrictionHeader");
            }
        }

        private string errorMessage = string.Empty;
        public string ErrorMessage
        {
            get { return errorMessage; }
            set
            {
                errorMessage = value;
                OnPropertyChanged("ErrorMessage");
            }
        }
        private bool isErrorBlockCollapsed = false;

        public bool IsErrorBlockCollapsed
        {
            get { return isErrorBlockCollapsed; }
            set
            {
                isErrorBlockCollapsed = value;
                OnPropertyChanged("IsErrorBlockCollapsed");
            }
        }

        private bool isComponentCollapsed = true;

        public bool IsComponentCollapsed
        {
            get { return isComponentCollapsed; }
            set
            {
                isComponentCollapsed = value;
                OnPropertyChanged("IsComponentCollapsed");
            }
        }
        public Models.Customer Customer { get; set; }
        public static string customerID = string.Empty;

        private string imageSource;
        public string ImageSource
        {
            get
            {
                return imageSource;
            }
            set
            {
                imageSource = value;
                OnPropertyChanged("ImageSource");
            }
        }

        private bool isItemRestrictionTabActive = false;
        public bool IsItemRestrictionTabActive
        {
            get { return isItemRestrictionTabActive; }
            set
            {
                isItemRestrictionTabActive = value;
                OnPropertyChanged("IsItemRestrictionTabActive");
            }
        }

        private string backgroundColorItemRestriction = string.Empty;
        public string BackgroundColorItemRestriction
        {
            get { return backgroundColorItemRestriction; }
            set
            {
                backgroundColorItemRestriction = value;
                OnPropertyChanged("BackgroundColorItemRestriction");
            }
        }

        private string totalRestrictedItems;
        public string TotalRestrictedItems
        {
            get { return totalRestrictedItems; }
            set
            {
                totalRestrictedItems = value;
                OnPropertyChanged("TotalRestrictedItems");
            }
        }

        private bool isTotalBlockCollapsed = true;

        public bool IsTotalBlockCollapsed
        {
            get { return isTotalBlockCollapsed; }
            set
            {
                isTotalBlockCollapsed = value;
                OnPropertyChanged("IsTotalBlockCollapsed");
            }
        }
        private ObservableCollection<Models.ItemRestriction> itemRestrictionList = new ObservableCollection<ItemRestriction>();
        public ObservableCollection<Models.ItemRestriction> ItemRestrictionList
        {
            get
            {
                return itemRestrictionList;
            }
            set
            {
                itemRestrictionList = value;
                OnPropertyChanged("ItemRestrictionList");
            }
        }

        #endregion

        #region Delegates

        #endregion
        #region Constructors and Methods

        public ItemRestrictionViewModel(string customerNO)
        {
            Logger.Info("ItemRestrictionViewModel Entered into ItemRestrictionViewModel(" + customerNO + ")");

            try
            {
                //this.Customer = Customer;
                customerID = customerNO;
                InitializeCommands();
                GetItemRestrictionList();
            }
            catch (Exception ex)
            {
                Logger.Error("ItemRestrictionViewModel ItemRestrictionViewModel(" + customerNO + ") error: " + ex.Message);
            }
            Logger.Info("ItemRestrictionViewModel Exit from into ItemRestrictionViewModel(" + customerNO + ")");

        }

        private void GetHeader(string flag)
        {
            Flag = flag;
            ItemRestrictionHeader = string.Empty;
            ImageSource = string.Empty;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ItemRestrictionViewModel][Start:GetHeader]");
            try
            {
                switch (Flag)
                {
                    case "I": ItemRestrictionHeader = "Included Items";
                        ImageSource = "../Resources/Images/Included-Items.png";
                        BackgroundColorItemRestriction = "#87ca70";
                        break;
                    case "E": ItemRestrictionHeader = "Excluded Items";
                        ImageSource = "../Resources/Images/Excluded-Items.png";
                        BackgroundColorItemRestriction = "#e94932";
                        break;
                    case " ": ItemRestrictionHeader = "No Restricted Items";
                        IsCollapsed = false;
                        ImageSource = "../Resources/Images/Blank.png";
                        BackgroundColorItemRestriction = "#sdfds";
                        IsTotalBlockCollapsed = false;
                        break;
                    default: ItemRestrictionHeader = "";
                        ImageSource = "../Resources/Images/Blank.png";
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ItemRestrictionViewModel][GetHeader][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ItemRestrictionViewModel][End:GetHeader]");
        }

        public void InitializeCommands()
        {
            
        }
        async void GetItemRestrictionList()
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ItemRestrictionViewModel][AsyncStart:GetItemRestrictionList]");
                try
                {
                Managers.ItemRestrictionManager itemRestriction = new Managers.ItemRestrictionManager();
                ItemRestrictionList = itemRestriction.GetItemRestrictionList(customerID);
                if (ItemRestrictionList != null)
                {
                    flag = itemRestriction.GetFlag(customerID).Split('-')[1];
                    GetHeader(flag);
                    TotalRestrictedItems = ItemRestrictionList.Count.ToString();
                    
                    if (ItemRestrictionList.Count == 0)
                    {
                        ErrorMessage = Helpers.Constants.Common.ItemRestrictionErrorMessage;
                        IsErrorBlockCollapsed = true;
                        IsComponentCollapsed = false;
                    }
                }
                else
                {
                    flag = " ";
                    GetHeader(flag);
                    ErrorMessage = Helpers.Constants.Common.NoRestrictionMessage;
                    IsErrorBlockCollapsed = true;
                    IsComponentCollapsed = false;
                    IsTotalBlockCollapsed = false;
                }
                itemRestriction = null; //Disposing The Objects --Vignesh D 
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ItemRestrictionViewModel][GetItemRestrictionList][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ItemRestrictionViewModel][AsyncEnd:GetItemRestrictionList]");
            });
        }
        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
