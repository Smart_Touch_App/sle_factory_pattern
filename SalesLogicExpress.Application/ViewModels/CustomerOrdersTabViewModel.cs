﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class CustomerOrdersTabViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CustomerReturnsTabViewModel");

        #region Delegates
        public DelegateCommand SetReturnOrder { get; set; }
        public DelegateCommand SetReturnItems { get; set; }
        public DelegateCommand PrintOrders { get; set; }

        public DelegateCommand ToggleSelectedOrderItem { get; set; }
        public DelegateCommand ToggleSelectedOrder { get; set; }
        #endregion
        #region Prop's

        private bool _IncludeAvailableZeroItems = false;

        /// <summary>
        /// Get or set flag for including zero available qty items
        /// If true, resfresh the data context of grid
        /// </summary>
        public bool IncludeAvailableZeroItems
        {
            get
            {
                return _IncludeAvailableZeroItems;
            }
            set
            {
                _IncludeAvailableZeroItems = value;
                OnPropertyChanged("IncludeAvailableZeroItems");

                IsReturnItemsEnable = false;
                IsPrintEnable = false;
                IsReturnOrderEnable = false;
                this.ReturnOrders.Clear();
                this.LoadOrders();
            }
        }

        bool _IsPrintEnable = false;
        public bool IsPrintEnable
        {
            get
            {
                return _IsPrintEnable;
            }
            set
            {
                _IsPrintEnable = value;
                OnPropertyChanged("IsPrintEnable");
            }
        }
        bool _IsReturnItemsEnable = false;
        public bool IsReturnItemsEnable
        {
            get
            {
                return _IsReturnItemsEnable;
            }
            set
            {
                _IsReturnItemsEnable = value;
                OnPropertyChanged("IsReturnItemsEnable");
            }
        }
        bool _IsReturnOrderEnable = false;
        public bool IsReturnOrderEnable
        {
            get
            {

                return _IsReturnOrderEnable;

            }
            set
            {
                _IsReturnOrderEnable = value;
                OnPropertyChanged("IsReturnOrderEnable");
            }
        }
        public int SelectedOrderId { get; set; }
        TrulyObservableCollection<ReturnItem> returnItems = new TrulyObservableCollection<ReturnItem>();
        TrulyObservableCollection<ReturnOrder> returnOrderList = new TrulyObservableCollection<ReturnOrder>();
        public TrulyObservableCollection<ReturnItem> ReturnItems
        {
            get
            {
                return returnItems;
            }
            set
            {
                returnItems = value;
                OnPropertyChanged("ReturnItems");
            }
        }


        TrulyObservableCollection<ReturnOrder> returnOrders = new TrulyObservableCollection<ReturnOrder>();
        public TrulyObservableCollection<ReturnOrder> ReturnOrders
        {
            get
            {
                if (returnOrders == null)
                {
                    returnOrders = new TrulyObservableCollection<ReturnOrder>();
                }

                return returnOrders;
            }
            set
            {

                returnOrders = value;
                OnPropertyChanged("ReturnOrders");
            }
        }
        bool selectCompleteOrder;

        public bool SelectCompleteOrder
        {
            get { return selectCompleteOrder; }
            set { selectCompleteOrder = value; OnPropertyChanged("SelectCompleteOrder"); }
        }

        public bool IsUnplannedCreatedForFuture { get; set; }
        public Guid MessageToken { get; set; }

        #endregion
        public CustomerOrdersTabViewModel(BaseViewModel ParentView, bool flag)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][Start:CustomerOrdersTabViewModel]");
            try
            {
                IsUnplannedCreatedForFuture = flag;
                this.ParentView = ParentView;
                this.ParentView.IsBusy = true;
                if (PayloadManager.ReturnOrderPayload.IsFromActivityTab)
                    this.ParentView.IsDirty = true;

                if (PayloadManager.ReturnOrderPayload.PickIsInProgress)
                    this.ParentView.IsDirty = true;
                LoadOrders();
                InitialzeCommands();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerOrdersTabViewModel.][CustomerOrdersTabViewModel][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][End:CustomerOrdersTabViewModel]");
        }
        Dictionary<string, bool> CompletelySelectedOrders = new Dictionary<string, bool>();
        private void InitialzeCommands()
        {


            SetReturnOrder = new DelegateCommand(param =>
            {

            });

            SetReturnItems = new DelegateCommand(param =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][Start:SetReturnItems]");
                try
                {
                    var r_itemsCollection = new List<ReturnItem>();
                    //foreach (var item in (((System.Collections.ObjectModel.ObservableCollection<object>)(param))))
                    //{
                    //    r_itemsCollection.Add(item as ReturnItem);
                    //}
                    foreach (var roitem in ReturnOrders)
                    {
                        foreach (var ritem in roitem.ReturnItems)
                        {
                            if (ritem.IsSelectedItem)
                            {
                                r_itemsCollection.Add(ritem);
                            }
                        }
                    }
                    PayloadManager.ReturnOrderPayload.ReturnItems = new TrulyObservableCollection<ReturnItem>();
                    bool ishold = true;
                    foreach (var item in r_itemsCollection)
                    {
                        ishold = new CustomerReturnsManager().IsHoldReturnItems(item.OrderID, item.ItemNumber);
                        if (ishold || PayloadManager.ReturnOrderPayload.IsReturnOrder)
                        {
                            PayloadManager.ReturnOrderPayload.ReturnItems.Add(item);
                        }
                        else
                        {
                            item.IsSelectedItem = false;
                        }
                    }
                    if (PayloadManager.ReturnOrderPayload.ReturnItems.Count != r_itemsCollection.Count)
                    {
                        var alartMessage = new Helpers.AlertWindow { Message = "Already this Item was Holded", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alartMessage, CommonNavInfo.MessageToken);
                        return;
                    }


                    PayloadManager.ReturnOrderPayload.IsReturnOrder = IsReturnOrderEnable;
                    PayloadManager.ReturnOrderPayload.SalesOrderID = IsReturnOrderEnable ? SelectedOrderId : 0;

                    PayloadManager.ReturnOrderPayload.CurrentViewName = PayloadManager.ReturnOrderPayload.IsReturnOrder ? ViewModelMappings.View.OrderReturnsScreen.ToString() : ViewModelMappings.View.ItemReturnsScreen.ToString();



                    //Delete previously hold items if ReturnOrder is in progress
                    if (PayloadManager.ReturnOrderPayload.ROIsInProgress)
                    {
                        new CustomerReturnsManager().DeleteHoldReturnItems(PayloadManager.ReturnOrderPayload.ReturnOrderID);
                    }
                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ItemReturnsScreen, CurrentViewName = ViewModelMappings.View.ReturnsAndCredits, CloseCurrentView = true, ShowAsModal = false };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerOrdersTabViewModel.][SetReturnItems][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][End:SetReturnItems]");
            });
            ToggleSelectedOrderItem = new DelegateCommand(param =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][Start:ToggleSelectedOrderItem]");
                try
                {
                    if (param == null) return;
                    List<object> parameters = param as List<object>;
                    var r_item = parameters[0] as ReturnItem;

                    var ro_item = ReturnOrders.FirstOrDefault(x => x.OrderId == r_item.OrderID);
                    IsReturnItemsEnable = true;
                    IsReturnOrderEnable = false;
                    IsPrintEnable = false;
                    int selectedCount = 0;
                    ReturnOrders.ToList().ForEach(x => { x.IsSelected = false; });

                    foreach (var roitem in ReturnOrders)
                    {
                        foreach (var ritem in roitem.ReturnItems)
                        {
                            selectedCount += ritem.IsSelectedItem ? 1 : 0;
                        }
                    }

                    IsReturnItemsEnable = selectedCount == 0 ? false : true;
                    //if (ro_item != null)
                    //{

                    //    //if (ro_item.ReturnItems.All(x => x.IsSelectedItem && x.PreviouslyReturnedQty == 0))
                    //    //{
                    //    //    //ro_item.IsSelected = true;
                    //    //    ReturnOrders.Where(x => x.OrderId != r_item.OrderID).ToList().ForEach(x => { x.IsSelected = false; });

                    //    //}
                    //}
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerOrdersTabViewModel.][ToggleSelectedOrderItem][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][End:ToggleSelectedOrderItem]");
            });
            ToggleSelectedOrder = new DelegateCommand(param =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][Start:ToggleSelectedOrder]");
                try
                {
                    if (param == null) return;
                    List<object> parameters = param as List<object>;
                    var orderItems = ReturnOrders.Where(ri => ri.OrderId == Convert.ToInt32(parameters[0])).FirstOrDefault().ReturnItems;
                    foreach (var item in orderItems)
                    {
                        if (item.QtyAvailableToReturn != 0)
                            item.IsSelectedItem = Convert.ToBoolean(parameters[1]);
                    }
                    ReturnOrders.Where(x => x.OrderId != Convert.ToInt32(parameters[0])).ToList().ForEach(x => { x.IsSelected = false; });

                    if (ReturnOrders.Where(ri => ri.OrderId == Convert.ToInt32(parameters[0])).FirstOrDefault().ReturnItems.All(x => x.IsSelectedItem && x.PreviouslyReturnedQty == 0))
                    {
                        ReturnOrders.Where(x => x.OrderId != Convert.ToInt32(parameters[0])).ToList().ForEach(x => { x.ReturnItems.ToList().ForEach(i => { i.IsSelectedItem = false; }); });

                        IsReturnOrderEnable = true;
                        IsPrintEnable = true;
                        IsReturnItemsEnable = false;
                        SelectedOrderId = Convert.ToInt32(parameters[0]);
                    }
                    if (!Convert.ToBoolean(parameters[1]))
                        IsPrintEnable = IsReturnOrderEnable = false;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerOrdersTabViewModel.][ToggleSelectedOrder][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][End:ToggleSelectedOrder]");
            });

            PrintOrders = new DelegateCommand(param =>
            {
                //var moveToView1 = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ItemReturnsScreen, CurrentViewName = ViewModelMappings.View.ReturnsAndCredits, CloseCurrentView = true, ShowAsModal = false };
                //Messenger.Default.Send<Helpers.NavigateToView>(moveToView1, CommonNavInfo.MessageToken);

                //PrintAsync();
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][Start:PrintOrders]");
              //Sathish/Vignesh reverted the changes due to some other screen freeze issue
                    try
                    {
                        IsBusy = true;              
                        string OrderNumber = "0";
                        decimal DocumentId = 0;
                        try
                        {
                            OrderNumber = ReturnOrders.Where(s => s.IsSelected).FirstOrDefault().OrderId.ToString();
                            DocumentId = ReturnOrders.Where(s => s.IsSelected).Where(x => x.DocumentID > 0).FirstOrDefault().DocumentID;
                        }
                        catch (Exception) { }

                        var DocInfo = new ReportEngine.DocumentInformation();
                        var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                        repEngine.Notify += repEngine_Notify;

                        repEngine.Log = this.Logger;
                        ReportEngine.IReportSLE ReportData = null;
                        //var DocumentId = DocumentId;
                        var SourceDocumentNo = Convert.ToDecimal(OrderNumber);
                        //var DocumentId = ReturnOrders.Where(s => s.IsSelected).FirstOrDefault().DocumentID; ;//addloadpick.DocumentID;
                        //var SourceDocumentNo = ReturnOrders.Where(s => s.IsSelected).FirstOrDefault().OrderId;//Convert.ToInt32(addloadpick.ReplnId);//Convert.ToDecimal(AddLoadPick.ReplnID);

                        if (DocumentId <= 0)
                        {
                            ReportData = new Application.ReportViewModels.InvoiceReport(OrderNumber.ToString(), true);
                            Dictionary<string, object> Parameters;
                            Parameters = new Dictionary<string, object>();
                            Parameters.Add("PrintLabels", Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PrintLabels"]));
                            DocInfo = repEngine.Generate<Reporting.Invoice>(SourceDocumentNo, true, ReportEngine.ReportCategory.Customer, ReportEngine.ReportsTypes.InvoiceReport, ReportData, Parameters, ReportEngine.ContentTypes.PDF);
                        }
                        else
                        {
                            DocInfo.DocumentId = DocumentId;
                        }
                        var item = ReturnOrders.Where(s => s.OrderId == Convert.ToInt32(OrderNumber)).FirstOrDefault().DocumentID;

                        if (DocInfo.IsEndOfSequence)
                        {
                            Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, Please sync to continue" }, MessageToken);
                            return;
                        }

                        if (DocInfo.IsDocumentCreated)
                            ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.Order_Header", "OrderID", SourceDocumentNo, DocInfo.DocumentId);
                        DocumentId = DocInfo.DocumentId; //Replace Latest



                        var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Customer, DocumentId, null);
                        if (ReportBytes == null)
                        {
                            Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Report not generated, Please try again." }, MessageToken);
                            return;
                        }
                        SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();

                        Payload.ReportFile = ReportBytes;
                        Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                        Payload.PreviousView = ViewModelMappings.View.ReturnsAndCredits;
                        Payload.CurrentViewTitle = "Invoice Report";
                        Payload.PreviousViewTitle = "Returns And Credits";
                        Payload.MessageToken = CommonNavInfo.MessageToken;
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.ReturnsAndCredits, CloseCurrentView = true, Payload = Payload, ShowAsModal = false };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);                      
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerOrdersTabViewModel.][PrintOrders][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][End:PrintOrders]");
                });
                //var moveToView1 = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.ReturnsAndCredits, CloseCurrentView = true, ShowAsModal = false };
                //Messenger.Default.Send<Helpers.NavigateToView>(moveToView1, CommonNavInfo.MessageToken);
           
        }
        //Vignesh.S fixed for Issue 221
        //Use asyn Function for Disable the ReturnOrder button
        //public async void printAsynfun()
        //{
           
        //}
        void PrintAsync()
        {
            //await Task.Run(() =>
            //{
            IsBusy = true;
            string payload = "0";
            decimal DocumentId = 0;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][Start:PrintAsync]");
            try
            {
                payload = ReturnOrders.Where(s => s.IsSelected).FirstOrDefault().OrderId.ToString();
                DocumentId = ReturnOrders.Where(s => s.IsSelected).FirstOrDefault().DocumentID;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerOrdersTabViewModel.][PrintAsync][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][End:PrintAsync]");
            // new InvoiceReport(payload);
            ShowCustomerOrdersReport(payload, DocumentId);

            //});
        }

        public void ShowCustomerOrdersReport(string OrderNumber, decimal DocmentId)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][Start:ShowCustomerOrdersReport]");
            try
            {
                var DocInfo = new ReportEngine.DocumentInformation();
                var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                repEngine.Notify += repEngine_Notify;

                repEngine.Log = this.Logger;
                ReportEngine.IReportSLE ReportData = null;
                var DocumentId = DocmentId;
                var SourceDocumentNo = Convert.ToDecimal(OrderNumber);
                //var DocumentId = ReturnOrders.Where(s => s.IsSelected).FirstOrDefault().DocumentID; ;//addloadpick.DocumentID;
                //var SourceDocumentNo = ReturnOrders.Where(s => s.IsSelected).FirstOrDefault().OrderId;//Convert.ToInt32(addloadpick.ReplnId);//Convert.ToDecimal(AddLoadPick.ReplnID);

                if (DocumentId <= 0)
                {
                    ReportData = new Application.ReportViewModels.InvoiceReport(OrderNumber.ToString(), true);
                    Dictionary<string, object> Parameters;
                    Parameters = new Dictionary<string, object>();
                    Parameters.Add("PrintLabels", Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PrintLabels"]));
                    DocInfo = repEngine.Generate<Reporting.Invoice>(SourceDocumentNo, true, ReportEngine.ReportCategory.Customer, ReportEngine.ReportsTypes.InvoiceReport, ReportData, Parameters, ReportEngine.ContentTypes.PDF);
                }
                else
                {
                    DocInfo.DocumentId = DocumentId;
                }
                var item = ReturnOrders.Where(s => s.OrderId == Convert.ToInt32(OrderNumber)).FirstOrDefault().DocumentID;

                if (DocInfo.IsDocumentCreated)
                    ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.Order_Header", "OrderID", SourceDocumentNo, DocInfo.DocumentId);
                DocumentId = DocInfo.DocumentId; //Replace Latest


                var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Customer, DocumentId, null);
                SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();

                Payload.ReportFile = ReportBytes;
                Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                Payload.PreviousView = ViewModelMappings.View.ReturnsAndCredits;
                Payload.CurrentViewTitle = "Returns";
                Payload.PreviousViewTitle = "ReturnsAndCredits";
                Payload.MessageToken = this.MessageToken;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.ReturnsAndCredits, CloseCurrentView = true, Payload = Payload, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerOrdersTabViewModel.][ShowCustomerOrdersReport][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][End:ShowCustomerOrdersReport]");
        }
        void repEngine_Notify(string msg)
        {
            System.Windows.MessageBox.Show(msg);
        }
        async void LoadOrders()
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][Start:LoadOrders]");
                try
                {
                    this.MessageToken = CommonNavInfo.MessageToken;
                    ReturnItems = new TrulyObservableCollection<ReturnItem>(new CustomerReturnsManager().GetItemsForOrdersTab(this.IncludeAvailableZeroItems));

                    if (ReturnOrders != null)
                        ReturnOrders.Clear();

                    var orderIDCollection = ReturnItems.GroupBy(x => x.OrderID).Distinct();
                    orderIDCollection = orderIDCollection.OrderByDescending(x => x.Key);
                    foreach (var item in orderIDCollection)
                    {
                        ReturnOrder ro_item = new ReturnOrder();
                        var ro_items = ReturnItems.Where(x => x.OrderID == Convert.ToInt32(item.Key)).Clone();
                        //var orderDetailCollection = new CustomerReturnsManager().GetOrderDetails(item.Key);

                        foreach (var ro in ro_items)
                        {
                            ro.OrderDateF = ReturnItems.Where(x => x.OrderID == ro.OrderID && x.ItemId == ro.ItemId).FirstOrDefault().OrderDateF;
                            ro_item.ReturnItems.Add(ro as ReturnItem);
                        }
                        ro_item.OrderId = item.Key;
                        var dict = new CustomerReturnsManager().GetOrderFlags(item.Key);
                        if (dict != null)
                        {
                            ro_item.IsSettled = dict["IsSettled"];
                            ro_item.IsOrderReturned = dict["IsOrderReturned"];
                        }

                        ro_item.IsEnabled = (!ro_item.ReturnItems.Any(x => x.OrderQty == x.ReturnQty));
                        ro_item.IsEnabled = !ro_item.ReturnItems.Any(x => x.PreviouslyReturnedQty > 0);
                        ro_item.IsEnabled = new CustomerReturnsManager().IsOrderPartiallyReturn(ro_item.OrderId);
                        foreach (var ro in ro_items)
                        {
                            if (ro.PreviouslyReturnedQty > 0 && ro.QtyAvailableToReturn == 0)
                            {
                                ro.IsItemEnabled = false;
                            }
                        }

                        ro_item.Surcharge = Convert.ToDecimal(ro_item.ReturnItems[0].EnergySurchargeAmt); // Energy surcharge is taken at item level as it is common for all items which belongs to same order
                        ro_item.OrderDate = ro_item.ReturnItems[0].OrderDateF;
                        // Below line added for User Issue Id : 5. Everytime calc Extended rice and Order Total
                        ro_item.ReturnItems.ToList().ForEach(x => { x.ExtendedPrice = (x.QtyAvailableToReturn * Math.Round(x.UnitPrice, 2, MidpointRounding.AwayFromZero)); });
                        ro_item.OrderTotal = Convert.ToDecimal(ro_item.ReturnItems.Sum(x => x.ExtendedPrice).ToString("0.00")) + (decimal)ro_item.ReturnItems[0].EnergySurchargeAmt + ro_item.ReturnItems.Sum(x => x.TaxAmount);
                        //ro_item.OrderTotal = ro_item.ReturnItems.Sum(x => (x.AvailableQty * x.UnitPrice)) + (decimal)ro_item.ReturnItems[0].EnergySurchargeAmt + ro_item.ReturnItems.Sum(x => x.TaxAmount);

                        ro_item.ReturnItems.CollectionChanged += ReturnItems_CollectionChanged;
                        if (ro_item.IsOrderReturned)
                        {
                            ro_item.IsEnabled = false;
                            ro_item.ReturnItems.ToList().ForEach(x => { x.IsItemEnabled = false; });

                        }

                        ro_item.TotalItems = ro_item.ReturnItems.Count();
                        ReturnOrders.Add(ro_item);
                    }
                    // List is used as reference to return orders
                    returnOrderList = new TrulyObservableCollection<ReturnOrder>(ReturnOrders.ToList<ReturnOrder>()).Clone();
                    foreach (var returnOrder in returnOrderList)
                    {
                        foreach (var rItem in returnOrder.ReturnItems)
                        {
                            rItem.OrderDateF = ReturnItems.FirstOrDefault(x => x.ItemId == rItem.ItemId && x.OrderID == rItem.OrderID).OrderDateF;
                            rItem.IsSelectedItem = ReturnOrders.FirstOrDefault(x => x.OrderId == returnOrder.OrderId).ReturnItems.FirstOrDefault(x => x.ItemId == rItem.ItemId).IsSelectedItem;
                            rItem.IsItemEnabled = ReturnOrders.FirstOrDefault(x => x.OrderId == returnOrder.OrderId).ReturnItems.FirstOrDefault(x => x.ItemId == rItem.ItemId).IsItemEnabled;

                        }
                    }

                    // Modify elements in existing return order
                    if (PayloadManager.ReturnOrderPayload.ROIsInProgress && PayloadManager.ReturnOrderPayload.ReturnItems.Count != 0)
                    {
                        foreach (var item in PayloadManager.ReturnOrderPayload.ReturnItems)
                        {
                            var ro_item = ReturnOrders.FirstOrDefault(x => x.OrderId == item.OrderID);

                            var R_item_derived = ro_item.ReturnItems.Where(x => x.OrderID == ro_item.OrderId && x.ItemId == item.ItemId).FirstOrDefault();
                            R_item_derived.ReturnQty = item.ReturnQty;
                            R_item_derived.NonSellableQty = item.NonSellableQty;
                            R_item_derived.IsSelectedItem = true;
                            if (R_item_derived.ReturnQty == R_item_derived.OrderQty)
                                R_item_derived.IsItemEnabled = true;
                            //ReturnItems.ElementAt(ReturnItems.IndexOf(ro_item)).IsSelectedItem = true;
                        }
                        IsReturnItemsEnable = true;
                        // Parent View model check to make sure RO is in terminating state
                        ParentView.IsDirty = true;
                    }
                    //ReturnOrders[0].ReturnItems.CollectionChanged 




                    ReturnItems.ItemPropertyChanged += ReturnItems_ItemPropertyChanged;
                    ReturnOrders.ItemPropertyChanged += ReturnOrders_ItemPropertyChanged;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][LoadOrders][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                finally
                {
                    this.ParentView.IsBusy = false;
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][End:LoadOrders]");
            });

        }

        private void ReturnOrders_ItemPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        void ReturnItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {

        }


        void ReturnItems_ItemPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][Start:ReturnItems_ItemPropertyChanged]");
            try
            {
                // Enable/Disable UI Buttons for Print/ReturnOrder/ReturnItems
                if (e.PropertyName.Equals("IsSelectedItem"))
                {
                    var items = ReturnItems.GroupBy(l => l.OrderID)
                              .Select(lg =>
                                    new
                                    {
                                        ItemCount = lg.Count(),
                                        TotalSelected = lg.Count(w => w.IsSelectedItem == true),
                                        ReturnedQty = lg.Count(w => w.PreviouslyReturnedQty > 0)
                                    });
                    var totalItemIn = items.Count(i => i.TotalSelected > 0);
                    var totallySelected = items.Count(i => i.ItemCount == i.TotalSelected && i.ReturnedQty == 0);
                    var newOrderSelected = items.Count(i => i.ItemCount == i.TotalSelected && i.ReturnedQty == 0);
                    IsReturnOrderEnable = totalItemIn == totallySelected && totalItemIn == 1 && newOrderSelected == 1 ? true : false;
                    IsReturnItemsEnable = totalItemIn > 0 && !IsReturnOrderEnable ? true : false;
                    IsPrintEnable = totalItemIn == 1;

                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][ReturnItems_ItemPropertyChanged][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][End:ReturnItems_ItemPropertyChanged]");
        }

        internal void Search(string searchTerm)
        {
            if (searchTerm.Trim().Length > 2)
            {
                SearchItems(searchTerm);
            }
            if (searchTerm.Trim().Length == 0)
            {
                ResetSearch();
            }
        }
        CancellationTokenSource tokenForCancelTask = new CancellationTokenSource();
        public async void SearchItems(string searchTerm)
        {
            await Task.Run(() =>
           {

               System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
               {
                   Logger.Info("[SalesLogicExpress.Application.ViewModels][CommonInfo][AsyncEnd:SearchItems]");
                   try
                   {
                       if (searchTerm.Trim().Length > 2 || searchTerm.Trim().Length == 0)
                       {
                           ReturnOrders = new TrulyObservableCollection<ReturnOrder>(returnOrderList.Where(c => c.OrderId.ToString().ToLower().Contains(searchTerm.ToLower()) || c.ReturnOrderID.ToString().ToLower().Contains(searchTerm.ToLower())).ToList());
                           //ReturnItems = new TrulyObservableCollection<ReturnItem>(returnItemsList.Where(c => c.ItemDescription.ToLower().Contains(searchTerm.ToLower()) || c.ItemNumber.ToLower().Contains(searchTerm.ToLower())).ToList<ReturnItem>());
                           //foreach (var returnOrder in ReturnOrders)
                           //{
                           //    var refROItem = returnOrderList.FirstOrDefault(x => x.OrderId == returnOrder.OrderId);
                           //    returnOrder.ReturnItems = new ObservableCollection<ReturnItem>(refROItem.ReturnItems.Where(c => c.ItemDescription.ToLower().Contains(searchTerm.ToLower()) || c.ItemNumber.ToLower().Contains(searchTerm.ToLower()) || c.ReturnOrderID.ToString().ToLower().Contains(searchTerm.ToLower()) || c.OrderID.ToString().ToLower().Contains(searchTerm.ToLower())).ToList<ReturnItem>());
                           //}
                       }
                   }
                   catch (Exception ex)
                   {
                       Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][SearchItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                   }
                   Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][AsyncEnd:SearchItems]");
               }));
           }, tokenForCancelTask.Token);
        }
        internal void ResetSearch()
        {
            //ReturnItems = new TrulyObservableCollection<ReturnItem>(returnItemsList.ToList<ReturnItem>());
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][Start:ResetSearch]");
            try
            {
                ReturnOrders = returnOrderList;
                //foreach (var returnOrder in ReturnOrders)
                //{
                //    returnOrder.ReturnItems.Clear();
                //    returnOrder.ReturnItems = returnOrderList.FirstOrDefault(x => x.OrderId == returnOrder.OrderId).ReturnItems.Clone();
                //    for (int index = 0; index < returnOrderList.FirstOrDefault(x => x.OrderId == returnOrder.OrderId).ReturnItems.Count(); index++)
                //    {
                //        returnOrder.ReturnItems[index].OrderDateF = returnOrderList.FirstOrDefault(x => x.OrderId == returnOrder.OrderId).ReturnItems[index].OrderDateF;
                //    }

                //}
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][ResetSearch][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][End:ResetSearch]");
        }
        private void FilterCurrentOrders()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][Start:FilterCurrentOrders]");
            try
            {
                if (ReturnOrders.Count() > 0)
                {
                    foreach (var ro_item in ReturnOrders)
                    {
                        foreach (var r_Item in ro_item.ReturnItems)
                        {
                            if (r_Item.QtyAvailableToReturn == 0)
                                ro_item.ReturnItems.Remove(r_Item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][FilterCurrentOrders][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][End:FilterCurrentOrders]");
        }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
