﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using SalesLogicExpress.Domain;
using Managers = SalesLogicExpress.Application.Managers;
using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.Helpers;
using log4net;
using SalesLogicExpress.Application.ViewModelPayload;
using System.Diagnostics;
using SalesLogicExpress.Application.Managers;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Windows.Threading;
using Models = SalesLogicExpress.Domain;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;

namespace SalesLogicExpress.Application.ViewModels
{
    public class ProspectInformationViewModel : ViewModelBase, IDataErrorInfo
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.ProspectInformationViewModel");
        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;
        private static void NotifyStaticPropertyChanged(string propertyName)
        {
            if (StaticPropertyChanged != null)
                StaticPropertyChanged(null, new PropertyChangedEventArgs(propertyName));
        }

        private bool isVisibleRelease = false;
        public bool IsVisibleRelease
        {
            get { return isVisibleRelease; }
            set { isVisibleRelease = value; OnPropertyChanged("IsVisibleRelease"); }
        }

        private bool isVisibleVoid = false;
        public bool IsVisibleVoid
        {
            get { return isVisibleVoid; }
            set { isVisibleVoid = value; OnPropertyChanged("IsVisibleVoid"); }
        }
        ProspectInformation _prospectInformation = null;
        public ProspectInformation prospectInformation
        {
            get
            {
                return _prospectInformation;
            }
            set
            {
                _prospectInformation = value;
                OnPropertyChanged("prospectInformation");
            }
        }
        List<string> _StateCodeList;
        public List<string> StateCodeList
        {
            get
            {
                return _StateCodeList;
            }
            set
            {
                _StateCodeList = value;
                OnPropertyChanged("StateCodeList");
            }
        }
        List<string> _SegmentTypeList;
        public List<string> SegmentTypeList
        {
            get
            {
                return _SegmentTypeList;
            }
            set
            {
                _SegmentTypeList = value;
                OnPropertyChanged("SegmentTypeList");
            }
        }
        private static SalesLogicExpress.Domain.Prospect prospect;
        public static Prospect Prospect
        {
            get
            {
                return prospect;
            }
            set
            {
                prospect = value;
                NotifyStaticPropertyChanged("Prospect");
            }
        }
        public Guid MessageToken { get; set; }

        public DelegateCommand SaveProspectInformation { get; set; }

        Managers.ProspectManager prospectManager = new Managers.ProspectManager();
        public ProspectInformationViewModel()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectInformationViewModel][Start:Constructor]");
            try
            {
                InitializeCommands();
                LoadDetails();
            }
            catch (Exception ex)
            {
                Logger.Error("ProspectInfo ProspectInfo() error: " + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectInformationViewModel][End:Constructor]");
        }
        async void LoadDetails()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectInformationViewModel][Start:LoadDetails]");
            await Task.Run(() =>
            {

                StateCodeList = prospectManager.GetStateCodes();
                SegmentTypeList = GetSegmentType();
                Prospect = PayloadManager.ProspectPayload.Prospect;
                if (Prospect != null)
                    prospectInformation = prospectManager.GetProspectInformation(Convert.ToInt32(Prospect.ProspectID));
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectInformationViewModel][End:LoadDetails]");
            });
        }
        public void InitializeCommands()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectInformationViewModel][Start:InitializeCommands]");
            SaveProspectInformation = new DelegateCommand((param) =>
            {
                if (prospectManager.SaveProspectInformation(prospectInformation) == 0)
                {
                    Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Constants.ProspectInformation.UnableToSaveAlert }, MessageToken);
                }
                else
                {
                    prospectInformation = prospectManager.GetProspectInformation(Convert.ToInt32(Prospect.ProspectID));
                    //UpdateTopNavigationWithLatestData();
                    Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Constants.ProspectInformation.InfoSaveAlert }, MessageToken);

                    //Clicking on Save not refresh the information provided - Fixed by Dinesh
                    PayloadManager.ProspectPayload.Prospect.Address = prospectInformation.Address1.Trim() + " " + prospectInformation.Address2.Trim() + " " + prospectInformation.Address3.Trim() + " " + prospectInformation.Address4.Trim();
                    PayloadManager.ProspectPayload.Prospect.City = prospectInformation.CustInfoCity;
                    PayloadManager.ProspectPayload.Prospect.State = prospectInformation.CustInfoState;
                    PayloadManager.ProspectPayload.Prospect.Zip = prospectInformation.CustInfoZip;
                    PayloadManager.ProspectPayload.Prospect.Name = prospectInformation.BusinessName;
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectInformationViewModel][End:InitializeCommands]");
            });
        }

        public void UpdateTopNavigationWithLatestData()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectInformationViewModel][Start:UpdateTopNavigationWithLatestData]");
            try
            {
                CommonNavInfo.Customer.Address = prospectInformation.Address1 + " " + prospectInformation.Address2 + " " + prospectInformation.Address3 + " " + prospectInformation.Address4;
                CommonNavInfo.Customer.Zip = prospectInformation.CustInfoZip;
                CommonNavInfo.Customer.State = prospectInformation.CustInfoState;
                CommonNavInfo.Customer.City = prospectInformation.CustInfoCity;
                PayloadManager.OrderPayload.Customer = CommonNavInfo.Customer;
                CommonNavInfo.SetSelectedCustomer(PayloadManager.OrderPayload.Customer);
            }
            catch (Exception ex)
            {
                Logger.Info("Error in UpdateTopNavigationWithLatestData Error = " + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectInformationViewModel][End:UpdateTopNavigationWithLatestData]");
        }

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get { throw new NotImplementedException(); }
        }

        public List<string> GetSegmentType()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectInformationViewModel][Start:GetSegmentType]");
            List<string> SegmentTypeList = new List<string>();
            try
            {
                SegmentTypeList.Add("1");
                SegmentTypeList.Add("2");
                SegmentTypeList.Add("3");
                SegmentTypeList.Add("4");
            }
            catch (Exception ex)
            {
                Logger.Info("Error in UpdateTopNavigationWithLatestData Error = " + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ProspectInformationViewModel][End:GetSegmentType]");
            return SegmentTypeList;
        }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                if (prospectManager != null)
                    prospectManager.Dispose();
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
