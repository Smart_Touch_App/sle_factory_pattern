﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using GalaSoft.MvvmLight.Messaging;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using System.Threading;
using System.Data;
using Managers = SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System.ComponentModel;
using log4net;
using System.Windows.Threading;
using System.Globalization;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;

namespace SalesLogicExpress.Application.ViewModels
{
    public class PreOrder : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.PreOrder");
        //CommonNavInfo navInfo = new CommonNavInfo();//Unsed code Vignesh.S

        #region Variables
        Managers.TemplateManager templateManager;
        Managers.ItemManager itemManager;
        public event EventHandler<ModelChangeArgs> ModelChanged;
        protected virtual void OnModelChanged(ModelChangeArgs e)
        {
            EventHandler<ModelChangeArgs> handler = ModelChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public class ModelChangeArgs : EventArgs
        {
            public ChangeType Change;
            public DateTime StateChangedTime { get; set; }
        }
        public enum ChangeType
        {
            Loaded,
            ResetCanvas,
            ItemAdded
        }
        CancellationTokenSource tokenForCancelTask = new CancellationTokenSource();
        private bool isUnplannedCreatedForFuture = false;
        public bool IsUnplannedCreatedForFuture
        {
            get { return isUnplannedCreatedForFuture; }
            set { isUnplannedCreatedForFuture = value; OnPropertyChanged("IsUnplannedCreatedForFuture"); }
        }
        #endregion

        #region Properties
        private ObservableCollection<Models.Item> _SearchItems = new ObservableCollection<Models.Item>();
        public ObservableCollection<Models.Item> SearchItemList
        {
            get
            {
                return this._SearchItems;
            }
            set
            {
                this._SearchItems.Clear();
                this._SearchItems = value;
                OnPropertyChanged("SearchItemList");
                OnPropertyChanged("SearchResultText");
            }
        }

        private bool isAcceptPreOrderEnable = false;
        public bool IsAcceptPreOrderEnable
        {
            get
            {
                return this.isAcceptPreOrderEnable;
            }
            set
            {
                this.isAcceptPreOrderEnable = value;
                OnPropertyChanged("IsAcceptPreOrderEnable");
            }
        }
        private List<int> _previousSequenceNumbers;
        public List<int> PreviousSequenceNumbers
        {
            get { return _previousSequenceNumbers; }
            set { _previousSequenceNumbers = value; }
        }
        private string _StopDateToShow;
        public string StopDateToShow
        {
            get
            {
                return _StopDateToShow;
            }
            set
            {
                _StopDateToShow = value;
            }
        }
        bool CheckIsEditEnded = true;

        private int _SearchItemsCount;
        public int SearchItemsCount
        {
            get
            {

                return _SearchItemsCount;
            }
            set
            {
                _SearchItemsCount = value;
                OnPropertyChanged("SearchItemsCount");
                OnPropertyChanged("SearchResultText");
            }
        }
        public Guid MessageToken { get; set; }
        private bool _IsBusy;
        public bool IsBusy
        {
            get
            {
                return _IsBusy;
            }
            set
            {
                _IsBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        //private Models.Customer _Customer;
        public Models.Customer Customer
        {
            get;
            set;
        }

        private Models.Customer orderPayCustomer;
        public Models.Customer OrderPayCustomer
        {
            get { return orderPayCustomer; }
            set { orderPayCustomer = value; OnPropertyChanged("OrderPayCustomer"); }
        }

        private DateTime _StopDate;
        public DateTime StopDate
        {
            get { return _StopDate; }
            set { _StopDate = value; }
        }

        private string _SearchResultText;
        public string SearchResultText
        {
            get
            {
                if (SearchItemsCount == 0)
                {
                    return "No items found";
                }
                else
                {
                    SearchResultTextVisibility = true;
                    OnPropertyChanged("SearchResultTextVisibility");
                    return "Items found " + SearchItemsCount;
                }
            }
            set
            {
                if (value != _SearchResultText)
                {
                    _SearchResultText = value;
                    OnPropertyChanged("SearchResultText");
                }
            }
        }

        bool _SearchResultTextVisibility = false;
        public bool SearchResultTextVisibility
        {
            get
            {
                return _SearchResultTextVisibility;
            }
            set
            {
                _SearchResultTextVisibility = value;
            }
        }
        private string _WeekDay;
        public string WeekDay
        {
            get { return _WeekDay; }
            set { _WeekDay = value; }
        }
        private DataTable _OrderHistory;
        public DataTable OrderHistory
        {
            get { return _OrderHistory; }
            set { _OrderHistory = value; OnPropertyChanged("OrderHistory"); }
        }
        private DataTable _OrderHistoryHeaders;
        public DataTable OrderHistoryHeaders
        {
            get { return _OrderHistoryHeaders; }
            set { _OrderHistoryHeaders = value; OnPropertyChanged("OrderHistoryHeaders"); }
        }
        private ObservableCollection<Models.TemplateItem> _PreOrderItems = new ObservableCollection<TemplateItem>();
        public ObservableCollection<Models.TemplateItem> PreOrderItems
        {
            get
            {
                return this._PreOrderItems;
            }
            set
            {
                _PreOrderItems = value;
                OnPropertyChanged("PreOrderItems");
            }
        }

        TemplateItem items = new TemplateItem();

        public TemplateItem Items
        {
            get { return items; }
            set { items = value; OnPropertyChanged("Items"); }
        }

        //private Dictionary<string, string> OrderQuantityMap = new Dictionary<string, string>();//Unused Code Vignesh.S
        public int OldSequenceNo { get; set; }

        bool isReadOnly = true;
        public bool IsReadOnly
        {
            get
            {
                return isReadOnly;
            }
            set
            {
                isReadOnly = value;
                OnPropertyChanged("IsReadOnly");
            }
        }

        private bool isOrderSaved;
        public bool IsOrderSaved
        {
            get { return isOrderSaved; }
            set { isOrderSaved = value; OnPropertyChanged("IsOrderSaved"); }
        }

        private bool isItemsDropdownEnabled = true;
        public bool IsItemsDropdownEnabled
        {
            get { return isItemsDropdownEnabled; }
            set { isItemsDropdownEnabled = value; OnPropertyChanged("IsItemsDropdownEnabled"); }
        }

        private bool isSearchAddEnabled = true;
        public bool IsSearchAddEnabled
        {
            get { return isSearchAddEnabled; }
            set { isSearchAddEnabled = value; OnPropertyChanged("IsSearchAddEnabled"); }
        }



        #endregion

        #region Commands
        public DelegateCommand AcceptPreOrder { get; set; }
        public DelegateCommand LoadPreOrderWithUsualQuantity { get; set; }
        public DelegateCommand ResetBackPreOrderQuantity { get; set; }
        public DelegateCommand DeleteItemFromPreOrder { get; set; }
        public DelegateCommand BeginGridCellEdit { get; set; }
        public DelegateCommand SearchItemOnType { get; set; }
        public DelegateCommand ClearSearchText { get; set; }
        public DelegateCommand SearchItem { get; set; }
        public DelegateCommand SearchResultTextChange { get; set; }
        public DelegateCommand AddItemToTemplate { get; set; }
        public DelegateCommand GridCellEditEnded { get; set; }
        public DelegateCommand UpdateAvailableQty { get; set; }
        public DelegateCommand DeleteItemFromTemplate { get; set; }
        public DelegateCommand LoadTemplateWithUsualQuantity { get; set; }
        public DelegateCommand ResetBackTemplateQuantity { get; set; }
        public DelegateCommand PreOrderQtyChanged { get; set; }
        #endregion

        #region Constructor

        public PreOrder(bool flag)
        {
            IsUnplannedCreatedForFuture = flag;
            PreOrderStops data = PayloadManager.PreOrderPayload.PreOrderStops;
            Customer = PayloadManager.PreOrderPayload.Customer;
            StopDate = DateTime.Parse(data.Stopdate, CultureInfo.CreateSpecificCulture("en-US"));
            //IsReadOnly = PayloadManager.PreOrderPayload.IsReadOnly;
            StopDateToShow = data.Stopdate;
            WeekDay = data.Weekday;
            Initialize();
            IsBusy = true;
            IsDirty = false;
            IsOrderSaved = true;
            GetPreOrderItems(StopDate);

            if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.CustomerHome_Activity.ToString())
            {
                SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Activity;
            }
            else if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.CustomerHome_DashBoard.ToString())
            {
                SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Dashboard;
            }
        }

        private void Items_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!IsOrderSaved)
            {
                IsDirty = true;
            }
            if (Customer.StopType.ToLower() == "unplanned")
            {
                IsDirty = false;
            }
        }

        void PreOrderItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            IsDirty = true;
            if (Customer.StopType.ToLower() == "unplanned")
            {
                IsDirty = false;
            }
        }

        #endregion

        #region Methods
        Managers.PreOrderManager preOrderM = new Managers.PreOrderManager();
        private void Initialize()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][Start:Initialize]");
            templateManager = new Managers.TemplateManager();
            itemManager = new Managers.ItemManager();
            if (this._SearchItems != null)
                _SearchItemsCount = this._SearchItems.Count;
            InitializeCommands();
            PreOrderItems.CollectionChanged += PreOrderItems_CollectionChanged;
            Items.PropertyChanged += Items_PropertyChanged;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][End:Initialize]");
        }
        bool LastKnownDirtyState = false;
        protected override void OnModelStateDirty()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][Start:OnModelStateDirty]");
            var d = IsDirty;//unused code Vignesh.S
            if (LastKnownDirtyState != IsDirty && IsDirty)
            {
                PayloadManager.ApplicationPayload.StartActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.PreOrder);
            }
            if (LastKnownDirtyState && !IsDirty)
            {
                PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.PreOrder);
            }
            LastKnownDirtyState = IsDirty;
            base.OnModelStateDirty();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][End:OnModelStateDirty]");
        }
        void LogActivity(List<TemplateItem> preorderitems, Customer customer, DateTime stopDate, string stopID)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][Start:LogActivity]");
            try
            {
                PayloadManager.PreOrderPayload.StopDate = stopDate;
                //PayloadManager.PreOrderPayload.PreOrderCreatedDate = DateTime.Now;
                PayloadManager.PreOrderPayload.Customer = customer;
                PayloadManager.PreOrderPayload.PreOrderItems = PreOrderItems.ToList<TemplateItem>();
                string activityType = string.Empty;
                PayloadManager.PreOrderPayload.PreOrderCreatedDate = preOrderM.CheckPreOrderForSameDate(Customer.CustomerNo, stopDate);
                if (PayloadManager.PreOrderPayload.PreOrderCreatedDate != default(DateTime))
                {
                    if (PayloadManager.PreOrderPayload.PreOrderCreatedDate.Date < PayloadManager.PreOrderPayload.Customer.StopDate.Value.Date)
                    {
                        activityType = ActivityKey.PreOrderChange.ToString();
                    }
                    else if (PayloadManager.PreOrderPayload.PreOrderCreatedDate.Date == PayloadManager.PreOrderPayload.Customer.StopDate.Value.Date)
                    {
                        activityType = ActivityKey.PreOrder.ToString();

                    }
                }
                else
                {
                    activityType = ActivityKey.PreOrder.ToString();
                    PayloadManager.PreOrderPayload.PreOrderCreatedDate = DateTime.Now;
                }

                Activity ac = new Activity
                {
                    CustomerID = PayloadManager.PreOrderPayload.Customer.CustomerNo,
                    RouteID = PayloadManager.ApplicationPayload.Route,
                    ActivityType = activityType,//PayloadManager.PreOrderPayload.IsFromActivityTab ? ActivityKey.PreOrderChange.ToString() : ActivityKey.PreOrder.ToString(),
                    ActivityStart = DateTime.Now,
                    ActivityEnd = DateTime.Now,
                    StopInstanceID = stopID,
                    IsTxActivity = true,
                    ActivityDetailClass = PayloadManager.PreOrderPayload.ToString(),
                    ActivityDetails = PayloadManager.PreOrderPayload.SerializeToJson()
                };
                ResourceManager.Transaction.LogActivity(ac);
                //Activity a = ResourceManager.Transaction.LogActivity(ac); //Unused Code Vignesh.S
            }

            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][PreOrder][preorderitems][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][End:preorderitems]");
        }
        void InitializeCommands()
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][Start:InitializeCommands]");
            Random qtyOnHand = new Random();
            AcceptPreOrder = new DelegateCommand((pram) =>
            {
                try
                {
                    Managers.PreOrderManager preOrderManager = new Managers.PreOrderManager();
                    ObservableCollection<Customer> customersForDate = new SalesLogicExpress.Application.Managers.ServiceRouteManager().GetCustomersForRoute(CommonNavInfo.RouteUser, StopDate);
                    Customer customer = customersForDate.FirstOrDefault(c => c.CustomerNo == Customer.CustomerNo);
                    preOrderManager.SavePreOrder(PreOrderItems, Customer.CustomerNo, StopDate);
                    // Log Activity for pre-order
                    LogActivity(PreOrderItems.ToList<TemplateItem>(), Customer, StopDate, customer.StopID);

                    #region Update Activity Count
                    string tempStopDate = string.Empty;
                    string stopID = string.Empty;
                    if (PayloadManager.PreOrderPayload.StopDate.Date > DateTime.Now.Date)
                    {
                        tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                        stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.PreOrderPayload.Customer.CustomerNo, tempStopDate);
                    }
                    else
                    {
                        tempStopDate = PayloadManager.PreOrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                        stopID = PayloadManager.PreOrderPayload.Customer.StopID;
                    }
                    new Managers.CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                    new Managers.CustomerManager().UpdateActivityCount(stopID, false, true);
                    PayloadManager.PreOrderPayload.Customer.SaleStatus = "";
                    PayloadManager.PreOrderPayload.Customer.CompletedActivity += 1;
                    new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.PreOrderPayload.Customer.CustomerNo);
                    #endregion
                    IsDirty = false;

                    if (!IsUnplannedCreatedForFuture)
                    {
                        #region Update tem squncing no.
                        DateTime todayDate = Convert.ToDateTime(tempStopDate);
                        ViewModels.CustomerDashboard custDash = new CustomerDashboard();
                        //Checks for customer alteration if user invoked past activity.
                        if (PayloadManager.PreOrderPayload.IsCustomerLoadedForPastActivity)
                        {
                            PayloadManager.PreOrderPayload.IsCustomerLoadedForPastActivity = false;
                            PayloadManager.OrderPayload.Customer = new CustomerDashboard().GetNewCustomer(PayloadManager.PreOrderPayload.StopDateBeforeInvokingActivity.Value.Date, PayloadManager.PreOrderPayload.Customer);
                        }
                        else
                            PayloadManager.OrderPayload.Customer = custDash.GetNewCustomer(todayDate, PayloadManager.PreOrderPayload.Customer);

                        #endregion

                    }
                    PayloadManager.PreOrderPayload.Reset();
                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.PreOrder, CloseCurrentView = true, Refresh = true, ShowAsModal = false };
                    if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.CustomerHome_Activity.ToString())
                    {
                        SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Activity;
                        ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = "";
                    }
                    moveToView.SelectTab = SelectedTab;
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                }

                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][PreOrder][AcceptPreOrder DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }

            });
            LoadPreOrderWithUsualQuantity = new DelegateCommand((param) =>
            {
                foreach (Models.TemplateItem item in this._PreOrderItems)
                {
                    item.OrderQty = item.UsualQty;
                }
                IsDirty = true;
            });
            ResetBackPreOrderQuantity = new DelegateCommand((param) =>
            {
                foreach (Models.TemplateItem item in this._PreOrderItems)
                {
                    item.OrderQty = 0;
                }
                IsDirty = true;
            });
            DeleteItemFromPreOrder = new DelegateCommand((Items) =>
            {
                try
                {
                    ObservableCollection<object> selectedItems = Items as ObservableCollection<object>;
                    if (selectedItems == null || selectedItems.Count == 0)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemForDeleteAlert, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.DeleteItemConfirmation, MessageIcon = "Alert", Confirmed = false };
                    Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                    if (!confirmMessage.Confirmed) return;
                    int selectionCount = selectedItems.Count;
                    for (int index = 0; index < selectionCount; index++)
                    {
                        PreOrderItems.Remove(selectedItems[index] as Models.TemplateItem);
                        index--;
                        selectionCount--;
                    }
                    OnPropertyChanged("TemplateItemsCount");
                    IsDirty = true;
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][PreOrder][DeleteItemFromPreOrder DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });

            BeginGridCellEdit = new DelegateCommand((param) =>
            {

                ObservableCollection<Models.TemplateItem> TempItems = new ObservableCollection<TemplateItem>();
                TempItems = (ObservableCollection<Models.TemplateItem>)param;
                PreviousSequenceNumbers = new List<int>();


                for (int index = 0; index < TempItems.Count; index++)
                {
                    PreviousSequenceNumbers.Add(TempItems[index].SeqNo);
                }
                CheckIsEditEnded = true;

            });
            ClearSearchText = new DelegateCommand((param) =>
            {
                SearchText = "";
                SearchItems(SearchText.ToString(), true);
            });
            SearchItemOnType = new DelegateCommand((searchTerm) =>
            {

                bool flag = searchTerm.ToString().Length == 0 ? true : false;
                SearchItems(searchTerm.ToString(), flag);
            });
            SearchItem = new DelegateCommand((searchTerm) =>
            {
                SearchItems(searchTerm.ToString(), true);
            });
            SearchResultTextChange = new DelegateCommand((param) =>
            {
                SearchItemsCount = Convert.ToInt32(param);
                OnPropertyChanged("SearchItemsCount");
                OnPropertyChanged("SearchResultText");
            });
            AddItemToTemplate = new DelegateCommand((Items) =>
            {
                try
                {
                    //Managers.ItemManager itemManager = new Managers.ItemManager();//Unused Code Vignesh.S
                    ObservableCollection<object> selectedItems = Items as ObservableCollection<object>;
                    if (selectedItems == null || selectedItems.Count == 0)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemToAddAlert, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    int selectionCount = selectedItems.Count;
                    int maxSequenceNo = PreOrderItems.Count > 0 ? PreOrderItems.Max(item => item.SeqNo) : 0;
                    int ItemsNotAddedCount = 0;
                    int TotalItemsToAdd = selectedItems.Count;
                    int seqNo = 1;
                    //Dictionary<string, List<string>> templateUms = ResourceManager.GetItemUMList;
                    Dictionary<string, List<string>> templateUms = Managers.UoMManager.GetItemUMList;
                    for (int index = 0; index < selectionCount; index++)
                    {
                        if (PreOrderItems.Any(i => i.ItemNumber == (selectedItems[index] as Models.Item).ItemNumber))
                        {
                            ItemsNotAddedCount = ItemsNotAddedCount + 1;
                            continue;
                        }
                        Models.TemplateItem addedItem = new TemplateItem((selectedItems[index] as Models.Item));
                        addedItem.UM = Managers.UoMManager.GetDefaultSaleableUOMForItem(addedItem.ItemNumber);
                        addedItem.EffectiveFrom = DateTime.Now.ToString("MM/dd/yyyy");
                        addedItem.EffectiveThru = DateTime.Now.ToString("MM/dd/yyyy");
                        SearchItemList.Remove(selectedItems[index] as Models.Item);
                        addedItem.SeqNo = maxSequenceNo + (seqNo * 5);
                        addedItem.ActualQtyOnHand = qtyOnHand.Next(5, 50);
                        //string itemNumber = addedItem.ItemNumber;//Unused Code Vignesh.S
                        // OrderItem order = new OrderItem(new Item());
                        //addedItem.AppliedUMS = itemManager.GetAppliedUMs(addedItem.ItemNumber).Count != 0 ? itemManager.GetAppliedUMs(addedItem.ItemNumber) : new List<string>() { addedItem.UM };
                        addedItem.AppliedUMS = templateUms.ContainsKey(addedItem.ItemNumber.Trim()) ? templateUms[addedItem.ItemNumber.Trim()] : new List<string>() { addedItem.UM };
                        PreOrderItems.Insert(0, addedItem);
                        index--;
                        selectionCount--;
                        seqNo++;
                        SearchItemsCount = SearchItemList.Count;
                        OnPropertyChanged("SearchItemsCount");
                        OnPropertyChanged("SearchResultText");
                    }
                    ValidateSearch(TotalItemsToAdd, ItemsNotAddedCount);
                    OnPropertyChanged("TemplateItemsCount");
                    IsDirty = true;
                    ModelChangeArgs args = new ModelChangeArgs();
                    args.StateChangedTime = DateTime.Now;
                    args.Change = ChangeType.ItemAdded;
                    OnModelChanged(args);
                    IsAcceptPreOrderEnable = PreOrderItems.Any(item => item.PreOrderQty > 0) ? true : false;
                }

                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][PreOrder][AddItemToTemplate DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            GridCellEditEnded = new DelegateCommand((param) =>
            {

                OnPropertyChanged("TemplateItems");

            });

            UpdateAvailableQty = new DelegateCommand((param) =>
            {
                TemplateItem templateItem = param as TemplateItem;
                templateItem.QtyOnHand = (templateItem.ActualQtyOnHand - templateItem.OrderQty) < 0 ? 0 : (templateItem.ActualQtyOnHand - templateItem.OrderQty);
            });

            DeleteItemFromTemplate = new DelegateCommand((Items) =>
            {
                try
                {
                    ObservableCollection<object> selectedItems = Items as ObservableCollection<object>;
                    if (selectedItems == null || selectedItems.Count == 0)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemForDeleteAlert, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.DeleteItemConfirmation, MessageIcon = "Alert", Confirmed = false };
                    Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                    if (!confirmMessage.Confirmed) return;
                    int selectionCount = selectedItems.Count;
                    for (int index = 0; index < selectionCount; index++)
                    {
                        PreOrderItems.Remove(selectedItems[index] as Models.TemplateItem);
                        index--;
                        selectionCount--;
                    }
                    OnPropertyChanged("TemplateItemsCount");
                    IsDirty = true;
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][PreOrder][DeleteItemFromTemplate DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });

            LoadTemplateWithUsualQuantity = new DelegateCommand((param) =>
            {
                foreach (Models.TemplateItem item in this._PreOrderItems)
                {
                    item.PreOrderQty = item.UsualQty;
                }

            });
            ResetBackTemplateQuantity = new DelegateCommand((param) =>
            {
                foreach (Models.TemplateItem item in this._PreOrderItems)
                {
                    item.PreOrderQty = 0;
                }
                IsDirty = false;
            });
            PreOrderQtyChanged = new DelegateCommand((param) =>
            {
                if (Customer.StopType.ToLower() != "unplanned" && !(DateTime.Compare(Customer.StopDate.Value.Date, StopDate.Date) == 0))
                {
                    IsDirty = IsAcceptPreOrderEnable = PreOrderItems.Any(item => item.PreOrderQty > 0) ? true : false;
                }
                if (PayloadManager.PreOrderPayload.IsPreOrderCreatedFromUnplanned || PayloadManager.PreOrderPayload.IsPreOrderExistForSameDate
                    || PayloadManager.PreOrderPayload.IsPreOrderForAnotherDate)
                {
                    IsDirty = IsAcceptPreOrderEnable = PreOrderItems.Any(item => item.PreOrderQty > 0) ? true : false;
                }

            });
        }
        async void GetPreOrderItems(DateTime date)
        {
            await Task.Run(() =>
            {//**********************************************************************************************************

                try
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][Start:GetPreOrderItems]");
                    //Managers.OrderManager orderManager = new Managers.OrderManager();
                    
                    // Get Templates
                    //**********************************************************************************************************
                    PreOrderItems = templateManager.GetTemplateItemsForCustomer(Customer.CustomerNo);
                    ObservableCollection<TemplateItem> PreOrder = new ObservableCollection<TemplateItem>();
                    Managers.PreOrderManager preOrderManager = new Managers.PreOrderManager();
                    PreOrder = preOrderManager.GetPreOrderItems(Customer.CustomerNo, date);
                    preOrderManager = null;
                    ObservableCollection<TemplateItem> items = this._PreOrderItems; ;
                    ObservableCollection<TemplateItem> updateItems = new ObservableCollection<TemplateItem>();
                    ObservableCollection<TemplateItem> inserItems = new ObservableCollection<TemplateItem>();
                    foreach (Models.TemplateItem item in items)
                    {
                        if (PreOrder.Any(x => x.ItemNumber == item.ItemNumber))
                        {
                            //Models.TemplateItem i = PreOrder.FirstOrDefault(x => x.ItemNumber == item.ItemNumber);
                            //updateItems.Add(i);
                            Models.TemplateItem i = PreOrder.FirstOrDefault(x => x.ItemNumber == item.ItemNumber);
                            //this._PreOrderItems.ElementAt(this._PreOrderItems.IndexOf(i)).PreOrderQty = i.PreOrderQty;
                            item.PreOrderQty = PreOrder.FirstOrDefault(x => x.ItemNumber == item.ItemNumber).PreOrderQty;
                            item.UM = PreOrder.FirstOrDefault(x => x.ItemNumber == item.ItemNumber).UM;
                            PreOrder.Remove(i);
                        }
                    }


                    if (((DateTime.Compare(Customer.StopDate.Value.Date, StopDate.Date) < 0) && (Customer.StopType.ToLower() == "unplanned"))
                        || (DateTime.Compare(Customer.StopDate.Value.Date, StopDate.Date) == 0) || PayloadManager.PreOrderPayload.IsPreOrderExistForSameDate)
                    {
                        IsDirty = false;

                        foreach (Models.TemplateItem item in PreOrderItems)
                        {
                            item.IsNumericPadEnabled = false;
                        }
                        foreach (Models.TemplateItem item in PreOrder)
                        {
                            item.IsNumericPadEnabled = false;
                        }
                        IsReadOnly = false;

                    }

                    if (IsUnplannedCreatedForFuture || PayloadManager.PreOrderPayload.IsPreOrderCreatedFromUnplanned
                        || PayloadManager.PreOrderPayload.IsPreOrderExistForSameDate || PayloadManager.PreOrderPayload.IsPreOrderForAnotherDate)
                    {
                        foreach (Models.TemplateItem item in PreOrderItems)
                        {
                            item.IsNumericPadEnabled = true;
                        }
                        foreach (Models.TemplateItem item in PreOrder)
                        {
                            item.IsNumericPadEnabled = true;
                        }
                        IsReadOnly = true;
                    }
                    if (DateTime.Compare(Customer.StopDate.Value.Date, StopDate.Date) > 0)
                    {
                        foreach (Models.TemplateItem item in PreOrderItems)
                        {
                            item.IsNumericPadEnabled = false;
                        }
                        foreach (Models.TemplateItem item in PreOrder)
                        {
                            item.IsNumericPadEnabled = false;
                        }
                        IsReadOnly = false;
                    }
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                       new Action(() =>
                       {
                           SearchItems("", true);

                           foreach (Models.TemplateItem item in PreOrder)
                           {
                               items.Add(item);
                           }
                       }
                   ));


                    //**********************************************************************************************************

                    IsBusy = false;
                    OnPropertyChanged("TemplateItemsCount");
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][PreOrder][GetPreOrderItems][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][End:GetPreOrderItems]");

            });

        }
        private void ValidateSearch(int TotalItemsToAdd, int ItemsNotAddedCount)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][Start:ValidateSearch]");
            if (TotalItemsToAdd == ItemsNotAddedCount)
            {
                //ALL/ONE ITEMS PRESNTS in item template
                Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Helpers.Constants.Common.AllOrOneItemsExists }, MessageToken);
            }
            else if (TotalItemsToAdd > ItemsNotAddedCount && ItemsNotAddedCount > 0)
            {
                //PARTIAL ITEM PRESENT in item template
                Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Helpers.Constants.Common.PartialItemsExists }, MessageToken);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][End:ValidateSearch]");
        }
        public async Task SearchItems(string searchTerm, bool skipMinLengthCheck)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][Start:SearchItems][searchTerm=" + searchTerm + ",skipMinLengthCheck=" + skipMinLengthCheck + "]");
            try
            {
                await Task.Delay(100, tokenForCancelTask.Token);
                if (!skipMinLengthCheck)
                {
                    if (searchTerm != null && searchTerm.ToString().Length < 3)
                    {
                        return;
                    }
                    if (searchTerm != null && searchTerm.ToString().Length == 0)
                    {
                        SearchItemList.Clear();
                        this.OnPropertyChanged("SearchItemsCount");
                        this.OnPropertyChanged("SearchResultText");
                        return;
                    }
                }
                SearchItemList.Clear();
                SearchItemList = itemManager.SearchItem(searchTerm.ToString(), Customer.CustomerNo);
                SearchItemsCount = SearchItemList.Count;
                this.OnPropertyChanged("SearchItemsCount");
                this.OnPropertyChanged("SearchResultText");
            }
            catch (Exception ex)
            {
                
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PreOrder][SearchItems][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][End:SearchItems][searchTerm=" + searchTerm + ",skipMinLengthCheck=" + skipMinLengthCheck + "]");

        }
        public Telerik.Windows.Data.SortDescriptor SortGrid()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][Start:SortGrid]");
            Telerik.Windows.Data.SortDescriptor descriptor = new Telerik.Windows.Data.SortDescriptor();
            descriptor.Member = "SeqNo";
            descriptor.SortDirection = ListSortDirection.Ascending;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][End:SortGrid]");
            return descriptor;
           
        }
        public override bool ConfirmSave()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][Start:ConfirmSave]");
            try
            {
                if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.CustomerHome_Activity.ToString())
                {
                    SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Activity;
                    ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = "";
                }
                Managers.PreOrderManager preOrderManager = new Managers.PreOrderManager();
                ObservableCollection<Customer> customersForDate = new SalesLogicExpress.Application.Managers.ServiceRouteManager().GetCustomersForRoute(CommonNavInfo.RouteUser, StopDate);
                Customer customer = customersForDate.FirstOrDefault(c => c.CustomerNo == Customer.CustomerNo);
                preOrderManager.SavePreOrder(PreOrderItems, Customer.CustomerNo, StopDate);
                // Log Activity for pre-order
                LogActivity(PreOrderItems.ToList<TemplateItem>(), Customer, StopDate, customer.StopID);

                #region Update Activity Count
                string tempStopDate = string.Empty;
                string stopID = string.Empty;
                if (PayloadManager.PreOrderPayload.StopDate.Date > DateTime.Now.Date)
                {
                    tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                    stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.PreOrderPayload.Customer.CustomerNo, tempStopDate);
                }
                else
                {
                    tempStopDate = PayloadManager.PreOrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                    stopID = PayloadManager.PreOrderPayload.Customer.StopID;
                }
                new Managers.CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                new Managers.CustomerManager().UpdateActivityCount(stopID, false, true);
                PayloadManager.PreOrderPayload.Customer.SaleStatus = "";
                PayloadManager.PreOrderPayload.Customer.CompletedActivity += 1;
                new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.PreOrderPayload.Customer.CustomerNo);
                #endregion
                IsDirty = false;
                IsOrderSaved = true;

                if (!IsUnplannedCreatedForFuture)
                {
                    #region Update tem squncing no.
                    DateTime todayDate = Convert.ToDateTime(tempStopDate);
                    ViewModels.CustomerDashboard custDash = new CustomerDashboard();
                    // Checks for customer alteration if user invoked past activity.
                    if (PayloadManager.PreOrderPayload.IsCustomerLoadedForPastActivity)
                    {
                        PayloadManager.PreOrderPayload.IsCustomerLoadedForPastActivity = false;
                        PayloadManager.OrderPayload.Customer = new CustomerDashboard().GetNewCustomer(PayloadManager.PreOrderPayload.StopDateBeforeInvokingActivity.Value.Date, PayloadManager.PreOrderPayload.Customer);
                    }
                    else
                        PayloadManager.OrderPayload.Customer = custDash.GetNewCustomer(todayDate, PayloadManager.PreOrderPayload.Customer);

                    #endregion
                }
                PayloadManager.PreOrderPayload.Reset();
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][PreOrder][ConfirmSave][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][End:ConfirmSave]");
            return base.ConfirmSave();
        }
        public override bool ConfirmCancel()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][Start:ConfirmCancel]");
            IsDirty = false;
            IsOrderSaved = true;
            base.CanNavigate = true;
            PayloadManager.PreOrderPayload.Reset();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreOrder][End:ConfirmCancel]");
            return base.ConfirmCancel();
        }
        #endregion

        /********************************* 
         
        string str = "1/18/2015";
        DateTime dt = Convert.ToDateTime(str); 
        Console.WriteLine(dt);
        Console.WriteLine("Year: {0}, Month: {1}, Day: {2}", dt.Year, dt.Month, dt.Day);
        string currentdate = DateTime.Now.ToString("MM/dd/yyyy");
        Console.WriteLine(currentdate);
         
         *********************************/

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                if (preOrderM != null)
                    preOrderM.Dispose();
                if (templateManager != null)
                    templateManager.Dispose();
                if (tokenForCancelTask != null)
                    tokenForCancelTask.Dispose();
                if (itemManager != null)
                    itemManager.Dispose();
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
