﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using log4net;
using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using System.Collections.ObjectModel;
using Models = SalesLogicExpress.Domain;

namespace SalesLogicExpress.Application.ViewModels
{
    public class UserSetupTabViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel");
        private ObservableCollection<Models.UserProfile> _UserProfileInfo = null;
        public BaseViewModel ParentViewModel { get; set; }

        public Guid MessageToken { get; set; }
        public ObservableCollection<Models.UserProfile> UserProfileInfo
        {
            get
            {
                return _UserProfileInfo;
            }
            set
            {
                _UserProfileInfo = value;
                OnPropertyChanged("UserProfileInfo");
            }
        }

        public UserSetupTabViewModel()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][UserSetupTabViewModel][Start:Constructor]");
            this.MessageToken = CommonNavInfo.MessageToken;
            this.UserProfileInfo = UserManager.GetUserProfile(CommonNavInfo.RouteID);
            this.UserProfileInfo.FirstOrDefault().UserTitle = UserManager.GetUserTitle();
            this.UserProfileInfo.FirstOrDefault().ScannerMode = UserManager.GetScannerMode();
            //this.InitializeCommands();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][UserSetupTabViewModel][End:Constructor]");
        }

        public DelegateCommand SaveUserProfile { get; set; }
        //private void InitializeCommands()
        //{
        //    SaveUserProfile = new DelegateCommand((param) =>
        //    {
        //        SaveUserProfileInformation();
        //    });
        //}

        //public void SaveUserProfileInformation()
        //{
        //    // Delegate Implementations
        //}

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
