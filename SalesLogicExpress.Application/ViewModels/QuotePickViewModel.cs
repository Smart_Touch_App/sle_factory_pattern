﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using GalaSoft.MvvmLight.Messaging;
using Telerik.Windows.Controls;
using System.Collections.ObjectModel;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.Managers.ProspectManagers;
using System.Collections;

namespace SalesLogicExpress.Application.ViewModels
{
    public class QuotePickViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.QuotePickViewModel");
        PickItemManager pickMgr;
        #region Properties & Variables
        public Guid MessageToken { get; set; }
        private string uPickID = string.Empty;

        private ObservableCollection<ReasonCode> manualPickReasonList = new ObservableCollection<ReasonCode>();

        public ObservableCollection<ReasonCode> ManualPickReasonList
        {
            get { return manualPickReasonList; }
            set { manualPickReasonList = value; }
        }



        #endregion

        #region OnPropertyChanged

        #region Bool




        private bool isEnableRemoveException = false;
        public bool IsEnableRemoveException
        {
            get { return isEnableRemoveException; }
            set { isEnableRemoveException = value; OnPropertyChanged("IsEnableRemoveException"); }
        }

        private bool isEnablePrint = false;
        public bool IsEnablePrint
        {
            get { return isEnablePrint; }
            set { isEnablePrint = value; OnPropertyChanged("IsEnablePrint"); }
        }

        private bool isEnableManualPick = false;
        public bool IsEnableManualPick
        {
            get { return isEnableManualPick; }
            set { isEnableManualPick = value; OnPropertyChanged("IsEnableManualPick"); }
        }


        private bool isFocusedManualPick = false;
        public bool IsFocusedManualPick
        {
            get { return isFocusedManualPick; }
            set { isFocusedManualPick = value; OnPropertyChanged("IsFocusedManualPick"); }
        }

        #endregion

        #region int
        private int manuallyPickCountOfSelected = 0;
        public int ManuallyPickCountOfSelected
        {
            get { return manuallyPickCountOfSelected; }
            set { manuallyPickCountOfSelected = value; OnPropertyChanged("ManuallyPickCountOfSelected"); }
        }
        #endregion

        #region String

        private string quoteNo = string.Empty;

        public string QuoteNo
        {
            get { return quoteNo; }
            set { quoteNo = value; OnPropertyChanged("QuoteNo"); }
        }


        private string messageLine1 = string.Empty;
        public string MessageLine1
        {
            get { return messageLine1; }
            set { messageLine1 = value; OnPropertyChanged("MessageLine1"); }
        }


        private string messageLine2 = string.Empty;
        public string MessageLine2
        {
            get { return messageLine2; }
            set { messageLine2 = value; OnPropertyChanged("MessageLine2"); }
        }


        private string itemNumberOfSelected = string.Empty;
        public string ItemNumberOfSelected
        {
            get { return itemNumberOfSelected; }
            set { itemNumberOfSelected = value; OnPropertyChanged("ItemNumberOfSelected"); }
        }


        private string primaryUMOfSelected = string.Empty;
        public string PrimaryUMOfSelected
        {
            get { return primaryUMOfSelected; }
            set { primaryUMOfSelected = value; OnPropertyChanged("PrimaryUMOfSelected"); }
        }


        private string primaryQtyHeader = string.Empty;
        public string PrimaryQtyHeader
        {
            get { return primaryQtyHeader; }
            set { primaryQtyHeader = value; OnPropertyChanged("PrimaryQtyHeader"); }
        }

        bool isScannerOn = false;
        public bool IsScannerOn
        {
            get
            {
                return isScannerOn;
            }
            set
            {
                isScannerOn = value;
                OnPropertyChanged("IsScannerOn");
            }
        }

        #endregion

        #region Class

        private QuotePickModel quotePickSelectedItem = null;
        public QuotePickModel QuotePickSelectedItem
        {
            get { return quotePickSelectedItem; }
            set
            {
                try
                {
                    quotePickSelectedItem = value; OnPropertyChanged("QuotePickSelectedItem");
                    if (value != null)
                    {
                        IsFocusedManualPick = false;
                        QuotePickExceptionSelectedItem = null;

                        // Set Manual pick panel
                        PrimaryUMOfSelected = QuotePickSelectedItem.TransactionUOM;
                        ManuallyPickCountOfSelected = Convert.ToInt32(QuotePickSelectedItem.TransactionQty);
                        ItemNumberOfSelected = QuotePickSelectedItem.ItemNumber;

                        //Enable Disable Short and manual pick
                        if (!PayloadManager.RouteReplenishmentPayload.NavigatedForVoid)
                        {
                            IsEnableManualPick = true;
                            QuotePickSelectedItem.IsEnableScanner = true;
                        }
                        QuotePickSelectedItem.IsAllowManualPick = false;
                        if (!QuotePickSelectedItem.IsAllowManualPick)
                        {
                            foreach (QuotePickModel item in PickItems)
                            {
                                item.IsAllowManualPick = false;
                            }
                        }
                        foreach (QuotePickModel item in PickItemsException)
                        {
                            item.IsAllowManualPick = false;
                            item.IsEnableScannerInException = false;
                        }
                        if (!PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck)
                        {
                            EnableDisableScannerForSelectedItem();
                        }
                        foreach (QuotePickModel item in PickItems)
                        {
                            item.IsEnableScannerInException = false;
                        }
                    }
                }
                catch (Exception ex)
                {

                    Logger.Error("QuotePickViewModel QuotePickSelectedItem error: " + ex.Message);
                }
            }
        }



        private QuotePickModel quotePickExceptionSelectedItem = null;
        public QuotePickModel QuotePickExceptionSelectedItem
        {
            get { return quotePickExceptionSelectedItem; }
            set
            {
                try
                {

                    quotePickExceptionSelectedItem = value; OnPropertyChanged("QuotePickExceptionSelectedItem");
                    if (value != null)
                    {
                        IsFocusedManualPick = false;
                        QuotePickSelectedItem = null;

                        if (!QuotePickExceptionSelectedItem.IsEnableScannerInException)
                        {
                            IsEnableRemoveException = true;
                        }
                        if (PayloadManager.RouteReplenishmentPayload.NavigatedForVoid)
                        {
                            IsEnableRemoveException = false;
                        }

                        PrimaryUMOfSelected = QuotePickExceptionSelectedItem.TransactionUOM;
                        ItemNumberOfSelected = QuotePickExceptionSelectedItem.ItemNumber;
                        //ManuallyPickCountOfSelected = 0;
                        ManuallyPickCountOfSelected = Convert.ToInt32(QuotePickExceptionSelectedItem.ExceptionCount);

                        if (QuotePickExceptionSelectedItem.IsEnableScannerInException)
                        {
                            QuotePickExceptionSelectedItem.IsAllowManualPick = false; IsEnableManualPick = true;
                        }
                        else
                        {
                            foreach (QuotePickModel item in PickItemsException)
                            {
                                item.IsAllowManualPick = false;
                                item.IsEnableScannerInException = false;
                            }
                            IsEnableManualPick = false;
                        }
                        QuotePickSelectedItem = null;
                    }
                    else
                    {
                        IsEnableRemoveException = false;
                    }
                }
                catch (Exception ex)
                {

                    Logger.Error("QuotePickViewModel QuotePickSelectedItem error: " + ex.Message);
                }
            }
        }



        private ReasonCode selectedManualPickReasonCode = new ReasonCode();
        public ReasonCode SelectedManualPickReasonCode
        {
            get { return selectedManualPickReasonCode; }
            set { selectedManualPickReasonCode = value; OnPropertyChanged("SelectedManualPickReasonCode"); }
        }

        private CustomerQuoteHeader customerQuote = new CustomerQuoteHeader();

        public CustomerQuoteHeader CustomerQuote
        {
            get { return customerQuote; }
            set { customerQuote = value; OnPropertyChanged("CustomerQuote"); }
        }

        #endregion

        #region Collection

        ObservableCollection<dynamic> pickItems = new ObservableCollection<dynamic>();
        public ObservableCollection<dynamic> PickItems
        {
            get { return pickItems; }
            set
            {
                pickItems = value; OnPropertyChanged("PickItems");
            }
        }



        private ObservableCollection<dynamic> pickItemsException = new ObservableCollection<dynamic>();
        public ObservableCollection<dynamic> PickItemsException
        {
            get { return pickItemsException; }
            set { pickItemsException = value; OnPropertyChanged("PickItemsException"); }
        }
        #endregion

        #endregion

        #region Commands Declaretion
        public DelegateCommand SaveCommand { get; set; }
        public DelegateCommand SaveGetManualPickReasonListCommand { get; set; }
        public DelegateCommand ScanByDeviceCommand { get; set; }
        public DelegateCommand RemoveCommand { get; set; }
        public DelegateCommand RemoveOKCommand { get; set; }
        public DelegateCommand ScanByDeviceExceptionCommand { get; set; }
        public DelegateCommand CompletePickCommand { get; set; }
        public DelegateCommand ShortCommand { get; set; }
        public DelegateCommand SetManualPickReasonCommand { get; set; }
        public DelegateCommand ManualPickCommand { get; set; }

        public DelegateCommand ManualPickContinueCommand { get; set; }
        public DelegateCommand UpdateOnManualCountCommand { get; set; }
        public DelegateCommand HoldCommand { get; set; }
        public DelegateCommand PrintCommand { get; set; }
        public DelegateCommand HoldYesCommand { get; set; }
        public DelegateCommand HoldNoCommand { get; set; }
        public DelegateCommand TwoLineAlertOKCommand { get; set; }

        public DelegateCommand SwitchScannerMode { get; set; }
        #endregion

        #region Command Defination

        public void InitialiseCommands()
        {
            SwitchScannerMode = new DelegateCommand((param) =>
            {
                bool InvertMode = !IsScannerOn;
                IsScannerOn = InvertMode;
            });

            SaveCommand = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    //SaveExecute();
                }
            });

            SaveGetManualPickReasonListCommand = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    ReasonCode obj = (ReasonCode)param;
                    SaveGetManualPickReasonListExecute(obj);
                }
            });

            ScanByDeviceCommand = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    QuotePickModel obj = (QuotePickModel)param;
                    ScanByDeviceExecute(obj);
                }
            });

            ScanByDeviceExceptionCommand = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    QuotePickModel obj = (QuotePickModel)param;
                    ScanByDeviceExceptionExecute(obj);

                }
            });

            RemoveCommand = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    QuotePickModel obj = (QuotePickModel)param;
                    RemoveExecute();
                }
            });

            RemoveOKCommand = new DelegateCommand((param) =>
            {
                RemoveOKExecute();

            });

            SetManualPickReasonCommand = new DelegateCommand((param) =>
            {
                SetManualPickReasonExecute();

            });
            ManualPickCommand = new DelegateCommand((param) =>
            {
                ManualPickExecute();
            });
            ManualPickContinueCommand = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    ManualPickContinueExecute();
                }
            });
            UpdateOnManualCountCommand = new DelegateCommand((param) =>
            {
                UpdateOnManualCountExecute();
            });
            HoldCommand = new DelegateCommand((param) =>
            {
                HoldExecute();
            });
            PrintCommand = new DelegateCommand((param) =>
            {
                PrintExecute();
            });

            HoldYesCommand = new DelegateCommand((param) =>
            {
                HoldYesExecute();
            });
            HoldNoCommand = new DelegateCommand((param) =>
            {
                HoldNoExecute();
            });
            TwoLineAlertOKCommand = new DelegateCommand((param) =>
            {
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
            });

        }

        public void ScanByDeviceExecute(QuotePickModel param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:ScanByDeviceExecute]\t" + DateTime.Now + "");

            try
            {
                param.ManuallyPickCount = 0; ManuallyPickCountOfSelected = 0;
                dynamic itm1 = PickItems.FirstOrDefault(i => i.ItemId == param.ItemId && i.TransactionDetailID == param.TransactionDetailID);
                if (itm1.ExceptionReasonCode == "Short")
                {
                    //Show popup 
                    MessageLine1 = "Your last picked item is short adjusted.";
                    MessageLine2 = "Please put it back.";
                    var dialog = new Helpers.DialogWindow { TemplateKey = "TwoLineAlert", Title = "Alert" };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                    return;
                }
                param.PickAdjusted = 0;
                param.IsManuallyPicking = false;
                if (param.TransactionUOM != "loose")
                {
                    // Null Data
                    pickMgr.PickItem(param, null, null,false);
                }

                foreach (QuotePickModel item in PickItems)
                {
                    if (item != null)
                    {
                        item.IsAllowManualPick = false;
                    }
                }
                foreach (QuotePickModel item in PickItemsException)
                {
                    if (item != null)
                    {
                        item.IsAllowManualPick = false;
                    }
                }

                QuotePickSelectedItem = param; QuotePickExceptionSelectedItem = null;

                if (QuotePickSelectedItem != null)
                {
                    QuotePickSelectedItem.IsAllowManualPick = true;
                }
                IsFocusedManualPick = true;

            }
            catch (Exception ex)
            {
                Logger.Error("QuotePickViewModel ScanByDeviceExecute(" + param.ItemNumber + ") error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:ScanByDeviceExecute]\t");



        }

        public void ScanByDeviceExceptionExecute(QuotePickModel param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:ScanByDeviceExceptionExecute]\t" + DateTime.Now + "");

            try
            {
                param.ManuallyPickCount = 0; ManuallyPickCountOfSelected = 0;
                //You can not unpick item which is not present in exception list
                dynamic itm1 = PickItemsException.FirstOrDefault(i => i.ItemId == param.ItemId && i.TransactionDetailID == param.TransactionDetailID);
                if (itm1 == null)
                {
                    //Show popup 
                    MessageLine1 = "Your last picked item is not in exception list.";
                    MessageLine2 = "Please put it back.";
                    var dialog = new Helpers.DialogWindow { TemplateKey = "TwoLineAlert", Title = "Alert" };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                    return;
                }
                param.IsManuallyPicking = false;
                if (param.TransactionUOM != "loose")
                {
                    pickMgr.UnPickItem(param);
                }

                foreach (QuotePickModel item in PickItemsException)
                {
                    if (item != null)
                    {
                        item.IsAllowManualPick = false;
                    }
                }
                foreach (QuotePickModel item in PickItems)
                {
                    if (item != null)
                    {
                        item.IsAllowManualPick = false;
                    }
                }
                QuotePickExceptionSelectedItem = param; QuotePickSelectedItem = null;
                if (QuotePickExceptionSelectedItem != null)
                {
                    QuotePickExceptionSelectedItem.IsAllowManualPick = true;
                }

                IsFocusedManualPick = true;
            }
            catch (Exception ex)
            {
                Logger.Error("QuotePickViewModel ScanByDeviceExceptionExecute(" + param.ItemNumber + ") error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:ScanByDeviceExceptionExecute]\t");



        }

        public void RemoveExecute()
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:RemoveExceptionsExecute]\t" + DateTime.Now + "");

            try
            {
                //Show popup for remove Exception
                MessageLine1 = "Are you sure, you want to remove exceptions?";
                MessageLine2 = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck ? "You should now put-away any exception picked by scanning them back onto your branch." : "You should now put-away any exception picked by scanning them back onto your truck.";
                var dialog = new Helpers.DialogWindow { TemplateKey = "RemoveExceptionInAddLoadPicks", Title = "Remove exception" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);

            }
            catch (Exception ex)
            {
                Logger.Error("QuotePickViewModel ScanByDeviceExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:RemoveExceptionsExecute]\t");
        }

        public void RemoveOKExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:RemoveExceptionOKExecute]\t" + DateTime.Now + "");

            try
            {
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                foreach (QuotePickModel item in PickItemsException)
                {
                    item.IsEnableScannerInException = true;
                }
                //QuotePickExceptionSelectedItem.IsEnableScannerInException = true;
                QuotePickSelectedItem = null; IsEnableManualPick = true; IsEnableRemoveException = false;
            }
            catch (Exception ex)
            {
                Logger.Error("QuotePickViewModel RemoveExceptionOKExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:RemoveExceptionOKExecute]\t");
        }
        public void SaveGetManualPickReasonListExecute(ReasonCode obj)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:SaveGetManualPickReasonListExecute]\t" + DateTime.Now + "");
            var dialog = new Helpers.DialogWindow { TemplateKey = "ManualPickReason", Title = "Reason Code", Payload = null };
            Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:SaveGetManualPickReasonListExecute]\t");
        }

        public void SetManualPickReasonExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:SetManualPickReasonExecute]\t" + DateTime.Now + "");
            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:SetManualPickReasonExecute]\t");
        }
        public void ManualPickExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:ManualPickExecute]\t" + DateTime.Now + "");

            try
            {
                bool showAlert = false;
                if (QuotePickSelectedItem != null)
                {
                    if (!string.IsNullOrEmpty(QuotePickSelectedItem.ItemId))
                    {
                        var dialog = new Helpers.DialogWindow { TemplateKey = "ManualPickReason", Title = "Reason Code", Payload = null };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                    }
                    else
                    {
                        showAlert = true;
                    }
                }
                else if (QuotePickExceptionSelectedItem != null)
                {

                    if (!string.IsNullOrEmpty(QuotePickExceptionSelectedItem.ItemId))
                    {
                        var dialog = new Helpers.DialogWindow { TemplateKey = "ManualPickReason", Title = "Reason Code", Payload = null };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                    }
                    else
                    {
                        showAlert = true;

                    }
                }
                else
                {
                    showAlert = true;

                }
                if (showAlert)
                {
                    var alertMessage = new Helpers.AlertWindow { Message = "Select item for manual pick.", MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    return;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("QuotePickViewModel ManualPickExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:ManualPickExecute]\t");



        }
        public void ManualPickContinueExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:ManualPickContinueExecute]\t" + DateTime.Now + "");

            try
            {
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                if (QuotePickSelectedItem != null)
                {
                    QuotePickSelectedItem.IsAllowManualPick = true;
                }
                if (QuotePickExceptionSelectedItem != null)
                {
                    QuotePickExceptionSelectedItem.IsAllowManualPick = true;
                }
                IsFocusedManualPick = true;

            }
            catch (Exception ex)
            {
                Logger.Error("QuotePickViewModel ManualPickContinueExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:ManualPickContinueExecute]\t");



        }
        public void UpdateOnManualCountExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:UpdateOnManualCountExecute]\t" + DateTime.Now + "");
            try
            {
                if (QuotePickSelectedItem != null)
                {
                    if (isScannerOn == false)
                        QuotePickSelectedItem.IsAllowManualPick = true;

                    if (QuotePickSelectedItem.IsAllowManualPick)
                    {
                        QuotePickSelectedItem.ManuallyPickCount = ManuallyPickCountOfSelected;
                        if (QuotePickSelectedItem.ManuallyPickCount == QuotePickSelectedItem.PickedQty)
                        {
                            return;
                        }
                        if (QuotePickSelectedItem.ManuallyPickCount > QuotePickSelectedItem.TransactionQty)
                        {
                            var alertMessage = new Helpers.AlertWindow { Message = "Manual quantity exceeds load quantity", MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                            return;
                        }
                        QuotePickSelectedItem.PickAdjusted = 0;
                        QuotePickSelectedItem.IsManuallyPicking = true;
                        if (QuotePickSelectedItem.TransactionUOM != "loose")
                        {
                            // Null Data
                            pickMgr.PickItem(QuotePickSelectedItem, null, null,false);

                        }
                        //Enable Disable buttons
                        ManuallyPickCountOfSelected = 0;
                        foreach (QuotePickModel item in PickItems)
                        {
                            item.IsAllowManualPick = false;
                        }
                        foreach (QuotePickModel item in PickItemsException)
                        {
                            item.IsAllowManualPick = false;
                        }
                    }
                    else if (!string.IsNullOrEmpty(QuotePickSelectedItem.ItemId))
                    {
                        if (QuotePickExceptionSelectedItem == null)
                        {
                            var alertMessage = new Helpers.AlertWindow { Message = "Select reason for manual pick.", MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        }
                    }
                }

                if (QuotePickExceptionSelectedItem != null)
                {
                    if (!QuotePickExceptionSelectedItem.IsEnableScannerInException)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = "Click remove button to enable scanner.", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    if (QuotePickExceptionSelectedItem.IsAllowManualPick && QuotePickExceptionSelectedItem.IsEnableScannerInException)
                    {
                        QuotePickExceptionSelectedItem.ManuallyPickCount = ManuallyPickCountOfSelected;
                        if (QuotePickExceptionSelectedItem.ManuallyPickCount == 0)
                        {
                            return;
                        }
                        if (QuotePickExceptionSelectedItem.ExceptionCount < QuotePickExceptionSelectedItem.ManuallyPickCount)
                        {
                            //AddLoadPickExceptionSelectedItem.ManuallyPickCount = AddLoadPickExceptionSelectedItem.ExceptionCount;
                            //ManuallyPickCountOfSelected = AddLoadPickExceptionSelectedItem.ExceptionCount;
                            var alertMessage = new Helpers.AlertWindow { Message = "Manual quantity exceeds load quantity", MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                            return;
                        }

                        QuotePickExceptionSelectedItem.IsManuallyPicking = true;
                        if (QuotePickExceptionSelectedItem.TransactionUOM != "loose")
                        {
                            pickMgr.UnPickItem(QuotePickExceptionSelectedItem);
                        }
                        ManuallyPickCountOfSelected = 0;
                        //Enable Disable buttons
                        EnableDisableCompletePick();
                        foreach (QuotePickModel item in PickItemsException)
                        {
                            item.IsAllowManualPick = false;
                        }
                        foreach (QuotePickModel item in PickItems)
                        {
                            item.IsAllowManualPick = false;
                        }
                    }
                    else if (!string.IsNullOrEmpty(QuotePickExceptionSelectedItem.ItemId))
                    {
                        if (QuotePickSelectedItem == null)
                        {
                            var alertMessage = new Helpers.AlertWindow { Message = "Select reason for manual pick.", MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken); return;
                        }
                    }
                }
                if (QuotePickExceptionSelectedItem == null && QuotePickSelectedItem == null)
                {
                    var alertMessage = new Helpers.AlertWindow { Message = "Select Item for manual pick.", MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken); return;
                }
                LogActivity(ActivityKey.PickItem);
            }
            catch (Exception ex)
            {
                Logger.Error("QuotePickViewModel UpdateOnManualCountExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:UpdateOnManualCountExecute]\t");

        }

        // Log Activity - Implementation in Quote - 11/11/2016

        void LogActivity(ActivityKey key)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:LogActivity]");
            LogActivity(key, false);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:LogActivity]");
        }

        void LogActivity(ActivityKey key, bool logEndTime)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:asyncLogActivity]");
            //await Task.Factory.StartNew(() =>
            //{
            try
            {
                PayloadManager.ProspectPayload.QuoteNo = QuoteNo;
                SalesLogicExpress.Domain.Activity ac = new SalesLogicExpress.Domain.Activity
                {
                    ActivityFlowID = Convert.ToInt32(PayloadManager.ProspectPayload.QuoteNo),
                    ActivityHeaderID = PayloadManager.ProspectPayload.TransactionID,
                    CustomerID = PayloadManager.ProspectPayload.Prospect.ToString(),
                    ActivityType = key.ToString(),
                    ActivityStart = DateTime.Now,
                    //ActivityEnd = logEndTime ? DateTime.Now : DateTime.MinValue,
                    IsTxActivity = true,
                    ActivityDetailClass = PayloadManager.ProspectPayload.ToString(),
                    ActivityDetails = PayloadManager.ProspectPayload.SerializeToJson()
                };
                if (key == ActivityKey.PickItem)
                {
                    ac.ActivityHeaderID = PayloadManager.OrderPayload.PickOrderTransactionID;
                    ac.ActivityDetailClass = "SalesLogicExpress.Domain.PickOrderItem";
                    ac.ActivityDetails = QuotePickSelectedItem.SerializeToJson();
                    ResourceManager.Transaction.LogActivity(ac);

                }
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][asyncLogActivity][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:asyncLogActivity]");
            //});
        }



        public void HoldExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:HoldExecute]\t" + DateTime.Now + "");
            // Show popup
            MessageLine1 = "Are you sure you want to hold this transaction.";
            MessageLine2 = "Do you want to continue?";
            var dialog = new Helpers.DialogWindow { TemplateKey = "HoldPickItemAddLoad", Title = "Confirmation", Payload = null };
            Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:HoldExecute]\t");
        }
        public void PrintExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:PrintExecute]\t" + DateTime.Now + "");
            var confirmPrintWindow = new ConfirmWindow { OkButtonContent = "Yes", CancelButtonContent = "No", Message = "Do you want to print this transaction?", Header = "Print", MessageIcon = "Alert", Confirmed = false };
            Messenger.Default.Send<ConfirmWindow>(confirmPrintWindow, MessageToken);
            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
            //PrintAsync(confirmPrintWindow.Confirmed);
            ExecutePrint(confirmPrintWindow.Confirmed);

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:HoldExecute]\t");
        }

        public void ExecutePrint(bool isConfirmPrintWindow)
        {
            try
            {
                if (isConfirmPrintWindow)
                {
                    new ReportViewModels.QuotePickReport(PickItems);
                    if (SelectedTab == ViewModelMappings.TabView.ProspectHome.Quotes)
                    {
                        if (quotePickSelectedItem == null)
                        {
                            ShowQuotePickReport(PickItems, null);
                        }
                        else { ShowQuotePickReport(PickItems, quotePickSelectedItem); }
                    }
                }
                //else
                //{
                //    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ProspectHome, CurrentViewName = ViewModelMappings.View.ProspectQuote, CloseCurrentView = true, Refresh = true, SelectTab = ViewModelMappings.TabView.ProspectHome.Quotes };
                //    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                //}
            }
            catch (Exception ex)
            {

                Logger.Error("QuotePickViewModel ExecutePrint() error: " + ex.Message);
            }
        }

        private async void PrintAsync(bool isConfirmPrintWindow)
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:PrintAsync]\t" + DateTime.Now + "");

                try
                {
                    //if (isConfirmPrintWindow)
                    //{
                    //    new ReportViewModels.QuotePickReport(PickItems);
                    //    if (SelectedTab == ViewModelMappings.TabView.ProspectHome.Quotes)
                    //    {
                    //        if (quotePickSelectedItem == null)
                    //        {
                    //            ShowQuotePickReport(PickItems, null);
                    //        }
                    //        else { ShowQuotePickReport(PickItems, quotePickSelectedItem); }
                    //    }
                    //}
                }
                catch (Exception ex)
                {
                    Logger.Error("QuotePickViewModel PrintAsync() error: " + ex.Message);
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:PrintAsync]\t");


            });
        }

        // Quote Pick Report Implementation
        public void ShowQuotePickReport(IList PickItems, QuotePickModel quotePick)
        {
            try
            {
                var DocInfo = new ReportEngine.DocumentInformation();
                var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                repEngine.Notify += repEngine_Notify;

                repEngine.Log = this.Logger;
                ReportEngine.IReportSLE ReportData = null;
                var DocumentId = 0;
                var SourceDocumentNo = Convert.ToInt32(QuoteNo);
                if (DocumentId <= 0)
                {
                    ReportData = new ReportViewModels.QuotePickReport(PickItems);
                    DocInfo = repEngine.Generate<Reporting.QuotePick>(SourceDocumentNo, true, ReportEngine.ReportCategory.Customer, ReportEngine.ReportsTypes.QuotePick, ReportData, null, ReportEngine.ContentTypes.PDF);
                }
                else
                {
                    DocInfo.DocumentId = DocumentId;
                }

                if (DocInfo.IsDocumentCreated)
                    ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.Prospect_Quote_Header", "ProspectQuoteId", SourceDocumentNo, DocInfo.DocumentId);

                //quotePick.DocumentID = DocInfo.DocumentId; //Replace Latest

                var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Customer, DocInfo.DocumentId, null);

                if (ReportBytes == null)
                {
                    Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Report not generated, Please try again." }, MessageToken);
                    return;
                }
                //GenericReport.GenericReportViewModel grv = new GenericReport.GenericReportViewModel();
                //grv.ReportSource = ReportBytes;

                SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
                Payload.ReportFile = ReportBytes;
                Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                Payload.PreviousView = ViewModelMappings.View.ProspectQuotePick;
                Payload.CurrentViewTitle = "Prospect Quote Pick";
                Payload.PreviousViewTitle = "Prospect Quote Pick";
                Payload.MessageToken = this.MessageToken;

                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.ProspectQuotePick, CloseCurrentView = true, Payload = Payload, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("ReplenishAddLoadPickViewModel ShowQuotePickReport() Error: " + ex.Message);
            }

        }

        void repEngine_Notify(string msg)
        {
            System.Windows.MessageBox.Show(msg);
        }

        public void HoldYesExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:HoldYesExecute]\t" + DateTime.Now + "");

            try
            {
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                IsDirty = false;
                // Navigate to replenishment tab
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.ReplenishAddLoadPickView, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = ViewModelMappings.TabView.RouteHome.Replenishment };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("QuotePickViewModel HoldYesExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:HoldYesExecute]\t");

        }
        public void HoldNoExecute()
        {
            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
        }
        #endregion

        #region Constructor & Methods
        public QuotePickViewModel()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:Constructor]\t" + DateTime.Now + "");
            try
            {
                InitialiseCommands();
                InitialiseVariables();

                SetScannerModeByProfile();

                //Set Headers
                CustomerQuote.QuoteIdString = "Quote No: " + ViewModelPayload.PayloadManager.QuotePayload.QuoteId;

                CustomerQuote.QuotePersonName = ViewModelPayload.PayloadManager.ProspectPayload.Prospect.Name;

                CustomerQuote.QuotePersonAddress = ViewModelPayload.PayloadManager.ProspectPayload.Prospect.Address;

                CustomerQuote.QuotePersonCity = ViewModelPayload.PayloadManager.ProspectPayload.Prospect.City;

                CustomerQuote.QuotePersonState = ViewModelPayload.PayloadManager.ProspectPayload.Prospect.State;

                CustomerQuote.QuotePersonZip = ViewModelPayload.PayloadManager.ProspectPayload.Prospect.Zip;

                CustomerQuote.QuotePersonPhone = ViewModelPayload.PayloadManager.ProspectPayload.Prospect.Phone;

                CustomerQuote.Parent = ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID + ", " + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.Name;

                CustomerQuote.BillTo = CustomerQuote.Parent;
            }
            catch (Exception ex)
            {
                Logger.Error("QuotePickViewModel Constructor() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:Constructor]\t");
        }
        public void InitialiseVariables()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:InitialiseVariables]\t" + DateTime.Now + "");

            try
            {

                QuoteNo = PayloadManager.QuotePayload.QuoteId;
                SelectedTab = ViewModelMappings.TabView.ProspectHome.Quotes;

                Managers.PickManager pickManager = new PickManager();
                ManualPickReasonList = pickManager.GetReasonListForManualPick();

                ObservableCollection<QuotePickModel> QuotePickCollectionTemp = QuotesManager.GetQuotePickCollection();
                GetPickCollection(QuotePickCollectionTemp);
                EnableDisableCompletePick();
            }
            catch (Exception ex)
            {
                Logger.Error("QuotePickViewModel InitialiseVariables() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:InitialiseVariables]\t");


        }

        public void GetPickCollection(ObservableCollection<QuotePickModel> PickCollectionTemp)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:GetPickCollection]\t" + DateTime.Now + "");

            try
            {

                List<dynamic> items = new System.Collections.Generic.List<dynamic>();
                //foreach (var CC_Item in PickCollectionTemp)
                //{
                //    items.Add(CC_Item);
                //}

                //Using Linq insted of foreach - by Dinesh
                items.AddRange(PickCollectionTemp);

                pickMgr = new PickItemManager(CommonNavInfo.RouteID.ToString(), PayloadManager.QuotePayload.QuoteId, PickItemManager.PickForTransaction.QuotePick, true);
                pickMgr.PickItemUpdate += pickMgr_PickItemUpdate;
                pickMgr.PickUpdate += pickMgr_PickUpdate;

                if (!PayloadManager.RouteReplenishmentPayload.NavigatedForVoid)
                {
                    foreach (var item in items)
                    {
                        if (item.PrimaryUM != item.TransactionUOM && item.PickQtyPrimaryUOM > 0)
                        {
                            double convFactor = UoMManager.GetUoMFactor(item.UM, item.PrimaryUM, Convert.ToInt32(item.ItemId.Trim()), item.ItemNumber.Trim());
                            item.TransactionQty = convFactor * item.TransactionQty;
                            item.ConversionFactor = 1;
                        }
                    }
                }


                Dictionary<PickItemManager.PickType, ObservableCollection<dynamic>> pickList = pickMgr.CreatePickList(items, typeof(QuotePickModel));
                PickItemsException = pickList[PickItemManager.PickType.Exception];
                PickItemsException.CollectionChanged += PickItemsException_CollectionChanged;


                if (!PayloadManager.ProspectPayload.NavigatedForVoid)
                {
                    PickItems = pickList[PickItemManager.PickType.Normal];
                    foreach (QuotePickModel item in PickItems)
                    {
                        if (item.PrimaryUM != item.TransactionUOM && item.PickedQty == 0)
                        {
                            double convFactor = UoMManager.GetUoMFactor(item.UM, item.PrimaryUM, Convert.ToInt32(item.ItemId.Trim()), item.ItemNumber.Trim());
                            item.TransactionQtyPrimaryUOM = convFactor * item.TransactionQty;
                            item.TransactionQtyDisplay = item.TransactionQty;
                            item.TransactionQty = item.TransactionQtyPrimaryUOM;
                            item.ConversionFactor = 1;
                        }

                        if (item.TransactionUOM == "loose")
                        {
                            item.PickedQty = Convert.ToInt32(item.TransactionQty);
                            item.ExceptionReasonCode = "Complete";
                        }
                    }
                    foreach (QuotePickModel item in PickItemsException)
                    {
                        item.ConversionFactor = 1;
                        item.TransactionQty = item.TransactionQtyPrimaryUOM;

                        if (item.TransactionUOM == "loose")
                        {
                            PickItemsException.Remove(item);
                        }
                    }
                }
                PickCollectionTemp.Clear();

                EnableDisableScanner();

            }
            catch (Exception ex)
            {
                Logger.Error("QuotePickViewModel GetPickCollection() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:GetPickCollection]\t");

        }

        private void EnableDisableScanner()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:EnableDisableScanner]\t" + DateTime.Now + "");

            try
            {
                foreach (var item in PickItems)
                {
                    //Disable scanner when available qty is 0 AND item is Short adjusted
                    if ("loose" == item.TransactionUOM)
                    {
                        item.IsEnableScanner = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("QuotePickViewModel EnableDisableScanner() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:EnableDisableScanner]\t");


        }

        void PickItemsException_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:PickItemsException_CollectionChanged]\t" + DateTime.Now + "");

            try
            {
                if (PayloadManager.RouteReplenishmentPayload.NavigatedForVoid)
                {
                    IsEnableRemoveException = false;
                    return;
                }
                //foreach (dynamic item in PickItemsException)
                //{
                //    // item.IsEnableScannerInException = false;
                //}
            }
            catch (Exception ex)
            {
                Logger.Error("QuotePickViewModel PickItemsException_CollectionChanged() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:PickItemsException_CollectionChanged]\t");


        }

        void pickMgr_PickUpdate(object sender, PickItemManager.PickEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:pickMgr_PickUpdate]\t" + DateTime.Now + "");
            try
            {
                EnableDisableCompletePick();
            }
            catch (Exception ex)
            {
                Logger.Error("QuotePickViewModel pickMgr_PickUpdate() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:pickMgr_PickUpdate]\t");

        }

        void pickMgr_PickItemUpdate(object sender, PickItemManager.PickItemEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:pickMgr_PickItemUpdate]\t" + DateTime.Now + "");

            try
            {
                EnableDisableCompletePick();
                //Update commited quantity for item in case of return item in DC and Unmanned


                string committedQty = Convert.ToString(e.Item.CommittedQty);
                string heldQty = Convert.ToString(e.Item.HeldQty);
                string ItemID = Convert.ToString(e.Item.ItemId);
                string actualQtyOnHand = Convert.ToString(e.Item.ActualQtyOnHand);
                ReplenishAddLoadManager.UpdateOnhandInventory(actualQtyOnHand, ItemID);


            }
            catch (Exception ex)
            {
                Logger.Error("QuotePickViewModel pickMgr_PickItemUpdate() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:pickMgr_PickItemUpdate]\t");

        }

        //public void CanRemoveException()
        //{
        //    //if (PickItemsException.Count == 0)
        //    //{
        //    //    CanRemoveExceptions = false; return;
        //    //}
        //    //foreach (AddLoadPick item in PickItems)
        //    //{
        //    //    if (item.PickQtyPrimaryUOM < item.TransactionQtyPrimaryUOM)
        //    //    {
        //    //        CanRemoveExceptions = false;
        //    //        break;
        //    //    }
        //    //}
        //}


        public void EnableDisableCompletePick()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:EnableDisableCompletePick]\t" + DateTime.Now + "");

            try
            {
                IsEnablePrint = false;
                if (PickItemsException.Count == 0)
                {
                    QuotePickModel obj = PickItems.FirstOrDefault(x => x.ExceptionReasonCode.ToString() == "UnderPicked" || x.ExceptionReasonCode.ToString() == "OverPicked");
                    if (obj == null)
                    {
                        IsEnablePrint = true;
                    }
                    else
                    {
                        IsEnablePrint = false;
                    }
                }
                if (QuotePickSelectedItem != null)
                {
                    IsEnableManualPick = true;
                    EnableDisableScannerForSelectedItem();

                }
                //ManuallyPickCountOfSelected = 0;
                ManuallyPickCountOfSelected = QuotePickSelectedItem != null ? Convert.ToInt32(QuotePickSelectedItem.TransactionQty) : (QuotePickExceptionSelectedItem != null) ? Convert.ToInt32(QuotePickExceptionSelectedItem.ExceptionCount) : 0;
            }
            catch (Exception ex)
            {
                Logger.Error("QuotePickViewModel EnableDisableCompletePick() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:EnableDisableCompletePick]\t");

        }

        private void EnableDisableScannerForSelectedItem()
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][Start:EnableDisableScannerForSelectedItem]\t" + DateTime.Now + "");

            try
            {
                //Disable scanner when available qty is 0 AND item is Short adjusted
                #region Enable disable scanner
                if (QuotePickSelectedItem.PrimaryUM == QuotePickSelectedItem.TransactionUOM)
                {
                    if (QuotePickSelectedItem.AvailableQty == 0 || QuotePickSelectedItem.ExceptionReasonCode == "Short")
                    {
                        QuotePickSelectedItem.IsAllowManualPick = false;
                        QuotePickSelectedItem.IsEnableScanner = false;
                    }
                    else if (QuotePickSelectedItem.ExceptionReasonCode != "Short")
                    {
                        QuotePickSelectedItem.IsEnableScanner = true;
                        //IsEnableManualPick = true;
                    }
                }
                else if (QuotePickSelectedItem.PrimaryUM != QuotePickSelectedItem.TransactionUOM)
                {
                    if (QuotePickSelectedItem.AvailableQty < Convert.ToInt32(QuotePickSelectedItem.ConversionFactor) || (QuotePickSelectedItem.AvailableQty == 0) || QuotePickSelectedItem.ExceptionReasonCode == "Short")
                    {
                        QuotePickSelectedItem.IsAllowManualPick = false;
                        //IsEnableManualPick = false; 
                        QuotePickSelectedItem.IsEnableScanner = false;

                    }
                    else if (QuotePickSelectedItem.ExceptionReasonCode != "Short")
                    {
                        QuotePickSelectedItem.IsEnableScanner = true;
                        //IsEnableManualPick = true;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("QuotePickViewModel EnableDisableScannerForSelectedItem() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][QuotePickViewModel][End:EnableDisableScannerForSelectedItem]\t");

        }

        // Set Scanner Mode
        private void SetScannerModeByProfile()
        {
            if (UserManager.GetProfileScannerMode() == 1)
            {
                IsScannerOn = false;
            }
            else
            {
                IsScannerOn = true;
            }
        }

        public override bool ConfirmSave()
        {
            IsDirty = false;
            CanNavigate = true;
            return base.ConfirmSave();
        }

        public override bool ConfirmCancel()
        {
            CanNavigate = false;
            IsDirty = true;
            return base.ConfirmCancel();
        }

        public override bool ConfirmClosed()
        {
            IsDirty = true;
            CanNavigate = false;
            return base.ConfirmClosed();
        }
        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
