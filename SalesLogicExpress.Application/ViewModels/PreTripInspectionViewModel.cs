﻿using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using Entities = SalesLogicExpress.Domain;
using Managers = SalesLogicExpress.Application.Managers;
using System.Globalization;
using log4net;
using SalesLogicExpress.Application.ViewModelPayload;
using System.ComponentModel;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using SalesLogicExpress.Domain;
using System.Windows.Ink;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.IO;
using System.Windows.Media;
using System.Configuration;

namespace SalesLogicExpress.Application.ViewModels
{
    public class PreTripInspectionViewModel : BaseViewModel, IDataErrorInfo
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.PreTripInspectionViewModel");

        public static DateTime SelectedCalendarDate = DateTime.Now;

        #region Properties

        private string _Route;
        public string Route
        {
            get
            {
                return _Route;
            }
            set
            {
                _Route = value;
                OnPropertyChanged("Route");
            }
        }

        private string selectedVehicleNumber;

        public string SelectedVehicleNumber
        {
            get { return selectedVehicleNumber; }
            set
            {
                selectedVehicleNumber = value; OnPropertyChanged("SelectedVehicleNumber");
            }
        }

        private string todayDate;
        public string TodayDate
        {
            get { return todayDate; }
            set { todayDate = value; OnPropertyChanged("TodayDate"); }
        }

        private string templateID;
        public string TemplateID
        {
            get { return templateID; }
            set { templateID = value; OnPropertyChanged("TemplateID"); }
        }

        private PreTripInspectionViewModel preTripInspectionVM;
        public PreTripInspectionViewModel PreTripInspectionVM
        {
            get { return preTripInspectionVM; }
            set { preTripInspectionVM = value; OnPropertyChanged("PreTripInspectionVM"); }
        }

        private string confirmationStatement_1;
        public string ConfirmationStatement_1
        {
            get { return confirmationStatement_1; }
            set { confirmationStatement_1 = value; OnPropertyChanged("ConfirmationStatement_1"); }
        }

        private string confirmationStatement_2;
        public string ConfirmationStatement_2
        {
            get { return confirmationStatement_2; }
            set { confirmationStatement_2 = value; OnPropertyChanged("ConfirmationStatement_2"); }
        }

        private string confirmationStatement_3;
        public string ConfirmationStatement_3
        {
            get { return confirmationStatement_3; }
            set { confirmationStatement_3 = value; OnPropertyChanged("ConfirmationStatement_3"); }
        }

        private PreTripVehicleInspection inspectionModel = new PreTripVehicleInspection();
        public PreTripVehicleInspection InspectionModel
        {
            get { return inspectionModel; }
            set { inspectionModel = value; OnPropertyChanged("InspectionModel"); }
        }
        private ObservableCollection<PreTripInspectionFormList> inspectionFormList = new ObservableCollection<PreTripInspectionFormList>();
        public ObservableCollection<PreTripInspectionFormList> InspectionFormList
        {
            get { return inspectionFormList; }
            set
            { inspectionFormList = value; OnPropertyChanged("InspectionFormList"); }
        }
        private ObservableCollection<PreTripInspectionFormList> inspectionFormListTemplate;

        public ObservableCollection<PreTripInspectionFormList> InspectionFormListTemplate
        {
            get { return inspectionFormListTemplate; }
            set { inspectionFormListTemplate = value; }
        }

        private string odometerReading = string.Empty;
        public string OdometerReading
        {
            get { return odometerReading; }
            set
            {
                odometerReading = value;
                OnPropertyChanged("OdometerReading");
                CheckForValidation();
            }
        }

        private string vehicleModel = string.Empty;
        public string VehicleModel
        {
            get { return vehicleModel; }
            set { vehicleModel = value; OnPropertyChanged("VehicleModel"); }
        }

        private ObservableCollection<VehicleNumber> vehicleNumber = new ObservableCollection<VehicleNumber>();
        public ObservableCollection<VehicleNumber> VehicleNumber
        {
            get { return vehicleNumber; }
            set { vehicleNumber = value; OnPropertyChanged("VehicleNumber"); }
        }

        private string additionalComments = string.Empty;
        public string AdditionalComments
        {
            get { return additionalComments; }
            set { additionalComments = value; OnPropertyChanged("AdditionalComments"); }
        }

        private bool toggleSignButton = true;
        public bool ToggleSignButton
        {
            get
            {
                return toggleSignButton;
            }
            set
            {
                toggleSignButton = value; OnPropertyChanged("ToggleSignButton");
            }
        }

        private bool toggleAcceptandSignButton = true;
        public bool ToggleAcceptandSignButton
        {
            get { return toggleAcceptandSignButton; }
            set { toggleAcceptandSignButton = value; OnPropertyChanged("ToggleAcceptandSignButton"); }
        }

        private bool isCanvasVisibe = false;
        public bool IsCanvasVisibe
        {
            get { return isCanvasVisibe; }
            set { isCanvasVisibe = value; OnPropertyChanged("IsCanvasVisibe"); }
        }

        private static bool isCanvasEnable;
        public static bool IsCanvasEnable
        {
            get { return PreTripInspectionViewModel.isCanvasEnable; }
            set { PreTripInspectionViewModel.isCanvasEnable = value; NotifyStaticPropertyChanged("IsCanvasEnable"); }
        }

        private InkCanvas myCanvas;
        public InkCanvas MyCanvas
        {
            get { return myCanvas; }
            set
            {
                myCanvas = value;
                OnPropertyChanged("MyCanvas");
            }
        }

        CanvasViewModel _CanvasVM;
        public CanvasViewModel SignatureCanvasVM
        {
            get
            {
                return _CanvasVM;
            }
            set
            {
                _CanvasVM = value;
                OnPropertyChanged("SignatureCanvasVM");
            }
        }

        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;
        private static void NotifyStaticPropertyChanged(string propertyName)
        {
            if (StaticPropertyChanged != null)
                StaticPropertyChanged(null, new PropertyChangedEventArgs(propertyName));
        }
        Guid _MessageToken;
        public Guid MessageToken
        {

            get
            {
                return _MessageToken;
            }
            set
            {
                _MessageToken = value;
            }

        }
        private StrokeCollection signStroke = new StrokeCollection();
        public StrokeCollection SignStroke
        {
            get { return signStroke; }
            set
            {
                signStroke = value;
                OnPropertyChanged("SignStroke");

            }
        }
        private bool IsNotPassedReasonEmpty = false;

        private string statusCode;
        public string StatusCode
        {
            get { return statusCode; }
            set { statusCode = value; OnPropertyChanged("StatusCode"); }
        }

        private VehicleNumber selectedVehicle;

        public VehicleNumber SelectedVehicle
        {
            get { return selectedVehicle; }
            set { selectedVehicle = value; OnPropertyChanged("SelectedVehicle"); }
        }
        //public int SelectedVehicle
        //{
        //    get { return selectedVehicle; }
        //    set 
        //    { 
        //        selectedVehicle = value;
        //        OnPropertyChanged("SelectedVehicle"); }
        //}

        private bool isSwitchDisable = false;

        public bool IsSwitchDisable
        {
            get { return isSwitchDisable; }
            set { isSwitchDisable = value; OnPropertyChanged("IsSwitchDisable"); }
        }

        private bool isSignButtonVisible = false;

        public bool IsSignButtonVisible
        {
            get { return isSignButtonVisible; }
            set { isSignButtonVisible = value; OnPropertyChanged("IsSignButtonVisible"); }
        }

        private bool isPrintButtonVisible = false;
        public bool IsPrintButtonVisible
        {
            get { return isPrintButtonVisible; }
            set { isPrintButtonVisible = value; OnPropertyChanged("IsPrintButtonVisible"); }
        }

        private bool isKeyboardToShow = true;
        public bool IsKeyboardToShow
        {
            get { return isKeyboardToShow; }
            set { isKeyboardToShow = value; OnPropertyChanged("IsKeyboardToShow"); }
        }

        #endregion

        #region Delegatecommands
        public DelegateCommand ConfirmationPopUp { get; set; }
        public DelegateCommand AcceptAndPrint { get; set; }
        public DelegateCommand ResetAllFields { get; set; }
        public DelegateCommand PropertyLostFocus { get; set; }
        public DelegateCommand ClearSign { get; set; }
        public DelegateCommand PrintAllDetails { get; set; }

        #endregion

        #region MyRegion
        public string Error
        {
            get
            {
                return null;
                //throw new NotImplementedException(); 
            }
        }

        public string this[string columnName]
        {
            get
            {
                string result = string.Empty;
                if (columnName == "NotPassedReason" && IsNotPassedReasonEmpty)
                {
                    result = Helpers.Constants.PreTripInspection.NotPassedReasonErrorMessage;
                }
                return result;
            }
        }
        #endregion

        #region Constructor
        public PreTripInspectionViewModel(Guid token, PreTripVehicleInspection preTripobject)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionViewModel][Start:Constructor]");
            if (preTripobject != null)
            {
                InitializeCommands();
                InspectionModel = preTripobject;
                GetAllDetailsForPreTrip(InspectionModel);
                IsSignButtonVisible = false; IsPrintButtonVisible = true;
            }
            else
            {
                IsCanvasEnable = false;

                TodayDate = (DateTime.Now.ToString("dddd', 'MM'/'dd'/'yyyy "));
                Route = PayloadManager.ApplicationPayload.Route;
                this.MessageToken = token;
                ToggleSignButton = false;
                IsSignButtonVisible = true; IsPrintButtonVisible = false;
                InitializeCommands();
                GetVehicleDetails();
                GetInspectionList();
                SaveTemplateMessage();
                foreach (PreTripInspectionFormList item in inspectionFormList)
                {
                    item.PropertyChanged += item_PropertyChanged;
                    // InspectionFormList. += InspectionFormList_CollectionChanged;
                }
                IsKeyboardToShow = true;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:Constructor]");
        }

        void item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

            try
            {
                IsDirty = true;
                CheckForValidation();
            }
            catch (Exception)
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region Methods

        void InitializeCommands()
        {
            #region ConfirmationPopUp
            ConfirmationPopUp = new DelegateCommand((param) =>
            {
                //RenderSign();
                IsCanvasEnable = true;
                LoadConfirmationStatements();
                var dialog = new Helpers.DialogWindow { TemplateKey = "AcceptAndConfirm", Title = "Certify Vehicle Inspection" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });
            #endregion

            #region AcceptAndPrint
            AcceptAndPrint = new DelegateCommand((param) =>
            {
                try
                {
                    //Byte[] sign = CreateCanvasToImage();
                    IsCanvasEnable = false;
                    VehicleNumber vehicle = new Entities.VehicleNumber();
                    // InspectionModel = new PreTripVehicleInspection();
                    InspectionModel.VehicleModel = VehicleModel;
                    foreach (VehicleNumber item in VehicleNumber)
                    {
                        if (item.VehicleID == SelectedVehicleNumber)
                        {
                            InspectionModel.VehicleNumber = item.VehicleNo;
                            vehicle.VehicleNo = item.VehicleNo;
                            vehicle.VehicleID = item.VehicleID;
                            break;
                        }
                    }
                    // vehicle.VehicleNo = SelectedVehicleNumber;

                    InspectionModel.RouteID = Route;
                    InspectionModel.TemplateID = TemplateID;
                    InspectionModel.OdometerReading = OdometerReading.Trim();
                    InspectionModel.AdditionalComments = string.IsNullOrEmpty(AdditionalComments) ? "" : AdditionalComments.Trim();
                    InspectionModel.PreTripDateTime = DateTime.Now;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        SignStroke.Save(ms);
                        InspectionModel.VerificationSignature = ms.ToArray();
                    }
                    CreateCanvasToImage();
                    InspectionModel.VehicleNo.Add(vehicle);
                    InspectionModel.QuestionList = InspectionFormList;
                    Managers.PreTripInspectionManager inspectionManager = new PreTripInspectionManager();
                    int inspectionID = inspectionManager.AddVehicleInspectionDetails(InspectionModel, InspectionFormList, Route);
                    if (inspectionID < 0)
                    {
                        Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow() { Message = Helpers.Constants.PreTrip.SaveErrorDetailInPretripInspection }, MessageToken);
                        return;
                    }
                    else
                    {
                        InspectionModel.PreTripInspectionID = inspectionID.ToString().Trim();
                    }


                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                    PreTripInspectionManager inspectionM = new PreTripInspectionManager();

                    Dictionary<int, string> dict = inspectionM.CheckVerificationSignature(Route);
                    int verifiedSign = dict.Keys.FirstOrDefault();
                    if (verifiedSign > 0)
                    {
                        bool flag = false;
                        if (InspectionModel.StatusCode == "PASS")
                        {
                            flag = true;
                        }
                        var PrintDirectly = true;
                        if (PrintDirectly)
                        {
                            PrintPreTripInspectionForm(InspectionModel);

                        }
                        //var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, Refresh = true, CurrentViewName = ViewModelMappings.View.PreTripVehicleInspection, CloseCurrentView = true, Payload = flag, ShowAsModal = false };
                        //Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                        //var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ServiceRoute, Refresh = true, CurrentViewName = ViewModelMappings.View.PreTripVehicleInspection, CloseCurrentView = true, Payload = flag, ShowAsModal = false };
                        //Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    }
                    //ResourceManager.CommonNavInfo.StartSync.Execute(null);
                    CommonNavInfo.IsManuallySyncAllProfiles = true;
                }
                catch (Exception ex)
                {

                    Logger.Error("[SalesLogicExpress.Application.ViewModels][PreTripInspectionViewModel][AcceptAndPrint DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }

            });
            #endregion

            #region ResetAllFields
            ResetAllFields = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    VehicleNumber obj = (VehicleNumber)param;
                    SelectedVehicleNumber = obj.VehicleID;
                    ResetAllFieldsOnClick();

                }
                //PreTripInspectionFormList inspectionList = new PreTripInspectionFormList();
            });
            #endregion

            #region PropertyLostFocus
            //PropertyLostFocus = new DelegateCommand((param) =>
            //{
            //    if (true)
            //    {

            //    }
            //});
            #endregion

            PrintAllDetails = new DelegateCommand((param) =>
            {
                PrintPreTripInspectionForm(InspectionModel);
            });

            ClearSign = new DelegateCommand((param) =>
            {
                if (SignStroke != null)
                {
                    SignStroke.Clear();
                }
            });
        }

        public void PrintPreTripInspectionForm(PreTripVehicleInspection InspectionModel)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:PrintPreTripInspectionForm]");
            try
            {
                var DocInfo = new ReportEngine.DocumentInformation();
                var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                repEngine.Notify += repEngine_Notify;

                repEngine.Log = this.Logger;
                ReportEngine.IReportSLE ReportData = null;
                var DocumentId = InspectionModel.DocumentId;
                var SourceDocumentNo = Convert.ToDecimal(InspectionModel.PreTripInspectionID);
                if (DocumentId <= 0)
                {
                    ReportData = new ReportViewModels.PreTripInspectionReport(InspectionModel);
                    DocInfo = repEngine.Generate<Reporting.PreTripInspection>(SourceDocumentNo, true, ReportEngine.ReportCategory.Route, ReportEngine.ReportsTypes.PreTripInspection, ReportData, null, ReportEngine.ContentTypes.PDF);
                }
                else
                {
                    DocInfo.DocumentId = DocumentId;
                }

                if (DocInfo.IsDocumentCreated)
                    ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.PreTrip_Inspection_Header", "PreTripInspectionHeaderId", SourceDocumentNo, DocInfo.DocumentId);
                InspectionModel.DocumentId = DocInfo.DocumentId; //Replace Latest

                // Report Regeneration - Process for Missing Reports
                // Report-ReGeneration Implementation by Velmani Karnan (11/29/2016)
                var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Route, InspectionModel.DocumentId, null);
                if (ReportBytes == null)
                {
                    DocumentId = 0;
                    if (DocumentId <= 0)
                    {
                        ReportData = new ReportViewModels.PreTripInspectionReport(InspectionModel);
                        DocInfo = repEngine.Generate<Reporting.PreTripInspection>(SourceDocumentNo, true, ReportEngine.ReportCategory.Route, ReportEngine.ReportsTypes.PreTripInspection, ReportData, null, ReportEngine.ContentTypes.PDF);
                    }
                    else
                    {
                        DocInfo.DocumentId = DocumentId;
                    }
                    if (DocInfo.IsDocumentCreated)
                        ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.PreTrip_Inspection_Header", "PreTripInspectionHeaderId", SourceDocumentNo, DocInfo.DocumentId);
                    InspectionModel.DocumentId = DocInfo.DocumentId;
                }

                SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
                Payload.ReportFile = ReportBytes;
                Payload.CurrentView = ViewModelMappings.View.RouteHome;
                Payload.PreviousView = ViewModelMappings.View.RouteHome;
                Payload.CurrentViewTitle = "Pre-Inspection Trip Report";
                Payload.PreviousViewTitle = "PreInspection Trip";
                Payload.MessageToken = this.MessageToken;

                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                //  SalesLogicExpress.Application.ViewModels.CommonNavInfo.SelectTab = ViewModelMappings.TabView.RouteHome.PreTrip;
                repEngine.PrintDirectly(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Route, DocInfo.DocumentId, null, true);


                //var moveToView = new Helpers.NavigateToView
                //{
                //    NextViewName = ViewModelMappings.View.AppPDfWindow,
                //    CurrentViewName = ViewModelMappings.View.ServiceRoute,
                //    CloseCurrentView = false,
                //    Payload = Payload,
                //    ShowAsModal = false
                //};

                //Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                var moveToView = new Helpers.NavigateToView
                {
                    NextViewName = ViewModelMappings.View.RouteHome,
                    Refresh = true,
                    CurrentViewName = ViewModelMappings.View.PreTripVehicleInspection,

                    CloseCurrentView = true,
                    Payload = Payload,
                    ShowAsModal = false
                };

                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);

                //new ReportViewModels.PreTripInspectionReport(InspectionModel);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PreTripInspectionViewModel][PrintPreTripInspectionForm][ExceptionStackTrace=" + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:PrintPreTripInspectionForm]");
        }

        void repEngine_Notify(string msg)
        {
            System.Windows.MessageBox.Show(msg);
        }

        private void RenderSign()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:RenderSign]");
            SignatureCanvasVM = new CanvasViewModel(Flow.PreTripInspection, SignType.PreTripInspection, Convert.ToInt32(InspectionModel.PreTripInspectionID))
            {
                Height = 250,
                Width = 300,
                FileName = "PreTripInspection.jpg",
                FooterText = "Customer's Signature"
            };
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:RenderSign]");
        }
        public void GetAllDetailsForPreTrip(PreTripVehicleInspection preTripObj)
        {
            // throw new NotImplementedException();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:GetAllDetailsForPreTrip]");
            InspectionModel.IsDetailsShown = false;
            try
            {
                Managers.PreTripInspectionManager inspectionM = new PreTripInspectionManager();
                InspectionFormList = inspectionM.GetDetailsForPreTrip(preTripObj);
                InspectionModel.QuestionList = InspectionFormList;
                SelectedVehicle = new VehicleNumber();
                //foreach (PreTripInspectionFormList item in InspectionFormList)
                //{
                //    if (item.ResponseID == 1)
                //    {
                //        item.IsYesChecked = true;
                //    }
                //    else item.IsNoChecked = true;
                //    item.IsCheckboxEnabled = false;
                //    item.IsNoPassedReasonEnabled = false;
                //}
                VehicleModel = InspectionModel.VehicleModel;
                SelectedVehicle.VehicleNo = InspectionModel.VehicleNumber;
                OdometerReading = InspectionModel.OdometerReading;
                AdditionalComments = string.IsNullOrEmpty(InspectionModel.AdditionalComments) ? "" : InspectionModel.AdditionalComments.Trim();
                PreTripVehicleInspection vehicleDetails = inspectionM.GetVehicleDetails(Route, string.Empty);
                VehicleNumber = new ObservableCollection<VehicleNumber>();
                foreach (var item in vehicleDetails.VehicleNo)
                {
                    if (item.VehicleNo == SelectedVehicle.VehicleNo)
                    {
                        SelectedVehicle.VehicleID = item.VehicleID;
                        VehicleNumber.Add(item);
                    }

                }
                IsDirty = false;
                IsKeyboardToShow = false;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PreTripInspectionViewModel][GetAllDetailsForPreTrip][ExceptionStackTrace=" + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:GetAllDetailsForPreTrip]");
        }
        public void GetVehicleDetails()
        {
            VehicleNumber = null;
            VehicleModel = null;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:GetVehicleDetails]");
            try
            {
                Managers.PreTripInspectionManager preTripInspectionM = new PreTripInspectionManager();
                PreTripVehicleInspection vehicleDetails = preTripInspectionM.GetVehicleDetails(Route, string.Empty);
                VehicleModel = vehicleDetails.VehicleModel;
                VehicleNumber = new ObservableCollection<VehicleNumber>();
                foreach (var item in vehicleDetails.VehicleNo)
                {
                    VehicleNumber.Add(item);
                }
                SelectedVehicle = VehicleNumber.ElementAt(0);
                SelectedVehicleNumber = SelectedVehicle.VehicleID;

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PreTripInspectionViewModel][GetVehicleDetails][ExceptionStackTrace=" + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:GetVehicleDetails]");
        }
        public void GetInspectionList()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:GetInspectionList]");
            InspectionFormList = new ObservableCollection<PreTripInspectionFormList>();
            InspectionFormListTemplate = new ObservableCollection<PreTripInspectionFormList>();
            Managers.PreTripInspectionManager inspectionManager = new PreTripInspectionManager();
            InspectionFormList = inspectionManager.GetInspectionList();
            InspectionFormListTemplate = InspectionFormList.Clone();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:GetInspectionList]");
        }
        public void SaveTemplateMessage()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:SaveTemplateMessage]");
            Message = Helpers.Constants.PreTripInspection.SaveTemplateOnBackNavigation;
            OkContent = "Ok";
            CancelContent = "Cancel";
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:SaveTemplateMessage]");
        }
        public void ResetAllFieldsOnClick()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:ResetAllFieldsOnClick]");
            try
            {
                InspectionModel = new PreTripVehicleInspection();
                //InspectionFormList = InspectionFormListTemplate.Clone();
                for (int i = 0; i < InspectionFormList.Count; i++)
                {
                    InspectionFormList[i].IsYesChecked = false;
                    InspectionFormList[i].IsNoChecked = false;
                    InspectionFormList[i].NotPassedReason = string.Empty;
                }
                InspectionModel.OdometerReading = string.Empty;
                InspectionModel.AdditionalComments = string.Empty;
                OdometerReading = string.Empty;
                AdditionalComments = string.Empty;
                IsDirty = false;
                ToggleSignButton = false;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PreTripInspectionViewModel][ResetAllFieldsOnClick][ExceptionStackTrace=" + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:ResetAllFieldsOnClick]");
        }
        public override bool ConfirmSave()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:ConfirmSave]");
            try
            {
                //int result = Managers.PreTripInspectionManager.AddVehicleInspectionDetails(InspectionModel, InspectionFormList, Route);
                ResetAllFieldsOnClick();
                IsCanvasEnable = false;
                CanNavigate = true;
                IsDirty = false;

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PreTripInspectionViewModel][ConfirmSave][ExceptionStackTrace=" + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:ConfirmSave]");
            return base.ConfirmSave();
        }
        public override bool ConfirmCancel()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:ConfirmCancel]");
            //IsDirty = false;
            CanNavigate = false;
            IsCanvasEnable = false;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:ConfirmCancel]");
            return base.ConfirmCancel();
        }
        public override bool ConfirmClosed()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:ConfirmClosed]");
            IsCanvasEnable = false;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:ConfirmClosed]");
            return base.ConfirmClosed();
        }
        public void LoadConfirmationStatements()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:LoadConfirmationStatements]");
            ConfirmationStatement_1 = Helpers.Constants.PreTripInspection.ConfirmationStatement_1;
            ConfirmationStatement_2 = Helpers.Constants.PreTripInspection.ConfirmationStatement_2;
            ConfirmationStatement_3 = Helpers.Constants.PreTripInspection.ConfirmationStatement_3;
            SignStroke = new StrokeCollection();
            SignStroke.StrokesChanged += SignStroke_StrokesChanged;

            MyCanvas = new InkCanvas();

            ToggleAcceptandSignButton = false;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:LoadConfirmationStatements]");
        }
        public void CheckForValidation()
        {
            int yesCounter = 0;
            int noCounter = 0;
            IsDirty = false;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:CheckForValidation]");
            try
            {

                foreach (PreTripInspectionFormList item in InspectionFormList)
                {
                    if (item.IsYesChecked)
                        yesCounter++;
                    else if (item.IsNoChecked)
                        noCounter++;
                }
                if ((OdometerReading.Length > 0) || (yesCounter + noCounter > 0))
                {
                    IsDirty = true;
                }
                if (OdometerReading.Length <= 1)
                {
                    ToggleSignButton = false; return;
                }

                if (yesCounter == 10)
                {
                    InspectionModel.StatusCode = "PASS";
                }
                else if (yesCounter < 10)
                {
                    InspectionModel.StatusCode = "FAIL";
                }


                if (yesCounter + noCounter == InspectionFormList.Count)
                {
                    // int noPassedCount = InspectionFormList.Sele1ct(x => x.IsNoChecked == true && string.IsNullOrEmpty(x.NotPassedReason.Trim())).Count();
                    int noPassedCount = 0;
                    foreach (PreTripInspectionFormList item in InspectionFormList)
                    {
                        if (item.IsNoChecked && string.IsNullOrEmpty(item.NotPassedReason.Trim()))
                        {
                            noPassedCount++;
                            break;
                        }
                    }
                    if (noPassedCount > 0)
                    {
                        ToggleSignButton = false;
                        OnPropertyChanged("NotPassedReason");
                    }
                    else
                        ToggleSignButton = true;
                }
                else
                {
                    ToggleSignButton = false; return;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PreTripInspectionViewModel][CheckForValidation][ExceptionStackTrace=" + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:CheckForValidation]");
        }
        //public void CheckForIsDirty()
        //{

        //}
        void SignStroke_StrokesChanged(object sender, StrokeCollectionChangedEventArgs e)
        {
            //throw new NotImplementedException();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:SignStroke_StrokesChanged]");
            if (SignStroke.Count > 0)
            {
                ToggleAcceptandSignButton = true;
            }
            else
            {
                ToggleAcceptandSignButton = false;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:SignStroke_StrokesChanged]");
        }
        public void CreateCanvasToImage()
        {
            //Byte[] bytes = null;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][Start:CreateCanvasToImage]");
            try
            {
                string savedFilePath = "";
                //MyCanvas.Strokes = SignStroke;
                MyCanvas = SalesLogicExpress.Application.ViewModelPayload.PayloadManager.RouteSettlementPayload.MyCanvasPayload;
                InkCanvas canvas = MyCanvas;
                canvas.Strokes = SignStroke;

                string fileName = ResourceManager.DomainPath + @"\Docs\" + "PreTripInspection.jpg";
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }
                System.IO.Directory.CreateDirectory(ResourceManager.DomainPath + @"\Docs");

                //RenderTargetBitmap rtb = new RenderTargetBitmap(290, 275, 0, 0, PixelFormats.Pbgra32);
                RenderTargetBitmap rtb = new RenderTargetBitmap((int)canvas.ActualWidth - 10, (int)canvas.ActualHeight, 0, 0, PixelFormats.Pbgra32);
                rtb.Render(canvas);
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(rtb));
                FileStream file = new FileStream(fileName, FileMode.Create);
                encoder.Save(file);
                savedFilePath = file.Name;
                file.Close();


                //FileStream fs = new FileStream(savedFilePath, FileMode.Open, FileAccess.Read);
                //BinaryReader br = new BinaryReader(fs);
                //bytes = br.ReadBytes((Int32)fs.Length);
                //br.Close();
                //fs.Close();
                //File.Delete(savedFilePath);
                //MyCanvas.Strokes = new StrokeCollection();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PreTripInspectionViewModel][CreateCanvasToImage][ExceptionStackTrace=" + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreTripInspectionHistoryViewModel][End:CreateCanvasToImage]");
        }

        //public Byte[] CreateCanvasToImage()
        //{
        //    Byte[] bytes = Convert.ToByte((Int32)1095);
        //    return bytes;

        //}
        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}