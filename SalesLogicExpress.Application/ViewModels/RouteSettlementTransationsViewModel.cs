﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using log4net.Repository.Hierarchy;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class RouteSettlemenntTransationsViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.RouteSettlemenntTransationsViewModel");

        #region Variable and Object declaration

        private double totalCash = 0;
        private double totalCheck = 0;
        private double totalExpenses = 0;
        private double totalMoneyOrder = 0;

        private double grandTotal = 0;
        private ObservableCollection<CheckDetails> checkDetailsCollection;
        private ObservableCollection<MoneyOrderDetails> moneyOrderDetailsCollection;

        private bool _ShowAllActivities = false;
        #endregion

        #region Constructor

        public RouteSettlemenntTransationsViewModel()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlemenntTransationsViewModel][Start:Constructor]");
            try
            {
                Transactions = new ObservableCollection<object>();
                IsBusy = true;
                this.InitializeCommands();
                LoadTransactions();
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][RouteSettlemenntTransationsViewModel][Constructor][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlemenntTransationsViewModel][End:Constructor]");
        }
        #endregion

        #region Properties

        public DelegateCommand ShowActivity { get; set; }

        /// <summary>
        /// Get or set the flag to show all activities 
        /// </summary>
        public bool ShowAllActivities
        {
            get { return _ShowAllActivities; }
            set
            {
                _ShowAllActivities = value;
                OnPropertyChanged("ShowAllActivities");
                this.LoadTransactions();
            }
        }

        public double TotalCash
        {
            get { return totalCash; }
            set
            {
                totalCash = value; OnPropertyChanged("TotalCash");
                PayloadManager.RouteSettlementPayload.TotalCash = value;
            }
        }

        private double voidedExpense = 0.00;

        public double VoidedExpense
        {
            get { return voidedExpense; }
            set { voidedExpense = value; }
        }

        private double voidedMO = 0.00;
        public double VoidedMO
        {
            get { return voidedMO; }
            set { voidedMO = value; }
        }
        public double TotalCheck
        {
            get { return totalCheck; }
            set { totalCheck = value; OnPropertyChanged("TotalCheck"); }
        }

        public double TotalExpenses
        {
            get { return totalExpenses; }
            set { totalExpenses = value; OnPropertyChanged("TotalExpenses"); }
        }

        public double TotalMoneyOrder
        {
            get { return totalMoneyOrder; }
            set { totalMoneyOrder = value; OnPropertyChanged("TotalMoneyOrder"); }
        }

        public Guid MessageToken { get; set; }

        public ObservableCollection<object> Transactions { get; set; }

        public double GrandTotal
        {
            get { return grandTotal; }
            set { grandTotal = value; OnPropertyChanged("GrandTotal"); }
        }

        public ObservableCollection<CheckDetails> CheckDetailsCollection
        {
            get { return checkDetailsCollection; }
            set { checkDetailsCollection = value; OnPropertyChanged("CheckDetailsCollection"); }
        }

        public ObservableCollection<MoneyOrderDetails> MoneyOrderDetailsCollection
        {
            get { return moneyOrderDetailsCollection; }
            set { moneyOrderDetailsCollection = value; OnPropertyChanged("MoneyOrderDetailsCollection"); }
        }

        #endregion

        #region Methods

        private void InitializeCommands()
        {
            ShowActivity = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlemenntTransationsViewModel][ShowActivity][START]");
                try
                {
                    if (param == null) return;

                    if (param.GetType() == typeof(PaymentTransaction))
                    {
                        PaymentTransaction objPaymentTransaction = (PaymentTransaction)param;
                        PayloadManager.PaymentPayload.ReceiptId = objPaymentTransaction.PaymentNo;
                        CustomerManager objCustomerManager = new CustomerManager();
                        ObservableCollection<Customer> customerList = objCustomerManager.GetCustomersForRoute(CommonNavInfo.RouteID.ToString());
                        Customer customer = customerList.Where(o => o.CustomerNo == objPaymentTransaction.BillTo.Substring(0, objPaymentTransaction.BillTo.IndexOf(','))).FirstOrDefault<Customer>();

                        objCustomerManager.Dispose();
                        objCustomerManager = null;

                        if (customer == null)
                            return;
                        else

                            //************************************************************************************************
                            // Comment: Passed the values to payment customer and Stopdate data.
                            // Created: Jan 28, 2016
                            // Author: Vivensas (Rajesh,Yuvaraj)
                            // Revisions: 
                            //*************************************************************************************************
                            PayloadManager.PaymentPayload.Customer = customer;
                        PayloadManager.PaymentPayload.Customer.StopDate = PayloadManager.OrderPayload.StopDate;
                        //*************************************************************************************************
                        // Vivensas changes ends over here
                        //**************************************************************************************************
                        CommonNavInfo.SetSelectedCustomer(customer);

                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = ViewModelMappings.View.RouteSettlement.GetEnumDescription();
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = ViewModelMappings.View.ARPayment.GetEnumDescription();
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteSettlement;
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ARPayment;
                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ARPayment, CurrentViewName = ViewModelMappings.View.RouteSettlement, CloseCurrentView = false, Payload = null, ShowAsModal = false };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    }
                    //Below code is commented for CR. Drill down behavior in EOD transaction tab
                    /* 
                     else if (param.GetType() == typeof(QuoteTransaction))
                     {
                         QuoteTransaction objQuoteTransaction = (QuoteTransaction)param;

                         ViewModelPayload.PayloadManager.QuotePayload.QuoteId = objQuoteTransaction.QuoteId;
                         PayloadManager.ProspectPayload.IsProspect = objQuoteTransaction.IsProspect;
                         if (objQuoteTransaction.IsProspect)
                         {
                             ObservableCollection<ProspectModel> ProspectList = new ProspectManager().GetProspectsForRoute(CommonNavInfo.RouteID.ToString());
                             ProspectModel prospect = ProspectList.Where(o => o.ProspectId == objQuoteTransaction.ProspectOrCustId).FirstOrDefault<ProspectModel>();
                             if (prospect == null)
                                 return;
                             else
                                 PayloadManager.ProspectPayload.Prospect = prospect;
                             PayloadManager.ProspectPayload.IsProspect = true;
                             SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = ViewModelMappings.View.ProspectQuote.GetEnumDescription();
                             SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = ViewModelMappings.View.RouteSettlement.GetEnumDescription();
                             ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.EOD_Transaction.ToString();

                             ViewModelPayload.PayloadManager.ProspectPayload.ShowEditModeButton = true;
                             var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ProspectQuote, CurrentViewName = ViewModelMappings.View.RouteSettlement, CloseCurrentView = false, ShowAsModal = false };
                             Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                         }
                         else
                         {
                             ObservableCollection<Customer> customerList = new CustomerManager().GetCustomersForRoute(CommonNavInfo.RouteID.ToString());
                             Customer customer = customerList.Where(o => o.CustomerNo == objQuoteTransaction.ProspectOrCustId).FirstOrDefault<Customer>();

                             if (customer == null)
                                 return;
                             else
                                 CommonNavInfo.SetSelectedCustomer(customer);
                             PayloadManager.ProspectPayload.IsProspect = false;
                             ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.EOD_Transaction.ToString();

                             var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerQuote, CurrentViewName = ViewModelMappings.View.RouteSettlement, CloseCurrentView = true, ShowAsModal = false };
                             Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                         }
                     }
                     else if (param.GetType() == typeof(CreditMemoTransaction))
                     {
                         CreditMemoTransaction objCreditTransaction = (CreditMemoTransaction)param;
                         #region Set Common
                         ObservableCollection<Customer> customerList = new CustomerManager().GetCustomersForRoute(CommonNavInfo.RouteID.ToString());
                         Customer customer = customerList.Where(o => o.CustomerNo == objCreditTransaction.CustomerId).FirstOrDefault<Customer>();

                         if (customer == null)
                             return;
                         else
                             CommonNavInfo.SetSelectedCustomer(customer);
                         PayloadManager.ProspectPayload.IsProspect = false;
                         PayloadManager.OrderPayload.Customer = customer;
                         ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.EOD_Transaction.ToString();
                         #endregion

                         var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.RouteSettlement, CloseCurrentView = false, Payload = null, ShowAsModal = false };
                         Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                     }
                     else if (param.GetType() == typeof(OrderReturnedTransaction))
                     {
                         #region Implementation Logic
                         OrderReturnedTransaction data = (OrderReturnedTransaction)param;
                         #region Set Common
                         ObservableCollection<Customer> customerList = new CustomerManager().GetCustomersForRoute(CommonNavInfo.RouteID.ToString());
                         Customer customer = customerList.Where(o => o.CustomerNo == data.CustomerID).FirstOrDefault<Customer>();

                         if (customer == null)
                             return;
                         else
                             CommonNavInfo.SetSelectedCustomer(customer);
                         PayloadManager.ProspectPayload.IsProspect = false;
                         PayloadManager.OrderPayload.Customer = customer;
                         ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.EOD_Transaction.ToString();
                         #endregion
                         PayloadManager.ReturnOrderPayload = data.Payload;
                         // Check is only for today as activity can be performed only today, if visitee has physical stop today
                         if (data.Date < DateTime.Today.Date)
                         {
                             PayloadManager.ReturnOrderPayload.StopDateBeforeInvokingActivity = PayloadManager.OrderPayload.Customer.StopDate;
                             PayloadManager.ReturnOrderPayload.Customer = new CustomerDashboard().GetNewCustomer(data.Date, data.Payload.Customer);
                             PayloadManager.ReturnOrderPayload.IsCustomerLoadedForPastActivity = true;
                         }
                         else
                         {
                             PayloadManager.ReturnOrderPayload.Customer = PayloadManager.OrderPayload.Customer;
                         }
                         GetWarnPopupForPastActivity(data.Date, false, data.CustomerID);

                         PayloadManager.ReturnOrderPayload.IsFromActivityTab = true;
                         PayloadManager.ReturnOrderPayload.ROIsInProgress = false; PayloadManager.ReturnOrderPayload.PickIsInProgress = false;
                         var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = true, ShowAsModal = false };
                         Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
                         #endregion
                     }
                     */
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][RouteSettlemenntTransationsViewModel][ShowActivity][ExceptionStackTrace = " + ex.ToString() + "]");
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlemenntTransationsViewModel][ShowActivity][End]");
            });
        }

        public void LoadTransactions()
        {
            #region Load Transaction

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlemenntTransationsViewModel][LoadTransactions][START]");
            //DataSet ds = new DataSet();
            try
            {
                Transactions.Clear();
                string activityState = string.Empty;
                ObservableCollection<RouteSettlementModel> transactions = RouteSettlementManager.GetTransactions(this.ShowAllActivities);
                Type detailClassType = null;
                DataContractJsonSerializer serializer = null;
                MemoryStream stream = null;
                CheckDetailsCollection = new ObservableCollection<CheckDetails>();
                MoneyOrderDetailsCollection = new ObservableCollection<MoneyOrderDetails>();
                ViewModelPayload.PayloadManager.RouteSettlementPayload.ActivityIDList = new ObservableCollection<string>();
                ViewModelPayload.PayloadManager.RouteSettlementPayload.ReceiptList.Clear();
                ViewModelPayload.PayloadManager.RouteSettlementPayload.ExpenseList.Clear();
                ViewModelPayload.PayloadManager.RouteSettlementPayload.MoneyOrderList.Clear();

                if (PayloadManager.QuotePayload.QuoteIdList == null)
                {
                    ViewModelPayload.PayloadManager.QuotePayload.QuoteIdList = new List<string>();
                }

                if (PayloadManager.QuotePayload.ProspectQuoteIdList == null)
                {
                    ViewModelPayload.PayloadManager.QuotePayload.ProspectQuoteIdList = new List<string>();
                }

                ViewModelPayload.PayloadManager.QuotePayload.ProspectQuoteIdList.Clear();
                ViewModelPayload.PayloadManager.QuotePayload.QuoteIdList.Clear();
                string settlementId = string.Empty;
                bool isVerified = false;
                foreach (RouteSettlementModel trans in transactions)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(trans.ActivityID))
                            ViewModelPayload.PayloadManager.RouteSettlementPayload.ActivityIDList.Add(trans.ActivityID);

                        switch (!string.IsNullOrEmpty(trans.ActivityType) ? trans.ActivityType.Trim() : "")
                        {
                            case "Order":

                                #region Order
                                if (string.IsNullOrEmpty(trans.ActivityDetails)) break;
                                detailClassType = Type.GetType(trans.ActivityDetailClass.Trim());
                                if (detailClassType.Name == typeof(OrderPayload).Name)     // Log Error - Unable to cast object of type    -- Vignesh D
                                {
                                    ViewModelPayload.OrderPayload obj = (ViewModelPayload.OrderPayload)Activator.CreateInstance(detailClassType);

                                    serializer = new DataContractJsonSerializer(detailClassType);
                                    stream = new MemoryStream(Encoding.UTF8.GetBytes(trans.ActivityDetails));
                                    obj = (ViewModelPayload.OrderPayload)serializer.ReadObject(stream);

                                    stream.Dispose();
                                    stream.Close();

                                    OrderTransaction data = new OrderTransaction();
                                    data.Date = trans.ActivityEnd;
                                    data.Type = "ORD";
                                    data.InvoiceNo = obj.InvoiceNo;
                                    data.Amount = "-----";
                                    data.OrderAmount = "$" + (string.IsNullOrEmpty(obj.Amount) ? "0.00" : obj.Amount);
                                    data.FilterTypeName = "Order";
                                    data.BillTo = CustomerManager.GetCustomerBillTo(obj.Customer.CustomerNo) + ", " + CustomerManager.GetCustomerBillToName(obj.Customer.CustomerNo);
                                    data.ShipTo = obj.Customer.CustomerNo + ", " + obj.Customer.CustName.Trim();
                                    if (obj.Customer.PaymentMode != null)     // Log Error - Null handling    --Vignesh D 
                                        data.PaymentTerm = obj.Customer.PaymentMode.Trim();
                                    data.InvoiceType = "SO";
                                    data.SettlementId = (string.IsNullOrEmpty(trans.SettlementID) ? "" : trans.SettlementID);

                                    string strSwitch = string.IsNullOrEmpty(trans.ActivityStatus) ? "" : trans.ActivityStatus.Trim();

                                    switch (strSwitch)
                                    //switch (trans.ActivityStatus.Replace(" / ", string.Empty).ParseEnum<ActivityKey>(ActivityKey.None).ToString())
                                    {
                                        case "HoldAtDeliverToCustomer":
                                            activityState = "DELIVERY SCREEN";
                                            break;
                                        case "HoldAtCashCollection":
                                            activityState = "CASH DELIVERY";
                                            break;
                                        case "HoldAtOrderEntry":
                                            activityState = "CREATE ORDER";
                                            break;
                                        case "HoldAtPick":
                                            activityState = "PICK ORDER";
                                            break;
                                        case "VoidAtDeliverCustomer":
                                            activityState = "VOID";
                                            break;
                                        case "PreOrder":
                                            activityState = "PREORDER";
                                            break;
                                        case "VoidAtCashCollection":
                                            activityState = "VOID";
                                            break;
                                        case "VoidAtOrderEntry":
                                            activityState = "VOID";
                                            break;
                                        case "PickComplete":
                                            activityState = "PICK ORDER";
                                            break;
                                        case "OrderDelivered":
                                            activityState = "DELIVERED";
                                            break;
                                        case "PickItem":
                                            activityState = "PICK ORDER";
                                            break;
                                        case "PickOrder":
                                            activityState = "PICK ORDER";
                                            break;
                                        case "SettlementVerified":
                                            activityState = "SETTLED";
                                            break;
                                        case "VOIDEDSETTLED":
                                            activityState = "VOIDED / SETTLED";
                                            break;
                                        default:
                                            break;
                                    }
                                    data.Status = activityState.Trim();

                                    Transactions.Add(data);
                                }
                                #endregion
                                break;
                            case "Payment":

                                #region Payment

                                if (string.IsNullOrEmpty(trans.ActivityDetails) || string.IsNullOrWhiteSpace(trans.ActivityDetails)) break;
                                detailClassType = Type.GetType(trans.ActivityDetailClass.Trim());
                                ViewModelPayload.PaymentPayload payloadTypeObj = (ViewModelPayload.PaymentPayload)Activator.CreateInstance(detailClassType);

                                serializer = new DataContractJsonSerializer(detailClassType);
                                stream = new MemoryStream(Encoding.UTF8.GetBytes(trans.ActivityDetails));
                                payloadTypeObj = (ViewModelPayload.PaymentPayload)serializer.ReadObject(stream);

                                stream.Dispose();
                                stream.Close();

                                PaymentTransaction data1 = new PaymentTransaction();
                                data1.Date = trans.ActivityEnd;
                                data1.Type = "PAY";
                                data1.PaymentNo = payloadTypeObj.TransactionID.Replace("receipt#", "");
                                data1.FilterTypeName = "Payment";
                                data1.InvoiceNo = payloadTypeObj.InvoiceNo;
                                data1.IsInvoiceNoVisible = data1.InvoiceNo.Count == 0 ? false : true;
                                data1.IsUnappliedAmntVisible = !data1.IsInvoiceNoVisible;
                                data1.PaymentType = payloadTypeObj.CashDelivery.PaymentMode ? "Check" : "Cash";
                                data1.BillTo = CustomerManager.GetCustomerBillTo(payloadTypeObj.Customer.CustomerNo) + ", " + CustomerManager.GetCustomerBillToName(payloadTypeObj.Customer.CustomerNo);
                                data1.ShipTo = payloadTypeObj.Customer.CustomerNo + ", " + payloadTypeObj.Customer.CustName.Trim();
                                data1.ChequeNo = payloadTypeObj.CashDelivery.PaymentMode ? payloadTypeObj.CashDelivery.ChequeNo : "";
                                data1.IsCheckVisible = string.IsNullOrEmpty(data1.ChequeNo) ? false : true;
                                if (string.IsNullOrEmpty(payloadTypeObj.CashDelivery.PaymentAmount))
                                {
                                    data1.Amount = "0.00";
                                }
                                else
                                {
                                    decimal amount = Convert.ToDecimal(payloadTypeObj.CashDelivery.PaymentAmount);
                                    data1.Amount = amount.ToString("F", System.Globalization.CultureInfo.InvariantCulture);
                                }

                                data1.UnAppliedAmount = (new ARPaymentManager(payloadTypeObj.Customer.CustomerNo, payloadTypeObj.Customer.Company)).GetReceiptUnAppliedAmt(payloadTypeObj.TransactionID.Replace("receipt#", "")).ToString();
                                //Get the unapplied amount 
                                if (!string.IsNullOrEmpty(data1.UnAppliedAmount))
                                    data1.UnAppliedAmount = "$" + data1.UnAppliedAmount;

                                if (!string.IsNullOrEmpty(trans.ActivityStatus))
                                {
                                    if (trans.ActivityStatus.ToLower().Contains("void"))
                                        data1.IsVoid = true;
                                    else
                                        data1.IsVoid = false;
                                }

                                data1.Status = string.IsNullOrEmpty(trans.ActivityStatus) ? "" : trans.ActivityStatus.ToLower();

                                //Check if settlement is verified or not
                                //If not verified, then update the cash/check amounts
                                if (string.IsNullOrEmpty(trans.SettlementID))
                                {
                                    isVerified = false;
                                }
                                else
                                {
                                    settlementId = DbEngine.ExecuteScalar("SELECT " + trans.SettlementID + " FROM BUSDTA.Route_Settlement WHERE ISNULL(Verifier,'')<>'' AND SettlementId=" + trans.SettlementID);
                                    isVerified = string.IsNullOrEmpty(settlementId) ? false : true;
                                }

                                //Update the check list and total cash if it is not settled
                                if (!isVerified)
                                {
                                    if (data1.IsCheckVisible)
                                    {
                                        CheckDetails objChk = new CheckDetails();
                                        objChk.Amount = "$" + data1.Amount;
                                        objChk.CheckNo = data1.ChequeNo;
                                        objChk.CustomerName = data1.BillTo;
                                        objChk.Date = data1.Date.ToString("MM/dd/yyyy");
                                        objChk.CheckID = payloadTypeObj.CashDelivery.ChequeNoId;
                                        if (!data1.IsVoid)
                                        {
                                            TotalCheck += Convert.ToDouble(data1.Amount);
                                            CheckDetailsCollection.Add(objChk);
                                        }
                                    }
                                    else
                                    {
                                        if (!data1.IsVoid)
                                        {
                                            TotalCash += Convert.ToDouble(data1.Amount);
                                        }
                                    }
                                }

                                switch (string.IsNullOrEmpty(trans.ActivityStatus) ? "" : trans.ActivityStatus.ToString().Trim())
                                {
                                    case "SettlementVerified":
                                        activityState = "SETTLED";
                                        data1.IsVoid = true;

                                        break;
                                    case "VOIDED / SETTLED":
                                        activityState = "VOIDED / SETTLED";
                                        //data.ActivityTime = activity.ActivityStart.ToString("MM/dd/yyyy hh:mm tt");
                                        break;
                                    case "PaymentVoid":
                                        activityState = "VOID";
                                        //data.ActivityTime = activity.ActivityStart.ToString("MM/dd/yyyy hh:mm tt");
                                        break;
                                    default:
                                        activityState = string.IsNullOrEmpty(trans.ActivityStatus) ? "" : trans.ActivityStatus.ToUpper();
                                        break;
                                }
                                data1.Status = activityState;
                                data1.SettlementId = (string.IsNullOrEmpty(trans.SettlementID) ? "" : trans.SettlementID);

                                Transactions.Add(data1);

                                ViewModelPayload.PayloadManager.RouteSettlementPayload.ReceiptList.Add(payloadTypeObj.TransactionID.Replace("receipt#", ""));
                                #endregion
                                break;

                            case "Expense":

                                #region Expenses

                                if (trans.ActivityDetails.Trim().Length == 0) break;

                                serializer = new DataContractJsonSerializer(typeof(Expenses));
                                stream = new MemoryStream(Encoding.UTF8.GetBytes(trans.ActivityDetails));
                                Expenses expenses = (Expenses)serializer.ReadObject(stream);

                                stream.Dispose();
                                stream.Close();

                                ExpensesTransaction data2 = new ExpensesTransaction();
                                data2.Date = expenses.ExpenseDate;
                                data2.Type = "EXP";
                                data2.Amount = expenses.ExpenseAmount;
                                data2.ExpenseType = expenses.ExpenseCategory;
                                data2.ExpenseDescription = expenses.Explanation;
                                data2.FilterTypeName = "Expenses";
                                data2.SettlementId = (string.IsNullOrEmpty(trans.SettlementID) ? "" : trans.SettlementID); ;

                                switch (string.IsNullOrEmpty(trans.ActivityStatus) ? "" : trans.ActivityStatus.Replace(" / ", string.Empty).ParseEnum<ActivityKey>(ActivityKey.None).ToString())
                                {
                                    case "CreateExpense":
                                        data2.Status = "UNSETTLED";
                                        break;

                                    case "VoidExpense":
                                        data2.Status = "VOID";
                                        break;
                                    case "SettlementVerified":
                                        data2.Status = "SETTLED";
                                        break;
                                    case "VOIDEDSETTLED":
                                        data2.Status = "VOIDED / SETTLED";
                                        break;
                                    default:
                                        break;
                                }

                                //settlementId = DbEngine.ExecuteScalar("SELECT " + trans.SettlementID + " FROM BUSDTA.Route_Settlement WHERE ISNULL(Verifier,'')<>'' AND SettlementId=" + trans.SettlementID);
                                isVerified = string.IsNullOrEmpty(expenses.RouteSettlementId) ? false : true;

                                if (data2.Status == "VOID")
                                {
                                    if (!isVerified)
                                    {
                                        VoidedExpense += Convert.ToDouble(data2.Amount);
                                    }
                                }
                                if (!isVerified)
                                {
                                    TotalExpenses += Convert.ToDouble(data2.Amount);
                                }

                                if (Convert.ToDecimal(data2.Amount) != Convert.ToDecimal(0))
                                {
                                    Transactions.Add(data2);
                                }

                                ViewModelPayload.PayloadManager.RouteSettlementPayload.ExpenseList.Add(expenses.ExpenseID);

                                #endregion
                                break;


                            case "MoneyOrder":

                                #region MoneyOrder
                                if (trans.ActivityDetails.Trim().Length == 0) break;


                                serializer = new DataContractJsonSerializer(typeof(MoneyOrder));
                                stream = new MemoryStream(Encoding.UTF8.GetBytes(trans.ActivityDetails));
                                MoneyOrder moneyOrder = (MoneyOrder)serializer.ReadObject(stream);

                                stream.Dispose();
                                stream.Close();

                                MoneyOrderTransaction data3 = new MoneyOrderTransaction();
                                data3.Date = trans.ActivityEnd;
                                data3.Type = "MO";
                                data3.Amount = moneyOrder.MoneyOrderAmount;
                                data3.MoneyOrderNo = moneyOrder.MoneyOrderNumber;
                                data3.MoneyOrerFeeAmnt = moneyOrder.FeeAmount;
                                data3.FilterTypeName = "Money Order";
                                data3.MoneyOrderId = moneyOrder.MoneyOrderID;
                                // TotalMoneyOrder += Convert.ToDouble(data3.Amount);

                                if (!string.IsNullOrEmpty(trans.ActivityStatus))
                                {
                                    if (trans.ActivityStatus == "CreateMoneyOrder" || trans.ActivityStatus == "EditMoneyOrder")
                                    {
                                        MoneyOrderDetails objMoney = new MoneyOrderDetails();
                                        objMoney.Amount = "$" + data3.Amount;
                                        objMoney.MoneyOrderNo = data3.MoneyOrderNo;
                                        objMoney.FeeAmount = "$" + data3.MoneyOrerFeeAmnt;
                                        objMoney.Date = data3.Date.ToString("MM/dd/yyyy");
                                        objMoney.MoneyOrderID = moneyOrder.MoneyOrderID;
                                        MoneyOrderDetailsCollection.Add(objMoney);
                                    }
                                }

                                data3.SettlementId = (string.IsNullOrEmpty(trans.SettlementID) ? "" : trans.SettlementID);
                                switch (string.IsNullOrEmpty(trans.ActivityStatus) ? "" : trans.ActivityStatus.Replace(" / ", string.Empty).ParseEnum<ActivityKey>(ActivityKey.None).ToString())
                                {
                                    case "CreateMoneyOrder":
                                        data3.Status = "UNSETTLED";
                                        break;

                                    case "VoidMoneyOrder":
                                        data3.Status = "VOID";
                                        break;
                                    case "SettlementVerified":
                                        data3.Status = "SETTLED";
                                        break;
                                    case "VOIDEDSETTLED":
                                        data3.Status = "VOIDED / SETTLED";
                                        break;
                                    default:
                                        break;
                                }
                                //settlementId = DbEngine.ExecuteScalar("SELECT " + trans.SettlementID + " FROM BUSDTA.Route_Settlement WHERE ISNULL(Verifier,'')<>'' AND SettlementId=" + trans.SettlementID);
                                isVerified = string.IsNullOrEmpty(moneyOrder.RouteSettlementId) ? false : true;
                                if (data3.Status == "VOID")
                                {
                                    if (!isVerified)
                                    {
                                        VoidedMO += Convert.ToDouble(data3.Amount);
                                    }
                                }
                                if (!isVerified)
                                {
                                    TotalMoneyOrder += Convert.ToDouble(data3.Amount);
                                }
                                if (Convert.ToDecimal(data3.Amount) != Convert.ToDecimal(0))
                                {
                                    Transactions.Add(data3);
                                }
                                ViewModelPayload.PayloadManager.RouteSettlementPayload.MoneyOrderList.Add(moneyOrder.MoneyOrderID);

                                #endregion
                                break;

                            case "HeldReturn":

                                #region HeldReturn
                                if (trans.ActivityDetails.Trim().Length == 0) break;
                                detailClassType = Type.GetType(trans.ActivityDetailClass.Trim());
                                HeldReturnLogActivity obj2 = new HeldReturnLogActivity();
                                serializer = new DataContractJsonSerializer(typeof(HeldReturnLogActivity));
                                stream = new MemoryStream(Encoding.UTF8.GetBytes(trans.ActivityDetails));
                                obj2 = (HeldReturnLogActivity)serializer.ReadObject(stream);

                                stream.Dispose();
                                stream.Close();

                                ReplenishmentTransaction dataRepln = new ReplenishmentTransaction();
                                dataRepln.Date = trans.ActivityEnd;
                                dataRepln.Type = "RTN";
                                dataRepln.FromBranch = obj2.FromBranch;
                                dataRepln.ToBranch = obj2.ToBranch;
                                dataRepln.Amount = "-----";
                                dataRepln.FilterTypeName = "RET";
                                dataRepln.RequedtedBy = obj2.RequedtedBy;
                                dataRepln.ReplnID = obj2.ReplnID;
                                dataRepln.ReplnType = obj2.ReplnType;

                                if (string.IsNullOrEmpty(trans.SettlementID))
                                {
                                    dataRepln.SettlementId = "";
                                }
                                else
                                {
                                    dataRepln.SettlementId = DbEngine.ExecuteScalar("SELECT " + trans.SettlementID + " FROM BUSDTA.Route_Settlement WHERE ISNULL(Verifier,'')<>'' AND SettlementId=" + trans.SettlementID);
                                }
                                dataRepln.Status = string.IsNullOrEmpty(dataRepln.SettlementId) ? "" : "SETTLED";
                                //dataRepln.Status = string.IsNullOrEmpty(trans.SettlementID.Trim()) ? "" : "SETTLED";
                                dataRepln.Status = string.IsNullOrEmpty(trans.SettlementID) ? "" : "SETTLED";
                                Transactions.Add(dataRepln);

                                #endregion
                                break;

                            case "CreditMemo":

                                #region CreditMemoEntry
                                if (trans.ActivityDetails.Trim().Length == 0) break;

                                serializer = new DataContractJsonSerializer(typeof(CreditProcess));
                                stream = new MemoryStream(Encoding.UTF8.GetBytes(trans.ActivityDetails));
                                CreditProcess creditProcess = (CreditProcess)serializer.ReadObject(stream);

                                stream.Dispose();
                                stream.Close();

                                CreditMemoTransaction creditMemo = new CreditMemoTransaction();
                                creditMemo.Amount = Convert.ToDecimal(creditProcess.CreditAmount).ToString("F2");
                                creditMemo.CreditReason = creditProcess.CreditReason;
                                creditMemo.VoidReason = creditProcess.VoidReason;
                                creditMemo.Note = creditProcess.CreditNote;
                                creditMemo.ReceiptID = creditProcess.ReceiptID.ToString().Trim();
                                creditMemo.Date = creditProcess.CreditMemoDate;
                                creditMemo.Type = "CRD";
                                creditMemo.FilterTypeName = "CRD";
                                creditMemo.CustomerId = trans.CustomerID;
                                creditMemo.BillTo = CustomerManager.GetCustomerBillTo(creditProcess.CustomerNo) + ", " + CustomerManager.GetCustomerBillToName(creditProcess.CustomerNo);
                                creditMemo.ShipTo = creditProcess.CustomerNo + ", " + creditProcess.CustomerName.Trim();
                                creditMemo.SettlementId = (string.IsNullOrEmpty(trans.SettlementID) ? "" : trans.SettlementID);
                                switch (string.IsNullOrEmpty(trans.ActivityStatus) ? "" : trans.ActivityStatus.Replace(" / ", string.Empty).ParseEnum<ActivityKey>(ActivityKey.None).ToString())
                                {
                                    case "CreateCreditMemo":
                                        creditMemo.Status = "UNSETTLED";
                                        break;

                                    case "VoidCreditMemo":
                                        creditMemo.Status = "VOID";
                                        break;
                                    case "SettlementVerified":
                                        creditMemo.Status = "SETTLED";
                                        break;
                                    case "VOIDEDSETTLED":
                                        creditMemo.Status = "VOIDED / SETTLED";
                                        break;
                                    default:
                                        break;
                                }
                                //creditMemo.ReceiptID = trans.
                                Transactions.Add(creditMemo);

                                ViewModelPayload.PayloadManager.RouteSettlementPayload.ReceiptList.Add(creditMemo.ReceiptID.Replace("receipt#", ""));

                                #endregion
                                break;

                            case "ReturnOrder":

                                #region Order Returned
                                if (trans.ActivityDetails.Trim().Length == 0) break;
                                detailClassType = Type.GetType(trans.ActivityDetailClass.Trim());
                                ViewModelPayload.ReturnOrderPayload obj1 = (ViewModelPayload.ReturnOrderPayload)Activator.CreateInstance(detailClassType);
                                serializer = new DataContractJsonSerializer(detailClassType);
                                stream = new MemoryStream(Encoding.UTF8.GetBytes(trans.ActivityDetails));
                                obj1 = (ViewModelPayload.ReturnOrderPayload)serializer.ReadObject(stream);

                                stream.Dispose();
                                stream.Close();

                                OrderReturnedTransaction dataReturn = new OrderReturnedTransaction();
                                dataReturn.Date = trans.ActivityEnd;
                                dataReturn.Type = "RTN";
                                dataReturn.ReturnOrderID = obj1.ReturnOrderID;
                                dataReturn.Amount = "-----";
                                //dataReturn.OrderAmount = "$" + obj1.OrderTotalAmount.ToString();
                                //below line added by Hari for when the order is returned then order total amount added with energy surcharge else no need to add energy surcharge.
                                decimal OrderTotalAmt = (obj1.IsReturnOrder == true) ? (obj1.OrderTotalAmount + obj1.EnergySurcharge) : obj1.OrderTotalAmount;
                                dataReturn.OrderAmount = "$" + OrderTotalAmt.ToString();
                                dataReturn.FilterTypeName = "RET";
                                dataReturn.BillTo = obj1.Customer.CustomerNo + ", " + obj1.Customer.CustName.Trim();
                                dataReturn.ShipTo = obj1.Customer.CustomerNo + ", " + obj1.Customer.CustName.Trim();
                                dataReturn.PaymentTerm = obj1.Customer.PaymentMode.Trim();
                                dataReturn.OrderType = "RO";
                                dataReturn.SettlementId = (string.IsNullOrEmpty(trans.SettlementID) ? "" : trans.SettlementID);
                                dataReturn.Payload = obj1;
                                dataReturn.ActivityStateKey = string.IsNullOrEmpty(trans.ActivityStatus) ? "" : trans.ActivityStatus;
                                if (string.IsNullOrEmpty(trans.SettlementID))
                                {
                                    dataReturn.SettlementId = "";
                                }
                                else
                                {
                                    dataReturn.SettlementId = DbEngine.ExecuteScalar("SELECT " + trans.SettlementID + " FROM BUSDTA.Route_Settlement WHERE ISNULL(Verifier,'')<>'' AND SettlementId=" + trans.SettlementID);
                                }
                                dataReturn.Status = string.IsNullOrEmpty(dataReturn.SettlementId) ? "" : "SETTLED";
                                dataReturn.CustomerID = obj1.Customer.CustomerNo;

                                // If Return Order - Void Display the Status
                                switch (string.IsNullOrEmpty(trans.ActivityStatus) ? "" : trans.ActivityStatus.Replace(" / ", string.Empty).ParseEnum<ActivityKey>(ActivityKey.None).ToString())
                                {
                                    case "VoidAtROEntry":
                                        dataReturn.Status = "VOID";
                                        break;
                                    case "VoidAtROPick":
                                        dataReturn.Status = "VOID";
                                        break;
                                    case "HoldAtROPick":
                                        dataReturn.Status = "HOLD";
                                        break;
                                    default:
                                        break;
                                }

                                Transactions.Add(dataReturn);
                                #endregion
                                break;

                            case "Quote":
                                #region Quote

                                if (trans.ActivityDetails.Trim().Length == 0) break;
                                detailClassType = Type.GetType(trans.ActivityDetailClass.Trim());
                                serializer = new DataContractJsonSerializer(detailClassType);
                                stream = new MemoryStream(Encoding.UTF8.GetBytes(trans.ActivityDetails));
                                QuoteActivityListItem quoteActivity = (QuoteActivityListItem)serializer.ReadObject(stream);

                                stream.Dispose();
                                stream.Close();

                                QuoteTransaction objtrans = new QuoteTransaction();
                                objtrans.Date = trans.ActivityEnd;
                                objtrans.QuoteId = quoteActivity.QuoteId;
                                objtrans.FilterTypeName = "QTE";
                                objtrans.IsPrinted = quoteActivity.IsPrinted;
                                objtrans.IsProspect = quoteActivity.IsProspect;
                                objtrans.ItemsCount = quoteActivity.ItemsCount;
                                objtrans.PriceSetup = quoteActivity.PriceSetup;
                                objtrans.ProspectOrCustId = quoteActivity.ProspectOrCustId;
                                objtrans.SettlementId = trans.SettlementID;
                                objtrans.Type = "QTE";
                                objtrans.FilterTypeName = "QTE";

                                if (string.IsNullOrEmpty(trans.SettlementID))
                                {
                                    objtrans.SettlementId = "";
                                }
                                else
                                {
                                    objtrans.SettlementId = DbEngine.ExecuteScalar("SELECT " + trans.SettlementID + " FROM BUSDTA.Route_Settlement WHERE ISNULL(Verifier,'')<>'' AND SettlementId=" + trans.SettlementID);
                                }
                                objtrans.Status = string.IsNullOrEmpty(quoteActivity.SettlementId) ? "" : "SETTLED";

                                Transactions.Add(objtrans);

                                if (string.IsNullOrEmpty(quoteActivity.SettlementId))
                                {
                                    if (quoteActivity.IsProspect)
                                    {
                                        PayloadManager.QuotePayload.ProspectQuoteIdList.Add(quoteActivity.QuoteId);
                                    }
                                    else
                                    {
                                        PayloadManager.QuotePayload.QuoteIdList.Add(quoteActivity.QuoteId);
                                    }
                                }

                                #endregion
                                break;

                            case "InvAdjustment":

                                #region Inventory Adjustment
                                if (trans.ActivityDetails.Trim().Length == 0) break;

                                serializer = new DataContractJsonSerializer(typeof(InventoryAdjustment));
                                stream = new MemoryStream(Encoding.UTF8.GetBytes(trans.ActivityDetails));
                                InventoryAdjustment invObj = (InventoryAdjustment)serializer.ReadObject(stream);

                                stream.Dispose();
                                stream.Close();

                                InventoryAdjustmentTransaction invTransaction = new InventoryAdjustmentTransaction();
                                invTransaction.Reason = invObj.SelectedReasonCode.ReasonCodeDescription;
                                invTransaction.AdjustmentQty = invObj.AdjustmentQuantity.ToString();
                                invTransaction.AdjustmentUOM = invObj.AdjustmentUOM;
                                invTransaction.ItemNumber = invObj.Item.ItemNumber;
                                invTransaction.ItemDescription = invObj.Item.ItemDescription;
                                invTransaction.Date = trans.ActivityEnd;
                                invTransaction.Amount = "-----";
                                invTransaction.Type = "INV";
                                invTransaction.FilterTypeName = "INV";
                                invTransaction.SettlementId = (string.IsNullOrEmpty(trans.SettlementID) ? "" : trans.SettlementID);

                                if (string.IsNullOrEmpty(trans.SettlementID))
                                {
                                    invTransaction.SettlementId = "";
                                }
                                else
                                {
                                    invTransaction.SettlementId = DbEngine.ExecuteScalar("SELECT " + trans.SettlementID + " FROM BUSDTA.Route_Settlement WHERE ISNULL(Verifier,'')<>'' AND SettlementId=" + trans.SettlementID);
                                }
                                invTransaction.Status = string.IsNullOrEmpty(invTransaction.SettlementId) ? "" : "SETTLED";
                                var flag = DbEngine.ExecuteScalar("select IsApproved from busdta.Inventory_Adjustment where InventoryAdjustmentId = " + invObj.InventoryAdjustmentID + "");

                                invTransaction.IsApproved = string.IsNullOrEmpty(flag) ? false : Convert.ToBoolean(flag);

                                Transactions.Add(invTransaction);
                                #endregion
                                break;
                            default:
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][RouteSettlemenntTransationsViewModel][LoadTransactions][ExceptionStackTrace = " + ex.StackTrace + "][Message = ]" + ex.Message);
                        continue;
                    }
                }
                TotalCash = TotalCash - (TotalExpenses + TotalMoneyOrder) + VoidedExpense + VoidedMO;
                TotalExpenses -= VoidedExpense;
                TotalMoneyOrder -= VoidedMO;
                GrandTotal = TotalCheck + TotalCash + TotalExpenses + TotalMoneyOrder;
                //if (Transactions.Count>0)
                //{
                //    Transactions.Select(x=>x)
                //}
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][RouteSettlemenntTransationsViewModel][LoadTransactions][ExceptionStackTrace = " + ex.StackTrace + "][Message = ]" + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlemenntTransationsViewModel][LoadTransactions][END]");
            IsBusy = false;
            #endregion
        }


        void GetWarnPopupForPastActivity(DateTime activityTime, bool isProspect, string customerNo)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlemenntTransationsViewModel][Start:GetWarnPopupForPastActivity]");
            if (activityTime.Date == DateTime.Today.Date)
                return;

            if (activityTime.Date < DateTime.Today.Date)
            {

                var alartMessage = new Helpers.AlertWindow { Message = Helpers.Constants.CustomerDashboard.AlertStatement, MessageIcon = "Alert" };
                Messenger.Default.Send<Helpers.AlertWindow>(alartMessage, CommonNavInfo.MessageToken);
                return;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlemenntTransationsViewModel][End:GetWarnPopupForPastActivity]");
        }

        //void SetCommonFields()
        //{

        //}
        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
    public class AllTransacions : ViewModelBase
    {
        public string Type { get; set; }
        public string FilterTypeName { get; set; }
        public string Amount { get; set; }
        public DateTime Date { get; set; }

        /// <summary>
        /// Get or set settlementId
        /// </summary>
        public string SettlementId { get; set; }

        /// <summary>
        /// Get or set the transaction status 
        /// </summary>
        public string Status { get; set; }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
    public class OrderTransaction : AllTransacions
    {
        public string ShipTo { get; set; }
        public string BillTo { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceType { get; set; }
        public string PaymentTerm { get; set; }
        public string OrderAmount { get; set; }
        private bool isVoid = false;

        public bool IsVoid
        {
            get { return isVoid; }
            set { isVoid = value; }
        }

    }
    public class PaymentTransaction : AllTransacions
    {
        public string ShipTo { get; set; }
        public string BillTo { get; set; }
        public List<string> InvoiceNo { get; set; }

        /// <summary>
        /// Get or set the payment no
        /// </summary>
        public string PaymentNo { get; set; }
        public string InvoiceNos
        {
            get
            {
                if (InvoiceNo != null)
                {
                    if (InvoiceNo.Count > 0)
                    {
                        for (int i = 0; i < InvoiceNo.Count; i++)
                        {
                            if (!InvoiceNo[i].Contains("/SO"))
                                InvoiceNo[i] = InvoiceNo[i] + "/SO";
                        }
                        return InvoiceNo.Aggregate((oldData, newData) => oldData + ", " + newData);
                    }
                }

                return "";
            }
        }
        public string PaymentType { get; set; }
        public string ChequeNo { get; set; }
        public string UnAppliedAmount { get; set; }

        private bool isVoid = false;

        public bool IsVoid
        {
            get { return isVoid; }
            set { isVoid = value; }
        }
        private bool isCheckVisible = false;

        public bool IsCheckVisible
        {
            get { return isCheckVisible; }
            set { isCheckVisible = value; }
        }
        private bool isInvoiceNoVisible = false;

        public bool IsInvoiceNoVisible
        {
            get { return isInvoiceNoVisible; }
            set { isInvoiceNoVisible = value; }
        }
        private bool isUnappliedAmntVisible = false;

        public bool IsUnappliedAmntVisible
        {
            get { return isUnappliedAmntVisible; }
            set { isUnappliedAmntVisible = value; }
        }

    }
    public class ExpensesTransaction : AllTransacions
    {
        public string ExpenseType { get; set; }
        public string ExpenseDescription { get; set; }

    }
    public class MoneyOrderTransaction : AllTransacions
    {
        public string MoneyOrderNo { get; set; }
        public string MoneyOrderId { get; set; }
        public string MoneyOrerFeeAmnt { get; set; }
    }
    public class ReplenishmentTransaction : AllTransacions
    {
        private string replnID = string.Empty;

        public string ReplnID
        {
            get { return replnID; }
            set { replnID = value; }
        }

        private string replnType = string.Empty;

        public string ReplnType
        {
            get { return replnType; }
            set { replnType = value; }
        }

        private string fromBranch = string.Empty;

        public string FromBranch
        {
            get { return fromBranch; }
            set { fromBranch = value; }
        }


        private string toBranch = string.Empty;

        public string ToBranch
        {
            get { return toBranch; }
            set { toBranch = value; }
        }

        private string requedtedBy = string.Empty;

        public string RequedtedBy
        {
            get { return requedtedBy; }
            set { requedtedBy = value; }
        }
    }
    public class CreditMemoTransaction : AllTransacions
    {
        public string ShipTo { get; set; }
        public string BillTo { get; set; }
        public string CreditReason { get; set; }
        public string Note { get; set; }
        public string ReceiptID { get; set; }
        public string VoidReason { get; set; }
        public string CustomerId { get; set; }
    }
    public class OrderReturnedTransaction : AllTransacions
    {
        public string ShipTo { get; set; }
        public string BillTo { get; set; }
        public string InvoiceNo { get; set; }
        public string OrderType { get; set; }
        public string PaymentTerm { get; set; }
        public string OrderAmount { get; set; }
        private bool isVoid = false;
        private int returnOrderID = 0;

        public int ReturnOrderID
        {
            get { return returnOrderID; }
            set { returnOrderID = value; }
        }

        public ViewModelPayload.ReturnOrderPayload Payload { get; set; }
        public string ActivityStateKey { get; set; }
        public string CustomerID { get; set; }
    }
    public class InventoryAdjustmentTransaction : AllTransacions
    {
        public string ItemNumber { get; set; }
        public string ItemDescription { get; set; }
        public string AdjustmentQty { get; set; }
        public string Reason { get; set; }
        public string AdjustmentUOM { get; set; }

        public bool IsApproved { get; set; }

    }

    public class QuoteTransaction : AllTransacions
    {
        public bool IsProspect { get; set; }
        public string QuoteId { get; set; }

        public bool IsPrinted { get; set; }

        public int ItemsCount { get; set; }

        public string PriceSetup { get; set; }

        public string SettlementId { get; set; }
        public string ProspectOrCustId { get; set; }

    }
}
