﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Domain;
using System;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class CustomerQuoteTabViewModel : BaseViewModel
    {
        #region Variable and object declaration

        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CustomerQuoteTabViewModel");
        private ObservableCollection<QuoteSummary> _QuoteList = new ObservableCollection<QuoteSummary>();
        private Managers.CustomerQuoteManager manager = new Managers.CustomerQuoteManager();
        private bool _IsProspectCustomer = false;
        private bool _IsQuoteGridVisible = false;

        Prospect prospect;
        public Prospect Prospect
        {
            get
            {
                return prospect;
            }
            set
            {
                prospect = value;
                OnPropertyChanged("Prospect");
            }
        }
        public bool prospectquote = false;
        public bool ProspectQuote
        {
            get
            {
                return prospectquote;
            }
            set
            {
                prospectquote = value;
                OnPropertyChanged("ProspectQuote");
            }
        }
        public string _SetHeight;
        public string SetHeight
        {
            get
            {
                return _SetHeight;
            }
            set
            {
                _SetHeight = value;
                OnPropertyChanged("SetHeight");
            }
        }
        #endregion

        #region Construtor

        public CustomerQuoteTabViewModel(BaseViewModel parent = null)
        {
            this.InitializeCommands();
            ViewModelPayload.PayloadManager.ProspectPayload.IsProspect = false;
            this.ParentView = parent;
            LoadQuoteSummaryList();
        }
        public CustomerQuoteTabViewModel(Guid token, bool isProspect, BaseViewModel Parent = null)
        {
            MessageToken = token;
            this.InitializeCommands();
            if (Parent != null)
            {
                this.ParentView = Parent;
            }
            if (isProspect)
            {
                IsProspectCustomer = true;
                ListTitle = "Prospect Quotes";
            }
            LoadQuoteSummaryList();

        }
        #endregion

        #region Property

        /// <summary>
        /// Get or set quote summary list 
        /// </summary>
        public ObservableCollection<QuoteSummary> QuoteList
        {
            get
            {
                return _QuoteList;
            }
            set
            {
                _QuoteList = value; OnPropertyChanged("QuoteList");
            }
        }

        /// <summary>
        /// Get or set ui token 
        /// </summary>
        public Guid MessageToken { get; set; }

        public bool IsProspectCustomer
        {
            get { return _IsProspectCustomer; }
            set
            {
                _IsProspectCustomer = value;
                OnPropertyChanged("IsProspectCustomer");
            }
        }


        private string listTitle = "Customer Quotes";

        public string ListTitle
        {
            get { return listTitle; }
            set { listTitle = value; OnPropertyChanged("ListTitle"); }
        }

        private bool _IsPrintEnabled = false;


        /// <summary>
        /// Get or set is print button enabled flag 
        /// </summary>
        public bool IsPrintEnabled
        {
            get
            {
                return _IsPrintEnabled;
            }
            set
            {
                _IsPrintEnabled = value;
                OnPropertyChanged("IsPrintEnabled");
            }
        }

        public bool IsQuoteGridVisible
        {
            get
            {
                return _IsQuoteGridVisible;
            }
            set
            {
                _IsQuoteGridVisible = value;
                OnPropertyChanged("IsQuoteGridVisible");
            }
        }

        #endregion

        #region DelegateProperty

        public DelegateCommand OpenQuoteScreenDlgtCmd { get; set; }

        public DelegateCommand PrintQuoteDlgtCmd { get; set; }

        public DelegateCommand RowSelectionChangedDlgtCmd { get; set; }
        #endregion

        #region Public Methods

        #endregion

        #region Private Methods
        /// <summary>
        /// Retrieves quote summary list async
        /// </summary>
        private void LoadQuoteSummaryList()
        {
            //await Task.Run(() =>
            //{ 
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerQuoteTabViewModel][Start:LoadSummaryList]");
            try
            {
                this.QuoteList = manager.GetQuoteSummaryList();
                this.IsQuoteGridVisible = (this.QuoteList.Count > 0);
                if (PayloadManager.ProspectPayload.IsProspect == true)
                {
                    Prospect = PayloadManager.ProspectPayload.Prospect;
                    ProspectQuote = true;
                    SetHeight = "50";
                }
                else
                {
                    ProspectQuote = false;
                    SetHeight = "0";
                }

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerQuoteTabViewModel][LoadSummaryList][ExceptionStackTrace = " + ex.ToString() + "]");
            }
            Logger.Info("SalesLogicExpress.Application.ViewModels][CustomerQuoteTabViewModel][End:LoadSummaryList]");
            //});
        }

        private void InitializeCommands()
        {
            RowSelectionChangedDlgtCmd = new DelegateCommand(
                (param) =>
                {
                    if (param != null)
                    {
                        this.IsPrintEnabled = true;
                    }
                }
                );

            PrintQuoteDlgtCmd = new DelegateCommand(
                (param) =>
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerQuoteTabViewModel][Start:PrintQuoteDlgtCmd]");
                    try
                    {

                        if (param == null)
                        {
                            return;
                        }

                        QuoteSummary quoteSummary = param as QuoteSummary;

                        if (quoteSummary == null)
                        {
                            return;
                        }

                        ViewModelPayload.PayloadManager.QuotePayload.QuoteId = quoteSummary.QuoteId.ToString();

                        CustomerQuoteHeader customerQuote = manager.GetCustomerQuote();
                        string confirmationMessage = string.Empty;

                        if (customerQuote.IsPrinted)
                        {
                            confirmationMessage = "Do you want to re-print this Quote?";
                        }
                        else
                        {
                            confirmationMessage = "Do you want to print this Quote?";
                        }

                        ConfirmWindow confirmReadOnlyNavigation = new Helpers.ConfirmWindow { OkButtonContent = "Yes", CancelButtonContent = "No" };
                        confirmReadOnlyNavigation.Message = confirmationMessage;
                        Messenger.Default.Send<Helpers.ConfirmWindow>(confirmReadOnlyNavigation, MessageToken);

                        if (confirmReadOnlyNavigation.Confirmed)
                        {
                            //Mark IsPrinted in DB for this quote
                            if (!customerQuote.IsPrinted)
                            {
                                manager.UpdateIsPrinted(customerQuote);
                                quoteSummary.IsPrinted = true;
                            }
                            //new ReportViewModels.CustomerQuoteReport(customerQuote);
                            ShowReport(customerQuote);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerQuoteTabViewModel.][PrintQuoteDlgtCmd][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerQuoteTabViewModel][End:PrintQuoteDlgtCmd]");

                });
            OpenQuoteScreenDlgtCmd = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerQuoteTabViewModel][Start:OpenQuoteScreenDlgtCmd]");
                try
                {
                    CustomerDashboard custDash = new CustomerDashboard();
                    bool IsPreTripDone = custDash.GetPreTripDetailsForDate();
                    if (IsPreTripDone)
                    {
                        if (param == null)
                        {
                            return;
                        }

                        if (param != null)
                        {
                            QuoteSummary quote = param as QuoteSummary;

                            if (quote != null)
                            {
                                ViewModelPayload.PayloadManager.QuotePayload.QuoteId = quote.QuoteId.ToString();
                                ViewModelPayload.PayloadManager.ProspectPayload.ShowEditModeButton = true;
                            }
                            else
                            {
                                ViewModelPayload.PayloadManager.QuotePayload.QuoteId = "NEW";
                                ViewModelPayload.PayloadManager.ProspectPayload.ShowEditModeButton = false;
                            }
                        }

                        //If EOD settlement has been completed, then show the appropriate message and return. 
                        //if ((bool)CommonNavInfo.IsEODCompleted)
                        //{
                        //    if (ViewModelPayload.PayloadManager.QuotePayload.QuoteId == "NEW")
                        //    {
                        //        var alertMessage = new Helpers.AlertWindow { Message = "EOD settlement has been completed,\nyou cannot create Quote!", MessageIcon = "Alert" };
                        //        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        //    }

                        //    return;
                        //}

                        if ((bool)CommonNavInfo.IsEODPendingForVerification)
                        {
                            if (ViewModelPayload.PayloadManager.QuotePayload.QuoteId == "NEW")
                            {
                                var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.EODConfirmedValidation, MessageIcon = "Alert" };
                                Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                            }

                            return;
                        }



                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                        if (IsProspectCustomer)
                        {
                            var parentVM = (ProspectsHome)this.ParentView;
                            if (!parentVM.DashboardVM.VerifyStopInfo(parentVM.DashboardVM.Prospect))
                                return;

                            ViewModelPayload.PayloadManager.ProspectPayload.IsProspect = true;
                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = ViewModelMappings.View.ProspectQuote.GetEnumDescription();
                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = ViewModelMappings.View.ProspectHome.GetEnumDescription();
                            ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.ProspectHome_Quote.ToString();

                            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ProspectQuote, CurrentViewName = ViewModelMappings.View.ProspectHome, CloseCurrentView = false, ShowAsModal = false, Refresh = false };
                            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                        }
                        else
                        {
                            ViewModelPayload.PayloadManager.ProspectPayload.IsProspect = false;
                            // Validations for stop management

                            # region Stop Related Validations
                            var dashboardVM = (ParentView as CustomerHome).CustomerDashboardVM;
                            if (!dashboardVM.IsAllCustomerTab && DateTime.Compare(dashboardVM.Customer.StopDate.Value.Date, dashboardVM.TodayDate.Date) != 0)
                            {
                                dashboardVM.GetCreateOrMoveStopDetails();
                                if (dashboardVM.IsActionInitiated)
                                {
                                    NavigateToQuoteScreen();
                                }
                                //return;
                            }
                            else if (!dashboardVM.Customer.IsTodaysStop)
                            {
                                dashboardVM.ShowStopConfirmation();
                                //return;
                                if (dashboardVM.IsActionInitiated)
                                    NavigateToQuoteScreen();

                                if (dashboardVM.IsCreateOrderMoveStopCliked)
                                {
                                    dashboardVM.IsCreateOrderMoveStopCliked = false;
                                    NavigateToQuoteScreen();
                                }
                            }
                            else
                            {
                                NavigateToQuoteScreen();

                            }
                            #endregion

                        }
                    }
                    else
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.Common.PreTripErrorMsg, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        if (alertMessage.Closed) return;

                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.CustomerQuote, Refresh = true, CloseCurrentView = true, ShowAsModal = false };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerQuoteTabViewModel.][OpenQuoteScreenDlgtCmd][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerQuoteTabViewModel][End:OpenQuoteScreenDlgtCmd]");
            });
        }

        private void NavigateToQuoteScreen()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = ViewModelMappings.View.CustomerQuote.GetEnumDescription();
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = ViewModelMappings.View.CustomerHome.GetEnumDescription();
            ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.CustomerHome_Quote.ToString();
            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerQuote, CurrentViewName = ViewModelMappings.View.CustomerHome, Refresh = true, CloseCurrentView = false, ShowAsModal = false };
            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
        }

        public void ShowReport(CustomerQuoteHeader customerQuote)
        {
            try
            {
                var DocInfo = new ReportEngine.DocumentInformation();
                var repEngine = ReportEngine.SLEReportEngine.Create(ResourceManager.GetOpenConnectionInstance());

                repEngine.Notify += repEngine_Notify;

                repEngine.Log = this.Logger;
                ReportEngine.IReportSLE ReportData = null;
                var DocumentId = customerQuote.DocumentId;
                var SourceDocumentNo = Convert.ToDecimal(customerQuote.QuoteId);
                if (DocumentId <= 0)
                {
                    ReportData = new ReportViewModels.CustomerQuoteReport(customerQuote);
                    DocInfo = repEngine.Generate<Reporting.CustomerQuote>(SourceDocumentNo, true, ReportEngine.ReportCategory.Customer, ReportEngine.ReportsTypes.CustomerQuote, ReportData, null, ReportEngine.ContentTypes.PDF);
                }
                else
                {
                    DocInfo.DocumentId = DocumentId;
                }

                if (DocInfo.IsEndOfSequence)
                {
                    Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, Please sync to continue" }, MessageToken);
                    return;
                }

                if (DocInfo.IsDocumentCreated)
                    ReportEngine.SLEReportEngine.SetDocumentID(ResourceManager.GetOpenConnectionInstance(), "BUSDTA.Customer_Quote_Header", "CustomerQuoteId", SourceDocumentNo, DocInfo.DocumentId);
                customerQuote.DocumentId = DocInfo.DocumentId; //Replace Latest

                var ReportBytes = repEngine.GetReport(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Customer, customerQuote.DocumentId, null);

                //GenericReport.GenericReportViewModel grv = new GenericReport.GenericReportViewModel();
                //grv.ReportSource = ReportBytes;

                if (ReportBytes == null)
                {
                    Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Report not generated, Please try again." }, MessageToken);
                    return;
                }

                SalesLogicExpress.Application.GenericReport.ReportPayloads Payload = new GenericReport.ReportPayloads();
                Payload.ReportFile = ReportBytes;
                Payload.CurrentView = ViewModelMappings.View.AppPDfWindow;
                Payload.PreviousView = ViewModelMappings.View.CustomerQuote;
                Payload.CurrentViewTitle = "Customer Quote Report";
                Payload.PreviousViewTitle = "Customer Quote";
                Payload.MessageToken = this.MessageToken;

                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;


                //Code For printing directly - Arvind
                var PrintDirectly = true;
                if (PrintDirectly)
                {
                    repEngine.PrintDirectly(ResourceManager.GetOpenConnectionInstance(), ReportEngine.ReportCategory.Customer, DocInfo.DocumentId, null, true);
                }
                else
                {
                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AppPDfWindow, CurrentViewName = ViewModelMappings.View.CustomerQuote, CloseCurrentView = false, Payload = Payload, ShowAsModal = false };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerQuoteTabViewModel][ShowReport][ExceptionStackTrace = " + ex.ToString() + "]");
            }
        }

        void repEngine_Notify(string msg)
        {
            System.Windows.MessageBox.Show(msg);
        }
        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
