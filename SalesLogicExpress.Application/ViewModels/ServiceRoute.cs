﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Threading;
using Telerik.Windows.Controls;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using System.Configuration;
using SalesLogicExpress.Domain.Prospect_Models;
using System.Reflection;

namespace SalesLogicExpress.Application.ViewModels
{
    public class ServiceRoute : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels");
        DateTime currentdate = DateTime.Today;
        ServiceRouteManager serviceRouteManager = new ServiceRouteManager();
        StopManager stopManager = new StopManager();
        CustomerManager customerManager = new CustomerManager();
        string baseDate = ConfigurationManager.AppSettings["BaseDate"].ToString();

        ObservableCollection<Customer> dailyStopItems = new ObservableCollection<Customer>();
        public static ObservableCollection<Customer> TodaysStops = new ObservableCollection<Customer>();
        public DelegateCommand AddNewProspect { get; set; }
        public DelegateCommand CreateStop { get; set; }
        public DelegateCommand MoveStop { get; set; }
        public DelegateCommand ShowMoveStopDialog { get; set; }
        public DelegateCommand ShowCreateStopDialog { get; set; }
        public DelegateCommand SearchItemOnType { get; set; }
        public DelegateCommand ChangeDate { get; set; }
        public DelegateCommand ViewStopSequencing { get; set; }
        public DelegateCommand ClearSearchText { get; set; }
        public DelegateCommand ViewSelectionChanged { get; set; }
        public DelegateCommand CustomerSelectedCommand { get; set; }
        public DelegateCommand ShowCustomerHome { get; set; }
        public DelegateCommand ShowProspectHome { get; set; }
        public DelegateCommand CalendarDateChanged { get; set; }
        public DelegateCommand OrderTemplate { get; set; }
        public DelegateCommand DailyStopSelectionChanged { get; set; }
        public DelegateCommand CustomerSelectionChanged { get; set; }
        public DelegateCommand ProspectSelectionChanged { get; set; }
        public DelegateCommand OpenNewProspectDialog { get; set; }
        public DelegateCommand ConfirmCreditHoldDialog { get; set; }
        public DelegateCommand ShowCustomerStopSequencing { get; set; }
        public DelegateCommand DragOverItem { get; set; }
        public DelegateCommand GetAuthorizeCode { get; set; }
        public DelegateCommand ContinueToCustomerHome { get; set; }
        public DelegateCommand SubmitAuthorizeCode { get; set; }

        private Customer _SelectedItem = null;
        private Prospect selectedProspect = new Prospect();

        public Prospect SelectedProspect
        {
            get { return selectedProspect; }
            set { selectedProspect = value; }
        }


        public ObservableCollection<Customer> CustomerListBackUpForSearch
        {
            get
            {
                return this.customerListBackUpForSearch;
            }
            set
            {
                this.customerListBackUpForSearch = value;
                OnPropertyChanged("CustomerListBackUpForSearch");
            }
        }

        public ObservableCollection<Prospect> ProsprctListBackUpForSearch
        {
            get
            {
                return this.prospectListBackUpForSearch;
            }
            set
            {
                this.prospectListBackUpForSearch = value;
                OnPropertyChanged("ProsprctListBackUpForSearch");
            }
        }

        public bool IsCreateStopButtonIsEnabled
        {
            get
            {
                return this.isCreateStopButtonIsEnabled;
            }
            set
            {
                this.isCreateStopButtonIsEnabled = value;
                OnPropertyChanged("IsCreateStopButtonIsEnabled");
            }
        }

        public Customer SelectedItem
        {
            get
            {
                return _SelectedItem;
            }
            set
            {
                _SelectedItem = value;

            }
        }


        private string _AuthorizeCode;
        public string AuthorizeCode
        {
            get
            {
                return _AuthorizeCode;
            }
            set
            {
                _AuthorizeCode = value;
                OnPropertyChanged("AuthorizeCode");
            }
        }


        private string _chkValid = "";
        public string chkValid
        {
            get
            {
                return _chkValid;
            }
            set
            {
                _chkValid = value;
                OnPropertyChanged("chkValid");
            }
        }


        private CreateAndMoveStopViewModel _MoveStopViewModelProperty = new CreateAndMoveStopViewModel();
        public CreateAndMoveStopViewModel MoveStopViewModelProperty
        {
            get
            {
                return _MoveStopViewModelProperty;
            }
            set
            {
                _MoveStopViewModelProperty = value;
                OnPropertyChanged("MoveStopViewModelProperty");
            }
        }
        private CreateAndMoveStopViewModel _CreateStopViewModelProperty = new CreateAndMoveStopViewModel();
        public CreateAndMoveStopViewModel CreateStopViewModelProperty
        {
            get
            {
                return _CreateStopViewModelProperty;
            }
            set
            {
                _CreateStopViewModelProperty = value;
                OnPropertyChanged("CreateStopViewModelProperty");
            }
        }
        public Guid MessageToken { get; set; }
        public static DateTime SelectedCalendarDate = DateTime.Now;
        public string Route { get; set; }



        private bool _IsMoveStopButtonEnabled = false;
        public bool IsMoveStopButtonEnabled
        {
            get { return _IsMoveStopButtonEnabled; }
            set { _IsMoveStopButtonEnabled = value; OnPropertyChanged("IsMoveStopButtonEnabled"); }
        }
        public string Day
        {
            get;
            set;
        }
        private string Date { get; set; }
        private string Month { get; set; }
        private string Year { get; set; }
        private ObservableCollection<Customer> _CustomerList = new ObservableCollection<Customer>();
        private ObservableCollection<Customer> customerListBackUpForSearch = new ObservableCollection<Customer>();
        private ObservableCollection<Prospect> prospectListBackUpForSearch = new ObservableCollection<Prospect>();
        public ObservableCollection<Prospect> _ProspectList = new ObservableCollection<Prospect>();
        public ObservableCollection<Customer> CustomerList
        {
            get
            {
                return _CustomerList;
            }
            set
            {
                _CustomerList = value;
                OnPropertyChanged("CustomerList");
            }
        }
        public ObservableCollection<Prospect> ProspectList
        {
            get
            {
                return _ProspectList;
            }
            set
            {
                _ProspectList = value;
                OnPropertyChanged("ProspectList");
            }
        }

        private ObservableCollection<Customer> _AllCustomers = new ObservableCollection<Customer>();
        public ObservableCollection<Customer> AllCustomers
        {
            get
            {
                return _AllCustomers;
            }
            set
            {
                _AllCustomers = value;
                OnPropertyChanged("AllCustomers");
            }
        }
        private Prospect _ProspectToAdd = new Prospect();
        public Prospect ProspectToAdd
        {
            get
            {
                return _ProspectToAdd;
            }
            set
            {
                _ProspectToAdd = value;
                OnPropertyChanged("ProspectToAdd");
            }
        }

        public int _TotalStops = 0;
        public int TotalStops
        {
            get { return _TotalStops; }
            set
            {

                _TotalStops = value;
                OnPropertyChanged("TotalStops");
            }
        }
        public int _ProspectsCount = 0;
        public int ProspectsCount
        {
            get { return ProspectList.Count; }
            set { _ProspectsCount = value; OnPropertyChanged("ProspectsCount"); }
        }
        List<string> _StateCodeList;
        public List<string> StateCodeList
        {
            get
            {
                return _StateCodeList;
            }
            set
            {
                _StateCodeList = value;
                OnPropertyChanged("StateCodeList");
            }
        }
        private string _TotalSales = "0.00";
        public string TotalSales
        {
            get
            {
                return "$" + _TotalSales;
            }
            set
            {
                _TotalSales = value;
                OnPropertyChanged("TotalSales");
            }
        }
        private string _TotalAllied = "0.00";
        public string TotalAllied
        {
            get
            {
                return "$" + _TotalAllied;
            }
            set
            {
                _TotalAllied = value;
                OnPropertyChanged("TotalAllied");
            }
        }


        private string _TotalCoffee = "0.00";
        public string TotalCoffee
        {
            get
            {
                return "$" + _TotalCoffee;
            }
            set
            {
                _TotalCoffee = value;
                OnPropertyChanged("TotalCoffee");
            }
        }

        private string _TotalCoffeePound = "0.00";
        public string TotalCoffeePound
        {
            get
            {
                return _TotalCoffeePound + "";
            }
            set
            {
                _TotalCoffeePound = value;
                OnPropertyChanged("TotalCoffeePound");
            }
        }

        private string _TotalReturn = "0.00";
        public string TotalReturn
        {
            get
            {
                return "$" + _TotalReturn;
            }
            set
            {
                _TotalReturn = value;
                OnPropertyChanged("TotalReturn");
            }
        }

        public int _TotalStopsServiced = 0;
        public int TotalStopsServiced
        {
            get
            {
                return _TotalStopsServiced;
            }
            set
            {
                _TotalStopsServiced = value;
                OnPropertyChanged("TotalStopsServiced");
            }
        }
        public int _TotalStopsRequired = 0;
        public int TotalStopsRequired
        {
            get
            {
                return _TotalStopsRequired;
            }
            set
            {
                if (value < 0)
                    value = 0;
                _TotalStopsRequired = value;
                OnPropertyChanged("TotalStopsRequired");
            }
        }
        bool _IsBusy = true;
        public bool IsBusy
        {
            get
            {
                return _IsBusy;
            }
            set
            {
                _IsBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        bool _CustomerLoading = false;
        public bool CustomerLoading
        {
            get
            {
                return _CustomerLoading;
            }
            set
            {
                _CustomerLoading = value;
                OnPropertyChanged("CustomerLoading");
            }
        }
        private int _PendingTransactions = 0;
        public int PendingTransactions
        {
            get
            {
                return _PendingTransactions;
            }
            set
            {
                _PendingTransactions = value;
                OnPropertyChanged("PendingTransactions");
            }
        }
        private int _TotalCustomers = 0;
        public int TotalCustomers
        {
            get
            {
                return _TotalCustomers;
            }
            set
            {
                _TotalCustomers = value;
                OnPropertyChanged("TotalCustomers");
            }
        }
        public int ItemReturns { get; set; }
        string _DailyStopDisplayDate;
        public DateTime selectableDateEnd { get; set; }
        private DateTime _DailyStopDate;
        public DateTime DailyStopDate
        {
            get
            {
                return _DailyStopDate;
            }
            set
            {
                _DailyStopDate = value;
                string weekNo = serviceRouteManager.GetCurrentWeekNo(_DailyStopDate);
                DailyStopDisplayDate = "Week " + weekNo + ": " + _DailyStopDate.ToString("dddd', 'MM'/'dd'/'yyyy ");
                if (DateTime.Compare(DailyStopDate.Date, DateTime.Now.Date) < 0)
                {
                    IsMoveStopButtonEnabled = false;
                }
                OnPropertyChanged("DailyStopDate");
                OnPropertyChanged("DailyStopDisplayDate");
            }
        }
        public string DailyStopDisplayDate
        {
            get
            {
                return _DailyStopDisplayDate;
            }
            set
            {
                _DailyStopDisplayDate = value;

                OnPropertyChanged("DailyStopDisplayDate");
            }
        }
        private DateTime _SelectedDate { get; set; }
        public DateTime SelectedDate
        {
            get { return _SelectedDate; }
            set
            {
                _SelectedDate = value;
                OnPropertyChanged("SelectedDate");
            }
        }
        private bool _IsCustomerTabActive = false;
        public bool IsCustomerTabActive
        {
            get
            {
                return _IsCustomerTabActive;
            }
            set
            {
                _IsCustomerTabActive = value;
                if (_IsCustomerTabActive)
                {
                    IsBusy = true;
                    //IsDailyStopTabActive = false;
                    //IsProspectsTabActive = false;
                    ClearSearchText.Execute(null);
                    GetCustomerList(null);
                    // For Page Navigation Sync Bug Issue Fix
                    //GetAllCustomers();
                    // OnPropertyChanged("AllCustomers");
                }
                OnPropertyChanged("IsCustomerTabActive");
            }
        }
        private bool _IsDailyStopTabActive = true;
        public bool IsDailyStopTabActive
        {
            get
            {
                return _IsDailyStopTabActive;
            }
            set
            {
                _IsDailyStopTabActive = value;
                if (value)
                {
                    //IsProspectsTabActive = false;
                    //IsCustomerTabActive = false;
                    ClearSearchText.Execute(null);
                    LoadCustomers(DailyStopDate);
                }
                OnPropertyChanged("IsDailyStopTabActive");
            }
        }
        bool isCreateStopButtonIsEnabled = false;
        private bool _IsProspectsTabActive = false;
        public bool IsProspectsTabActive
        {
            get
            {
                return _IsProspectsTabActive;
            }
            set
            {
                _IsProspectsTabActive = value;

                if (_IsProspectsTabActive)
                {
                    //IsDailyStopTabActive = false;
                    //IsCustomerTabActive = false;
                    IsBusy = true;
                    ClearSearchText.Execute(null);

                    GetProspectList();

                }
                OnPropertyChanged("IsProspectsTabActive");
            }
        }

        private bool hasDragged = false;
        public bool HasDragged
        {
            get
            {
                return this.hasDragged;
            }
            set
            {
                this.hasDragged = value;
                OnPropertyChanged("HasDragged");
            }
        }

        static int listCount = 0;

        //*******************
        public int _TotalCustomersOnCreditHold = 0;
        public int TotalCustomersOnCreditHold
        {
            get
            {
                return _TotalCustomersOnCreditHold;
            }
            set
            {
                _TotalCustomersOnCreditHold = value;
                OnPropertyChanged("TotalCustomersOnCreditHold");
            }
        }
        private bool isFirstCallToCalendarDateChanged = true;

        public int ItemReturnsByCustomers { get; set; }
        public string CustomersDisplayDate { get; set; }
        private Customer _CustomerToMove = new Customer();
        public Customer CustomerToMove
        {
            get
            {
                return _CustomerToMove;
            }
            set
            {
                _CustomerToMove = value;
            }
        }
        public ServiceRoute()
        {
            DateTime Timer = DateTime.Now;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:ServiceRouteViewModel]\t" + DateTime.Now + "");
            IsBusy = true;
            Initialize();
            DailyStopDate = ServiceRoute.SelectedCalendarDate;
            InitializeCommands();
            try
            {
                LoadCustomers(DailyStopDate);
                Route = PayloadManager.ApplicationPayload.Route;
                Day = currentdate.ToString("dddd") + ", ";
                Month = currentdate.ToString("MMMM") + "  ";
                Date = currentdate.Day.ToString() + ", ";
                Year = currentdate.Year.ToString() + " ";

                GetLastDate();
                DailyStopDisplayDate = CustomersDisplayDate = ServiceRoute.SelectedCalendarDate.ToString("dddd', 'M'/'dd'/'yyyy ");

                SelectedDate = DailyStopDate.AddDays(1);
                GetAllCustomers();
                if (!(dailyStopItems.Count() == 0))
                    dailyStopItems.Clear();
                OnPropertyChanged("AllCustomers");
                
                ViewModelPayload.PayloadManager.ProspectPayload.IsProspect = false;
                IsBusy = false;
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][ServiceRouteViewModel][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:ServiceRouteViewModel]\t" + (DateTime.Now - Timer) + "");
        }

        private void GetLastDate()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:GetLastDate]\t" + DateTime.Now + "");
            selectableDateEnd = new StopManager().GetLastDate;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:GetLastDate]\t" + (DateTime.Now ) + "");
        }
        private void MoveStopViewModelProperty_StopMoved(object sender, StopActionCompletedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:MoveStopViewModelProperty_StopMoved]\t" + DateTime.Now + "");
            try
            {
                LoadCustomers(DailyStopDate);
                LoadDataStatistics();
                Visitee cust = e.visiteeChanged;
                if (!(dailyStopItems.Count() == 0))
                    dailyStopItems.Clear();
                //if (!DateTime.Equals(e.DateSelected.Value.Date, DateTime.Today))
                //{
                if (AllCustomers.Any(item => item.CustomerNo == cust.VisiteeId))
                {
                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    dict = serviceRouteManager.GetVisiteeStopInfo(e.visiteeChanged, DateTime.Today.Date, true, true);
                    AllCustomers.Where(item => item.CustomerNo == cust.VisiteeId).FirstOrDefault().NextStop = dict["NextStop"];
                }
                //}
                if (IsCustomerTabActive)
                {
                    CustomerSelectionChanged.Execute(e.visiteeChanged);
                }
                ClearSearchText.Execute(null);

                GetProspectList(true);
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][MoveStopViewModelProperty_StopMoved][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:MoveStopViewModelProperty_StopMoved]");
        }
        private void LoadDataStatistics()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:LoadDataStatistics]\t" + DateTime.Now + "");
            try
            {
                int serviced = CustomerList.Count(item => item.Activity == true || (item.SaleStatus.ToLower() == "nosale"));
                int total = CustomerList.Count();

                TotalStops = CustomerList.Count() > 0 ? CustomerList.Count() : 0;
                TotalCustomers = AllCustomers.Count() > 0 ? AllCustomers.Count() : 0;
                TotalStopsRequired = total;
                TotalStopsServiced = serviced;

                PendingTransactions = CustomerList.Where(item => item.PendingActivity > 0).Sum(s => s.PendingActivity);
                TotalCustomersOnCreditHold = AllCustomers.Where(item => item.CreditHold == true).Count();

                TotalStops = TotalStops - CustomerList.Where(item => item.StopType == "Moved").Count();
                TotalStopsRequired = TotalStopsRequired - CustomerList.Where(item => item.StopType == "Moved" || item.HasActivity == true || (item.SaleStatus.ToLower() == "nosale" && item.StopType.ToLower() != "unplanned")).Count();
                TotalStopsRequired -= CustomerList.Where(item => (item.StopType.ToLower() == "unplanned" && item.HasActivity == false)).Count();
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][LoadDataStatistics][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:LoadDataStatistics]");
        }

        void InitializeCommands()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:InitializeCommands]");
            try
            {

                AddNewProspect = new DelegateCommand((param) =>
                {
                    try
                    {
                        Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:AddNewProspect][Param =" + param + "]");

                        int result = new ProspectManager().AddNewProspect(ProspectToAdd, CommonNavInfo.RouteID.ToString());
                        //bool IsSaved = Convert.ToBoolean(new ProspectManager().AddNewProspect(ProspectToAdd, CommonNavInfo.RouteID.ToString()));

                        if (result == 1)
                        {
                            IsDirty = false;
                            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                            GetProspectList(true);
                            ClearSearchText.Execute(null);
                        }
                        else if (result == 0)
                        {
                            Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "Prospect name already exists" }, MessageToken);
                        }
                        else if (result == 2)
                        {
                            Messenger.Default.Send<Helpers.AlertWindow>(new AlertWindow { Message = "End of sequence, please sync to continue" }, MessageToken);
                        }
                        Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:AddNewProspect][Param =" + param + "]");


                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][AddNewProspect][ExceptionStackTrace = " + ex.StackTrace + "]");
                        throw;
                    }
                });

                OpenNewProspectDialog = new DelegateCommand((param) =>
                {
                    StateCodeList = customerManager.GetStateCodes();
                    ProspectToAdd = new Prospect();
                    ProspectToAdd.PropertyChanged += ProspectToAdd_PropertyChanged;
                    var dialog = new Helpers.DialogWindow { TemplateKey = "AddNewProspect", Title = "Add Prospect", Payload = this };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                });


                ConfirmCreditHoldDialog = new DelegateCommand((param) =>
                {

                    try
                    {
                        if (AuthorizeCode != null)
                        {
                            chkValid = "";
                            if (AuthorizeCode == "Accepted")
                            {
                                if (!IsDailyStopTabActive)
                                {
                                    if (dailyStopItems.Count() == 0)
                                    {
                                        dailyStopItems = serviceRouteManager.GetCustomersForRoute(CommonNavInfo.RouteUser, DateTime.Today);
                                    }
                                    //SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                                    //SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Customer Home";
                                    //SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Service Route";



                                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(SelectedItem);
                                    SelectedCalendarDate = DailyStopDate;
                                    SelectedItem.IsTodaysStop = dailyStopItems.Any(item => item.CustomerNo == SelectedItem.CustomerNo) ? true : false;
                                    Customer c = dailyStopItems.FirstOrDefault(x => x.CustomerNo == SelectedItem.CustomerNo);
                                    if (c != null)
                                    {
                                        SelectedItem.IsTodaysStop = c.StopType.ToLower() == "moved" ? false : true;
                                        if (SelectedItem.StopType.ToLower() == "moved")
                                        {
                                            if (c.StopType.ToLower() == "unplanned")
                                            {
                                                //Customer custPresent = dailyStopItems.First(x => x.CustomerNo == SelectedItem.CustomerNo);
                                                //SelectedItem.StopType = custPresent.StopType;
                                                //SelectedItem.NextStop = custPresent.NextStop;
                                                SelectedItem = dailyStopItems.First(x => x.CustomerNo == SelectedItem.CustomerNo);
                                            }
                                        }
                                        SelectedItem.StopDate = c.StopDate.Value;
                                        //SelectedItem.StopID = c.StopID;
                                    }
                                    else
                                    {
                                        Customer inSelectedDateList = CustomerList.FirstOrDefault(x => x.CustomerNo == SelectedItem.CustomerNo);
                                        if (inSelectedDateList != null)
                                        {
                                            SelectedItem.StopDate = inSelectedDateList.StopDate.Value;
                                            //SelectedItem.StopID = inSelectedDateList.StopID;
                                        }
                                    }
                                    ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.ServiceRoute_Customer.ToString();

                                }
                                else
                                {
                                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(SelectedItem);
                                    ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.ServiceRoute_Dailystop.ToString();
                                }
                                PayloadManager.OrderPayload.Customer = SelectedItem;
                                PayloadManager.OrderPayload.StopDate = SelectedCalendarDate;
                                PayloadManager.OrderPayload.Customer.CreditHold = false;
                                ServiceRoute.TodaysStops = dailyStopItems.Clone();
                                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, Refresh = false, CurrentViewName = ViewModelMappings.View.ServiceRoute, CloseCurrentView = true, ShowAsModal = false };
                                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                                IsCustomerTabActive = false;
                                IsDailyStopTabActive = false;
                                if (AllCustomers.Any() || AllCustomers != null)
                                    AllCustomers.Clear();
                                if (dailyStopItems.Any() || dailyStopItems != null)
                                    dailyStopItems.Clear();
                            }
                            else
                            {
                                var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.ServiceRoute.InvalidCode, MessageIcon = "Alert" };
                                Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);


                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        
                       Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][ConfirmCreditHoldDialog][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }

                });


                GetAuthorizeCode = new DelegateCommand((param) =>
                {
                    var dialog = new Helpers.DialogWindow { TemplateKey = "ApplyAuthorizeCode", Title = "Release Credit Hold", Payload = this };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                });



                ContinueToCustomerHome = new DelegateCommand((param) =>
                {
                    try
                    {

                        if (!IsDailyStopTabActive)
                        {
                            if (dailyStopItems.Count() == 0)
                            {
                                dailyStopItems = serviceRouteManager.GetCustomersForRoute(CommonNavInfo.RouteUser, DateTime.Today);
                            }
                            //SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                            //SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Customer Home";
                            //SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Service Route";



                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(SelectedItem);
                            SelectedCalendarDate = DailyStopDate;
                            SelectedItem.IsTodaysStop = dailyStopItems.Any(item => item.CustomerNo == SelectedItem.CustomerNo) ? true : false;
                            Customer c = dailyStopItems.FirstOrDefault(x => x.CustomerNo == SelectedItem.CustomerNo);
                            if (c != null)
                            {
                                SelectedItem.IsTodaysStop = c.StopType.ToLower() == "moved" ? false : true;
                                if (SelectedItem.StopType.ToLower() == "moved")
                                {
                                    if (c.StopType.ToLower() == "unplanned")
                                    {
                                        //Customer custPresent = dailyStopItems.First(x => x.CustomerNo == SelectedItem.CustomerNo);
                                        //SelectedItem.StopType = custPresent.StopType;
                                        //SelectedItem.NextStop = custPresent.NextStop;
                                        SelectedItem = dailyStopItems.First(x => x.CustomerNo == SelectedItem.CustomerNo);
                                    }
                                }
                                SelectedItem.StopDate = c.StopDate.Value;
                                //SelectedItem.StopID = c.StopID;
                            }
                            else
                            {
                                Customer inSelectedDateList = CustomerList.FirstOrDefault(x => x.CustomerNo == SelectedItem.CustomerNo);
                                if (inSelectedDateList != null)
                                {
                                    SelectedItem.StopDate = inSelectedDateList.StopDate.Value;
                                    //SelectedItem.StopID = inSelectedDateList.StopID;
                                }
                            }
                            ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.ServiceRoute_Customer.ToString();

                        }
                        else
                        {
                            SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(SelectedItem);
                            ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.ServiceRoute_Dailystop.ToString();
                        }
                        PayloadManager.OrderPayload.Customer = SelectedItem;
                        PayloadManager.OrderPayload.StopDate = SelectedCalendarDate;
                        ServiceRoute.TodaysStops = dailyStopItems.Clone();
                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, Refresh = false, CurrentViewName = ViewModelMappings.View.ServiceRoute, CloseCurrentView = true, ShowAsModal = false };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                        IsCustomerTabActive = false;
                        IsDailyStopTabActive = false;
                        if (AllCustomers.Any() || AllCustomers != null)
                            AllCustomers.Clear();
                        if (dailyStopItems.Any() || dailyStopItems != null)
                            dailyStopItems.Clear();
                    }
                    catch (Exception ex)
                    {
                        
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][ContinueToCustomerHome][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                });
                ShowCustomerHome = new DelegateCommand((param) =>
                {
                    try
                    {

                        if (param != null)
                        {
                            Customer cust = param as Customer;
                            SelectedCalendarDate = DailyStopDate;
                            if (cust.VisiteeType.ToLower() != "prospect")
                            {
                                //if (cust.CreditHold != true)
                                //{                                
                                cust = null;    // Disposing The Objects --Vignesh D
                                this.IsBusy = true;
                                if (!IsDailyStopTabActive)
                                {
                                    if (dailyStopItems.Count() == 0)
                                    {
                                        dailyStopItems = serviceRouteManager.GetCustomersForRoute(CommonNavInfo.RouteUser, DateTime.Today);
                                    }
                                    //SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                                    //SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Customer Home";
                                    //SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Service Route";

                                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(SelectedItem);
                                    SelectedCalendarDate = DailyStopDate;
                                    SelectedItem.IsTodaysStop = dailyStopItems.Any(item => item.CustomerNo == SelectedItem.CustomerNo) ? true : false;
                                    Customer c = dailyStopItems.FirstOrDefault(x => x.CustomerNo == SelectedItem.CustomerNo);
                                    if (c != null)
                                    {
                                        SelectedItem.IsTodaysStop = c.StopType.ToLower() == "moved" ? false : true;
                                        if (SelectedItem.StopType.ToLower() == "moved")
                                        {
                                            if (c.StopType.ToLower() == "unplanned")
                                            {
                                                //Customer custPresent = dailyStopItems.First(x => x.CustomerNo == SelectedItem.CustomerNo);
                                                //SelectedItem.StopType = custPresent.StopType;
                                                //SelectedItem.NextStop = custPresent.NextStop;
                                                SelectedItem = dailyStopItems.First(x => x.CustomerNo == SelectedItem.CustomerNo);
                                            }
                                        }
                                        SelectedItem.StopDate = c.StopDate.Value;
                                        //SelectedItem.StopID = c.StopID;
                                    }
                                    else
                                    {
                                        Customer inSelectedDateList = CustomerList.FirstOrDefault(x => x.CustomerNo == SelectedItem.CustomerNo);
                                        if (inSelectedDateList != null)
                                        {
                                            SelectedItem.StopDate = inSelectedDateList.StopDate.Value;
                                            //SelectedItem.StopID = inSelectedDateList.StopID;
                                        }
                                    }
                                    ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.ServiceRoute_Customer.ToString();


                                }
                                else
                                {
                                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(SelectedItem);
                                    ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.ServiceRoute_Dailystop.ToString();
                                }
                                PayloadManager.OrderPayload.Customer = SelectedItem;
                                PayloadManager.OrderPayload.StopDate = SelectedCalendarDate;
                                ServiceRoute.TodaysStops = dailyStopItems.Clone();
                                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, Refresh = false, CurrentViewName = ViewModelMappings.View.ServiceRoute, CloseCurrentView = true, ShowAsModal = false };
                                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                                IsCustomerTabActive = false;
                                IsDailyStopTabActive = false;
                                if (AllCustomers.Any() || AllCustomers != null)
                                    AllCustomers.Clear();
                                if (dailyStopItems.Any() || dailyStopItems != null)
                                    dailyStopItems.Clear();
                                //}
                                //else
                                //{
                                //    var dialog = new Helpers.DialogWindow { TemplateKey = "ConfirmCreditHold", Title = "Confirm", Payload = this };
                                //    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                                //}
                            }
                            else
                            {
                                var customer = param as Customer;
                                Prospect prospect = (Prospect)(customer);
                                prospect.HasActivity = customer.HasActivity;
                                ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.ServiceRoute_Dailystop.ToString();
                                ShowProspectHome.Execute(prospect);
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][ShowCustomerHome][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }

                });
                //ShowProspectHome

                ShowProspectHome = new DelegateCommand((param) =>
                {
                    try
                    {

                        if (param != null)
                        {
                            Prospect prosp = param as Prospect;
                            SelectedCalendarDate = DailyStopDate;
                            prosp.IsTodaysStop = dailyStopItems.Any(item => item.CustomerNo == SelectedProspect.ProspectID) ? true : false;
                            this.IsBusy = true;
                            if (IsProspectsTabActive)
                            {
                                ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.ServiceRoute_Prospect.ToString();
                                prosp.IsFromListingTab = true;
                            }
                            // As prospect stop is always unplanned.
                            prosp.StopType = "unplanned";
                            PayloadManager.ProspectPayload.Prospect = prosp;
                            if (ProspectList.Any() || ProspectList != null)
                                ProspectList.Clear();

                            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ProspectHome, CurrentViewName = ViewModelMappings.View.ServiceRoute, CloseCurrentView = true, ShowAsModal = false };
                            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                            IsCustomerTabActive = false;
                            IsDailyStopTabActive = false;
                            IsProspectsTabActive = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        
                       Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][ShowProspectHome][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }

                });
                ClearSearchText = new DelegateCommand((param) =>
                {
                    SearchText = "";
                    SearchItemOnType.Execute(SearchText);
                });
                DailyStopSelectionChanged = new DelegateCommand((param) =>
                {
                    try
                    {

                        Dictionary<string, string> stopList = new Dictionary<string, string>();

                        if (param != null)
                        {
                            IsCreateStopButtonIsEnabled = true;
                            Customer cust = param as Customer;
                            SelectedItem = param as Customer;
                            stopList = serviceRouteManager.GetVisiteeStopInfo(cust, DateTime.Today, true, true);
                            string lastStop = stopList["LastStop"];

                            IsMoveStopButtonEnabled = true;
                            #region Code that checks whether last stop is serviced or not
                            //if (!(cust.VisiteeType.ToLower() == "prospect") || !(cust.StopType.ToLower() == "unplanned"))
                            //{
                            //    if (!(lastStop.ToLower() == "none"))
                            //    {
                            //        bool isLastStopServiced = serviceRouteManager.IsLastStopServiced(cust.CustomerNo, lastStop);
                            //        if (isLastStopServiced)
                            //        {
                            //            IsMoveStopButtonEnabled = true;
                            //            //IsCreateStopButtonEnabled = true;
                            //        }
                            //        else
                            //        {
                            //            IsMoveStopButtonEnabled = false;
                            //            //IsCreateStopButtonEnabled = false;
                            //        }
                            //   }
                            //}
                            //else
                            //{
                            //    IsMoveStopButtonEnabled = true;
                            //}
                            //if (lastStop.ToLower() == "none")
                            //{
                            //    IsMoveStopButtonEnabled = true;
                            //}
                            #endregion
                            if (dailyStopItems.Count() == 0)
                            {
                                dailyStopItems = serviceRouteManager.GetCustomersForRoute(CommonNavInfo.RouteUser, DateTime.Today);
                            }

                            SelectedItem.IsTodaysStop = dailyStopItems.Any(item => item.CustomerNo == SelectedItem.CustomerNo) ? true : false;
                            if (!(lastStop.ToLower() == "none"))
                            {

                                if (DailyStopDate.Date < lastStop.StringToUSDateFormat().Date)
                                {
                                    IsMoveStopButtonEnabled = false;
                                }
                            }
                            else
                            {
                                IsMoveStopButtonEnabled = true;

                            }
                            if (cust.StopType == "Moved" || cust.SaleStatus.ToLower() == "nosale")
                            {
                                IsMoveStopButtonEnabled = false;
                            }

                            if (SelectedItem.PendingActivity > 0 || SelectedItem.CompletedActivity > 0)
                            {
                                IsMoveStopButtonEnabled = false;
                            }
                        }
                        HasDragged = true;
                    }
                    catch (Exception ex)
                    {

                        Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][DailyStopSelectionChanged][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }

                   

                });
                CustomerSelectionChanged = new DelegateCommand((param) =>
                {
                    try
                    {

                        if (param != null)
                        {
                            IsCreateStopButtonIsEnabled = true;
                            if (dailyStopItems.Count() == 0)
                            {
                                dailyStopItems = serviceRouteManager.GetCustomersForRoute(CommonNavInfo.RouteUser, DateTime.Today);
                            }
                            SelectedItem = param as Customer;
                            ObservableCollection<Customer> list;// = new ObservableCollection<Customer>(CustomerList.Where(x => x.CustomerNo == SelectedItem.CustomerNo).Concat(AllCustomers.Where(x => x.CustomerNo == SelectedItem.CustomerNo)).OrderBy(i => i.StopDate).ToList<Customer>());
                            Customer customerItemForDashboard = CustomerList.Where(x => x.CustomerNo == SelectedItem.CustomerNo).Concat(dailyStopItems.Where(x => x.CustomerNo == SelectedItem.CustomerNo)).OrderBy(i => i.StopDate).FirstOrDefault();

                            if (customerItemForDashboard == null)
                            {
                                list = AllCustomers;
                            }
                            else
                            {
                                list = CustomerList.Count(x => x.CustomerNo == SelectedItem.CustomerNo) > 0 ? CustomerList : dailyStopItems;
                            }
                            if (list.Any(x => x.CustomerNo == SelectedItem.CustomerNo))
                            {
                                Customer cust = list.FirstOrDefault(x => x.CustomerNo == SelectedItem.CustomerNo);

                                SelectedItem.StopType = cust.StopType;
                                SelectedItem.OriginalDate = cust.OriginalDate;
                                SelectedItem.PendingActivity = cust.PendingActivity;
                                SelectedItem.CompletedActivity = cust.CompletedActivity;
                                SelectedItem.SequenceNo = cust.SequenceNo;
                                SelectedItem.TempSequenceNo = cust.TempSequenceNo;
                                SelectedItem.RescheduledDate = cust.RescheduledDate;
                                SelectedItem.SaleStatus = cust.SaleStatus;
                                SelectedItem.Activity = cust.Activity;
                                SelectedItem.SaleStatusReason = cust.SaleStatusReason;
                                SelectedItem.StopID = cust.StopID;
                                SelectedItem.ReferenceStopId = cust.ReferenceStopId;
                                SelectedItem.IsFromListingTab = true;
                                SelectedItem.IsTodaysStop = false;

                            }
                            else
                            {
                                SelectedItem.StopType = null;
                                SelectedItem.IsTodaysStop = false;
                            }
                            if (dailyStopItems.Any(x => x.CustomerNo == SelectedItem.CustomerNo))
                            {
                                SelectedItem.IsTodaysStop = true;
                                if (dailyStopItems.First(x => x.CustomerNo == SelectedItem.CustomerNo).StopType.ToLower() == "moved")
                                    SelectedItem.IsTodaysStop = false;
                            }
                            SelectedItem.VisiteeType = "Cust";
                            SelectedItem.IsFromListingTab = true;

                        }
                    }
                    catch (Exception ex)
                    {

                        Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][CustomerSelectionChanged][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                });
                ProspectSelectionChanged = new DelegateCommand((param) =>
                {
                    if (param != null)
                    {
                        IsCreateStopButtonIsEnabled = true;
                        SelectedProspect = param as Prospect;
                    }
                });
                CustomerSelectedCommand = new DelegateCommand((param) =>
                {
                    if (param != null)
                    {
                        Customer cust = param as Customer;
                        //SetPayload(param);
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Customer Home";
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Service Route";
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(cust);

                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.ServiceRoute, CloseCurrentView = false, Payload = SelectedItem, ShowAsModal = false };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    }

                });
                OrderTemplate = new DelegateCommand((param) =>
                {
                    if (param == null)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = "Please select a customer to proceed", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        SelectedCalendarDate = DailyStopDate;
                        PayloadManager.OrderPayload.StopDate = DailyStopDate;
                        return;
                    }
                    else
                    {
                        //SetPayload(param);
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Order Template";
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Service Route";
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(SelectedItem);
                        PayloadManager.OrderPayload.Customer = SelectedItem;
                        PayloadManager.OrderPayload.StopDate = DailyStopDate;
                        SelectedCalendarDate = DailyStopDate;
                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.ServiceRoute, CloseCurrentView = false, Payload = SelectedItem, ShowAsModal = false };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                        //var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.OrderTemplate, CurrentViewName = ViewModelMappings.View.ServiceRoute, CloseCurrentView = false, Payload = SelectedItem, ShowAsModal = false };
                        //Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    }
                });
                //CreateStop = new DelegateCommand((StopName) =>
                //{

                //});
                //ViewSelectionChanged = new DelegateCommand((StopName) =>
                //{

                //});
                ShowMoveStopDialog = new DelegateCommand((param) =>
                {
                    IsMoveStopButtonEnabled = false;
                    IsCreateStopButtonIsEnabled = false;
                    IsBusy = true;
                    SelectedDate = DailyStopDate;
                    if (param != null)
                    {
                        Customer cust = param as Customer;
                        SelectedItem = param as Customer;
                        CustomerToMove = SelectedItem;
                        MoveStopViewModelProperty = new CreateAndMoveStopViewModel(SelectedItem);
                        MoveStopViewModelProperty.StopModified += MoveStopViewModelProperty_StopMoved;

                        stopManager.MoveStopDialogContext(ref _MoveStopViewModelProperty, SelectedItem, DailyStopDate, MessageToken);
                    }
                    //AllCustomers.Clear();
                    IsCustomerTabActive = false;
                });
                ShowCreateStopDialog = new DelegateCommand((param) =>
                {

                    IsCreateStopButtonIsEnabled = false;
                    IsMoveStopButtonEnabled = false;
                    IsBusy = true;
                    SelectedDate = DailyStopDate;
                    if (IsProspectsTabActive)
                    {
                        var visitee = ((Visitee)SelectedProspect);
                        CreateStopViewModelProperty = new CreateAndMoveStopViewModel(visitee);
                        CreateStopViewModelProperty.StopModified += MoveStopViewModelProperty_StopMoved;
                        stopManager.CreateStopDialogContext(ref _CreateStopViewModelProperty, visitee, DailyStopDate, MessageToken);
                        visitee = null;   // object dispose for Memory Optimization 
                    }
                    else
                    {
                        var visiteeObj = ((Visitee)SelectedItem);

                        CreateStopViewModelProperty = new CreateAndMoveStopViewModel(visiteeObj);
                        CreateStopViewModelProperty.StopModified += MoveStopViewModelProperty_StopMoved;

                        //Visitee visitee = SelectedItem;
                        stopManager.CreateStopDialogContext(ref _CreateStopViewModelProperty, visiteeObj, DailyStopDate, MessageToken);
                        visiteeObj = null; // object dispose for Memory Optimization 
                    }

                    //IsCreateStopButtonIsEnabled = _CreateStopViewModelProperty.IsCreateButton;
                });
                MoveStop = new DelegateCommand((param) =>
                {
                    DateTime moveToDate = Convert.ToDateTime(param);
                    if (param != null)
                    {
                        if (serviceRouteManager.MoveCustomerStop(CustomerToMove, moveToDate, DailyStopDate))
                        {
                            int index = CustomerList.IndexOf(CustomerToMove);
                            CustomerList[index].SequenceNo = -1;
                            OnPropertyChanged("CustomerList");
                        }
                    }
                });
                System.Threading.CancellationTokenSource tokenForCancelTask = new System.Threading.CancellationTokenSource();
                SearchItemOnType = new DelegateCommand((searchTerm) =>
                {
                    try
                    {
                        if (IsDailyStopTabActive)
                        {
                            SearchInList(tokenForCancelTask, searchTerm, CustomerList);
                        }
                        if (IsCustomerTabActive)
                        {
                            SearchInList(tokenForCancelTask, searchTerm, AllCustomers);
                        }
                        if (IsProspectsTabActive)
                        {
                            SearchInList(tokenForCancelTask, searchTerm, ProspectList);
                        }
                    }
                    catch (Exception ex)
                    {

                        Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][SearchItemOnType][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                });
                ChangeDate = new DelegateCommand((Direction) =>
                {
                    IsBusy = true;
                    if (Direction.ToString().ToUpper().Equals("PREV"))
                    {
                        _DailyStopDate = _DailyStopDate.AddDays(-1);
                        //************************************************************************************************
                        // Comment: Commented to enable sunday and saturday.
                        // created: Dec 25, 2015
                        // Author: Vivensas (Rajesh,Yuvaraj)
                        // Revisions: 
                        //*************************************************************************************************

                        //if (_DailyStopDate.DayOfWeek.ToString() == "Sunday")
                        //{
                        //    _DailyStopDate = _DailyStopDate.AddDays(-2);
                        //}
                        //if (_DailyStopDate.DayOfWeek.ToString() == "Saturday")
                        //{
                        //    _DailyStopDate = _DailyStopDate.AddDays(-1);
                        //}

                        //*************************************************************************************************
                        // Vivensas changes ends over here
                        //**************************************************************************************************
                    }
                    else
                    {
                        _DailyStopDate = _DailyStopDate.AddDays(1);
                        //************************************************************************************************
                        // Comment: Commented to enable sunday and saturday.
                        // created: Dec 25, 2015
                        // Author: Vivensas (Rajesh,Yuvaraj)
                        // Revisions: 
                        //*************************************************************************************************
                        //if (_DailyStopDate.DayOfWeek.ToString() == "Sunday")
                        //{
                        //    _DailyStopDate = _DailyStopDate.AddDays(1);
                        //}
                        //if (_DailyStopDate.DayOfWeek.ToString() == "Saturday")
                        //{
                        //    _DailyStopDate = _DailyStopDate.AddDays(2);
                        //}

                        //*************************************************************************************************
                        // Vivensas changes ends over here
                        //**************************************************************************************************
                    }

                    DailyStopDisplayDate = _DailyStopDate.ToString("dddd', 'M'/'dd'/'yyyy ");
                    LoadCustomers(_DailyStopDate);

                    if (DailyStopDate.Date == DateTime.Today.Date)
                    {
                        dailyStopItems = CustomerList;
                    }
                    LoadDataStatistics();
                    IsMoveStopButtonEnabled = false;
                    ClearSearchText.Execute(null);
                    isFirstCallToCalendarDateChanged = true;
                });
                //ViewStopSequencing = new DelegateCommand((param) =>
                //{

                //});
                CalendarDateChanged = new DelegateCommand((param) =>
                {
                    try
                    {

                        IsBusy = true;
                        DateTime date = Convert.ToDateTime(param);
                        //if (!isFirstCallToCalendarDateChanged)
                        //{
                        LoadCustomers(DailyStopDate);
                        //}
                        //isFirstCallToCalendarDateChanged = false;
                        IsBusy = false;
                    }
                    catch (Exception ex)
                    {

                        Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][CalendarDateChanged][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                });
                ShowCustomerStopSequencing = new DelegateCommand((param) =>
                {
                    try
                    {
                        ClearSearchText.Execute(null);
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
                        //SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Service Route";
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Customer Stop Sequencing";
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.ServiceRoute;
                        SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.CustomerStopSequencingView;
                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerStopSequencingView, CurrentViewName = ViewModelMappings.View.ServiceRoute, CloseCurrentView = true, Payload = null, ShowAsModal = false };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    }
                    catch (Exception ex)
                    {

                        Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][ShowCustomerStopSequencing][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                });
                DragOverItem = new DelegateCommand((param) =>
                {
                    HasDragged = true;
                });

                Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:InitializeCommands]");
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][InitializeCommands][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
        }
        void ProspectToAdd_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:ProspectToAdd_PropertyChanged]");
            var property = ProspectToAdd.GetType().GetProperty(e.PropertyName);

            var value = property.GetValue(ProspectToAdd, null);

            if (!(property.Name.ToLower() == "isvalid"))
                if (!string.IsNullOrEmpty(value.ToString()))
                    IsDirty = true;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:ProspectToAdd_PropertyChanged]");
        }
        private void SearchInList(System.Threading.CancellationTokenSource tokenForCancelTask, object searchTerm, ObservableCollection<Customer> List)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:SearchInList]");
            try
            {
                if (IsDailyStopTabActive)
                {
                    if (string.IsNullOrEmpty(searchTerm.ToString()))
                    {
                        foreach (Customer cust in List)
                        {
                            cust.ItemVisible = true;// cust.Name.ToUpper().Contains(searchTerm.ToString().ToUpper()) || cust.CustomerNo.ToUpper().Contains(searchTerm.ToString().ToUpper()) ? true : false;
                            cust.SaleStatus = string.IsNullOrEmpty(cust.SaleStatus) ? "" : cust.SaleStatus;
                            if ((cust.PendingActivity > 0 || cust.CompletedActivity > 0 || cust.SaleStatus.Trim().ToLower() == "nosale") && IsDailyStopTabActive)
                            {
                                cust.IsAllowCaptureDrag = false;
                            }
                            else if (IsDailyStopTabActive)
                            {
                                cust.IsAllowCaptureDrag = true;
                            }
                        }

                    }
                    Task.Delay(100, tokenForCancelTask.Token);
                    if (searchTerm != null && searchTerm.ToString().Length < 3)
                    {
                        return;
                    }
                    CustomerListBackUpForSearch.Clear();
                    foreach (Customer cust in List)
                    {
                        cust.ItemVisible = cust.Name.ToUpper().Contains(searchTerm.ToString().ToUpper()) || cust.CustomerNo.ToUpper().Contains(searchTerm.ToString().ToUpper()) ? true : false;
                        if (cust.ItemVisible && IsDailyStopTabActive)
                        {
                            cust.IsAllowCaptureDrag = false;
                        }
                        CustomerListBackUpForSearch.Add(cust);
                    }
                    CustomerList.Clear();
                    foreach (Customer item in CustomerListBackUpForSearch)
                    {
                        CustomerList.Add(item);
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(searchTerm.ToString()))
                    {
                        foreach (Customer cust in List)
                        {
                            cust.ItemVisible = true;// cust.Name.ToUpper().Contains(searchTerm.ToString().ToUpper()) || cust.CustomerNo.ToUpper().Contains(searchTerm.ToString().ToUpper()) ? true : false;
                            cust.SaleStatus = string.IsNullOrEmpty(cust.SaleStatus) ? "" : cust.SaleStatus;
                        }

                    }
                    Task.Delay(100, tokenForCancelTask.Token);
                    if (searchTerm != null && searchTerm.ToString().Length < 3)
                    {
                        return;
                    }
                    foreach (Customer cust in List)
                    {
                        cust.ItemVisible = cust.Name.ToUpper().Contains(searchTerm.ToString().ToUpper()) || cust.CustomerNo.ToUpper().Contains(searchTerm.ToString().ToUpper()) ? true : false;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("ServiceRoute ServiceRoute([SearchInList] ) error: " + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:SearchInList]");
        }
        private void SearchInList(System.Threading.CancellationTokenSource tokenForCancelTask, object searchTerm, ObservableCollection<Prospect> List)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:SearchInList]");
            try
            {
                if (IsDailyStopTabActive)
                {
                    if (string.IsNullOrEmpty(searchTerm.ToString()))
                    {
                        foreach (Prospect cust in List)
                        {
                            cust.ItemVisible = true;// cust.Name.ToUpper().Contains(searchTerm.ToString().ToUpper()) || cust.CustomerNo.ToUpper().Contains(searchTerm.ToString().ToUpper()) ? true : false;

                        }

                    }
                    Task.Delay(100, tokenForCancelTask.Token);
                    if (searchTerm != null && searchTerm.ToString().Length < 3)
                    {
                        return;
                    }
                    ProsprctListBackUpForSearch.Clear();
                    foreach (Prospect cust in List)
                    {
                        cust.ItemVisible = cust.Name.ToUpper().Contains(searchTerm.ToString().ToUpper()) || cust.ProspectID.ToUpper().Contains(searchTerm.ToString().ToUpper()) ? true : false;

                        ProsprctListBackUpForSearch.Add(cust);
                    }
                    CustomerList.Clear();
                    foreach (Prospect item in ProsprctListBackUpForSearch)
                    {
                        ProspectList.Add(item);
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(searchTerm.ToString()))
                    {
                        foreach (Prospect cust in List)
                        {
                            cust.ItemVisible = true;// cust.Name.ToUpper().Contains(searchTerm.ToString().ToUpper()) || cust.CustomerNo.ToUpper().Contains(searchTerm.ToString().ToUpper()) ? true : false;
                        }

                    }
                    Task.Delay(100, tokenForCancelTask.Token);
                    if (searchTerm != null && searchTerm.ToString().Length < 3)
                    {
                        return;
                    }
                    foreach (Prospect cust in List)
                    {
                        cust.ItemVisible = cust.Name.ToUpper().Contains(searchTerm.ToString().ToUpper()) || cust.ProspectID.ToUpper().Contains(searchTerm.ToString().ToUpper()) ? true : false;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("ServiceRoute ServiceRoute([SearchInList] ) error: " + ex.Message);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:SearchInList]");
        }
        private void SetPayload(object param)
        {
            SelectedItem = (Customer)param;
        }
        private void Initialize()
        {
            InitializeNavigationHeaders();
        }
        public void RefreshStops(DateTime date)
        {
            IsBusy = true;
            LoadCustomers(date);
        }
        async void GetCustomerList(Action action)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:GetCustomerList]");
            await Task.Run(() =>
            {
                if (AllCustomers.Count == 0)
                {
                    IsBusy = true;
                    try
                    {
                        AllCustomers = customerManager.GetCustomersForRoute(Route);
                        LoadDataStatistics();
                    }
                    catch (Exception ex)
                    {
                        IsBusy = false;
                        Logger.Error("[SalesLogicExpress.Application.ViewModels][ServiceRoute][GetCustomerList][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                   
                    if (action != null)
                    {
                        action();
                    }
                }
                IsBusy = false;
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:GetCustomerList]");
            });
        }
        async void GetProspectList(bool IsRefresh = false)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:GetProspectList]");
            await Task.Run(() =>
            {
                if (ProspectList.Count == 0 || IsRefresh)
                {
                    ProspectList = new ProspectManager().GetProspectsForRoute(Route);
                    OnPropertyChanged("ProspectsCount");
                }
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                {

                }));
                IsBusy = false;
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:GetProspectList]");
            });
        }
        void LoadCustomers(DateTime date)
        {
            //await Task.Run(() =>
            //{
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:LoadCustomers]");
                try
                {

                    IsBusy = true;

                    System.Diagnostics.Debug.WriteLine("LoadCustomers " + date.Millisecond);
                    CustomerList = serviceRouteManager.GetCustomersForRoute(CommonNavInfo.RouteUser, date);
                    listCount = CustomerList.Count;
                    CustomerList.CollectionChanged += CustomerList_CollectionChanged;
                    DailyStopDate = date;
                    Dictionary<string, float> dict = new Dictionary<string, float>();
                    dict = serviceRouteManager.GetDataStatistics(DailyStopDate);
                    if (dict.Count > 0)
                    {
                        TotalSales = dict["TotalSale"].ToString("0.00");
                        TotalCoffee = dict["TotalCoffee"].ToString("0.00");
                        TotalAllied = dict["TotalAlliedAmt"].ToString("0.00");
                    }

                    TotalCoffeePound = "0.00";
                    TotalReturn = "0.00";
                    TotalCoffeePound = serviceRouteManager.GetCoffeePoundsSoldForDataStatistics(CommonNavInfo.RouteID.ToString(), date);
                    TotalReturn = (serviceRouteManager.GetTotalReturnAmount(CommonNavInfo.RouteID, date)).ToString("0.00");

                    OnPropertyChanged("CustomerList");
                    LoadDataStatistics();
                    if (DailyStopDate.Date == DateTime.Today.Date)
                    {
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                    new Action(() =>
                    {
                        dailyStopItems.Clear();
                        foreach (var cust in CustomerList)
                            dailyStopItems.Add(cust.DeepCopy());
                    }
                ));


                    }
                    IsMoveStopButtonEnabled = false;
                    IsBusy = false;
                }

                catch (Exception ex)
                {
                    IsBusy = false;
                    Logger.Error("ServiceRoute ServiceRoute([LoadCustomers]" + SelectedDate.ToString() + ") error: " + ex.Message);
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:LoadCustomers]");
            //});
        }
        void CustomerList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:CustomerList_CollectionChanged]");
            if (CustomerList.Count == listCount && IsDailyStopTabActive)
            {
                //Update sequencing
                int seqNo = 1;
                for (int i = 0; i < CustomerList.Count; i++)
                {
                    try
                    {
                        // Update sequencing logic for planned customers only. Other customers will be treated as 0. Here TempSequenceNo is for show on interface purpose only.
                        //Not saving in database.
                        if (CustomerList[i].StopType.ToLower() == "planned")
                        {
                            CustomerList[i].TempSequenceNo = seqNo;
                            seqNo++;
                        }
                        else
                        {
                            CustomerList[i].TempSequenceNo = 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("ServiceRoute ServiceRoute([CustomerList_CollectionChanged] ) error: " + ex.Message);
                    }
                }
                string userName = CommonNavInfo.UserName.Trim();
                for (int i = 0; i < CustomerList.Count; i++)
                {
                    ServiceRouteManager objSrm = new ServiceRouteManager();
                    string stopID = CustomerList[i].StopID.Trim();
                    string refStopID = CustomerList[i].ReferenceStopId.Trim();
                    objSrm.UpadateSequenceNo(stopID, (i + 1).ToString(), userName, DailyStopDate.Date.ToString("yyyy-MM-dd"), refStopID);
                }
                var result = CustomerList.Where(x => x.PendingActivity > 0 || x.CompletedActivity > 0 || x.SaleStatus.Trim().ToLower() == "nosale").OrderBy(x => x.ActivityTimeIndex);

                ObservableCollection<Customer> activityCustomers = new ObservableCollection<Customer>(result);
                int indexOfActivity = 0;//CustomerList.IndexOf(CustomerList.FirstOrDefault(x => x.CustomerNo.Trim() == activityCustomers[0].CustomerNo.Trim()));
                if (activityCustomers.Count > 0)
                {
                    indexOfActivity = CustomerList.IndexOf(CustomerList.FirstOrDefault(x => x.CustomerNo.Trim() == activityCustomers[0].CustomerNo.Trim()));
                }
                int idx = 0;
                foreach (Customer item in CustomerList)
                {

                    if (idx < indexOfActivity)
                    {
                        item.IsAllowDrop = false;
                    }
                    else if (idx > indexOfActivity)
                    {
                        item.IsAllowDrop = true;
                    }
                    if (item.PendingActivity > 0 || item.CompletedActivity > 0 || item.SaleStatus.Trim().ToLower() == "nosale")
                    {
                        item.IsAllowDrop = false;
                    }
                    idx++;
                }
                try
                {
                    if (!(dailyStopItems.Count() == 0))
                        dailyStopItems.Clear();
                }
                catch (Exception ex)
                {
                    Logger.Error("ServiceRoute ServiceRoute([CustomerList_CollectionChanged] ) error: " + ex.Message);
                }

            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][End:CustomerList_CollectionChanged]");
            IsMoveStopButtonEnabled = false;
            hasDragged = false;
        }
        async void GetAllCustomers()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][Start:GetAllCustomers]");

            await Task.Run(() =>
            {
                //IsBusy = true;
                LoadCustomers(ServiceRoute.SelectedCalendarDate);
                //AllCustomers = customerManager.GetCustomersForRoute(Route);
                LoadDataStatistics();
                OnPropertyChanged("AllCustomers");
                // IsBusy = false;
            });
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ServiceRoute][END:GetAllCustomers]");
        }
        private void InitializeNavigationHeaders()
        {
        }
        public override bool ConfirmSave()
        {
            return base.ConfirmSave();

        }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }


    public class CollectionData
    {
        public CollectionData(string a, int b)
        {
            OrderState = a;
            OrderCount = b;
        }
        public string OrderState { set; get; }
        public int OrderCount { get; set; }
    }
}
