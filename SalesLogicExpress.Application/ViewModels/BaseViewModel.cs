﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Domain;
using System;
using System.ComponentModel;
using System.Diagnostics;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class BaseViewModel : ViewModelBase
    {
        private static readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.BaseViewModel");

        public BaseViewModel()
        {
            SalesLogicExpress.Application.Helpers.ResourceManager.Synchronization.SyncProgressChanged -= Synchronization_SyncProgressChanged;
            SalesLogicExpress.Application.Helpers.ResourceManager.Synchronization.SyncProgressChanged += Synchronization_SyncProgressChanged;
        }

        void Synchronization_SyncProgressChanged(object sender, SyncUpdatedEventArgs e)
        {
            OnSyncProgressChanged(e);
        }
        protected virtual void OnSyncProgressChanged(SyncUpdatedEventArgs e)
        {

        }
        private static bool isDirtyStatic;
        protected virtual void OnModelStateDirty()
        {

        }
        BaseViewModel _ParentView;
        public BaseViewModel ParentView
        {
            get
            {
                return _ParentView;
            }
            set
            {
                _ParentView = value;
                OnPropertyChanged("ParentView");
            }
        }
        public static bool IsDirtyStatic
        {
            get { return BaseViewModel.isDirtyStatic; }
            set
            {
                BaseViewModel.isDirtyStatic = value;
                if (BaseViewModel.isDirtyStatic)
                {
                    var stackTrace = new StackTrace();
                    var methodBase = stackTrace.GetFrame(1).GetMethod();
                    //Console.WriteLine(methodBase.Name);
                    Logger.Warn("isDirtyStatic = true, Stake trace = " + stackTrace.ToString());
                }
                NotifyStaticPropertyChanged("IsDirtyStatic");
            }
        }
        private bool _IsDirty;
        public bool IsDirty
        {
            get
            {
                return _IsDirty;
            }
            set
            {
                _IsDirty = value;
                if (_IsDirty)
                {
                    var stackTrace = new StackTrace();
                    var methodBase = stackTrace.GetFrame(1).GetMethod();
                    //Console.WriteLine(methodBase.Name);
                    Logger.Warn("_IsDirty = true, Stake trace = " + stackTrace.ToString());
                }
                OnPropertyChanged("IsDirty");
                OnModelStateDirty();
            }
        }
        public virtual bool ConfirmSave()
        {
            return CanNavigate;
        }
        public virtual bool ConfirmCancel()
        {
            return CanNavigate;
        }
        public virtual bool ConfirmClosed()
        {
            return false;
        }

        public virtual bool OnActivate()
        {
            return CanNavigate;
        }
        bool cannavigate = true;
        public bool CanNavigate { get { return cannavigate; } set { cannavigate = value; } }
        public string Message { get; set; }
        public bool IsVoidClicked { get; set; }
        public string OkContent { get; set; }
        public string CancelContent { get; set; }
        public Guid MessageToken { get; set; }

        private bool _IsBusy;
        public bool IsBusy
        {
            get
            {
                return _IsBusy;
            }
            set
            {
                _IsBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        private bool _IsInTransition;
        public bool IsInTransition
        {
            get
            {
                return _IsInTransition;
            }
            set
            {
                _IsInTransition = value;
                OnPropertyChanged("IsInTransition");
            }
        }


        private string selectedTab = string.Empty;

        public string SelectedTab
        {
            get { return selectedTab; }
            set { selectedTab = value; OnPropertyChanged("SelectedTab"); }
        }



        public event EventHandler<ModelChangeArgs> ModelChanged;
        protected virtual void OnModelChanged(ModelChangeArgs e)
        {
            EventHandler<ModelChangeArgs> handler = ModelChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public class ModelChangeArgs : EventArgs
        {
            public DateTime StateChangedTime { get; set; }
        }

        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void NotifyStaticPropertyChanged(string propertyName)
        {
            if (StaticPropertyChanged != null)
                StaticPropertyChanged(null, new PropertyChangedEventArgs(propertyName));
        }

        #region Moved code from individual ViewModels to this base class to generalize the clear/search functionality

        private string _SearchText;     //Variable to hold the search text property value 

        /// <summary>
        /// Get or set the search text 
        /// </summary>
        public string SearchText
        {
            get
            {
                return _SearchText;
            }
            set
            {
                _SearchText = value;
                OnPropertyChanged("SearchText");
                OnPropertyChanged("ShowLookupCancelButton");
                OnPropertyChanged("ShowLookupSearchButton");
                OnPropertyChanged("DefaultClearSearchText");
            }
        }

        private string _ShowLookupCancelButton = "Hidden";      //Variable to hold cancel icon visibility value 
        /// <summary>
        /// Get or set cancel button visibility value based on search text 
        /// </summary>
        public string ShowLookupCancelButton
        {
            get
            {
                if (string.IsNullOrEmpty(this.SearchText))
                    _ShowLookupCancelButton = "Hidden";
                else
                    _ShowLookupCancelButton = "Visible";

                return _ShowLookupCancelButton;
            }
            set
            {
                _ShowLookupCancelButton = value;
                OnPropertyChanged("ShowLookupCancelButton");
            }
        }

        private string _ShowLookupSearchButton = "Hidden";   //Variable to hold the search icon visibility value 

        /// <summary>
        /// Get or set search button visibility value based on search text 
        /// </summary>
        public string ShowLookupSearchButton
        {
            get
            {
                if (string.IsNullOrEmpty(this.SearchText))
                {
                    _ShowLookupSearchButton = "Visible";
                }
                else
                {
                    _ShowLookupSearchButton = "Hidden";
                }

                return _ShowLookupSearchButton;
            }
            set
            {
                _ShowLookupSearchButton = value;
                OnPropertyChanged("ShowLookupSearchButton");
            }
        }

        private string _DefaultClearSearchText = "";//Variable holds the default watermark text 

        /// <summary>
        /// Get or set deault watermark search text based on search text 
        /// </summary>
        public string DefaultClearSearchText
        {
            get
            {
                if (string.IsNullOrEmpty(this.SearchText))
                    _DefaultClearSearchText = "Enter search text ...";
                else
                    _DefaultClearSearchText = "";

                return _DefaultClearSearchText;
            }
            set
            {
                _DefaultClearSearchText = value;
                OnPropertyChanged("DefaultClearSearchText");
            }
        }
        public bool? ShowConfirmDialog(string Description, object Token)
        {
            string flag = string.Empty;
            var confirmMessage = new Helpers.ConfirmWindow { Message = Description, OkButtonContent = "Yes", CancelButtonContent = "No", MessageIcon = "Alert", Confirmed = false };
            Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, Token);
            if (confirmMessage.Confirmed)
            {
                ConfirmSave();
                return true;
            }
            if (confirmMessage.Cancelled)
            {
                ConfirmCancel();
                return false;
            }
            return null;
        }
        #endregion



        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
