﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Domain;
using Telerik.Windows.Controls;
using log4net;

namespace SalesLogicExpress.Application.ViewModels
{
    public class ExpensesViewModel : BaseViewModel, IDataErrorInfo
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.ExpensesViewModel");

        private readonly ExpensesManager expenseManager = new ExpensesManager();
        private readonly System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        private Guid messageToken = new System.Guid();
        private string routeID = string.Empty;
        private string routeName;
        private string route;
        private string userRoute;
        private Expenses expensesObject = new Domain.Expenses();
        private ObservableCollection<Expenses> expensesList = new ObservableCollection<Domain.Expenses>();

        private ObservableCollection<ReasonCode> reasonCode = new ObservableCollection<ReasonCode>();
        private ObservableCollection<ExpenseCategory> expenseCategoryList = new ObservableCollection<ExpenseCategory>();

        private string cashOnHand = string.Empty;

        private string amount = string.Empty;
        private string explanation = string.Empty;
        private int selectedCategory;
        private int selectedReasonCode = -1;
        private bool isEditEnabled = false;
        private bool isVoidEnabled = false;
        private bool isAddEnabled = false;
        private bool toggleReasonCodeButton = false;
        private bool toggleAddButton;
        private bool isValidAmount = true;
        private bool isMoveDialogOpened;
        private bool disposed = false;

        public ExpensesViewModel()
        {
            this.InitializeCommands();
            this.LoadData();
        }

        public Guid MessageToken
        {
            get
            {
                return this.messageToken;
            }
            set
            {
                this.messageToken = value;
                this.OnPropertyChanged("MessageToken");
            }
        }

        public string RouteID
        {
            get
            {
                return this.routeID;
            }
            set
            {
                this.routeID = value;
                this.OnPropertyChanged("RouteID");
            }
        }

        public string RouteName
        {
            get
            {
                return this.routeName;
            }
            set
            {
                this.routeName = value;
                this.OnPropertyChanged("RouteName");
            }
        }

        public string Route
        {
            get
            {
                return this.route;
            }
            set
            {
                this.route = value;
                this.OnPropertyChanged("Route");
            }
        }

        public string UserRoute
        {
            get
            {
                return this.userRoute;
            }
            set
            {
                this.userRoute = value;
                this.OnPropertyChanged("UserRoute");
            }
        }

        public Expenses ExpensesObject
        {
            get
            {
                return this.expensesObject;
            }
            set
            {
                this.expensesObject = value;
                this.OnPropertyChanged("ExpensesObject");
            }
        }

        public ObservableCollection<Expenses> ExpensesList
        {
            get
            {
                return this.expensesList;
            }
            set
            {
                this.expensesList = value;
                this.OnPropertyChanged("ExpensesList");
            }
        }

        public ObservableCollection<ReasonCode> ReasonCode
        {
            get
            {
                return this.reasonCode;
            }
            set
            {
                this.reasonCode = value;
                this.OnPropertyChanged("ReasonCode");
            }
        }

        public ObservableCollection<ExpenseCategory> ExpenseCategoryList
        {
            get
            {
                return this.expenseCategoryList;
            }
            set
            {
                this.expenseCategoryList = value;
                this.OnPropertyChanged("ExpenseCategoryList");
            }
        }

        public string CashOnHand
        {
            get
            {
                return this.cashOnHand;
            }
            set
            {
                this.cashOnHand = value;
                this.OnPropertyChanged("CashOnHand");
            }
        }

        public string Amount
        {
            get
            {
                return this.amount;
            }
            set
            {
                this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][Start:Amount]");
                try
                {
                    this.amount = value;
                    this.isValidAmount = false;
                    if (!string.IsNullOrEmpty(this.Amount) && (Convert.ToDecimal(0.00) < Convert.ToDecimal(this.Amount)) && Convert.ToDecimal(this.Amount) <= Convert.ToDecimal(this.CashOnHand))
                    {
                        this.isValidAmount = true;
                        if (!string.IsNullOrEmpty(this.Explanation.Trim()) && this.SelectedCategory > 0)
                        {
                            this.ToggleAddButton = true;
                        }
                        else
                        {
                            this.ToggleAddButton = false;
                        }
                    }
                    else
                    {
                        this.ToggleAddButton = false;
                    }
                    this.OnPropertyChanged("Amount");
                }
                catch (Exception ex)
                {
                    this.Logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][Amount][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
                }
                this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][End:Amount]");
            }
        }

        public string Explanation
        {
            get
            {
                return this.explanation;
            }
            set
            {
                this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][Start:Explanation]");
                try
                {
                    this.explanation = value;
                    if (!string.IsNullOrEmpty(this.explanation.Trim()))
                    {
                        if (!string.IsNullOrEmpty(this.Amount) && this.isValidAmount && this.SelectedCategory > 0)
                        {
                            this.ToggleAddButton = true;
                        }
                        else
                        {
                            this.ToggleAddButton = false;
                        }
                    }
                    else
                    {
                        this.ToggleAddButton = false;
                    }
                    this.OnPropertyChanged("Explanation");
                }
                catch (Exception ex)
                {
                    this.Logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][Explanation][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
                }
                this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][End:Explanation]");
            }
        }

        public int SelectedCategory
        {
            get
            {
                return this.selectedCategory;
            }
            set
            {
                this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][Start:SelectedCategory]");
                try
                {
                    this.selectedCategory = value;
                    if (this.selectedCategory > 0)
                    {
                        if (!string.IsNullOrEmpty(this.Amount) && this.isValidAmount && !string.IsNullOrEmpty(this.Explanation.Trim()))
                        {
                            this.ToggleAddButton = true;
                        }
                        else
                        {
                            this.ToggleAddButton = false;
                        }
                    }
                    else
                    {
                        this.ToggleAddButton = false;
                    }
                    this.OnPropertyChanged("SelectedCategory");
                }
                catch (Exception ex)
                {
                    this.Logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][SelectedCategory][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
                }
                this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][End:SelectedCategory]");
            }
        }

        public int SelectedReasonCode
        {
            get
            {
                return this.selectedReasonCode;
            }
            set
            {
                this.selectedReasonCode = value;
                if (this.selectedReasonCode > 0)
                {
                    this.ToggleReasonCodeButton = true;
                }
                else
                {
                    this.ToggleReasonCodeButton = false;
                }
                this.OnPropertyChanged("SelectedReasonCode");
            }
        }

        public bool IsEditEnabled
        {
            get
            {
                return this.isEditEnabled;
            }
            set
            {
                this.isEditEnabled = value;
                this.OnPropertyChanged("IsEditEnabled");
            }
        }

        public bool IsVoidEnabled
        {
            get
            {
                return this.isVoidEnabled;
            }
            set
            {
                this.isVoidEnabled = value;
                this.OnPropertyChanged("IsVoidEnabled");
            }
        }

        public bool IsAddEnabled
        {
            get
            {
                return this.isAddEnabled;
            }
            set
            {
                this.isAddEnabled = value;
                this.OnPropertyChanged("IsAddEnabled");
            }
        }

        public bool ToggleReasonCodeButton
        {
            get
            {
                return this.toggleReasonCodeButton;
            }
            set
            {
                this.toggleReasonCodeButton = value;
                this.OnPropertyChanged("ToggleReasonCodeButton");
            }
        }

        public bool ToggleAddButton
        {
            get
            {
                return this.toggleAddButton;
            }
            set
            {
                this.toggleAddButton = value;
                this.OnPropertyChanged("ToggleAddButton");
            }
        }

        public bool IsMoveDialogOpened
        {
            get
            {
                return this.isMoveDialogOpened;
            }
            set
            {
                this.isMoveDialogOpened = value;
                this.OnPropertyChanged("IsMoveDialogOpened");
            }
        }

        public string Error
        {
            get
            {
                return null;
                //throw new NotImplementedException(); 
            }
        }

        public DelegateCommand OpenEditDialogForExpense { get; set; }

        public DelegateCommand OpenVoidDialogForExpense { get; set; }

        public DelegateCommand OpenAddDialogForExpense { get; set; }

        public DelegateCommand SelectionItemInvoke { get; set; }

        public DelegateCommand VoidExpenses { get; set; }

        public DelegateCommand SaveNewExpense { get; set; }

        public DelegateCommand SaveEditedExpense { get; set; }

        public string this[string columnName]
        {
            get
            {
                string result = string.Empty;
                if (columnName == "Amount" && !this.isValidAmount)
                {
                    result = Helpers.Constants.Common.ExpenseAmountErrorMsg;
                }
                return result;
            }
        }

        public bool ValidateCategory()
        {
            return this.ToggleAddButton;
        }

        public void InitializeCommands()
        {
            this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][Start:InitializeCommands]");

            this.OpenAddDialogForExpense = new DelegateCommand((param) =>
            {
                this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][Start:OpenAddDialogForExpense]");
                try
                {
                    //Issue ID- 101- While Settlement In progress if we adding new Money order, Expenses and system should ask Void the previous settlement. Once done newly added Money order, Cash or Checks should reflect in Settlement.- Zakir
                    bool flag = SettlementConfirmationManager.IsPendingVerification();
                    if (flag)
                    {
                        //Alert message for pending verification
                        var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.EODConfirmedValidation, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, this.MessageToken);
                    }
                    else
                    {
                        this.ExpensesObject.ExpenseAmount = string.Empty;
                        this.ExpensesObject.Explanation = string.Empty;
                        this.ExpenseCategoryList = this.expenseManager.GetCategoryForExpenses();
                        this.SelectedCategory = -1;
                        this.Amount = string.Empty;
                        this.Explanation = string.Empty;
                        this.ToggleAddButton = false;
                        this.isValidAmount = true;
                        this.IsMoveDialogOpened = true;
                        var dialogWindow = new Helpers.DialogWindow { TemplateKey = "AddExpenseDialog", Title = "Add New Expense" };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialogWindow, this.MessageToken);
                    }
                }
                catch (Exception ex)
                {
                    this.Logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][OpenAddDialogForExpense][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
                }
                this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][End:OpenAddDialogForExpense]");
            });

            this.OpenVoidDialogForExpense = new DelegateCommand((param) =>
            {
                //Issue ID- 101- While Settlement In progress if we adding new Money order, Expenses and system should ask Void the previous settlement. Once done newly added Money order, Cash or Checks should reflect in Settlement.- Zakir
                bool flag = SettlementConfirmationManager.IsPendingVerification();
                if (flag)
                {
                    //Alert message for pending verification
                    var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.EODConfirmedValidation, MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, this.MessageToken);
                }
                else
                {
                    this.ReasonCode = this.expenseManager.GetVoidReasonCode();
                    this.SelectedReasonCode = -1;
                    this.ToggleReasonCodeButton = false;
                    var dialogWindow = new Helpers.DialogWindow { TemplateKey = "VoidExpenseDialog", Title = "Void Expense" };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialogWindow, this.MessageToken);
                }
            });

            this.OpenEditDialogForExpense = new DelegateCommand((param) =>
            {
                this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][Start:OpenEditDialogForExpense]");
                try
                {
                    //Issue ID- 101- While Settlement In progress if we adding new Money order, Expenses and system should ask Void the previous settlement. Once done newly added Money order, Cash or Checks should reflect in Settlement.- Zakir
                    bool flag = SettlementConfirmationManager.IsPendingVerification();
                    if (flag)
                    {
                        //Alert message for pending verification
                        var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.EODConfirmedValidation, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, this.MessageToken);
                    }
                    else
                    {
                        this.CashOnHand = (Convert.ToDecimal(this.CashOnHand) + Convert.ToDecimal(this.ExpensesObject.ExpenseAmount)).ToString("F2");
                        this.Explanation = string.IsNullOrEmpty(this.ExpensesObject.Explanation) ? "" : this.ExpensesObject.Explanation.Trim();
                        this.Amount = string.IsNullOrEmpty(this.ExpensesObject.ExpenseAmount) ? "0.00" : Convert.ToDecimal(this.ExpensesObject.ExpenseAmount).ToString("F2").Trim();
                        this.ExpenseCategoryList = this.expenseManager.GetCategoryForExpenses();
                        this.SelectedCategory = Convert.ToInt32(this.ExpensesObject.CategotyID);
                        this.ToggleAddButton = false;
                        var dialogWindow = new Helpers.DialogWindow { TemplateKey = "EditExpenseDialog", Title = "Edit Expense" };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialogWindow, this.MessageToken);

                        if (dialogWindow.Cancelled)
                        {
                            this.CashOnHand = (Convert.ToDecimal(this.CashOnHand) - Convert.ToDecimal(this.ExpensesObject.ExpenseAmount)).ToString("F2");
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.Logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][OpenEditDialogForExpense][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
                }
                this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][End:OpenEditDialogForExpense]");
            });

            this.VoidExpenses = new DelegateCommand((param) =>
            {
                this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][Start:VoidExpenses]");
                try
                {
                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), this.MessageToken);
                    this.ExpensesObject.VoidReasonID = this.SelectedReasonCode.ToString();
                    string statusID = this.expenseManager.UpdateExpenses(this.ExpensesObject, false).ToString();

                    this.ExpensesObject.StatusID = string.IsNullOrEmpty(statusID.Trim()) ? this.ExpensesObject.StatusID : statusID.Trim();

                    this.ExpensesObject.Status = "VOID";
                    this.ExpensesObject.StatusCode = StatusTypesEnum.VOID.ToString();
                    this.LogActivityForExpense(ActivityKey.VoidExpense);
                    this.CashOnHand = (Convert.ToDecimal(this.CashOnHand) + Convert.ToDecimal(this.ExpensesObject.ExpenseAmount)).ToString("F2").Trim();
                    this.IsEditEnabled = this.IsVoidEnabled = false;
                    this.GetExpenseList();
                }
                catch (System.Exception ex)
                {
                    this.Logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][VoidExpenses][ExceptionStackTrace = {0}][Exception Message={1}]", ex.StackTrace, ex.Message));
                }
                this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][End:VoidExpenses]");
            });

            this.SaveNewExpense = new DelegateCommand((param) =>
            {
                this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][Start:SaveNewExpense]");
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), this.MessageToken);
                try
                {
                    this.ExpensesObject.Explanation = string.IsNullOrEmpty(this.Explanation) ? "" : this.Explanation.Trim();
                    this.ExpensesObject.ExpenseAmount = string.IsNullOrEmpty(this.Amount) ? "0.00" : (Convert.ToDecimal(this.Amount)).ToString("F2");
                    this.ExpensesObject.CategotyID = this.SelectedCategory.ToString();
                    foreach (ExpenseCategory item in this.ExpenseCategoryList)
                    {
                        if (item.CategoryID == this.SelectedCategory)
                        {
                            this.ExpensesObject.ExpenseCategory = item.Description;
                            break;
                        }
                    }

                    this.ExpensesObject.ExpenseDate = DateTime.Now;
                    this.ExpensesObject.VoidReasonID = string.Empty;
                    var obj = new Domain.Expenses();
                    obj = this.expenseManager.AddExpenses(this.ExpensesObject);
                    if (obj != null)
                    {
                        this.ExpensesObject.StatusID = obj.StatusID;
                        this.ExpensesObject.ExpenseID = obj.ExpenseID;
                    }
                    this.ExpensesObject.Status = "UNSETTLED";
                    this.ExpensesObject.StatusCode = StatusTypesEnum.USTLD.ToString();
                    this.LogActivityForExpense(ActivityKey.CreateExpense);
                    this.IsMoveDialogOpened = false;

                    this.GetExpenseList();

                    this.CashOnHand = (Convert.ToDecimal(this.CashOnHand) - Convert.ToDecimal(this.ExpensesObject.ExpenseAmount)).ToString("F2");
                }
                catch (System.Exception ex)
                {
                    this.Logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][SaveNewExpense][ExceptionStackTrace = {0}][Exception Message={1}]", ex.StackTrace, ex.Message));
                }
                this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][End:SaveNewExpense]");
            });

            this.SaveEditedExpense = new DelegateCommand((param) =>
            {
                this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][Start:SaveEditedExpense]");
                try
                {
                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), this.MessageToken);
                    this.ExpensesObject.ExpenseAmount = string.IsNullOrEmpty(this.Amount.Trim()) ? "" : (Convert.ToDecimal(this.Amount)).ToString("F2").Trim();
                    this.ExpensesObject.Explanation = string.IsNullOrEmpty(this.Explanation.Trim()) ? "" : this.Explanation.Trim();
                    this.ExpensesObject.CategotyID = this.SelectedCategory.ToString();

                    this.expenseManager.UpdateExpenses(this.ExpensesObject, true);
                    this.CashOnHand = (Convert.ToDecimal(this.CashOnHand) - Convert.ToDecimal(this.ExpensesObject.ExpenseAmount)).ToString("F2");
                    this.LogActivityForExpense(ActivityKey.EditExpense);
                    this.IsEditEnabled = this.IsVoidEnabled = false;

                    this.GetExpenseList();
                }
                catch (System.Exception ex)
                {
                    this.Logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][SaveEditedExpense][ExceptionStackTrace = {0}][Exception Message={1}]", ex.StackTrace, ex.Message));
                }
                this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][End:SaveEditedExpense]");
            });

            this.SelectionItemInvoke = new DelegateCommand((param) =>
            {
                this.ExpensesObject = param as Expenses;

                if (this.ExpensesObject.Status == "UNSETTLED")
                {
                    this.IsEditEnabled = true;
                    this.IsVoidEnabled = true;
                }
                else if (this.ExpensesObject.Status == "VOID" || this.ExpensesObject.Status == "SETTLED" || this.ExpensesObject.Status == "VOIDED / SETTLED")
                {
                    this.IsEditEnabled = false;
                    this.IsVoidEnabled = false;
                }
                if (this.ExpensesObject.Status == "UNSETTLED" && this.ExpensesObject.IsMoneyOrderCharge)
                {
                    this.IsEditEnabled = false;
                    this.IsVoidEnabled = false;
                }
            });

            this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][End:InitializeCommands]");
        }

        public void GetExpenseList()
        {
            this.ExpensesList = new ObservableCollection<Domain.Expenses>();
            this.ExpensesList = this.expenseManager.GetExpensesList();
            //this.ExpensesList = exp;
            //foreach (Expenses item in exp)
            //{
            //    if (Convert.ToDecimal(item.ExpenseAmount) != Convert.ToDecimal(0))
            //    {
            //        this.ExpensesList.Add(item);
            //    }
            //}
            //exp.Clear();
        }

        public void CalculateCashOnHand()
        {
        }

        public void LogActivityForExpense(ActivityKey key)
        {
            this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][Start:LogActivityForExpense]");
            try
            {
                var ac = new Activity
                {
                    ActivityType = key.ToString(),
                    ActivityStart = DateTime.Now,
                    ActivityEnd = DateTime.Now,
                    IsTxActivity = true,
                    ActivityDetailClass = this.ExpensesObject.ToString(),
                    ActivityDetails = this.ExpensesObject.SerializeToJson(),
                    RouteID = this.RouteID
                };
                if (key.ToString().ToLower() == "CreateExpense".ToString().ToLower())
                {
                    ac.ActivityStatus = "USTLD";
                }
                else if (key.ToString().ToLower() == "VoidExpense".ToString().ToLower())
                {
                    ac.ActivityStatus = "VOID";
                }

                //Get the parent activity id
                ac.ActivityHeaderID = this.GetParentActivity(this.ExpensesObject.ExpenseID.ToString());
                ResourceManager.Transaction.LogActivity(ac);
            }
            catch (Exception ex)
            {
                this.Logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][LogActivityForExpense][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
            }
            this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][End:LogActivityForExpense]");
        }

        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            if (disposing)
            {
                this.handle.Dispose();
            }
            base.Dispose(disposing);
            this.disposed = true;
        }

        private async void LoadData()
        {
            await Task.Run(() =>
            {
                this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][AsyncStart:LoadData]");
                try
                {
                    this.MessageToken = CommonNavInfo.MessageToken;
                    this.RouteID = CommonNavInfo.RouteID.ToString();
                    this.UserRoute = Managers.UserManager.UserRoute;
                    this.RouteName = Managers.UserManager.RouteName;
                    this.Route = PayloadManager.ApplicationPayload.Route;
                    this.CashOnHand = PayloadManager.RouteSettlementPayload.TotalCash.ToString("F2").Trim();
                    if (Convert.ToDecimal(this.CashOnHand) > 0)
                    {
                        this.IsAddEnabled = true;
                    }
                    this.GetExpenseList();
                }
                catch (Exception ex)
                {
                    this.Logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][LoadData][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
                }
                this.Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][AsyncEnd:LoadData]");
            });
        }

        private string GetParentActivity(string expenseId)
        {
            string query = "";

            try
            {
                query = string.Format("SELECT TDID FROM BUSDTA.M50012 WHERE TDTYP='Expense' AND TDDTLS LIKE '%\"ExpenseID\":\"{0}\"%' AND TDPNTID=0", expenseId);

                return DbEngine.ExecuteScalar(query);
            }
            catch (Exception ex)
            {
                this.Logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][GetParentActivity][ExceptionStackTrace = {0}][Exception Message={1}]", ex.StackTrace, ex.Message));
                return string.Empty;
            }
        }
    }
}
