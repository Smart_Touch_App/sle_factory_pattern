﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Models = SalesLogicExpress.Domain;
using Telerik.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.ObjectModel;
using SalesLogicExpress.Domain;
using System.ComponentModel;
using System.Collections.Specialized;
using log4net;

namespace SalesLogicExpress.Application.ViewModels
{
    public class CustomerNotesViewModel : BaseViewModel, IDataErrorInfo
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CustomerNotesViewModel");
        public event EventHandler<NotesVMArgs> ViewModelUpdated;
        protected virtual void OnViewModelStateChanged(NotesVMArgs e)
        {
            EventHandler<NotesVMArgs> handler = ViewModelUpdated;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        private CustomerNotesViewModel _CustomerNote;
        public static string customerNoVM = string.Empty;
        public CustomerNotesViewModel CustomerNote
        {
            get { return _CustomerNote; }
            set { _CustomerNote = value; }
        }

        string _NoteID, _NoteDetails, _DateTime;

        public string DateTimeVM
        {
            get
            {
                return _DateTime;
            }
            set
            {
                _DateTime = value;
                OnPropertyChanged("DateTime");
            }
        }

        public string NoteDetails
        {
            get
            {
                return _NoteDetails;
            }
            set
            {
                _NoteDetails = value;
                if (string.IsNullOrEmpty(_NoteDetails.Trim()))
                    IsEmpty = true;

                else
                {
                    IsEmpty = false;
                    ToggleButton = true;
                }
                OnPropertyChanged("NoteDetails");
            }
        }

        public string NoteID
        {
            get
            {
                return _NoteID;
            }
            set
            {
                _NoteID = value;
                OnPropertyChanged("NoteID");
            }
        }

        private bool _SelectAllCheck = false;
        public bool SelectAllCheck
        {
            get
            {
                return _SelectAllCheck;
            }
            set
            {
                _SelectAllCheck = value;
                OnPropertyChanged("SelectAllCheck");
            }
        }
        private bool _ToggleButton = false;
        public bool ToggleButton
        {
            get
            {
                return _ToggleButton;
            }
            set
            {
                _ToggleButton = value;
                OnPropertyChanged("ToggleButton");
            }
        }

        #region Properties and Delegates
        public DelegateCommand AddNewNote { get; set; }
        public DelegateCommand SelectAllNotes { get; set; }
        public DelegateCommand SetDefaultNote { get; set; }
        public DelegateCommand SelectNote { get; set; }
        public DelegateCommand SaveNewNote { get; set; }
        public DelegateCommand DeleteNote { get; set; }
        public Models.Customer Customer
        {
            get;
            set;
        }
        private ObservableCollection<Models.CustomerNote> _CustomerNotesList;
        public ObservableCollection<Models.CustomerNote> CustomerNotesList
        {
            get
            {
                return _CustomerNotesList;
            }
            set
            {
                _CustomerNotesList = value;
                OnPropertyChanged("CustomerNotesList");
            }
        }
        #endregion

        #region Constrctors and Methods
        public CustomerNotesViewModel(string customerNo)
        {
            DateTime timer = DateTime.Now;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerNotesViewModel][Start:CustomerNotesViewModel]\t" + DateTime.Now + "");

            try
            {
                customerNoVM = customerNo;
                InitializeCommands();
                GetCustomerNotes();
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerNotesViewModel CustomerNotesViewModel(" + customerNo + ") error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerNotesViewModel][End:CustomerNotesViewModel]\t" + (DateTime.Now - timer) + "");

        }

        void CustomerNotesList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged("CustomerNotesList");
        }
        void InitializeCommands()
        {
            #region SetDefaultNote
            SetDefaultNote = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerNotesViewModel][Start:SetDefaultNote]");

                try
                {
                    CustomerNote selectedNote = param as CustomerNote;
                    foreach (CustomerNote note in CustomerNotesList)
                    {
                        note.IsDefault = selectedNote.NoteID == note.NoteID ? true : false;
                    }
                    selectedNote.IsDefault = true;
                    new Managers.CustomerNotesManager().SetDefaultNote(selectedNote);
                    NotesVMArgs args = new NotesVMArgs();
                    args.Note = selectedNote;
                    OnViewModelStateChanged(args);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerNotesViewModel.][SetDefaultNote][customerID =" + customerNoVM + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerNotesViewModel][End:SetDefaultNote]");
            });
            #endregion

            #region AddNewNote
            AddNewNote = new DelegateCommand((param) =>
            {
                # region Stop Related Validations
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerNotesViewModel][Start:AddNewNote]");

                try
                {
                    var dashboardVM = (ParentView as CustomerHome).CustomerDashboardVM;
                    if (!dashboardVM.IsAllCustomerTab && DateTime.Compare(dashboardVM.Customer.StopDate.Value.Date, dashboardVM.TodayDate.Date) != 0)
                    {
                        dashboardVM.GetCreateOrMoveStopDetails();
                        if (dashboardVM.IsActionInitiated)
                            NavigateToNewNoteScreen();

                        //return;
                    }
                    else if (!dashboardVM.Customer.IsTodaysStop)
                    {
                        dashboardVM.ShowStopConfirmation();
                        //return;
                    }
                    else
                    {
                        NavigateToNewNoteScreen();

                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerNotesViewModel.][AddNewNote][customerID =" + customerNoVM + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerNotesViewModel][End:AddNewNote]");
                #endregion

            });
            #endregion

            #region SelectAllNotes
            SelectAllNotes = new DelegateCommand((SelectAll) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerNotesViewModel][Start:SelectAllNotes]");

                try
                {
                    bool select = (bool)SelectAll;
                    foreach (CustomerNote note in CustomerNotesList)
                    {
                        note.IsSelected = select;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerNotesViewModel.][SelectAllNotes][customerID =" + customerNoVM + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerNotesViewModel][End:SelectAllNotes]");
            });
            #endregion

            #region SelectNote
            SelectNote = new DelegateCommand((Selected) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerNotesViewModel][Start:SelectNote]");

                try
                {
                    bool select = (bool)Selected;
                    if (!select)
                    {
                        SelectAllCheck = select;
                    }
                    else
                    {
                        bool isAllSelected = true;
                        foreach (CustomerNote note in CustomerNotesList)
                        {
                            if (!note.IsSelected)
                            {
                                isAllSelected = false;
                            }
                        }
                        SelectAllCheck = isAllSelected == true ? true : false;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerNotesViewModel.][SelectNote][customerID =" + customerNoVM + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerNotesViewModel][End:SelectNote]");
            });
            #endregion

            #region SaveNewNote
            SaveNewNote = new DelegateCommand((param) =>
            {
                CustomerNote note = new CustomerNote();
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerNotesViewModel][Start:SaveNewNote]");
                try
                {
                    note.NoteDetails = NoteDetails.Trim();
                    note.CustomerID = customerNoVM;
                    if (CustomerNotesList.Count == 0)
                    {
                        note.IsDefault = true;
                    }
                    Managers.CustomerNotesManager noteManager = new Managers.CustomerNotesManager();
                    noteManager.AddNewCustomerNote(note);
                    GetCustomerNotes();
                    //NotesVMArgs args = new NotesVMArgs();
                    //args.Note = CustomerNotesList.FirstOrDefault(addednote => addednote.IsDefault == true);// note;//CustomerNotesList.FirstOrDefault(addednote => addednote.IsDefault == true);
                    //OnViewModelStateChanged(args);
                    ToggleButton = false;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerNotesViewModel.][SaveNewNote][customerID =" + customerNoVM + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerNotesViewModel][End:SaveNewNote]");
            });
            #endregion

            #region DeleteNote
            DeleteNote = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerNotesViewModel][Start:DeleteNote]");
                try
                {
                    List<CustomerNote> noteList = CustomerNotesList.Where(note => note.IsSelected == true).ToList<CustomerNote>();

                    int selectedNotes = noteList.Count();

                    var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Notes.DeleteNoteConfirmationForMany, MessageIcon = "Alert", Confirmed = false };
                    switch (selectedNotes)
                    {
                        case 0:
                            var alertMessage = new Helpers.AlertWindow { Message = "Please select note to delete!", MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                            return;
                            break;
                        case 1:
                            confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Notes.DeleteNoteConfirmation, MessageIcon = "Alert", Confirmed = false };
                            Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);

                            break;
                        default:
                            confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Notes.DeleteNoteConfirmationForMany, MessageIcon = "Alert", Confirmed = false };
                            Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                            break;
                    }

                    if (!confirmMessage.Confirmed) return;

                    Managers.CustomerNotesManager noteManager = new Managers.CustomerNotesManager();
                    Models.CustomerNote customerNote = new Models.CustomerNote();
                    customerNote.CustomerID = customerNoVM;

                    for (int index = 0; index < selectedNotes; index++)
                    {
                        customerNote.NoteID = noteList[index].NoteID;
                        noteManager.DeleteSelectedNote(customerNote);
                        CustomerNotesList.Remove(CustomerNotesList.ElementAt(CustomerNotesList.IndexOf(CustomerNotesList.First(note => note.NoteID == customerNote.NoteID))));
                    }
                    bool isDefaultNoteAvailable = Convert.ToBoolean(CustomerNotesList.Count(note => note.IsDefault == true));
                    if (!isDefaultNoteAvailable && CustomerNotesList.Count > 0)
                    {
                        Models.CustomerNote latestNote = CustomerNotesList.OrderByDescending(t => System.DateTime.ParseExact(t.DateTime, "MM/dd/yyyy hh:mmtt", System.Globalization.CultureInfo.InvariantCulture)).First();
                        CustomerNotesList.ElementAt(CustomerNotesList.IndexOf(latestNote)).IsDefault = true;
                        new Managers.CustomerNotesManager().SetDefaultNote(latestNote);
                        NotesVMArgs args = new NotesVMArgs();
                        args.Note = latestNote;
                        OnViewModelStateChanged(args);
                    }
                    if (CustomerNotesList.Count == 0)
                    {
                        NotesVMArgs args = new NotesVMArgs();
                        args.Note = null;
                        OnViewModelStateChanged(args);
                    }
                    SelectAllCheck = false;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerNotesViewModel.][DeleteNote][customerID =" + customerNoVM + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerNotesViewModel][End:DeleteNote]");
            });
            #endregion
        }

        private void NavigateToNewNoteScreen()
        {
            NoteDetails = string.Empty;
            IsEmpty = false;
            var dialog = new Helpers.DialogWindow { TemplateKey = "AddNewNote", Title = "Add New Note", Payload = this };
            Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
        }

        bool IsEmpty = false;
        async void GetCustomerNotes()
        {
            await Task.Run(() =>
            {

                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerNotesViewModel][AsyncStart:GetCustomerNotes]");
                try
                {
                    Managers.CustomerNotesManager noteManager = new Managers.CustomerNotesManager();
                    CustomerNotesList = noteManager.GetCustomerNotes(customerNoVM);
                    if (CustomerNotesList.Count == 1)
                    {
                        CustomerNotesList[0].IsDefault = true;
                    }
                    NotesVMArgs args = new NotesVMArgs();
                    args.Note = CustomerNotesList.FirstOrDefault(addednote => addednote.IsDefault == true);// note;//CustomerNotesList.FirstOrDefault(addednote => addednote.IsDefault == true);
                    OnViewModelStateChanged(args);
                    SelectAllCheck = false;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerNotesViewModel][GetCustomerNotes][customerID =" + customerNoVM + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerNotesViewModel][AsyncEnd:GetCustomerNotes]");
            });
        }


        public Guid MessageToken { get; set; }
        #endregion

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                string result = string.Empty;
                if (columnName == "NoteDetails" && IsEmpty)
                {
                    // result = Helpers.Constants.Common.NoteValidationMsg;
                    ToggleButton = false;
                }
                return result;
            }
        }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }

    public class NotesVMArgs : EventArgs
    {
        public CustomerNote Note { get; set; }

    }
}
