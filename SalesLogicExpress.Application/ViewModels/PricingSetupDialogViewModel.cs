﻿using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using SalesLogicExpress.Application.Helpers; 
using System.Threading.Tasks;
using System.Windows.Threading;
using Telerik.Windows.Controls;
using SalesLogicExpress.Application.Managers;
using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.ViewModelPayload;

using log4net;

namespace SalesLogicExpress.Application.ViewModels
{
    public class PricingSetupDialogViewModel : BaseViewModel
    {
        #region Variable and object declaration
        public static readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.PricingSetupDialogViewModel");
        private CustomerQuoteHeader customerQuote = null; 
        CancellationTokenSource tokenForCancelTask = new CancellationTokenSource();
        Managers.CustomerQuoteManager manager = new Managers.CustomerQuoteManager();
        private bool IsInProcess = false; //Flag used to avoid the mulitple calls for the selection changd on item source rebind 
        private bool _IsOverrideView = false;
        private bool IsApplyButtonClicked = false;
        private bool IsFirstTimeLoadCompleted = false;

        private bool _IsApplyButtonEnabled = false;
        private bool _IsResetButtonEnabled = false;

        //original prices 
        private ObservableCollection<PriceDetails> originalOverallList = new ObservableCollection<PriceDetails>();
        private ObservableCollection<PriceDetails> originalCategory1List = new ObservableCollection<PriceDetails>();
        private ObservableCollection<PriceDetails> originalCategory2List = new ObservableCollection<PriceDetails>();
        private ObservableCollection<PriceDetails> originalCategory3List = new ObservableCollection<PriceDetails>();
        private ObservableCollection<PriceDetails> originalCategory4List = new ObservableCollection<PriceDetails>();
        private ObservableCollection<PriceDetails> originalLiquidBracketList = new ObservableCollection<PriceDetails>();
        private ObservableCollection<PriceDetails> originalCoffeeVolumeList = new ObservableCollection<PriceDetails>();
        private ObservableCollection<PriceDetails> originalEquipPgmList = new ObservableCollection<PriceDetails>();
        private ObservableCollection<PriceDetails> originalPOSChargeList = new ObservableCollection<PriceDetails>();
        private ObservableCollection<PriceDetails> originalCCPBracketList = new ObservableCollection<PriceDetails>();

        private ObservableCollection<PriceDetails> _OverallList = new ObservableCollection<PriceDetails>();
        private ObservableCollection<PriceDetails> _Category1List = new ObservableCollection<PriceDetails>();
        private ObservableCollection<PriceDetails> _Category2List = new ObservableCollection<PriceDetails>();
        private ObservableCollection<PriceDetails> _Category3List = new ObservableCollection<PriceDetails>();
        private ObservableCollection<PriceDetails> _Category4List = new ObservableCollection<PriceDetails>();
        private ObservableCollection<PriceDetails> _LiquidBracketList = new ObservableCollection<PriceDetails>();
        private ObservableCollection<PriceDetails> _CoffeeVolumeList = new ObservableCollection<PriceDetails>();
        private ObservableCollection<PriceDetails> _EquipPgmList = new ObservableCollection<PriceDetails>();
        private ObservableCollection<PriceDetails> _POSChargeList = new ObservableCollection<PriceDetails>();
        private ObservableCollection<PriceDetails> _CCPBracketList = new ObservableCollection<PriceDetails>();
        private readonly PricingDetails _MasterPricingDetail = null;
        private readonly PricingDetails _OverridePricingDetail = null;
        private PriceDetails _OverallSelectedItem;
        private PriceDetails _Category2SelectedItem;
        private PriceDetails _Category3SelectedItem;
        private PriceDetails _Category4SelectedItem;
        private PriceDetails _Category1SelectedItem;
        private PriceDetails _LiquidBracketSelectedItem;
        private PriceDetails _CoffeeVolumeSelectedItem;
        private PriceDetails _EquipPgmSelectedItem;
        private PriceDetails _POSChargeSelectedItem;
        private PriceDetails _CCPBracketSelectedItem;

        private PriceDetails oldCategory2SelectedItem;
        private PriceDetails oldCategory3SelectedItem=new PriceDetails();
        private PriceDetails oldCategory4SelectedItem;
        private PriceDetails oldCategory1SelectedItem=new PriceDetails();

        private string _OverallDesc = string.Empty;
        private string _Category1Desc = string.Empty;
        private string _Category2Desc = string.Empty;
        private string _Category3Desc = string.Empty;
        private string _Category4Desc = string.Empty;
        private string _LiquidBracketDesc = string.Empty;
        private string _PriceDesc = string.Empty;
        private string _CCPDesc = string.Empty;
        private string _CoffeVolumeDesc = string.Empty;
        private string _POSDesc = string.Empty;
        private string _EquipPgmDesc = string.Empty;
        public Guid MessageToken { get; set; }

        #endregion

        #region Constructor

        public PricingSetupDialogViewModel(CustomerQuoteHeader pCustomerQuote, Guid token, BaseViewModel Parent = null)
        {
            MessageToken = token;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][Start:Constructor]");
            this.customerQuote = pCustomerQuote; 
            this._MasterPricingDetail = pCustomerQuote.MasterPriceSetup;
            this._OverridePricingDetail = pCustomerQuote.OverridePriceSetup;
            this.InitializeCommands();
            if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
            {
                IsProspeptQuote = true; ApplyButtonContent = "Copy to Master";
                this.ParentView = Parent;

                if (ViewModelPayload.PayloadManager.ProspectPayload.IsForPricingTab)
                {
                    LoadLists();
                } 
            }

            this.IsApplyButtonEnabled = false;
            this.IsResetButtonEnabled = this.IsDirty;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][End:Constructor]");
        }

        public override bool ConfirmSave()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][Start:ConfirmSave]");
            CopyToMaster(true);
            this.ParentView.IsDirty = false;
            IsResetButtonEnabled = false;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][End:ConfirmSave]");
            return base.ConfirmSave();
        }

        public override bool ConfirmCancel()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][Start:ConfirmCancel]");
            this.ParentView.IsDirty = false;
            CanNavigate = true;
            PayloadManager.ProspectPayload.IsForPricingTab = false;
            IsResetButtonEnabled = false;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][End:ConfirmCancel]");
            return base.ConfirmCancel();
        }
        #endregion

        #region Properties

        public bool IsEditable
        {
            get
            {
                return (IsOverrideView ? !(customerQuote.IsPrinted || ((DateTime.Now.Date - customerQuote.QuoteDate.Date).TotalDays != 0) || customerQuote.IsSettled) : true); 
            }
        }
        public bool IsPriceSetupDirty
        {
            get
            {
                return (OverridePricingDetail.IsAlliedProductLine1Overriden || OverridePricingDetail.IsAlliedProductLine2Overriden ||
                    OverridePricingDetail.IsAlliedProductLine3Overriden || OverridePricingDetail.IsAlliedProductLine4Overriden ||
                    OverridePricingDetail.IsAlliedProductLineOverallOverriden || OverridePricingDetail.IsCoffVolumeOverriden ||
                    OverridePricingDetail.IsEquipProgOverriden || OverridePricingDetail.IsLiquidBracketOverriden ||
                    OverridePricingDetail.IsPOSUpChargeOverriden || OverridePricingDetail.IsSpecialCCPOverriden); 
            }
        }

        public bool IsResetButtonEnabled
        {
            get { return _IsResetButtonEnabled; }
            set
            {
                _IsResetButtonEnabled = value;
                this.OnPropertyChanged("IsResetButtonEnabled");
                if (ViewModelPayload.PayloadManager.ProspectPayload.IsForPricingTab && IsResetButtonEnabled)
                {
                    this.ParentView.IsDirty = true;
                }
                else if (ViewModelPayload.PayloadManager.ProspectPayload.IsForPricingTab && !IsResetButtonEnabled)
                {
                    this.ParentView.IsDirty = false;
                }
            }
        }

        public bool IsApplyButtonEnabled
        {
            get { return _IsApplyButtonEnabled; }
            set
            {
                _IsApplyButtonEnabled = value;
                this.OnPropertyChanged("IsApplyButtonEnabled");
            }
        }

        /// <summary>
        /// Get or set flag to determine override view 
        /// </summary>
        public bool IsOverrideView
        {
            get { return _IsOverrideView;  }
            set
            {
                _IsOverrideView = value;
                this.OnPropertyChanged("IsOverrideView");
            }
        }

        private string applyButtonContent = "Apply";

        public string ApplyButtonContent
        {
            get { return applyButtonContent; }
            set { applyButtonContent = value; OnPropertyChanged("ApplyButtonContent"); }
        }


        private bool isProspeptQuote = false;

        public bool IsProspeptQuote
        {
            get { return isProspeptQuote; }
            set { isProspeptQuote = value; OnPropertyChanged("IsProspeptQuote"); }
        }

        #region Category selected items 

        /// <summary>
        /// Get or set overall category selected item 
        /// </summary>
        public PriceDetails OverallSelectedItem
        {
            get { return _OverallSelectedItem; }
            set
            {
                _OverallSelectedItem = value;
                this.OverallDesc = value.Description; 
                this.OnPropertyChanged("OverallSelectedItem");
            }
        }

        /// <summary>
        /// Get or set category 1selected item 
        /// </summary>
        public PriceDetails Category1SelectedItem
        {
            get { return _Category1SelectedItem; }
            set
            {
                if (!IsInProcess || !IsFirstTimeLoadCompleted)
                {
                    oldCategory1SelectedItem = _Category1SelectedItem;
                    _Category1SelectedItem = value;
                    this.Category1Desc = value.Description; 
                }

                this.OnPropertyChanged("Category1SelectedItem");
            }
        }

        /// <summary>
        /// Get or set category 2 selected item 
        /// </summary>
        public PriceDetails Category2SelectedItem
        {
            get { return _Category2SelectedItem; }
            set
            {
                if (!IsInProcess || !IsFirstTimeLoadCompleted)
                {
                    oldCategory2SelectedItem = _Category2SelectedItem;
                    _Category2SelectedItem = value;
                    this.Category2Desc = value.Description; 
                }

                this.OnPropertyChanged("Category2SelectedItem");
            }
        }

        /// <summary>
        /// Get or set category 3 selected item 
        /// </summary>
        public PriceDetails Category3SelectedItem
        {
            get { return _Category3SelectedItem; }
            set
            {
                if (!IsInProcess || !IsFirstTimeLoadCompleted)
                {
                    oldCategory3SelectedItem = _Category3SelectedItem;
                    _Category3SelectedItem = value;
                    this.Category3Desc = value.Description; 
                }
                this.OnPropertyChanged("Category3SelectedItem");
            }
        }

        /// <summary>
        /// Get or set category 4 selected item 
        /// </summary>
        public PriceDetails Category4SelectedItem
        {
            get { return _Category4SelectedItem; }
            set
            {
                if (!IsInProcess || !IsFirstTimeLoadCompleted)
                {
                    oldCategory4SelectedItem = _Category4SelectedItem;
                    _Category4SelectedItem = value;
                    this.Category4Desc = value.Description; 
                }
                this.OnPropertyChanged("Category4SelectedItem");
            }
        }

        /// <summary>
        /// Get or set category LiquidBracket selected item 
        /// </summary>
        public PriceDetails LiquidBracketSelectedItem
        {
            get { return _LiquidBracketSelectedItem; }
            set
            {
                _LiquidBracketSelectedItem = value;
                this.LiquidBracketDesc = value.Description; 
                this.OnPropertyChanged("LiquidBracketSelectedItem");
            }
        }

        /// <summary>
        /// Get or set category CoffeeVolume SelectedItem 
        /// </summary>
        public PriceDetails CoffeeVolumeSelectedItem
        {
            get { return _CoffeeVolumeSelectedItem; }
            set
            {
                _CoffeeVolumeSelectedItem = value;
                this.CoffeVolumeDesc = value.Description; 
                this.OnPropertyChanged("CoffeeVolumeSelectedItem");
            }
        }

        /// <summary>
        /// Get or set category equip pgm selected item 
        /// </summary>
        public PriceDetails EquipPgmSelectedItem
        {
            get { return _EquipPgmSelectedItem; }
            set
            {
                _EquipPgmSelectedItem = value;
                this.EquipPgmDesc = value.Description; 
                this.OnPropertyChanged("EquipPgmSelectedItem");
            }
        }

        /// <summary>
        /// Get or set category POS Charge selected item 
        /// </summary>
        public PriceDetails POSChargeSelectedItem
        {
            get { return _POSChargeSelectedItem; }
            set
            {
                _POSChargeSelectedItem = value;
                this.POSDesc = value.Description;
                this.OnPropertyChanged("POSChargeSelectedItem");
            }
        }

        /// <summary>
        /// Get or set category CCPBracket Selected Item
        /// </summary>
        public PriceDetails CCPBracketSelectedItem
        {
            get { return _CCPBracketSelectedItem; }
            set
            {
                _CCPBracketSelectedItem = value;
                this.CCPDesc = value.Description;
                this.OnPropertyChanged("CCPBracketSelectedItem");
            }
        }

        private PriceDetails taxGroupSelectedItem = new PriceDetails();

        public PriceDetails TaxGroupSelectedItem
        {
            get { return taxGroupSelectedItem; }
            set
            {
                taxGroupSelectedItem = value;
                TaxGroupDesc = value.Description;
                OnPropertyChanged("TaxGroupSelectedItem");
            }
        }
        #endregion 
        
        #region Lists

        /// <summary>
        /// Get or set overall pricing list 
        /// </summary>
        public ObservableCollection<PriceDetails> OverallList
        {
            get
            {
                return _OverallList;
            }
            set
            {
                _OverallList = value;
                this.OnPropertyChanged("OverallList");
            }
        }

        /// <summary>
        /// Get or set category 1 list 
        /// </summary>
        public ObservableCollection<PriceDetails> Category1List
        {
            get
            {
                return _Category1List;
            }
            set
            {
                _Category1List = value;
                this.OnPropertyChanged("Category1List");
            }
        }

        /// <summary>
        /// Get or set category 2 list 
        /// </summary>
        public ObservableCollection<PriceDetails> Category2List
        {
            get
            {
                return _Category2List;
            }
            set
            {
                _Category2List = value;
                this.OnPropertyChanged("Category2List");
            }
        }

        /// <summary>
        /// Get or set category 3 list 
        /// </summary>
        public ObservableCollection<PriceDetails> Category3List
        {
            get
            {
                return _Category3List;
            }
            set
            {
                _Category3List = value;
                this.OnPropertyChanged("Category3List");
            }
        }

        /// <summary>
        /// Get or set category 4 list 
        /// </summary>
        public ObservableCollection<PriceDetails> Category4List
        {
            get
            {
                return _Category4List;
            }
            set
            {
                _Category4List = value;
                this.OnPropertyChanged("Category4List");
            }
        }

        /// <summary>
        /// Get or set liquid bracket list 
        /// </summary>
        public ObservableCollection<PriceDetails> LiquidBracketList
        {
            get
            {
                return _LiquidBracketList;
            }
            set
            {
                _LiquidBracketList = value;
                this.OnPropertyChanged("LiquidBracketList");
            }
        }

        /// <summary>
        /// Get or set coffee volume list 
        /// </summary>
        public ObservableCollection<PriceDetails> CoffeeVolumeList
        {
            get
            {
                return _CoffeeVolumeList;
            }
            set
            {
                _CoffeeVolumeList = value;
                this.OnPropertyChanged("CoffeeVolumeList");
            }
        }

        /// <summary>
        /// Get or set equip program list 
        /// </summary>
        public ObservableCollection<PriceDetails> EquipPgmList
        {
            get
            {
                return _EquipPgmList;
            }
            set
            {
                _EquipPgmList = value;
                this.OnPropertyChanged("EquipPgmList");
            }
        }

        /// <summary>
        /// Get or set POS charge list 
        /// </summary>
        public ObservableCollection<PriceDetails> POSChargeList
        {
            get
            {
                return _POSChargeList;
            }
            set
            {
                _POSChargeList = value;
                this.OnPropertyChanged("POSChargeList");
            }
        }

        /// <summary>
        /// Get or set CCP bracket list 
        /// </summary>
        public ObservableCollection<PriceDetails> CCPBracketList
        {
            get
            {
                return _CCPBracketList;
            }
            set
            {
                _CCPBracketList = value;
                this.OnPropertyChanged("CCPBracketList");
            }
        }

        private ObservableCollection<PriceDetails> taxGroupList = new ObservableCollection<PriceDetails>();

        public ObservableCollection<PriceDetails> TaxGroupList
        {
            get { return taxGroupList; }
            set { taxGroupList = value; OnPropertyChanged("TaxGroupList"); }
        }
        #endregion

        #region Readonly Pricing set-up

        /// <summary>
        /// Get master price setup details 
        /// </summary>
        public PricingDetails MasterPricingDetail
        {
            get
            {
                return _MasterPricingDetail;
            }
        }


        /// <summary>
        /// Get or set overall pricing list 
        /// </summary>
        public PricingDetails OverridePricingDetail
        {
            get
            {
                //OverrideView==true means we need to show setup screen in read-only mode
                //false means user can override
                if(this.IsOverrideView)
                {
                    return _OverridePricingDetail;
                }
                else
                {
                    return _MasterPricingDetail;
                }
            }
        }

        #endregion

        #region Description Properties

        /// <summary>
        /// Get or set the overall decsription 
        /// </summary>
        public string OverallDesc
        {
            get
            {
                return _OverallDesc;
            }
            set
            {
                _OverallDesc = value;
                this.OnPropertyChanged("OverallDesc");
            }
        }

        /// <summary>
        /// Get or set category 1 description 
        /// </summary>
        public string Category1Desc
        {
            get
            {
                return _Category1Desc;
            }
            set
            {
                _Category1Desc = value;
                this.OnPropertyChanged("Category1Desc");
            }
        }

        /// <summary>
        /// Get or set category 2 description 
        /// </summary>
        public string Category2Desc
        {
            get
            {
                return _Category2Desc;
            }
            set
            {
                _Category2Desc = value;
                this.OnPropertyChanged("Category2Desc");
            }
        }

        /// <summary>
        /// Get or set category 3 description 
        /// </summary>
        public string Category3Desc
        {
            get
            {
                return _Category3Desc;
            }
            set
            {
                _Category3Desc = value;
                this.OnPropertyChanged("Category3Desc");
            }
        }

        /// <summary>
        /// Get or set category 4 description 
        /// </summary>
        public string Category4Desc
        {
            get
            {
                return _Category4Desc;
            }
            set
            {
                _Category4Desc = value;
                this.OnPropertyChanged("Category4Desc");
            }
        }

        /// <summary>
        /// Get or set liquid bracket description 
        /// </summary>
        public string LiquidBracketDesc
        {
            get
            {
                return _LiquidBracketDesc;
            }
            set
            {
                _LiquidBracketDesc = value;
                this.OnPropertyChanged("LiquidBracketDesc");
            }
        }

        /// <summary>
        /// Get or set cofee volume description 
        /// </summary>
        public string CoffeVolumeDesc
        {
            get
            {
                return _CoffeVolumeDesc;
            }
            set
            {
                _CoffeVolumeDesc = value;
                this.OnPropertyChanged("CoffeVolumeDesc");
            }
        }

        /// <summary>
        /// Get or set equip program description 
        /// </summary>
        public string EquipPgmDesc
        {
            get
            {
                return _EquipPgmDesc;
            }
            set
            {
                _EquipPgmDesc = value;
                this.OnPropertyChanged("EquipPgmDesc");
            }
        }

        /// <summary>
        /// Get or set POS up charge description 
        /// </summary>
        public string POSDesc
        {
            get
            {
                return _POSDesc;
            }
            set
            {
                _POSDesc = value;
                this.OnPropertyChanged("POSDesc");
            }
        }

        /// <summary>
        /// Get or set CCP bracket description 
        /// </summary>
        public string CCPDesc
        {
            get
            {
                return _CCPDesc;
            }
            set
            {
                _CCPDesc = value;
                this.OnPropertyChanged("CCPDesc");
            }
        }
        private string taxGroupDesc = string.Empty;

        public string TaxGroupDesc
        {
            get { return taxGroupDesc; }
            set { taxGroupDesc = value; OnPropertyChanged("TaxGroupDesc"); }
        }

        /// <summary>
        /// Get or set price protection description 
        /// </summary>
        public string PriceDesc
        {
            get
            {
                return _PriceDesc;
            }
            set
            {
                _PriceDesc = value;
                this.OnPropertyChanged("PriceDesc");
            }
        }

        #endregion

        #endregion

        #region Delegate Properties

        public DelegateCommand Category1SelectionChangedDlgtCmd { get; set; }
        public DelegateCommand Category2SelectionChangedDlgtCmd { get; set; }
        public DelegateCommand Category3SelectionChangedDlgtCmd { get; set; }
        public DelegateCommand Category4SelectionChangedDlgtCmd { get; set; }

        public DelegateCommand ComboSelectionChangedDlgtCmd { get; set; }

        public DelegateCommand ResetToMasterDlgtCmd { get; set; }
        public DelegateCommand CopyToMasterDlgtCmd { get; set; }
        public DelegateCommand ApplyDlgtCmd { get; set; }
        public DelegateCommand GridUnloadDlgtCmd { get; set; }
        public DelegateCommand GridLoadedDlgtCmd { get; set; }
        public DelegateCommand SavePricingCommand { get; set; }

        #endregion

        #region Private Methods

        /// <summary>
        /// Intializes commands 
        /// </summary>
        private void InitializeCommands()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][Start:InitializeCommands]");
            this.ComboSelectionChangedDlgtCmd = new DelegateCommand(
                (param)=>
                {
                    if (IsFirstTimeLoadCompleted)
                    {
                        this.IsResetButtonEnabled = true;
                        this.IsApplyButtonEnabled = true;
                    }
                    else
                    {
                        this.IsResetButtonEnabled = false;
                        this.IsApplyButtonEnabled = false;
                    }
                }
                ); 
            this.GridLoadedDlgtCmd = new DelegateCommand(
              (param) =>
              {
                  System.Diagnostics.Debug.WriteLine("Start grid load:" + DateTime.Now);
                  this.IsApplyButtonClicked = false; //Reset the flag

                  //this.GetDescription();

                  this.IsInProcess = false;
                  this.IsBusy = false;
                  IsFirstTimeLoadCompleted = true;
                  System.Diagnostics.Debug.WriteLine("End grid load:" + DateTime.Now);

              });
            
            this.GridUnloadDlgtCmd = new DelegateCommand(
                (param)=>
                {
                    System.Diagnostics.Debug.WriteLine("Start grid unload:" + DateTime.Now);
                    //this.ResetToMaster(); //If screen is without click of Apply button, then reset to master 

                    IsFirstTimeLoadCompleted = false;
                    System.Diagnostics.Debug.WriteLine("End grid unload:" + DateTime.Now);
                });

            this.ApplyDlgtCmd = new DelegateCommand(
               (param) =>
               {
                   try
                   {
                       IsApplyButtonClicked = true;

                       if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                       {
                           ((CustomerQuoteViewModel)this.ParentView).IsDirty = true;

                           if (param.ToString() == "Master")
                           {
                               this.CopyToMaster(true);
                           }
                           else if (param.ToString() == "Quote")
                           {
                               this.CopyToMaster(false);
                           }
                           manager.ApplyPricingToTemplates(customerQuote.Items, CommonNavInfo.UserBranch, ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID, customerQuote.OverridePriceSetup, true);
                       }
                       else
                       {
                           this.ApplyNewPriceSetup();

                           ((CustomerQuoteViewModel)SalesLogicExpress.Application.ViewModelPayload.PayloadManager.QuotePayload.Parent).IsDirty = true;
                           manager.ApplyPricingToTemplates(customerQuote.Items, CommonNavInfo.UserBranch, CommonNavInfo.Customer.CustomerNo, customerQuote.OverridePriceSetup);
                       }
                   }
                   catch (Exception ex)
                   {

                       Logger.Error("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][ApplyDlgtCmd DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                   }
               }); 


            this.ResetToMasterDlgtCmd = new DelegateCommand(
                (param) =>
                {
                    if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    {


                    }
                    else
                    {
                        ((CustomerQuoteViewModel)SalesLogicExpress.Application.ViewModelPayload.PayloadManager.QuotePayload.Parent).IsDirty = true;
                        this.ResetToMaster();
                    }

                    this.IsResetButtonEnabled = false;
                    this.IsApplyButtonEnabled = true;// this.IsPriceSetupDirty; 
                });

            //this.CopyToMasterDlgtCmd = new DelegateCommand(
            //    (param) =>
            //    {
            //    });
            //

            this.Category1SelectionChangedDlgtCmd = new DelegateCommand(
                (param) =>
                {
                    if(!IsInProcess)
                    {
                        IsInProcess = true;
                    }
                    else
                    {
                        return; 
                    }

                    if (param != null)
                    {
                        //this.Category2List = this.UpdateItemVisibility(this.Category2List, this.oldCategory1SelectedItem.Key, true);
                        //this.Category3List = this.UpdateItemVisibility(this.Category3List, this.oldCategory1SelectedItem.Key, true);
                        //this.Category4List = this.UpdateItemVisibility(this.Category4List, this.oldCategory1SelectedItem.Key, true);

                        //this.Category2List = this.UpdateItemVisibility(this.Category2List, this.Category1SelectedItem.Key, false);
                        //this.Category3List = this.UpdateItemVisibility(this.Category3List, this.Category1SelectedItem.Key, false);
                        //this.Category4List = this.UpdateItemVisibility(this.Category4List, this.Category1SelectedItem.Key, false);

                        string filter1 = this.GetFilterString(Category1SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));
                        string filter2 = this.GetFilterString(Category2SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));
                        string filter3 = this.GetFilterString(Category3SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));
                        string filter4 = this.GetFilterString(Category4SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));

                        this.Category2List = originalCategory2List.Where(o => !(o.Key.StartsWith(filter1) || o.Key.StartsWith(filter3) || o.Key.StartsWith(filter4))).ToObservableCollection();
                        this.Category3List = originalCategory3List.Where(o => !(o.Key.StartsWith(filter1) || o.Key.StartsWith(filter2) || o.Key.StartsWith(filter4))).ToObservableCollection();
                        this.Category4List = originalCategory4List.Where(o => !(o.Key.StartsWith(filter1) || o.Key.StartsWith(filter2) || o.Key.StartsWith(filter3))).ToObservableCollection();

                        //Reset the flag as it is not updated when filter in in progress
                        this.Category2SelectedItem = this.Category2SelectedItem;
                        this.Category3SelectedItem = this.Category3SelectedItem;
                        this.Category4SelectedItem = this.Category4SelectedItem;
                    }

                    IsInProcess = false;

                    if (IsFirstTimeLoadCompleted)
                    {
                        this.IsResetButtonEnabled = true;
                        this.IsApplyButtonEnabled = true;
                    }
                    else
                    {
                        this.IsResetButtonEnabled = false;
                         this.IsApplyButtonEnabled = false;
                    }
                }
                );

            this.Category2SelectionChangedDlgtCmd = new DelegateCommand(
                (param) =>
                {
                    if(!IsInProcess)
                    {
                        IsInProcess = true;
                    }
                    else
                    {
                        return; 
                    }

                    if (param != null)
                    {
                        //this.Category1List = this.UpdateItemVisibility(this.Category1List, this.oldCategory2SelectedItem.Key, true);
                        //this.Category3List = this.UpdateItemVisibility(this.Category3List, this.oldCategory2SelectedItem.Key, true);
                        //this.Category4List = this.UpdateItemVisibility(this.Category4List, this.oldCategory2SelectedItem.Key, true);

                        //this.Category1List = this.UpdateItemVisibility(this.Category1List, this.Category2SelectedItem.Key, false);
                        //this.Category3List = this.UpdateItemVisibility(this.Category3List, this.Category2SelectedItem.Key, false);
                        //this.Category4List = this.UpdateItemVisibility(this.Category4List, this.Category2SelectedItem.Key, false);
                        try
                        {
                            string filter1 = this.GetFilterString(Category1SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));
                            string filter2 = this.GetFilterString(Category2SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));
                            string filter3 = this.GetFilterString(Category3SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));
                            string filter4 = this.GetFilterString(Category4SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));

                            this.Category1List = originalCategory1List.Where(o => !(o.Key.StartsWith(filter2) || o.Key.StartsWith(filter3) || o.Key.StartsWith(filter4))).ToObservableCollection();
                            this.Category3List = originalCategory3List.Where(o => !(o.Key.StartsWith(filter1) || o.Key.StartsWith(filter2) || o.Key.StartsWith(filter4))).ToObservableCollection();
                            this.Category4List = originalCategory4List.Where(o => !(o.Key.StartsWith(filter1) || o.Key.StartsWith(filter2) || o.Key.StartsWith(filter3))).ToObservableCollection();

                            //Reset the flag as it is not updated when filter in in progress
                            this.Category1SelectedItem = this.Category1SelectedItem;
                            this.Category3SelectedItem = this.Category3SelectedItem;
                            this.Category4SelectedItem = this.Category4SelectedItem;
                        }
                        catch (Exception ex)
                        {

                            Logger.Error("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][Category2SelectionChangedDlgtCmd DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                        }
                    }
                    
                    IsInProcess = false;

                    if (IsFirstTimeLoadCompleted)
                    {
                        this.IsResetButtonEnabled = true;
                        this.IsApplyButtonEnabled = true;
                    }
                    else
                    {
                        this.IsResetButtonEnabled = false;
                        this.IsApplyButtonEnabled = false;
                    }
                }
                );

            this.Category3SelectionChangedDlgtCmd = new DelegateCommand(
                (param) =>
                {
                    if(!IsInProcess)
                    {
                        IsInProcess = true;
                    }
                    else
                    {
                        return; 
                    }

                    if (param != null)
                    {
                        //this.Category1List = this.UpdateItemVisibility(this.Category1List, this.oldCategory3SelectedItem.Key, true);
                        //this.Category2List = this.UpdateItemVisibility(this.Category2List, this.oldCategory3SelectedItem.Key, true);
                        //this.Category4List = this.UpdateItemVisibility(this.Category4List, this.oldCategory3SelectedItem.Key, true);

                        //this.Category1List = this.UpdateItemVisibility(this.Category1List, this.Category3SelectedItem.Key, false);
                        //this.Category2List = this.UpdateItemVisibility(this.Category2List, this.Category3SelectedItem.Key, false);
                        //this.Category4List = this.UpdateItemVisibility(this.Category4List, this.Category3SelectedItem.Key, false);


                        try
                        {
                            string filter1 = this.GetFilterString(Category1SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));
                            string filter2 = this.GetFilterString(Category2SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));
                            string filter3 = this.GetFilterString(Category3SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));
                            string filter4 = this.GetFilterString(Category4SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));


                            this.Category1List = originalCategory1List.Where(o => !(o.Key.StartsWith(filter2) || o.Key.StartsWith(filter3) || o.Key.StartsWith(filter4))).ToObservableCollection();
                            this.Category2List = originalCategory2List.Where(o => !(o.Key.StartsWith(filter1) || o.Key.StartsWith(filter3) || o.Key.StartsWith(filter4))).ToObservableCollection();
                            this.Category4List = originalCategory4List.Where(o => !(o.Key.StartsWith(filter1) || o.Key.StartsWith(filter2) || o.Key.StartsWith(filter3))).ToObservableCollection();

                            //Reset the flag as it is not updated when filter in in progress
                            this.Category1SelectedItem = this.Category1SelectedItem;
                            this.Category2SelectedItem = this.Category2SelectedItem;
                            this.Category4SelectedItem = this.Category4SelectedItem;
                        }
                        catch (Exception ex)
                        {

                            Logger.Error("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][Category3SelectionChangedDlgtCmd DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                        }
                    }

                    IsInProcess = false;

                    if (IsFirstTimeLoadCompleted)
                    {
                        this.IsResetButtonEnabled = true;
                        this.IsApplyButtonEnabled = true;
                    }
                    else
                    {
                        this.IsResetButtonEnabled = false;
                        this.IsApplyButtonEnabled = false;
                    }
                }
                );

            this.Category4SelectionChangedDlgtCmd = new DelegateCommand(
                (param) =>
                {
                    if(!IsInProcess)
                    {
                        IsInProcess = true;
                    }
                    else
                    {
                        return; 
                    }

                    if (param != null)
                    {
                        //this.Category1List = this.UpdateItemVisibility(this.Category1List, this.oldCategory4SelectedItem.Key, true);
                        //this.Category2List = this.UpdateItemVisibility(this.Category2List, this.oldCategory4SelectedItem.Key, true);
                        //this.Category3List = this.UpdateItemVisibility(this.Category3List, this.oldCategory4SelectedItem.Key, true);

                        //this.Category1List = this.UpdateItemVisibility(this.Category1List, this.Category4SelectedItem.Key, false);
                        //this.Category2List = this.UpdateItemVisibility(this.Category2List, this.Category4SelectedItem.Key, false);
                        //this.Category3List = this.UpdateItemVisibility(this.Category3List, this.Category4SelectedItem.Key, false);


                        try
                        {
                            string filter1 = this.GetFilterString(Category1SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));
                            string filter2 = this.GetFilterString(Category2SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));
                            string filter3 = this.GetFilterString(Category3SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));
                            string filter4 = this.GetFilterString(Category4SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));

                            this.Category1List = originalCategory1List.Where(o => !(o.Key.StartsWith(filter2) || o.Key.StartsWith(filter3) || o.Key.StartsWith(filter4))).ToObservableCollection();
                            this.Category2List = originalCategory2List.Where(o => !(o.Key.StartsWith(filter1) || o.Key.StartsWith(filter3) || o.Key.StartsWith(filter4))).ToObservableCollection();
                            this.Category3List = originalCategory3List.Where(o => !(o.Key.StartsWith(filter1) || o.Key.StartsWith(filter2) || o.Key.StartsWith(filter4))).ToObservableCollection();

                            //Reset the flag as it is not updated when filter in in progress
                            this.Category1SelectedItem = this.Category1SelectedItem;
                            this.Category2SelectedItem = this.Category2SelectedItem;
                            this.Category3SelectedItem = this.Category3SelectedItem;
                        }
                        catch (Exception ex)
                        {

                            Logger.Error("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][Category4SelectionChangedDlgtCmd DelegateCommand][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                        }

                    }

                    IsInProcess = false;

                    if (IsFirstTimeLoadCompleted)
                    {
                        this.IsResetButtonEnabled = true;
                        this.IsApplyButtonEnabled = true;
                    }
                    else
                    {
                        this.IsResetButtonEnabled = false;
                        this.IsApplyButtonEnabled = false;
                    }
                }
                );

            SavePricingCommand = new DelegateCommand((param) =>
            {
                bool flag= CopyToMaster(true  );
                this.ParentView.IsDirty = false;
                IsResetButtonEnabled = false;
                string messagestr = flag ? "Pricing Saved Successfully." : "Pricing save failure.";
                var alertMessage = new Helpers.AlertWindow { Header = "Save", Message = messagestr, MessageIcon = "Alert" };
                Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                return;
                
            });
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][End:InitializeCommands]");
        }

        /// <summary>
        /// Async method loads all the list 
        /// </summary>
        public async void LoadLists()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][Start:LoadLists]");
            await Task.Run(() =>
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    //Do not fetch item when printed 
                    try{
                    System.Diagnostics.Debug.WriteLine("Start load list:" + DateTime.Now);
                    this.IsBusy = true;

                    //Get original prices 
                    originalCategory1List = manager.GetPricingList(Helpers.PricingDetailCategory.AlliedProductLine1);
                    originalCategory2List = manager.GetPricingList(Helpers.PricingDetailCategory.AlliedProductLine2);
                    originalCategory3List = manager.GetPricingList(Helpers.PricingDetailCategory.AlliedProductLine3);
                    originalCategory4List = manager.GetPricingList(Helpers.PricingDetailCategory.AlliedProductLine4);

                    this.OverallList = manager.GetPricingList(Helpers.PricingDetailCategory.AlliedProductLineOverall);
                    this.Category1List = manager.GetPricingList(Helpers.PricingDetailCategory.AlliedProductLine1);
                    this.Category2List = manager.GetPricingList(Helpers.PricingDetailCategory.AlliedProductLine2);
                    this.Category3List = manager.GetPricingList(Helpers.PricingDetailCategory.AlliedProductLine3);
                    this.Category4List = manager.GetPricingList(Helpers.PricingDetailCategory.AlliedProductLine4);
                    this.LiquidBracketList = manager.GetPricingList(Helpers.PricingDetailCategory.LiquidBracket);
                    this.CoffeeVolumeList = manager.GetPricingList(Helpers.PricingDetailCategory.CoffVolume);
                    this.EquipPgmList = manager.GetPricingList(Helpers.PricingDetailCategory.EquipProg);
                    this.POSChargeList = manager.GetPricingList(Helpers.PricingDetailCategory.POSUpCharge);
                    this.CCPBracketList = manager.GetPricingList(Helpers.PricingDetailCategory.SpecialCCP);
                    this.TaxGroupList = manager.GetPricingList(Helpers.PricingDetailCategory.TaxGroup);

                    this.IsInProcess = true;

                    if (string.IsNullOrEmpty(Category1Desc))
                    {
                        this.Category1SelectedItem = this.Category1List.Where(o => o.Key.Equals("---")).First(); 
                    }

                    if (string.IsNullOrEmpty(Category2Desc))
                    {
                        this.Category2SelectedItem = this.Category2List.First(o => o.Key.Equals("---"));
                    }

                    if (string.IsNullOrEmpty(Category3Desc))
                    {
                        this.Category3SelectedItem = this.Category3List.First(o => o.Key.Equals("---"));
                    }

                    if (string.IsNullOrEmpty(Category1Desc))
                    {
                        this.Category4SelectedItem = this.Category4List.First(o => o.Key.Equals("---"));
                    }

                    string filter1 = this.GetFilterString(Category1SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));
                    string filter2 = this.GetFilterString(Category2SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));
                    string filter3 = this.GetFilterString(Category3SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));
                    string filter4 = this.GetFilterString(Category4SelectedItem.Key.Replace("---", "zzzzzzzzzzz"));

                    //if (this.Category1Desc.Trim().Length > 0)
                    //{
                        this.Category1List = originalCategory1List.Where(o => !(o.Key.StartsWith(filter2) || o.Key.StartsWith(filter3) || o.Key.StartsWith(filter4))).ToObservableCollection();
                        this.Category2List = originalCategory2List.Where(o => !(o.Key.StartsWith(filter1) || o.Key.StartsWith(filter3) || o.Key.StartsWith(filter4))).ToObservableCollection();
                        this.Category3List = originalCategory3List.Where(o => !(o.Key.StartsWith(filter1) || o.Key.StartsWith(filter2) || o.Key.StartsWith(filter4))).ToObservableCollection();
                        this.Category4List = originalCategory4List.Where(o => !(o.Key.StartsWith(filter1) || o.Key.StartsWith(filter2) || o.Key.StartsWith(filter3))).ToObservableCollection();
                    //}

                    //if (this.Category2Desc.Trim().Length > 0)
                    //{
                    //    this.Category1List = this.UpdateItemVisibility(this.Category1List, this.Category2SelectedItem.Key);
                    //    this.Category3List = this.UpdateItemVisibility(this.Category3List, this.Category2SelectedItem.Key);
                    //    this.Category4List = this.UpdateItemVisibility(this.Category4List, this.Category2SelectedItem.Key);
                    //}

                    //if (this.Category3Desc.Trim().Length > 0)
                    //{
                    //    this.Category4List = this.UpdateItemVisibility(this.Category4List, this.Category3SelectedItem.Key);
                    //    this.Category2List = this.UpdateItemVisibility(this.Category2List, this.Category3SelectedItem.Key);
                    //    this.Category1List = this.UpdateItemVisibility(this.Category1List, this.Category3SelectedItem.Key);
                    //}

                    //if (this.Category4Desc.Trim().Length > 0)
                    //{
                    //    this.Category3List = this.UpdateItemVisibility(this.Category3List, this.Category4SelectedItem.Key);
                    //    this.Category2List = this.UpdateItemVisibility(this.Category2List, this.Category4SelectedItem.Key);
                    //    this.Category1List = this.UpdateItemVisibility(this.Category1List, this.Category4SelectedItem.Key);
                    //}

                    IsFirstTimeLoadCompleted = true; 
                    this.IsInProcess = false;
                    this.IsBusy = false;

                    System.Diagnostics.Debug.WriteLine("End load list:" + DateTime.Now);
                    }
                    catch (Exception ex)
                    {

                        Logger.Error("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][LoadLists][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                    
                }));
                Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][End:LoadLists]");
            }, tokenForCancelTask.Token);
        }
        private async void GetList()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][Start:GetList]");
            await Task.Run(() =>
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    try
                    {
                        this.OverallList = manager.GetPricingList(Helpers.PricingDetailCategory.AlliedProductLineOverall);
                        this.Category1List = manager.GetPricingList(Helpers.PricingDetailCategory.AlliedProductLine1);
                        this.Category2List = manager.GetPricingList(Helpers.PricingDetailCategory.AlliedProductLine2);
                        this.Category3List = manager.GetPricingList(Helpers.PricingDetailCategory.AlliedProductLine3);
                        this.Category4List = manager.GetPricingList(Helpers.PricingDetailCategory.AlliedProductLine4);
                        this.LiquidBracketList = manager.GetPricingList(Helpers.PricingDetailCategory.LiquidBracket);
                        this.CoffeeVolumeList = manager.GetPricingList(Helpers.PricingDetailCategory.CoffVolume);
                        this.EquipPgmList = manager.GetPricingList(Helpers.PricingDetailCategory.EquipProg);
                        this.POSChargeList = manager.GetPricingList(Helpers.PricingDetailCategory.POSUpCharge);
                        this.CCPBracketList = manager.GetPricingList(Helpers.PricingDetailCategory.SpecialCCP);
                        this.TaxGroupList = manager.GetPricingList(Helpers.PricingDetailCategory.TaxGroup);
                    }
                    catch (Exception ex)
                    {

                        Logger.Error("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][GetList][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                }));
                Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][End:GetList]");
            }, tokenForCancelTask.Token);
        }

        public async void GetDescription()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][Start:GetDescription]");
            await Task.Run(() =>
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    try
                    {
                        this.OverallSelectedItem = this.OverallList.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.OverridePricingDetail.AlliedProductLineOverall).Trim()) ? "---" : this.OverridePricingDetail.AlliedProductLineOverall.Trim()));
                        this.Category1SelectedItem = this.Category1List.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.OverridePricingDetail.AlliedProductLine1).Trim()) ? "---" : this.OverridePricingDetail.AlliedProductLine1.Trim()));
                        this.Category2SelectedItem = this.Category2List.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.OverridePricingDetail.AlliedProductLine2).Trim()) ? "---" : this.OverridePricingDetail.AlliedProductLine2.Trim()));
                        this.Category3SelectedItem = this.Category3List.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.OverridePricingDetail.AlliedProductLine3).Trim()) ? "---" : this.OverridePricingDetail.AlliedProductLine3.Trim()));
                        this.Category4SelectedItem = this.Category4List.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.OverridePricingDetail.AlliedProductLine4).Trim()) ? "---" : this.OverridePricingDetail.AlliedProductLine4.Trim()));
                        this.LiquidBracketSelectedItem = this.LiquidBracketList.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.OverridePricingDetail.LiquidBracket).Trim()) ? "---" : this.OverridePricingDetail.LiquidBracket.Trim()));
                        this.CoffeeVolumeSelectedItem = this.CoffeeVolumeList.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.OverridePricingDetail.CoffVolume).Trim()) ? "---" : this.OverridePricingDetail.CoffVolume.Trim()));
                        this.EquipPgmSelectedItem = this.EquipPgmList.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.OverridePricingDetail.EquipProg).Trim()) ? "---" : this.OverridePricingDetail.EquipProg.Trim()));
                        this.POSChargeSelectedItem = this.POSChargeList.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.OverridePricingDetail.POSUpCharge).Trim()) ? "---" : this.OverridePricingDetail.POSUpCharge.Trim()));
                        this.CCPBracketSelectedItem = this.CCPBracketList.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.OverridePricingDetail.SpecialCCP).Trim()) ? "---" : this.OverridePricingDetail.SpecialCCP.Trim()));
                        this.TaxGroupSelectedItem = this.TaxGroupList.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.OverridePricingDetail.TaxGroup).Trim()) ? "---" : this.OverridePricingDetail.TaxGroup.Trim()));

                        if (IsOverrideView)
                        {
                            IsApplyButtonEnabled = false;
                            IsResetButtonEnabled = this.IsPriceSetupDirty;
                        }
                    }
                    catch (Exception ex)
                    {

                        Logger.Error("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][GetDescription][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }

                }));
                Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][End:GetDescription]");
            }, tokenForCancelTask.Token);
        }

        private async void ValidateCategoryList()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][Start:ValidateCategoryList]");

            await Task.Run(() =>
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    try
                    {
                        if (this.Category1Desc.Trim().Length > 0)
                        {
                            this.Category2List = this.UpdateItemVisibility(this.Category2List, this.Category1SelectedItem.Key);
                            this.Category3List = this.UpdateItemVisibility(this.Category3List, this.Category1SelectedItem.Key);
                            this.Category4List = this.UpdateItemVisibility(this.Category4List, this.Category1SelectedItem.Key);
                        }

                        if (this.Category2Desc.Trim().Length > 0)
                        {
                            this.Category1List = this.UpdateItemVisibility(this.Category1List, this.Category2SelectedItem.Key);
                            this.Category3List = this.UpdateItemVisibility(this.Category3List, this.Category2SelectedItem.Key);
                            this.Category4List = this.UpdateItemVisibility(this.Category4List, this.Category2SelectedItem.Key);
                        }

                        if (this.Category3Desc.Trim().Length > 0)
                        {
                            this.Category4List = this.UpdateItemVisibility(this.Category4List, this.Category3SelectedItem.Key);
                            this.Category2List = this.UpdateItemVisibility(this.Category2List, this.Category3SelectedItem.Key);
                            this.Category1List = this.UpdateItemVisibility(this.Category1List, this.Category3SelectedItem.Key);
                        }

                        if (this.Category4Desc.Trim().Length > 0)
                        {
                            this.Category3List = this.UpdateItemVisibility(this.Category3List, this.Category4SelectedItem.Key);
                            this.Category2List = this.UpdateItemVisibility(this.Category2List, this.Category4SelectedItem.Key);
                            this.Category1List = this.UpdateItemVisibility(this.Category1List, this.Category4SelectedItem.Key);
                        }
                    }
                    catch (Exception ex)
                    {

                        Logger.Error("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][ValidateCategoryList][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }

                }));
                Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][End:ValidateCategoryList]");
            }, tokenForCancelTask.Token);
        }

        /// <summary>
        /// Returns updated observable collection 
        /// </summary>
        /// <param name="collection">Collection to be updated</param>
        /// <param name="filter">Fiter string needs to be aplied on the collection</param>
        /// <returns>Updated observable collections</returns>
        private ObservableCollection<PriceDetails> UpdateItemVisibility(ObservableCollection<PriceDetails> collection, string filter)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][Start:UpdateItemVisibility]");

                filter=this.GetFilterString(filter);

                if (collection.Any() & !string.IsNullOrEmpty(filter))
                {
                    for (int i = 0; i < collection.Count(); i++)
                    {
                        PriceDetails item = collection[i];
                        //If lngth less than 2, then same value will not be available in any other list 
                        //If length >2, then first 2 char will not be avilable in any other list 
                        if (item.Key.Length < 2)
                        {
                            if (item.Key.Equals(filter))
                            {
                                item.IsVisible = false;
                            }
                            else
                            {
                                item.IsVisible = true;
                            }
                        }
                        else
                        {
                            if (item.Key.StartsWith(filter))
                            {
                                item.IsVisible = false;
                            }
                            else
                            {
                                item.IsVisible = true;
                            }
                        }

                        collection[i] = item;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][UpdateItemVisibility][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][End:UpdateItemVisibility]");
            return collection; 
        }

        /// <summary>
        /// Updates item visibility to specified status 
        /// </summary>
        /// <param name="collection">Collection needs to be updated </param>
        /// <param name="key">filter key</param>
        /// <param name="flag">status to be updated</param>
        /// <returns>Updated collection</returns>
        public ObservableCollection<PriceDetails> UpdateItemVisibility(ObservableCollection<PriceDetails> collection, string key, bool flag)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][Start:UpdateItemVisibility]");
            try
            {
                if(string.IsNullOrEmpty(key))
                {
                    return collection; 
                }

                string filter = this.GetFilterString(key);

                if(collection.Any())
                {
                    for (int i = 0; i < collection.Count(); i++)
                    {
                        PriceDetails item = collection[i];

                        //If lngth less than 2, then same value will not be available in any other list 
                        //If length >2, then first 2 char will not be avilable in any other list 
                        if (item.Key.Length < 2)
                        {
                            if (item.Key.Equals(filter))
                            {
                                item.IsVisible = flag;
                            }
                        }
                        else
                        {
                            if (item.Key.StartsWith(filter))
                            {
                                item.IsVisible = flag;
                            }
                        }

                        collection[i] = item;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][UpdateItemVisibility][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][End:UpdateItemVisibility]");

            return collection; 

        }

        /// <summary>
        /// Returns filter key upto 2 characters 
        /// </summary>
        /// <param name="filterKey">filter key</param>
        /// <returns>filter key</returns>
        private string GetFilterString(string key)
        {
            string filterKey = "";
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][Start:GetFilterString]");
            try
            {
                if(string.IsNullOrEmpty(key))
                {
                    filterKey = "zzzzzzzzzzz";
                }
                else if (key.Equals("zzzzzzzzzzz"))
                {
                    return key;
                }
                else
                {
                    if (key.Trim().Length <= 2)
                    {
                        filterKey = key.Trim();
                    }
                    else
                    {
                        filterKey = key.Trim().Substring(0, 2);
                    }
                }
            }
            catch (Exception ex)
            {
                filterKey = "";
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][GetFilterString][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][End:GetFilterString]");
            return filterKey; 
        }

        /// <summary>
        /// Resets the values to master
        /// </summary>
        private void ResetToMaster()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][Start:ResetToMaster]");
            try
            {
                System.Diagnostics.Debug.WriteLine("Start reset to master:" + DateTime.Now);
                if (!IsApplyButtonClicked && IsOverrideView)
                {
                    this.IsBusy = true;

                    this.OverallSelectedItem = this.OverallList.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.MasterPricingDetail.AlliedProductLineOverall).Trim()) ? "---" : this.MasterPricingDetail.AlliedProductLineOverall.Trim()));
                    this.Category1SelectedItem = this.Category1List.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.MasterPricingDetail.AlliedProductLine1).Trim()) ? "---" : this.MasterPricingDetail.AlliedProductLine1.Trim()));
                    this.Category2SelectedItem = this.Category2List.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.MasterPricingDetail.AlliedProductLine2).Trim()) ? "---" : this.MasterPricingDetail.AlliedProductLine2.Trim()));
                    this.Category3SelectedItem = this.Category3List.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.MasterPricingDetail.AlliedProductLine3).Trim()) ? "---" : this.MasterPricingDetail.AlliedProductLine3.Trim()));
                    this.Category4SelectedItem = this.Category4List.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.MasterPricingDetail.AlliedProductLine4).Trim()) ? "---" : this.MasterPricingDetail.AlliedProductLine4.Trim()));
                    this.LiquidBracketSelectedItem = this.LiquidBracketList.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.MasterPricingDetail.LiquidBracket).Trim()) ? "---" : this.MasterPricingDetail.LiquidBracket.Trim()));
                    this.CoffeeVolumeSelectedItem = this.CoffeeVolumeList.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.MasterPricingDetail.CoffVolume).Trim()) ? "---" : this.MasterPricingDetail.CoffVolume.Trim()));
                    this.EquipPgmSelectedItem = this.EquipPgmList.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.MasterPricingDetail.EquipProg).Trim()) ? "---" : this.MasterPricingDetail.EquipProg.Trim()));
                    this.POSChargeSelectedItem = this.POSChargeList.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.MasterPricingDetail.POSUpCharge).Trim()) ? "---" : this.MasterPricingDetail.POSUpCharge.Trim()));
                    this.CCPBracketSelectedItem = this.CCPBracketList.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.MasterPricingDetail.SpecialCCP).Trim()) ? "---" : this.MasterPricingDetail.SpecialCCP.Trim()));
                    this.taxGroupSelectedItem = this.TaxGroupList.First(o => o.Key.Equals(string.IsNullOrEmpty(Convert.ToString(this.MasterPricingDetail.TaxGroup).Trim()) ? "---" : this.MasterPricingDetail.TaxGroup.Trim()));

                    //this.OverallDesc = this.OverallList.GetDescription(this.MasterPricingDetail.AlliedProductLineOverall);
                    //this.Category1Desc = this.Category1List.GetDescription(this.MasterPricingDetail.AlliedProductLine1);
                    //this.Category2Desc = this.Category2List.GetDescription(this.MasterPricingDetail.AlliedProductLine2);
                    //this.Category3Desc = this.Category3List.GetDescription(this.MasterPricingDetail.AlliedProductLine3);
                    //this.Category4Desc = this.Category4List.GetDescription(this.MasterPricingDetail.AlliedProductLine4);
                    //this.LiquidBracketDesc = this.LiquidBracketList.GetDescription(this.MasterPricingDetail.LiquidBracket);
                    //this.CoffeVolumeDesc = this.CoffeeVolumeList.GetDescription(this.MasterPricingDetail.CoffVolume);
                    //this.EquipPgmDesc = this.EquipPgmList.GetDescription(this.MasterPricingDetail.EquipProg);
                    //this.POSDesc = this.POSChargeList.GetDescription(this.MasterPricingDetail.POSUpCharge);
                    //this.CCPDesc = this.CCPBracketList.GetDescription(this.MasterPricingDetail.SpecialCCP);

                    //Reset flag to false
                    this.OverridePricingDetail.IsAlliedProductLineOverallOverriden = false;
                    this.OverridePricingDetail.IsAlliedProductLine1Overriden = false;
                    this.OverridePricingDetail.IsAlliedProductLine2Overriden = false;
                    this.OverridePricingDetail.IsAlliedProductLine3Overriden = false;
                    this.OverridePricingDetail.IsAlliedProductLine4Overriden = false;
                    this.OverridePricingDetail.IsCoffVolumeOverriden = false;
                    this.OverridePricingDetail.IsEquipProgOverriden = false;
                    this.OverridePricingDetail.IsLiquidBracketOverriden = false;
                    this.OverridePricingDetail.IsPOSUpChargeOverriden = false;
                    this.OverridePricingDetail.IsSpecialCCPOverriden = false;

                    
                    if(!customerQuote.IsPrinted)
                    {
                        manager.ApplyPricingToTemplates(customerQuote.Items, CommonNavInfo.UserBranch,
                        ViewModelPayload.PayloadManager.ProspectPayload.IsProspect ? ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID :
                        CommonNavInfo.Customer.CustomerNo, customerQuote.OverridePriceSetup);
                    }

                    this.IsApplyButtonEnabled = true;
                    this.IsBusy = false;
                }

                System.Diagnostics.Debug.WriteLine("End reset to master:" + DateTime.Now);
            }
            catch (Exception ex)
            {
                this.IsBusy = false;
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][ResetToMaster][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                //Do nothing
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][End:ResetToMaster]");
        }

        private bool CopyToMaster(bool IsUpdateMaster)
        {
            bool flag = false;
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][Start:CopyToMaster]");
            try
            {
                this.OverridePricingDetail.AlliedProductLine1 = this.Category1SelectedItem.Key.Replace("---","");
                this.OverridePricingDetail.AlliedProductLine2 = this.Category2SelectedItem.Key.Replace("---", "");
                this.OverridePricingDetail.AlliedProductLine3 = this.Category3SelectedItem.Key.Replace("---", "");
                this.OverridePricingDetail.AlliedProductLine4 = this.Category4SelectedItem.Key.Replace("---", "");
                this.OverridePricingDetail.AlliedProductLineOverall = this.OverallSelectedItem.Key.Replace("---", "");

                this.OverridePricingDetail.LiquidBracket = this.LiquidBracketSelectedItem.Key.Replace("---", "");
                this.OverridePricingDetail.EquipProg = this.EquipPgmSelectedItem.Key.Replace("---", "");
                this.OverridePricingDetail.CoffVolume = this.CoffeeVolumeSelectedItem.Key.Replace("---", "");
                this.OverridePricingDetail.POSUpCharge = this.POSChargeSelectedItem.Key.Replace("---", "");
                this.OverridePricingDetail.SpecialCCP = this.CCPBracketSelectedItem.Key.Replace("---", "");
                this.OverridePricingDetail.TaxGroup = (this.TaxGroupSelectedItem.Key == "" || this.TaxGroupSelectedItem.Key.Equals("---") ? this.OverridePricingDetail.TaxGroup.Replace("---", "") : this.TaxGroupSelectedItem.Key.Replace("---", ""));

              

                if (!this.OverridePricingDetail.AlliedProductLine1.Equals(this.MasterPricingDetail.AlliedProductLine1.Trim()))
                {
                    this.OverridePricingDetail.IsAlliedProductLine1Overriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsAlliedProductLine1Overriden = false;

                }
                

                if (!this.OverridePricingDetail.AlliedProductLine2.Equals(this.MasterPricingDetail.AlliedProductLine2.Trim()))
                {
                    this.OverridePricingDetail.IsAlliedProductLine2Overriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsAlliedProductLine2Overriden = false;

                }

                if (!this.OverridePricingDetail.AlliedProductLine3.Equals(this.MasterPricingDetail.AlliedProductLine3.Trim()))
                {
                    this.OverridePricingDetail.IsAlliedProductLine3Overriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsAlliedProductLine3Overriden = false;

                }
               

                if (!this.OverridePricingDetail.AlliedProductLine4.Equals(this.MasterPricingDetail.AlliedProductLine4.Trim()))
                {
                    this.OverridePricingDetail.IsAlliedProductLine4Overriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsAlliedProductLine4Overriden = false;

                }
   

                if (!this.OverridePricingDetail.AlliedProductLineOverall.Equals(this.MasterPricingDetail.AlliedProductLineOverall.Trim()))
                {
                    this.OverridePricingDetail.IsAlliedProductLineOverallOverriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsAlliedProductLineOverallOverriden = false;

                }
                

                if (!this.OverridePricingDetail.LiquidBracket.Equals(this.MasterPricingDetail.LiquidBracket.Trim()))
                {
                    this.OverridePricingDetail.IsLiquidBracketOverriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsLiquidBracketOverriden = false;

                }
                

                if (!this.OverridePricingDetail.EquipProg.Equals(this.MasterPricingDetail.EquipProg.Trim()))
                {
                    this.OverridePricingDetail.IsEquipProgOverriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsEquipProgOverriden = false;

                }
                

                if (!this.OverridePricingDetail.CoffVolume.Equals(this.MasterPricingDetail.CoffVolume.Trim()))
                {
                    this.OverridePricingDetail.IsCoffVolumeOverriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsCoffVolumeOverriden = false;

                }
                

                if (!this.OverridePricingDetail.POSUpCharge.Equals(this.MasterPricingDetail.POSUpCharge.Trim()))
                {
                    this.OverridePricingDetail.IsPOSUpChargeOverriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsPOSUpChargeOverriden = false;

                }
                

                if (!this.OverridePricingDetail.SpecialCCP.Equals(this.MasterPricingDetail.SpecialCCP.Trim()))
                {
                    this.OverridePricingDetail.IsSpecialCCPOverriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsSpecialCCPOverriden = false;

                }
                
                //UPDATE MASTER PRICING
                if (IsUpdateMaster)
                {
                    this.MasterPricingDetail.AlliedProductLine1 = this.Category1SelectedItem.Key.Replace("---", "");
                    this.MasterPricingDetail.AlliedProductLine2 = this.Category2SelectedItem.Key.Replace("---", "");
                    this.MasterPricingDetail.AlliedProductLine3 = this.Category3SelectedItem.Key.Replace("---", "");
                    this.MasterPricingDetail.AlliedProductLine4 = this.Category4SelectedItem.Key.Replace("---", "");
                    this.MasterPricingDetail.AlliedProductLineOverall = this.OverallSelectedItem.Key.Replace("---", "");

                    this.MasterPricingDetail.LiquidBracket = this.LiquidBracketSelectedItem.Key.Replace("---", "");
                    this.MasterPricingDetail.EquipProg = this.EquipPgmSelectedItem.Key.Replace("---", "");
                    this.MasterPricingDetail.CoffVolume = this.CoffeeVolumeSelectedItem.Key.Replace("---", "");
                    this.MasterPricingDetail.POSUpCharge = this.POSChargeSelectedItem.Key.Replace("---", "");
                    this.MasterPricingDetail.SpecialCCP = this.CCPBracketSelectedItem.Key.Replace("---", "");
                    this.MasterPricingDetail.TaxGroup = (this.TaxGroupSelectedItem.Key == "" || this.TaxGroupSelectedItem.Key.Equals("---") ? this.MasterPricingDetail.TaxGroup.Replace("---", "") : this.TaxGroupSelectedItem.Key.Replace("---", ""));

                    if (!this.OverridePricingDetail.AlliedProductLine1.Equals(this.MasterPricingDetail.AlliedProductLine1.Trim()))
                    {
                        this.OverridePricingDetail.IsAlliedProductLine1Overriden = true;
                    }
                    else
                    {
                        this.OverridePricingDetail.IsAlliedProductLine1Overriden = false;

                    }


                    if (!this.OverridePricingDetail.AlliedProductLine2.Equals(this.MasterPricingDetail.AlliedProductLine2.Trim()))
                    {
                        this.OverridePricingDetail.IsAlliedProductLine2Overriden = true;
                    }
                    else
                    {
                        this.OverridePricingDetail.IsAlliedProductLine2Overriden = false;

                    }

                    if (!this.OverridePricingDetail.AlliedProductLine3.Equals(this.MasterPricingDetail.AlliedProductLine3.Trim()))
                    {
                        this.OverridePricingDetail.IsAlliedProductLine3Overriden = true;
                    }
                    else
                    {
                        this.OverridePricingDetail.IsAlliedProductLine3Overriden = false;

                    }


                    if (!this.OverridePricingDetail.AlliedProductLine4.Equals(this.MasterPricingDetail.AlliedProductLine4.Trim()))
                    {
                        this.OverridePricingDetail.IsAlliedProductLine4Overriden = true;
                    }
                    else
                    {
                        this.OverridePricingDetail.IsAlliedProductLine4Overriden = false;

                    }


                    if (!this.OverridePricingDetail.AlliedProductLineOverall.Equals(this.MasterPricingDetail.AlliedProductLineOverall.Trim()))
                    {
                        this.OverridePricingDetail.IsAlliedProductLineOverallOverriden = true;
                    }
                    else
                    {
                        this.OverridePricingDetail.IsAlliedProductLineOverallOverriden = false;

                    }


                    if (!this.OverridePricingDetail.LiquidBracket.Equals(this.MasterPricingDetail.LiquidBracket.Trim()))
                    {
                        this.OverridePricingDetail.IsLiquidBracketOverriden = true;
                    }
                    else
                    {
                        this.OverridePricingDetail.IsLiquidBracketOverriden = false;

                    }


                    if (!this.OverridePricingDetail.EquipProg.Equals(this.MasterPricingDetail.EquipProg.Trim()))
                    {
                        this.OverridePricingDetail.IsEquipProgOverriden = true;
                    }
                    else
                    {
                        this.OverridePricingDetail.IsEquipProgOverriden = false;

                    }


                    if (!this.OverridePricingDetail.CoffVolume.Equals(this.MasterPricingDetail.CoffVolume.Trim()))
                    {
                        this.OverridePricingDetail.IsCoffVolumeOverriden = true;
                    }
                    else
                    {
                        this.OverridePricingDetail.IsCoffVolumeOverriden = false;

                    }


                    if (!this.OverridePricingDetail.POSUpCharge.Equals(this.MasterPricingDetail.POSUpCharge.Trim()))
                    {
                        this.OverridePricingDetail.IsPOSUpChargeOverriden = true;
                    }
                    else
                    {
                        this.OverridePricingDetail.IsPOSUpChargeOverriden = false;

                    }


                    if (!this.OverridePricingDetail.SpecialCCP.Equals(this.MasterPricingDetail.SpecialCCP.Trim()))
                    {
                        this.OverridePricingDetail.IsSpecialCCPOverriden = true;
                    }
                    else
                    {
                        this.OverridePricingDetail.IsSpecialCCPOverriden = false;

                    }
                    flag = new CustomerQuoteManager().UpdatePricingSetupProspectMaster(this.MasterPricingDetail);
                }
            }
            catch (Exception ex)
            {
                this.IsBusy = false;
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][CopyToMaster][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                //Do nothing
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][End:CopyToMaster]");
            return flag;
        }

        private void ApplyNewPriceSetup()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][Start:ApplyNewPriceSetup]");
            try
            {
                System.Diagnostics.Debug.WriteLine("Start apply new price:" + DateTime.Now);

                this.OverridePricingDetail.AlliedProductLine1 = this.Category1SelectedItem.Key.Replace("---","");
                this.OverridePricingDetail.AlliedProductLine2 = this.Category2SelectedItem.Key.Replace("---","");
                this.OverridePricingDetail.AlliedProductLine3 = this.Category3SelectedItem.Key.Replace("---","");
                this.OverridePricingDetail.AlliedProductLine4 = this.Category4SelectedItem.Key.Replace("---","");
                this.OverridePricingDetail.AlliedProductLineOverall = this.OverallSelectedItem.Key.Replace("---","");

                this.OverridePricingDetail.LiquidBracket = this.LiquidBracketSelectedItem.Key.Replace("---","");
                this.OverridePricingDetail.EquipProg = this.EquipPgmSelectedItem.Key.Replace("---","");
                this.OverridePricingDetail.CoffVolume = this.CoffeeVolumeSelectedItem.Key.Replace("---","");
                this.OverridePricingDetail.POSUpCharge = this.POSChargeSelectedItem.Key.Replace("---","");
                this.OverridePricingDetail.SpecialCCP = this.CCPBracketSelectedItem.Key.Replace("---","");

                if (!this.OverridePricingDetail.AlliedProductLine1.Equals(this.MasterPricingDetail.AlliedProductLine1.Trim()))
                {
                    this.OverridePricingDetail.IsAlliedProductLine1Overriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsAlliedProductLine1Overriden = false;
                }

                if (!this.OverridePricingDetail.AlliedProductLine2.Equals(this.MasterPricingDetail.AlliedProductLine2.Trim()))
                {
                    this.OverridePricingDetail.IsAlliedProductLine2Overriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsAlliedProductLine2Overriden = false;
                }

                if (!this.OverridePricingDetail.AlliedProductLine3.Equals(this.MasterPricingDetail.AlliedProductLine3.Trim()))
                {
                    this.OverridePricingDetail.IsAlliedProductLine3Overriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsAlliedProductLine3Overriden = false;
                }

                if (!this.OverridePricingDetail.AlliedProductLine4.Equals(this.MasterPricingDetail.AlliedProductLine4.Trim()))
                {
                    this.OverridePricingDetail.IsAlliedProductLine4Overriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsAlliedProductLine4Overriden = false;
                }

                if (!this.OverridePricingDetail.AlliedProductLineOverall.Equals(this.MasterPricingDetail.AlliedProductLineOverall.Trim()))
                {
                    this.OverridePricingDetail.IsAlliedProductLineOverallOverriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsAlliedProductLineOverallOverriden = false;
                }

                if (!this.OverridePricingDetail.LiquidBracket.Equals(this.MasterPricingDetail.LiquidBracket.Trim()))
                {
                    this.OverridePricingDetail.IsLiquidBracketOverriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsLiquidBracketOverriden = false;
                }

                if (!this.OverridePricingDetail.EquipProg.Equals(this.MasterPricingDetail.EquipProg.Trim()))
                {
                    this.OverridePricingDetail.IsEquipProgOverriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsEquipProgOverriden = false;
                }

                if (!this.OverridePricingDetail.CoffVolume.Equals(this.MasterPricingDetail.CoffVolume.Trim()))
                {
                    this.OverridePricingDetail.IsCoffVolumeOverriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsCoffVolumeOverriden = false;
                }

                if (!this.OverridePricingDetail.POSUpCharge.Equals(this.MasterPricingDetail.POSUpCharge.Trim()))
                {
                    this.OverridePricingDetail.IsPOSUpChargeOverriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsPOSUpChargeOverriden = false;
                }

                if (!this.OverridePricingDetail.SpecialCCP.Equals(this.MasterPricingDetail.SpecialCCP.Trim()))
                {
                    this.OverridePricingDetail.IsSpecialCCPOverriden = true;
                }
                else
                {
                    this.OverridePricingDetail.IsSpecialCCPOverriden = false;
                }

                System.Diagnostics.Debug.WriteLine("End apply new price:" + DateTime.Now);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][ApplyNewPriceSetup][Error=" + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                //throw;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PricingSetupDialogViewModel][End:ApplyNewPriceSetup]");
        }
        #endregion

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }

    public struct PriceDetails
    {
        #region Variable and object declaration 
        
        private string _Key ;
        private string _Description;
        private bool _IsVisible; 

        #endregion 

        #region Property

        /// <summary>
        /// Get or set pricing Key 
        /// </summary>
        public string Key
        {
            get { return (string.IsNullOrEmpty(_Key)?string.Empty:_Key); }
            set { _Key = value; }
        }

        /// <summary>
        /// Get or set the pricing description 
        /// </summary>
        public string Description
        {
            get { return (string.IsNullOrEmpty(_Description) ? string.Empty : _Description); }
            set { _Description = value; }
        }

        /// <summary>
        /// Get or set is visible to true 
        /// </summary>
        public bool IsVisible
        {
            get { return _IsVisible; }
            set { _IsVisible = value; }
        }
        #endregion 
    }
}
