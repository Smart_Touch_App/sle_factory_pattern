﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Domain;
using Telerik.Windows.Controls;
using log4net;

namespace SalesLogicExpress.Application.ViewModels
{
    public class MoneyOrderViewModel : BaseViewModel, IDataErrorInfo
    {
        private readonly ILog logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.MoneyOrderViewModel");

        private readonly MoneyOrderManager mOManager = new MoneyOrderManager();
        private readonly System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        private Guid messageToken = new System.Guid();
        private string routeID = string.Empty;
        private string route;
        private string routeName;
        private string userRoute;
        private ObservableCollection<MoneyOrder> moneyOrderList = new ObservableCollection<Domain.MoneyOrder>();

        private MoneyOrder moneyOrderObject = new Domain.MoneyOrder();

        private Expenses expense = new Domain.Expenses();
        private ExpensesViewModel expenseVM = new ExpensesViewModel();
        private ObservableCollection<ReasonCode> reasonCode = new ObservableCollection<ReasonCode>();
        private int selectedReasonCode = -1;
        private bool isEditEnabled = false;
        private bool isVoidEnabled = false;
        private bool isAddEnabled = false;
        private bool toggleReasonCodeButton;
        private bool toggleAddButton = false;

        private string moneyOrderNumber = string.Empty;
        private string cashOnHand;
        private string amount = string.Empty;
        private string feeAmount = string.Empty;
        private string totalAmount = string.Empty;
        private bool isValidFee = true;
        private bool isValidAmount = true;
        private bool isValidMONo = true;
        //private bool isAddMO = false;
        private bool isValidTotal = true;
        private bool isEmptyMONo = true;
        private bool disposed = false;

        public MoneyOrderViewModel()
        {
            this.InitializeCommands();
            this.LoadData();
        }

        public Guid MessageToken
        {
            get
            {
                return this.messageToken;
            }
            set
            {
                this.messageToken = value;
                this.OnPropertyChanged("MessageToken");
            }
        }

        public string RouteID
        {
            get
            {
                return this.routeID;
            }
            set
            {
                this.routeID = value;
                this.OnPropertyChanged("RouteID");
            }
        }

        public string Route
        {
            get
            {
                return this.route;
            }
            set
            {
                this.route = value;
                this.OnPropertyChanged("Route");
            }
        }

        public string RouteName
        {
            get
            {
                return this.routeName;
            }
            set
            {
                this.routeName = value;
                this.OnPropertyChanged("RouteName");
            }
        }

        public string UserRoute
        {
            get
            {
                return this.userRoute;
            }
            set
            {
                this.userRoute = value;
                this.OnPropertyChanged("UserRoute");
            }
        }

        public ObservableCollection<MoneyOrder> MoneyOrderList
        {
            get
            {
                return this.moneyOrderList;
            }
            set
            {
                this.moneyOrderList = value;
                this.OnPropertyChanged("MoneyOrderList");
            }
        }

        public MoneyOrder MoneyOrderObject
        {
            get
            {
                return this.moneyOrderObject;
            }
            set
            {
                this.moneyOrderObject = value;
                this.OnPropertyChanged("MoneyOrderObject");
            }
        }

        public Expenses Expense
        {
            get
            {
                return this.expense;
            }
            set
            {
                this.expense = value;
                this.OnPropertyChanged("Expense");
            }
        }

        public ExpensesViewModel ExpenseVM
        {
            get
            {
                return this.expenseVM;
            }
            set
            {
                this.expenseVM = value;
                this.OnPropertyChanged("ExpenseVM");
            }
        }

        public ObservableCollection<ReasonCode> ReasonCode
        {
            get
            {
                return this.reasonCode;
            }
            set
            {
                this.reasonCode = value;
                this.OnPropertyChanged("ReasonCode");
            }
        }

        public int SelectedReasonCode
        {
            get
            {
                return this.selectedReasonCode;
            }
            set
            {
                this.selectedReasonCode = value;
                if (this.selectedReasonCode > 0)
                {
                    this.ToggleReasonCodeButton = true;
                }
                else
                {
                    this.ToggleReasonCodeButton = false;
                }
                this.OnPropertyChanged("SelectedReasonCode");
            }
        }

        public bool IsEditEnabled
        {
            get
            {
                return this.isEditEnabled;
            }
            set
            {
                this.isEditEnabled = value;
                this.OnPropertyChanged("IsEditEnabled");
            }
        }

        public bool IsVoidEnabled
        {
            get
            {
                return this.isVoidEnabled;
            }
            set
            {
                this.isVoidEnabled = value;
                this.OnPropertyChanged("IsVoidEnabled");
            }
        }

        public bool IsAddEnabled
        {
            get
            {
                return this.isAddEnabled;
            }
            set
            {
                this.isAddEnabled = value;
                this.OnPropertyChanged("IsAddEnabled");
            }
        }

        public bool ToggleReasonCodeButton
        {
            get
            {
                return this.toggleReasonCodeButton;
            }
            set
            {
                this.toggleReasonCodeButton = value;
                this.OnPropertyChanged("ToggleReasonCodeButton");
            }
        }

        public bool ToggleAddButton
        {
            get
            {
                return this.toggleAddButton;
            }
            set
            {
                this.toggleAddButton = value;
                this.OnPropertyChanged("ToggleAddButton");
            }
        }

        public string MoneyOrderNumber
        {
            get
            {
                return this.moneyOrderNumber;
            }
            set
            {
                this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][Start:MoneyOrderNumber]");
                try
                {
                    this.moneyOrderNumber = value;
                    this.isValidMONo = this.isEmptyMONo = false;
                    if (!string.IsNullOrEmpty(this.moneyOrderNumber))
                    {
                        this.isValidMONo = this.isEmptyMONo = true;
                        if (!string.IsNullOrEmpty(this.Amount) && this.isValidAmount && this.isValidFee)
                        {
                            this.ToggleAddButton = true;
                        }
                        else
                        {
                            this.ToggleAddButton = false;
                        }
                    }
                    else
                    {
                        this.isEmptyMONo = false;
                        this.ToggleAddButton = false;
                    }
                    this.OnPropertyChanged("MoneyOrderNumber");
                }
                catch (Exception ex)
                {
                    this.logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][MoneyOrderNumber][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
                }
                this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][End:MoneyOrderNumber]");
            }
        }

        public string CashOnHand
        {
            get
            {
                return this.cashOnHand;
            }
            set
            {
                this.cashOnHand = value;
                this.OnPropertyChanged("CashOnHand");
            }
        }

        public string Amount
        {
            get
            {
                return this.amount;
            }
            set
            {
                this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][Start:Amount]");
                try
                {
                    this.amount = value;
                    this.isValidAmount = false;
                    if (!string.IsNullOrEmpty(this.amount) && (Convert.ToDecimal(this.amount) > 0) && (Convert.ToDecimal(this.amount) <= Convert.ToDecimal(this.CashOnHand)))
                    {
                        this.isValidAmount = true;
                        if (!string.IsNullOrEmpty(this.MoneyOrderNumber) && this.isValidMONo && this.isValidFee)
                        {
                            this.ToggleAddButton = true;
                        }
                        else
                        {
                            this.ToggleAddButton = false;
                        }
                    }
                    else
                    {
                        this.isValidAmount = false;
                        this.ToggleAddButton = false;
                    }
                    this.OnPropertyChanged("Amount");
                    this.CalculateTotal();
                }
                catch (Exception ex)
                {
                    this.logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][Amount][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
                }
                this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][End:Amount]");
            }
        }

        public string FeeAmount
        {
            get
            {
                return this.feeAmount;
            }
            set
            {
                this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][Start:FeeAmount]");
                try
                {
                    this.feeAmount = value; //IsValidFee = false;
                    if (!string.IsNullOrEmpty(this.feeAmount) && !string.IsNullOrEmpty(this.Amount) && this.isValidAmount && !string.IsNullOrEmpty(this.MoneyOrderNumber))
                    {
                        if (Convert.ToDecimal(this.feeAmount) >= Convert.ToDecimal(this.Amount))
                        {
                            this.isValidFee = false;
                            this.ToggleAddButton = false;
                        }
                        else
                        {
                            this.ToggleAddButton = true;
                            this.isValidFee = true;
                        }
                    }
                    this.CalculateTotal();
                    this.OnPropertyChanged("FeeAmount");
                }
                catch (Exception ex)
                {
                    this.logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][FeeAmount][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
                }
                this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][End:FeeAmount]");
            }
        }

        public string TotalAmount
        {
            get
            {
                return this.totalAmount;
            }
            set
            {
                this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][Start:TotalAmount]");
                try
                {
                    this.totalAmount = value;
                    this.isValidTotal = false;
                    if (!string.IsNullOrEmpty(this.TotalAmount))
                    {
                        if (Convert.ToDecimal(this.TotalAmount) > Convert.ToDecimal(this.CashOnHand))
                        {
                            this.isValidTotal = false;
                            this.ToggleAddButton = false;
                        }
                        else
                        {
                            this.isValidTotal = true;// ToggleAddButton = true;
                        }
                    }
                    this.OnPropertyChanged("TotalAmount");
                }
                catch (Exception ex)
                {
                    this.logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][TotalAmount][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
                }
                this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][End:TotalAmount]");
            }
        }

        public string Error
        {
            get
            {
                return null;
                //throw new NotImplementedException(); 
            }
        }

        public DelegateCommand OpenEditDialogForMO { get; set; }

        public DelegateCommand OpenVoidDialogForMO { get; set; }

        public DelegateCommand OpenAddDialogForMO { get; set; }

        public DelegateCommand SaveNewMoneyOrder { get; set; }

        public DelegateCommand VoidMoneyOrder { get; set; }

        public DelegateCommand SaveEditedMoneyOrder { get; set; }

        public DelegateCommand SelectionItemInvoke { get; set; }

        public string this[string columnName]
        {
            get
            {
                string result = string.Empty;
                if (columnName == "Amount" && !this.isValidAmount)
                {
                    result = Helpers.Constants.Common.ExpenseAmountErrorMsg;
                }
                if (columnName == "FeeAmount" && !this.isValidFee)
                {
                    result = Helpers.Constants.Common.FeeAmountErrorMsg;
                }
                if (columnName == "MoneyOrderNumber" && !this.isValidMONo)
                {
                    result = Helpers.Constants.Common.MoneyOrderNoErrorMsg;
                }
                if (columnName == "MoneyOrderNumber" && !this.isEmptyMONo)
                {
                    result = Helpers.Constants.Common.MoneyOrderNoEmptyMsg;
                }
                if (columnName == "TotalAmount" && !this.isValidTotal)
                {
                    result = Helpers.Constants.Common.TotalAmountErrorMsg;
                }
                return result;
            }
        }

        public void CalculateTotal()
        {
            this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][Start:CalculateTotal]");
            try
            {
                if (!string.IsNullOrEmpty(this.Amount))
                {
                    if (string.IsNullOrEmpty(this.FeeAmount))
                    {
                        this.FeeAmount = "0.00";
                    }
                    this.TotalAmount = (Convert.ToDecimal(this.Amount) + Convert.ToDecimal(this.FeeAmount)).ToString("F2");
                }
            }
            catch (Exception ex)
            {
                this.logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][CalculateTotal][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
            }
            this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][End:CalculateTotal]");
        }

        public void InitializeCommands()
        {
            this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][Start:InitializeCommands]");

            this.OpenAddDialogForMO = new DelegateCommand((param) =>
            {
                this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][Start:OpenAddDialogForMO]");
                try
                {
                    //Issue ID- 101- While Settlement In progress if we adding new Money order, Expenses and system should ask Void the previous settlement. Once done newly added Money order, Cash or Checks should reflect in Settlement.- Zakir
                    bool flag = SettlementConfirmationManager.IsPendingVerification();
                    if (flag)
                    {
                        //Alert message for pending verification
                        var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.EODConfirmedValidation, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, this.MessageToken);
                    }
                    else
                    {
                        this.MoneyOrderNumber = string.Empty;
                        this.Amount = string.Empty;
                        this.TotalAmount = string.Empty;
                        this.FeeAmount = string.Empty;
                        this.isValidMONo = this.isEmptyMONo = true;
                        this.isValidAmount = this.isValidFee = this.isValidTotal = true;
                        this.ToggleAddButton = false;
                        var dialogWindow = new Helpers.DialogWindow { TemplateKey = "AddMoneyOrder", Title = "Add New Money Order" };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialogWindow, this.MessageToken);
                    }
                }
                catch (Exception ex)
                {
                    this.logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][OpenAddDialogForMO][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
                }
                this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][End:OpenAddDialogForMO]");
            });

            this.OpenEditDialogForMO = new DelegateCommand((param) =>
            {
                this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][Start:OpenEditDialogForMO]");
                try
                {
                    //Issue ID- 101- While Settlement In progress if we adding new Money order, Expenses and system should ask Void the previous settlement. Once done newly added Money order, Cash or Checks should reflect in Settlement.- Zakir
                    bool flag = SettlementConfirmationManager.IsPendingVerification();
                    if (flag)
                    {
                        //Alert message for pending verification
                        var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.EODConfirmedValidation, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, this.MessageToken);
                    }
                    else
                    {
                        //this.isAddMO = false;
                        this.CashOnHand = (Convert.ToDecimal(this.CashOnHand) + Convert.ToDecimal(this.MoneyOrderObject.TotalAmount)).ToString("F2");
                        this.MoneyOrderNumber = this.MoneyOrderObject.MoneyOrderNumber.ToString();
                        this.Amount = Convert.ToDecimal(this.MoneyOrderObject.MoneyOrderAmount).ToString("F2");
                        this.FeeAmount = string.IsNullOrEmpty(this.MoneyOrderObject.FeeAmount) ? "0.00" : Convert.ToDecimal(this.MoneyOrderObject.FeeAmount).ToString("F2");
                        this.TotalAmount = string.IsNullOrEmpty(this.MoneyOrderObject.TotalAmount) ? "0.00" : Convert.ToDecimal(this.MoneyOrderObject.TotalAmount).ToString("F2");
                        //IsValidAmount = false; 
                        this.ToggleAddButton = false;
                        var dialogWindow = new Helpers.DialogWindow { TemplateKey = "EditMoneyOrder", Title = "Edit Money Order" };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialogWindow, this.MessageToken);
                        if (dialogWindow.Cancelled)
                        {
                            this.CashOnHand = (Convert.ToDecimal(this.CashOnHand) - Convert.ToDecimal(this.MoneyOrderObject.TotalAmount)).ToString("F2");
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][OpenEditDialogForMO][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
                }
                this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][End:OpenEditDialogForMO]");
            });

            this.OpenVoidDialogForMO = new DelegateCommand((param) =>
            {
                //Issue ID- 101- While Settlement In progress if we adding new Money order, Expenses and system should ask Void the previous settlement. Once done newly added Money order, Cash or Checks should reflect in Settlement.- Zakir
                bool flag = SettlementConfirmationManager.IsPendingVerification();
                if (flag)
                {
                    //Alert message for pending verification
                    var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.EODConfirmedValidation, MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, this.MessageToken);
                }
                else
                {
                    this.ReasonCode = this.mOManager.GetVoidReasonCode();
                    var dialogWindow = new Helpers.DialogWindow { TemplateKey = "VoidMoneyOrder", Title = "Void Money Order" };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialogWindow, this.MessageToken);
                }
            });

            this.SaveNewMoneyOrder = new DelegateCommand((param) =>
            {
                this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][Start:SaveNewMoneyOrder]");
                try
                {
                    this.MoneyOrderObject.MoneyOrderNumber = this.MoneyOrderNumber.ToString();
                    this.isValidMONo = this.mOManager.CheckMONumber(this.MoneyOrderObject.MoneyOrderNumber, true);
                    if (!this.isValidMONo)
                    {
                        this.OnPropertyChanged("MoneyOrderNumber");
                        this.ToggleAddButton = false;
                        return;
                    }

                    this.MoneyOrderObject.MoneyOrderAmount = Convert.ToDecimal(this.Amount).ToString("F2");
                    this.MoneyOrderObject.FeeAmount = string.IsNullOrEmpty(this.FeeAmount) ? string.Empty : Convert.ToDecimal(this.FeeAmount).ToString("F2");
                    this.MoneyOrderObject.TotalAmount = (Convert.ToDecimal(this.Amount) + Convert.ToDecimal(this.FeeAmount)).ToString("F2");
                    this.MoneyOrderObject.Status = "UNSETTLED";
                    this.MoneyOrderObject.StatusCode = StatusTypesEnum.USTLD.ToString();
                    this.MoneyOrderObject.VoidReasonID = string.Empty;
                    this.MoneyOrderObject.VoidReason = string.Empty;
                    this.MoneyOrderObject.RouteSettlementId = string.Empty;
                    this.MoneyOrderObject.RouteID = CommonNavInfo.RouteID.ToString();
                    this.MoneyOrderObject.MoneyOrderDate = DateTime.Now;

                    var obj = new Domain.MoneyOrder();
                    obj = this.mOManager.AddMoneyOrder(this.MoneyOrderObject);
                    if (obj != null)
                    {
                        this.MoneyOrderObject.StatusID = obj.StatusID;
                        this.MoneyOrderObject.MoneyOrderID = obj.MoneyOrderID;
                    }

                    obj = null;

                    if (Convert.ToDecimal(this.MoneyOrderObject.FeeAmount) > Convert.ToDecimal(0))
                    {
                        var expense = new Managers.ExpensesManager();

                        this.GenerateExpense(MoneyOrderObject);
                        this.Expense = expense.AddExpenses(this.Expense);
                        this.ExpenseVM.ExpensesObject = this.Expense;
                        this.ExpenseVM.LogActivityForExpense(ActivityKey.CreateExpense);

                        expense.Dispose();
                        expense = null;
                    }
                    this.LogActivity(ActivityKey.CreateMoneyOrder);
                    this.CashOnHand = (Convert.ToDecimal(this.CashOnHand) - Convert.ToDecimal(this.MoneyOrderObject.MoneyOrderAmount)).ToString("F2");
                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), this.MessageToken);

                    this.GetMoneyOrderList();
                }
                catch (System.Exception ex)
                {
                    this.logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][SaveNewMoneyOrder][ExceptionStackTrace = {0}][Exception Message={1}]", ex.StackTrace, ex.Message));

                    throw;
                }
                this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][End:SaveNewMoneyOrder]");
            });

            this.VoidMoneyOrder = new DelegateCommand((param) =>
            {
                try
                {
                    this.MoneyOrderObject.VoidReasonID = this.SelectedReasonCode.ToString();
                    string statusID = this.mOManager.UpdateMoneyOrder(this.MoneyOrderObject, false).ToString();
                    this.MoneyOrderObject.StatusID = string.IsNullOrEmpty(statusID) ? this.MoneyOrderObject.StatusID : statusID.Trim();
                    this.MoneyOrderObject.Status = "VOID";
                    this.MoneyOrderObject.StatusCode = StatusTypesEnum.VOID.ToString();

                    int count = Convert.ToInt32(DbEngine.ExecuteScalar(string.Format("select count(isnull(ExpenseId, 0)) from BUSDTA.ExpenseDetails where ExpenseDetails.TransactionId = '{0}'", this.MoneyOrderObject.MoneyOrderID)));
                    if (count != 0)
                    {
                        this.Expense = this.mOManager.UpdateExpense(this.MoneyOrderObject, false);
                        this.ExpenseVM.ExpensesObject = this.Expense;
                        this.ExpenseVM.LogActivityForExpense(ActivityKey.VoidExpense);
                    }

                    this.LogActivity(ActivityKey.VoidMoneyOrder);
                    this.CashOnHand = (Convert.ToDecimal(this.CashOnHand) + Convert.ToDecimal(this.MoneyOrderObject.TotalAmount)).ToString("F2");
                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), this.MessageToken);
                    this.IsEditEnabled = this.IsVoidEnabled = false;
                    this.GetMoneyOrderList();
                }
                catch (System.Exception ex)
                {
                    this.logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][VoidExpenses][ExceptionStackTrace = {0}][Exception Message={1}]", ex.StackTrace, ex.Message));
                    throw;
                }
            });

            this.SaveEditedMoneyOrder = new DelegateCommand((param) =>
            {
                try
                {
                    bool mOchanged = false;//string.Empty;
                    if (this.MoneyOrderObject.MoneyOrderNumber != this.MoneyOrderNumber.ToString())
                    {
                        mOchanged = true;
                    }

                    this.isValidMONo = this.mOManager.CheckMONumber(this.MoneyOrderNumber, mOchanged);
                    if (!this.isValidMONo)
                    {
                        this.OnPropertyChanged("MoneyOrderNumber");
                        this.ToggleAddButton = false;
                        return;
                    }
                    else
                    {
                        this.MoneyOrderObject.MoneyOrderNumber = this.MoneyOrderNumber.ToString();
                    }

                    this.MoneyOrderObject.MoneyOrderAmount = Convert.ToDecimal(this.Amount).ToString("F2");
                    this.MoneyOrderObject.FeeAmount = string.IsNullOrEmpty(this.FeeAmount) ? "0.00" : Convert.ToDecimal(this.FeeAmount).ToString("F2");
                    this.MoneyOrderObject.TotalAmount = (Convert.ToDecimal(this.Amount) + Convert.ToDecimal(this.FeeAmount)).ToString("F2");
                    this.mOManager.UpdateMoneyOrder(this.MoneyOrderObject, true).ToString();
                    this.CashOnHand = (Convert.ToDecimal(this.CashOnHand) - Convert.ToDecimal(this.MoneyOrderObject.TotalAmount)).ToString("F2");

                    int count = Convert.ToInt32(DbEngine.ExecuteScalar(string.Format("select count(isnull(ExpenseId, 0)) from BUSDTA.ExpenseDetails where TransactionId = '{0}'", this.MoneyOrderObject.MoneyOrderID)));
                    if (count == 0)
                    {
                        if (Convert.ToDecimal(this.MoneyOrderObject.FeeAmount) > Convert.ToDecimal(0))
                        {
                            var expense = new Managers.ExpensesManager();
                            this.GenerateExpense(MoneyOrderObject);
                            this.Expense = expense.AddExpenses(this.Expense);

                            expense.Dispose();
                            expense = null;
                        }
                    }
                    else
                    {
                        this.Expense = this.mOManager.UpdateExpense(this.MoneyOrderObject, true);
                    }
                    if (this.Expense != null)
                    {
                        this.ExpenseVM.ExpensesObject = this.Expense;
                        this.ExpenseVM.LogActivityForExpense(ActivityKey.EditExpense);
                    }

                    this.LogActivity(ActivityKey.EditMoneyOrder);
                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), this.MessageToken);
                    this.IsEditEnabled = this.IsVoidEnabled = false;

                    this.GetMoneyOrderList();
                }
                catch (System.Exception ex)
                {
                    this.logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][SaveEditedMoneyOrder][ExceptionStackTrace = {0}][Exception Message={1}]", ex.StackTrace, ex.Message));
                    throw;
                }
            });

            this.SelectionItemInvoke = new DelegateCommand((param) =>
            {
                this.MoneyOrderObject = param as MoneyOrder;

                if (this.MoneyOrderObject.Status == "UNSETTLED")
                {
                    this.IsEditEnabled = true;
                    this.IsVoidEnabled = true;
                }
                else if (this.MoneyOrderObject.Status == "VOID" || this.MoneyOrderObject.Status == "SETTLED" || this.MoneyOrderObject.Status == "VOIDED / SETTLED")
                {
                    this.IsEditEnabled = false;
                    this.IsVoidEnabled = false;
                }
            });

            this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][End:InitializeCommands]");
        }

        public void GetMoneyOrderList()
        {
            try
            {
                this.MoneyOrderList = this.mOManager.GetMoneyOrderList();

                //this.MoneyOrderList = new ObservableCollection<Domain.MoneyOrder>();
                //ObservableCollection<MoneyOrder> moList = this.mOManager.GetMoneyOrderList();
                //foreach (MoneyOrder item in moList)
                //{
                //    if (Convert.ToDecimal(item.MoneyOrderAmount) != Convert.ToDecimal(0))
                //    {
                //        this.MoneyOrderList.Add(item);
                //    }
                //}
                //moList.Clear();
            }
            catch (Exception ex)
            {
                this.logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][GetMoneyOrderList][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
            }
        }

        public void LogActivity(ActivityKey key)
        {
            this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][Start:LogActivity]");
            try
            {
                var ac = new Activity
                {
                    ActivityType = key.ToString(),
                    ActivityStart = DateTime.Now,
                    ActivityEnd = DateTime.Now,
                    IsTxActivity = true,
                    ActivityDetailClass = this.MoneyOrderObject.ToString(),
                    ActivityDetails = this.MoneyOrderObject.SerializeToJson(),
                    RouteID = this.RouteID
                };
                if (key.ToString().ToLower() == "CreateMoneyOrder".ToString().ToLower())
                {
                    ac.ActivityStatus = "USTLD";
                }
                else if (key.ToString().ToLower() == "VoidMoneyOrder".ToString().ToLower())
                {
                    ac.ActivityStatus = "VOID";
                }

                //Get the parent activity id
                ac.ActivityHeaderID = this.GetParentActivity(this.MoneyOrderObject.MoneyOrderID.ToString());
                ResourceManager.Transaction.LogActivity(ac);
            }
            catch (Exception ex)
            {
                this.logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][LogActivity][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
            }
            this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][End:LogActivity]");
        }

        public void GenerateExpense(MoneyOrder order)
        {
            this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][Start:GenerateExpense]");
            try
            {
                this.Expense.TransactionId = string.IsNullOrEmpty(order.MoneyOrderID) ? string.Empty : order.MoneyOrderID;
                this.Expense.VoidReasonID = string.Empty;
                this.Expense.ExpenseAmount = order.FeeAmount;
                this.Expense.RouteSettlementId = string.Empty;
                this.Expense.Explanation = string.Format("Money order charges for {0}", order.MoneyOrderNumber);
                this.Expense.ExpenseDate = DateTime.Now;
                DataSet cat = DbEngine.ExecuteDataSet("select CategoryTypeID, CategoryTypeDESC from BUSDTA.Category_Type where CategoryTypeCD = 'MOCHR'");
                DataRow row = cat.Tables[0].Rows[0];
                this.Expense.CategotyID = string.IsNullOrEmpty(row["CategoryTypeID"].ToString()) ? string.Empty : row["CategoryTypeID"].ToString();
                this.Expense.ExpenseCategory = string.IsNullOrEmpty(row["CategoryTypeDESC"].ToString()) ? string.Empty : row["CategoryTypeDESC"].ToString();

                cat.Dispose();
                cat = null;
            }
            catch (Exception ex)
            {
                this.logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][GenerateExpense][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
            }
            this.logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][End:GenerateExpense]");
        }

        protected override void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }
            if (disposing)
            {
                this.handle.Dispose();

                if (this.mOManager != null)
                {
                    this.mOManager.Dispose();
                }
            }
            base.Dispose(disposing);
            this.disposed = true;
        }

        private async void LoadData()
        {
            await Task.Run(() =>
            {
                try
                {
                    this.MessageToken = CommonNavInfo.MessageToken;
                    this.RouteID = CommonNavInfo.RouteID.ToString();
                    this.Route = PayloadManager.ApplicationPayload.Route;
                    this.UserRoute = Managers.UserManager.UserRoute;
                    this.RouteName = Managers.UserManager.RouteName;
                    this.CashOnHand = PayloadManager.RouteSettlementPayload.TotalCash.ToString("F2").Trim();
                    if (Convert.ToDecimal(this.CashOnHand) > 0)
                    {
                        this.IsAddEnabled = true;
                    }
                    this.GetMoneyOrderList();
                }
                catch (Exception ex)
                {
                    this.logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][LoadData][ExceptionMessage = {0}][ExceptionStackTrace = {1}]", ex.Message, ex.StackTrace));
                }
            });
        }

        private string GetParentActivity(string moneyOrderID)
        {
            string query = "";

            try
            {
                query = string.Format("SELECT TDID FROM BUSDTA.M50012 WHERE TDTYP='MoneyOrder' AND TDDTLS LIKE '%\"MoneyOrderId\":\"{0}\"%' AND TDPNTID=0", moneyOrderID);

                return DbEngine.ExecuteScalar(query);
            }
            catch (Exception ex)
            {
                this.logger.Error(string.Format("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][GetParentActivity][ExceptionStackTrace = {0}][Exception Message={1}]", ex.StackTrace, ex.Message));
                return string.Empty;
            }
        }
    }
}
