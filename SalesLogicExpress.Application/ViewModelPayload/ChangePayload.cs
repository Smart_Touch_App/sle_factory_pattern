﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.ViewModelPayload
{
    /// <summary>
    /// Holds reference of the changes done to a particular document such as Payment/Invoice/Order, etc.
    /// </summary>
    public class ChangePayload: PayloadBase
    {
        #region Object and variable declaration

        private Dictionary<string, string> _UpdatedProperties = new Dictionary<string, string>();

        #endregion 

        #region Properties

        /// <summary>
        /// Get or set document type 
        /// </summary>
        public string DocumentType { get; set; }

        /// <summary>
        /// Get or set document number
        /// </summary>
        public string DocumentNumber { get; set; }

        /// <summary>
        /// Get or set list of changed properties
        /// </summary>
        public Dictionary<string, string> UpdatedProperties
        {
            get { return _UpdatedProperties; }
            set { _UpdatedProperties = value; }
        }

        /// <summary>
        /// Get or set route id
        /// </summary>
        public string RouteId { get; set; }

        /// <summary>
        /// Get or set customer id
        /// </summary>
        public string CustomerId { get; set; }

        #endregion
    }
}
