﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.ViewModelPayload
{
    public class RouteReplenishmentPayload : PayloadBase
    {
        private string fromBranch;
        public string FromBranch
        {
            get { return fromBranch; }
            set { fromBranch = value; }
        }

        private string status = string.Empty;
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        private string toBranch;
        public string ToBranch
        {
            get { return toBranch; }
            set { toBranch = value; }
        }

        //Sathish Issue ID-178
        //"Adding Add Load in Replenishment Transfer Date From and Transfer Date TO is Displaying Previous Date"
        private DateTime fromDate = DateTime.Now.Date;

        public DateTime FromDate
        {
            get { return fromDate.Date; }
            set { fromDate = value; }
        }

        private DateTime toDate = DateTime.Now.Date;

        public DateTime ToDate
        {
            get { return toDate.Date; }
            set { toDate = value; }
        }

        private string routeNameAddLoad;

        public string RouteNameAddLoad
        {
            get { return routeNameAddLoad; }
            set { routeNameAddLoad = value; }
        }

        private string selectedReplnId = string.Empty;

        public string SelectedReplnId
        {
            get { return selectedReplnId; }
            set { selectedReplnId = value; }
        }
        private bool navigatedForVoid = false;
        public bool NavigatedForVoid
        {
            get { return navigatedForVoid; }
            set { navigatedForVoid = value; }
        }

        private bool isNonEditableReplenishment = false;

        public bool IsNonEditableReplenishment
        {
            get { return isNonEditableReplenishment; }
            set { isNonEditableReplenishment = value; }
        }
        private bool isCompletedReplenishmnet = false;
        public bool IsCompletedReplenishmnet
        {
            get { return isCompletedReplenishmnet; }
            set { isCompletedReplenishmnet = value; }
        }
        private bool isFromBranchToTruck = false;

        public bool IsFromBranchToTruck
        {
            get { return isFromBranchToTruck; }
            set { isFromBranchToTruck = value; }
        }

        private bool isAddLoadView = false;

        public bool IsAddLoadView
        {
            get { return isAddLoadView; }
            set { isAddLoadView = value; }
        }

        private string currentViewName = string.Empty;
        public string CurrentViewName
        {
            get { return currentViewName; }
            set { currentViewName = value; }
        }


        private bool isSalesBranch = false;

        public bool IsSalesBranch
        {
            get { return isSalesBranch; }
            set { isSalesBranch = value; }
        }

        private string voidReasonID = string.Empty;
        public string VoidReasonID
        {
            get { return voidReasonID; }
            set { voidReasonID = value; }
        }


        private bool isSuggestionReplenishment = false;
        public bool IsSuggestionReplenishment
        {
            get { return isSuggestionReplenishment; }
            set { isSuggestionReplenishment = value; }
        }

        private bool isNavigatedFromRouteHome = false;

        public bool IsNavigatedFromRouteHome
        {
            get { return isNavigatedFromRouteHome; }
            set { isNavigatedFromRouteHome = value; }
        }

        private bool isRefreshInventoryTab = false;

        public bool IsRefreshInventoryTab
        {
            get { return isRefreshInventoryTab; }
            set { isRefreshInventoryTab = value; }
        }

        // Property to Check Is Checked Zero Quantity - Added By Velmani Karnan (12/13/2016)
        public bool isCheckedShowZeroQuantity = false;
        public bool IsCheckedShowZeroQuantity
        {
            get { return isCheckedShowZeroQuantity; }
            set { isCheckedShowZeroQuantity = value; }
        }
    }
}
