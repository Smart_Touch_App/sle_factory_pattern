﻿using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.ViewModelPayload
{
    public class PaymentPayload : PayloadBase
    {
        public CashDelivery CashDelivery { get; set; }
        public Customer Customer { get; set; }
        List<string> _InvoiceNo = new List<string>();

        public List<string> InvoiceNo { get { return _InvoiceNo; } set { _InvoiceNo = value; } }
        public string RouteID { get; set; }
        public string StopID
        {
            get;
            set;
        }

        /// <summary>
        /// Get or set the receipt id 
        /// It is used to store the receipt id when trying to open ReceiptHistory Screen from activity tab 
        /// </summary>
        public string ReceiptId
        {
            get;
            set;
        }

        /// <summary>
        /// Get or set flag to indicate the usage of customer activity screen 
        /// </summary>
        public bool IsSourceCustomerActivity
        {
            get;
            set;
        }
    }
}