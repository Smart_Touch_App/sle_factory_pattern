﻿using SalesLogicExpress.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModelPayload
{
    public  class RouteSettlementPayload: PayloadBase
    {
        private double totalCash;
        public double TotalCash
        {
            get { return totalCash; }
            set { totalCash = value; }
        }

        private double totalCheck;
        public double TotalCheck
        {
            get { return totalCheck; }
            set { totalCheck = value;  }
        }

        private double totalExpenses;
        public double TotalExpenses
        {
            get { return totalExpenses; }
            set { totalExpenses = value;  }
        }

        private double totalMoneyOrder;
        public double TotalMoneyOrder
        {
            get { return totalMoneyOrder; }
            set { totalMoneyOrder = value;  }
        }

        private double grandTotal;
        public double GrandTotal
        {
            get { return grandTotal; }
            set { grandTotal = value;  }
        }
        private bool isSettlementTabActive = false;

        public bool IsSettlementTabActive
        {
            get { return isSettlementTabActive; }
            set { isSettlementTabActive = value; }
        }

        private  ObservableCollection<CheckDetails> checkDetailsCollection;

        public  ObservableCollection<CheckDetails> CheckDetailsCollection
        {
            get { return checkDetailsCollection; }
            set { checkDetailsCollection = value; }
        }

        private  ObservableCollection<MoneyOrderDetails> moneyOrderDetailsCollection;

        public  ObservableCollection<MoneyOrderDetails> MoneyOrderDetailsCollection
        {
            get { return moneyOrderDetailsCollection; }
            set { moneyOrderDetailsCollection = value; }
        }

        private ObservableCollection<string> activityIDList;

        public ObservableCollection<string> ActivityIDList
        {
            get { return activityIDList; }
            set { activityIDList = value; }
        }

        private string settlementID;

        public string SettlementID
        {
            get { return settlementID; }
            set { settlementID = value; }
        }

        private string settlementNo;

        public string SettlementNo
        {
            get { return settlementNo; }
            set { settlementNo = value; }
        }

        private bool isForVerification = false;

        public bool IsForVerification
        {
            get { return isForVerification; }
            set { isForVerification = value; }
        }

        private string verifierID;

        public string VerifierID
        {
            get { return verifierID; }
            set { verifierID = value; }
        }

        private string verifierName;

        public string VerifierName
        {
            get { return verifierName; }
            set { verifierName = value; }
        }
        private string originatorName;

        public string OriginatorName
        {
            get { return originatorName; }
            set { originatorName = value; }
        }

        private string originatorID;

        public string OriginatorID
        {
            get { return originatorID; }
            set { originatorID = value; }
        }
        private bool isReinitialiseTransaction;

        public bool IsReinitialiseTransaction
        {
            get { return isReinitialiseTransaction; }
            set { isReinitialiseTransaction = value; }
        }
        private InkCanvas myCanvasPayload;

        public InkCanvas MyCanvasPayload
        {
            get { return myCanvasPayload; }
            set { myCanvasPayload = value; }
        }

        private List<string> receiptList = new List<string>();

        /// <summary>
        /// Add or remove receipt id
        /// </summary>
        public List<string> ReceiptList
        {
            get { return receiptList; }
            set { receiptList = value; }
        }

        private List<string> expenseList = new List<string>();

        public List<string> ExpenseList
        {
            get { return expenseList; }
            set { expenseList = value; }
        }

        private List<string> moneyOrderList = new List<string>();

        public List<string> MoneyOrderList
        {
            get { return moneyOrderList; }
            set { moneyOrderList = value; }
        }
    }
}
