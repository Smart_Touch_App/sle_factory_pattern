﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.GenericReport
{
    public struct ReportPayloads
    {
        public byte[] ReportFile;
        public string CurrentViewTitle;
        public string PreviousViewTitle;
        public Application.Helpers.ViewModelMappings.View CurrentView;
        public Application.Helpers.ViewModelMappings.View PreviousView;
        public bool ShowBackNavigation;
        public Guid MessageToken;
        public System.Drawing.Printing.PrinterSettings PrinterSetting;
        public bool ShowPrintDialog;
        public bool PrintDirectly;
    }

}
