﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.GenericReport
{
    public class GenericReportViewModel : ViewModels.BaseViewModel
    {
        object _ReportSource;

        public object ReportSource
        {
            get
            {
                return _ReportSource;
            }
            set
            {
                _ReportSource = value;
                OnPropertyChanged("ReportSource");
            }
        }

        private bool _disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            base.Dispose(disposing);
            _disposed = true;
        }
    }
}
